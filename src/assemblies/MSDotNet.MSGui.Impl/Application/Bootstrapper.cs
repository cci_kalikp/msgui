/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Application/Bootstrapper.cs#194 $
// $Change: 902897 $
// $DateTime: 2014/10/29 01:43:59 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.AccessControl;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;
//using Microsoft.Practices.Composite.Events;
//using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ScreenGroup;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeConfig;
using MorganStanley.MSDotNet.MSGui.Impl.ScreenGroup;
using MSDesktop.Isolation.Interfaces;
using Microsoft.Practices.Unity;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.IED.Concord.Runtime;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ShellMenu;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.StatusBar;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.SystemTray;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.TitleBar;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.Alert;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.LauncherBar;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Statusbar;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.SystemTray;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Titlebar;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.Concord;
using MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions.EnvironmentExtractor;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.HidingLocking;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.MailAttachment;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using MorganStanley.MSDotNet.MSGui.Impl.Options;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence.LayoutLoadingScreen;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.LayoutManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.UserPreference;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDotNet.My;
using StatusLevel = MorganStanley.MSDotNet.MSGui.Core.StatusLevel;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
    internal sealed class Bootstrapper : BootstrapperBase, IHideObjectMembers
    {
        private static readonly IMSLogger _logger = MSLoggerFactory.CreateLogger<Bootstrapper>();

        private readonly InitialSettings _initialSettings;
        private readonly Application _application;
        private readonly string _configFile;

        private IPersistenceStorage _persistenceStorage;
        private IShell _shell;
        private ChromeManagerBase _chromeManager;
        private readonly ChromeRegistry _chromeRegistry = new ChromeRegistry();
        private ICommonFactories _chromeHelper;
        private DispatcherTimer _shutdownTimer;
        private CommunicationChannelStatus _imcStatus = CommunicationChannelStatus.Required;
        private CommunicationChannelStatus _ipcStatus = CommunicationChannelStatus.Required;
         
        internal bool ConfigurationDependentTheme { get; set; }
        internal static bool ThemeUserConfigurable { get; set; }
        internal bool ConfigurationDependentShellMode { get; set; }
        internal static bool ShellModeUserConfigurable { get; set; }
        internal event EventHandler<EventArgs<string>> ConfigurationDependentThemeDetermined;

        internal const string DEFAULT_LAYOUT_NAME = "Default";

        public Bootstrapper(IUnityContainer container, InitialSettings initialSettings)
            : base(container)
        {
            MaxMemoryLogEntryCount = 200;
            container.RegisterInstance<IModuleLoadInfos>(moduleLoadInfos);
            _logger.Debug("Created a new instance of Bootstrapper.");
            _initialSettings = initialSettings;
            _application = new Application();
            InitializeShutdownTimer();
            _application.Settings.PropertyChanged += ApplicationSettingsOnPropertyChanged;
        }

        public Bootstrapper(IUnityContainer container, string configFile, InitialSettings initialSettings)
            : this(container, initialSettings)
        {
            _configFile = configFile;
            IsUseReflection = false;
            _logger.DebugWithFormat("Config file {0} has been provided to the bootstrapper", configFile);
        }

        private void InitializeShutdownTimer()
        {
            _shutdownTimer = new DispatcherTimer();
            _shutdownTimer.Tick += (o, args) =>
                {
                    if (_application.Settings.ShowExitDialog && ExitDialog.DefaultTimeout == null)
                    {
                        ExitDialog.DefaultTimeout = 60;
                        ExitTaskDialogLauncher.DefaultTimeout = 60;
                        _application.Settings.SaveOnExit = false;
                    }
                    _shell.Close();
                };
        }

        private void ApplicationSettingsOnPropertyChanged(object sender,
                                                          PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName == "ShutdownTime"
                && _application.Settings.IsShutdownTimeEnabled)
            {
                var utc = _application.Settings.IsShutdownTimeUTC;
                var shutdownDateTime = DateTime.Today + _application.Settings.ShutdownTime;
                if (utc) DateTime.SpecifyKind(shutdownDateTime, DateTimeKind.Utc);
                if (shutdownDateTime < (utc ? DateTime.UtcNow : DateTime.Now))
                    shutdownDateTime += new TimeSpan(1, 0, 0, 0);
                _shutdownTimer.Stop();
                var shutdownIn = shutdownDateTime - (utc ? DateTime.UtcNow : DateTime.Now);
                _shutdownTimer.Interval = shutdownIn;
                _shutdownTimer.Start();
            }
        }

        public bool IsUseReflection { get; set; }

        public Application Application
        {
            get { return _application; }
        }

        internal static Form ConcordFakeFormShortcut { get; set; }

        //public void AddModule(Type moduleType_, bool ent)
        //{
        //    m_logger.DebugWithFormat("Adding module {0}", moduleType_.FullName);
        //    m_registeredModuleTypes.Add(moduleType_);
        //    var entitlement = Container.TryResolve<IEntitlementService>();
        //    //entitlement.
        //}



        protected override IUnityContainer CreateContainer()
        {
            _logger.Debug("Creating new UnityContainer");
            return new UnityContainer();
        }


        protected override DependencyObject CreateShell()
        {
            _logger.Debug("Creating shell"); 
            SetDefaultValues();
            _initialSettings.Freeze();
            ModuleFrameworkHandler.BeforeModuleInitialized += OnBeforeModuleInitialized;
            ModuleFrameworkHandler.AfterModuleInitialized += OnAfterModuleInitialized;
            RegisterTypes();

            Container.Resolve<ISplashScreen>();


            //InitializeLayoutLoadStrategies();
            InitializeQuitBehaviour();
            AddPersistors();

            _initialSettings.AlertConsumer.Initialize(Container);
            _shell.Initialize();
            ThemeExtensions.RegisterMsGuiColorPalette(Container);
            return _shell.MainWindow;
        }

        private void InitializeShellMode()
        {
            if (!ConfigurationDependentShellMode ||
                !ConcordConfigurationManager.PreInitialize(new Arguments(Environment.GetCommandLineArgs())))
            {
                return;
              
            }
            try
            {
                var shellModeConfig = (Configurator)ConfigurationManager.GetConfig(UserPreferenceInitialSettings.ShellModeConfig);
                var shellModeString = shellModeConfig.GetValue(UserPreferenceInitialSettings.ShellModeConfig, null);
                ShellMode shellMode;
                if (ShellMode.TryParse(shellModeString, true, out shellMode) && UserPreferenceInitialSettings.AvailableShellModes.Contains(shellMode))
                { 
                    _initialSettings.ShellMode = shellMode;
                    if (shellMode == ShellMode.LauncherBarAndWindow ||
                        shellMode == ShellMode.RibbonWindow)
                    {
                        DockViewModel.UseHarmonia = false;
                    }
                }  
            }
            finally
            {
                ConfigurationManager.Reset();
            }
           
        }

        private void RegisterTypes()
        {
            _logger.Debug("Registering types...");
#if DEBUG_BREAK
			System.Diagnostics.Debugger.Break();
#endif
            Container.RegisterInstance<IApplication>(_application);
            Container.RegisterInstance(_application.Settings);
            Container.RegisterInstance(_initialSettings);
            Container.RegisterInstance(_persistenceStorage);
            Container.RegisterType<PersistenceProfileService, PersistenceProfileService>(
                new ContainerControlledLifetimeManager());
            Container.RegisterType<IPersistenceService, PersistenceService>(new ContainerControlledLifetimeManager());

            var persistenceService = (PersistenceService)Container.Resolve<IPersistenceService>();
            Container.RegisterInstance<IItemsDictionaryService>(persistenceService);
            Container.RegisterInstance<IPersistenceProfileServiceProxy>(persistenceService);
            ComponentPersistenceExtensions.PersistenceService = persistenceService;
            //Container.RegisterType<IStatusBar, StatusBarImpl>(new ContainerControlledLifetimeManager());
            Container.RegisterInstance<IChromeRegistry>(_chromeRegistry);

            var hidingLockingManager = new HidingLockingManager
                {
                    Granularity = ShellModeExtension.HideUIGranularity
                };

            Container.RegisterInstance<IHidingLockingManager>(hidingLockingManager);
            switch (_initialSettings.ShellMode)
            {
                case ShellMode.RibbonWindow:
                case ShellMode.RibbonAndFloatingWindows:
                case ShellMode.RibbonMDI:
                    _shell = new ShellWithRibbon(Container, _initialSettings);
                    break;
                case ShellMode.LauncherBarAndFloatingWindows:
                case ShellMode.LauncherBarAndWindow:
                    _shell = new ShellWithLauncher(Container, _initialSettings);
                    break;
                case ShellMode.External:
                    _shell = Container.Resolve<IExternalShellProvider>().GetShellInstance(Container, _initialSettings);
                    break;
                default:
                    throw new NotImplementedException("This type of shell does not have an implementation");
            }

            Container.RegisterInstance(_shell);
            Container.RegisterInstance<IStatusbarFactories>(new StatusbarHelper(_shell, _chromeRegistry));
            Container.RegisterInstance<ITitleBarFactories>(new TitleBarFactories(_shell, _chromeRegistry));
            Container.RegisterInstance<IChromeElementInitializerRegistry>(_chromeRegistry);

            StatusbarExtensions.InitHelpers(Container);
            Container.RegisterInstance<ISystemTrayFactories>(new SystemTrayFactories(_chromeRegistry));
            SystemTrayExtensions.InitHelpers(Container);

            var windowFactory = new WindowFactory(_chromeRegistry, _shell.WindowManager);

            switch (_initialSettings.ShellMode)
            {
                case ShellMode.RibbonWindow:
                case ShellMode.RibbonMDI:
                case ShellMode.RibbonAndFloatingWindows:
                    _chromeManager = new RibbonChromeManager(_chromeRegistry, _shell, null, persistenceService,
                                                             Container, windowFactory, _initialSettings);
                    _chromeHelper = new RibbonFactories(_chromeRegistry);
                    break;
                case ShellMode.LauncherBarAndFloatingWindows:
                case ShellMode.LauncherBarAndWindow:
                    _chromeManager = new LauncherBarChromeManager(_chromeRegistry, _shell, null, persistenceService,
                                                                  Container, windowFactory);
                    _chromeHelper = new LauncherBarFactories(_chromeRegistry);
                    break;
                case ShellMode.External:
                    _chromeManager = Container.Resolve<IExternalShellProvider>().GetChromeManagerInstance(_chromeRegistry, _shell, null, persistenceService, Container, windowFactory);
                    _chromeHelper = Container.Resolve<IExternalShellProvider>().GetFactoriesInstance(_chromeRegistry);
                    break;
                default:
                    throw new NotImplementedException("This type of shell does not have an implementation");
            }

            if (ribbonIcon != null)
            {
                _chromeManager.SetRibbonIcon(ribbonIcon);
            }
            var moduleGroup = new HostProcessModuleGroup(Container, _chromeManager, windowFactory, _chromeRegistry);
            Container.RegisterInstance<IModuleGroup>(moduleGroup);
            Container.RegisterInstance(moduleGroup);

            Container.RegisterInstance<IChromeManager>(_chromeManager);
            Container.RegisterInstance<ISubLayoutManager>(_chromeManager);
            Container.RegisterInstance<ILegacyWorkspacesManager>(_chromeManager);
            Container.RegisterInstance<IViewManager>(_chromeManager);
            Container.RegisterInstance(_chromeHelper);
            CommonHelpers.InitHelpers(Container);

            if (_initialSettings.ShellMode == ShellMode.RibbonWindow ||
                _initialSettings.ShellMode == ShellMode.RibbonMDI)
            {
                ShellMenuExtensions.InitHelpers(Container);
            }

            Container.RegisterInstance(
                _chromeManager[ChromeManagerBase.STATUSBAR_COMMANDAREA_NAME] as IStatusBar);

            /*
			 * These are only needed for controlParadigm compatibility. Due to the differences in how the interfaces
			 * work, CR implementing ICPR has to inject placements into CM and build controls via CH.
			 */
            _chromeRegistry.ChromeHelper = _chromeHelper;
            _chromeRegistry.ChromeManager = _chromeManager;

            Container.RegisterType<IApplicationOptions, ApplicationOptions>(new ContainerControlledLifetimeManager());
            Container.RegisterType<ISplashScreen, SplashScreen>(new ContainerControlledLifetimeManager());
            Container.RegisterType<LayoutLoadingScreenRetriever>(new ContainerControlledLifetimeManager());
            Container.RegisterType<LayoutLoadingScreenLauncher>(new ContainerControlledLifetimeManager());
            //			Container.RegisterType<IViewManager, ChromeManager>(new ContainerControlledLifetimeManager());
            //			Container.RegisterType<IViewManager, ViewManager>(new ContainerControlledLifetimeManager());
            Container.RegisterInstance<IAlert>(new AlertImpl(_initialSettings.AlertConsumer));
            Container.RegisterInstance<IControlParadigmRegistrator>(_chromeRegistry);
            Container.RegisterInstance<IControlParadigmImplementator>(_chromeManager);

            //Container.RegisterType<IControlParadigmRegistrator, ControlParadigmRegistrator>(new ContainerControlledLifetimeManager());
            //Container.RegisterType<IControlParadigmImplementator, ControlParadigmImplementator>(new ContainerControlledLifetimeManager());

            ModuleFrameworkHandler.DisableEventAggregatorBridge = DisableEventAggregatorBridge;
            ModuleFrameworkHandler.RegisterTypes();

            //Container.RegisterInstance<IEventAggregator>(eventAggregator);
            //Container.RegisterInstance(new AdapterService(eventAggregator));
            Container.RegisterInstance(new AdditionalLayoutInfo(Container));

            Container.RegisterType<IConcordTypeToViewCreator, ConcordTypeToViewCreator>(
                new ContainerControlledLifetimeManager());

            //		  CustomContentPane.viewManager = (IViewManager)Container.Resolve<IViewManager>();
            Container.RegisterInstance<IApplication>(Application);

            Container.RegisterType<IAdapterService, ViewMessageAdapterService>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IEventRouter, EventRouter>(new ContainerControlledLifetimeManager());

            var communicatorSettings = new CommunicatorSettings(Process.GetCurrentProcess().Id, _ipcStatus, _imcStatus);
            Container.RegisterInstance<ICommunicatorSettings>(communicatorSettings, new ContainerControlledLifetimeManager());

            //Container.RegisterType<ICommunicator, Communicator>(new ContainerControlledLifetimeManager());
            //IPCCompositePresentationEventContainerHolder.Container = Container;
            //Container.RegisterType<IEventRegistrator, InterProcessMessenger>(new ContainerControlledLifetimeManager());
            Container.RegisterInstance(new IpcModuleLoadHacker(_shell));

            Container.RegisterType<IMessageTypeRegistrator, MessageTypeRegistrator>(
                new ContainerControlledLifetimeManager());
            //Container.RegisterType<V2VConfigModule>(new ContainerControlledLifetimeManager());

            if (RegisterJsonFormatter)
            {
                Container.RegisterType<IFormatter, JsonFormatter>();
            }
            else
            {
                Container.RegisterType<IFormatter, CustomizedFormatter>();
            }

            Container.RegisterType<IDelayedProfileLoader, DelayedProfileLoader>(new ContainerControlledLifetimeManager());

            Container.RegisterInstance<IMailAttachmentService>(new MailAttachmentService(Application));

            Container.RegisterInstance(WidgetStatesInfo.LocalWidgetStatesInfoInstanceId,
                                       new WidgetStatesInfo(WidgetStatesInfo.LocalWidgetStatesInfoInstanceId, Container));
            Container.RegisterInstance(WidgetStatesInfo.GlobalWidgetStatesInfoInstanceId,
                                       new WidgetStatesInfo(WidgetStatesInfo.GlobalWidgetStatesInfoInstanceId, Container));

            if (ScreenGroupManager.IsScreenGroupSupportEnabled)
            {
                Container.RegisterType<IScreenGroupManager, ScreenGroupManager>( new ContainerControlledLifetimeManager());
            }
        }

        private void InitializeQuitBehaviour()
        {
            //if (m_quitDialogSeconds != -1)
            //{
            //  m_shell.SetQuitDialogAutomaticBehaviour(m_quitDialogSeconds, m_quitDialogSave);
            //}
        }

        private void SetDefaultValues()
        {
            _logger.Debug("Setting default values");
            if (_persistenceStorage == null) //not specified by user before
            {
                //m_persistenceStorage = new FilePersistenceStorage(m_application.Name); //default storage
                _persistenceStorage = MSDesktopLayoutsAdapter.Fps is ConcordPersistenceStorage
                                          ? MSDesktopLayoutsAdapter.Fps
                                          : new ConcordPersistenceStorage();
            }
            if (_initialSettings.WindowRenameGesture == null)
            {
                _initialSettings.WindowRenameGesture = new KeyGesture(Key.F2, ModifierKeys.Alt);
            }
            if (_initialSettings.AlertConsumer == null)
            {
                _initialSettings.AlertConsumer = new AlertConsumerNullImplementation();
            }

            // Overwrite app name with app name to init some variables

            SetApplicationName(Application.Name ??
                               EntryAssemblyHelper.GetEntryAssembly().GetName().Name.Replace(".", " "));

            InitializeShellMode();
        }

        protected override void SetUpLogging()
        {
            base.SetUpLogging();
            if (EnableLogging)
            {
                try
                {

                    string directory = Path.Combine(
                        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                        "Morgan Stanley",
                        Application.Name,
                        "Log");
                    if (!string.IsNullOrEmpty(LoggingParameter.Directory))
                    {
                        directory = PathUtilities.ReplaceEnvironmentVariables(LoggingParameter.Directory);
                    }
                    if (!string.IsNullOrEmpty(LoggingParameter.ApplicationName))
                    {
                        directory = Path.Combine(
                            Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                            "Morgan Stanley",
                            LoggingParameter.ApplicationName,
                            "Log");
                    }

                    if (!(System.IO.Directory.Exists(directory)))
                    {
                        try
                        {
                            System.IO.Directory.CreateDirectory(directory);
                        }
                        catch
                        {
                            // this means we cannot log, so, cannot log the error
                        }
                    }
                    if (System.IO.Directory.Exists(directory))
                    {
                        if (
                            System.IO.Directory.GetAccessControl(directory)
                                  .GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount))
                                  .
                                   Cast<FileSystemAccessRule>()
                                  .Any(accessRule => accessRule.AccessControlType == AccessControlType.Allow))
                        {
                            var logFilename = string.Format("log{0}.log",
                                                            string.IsNullOrEmpty(LoggingParameter.Basename)
                                                                ? Process.GetCurrentProcess()
                                                                         .Id.ToString(CultureInfo.InvariantCulture)
                                                                : LoggingParameter.Basename);
                            if (LoggingParameter.LoggingLevels == null || LoggingParameter.LoggingLevels.Count == 0)
                                // we listen to all when 1. LoggingLevels is not provided, 2. none is specified in the LoggingLevels.
                                MSLog.MSLog.Init(
                                    Path.Combine(directory, logFilename),
                                    MSLogPriority.All); 
                            else
                                MSLog.MSLog.Init(Path.Combine(directory, logFilename));
                        }
                    }

                    if (LoggingParameter.LoggingLevels != null)
                    {
                        foreach (var target in LoggingParameter.LoggingLevels.Keys)
                        {
                            try
                            {
                                MSLog.MSLog.SetThresholdPolicy(target, LoggingParameter.LoggingLevels[target]);
                            }
                            catch (Exception ex_)
                            {
                                //TODO:?
                            }
                        }
                    }

                }
                catch (MSLogDestinationRegistry.UsedDestinationNameException exc)
                {
                }
            }

            // Register the in-memory destination - invariant to the logging settings.
            MSLog.MSLog.AddDestination("/default/errorlogging",
                                       new MSLogMemoryRollingDestination("ExceptionLogger", MaxMemoryLogEntryCount)); 
        }

        #region Logging

        internal struct LoggingParameterInfo
        {
            public string Directory;
            public string Basename;
            public IDictionary<string, string> LoggingLevels;
            public string ApplicationName;

            public LoggingParameterInfo(string directory_, string basename_, IDictionary<string, string> loggingLevels_,
                                        string applicationName_)
            {
                this.Directory = directory_;
                this.Basename = basename_;
                this.LoggingLevels = loggingLevels_;
                this.ApplicationName = applicationName_;
            }
        }

        internal bool EnableLogging { get; set; }
        internal int MaxMemoryLogEntryCount { get; set; }
        internal LoggingParameterInfo LoggingParameter { get; set; }

        public bool DisableEventAggregatorBridge { get; set; }

        #endregion Logging



        private void InitializeLayoutLoadStrategies()
        {
            if (_initialSettings.LayoutLoadStrategy == LayoutLoadStrategy.UseCallback)
            {
                _shell.SetLayoutLoadStrategy(_initialSettings.LayoutLoadStrategy,
                                             _initialSettings.LayoutLoadStrategyCallback);
            }
            else
            {
                _shell.SetLayoutLoadStrategy(_initialSettings.LayoutLoadStrategy);
            }
        }

        internal static bool StorageAvailableListStale;

        protected override void InitializeModules()
        {
            _logger.Debug("Initializing modules");

            PersistenceService persistenceService = (PersistenceService)Container.Resolve<IPersistenceService>();
            PersistenceProfileService persistenceProfileService = Container.Resolve<PersistenceProfileService>(); 

            IPersistenceStorage storage = Container.Resolve<IPersistenceStorage>();

            if (_initialSettings.ConcordEnabled)
            {
                ConcordPersistenceStorage.Container = Container;
                //ConcordConfigurationManager.container_ = Container;
            }
            // IPersistenceStorage may depend on the initialization of the ConcordConfigurationManager.Container
            storage.InitializeStorage();
             
            Container.RegisterInstance("CurrentConcordUser", ConcordRuntime.Current.UserInfo.Username);
            Container.RegisterInstance("CurrentConcordProfile", ConcordConfigurationManager.CurrentProfile);
            Container.RegisterInstance("CurrentConcordProfileValue", ConcordConfigurationManager.GetConcordProfileString(ConcordConfigurationManager.CurrentProfile));

            OnAfterPersistenceStorageInitialized(new ContainerEventArgs(Container));
            if (_initialSettings.ConcordEnabled)
            {
                ConcordInitializer.BeforeModuleInitialized += OnBeforeModuleInitializedConcord;
                ConcordInitializer.AfterModuleInitialized += OnAfterModuleInitializedConcord;
                ConcordInitializer.InitializeConcordApplication(Container, persistenceService, persistenceProfileService,
                                                                string.IsNullOrEmpty(
                                                                    _initialSettings.ConcordDefaultProfile)
                                                                    ? null
                                                                    : _initialSettings.ConcordDefaultProfile,
                                                                EnableLogging);
            }

            AddOptionsPages();
            InitializeThemes();
            OnBeforeInitializeModules(new ContainerEventArgs(Container));
            base.InitializeModules();

            ModuleFrameworkHandler.InvokeAllModulesLoaded();
            // apply the storage, when used in Concord, Apply shall initialize the COncord Layout.
            storage.Apply();
            StorageAvailableListStale = true;

            string defaultName = _persistenceStorage.DefaultName ?? DEFAULT_LAYOUT_NAME;
            if (
                !string.IsNullOrEmpty(
                    Environment.GetCommandLineArgs()
                               .FirstOrDefault(s => s.ToLowerInvariant().StartsWith("/defaultlayout:"))))
            {
                string newDefaultName =
                    Environment.GetCommandLineArgs()
                               .FirstOrDefault(s => s.ToLowerInvariant().StartsWith("/defaultlayout:"))
                               .Substring("/defaultlayout:".Length);

                _logger.InfoWithFormat("Overriding default layout {0} with {1}", defaultName, newDefaultName);
                defaultName = newDefaultName;
            }

            if (ChromeManagerBase.WidgetUserConfigEnabled && persistenceProfileService.AvailableProfiles.Contains(defaultName) &&
                persistenceProfileService.GetProfileItem(defaultName, ChromeConfigViewModel.ChromeConfigPersistorId) !=
                null)
            {
                ChromeManagerBase.WillLoadChromeFromState = true;
            }


            //enable offscreen window initialization
            FloatingWindowUtil.HideLoadingWindows = true;
            if (_initialSettings.LegacyControlPlacementEnabled)
            {
                //controlParadigmImplementator.PlaceAllControls();
                this._chromeManager.CreateChrome(true);
            }
            else
            {
                this._chromeManager.CreateChrome();
            }

            if (_initialSettings.ConcordEnabled)
            {
                RegisterConcordShortcuts();
            }
             
            _shell.PostChromeCreationInitialize();  

            _shell.SetLayoutLoadStrategy(LayoutLoadStrategy.NeverAsk);
           
            _chromeManager.StatusBar.MainItem.UpdateStatus(StatusLevel.Information, "Loading layout" + " '" + defaultName + "'");
            if (persistenceProfileService.AvailableProfiles.Contains(defaultName) &&
                !DelayedProfileLoadingExtensionPoints.DelayLoadProfile)
            {
                _shell.Show();
                persistenceProfileService.SetCurrentProfile(defaultName);
                _shell.LoadProfile(defaultName, true);
            }
            else
            {
                _shell.Show();
                persistenceProfileService.SetCurrentProfile(defaultName); 
                ChromeManagerBase viewManager = (ChromeManagerBase)Container.Resolve<IViewManager>();
                viewManager.LoadState(null);
                _shell.LoadProfile(null, true);
                Container.Resolve<IDelayedProfileLoader>().PersistanceInitialized.Set();                
            }

            InitializeLayoutLoadStrategies();

        }

        private void RegisterConcordShortcuts()
        { 
            #region

            if (ConcordExtensionStorage.KeyBindings.Count > 0)
            {
                Window window = _shell.MainWindow;
                foreach (object tool in ConcordExtensionStorage.KeyBindings.Keys)
                {
                    ConcordExtensionStorage.ShortcutMetadata shortcutMetadata =
                        ConcordExtensionStorage.KeyBindings[tool];

                    if (shortcutMetadata.Shortcut != System.Windows.Forms.Shortcut.None)
                    {
                        var k = Key.MediaStop;
                        var mk = ModifierKeys.None;

                        #region Shortcut mapping...

                        switch (shortcutMetadata.Shortcut)
                        {
                            case Shortcut.None:
                                k = Key.None;
                                mk = ModifierKeys.None;
                                break;
                            case Shortcut.Ins:
                                k = Key.Insert;
                                mk = ModifierKeys.None;
                                break;
                            case Shortcut.Del:
                                k = Key.Delete;
                                mk = ModifierKeys.None;
                                break;
                            case Shortcut.F1:
                                k = Key.F1;
                                mk = ModifierKeys.None;
                                break;
                            case Shortcut.F2:
                                k = Key.F2;
                                mk = ModifierKeys.None;
                                break;
                            case Shortcut.F3:
                                k = Key.F3;
                                mk = ModifierKeys.None;
                                break;
                            case Shortcut.F4:
                                k = Key.F4;
                                mk = ModifierKeys.None;
                                break;
                            case Shortcut.F5:
                                k = Key.F5;
                                mk = ModifierKeys.None;
                                break;
                            case Shortcut.F6:
                                k = Key.F6;
                                mk = ModifierKeys.None;
                                break;
                            case Shortcut.F7:
                                k = Key.F7;
                                mk = ModifierKeys.None;
                                break;
                            case Shortcut.F8:
                                k = Key.F8;
                                mk = ModifierKeys.None;
                                break;
                            case Shortcut.F9:
                                k = Key.F9;
                                mk = ModifierKeys.None;
                                break;
                            case Shortcut.F10:
                                k = Key.F10;
                                mk = ModifierKeys.None;
                                break;
                            case Shortcut.F11:
                                k = Key.F11;
                                mk = ModifierKeys.None;
                                break;
                            case Shortcut.F12:
                                k = Key.F12;
                                mk = ModifierKeys.None;
                                break;
                            case Shortcut.ShiftIns:
                                k = Key.Insert;
                                mk = ModifierKeys.Shift;
                                break;
                            case Shortcut.ShiftDel:
                                k = Key.Delete;
                                mk = ModifierKeys.Shift;
                                break;
                            case Shortcut.ShiftF1:
                                k = Key.F1;
                                mk = ModifierKeys.Shift;
                                break;
                            case Shortcut.ShiftF2:
                                k = Key.F2;
                                mk = ModifierKeys.Shift;
                                break;
                            case Shortcut.ShiftF3:
                                k = Key.F3;
                                mk = ModifierKeys.Shift;
                                break;
                            case Shortcut.ShiftF4:
                                k = Key.F4;
                                mk = ModifierKeys.Shift;
                                break;
                            case Shortcut.ShiftF5:
                                k = Key.F5;
                                mk = ModifierKeys.Shift;
                                break;
                            case Shortcut.ShiftF6:
                                k = Key.F6;
                                mk = ModifierKeys.Shift;
                                break;
                            case Shortcut.ShiftF7:
                                k = Key.F7;
                                mk = ModifierKeys.Shift;
                                break;
                            case Shortcut.ShiftF8:
                                k = Key.F8;
                                mk = ModifierKeys.Shift;
                                break;
                            case Shortcut.ShiftF9:
                                k = Key.F9;
                                mk = ModifierKeys.Shift;
                                break;
                            case Shortcut.ShiftF10:
                                k = Key.F10;
                                mk = ModifierKeys.Shift;
                                break;
                            case Shortcut.ShiftF11:
                                k = Key.F11;
                                mk = ModifierKeys.Shift;
                                break;
                            case Shortcut.ShiftF12:
                                k = Key.F12;
                                mk = ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlIns:
                                k = Key.Insert;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlDel:
                                k = Key.Delete;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.Ctrl0:
                                k = Key.D0;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.Ctrl1:
                                k = Key.D1;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.Ctrl2:
                                k = Key.D2;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.Ctrl3:
                                k = Key.D3;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.Ctrl4:
                                k = Key.D4;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.Ctrl5:
                                k = Key.D5;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.Ctrl6:
                                k = Key.D6;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.Ctrl7:
                                k = Key.D7;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.Ctrl8:
                                k = Key.D8;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.Ctrl9:
                                k = Key.D9;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlA:
                                k = Key.A;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlB:
                                k = Key.B;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlC:
                                k = Key.C;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlD:
                                k = Key.D;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlE:
                                k = Key.E;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlF:
                                k = Key.F;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlG:
                                k = Key.G;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlH:
                                k = Key.H;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlI:
                                k = Key.I;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlJ:
                                k = Key.J;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlK:
                                k = Key.K;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlL:
                                k = Key.L;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlM:
                                k = Key.M;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlN:
                                k = Key.N;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlO:
                                k = Key.O;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlP:
                                k = Key.P;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlQ:
                                k = Key.Q;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlR:
                                k = Key.R;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlS:
                                k = Key.S;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlT:
                                k = Key.T;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlU:
                                k = Key.U;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlV:
                                k = Key.V;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlW:
                                k = Key.W;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlX:
                                k = Key.X;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlY:
                                k = Key.Y;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlZ:
                                k = Key.Z;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlF1:
                                k = Key.F1;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlF2:
                                k = Key.F2;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlF3:
                                k = Key.F3;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlF4:
                                k = Key.F4;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlF5:
                                k = Key.F5;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlF6:
                                k = Key.F6;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlF7:
                                k = Key.F7;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlF8:
                                k = Key.F8;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlF9:
                                k = Key.F9;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlF10:
                                k = Key.F10;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlF11:
                                k = Key.F11;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlF12:
                                k = Key.F12;
                                mk = ModifierKeys.Control;
                                break;
                            case Shortcut.CtrlShift0:
                                k = Key.D0;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShift1:
                                k = Key.D1;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShift2:
                                k = Key.D2;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShift3:
                                k = Key.D3;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShift4:
                                k = Key.D4;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShift5:
                                k = Key.D5;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShift6:
                                k = Key.D6;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShift7:
                                k = Key.D7;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShift8:
                                k = Key.D8;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShift9:
                                k = Key.D9;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftA:
                                k = Key.A;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftB:
                                k = Key.B;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftC:
                                k = Key.C;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftD:
                                k = Key.D;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftE:
                                k = Key.E;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftF:
                                k = Key.F;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftG:
                                k = Key.G;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftH:
                                k = Key.H;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftI:
                                k = Key.I;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftJ:
                                k = Key.J;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftK:
                                k = Key.K;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftL:
                                k = Key.L;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftM:
                                k = Key.M;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftN:
                                k = Key.N;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftO:
                                k = Key.O;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftP:
                                k = Key.P;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftQ:
                                k = Key.Q;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftR:
                                k = Key.R;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftS:
                                k = Key.S;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftT:
                                k = Key.T;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftU:
                                k = Key.U;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftV:
                                k = Key.V;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftW:
                                k = Key.W;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftX:
                                k = Key.X;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftY:
                                k = Key.Y;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftZ:
                                k = Key.Z;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftF1:
                                k = Key.F1;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftF2:
                                k = Key.F2;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftF3:
                                k = Key.F3;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftF4:
                                k = Key.F4;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftF5:
                                k = Key.F5;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftF6:
                                k = Key.F6;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftF7:
                                k = Key.F7;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftF8:
                                k = Key.F8;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftF9:
                                k = Key.F9;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftF10:
                                k = Key.F10;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftF11:
                                k = Key.F11;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.CtrlShiftF12:
                                k = Key.F12;
                                mk = ModifierKeys.Control | ModifierKeys.Shift;
                                break;
                            case Shortcut.AltBksp:
                                k = Key.Back;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.AltLeftArrow:
                                k = Key.Left;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.AltUpArrow:
                                k = Key.Up;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.AltRightArrow:
                                k = Key.Right;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.AltDownArrow:
                                k = Key.Down;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.Alt0:
                                k = Key.D0;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.Alt1:
                                k = Key.D1;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.Alt2:
                                k = Key.D2;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.Alt3:
                                k = Key.D3;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.Alt4:
                                k = Key.D4;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.Alt5:
                                k = Key.D5;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.Alt6:
                                k = Key.D6;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.Alt7:
                                k = Key.D7;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.Alt8:
                                k = Key.D8;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.Alt9:
                                k = Key.D9;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.AltF1:
                                k = Key.F1;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.AltF2:
                                k = Key.F2;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.AltF3:
                                k = Key.F3;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.AltF4:
                                k = Key.F4;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.AltF5:
                                k = Key.F5;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.AltF6:
                                k = Key.F6;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.AltF7:
                                k = Key.F7;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.AltF8:
                                k = Key.F8;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.AltF9:
                                k = Key.F9;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.AltF10:
                                k = Key.F10;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.AltF11:
                                k = Key.F11;
                                mk = ModifierKeys.Alt;
                                break;
                            case Shortcut.AltF12:
                                k = Key.F12;
                                mk = ModifierKeys.Alt;
                                break;
                        }

                        #endregion Shortcut mapping...

                        if (shortcutMetadata.Command != null)
                        {
                            var keyBinding = new KeyBinding(shortcutMetadata.Command, k, mk);
                            window.InputBindings.Add(keyBinding);
                        }
                    }
                }
            }

            #endregion
        }

        private void AddPersistors()
        {
            _logger.Debug("Adding persistors");
            var persistenceService = (PersistenceService)Container.Resolve<IPersistenceService>();
            var additionalLayoutInfo = Container.Resolve<AdditionalLayoutInfo>();

            var viewManager = (ChromeManagerBase)Container.Resolve<IViewManager>();

            persistenceService.AddPersistor(_shell.PersistorId, _shell.LoadState, _shell.SaveState);
            persistenceService.AddPersistor(viewManager.PersistorId, viewManager.LoadState, viewManager.SaveState, ChromeManagerBase.AsynchronousLayoutLoading);

            persistenceService.AddPersistor(additionalLayoutInfo.PersistorId, additionalLayoutInfo.LoadState, additionalLayoutInfo.SaveState);

            var layoutScreenshotProvider = Container.Resolve<LayoutScreenshotProvider>();
            Container.RegisterInstance<LayoutScreenshotProvider>(layoutScreenshotProvider);
            persistenceService.AddGlobalPersistor(layoutScreenshotProvider.PersistorId, layoutScreenshotProvider.LoadState, layoutScreenshotProvider.SaveState);

            var iao = (IPersistable)Container.Resolve<IApplicationOptions>();
            persistenceService.AddPersistor(iao.PersistorId, iao.LoadState, iao.SaveState);

            var localWidgetStatesInfo =
                Container.Resolve<WidgetStatesInfo>(WidgetStatesInfo.LocalWidgetStatesInfoInstanceId);
            var globalWidgetStatesInfo =
                Container.Resolve<WidgetStatesInfo>(WidgetStatesInfo.GlobalWidgetStatesInfoInstanceId);

            persistenceService.AddPersistor(localWidgetStatesInfo.PersistorId, localWidgetStatesInfo.LoadState,
                                            localWidgetStatesInfo.SaveState);
            persistenceService.AddGlobalPersistor(globalWidgetStatesInfo.PersistorId, globalWidgetStatesInfo.LoadState,
                                                  globalWidgetStatesInfo.SaveState);

            //persistenceService.AddPersistor(eventRegistrator.PersistorId, eventRegistrator.LoadState, eventRegistrator.SaveState);
            // IPersistable localConnectionManager = Container.Resolve<IpcLocalConnectionManager>();
            // persistenceService.AddPersistor(localConnectionManager.PersistorId, localConnectionManager.LoadState, localConnectionManager.SaveState);

            //IPersistable externalConnectionManager = Container.Resolve<ExternalConnectionManager>();
            //persistenceService.AddPersistor(externalConnectionManager.PersistorId, externalConnectionManager.LoadState, externalConnectionManager.SaveState);
            //persistenceService.AddPersistor(eventRegistrator.PersistorId, eventRegistrator.LoadState, eventRegistrator.SaveState);
        }

        private void AddOptionsPages()
        {
            _logger.Debug("Adding option pages");
            var options = Container.Resolve<IApplicationOptions>();
 if (ShellModeUserConfigurable && ThemeUserConfigurable)
            {
                options.AddOptionPage("MSDesktop", "Shell Mode and Theme", new InitialSettingsOptionView(true, true, AvailableThemes));
            }
            else if (ShellModeUserConfigurable)
            {
                options.AddOptionPage("MSDesktop", "Shell Mode", new InitialSettingsOptionView(true, false, null));
            }
            else if (ThemeUserConfigurable)
            {
                options.AddOptionPage("MSDesktop", "Theme", new InitialSettingsOptionView(false, true, AvailableThemes));
            }
            options.AddViewModelBasedOptionPage<ApplicationSettings>("MSDesktop", "Application Settings", true);
            var rename = new RenameDeleteOptionsView(Container.Resolve<PersistenceProfileService>());
            rename.UpdateMenu += rename_UpdateMenu;
            options.AddOptionPage("MSDesktop", "Layout Manager", rename);

            //m_options.AddOptionPage("MSGui", "Application Info",
            //  new ApplicationInfoOptionsView(m_persistenceStorage, m_enableConcord));
        }

        private void rename_UpdateMenu(object sender, EventArgs e)
        {
            switch (_initialSettings.ShellMode)
            {
                case ShellMode.RibbonWindow:
                case ShellMode.RibbonMDI:
                case ShellMode.RibbonAndFloatingWindows:
                    ((ShellWithRibbon)_shell).ShellWindow.RedrawMenus();
                    break;
                case ShellMode.LauncherBarAndFloatingWindows:
                case ShellMode.LauncherBarAndWindow:
                    ((ShellWithLauncher)_shell).OnRedrawNeeded(null, null);
                    break;
            }
        }

        public void SetPersistenceStorage(IPersistenceStorage storage)
        {
            _logger.DebugWithFormat("Setting persistence storage to {0}", storage.GetType().FullName);
            if (_persistenceStorage == null)
            {
                //m_persistenceStorage = storage_;
                MSDesktopLayoutsAdapter.Fps = storage;
                var concordStorage = storage as ConcordPersistenceStorage;
                if (concordStorage != null)
                {
                    concordStorage.ApplicationName = Application.Name;
                    concordStorage.ApplicationVersion = Application.Version;
                }
                return;
            }
            throw new InvalidOperationException(
                "It is allowed to set persistance storage only once before the shell is created.");
        }

        #region Themes

        private readonly IList<ThemeInfo> m_themesToBeAdded = new List<ThemeInfo>();
        private bool m_disableThemeChanger = true;
        private string m_startupTheme;

        public void SuppressDefaultThemes()
        {
        }

        internal readonly IList<ThemeInfo> AvailableThemes = new List<ThemeInfo>();
        public void AddAvailableTheme(string name, ImageSource icon)
        {
            AvailableThemes.Add(new ThemeInfo(name, icon));
        }

        public void AddTheme(Theme theme)
        {
            m_themesToBeAdded.Add(new ThemeInfo(theme.Name, theme.Icon, theme));
        }

        public void AddTheme(string name, ImageSource icon, Theme theme)
        {
            m_themesToBeAdded.Add(new ThemeInfo(name, icon, theme));
        }

        private void InitializeThemes()
        {
            Theme defaultTheme = null;

            // m_supressDefaultThemes is constant true now, to get rid of FX theme dependency

            //if (!m_supressDefaultThemes)
            //{
            //  defaultTheme = Themes.ThemeCreator.CreateDarkBlueTheme(Container);
            //  m_themesToBeAdded.Add(new ThemeInfo(defaultTheme));
            //}

            if (ConfigurationDependentTheme)
            {
                var themeConfig = (Configurator)ConfigurationManager.GetConfig(UserPreferenceInitialSettings.ThemeConfig);
                var themeName = themeConfig.GetValue(UserPreferenceInitialSettings.ThemeConfig, null);
                OnConfigurationDependentThemeDetermined(new EventArgs<string>(themeName));
            }


            if (m_themesToBeAdded.Count == 0)
            {
                _logger.Error(
                    "There is no theme set in the current context. Please reference the Themes dll, and call one of the Add****Theme() methods at boot time");
                throw new ArgumentOutOfRangeException("NumberOfThemes", 0,
                                                      @"There is no theme set in the current context. Please reference the Themes dll, and call one of the Add****Theme() methods at boot time");
            }

            if (m_themesToBeAdded.Count > 1)
            {
                _logger.ErrorWithFormat(
                    "There are multiple ({0}) themes defined at boot time. Please remove the multiple Add***Theme() methods",
                    m_themesToBeAdded.Count);
                throw new ArgumentOutOfRangeException("NumberOfThemes", m_themesToBeAdded.Count,
                                                      string.Format(
                                                          @"There are multiple ({0}) themes defined at boot time. Please remove the multiple Add***theme() methods",
                                                          m_themesToBeAdded.Count));
            }

            foreach (ThemeInfo themeInfo in m_themesToBeAdded)
            {
                _shell.AddTheme(themeInfo.Name, themeInfo.Icon, themeInfo.Theme);
            }

            if (m_startupTheme != null)
            {
                _shell.SetTheme(m_startupTheme);
            }
            else if (defaultTheme != null)
            {
                _application.SetTheme(defaultTheme.Name, _initialSettings);
            } //if theme initialization is disabled and no theme is set, it is a problem

            if (m_disableThemeChanger)
            {
                _shell.DisableThemeChanger();
            }
        }

        public void SetTheme(string name)
        {
            m_startupTheme = name;
        }

        public void DisableThemeChanger()
        {
            m_disableThemeChanger = true;
        }

        internal struct ThemeInfo
        {
            private readonly string _name;
            private readonly ImageSource _icon;
            private readonly Theme _theme;
            public ThemeInfo(string name, ImageSource icon)
            {
                _name = name;
                _icon = icon;
                _theme = null;
            }
            public ThemeInfo(string name, ImageSource icon, Theme theme)
            {
                _theme = theme;
                _icon = icon;
                _name = name;
            }

            public ThemeInfo(Theme theme)
            {
                _name = theme.Name;
                _icon = theme.Icon;
                _theme = theme;
            }

            public string Name
            {
                get { return _name; }
            }

            public ImageSource Icon
            {
                get { return _icon; }
            }

            public Theme Theme
            {
                get { return _theme; }
            }
        }

        #endregion Themes

        public void EnableConfigEnvironmentVariableExtractor()
        {
            RegisterEnvironmentVariablesInSystem(CreateDefaultConfigEnvironmentVariableExtractor());
        }

        public void EnableConfigEnvironmentVariableExtractor(IVersionConfigurationReader configurationReader,
                                                             IEnvironmentVariableDetailsConfigurationReader
                                                                 variableDetailsConfigurationReader)
        {
            RegisterEnvironmentVariablesInSystem(new ConfigEnvironmentVariableExtractor(configurationReader,
                                                                                        variableDetailsConfigurationReader));
        }

        private static void RegisterEnvironmentVariablesInSystem(ConfigEnvironmentVariableExtractor extractor)
        {
            _logger.Debug("Registering environment variables in system");
            EnvironmentVariables environmentVariables = extractor.Read();

            foreach (EnvironmentVariable envVar in environmentVariables)
            {
                _logger.DebugWithFormat("Registered {0} = {1}", envVar.Name, envVar.Value);
            }
            environmentVariables.RegisterVariablesInSystem();
        }

        private static ConfigEnvironmentVariableExtractor CreateDefaultConfigEnvironmentVariableExtractor()
        {
            _logger.Debug("Creating default config environment variable extractor");
            var msdeConfigFinder = new MsdeConfigFinder();
            var msdeVersionConfigurationReader = new MsdeVersionConfigurationReader(msdeConfigFinder);
            var environmentVariableDetailsConfigurationReader = new EnvironmentVariableDetailsConfigurationReader();
            return new ConfigEnvironmentVariableExtractor(msdeVersionConfigurationReader,
                                                          environmentVariableDetailsConfigurationReader);
        }

        //private int m_quitDialogSeconds = -1;
        //private bool m_quitDialogSave;

        //internal void SetQuitDialogAutomaticBehaviour(int seconds, bool save)
        //{
        //    m_quitDialogSeconds = seconds;
        //    m_quitDialogSave = save;
        //}

        public void SetIpcCommunicationStatus(CommunicationChannelStatus status)
        {
            _ipcStatus = status;
        }

        public void SetImcCommunicationStatus(CommunicationChannelStatus status)
        {
            _imcStatus = status;
        }

        public void EnableSoftwareRendering()
        {
            //m_initialSettings.EnableSoftwareRendering = true;
            RenderOptions.ProcessRenderMode = RenderMode.SoftwareOnly;
        }

        public void DisableEnforcedSizeRestrictions()
        {
            _initialSettings.DisableEnforcedSizeRestrictions = true;
        }

        public void EnableNonOwnedWindows()
        {
            _initialSettings.EnableNonOwnedWindows = true;
            FloatingWindowUtil.EnableNonOwnedWindows = true;
        }

        public void SkipSaveProfileOnExitWhen(SkipSaveProfileOnExitCondition onExitCondition_)
        {
            _initialSettings.SkipSaveProfileOnExitCondition = onExitCondition_;
        }

        public void SetApplicationName(string name_)
        {
            Application.Name = name_;
            ConcordConfigurationManager.ApplicationName = name_;
            var concordStorage = _persistenceStorage as ConcordPersistenceStorage;
            if (concordStorage != null)
            {
                concordStorage.ApplicationName = name_;
            }
        }

        public void SetApplicationVersion(string version_)
        {
            Application.Version = version_;
            ConcordConfigurationManager.ApplicationVersion = version_;
            var concordStorage = _persistenceStorage as ConcordPersistenceStorage;
            if (concordStorage != null)
            {
                concordStorage.ApplicationVersion = version_;
            }
        }

        private ImageSource ribbonIcon;
        public void SetRibbonIcon(ImageSource icon_)
        {
            ribbonIcon = icon_;
        }
        internal event EventHandler<ModuleInitializeArgs> BeforeModuleInitialized;

        internal event EventHandler<ModuleInitializeArgs> AfterModuleInitialized;

        internal event EventHandler<ContainerEventArgs> BeforeInitializeModules;

        internal event EventHandler<ContainerEventArgs> AfterPersistenceStorageInitialized;
        private void OnAfterPersistenceStorageInitialized(ContainerEventArgs e_)
        {
            var handler = AfterPersistenceStorageInitialized;
            if (handler != null) handler(this, e_);

        }
        private void OnBeforeInitializeModules(ContainerEventArgs e_)
        {
            var handler = BeforeInitializeModules;
            if (handler != null) handler(this, e_);
        }

        private readonly ModuleLoadInfos moduleLoadInfos = new ModuleLoadInfos();
        public bool RegisterJsonFormatter;

        protected override void OnBeforeModuleInitialized(object sender, EventArgs<string> e)
        {
            base.OnBeforeModuleInitialized(sender, e);
            BeforeModuleInitializedCore(sender, e, false);
        }

        protected void OnBeforeModuleInitializedConcord(object sender, EventArgs<string> e)
        {
            base.OnBeforeModuleInitialized(sender, e);
            BeforeModuleInitializedCore(sender, e, true);
        }

        private void BeforeModuleInitializedCore(object sender, EventArgs<string> e, bool isConcordApplet)
        {
            IModuleLoadInfo info = moduleLoadInfos.GetModuleLoadInfo(e.Value1);
            if (info != null)
            {
                if (isConcordApplet)
                {
                    info.Type = ModuleType.ConcordApplet;

                    if (!this._initialSettings.ConcordEnabled)
                    {
                        Type moduleType = Type.GetType(e.Value1);
                        if (moduleType != null)
                        {
                            Type typecopy = moduleType;
                            while (typecopy != null)
                            {
                                if (typecopy.Name == "ConcordAppletBase")
                                {
                                    info.Error = ModuleFrameworkHandler.GetModuleInitializeException(String.Format(CultureInfo.CurrentCulture,
                                          "Module {0} is not valid when Concord is not enabled.",
                                          e.Value1));
                                    throw info.Error;

                                }
                                typecopy = typecopy.BaseType;
                            }

                        }
                    }
                }

                moduleLoadInfos.StartLoading(info);
            }

            try
            {
                string modulename = e.Value1;
                modulename = modulename.Substring(0, modulename.IndexOf(","));
                modulename = modulename.Substring(modulename.LastIndexOf(".") + 1);
                (_chromeManager.DefaultChromeArea[ChromeManagerBase.STATUSBAR_COMMANDAREA_NAME] as IStatusBar).MainItem.UpdateStatus(StatusLevel.Information, String.Format("Loading module \'{0}\'", modulename));
            }
            catch
            {
            }
            _logger.InfoWithFormat("Module {0} is being initialized", e.Value1);
            var copy = BeforeModuleInitialized;
            if (copy != null)
            {
                copy(sender, new ModuleInitializeArgs(e.Value1));
            }
        }

        protected override void OnAfterModuleInitialized(object sender, EventArgs<string> e)
        {
            base.OnAfterModuleInitialized(sender, e);
            (_chromeManager.DefaultChromeArea[ChromeManagerBase.STATUSBAR_COMMANDAREA_NAME] as IStatusBar).MainItem.ClearStatus();
            IModuleLoadInfo info = moduleLoadInfos.GetModuleLoadInfo(e.Value1);
            if (info != null)
            {
                moduleLoadInfos.EndLoading(info);

            }

            _logger.InfoWithFormat("Module {0} has been initialized [{1} ticks]", e.Value1, info != null ? info.LoadingTime.ToString() : "unknown");
            _logger.InfoWithFormat("ModuleLoad, {0}, Module, {1}", e.Value1, info != null ? info.LoadingTime.ToString() : "unknown");
            var copy = AfterModuleInitialized;
            if (copy != null)
            {
                copy(sender, new ModuleInitializeArgs(e.Value1));
            }
        }

        private void OnAfterModuleInitializedConcord(object sender, EventArgs<string> e)
        {
            base.OnAfterModuleInitialized(sender, e);
            IModuleLoadInfo info = moduleLoadInfos.GetModuleLoadInfo(e.Value1);
            if (info != null)
            {
                moduleLoadInfos.EndLoading(info);
            }
            _logger.InfoWithFormat("Module {0} has been initialized [{1} ticks]", e.Value1, info != null ? info.LoadingTime.ToString() : "unknown");
            _logger.InfoWithFormat("ModuleLoad, {0}, ConcordApplet, {1}", e.Value1, info != null ? info.LoadingTime.ToString() : "unknown");
            var copy = AfterModuleInitialized;
            if (copy != null)
            {
                copy(sender, new ModuleInitializeArgs(e.Value1));
            }
        }


        protected override void InitializeModuleHandler()
        {
            if (UsePrismAsInfrasturcture)
            {
                ModuleFrameworkHandler = ModuleFrameworkFactory.CreatePrismHandler(Container, IsUseReflection,
                                                                                   _configFile);
            }
            else
            {
                ModuleFrameworkHandler = ModuleFrameworkFactory.CreateCompositeHandler(Container, IsUseReflection,
                                                                                       _configFile);
            }
        }

        public void AddModule(Type moduleType)
        {
            ModuleFrameworkHandler.AddModule(moduleType);
        }

        public void AddModule(Type moduleType, string moduleName)
        {
            ModuleFrameworkHandler.AddModule(moduleType, moduleName);
        }

        public void AddPreInitializer(IPreInitializer preInitializer)
        {
            ModuleFrameworkHandler.AddPreInitializer(preInitializer);
        }

        public void AddModule(object moduleInfo)
        {
            ModuleFrameworkHandler.AddModule(moduleInfo);
        }


        public bool HasModule(Type moduleType)
        {
            return ModuleFrameworkHandler.HasModule(moduleType);
        }

        private void OnConfigurationDependentThemeDetermined(EventArgs<string> e_)
        {
            var handler = ConfigurationDependentThemeDetermined;
            if (handler != null) handler(this, e_);
        }
    }
}
