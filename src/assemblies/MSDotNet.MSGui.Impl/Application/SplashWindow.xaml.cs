﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Application/SplashWindow.xaml.cs#27 $
// $Change: 898267 $
// $DateTime: 2014/09/25 05:16:06 $
// $Author: caijin $

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Timers;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.TestSupport;
using Point = System.Windows.Point;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
	/// <summary>
	/// Interaction logic for SplashWindow.xaml
	/// </summary>
	public partial class SplashWindow : Window
	{

		public static readonly DependencyProperty ToolTipTextProperty = DependencyProperty.Register(
		"ToolTipText",
		typeof(string),
		typeof(SplashWindow),
		new UIPropertyMetadata(String.Empty));

		public static readonly DependencyProperty VersionTextProperty = DependencyProperty.Register(
			 "VersionText",
			 typeof(string),
			 typeof(SplashWindow),
			 new UIPropertyMetadata(String.Empty));

		public static readonly DependencyProperty ProgressTextProperty = DependencyProperty.Register(
			 "ProgressText",
			 typeof(string),
			 typeof(SplashWindow),
			 new UIPropertyMetadata("MSDesktop Launching"));

		public static readonly DependencyProperty BackgroundImageProperty = DependencyProperty.Register(
				"BackgroundImage",
				typeof(string),
				typeof(SplashWindow),
				new UIPropertyMetadata(null, BackgroundImageChanged));

		/// <summary>
		/// Gets or sets a value indicating whether this instance is application name shown.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if application name shown; otherwise, <c>false</c>.
		/// </value>
		public bool IsApplicationNameShown
		{
			get { return (bool)GetValue(IsApplicationNameShownProperty); }
			set { SetValue(IsApplicationNameShownProperty, value); }
		}

		public static readonly DependencyProperty IsApplicationNameShownProperty =
				DependencyProperty.Register("IsApplicationNameShown", typeof(bool), typeof(SplashWindow), new UIPropertyMetadata(SplashLauncher<SplashWindow>.IsApplicationNameShown));
        public static readonly DependencyProperty IsTopMostProperty =
    DependencyProperty.Register("IsTopMost", typeof(bool), typeof(SplashWindow), new UIPropertyMetadata(SplashLauncher<SplashWindow>.IsTopMost));

        public bool IsTopMost
        {
            get { return (bool)GetValue(IsTopMostProperty); }
            set { SetValue(IsTopMostProperty, value); }
        }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is autosized.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if autosized; otherwise, <c>false</c>.
		/// </value>
		public bool IsAutosize
		{
			get { return (bool)GetValue(IsAutosizeProperty); }
			set { SetValue(IsAutosizeProperty, value); }
		}

		public static readonly DependencyProperty IsAutosizeProperty =
				DependencyProperty.Register("IsAutosizeProperty", typeof(bool), typeof(SplashWindow), new UIPropertyMetadata(SplashLauncher<SplashWindow>.IsAutosize, BackgroundImageChanged));

		/// <summary>
		/// Gets or sets a value indicating whether this instance is bitmap transparent.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is bitmap transparent; otherwise, <c>false</c>.
		/// </value>
		public bool IsBitmapTransparent
		{
			get { return (bool)GetValue(IsBitmapTransparentProperty); }
			set { SetValue(IsBitmapTransparentProperty, value); }
		}

		public static readonly DependencyProperty IsBitmapTransparentProperty =
				DependencyProperty.Register("IsBitmapTransparent", typeof(bool), typeof(SplashWindow), new UIPropertyMetadata(SplashLauncher<SplashWindow>.IsBitmapTransparent));

		/// <summary>
		/// Gets or sets the color of the version.
		/// </summary>
		/// <value>
		/// The color of the version.
		/// </value>
		public SolidColorBrush VersionColor
		{
			get { return (SolidColorBrush)GetValue(VersionColorProperty); }
			set { SetValue(VersionColorProperty, value); }
		}

		public static readonly DependencyProperty VersionColorProperty =
				DependencyProperty.Register("VersionColor", typeof(SolidColorBrush), typeof(SplashWindow), new UIPropertyMetadata(new SolidColorBrush(SplashLauncher<SplashWindow>.VersionColor)));

		/// <summary>
		/// Gets or sets the color of the status.
		/// </summary>
		/// <value>
		/// The color of the status.
		/// </value>
		public SolidColorBrush StatusColor
		{
			get { return (SolidColorBrush)GetValue(StatusColorProperty); }
			set { SetValue(StatusColorProperty, value); }
		}

		public static readonly DependencyProperty StatusColorProperty =
				DependencyProperty.Register("StatusColor", typeof(SolidColorBrush), typeof(SplashWindow), new UIPropertyMetadata(new SolidColorBrush(SplashLauncher<SplashWindow>.StatusColor)));


		/// <summary>
		/// Gets or sets the color of the name.
		/// </summary>
		/// <value>
		/// The color of the name.
		/// </value>
		public SolidColorBrush NameColor
		{
			get { return (SolidColorBrush)GetValue(NameColorProperty); }
			set { SetValue(NameColorProperty, value); }
		}

		public static readonly DependencyProperty NameColorProperty =
				DependencyProperty.Register("NameColor", typeof(SolidColorBrush), typeof(SplashWindow), new UIPropertyMetadata(new SolidColorBrush(SplashLauncher<SplashWindow>.NameColor)));

		/// <summary>
		/// Gets or sets the status position.
		/// </summary>
		/// <value>
		/// The status position.
		/// </value>
		public Point StatusPosition
		{
			get { return (Point)GetValue(StatusPositionProperty); }
			set { SetValue(StatusPositionProperty, value); }
		}

		public static readonly DependencyProperty StatusPositionProperty =
				DependencyProperty.Register("StatusPosition", typeof(Point), typeof(SplashWindow), new UIPropertyMetadata(SplashLauncher<SplashWindow>.StatusPosition, TextPositionChanged));

		/// <summary>
		/// Gets or sets the name position.
		/// </summary>
		/// <value>
		/// The name position.
		/// </value>
		public Point NamePosition
		{
			get { return (Point)GetValue(NamePositionProperty); }
			set { SetValue(NamePositionProperty, value); }
		}

		public static readonly DependencyProperty NamePositionProperty =
				DependencyProperty.Register("NamePosition", typeof(Point), typeof(SplashWindow), new UIPropertyMetadata(SplashLauncher<SplashWindow>.NamePosition, TextPositionChanged));

		/// <summary>
		/// Gets or sets the version position.
		/// </summary>
		/// <value>
		/// The version position.
		/// </value>
		public Point VersionPosition
		{
			get { return (Point)GetValue(VersionPositionProperty); }
			set { SetValue(VersionPositionProperty, value); }
		}

		public static readonly DependencyProperty VersionPositionProperty =
				DependencyProperty.Register("VersionPosition", typeof(Point), typeof(SplashWindow), new UIPropertyMetadata(SplashLauncher<SplashWindow>.VersionPosition, TextPositionChanged));

		/// <summary>
		/// Gets or sets the status font family.
		/// </summary>
		/// <value>
		/// The status font family.
		/// </value>
		public FontFamily StatusFontFamily
		{
			get { return (FontFamily)GetValue(StatusFontFamilyProperty); }
			set { SetValue(StatusFontFamilyProperty, value); }
		}

		public static readonly DependencyProperty StatusFontFamilyProperty =
				DependencyProperty.Register("StatusFontFamily", typeof(FontFamily), typeof(SplashWindow), new UIPropertyMetadata(null));

		/// <summary>
		/// Gets or sets the name font family.
		/// </summary>
		/// <value>
		/// The name font family.
		/// </value>
		public FontFamily NameFontFamily
		{
			get { return (FontFamily)GetValue(NameFontFamilyProperty); }
			set { SetValue(NameFontFamilyProperty, value); }
		}

		public static readonly DependencyProperty NameFontFamilyProperty =
				DependencyProperty.Register("NameFontFamily", typeof(FontFamily), typeof(SplashWindow), new UIPropertyMetadata(null));

		/// <summary>
		/// Gets or sets the X button position.
		/// </summary>
		/// <value>
		/// The X button position.
		/// </value>
		public Point XButtonPosition
		{
			get { return (Point)GetValue(XButtonPositionProperty); }
			set { SetValue(XButtonPositionProperty, value); }
		}

		public static readonly DependencyProperty XButtonPositionProperty =
				DependencyProperty.Register("XButtonPosition", typeof(Point), typeof(SplashWindow), new UIPropertyMetadata(SplashLauncher<SplashWindow>.XButtonPosition));

		public Size XButtonSize
		{
			get { return (Size)GetValue(XButtonSizeProperty); }
			set { SetValue(XButtonSizeProperty, value); }
		}

		public static readonly DependencyProperty XButtonSizeProperty =
				DependencyProperty.Register("XButtonSize", typeof(Size), typeof(SplashWindow), new UIPropertyMetadata(SplashLauncher<SplashWindow>.XButtonSize));


		/// <summary>
		/// Gets or sets the tool tip text.
		/// </summary>
		/// <value>
		/// The tool tip text.
		/// </value>
		public string ToolTipText
		{
			get { return (string)GetValue(ToolTipTextProperty); }
			set { SetValue(ToolTipTextProperty, value); }
		}

		/// <summary>
		/// Gets or sets the version text.
		/// </summary>
		/// <value>
		/// The version text.
		/// </value>
		public string VersionText
		{
			get { return (string)GetValue(VersionTextProperty); }
			set { SetValue(VersionTextProperty, value); }
		}

		/// <summary>
		/// Gets or sets the progress text.
		/// </summary>
		/// <value>
		/// The progress text.
		/// </value>
		public string ProgressText
		{
			get { return (string)GetValue(ProgressTextProperty); }
			set { SetValue(ProgressTextProperty, value); }
		}

		/// <summary>
		/// Gets or sets the background image.
		/// </summary>
		/// <value>
		/// The background image.
		/// </value>
		public string BackgroundImage
		{
			get { return (string)GetValue(BackgroundImageProperty); }
			set { SetValue(BackgroundImageProperty, value); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether the X button is shown.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if the X button is shown; otherwise, <c>false</c>.
		/// </value>
		public bool IsXButtonShown
		{
			get { return (bool)GetValue(IsXButtonShownProperty); }
			set { SetValue(IsXButtonShownProperty, value); }
		}

		public static readonly DependencyProperty IsXButtonShownProperty = DependencyProperty.Register("IsXButtonShown", typeof(bool), typeof(SplashWindow), new UIPropertyMetadata(SplashLauncher<SplashWindow>.IsXButtonShown));

		private Canvas m_canvas;

		internal static bool EnableDoubleClickDismiss;
		internal static bool EnableMinimizeOnHide;

		/// <summary>
		/// Initializes a new instance of the <see cref="SplashWindow"/> class.
		/// </summary>
		public SplashWindow()
		{
			InitializeComponent();

			m_clickTimer.Elapsed += clickTimer_Tick;

			MouseLeftButtonDown += SplashWindowMouseDown;
			if (EnableDoubleClickDismiss)
				MouseLeftButtonUp += SplashWindowMouseUp;
			this.DataContext = this;

			ToolTipText = "Hide";
			TextPositionChanged(this, new DependencyPropertyChangedEventArgs());
			DependencyPropertyDescriptor dpd = DependencyPropertyDescriptor.FromProperty(WindowStateProperty, typeof(Window));
			if (dpd != null)
			{
				dpd.AddValueChanged(this, ResetTaskBar);
			}
		}

		private void ResetTaskBar(object sender, EventArgs args)
		{
			if (WindowState == WindowState.Normal)
				ShowInTaskbar = false;

		}

		protected override void OnClosing(CancelEventArgs e)
		{
			base.OnClosing(e);
			DependencyPropertyDescriptor dpd = DependencyPropertyDescriptor.FromProperty(WindowStateProperty, typeof(Window));
			dpd.RemoveValueChanged(this, ResetTaskBar);
		}

		/// <summary>
		/// Text position change handler.
		/// Switches to canvas layouting when not all text positions are specified
		/// </summary>
		/// <param name="d_">The splash window.</param>
		/// <param name="e_">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
		private static void TextPositionChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
		{
			var splashWindow = d_ as SplashWindow;
			if (splashWindow == null) return;
			if (splashWindow.StatusPosition == new Point() || splashWindow.VersionPosition == new Point() || (splashWindow.IsApplicationNameShown && splashWindow.NamePosition == new Point()))
			{
				splashWindow.itemsControl.ItemsPanel = splashWindow.Resources["grid"] as ItemsPanelTemplate;
			}
			else
			{
				splashWindow.itemsControl.ItemsPanel = splashWindow.Resources["canvas"] as ItemsPanelTemplate;
                 if (splashWindow.IsApplicationNameShown && splashWindow.NamePosition != new Point())
                 {
                     Canvas.SetLeft(splashWindow.appNameBox, splashWindow.NamePosition.X);
                     Canvas.SetTop(splashWindow.appNameBox, splashWindow.NamePosition.Y);
                 }
			}
		}

		/// <summary>
		/// Background image change handler.
		/// Modifies the splash screen's size to match that of the image
		/// </summary>
		/// <param name="d_">The splash window.</param>
		/// <param name="e_">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
		private static void BackgroundImageChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
		{
			var splashWindow = d_ as SplashWindow;
			if (splashWindow == null || !(splashWindow.Resources["bgImage"] is ImageBrush) || !(((ImageBrush)splashWindow.Resources["bgImage"]).ImageSource is BitmapSource))
				return;
			var bitmap = ((ImageBrush)splashWindow.Resources["bgImage"]).ImageSource as BitmapSource;
			splashWindow.Width = splashWindow._backBorder.Width = splashWindow.BackgroundImage != null && splashWindow.IsAutosize ? bitmap.PixelWidth : 280.0;
			splashWindow.Height = splashWindow._backBorder.Height = splashWindow.BackgroundImage != null && splashWindow.IsAutosize ? bitmap.PixelHeight : 150.0;
		}

		/// <summary>
		/// Sets the status text.
		/// </summary>
		/// <param name="progressText_">The progress text.</param>
		public void SetProgressText(string progressText_)
		{
			this.Dispatcher.BeginInvoke(DispatcherPriority.DataBind, (DispatcherOperationCallback)delegate
			{
				ProgressText = progressText_;
				return null;
			}, null);
		}

		/// <summary>
		/// Mouse down handler.
		/// </summary>
		/// <param name="sender_">The sender_.</param>
		/// <param name="e_">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
		void SplashWindowMouseDown(object sender_, MouseButtonEventArgs e_)
		{
            if (e_.ButtonState == MouseButtonState.Released) return;
			DragMove();
		}

		private bool m_singleClick;

		private System.Timers.Timer m_clickTimer = new Timer() { AutoReset = false };

		internal static ImageSource HideIcon = null;

		private void SplashWindowMouseUp(object sender, MouseButtonEventArgs e)
		{

			if (e.ChangedButton == MouseButton.Left)
			{
				if (!m_singleClick)
				{
					m_singleClick = true;
					m_clickTimer.Interval = System.Windows.Forms.SystemInformation.DoubleClickTime;
					m_clickTimer.Start();
				}
				else
				{
					m_clickTimer.Stop();
					m_singleClick = false;
					OwnHide();
				}
			}
		}

		private void clickTimer_Tick(object sender, EventArgs e)
		{
			if (m_singleClick)
			{
				m_clickTimer.Stop();
				m_singleClick = false;
			}
		}

		private void OwnHide()
		{
			if (EnableMinimizeOnHide)
			{
                Icon = HideIcon;
                ShowInTaskbar = true;
                WindowState = WindowState.Minimized;
			}
			else
			{
				Hide();
			}
		}

		/// <summary>
		/// X button click handler.
		/// </summary>
		/// <param name="sender_">The sender.</param>
		/// <param name="e_">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
		private void XButtonClick(object sender_, RoutedEventArgs e_)
		{
			OwnHide();
		}

		/// <summary>
		/// Returns the specified <paramref name="value_"/> if it is positive.
		/// </summary>
		/// <param name="value_">The value to return.</param>
		/// <returns>The specified <paramref name="value_"/> if it is positive, 0.0 otherwise.</returns>
		private static double Pos(double value_)
		{
			return value_ > 0.0 ? value_ : 0.0;
		}

		/// <summary>
		/// Splash canvas layout handler.
		/// </summary>
		/// <param name="sender_">The sender.</param>
		/// <param name="e_">The <see cref="System.Windows.SizeChangedEventArgs"/> instance containing the event data.</param>
		private void LayoutCanvasItems(object sender_, SizeChangedEventArgs e_)
		{
			var canvas = sender_ as Canvas;
			if (canvas == null)
			{
				if (m_canvas != null)
				{
					canvas = m_canvas;
				}
				else
				{
					return;
				}
			}
			else
			{
				m_canvas = canvas;
			}

			var ctrls = IsApplicationNameShown ? new FrameworkElement[] { _statusText, _versionText, appNameBox } : new[] { _statusText, _versionText };
			var positions = IsApplicationNameShown ? new[] { StatusPosition, VersionPosition, NamePosition } : new[] { StatusPosition, VersionPosition };
			for (var i = 0; i < ctrls.Length; i++)
			{
				if (positions[i].X < 0.0 && positions[i].Y >= 0.0)
				{
					Canvas.SetLeft(ctrls[i], Pos((canvas.ActualWidth - ctrls[i].ActualWidth) / 2));
					Canvas.SetTop(ctrls[i], positions[i].Y);
				}
				else if (positions[i].X >= 0.0 && positions[i].Y < 0.0)
				{
					Canvas.SetLeft(ctrls[i], positions[i].X);
					Canvas.SetTop(ctrls[i], Pos((canvas.ActualHeight - ctrls[i].ActualHeight) / 2));
				}
				else if (positions[i].X < 0.0 && positions[i].Y < 0.0)
				{
					Canvas.SetLeft(ctrls[i], Pos((canvas.ActualWidth - ctrls[i].ActualWidth) / 2));
					Canvas.SetTop(ctrls[i], Pos((canvas.ActualHeight - ctrls[i].ActualHeight) / 2));
				}
				else
				{
					Canvas.SetLeft(ctrls[i], positions[i].X);
					Canvas.SetTop(ctrls[i], positions[i].Y);
				}
			}
        }

        #region For CUIT
        protected override System.Windows.Automation.Peers.AutomationPeer OnCreateAutomationPeer()
        {  
            var inner =  base.OnCreateAutomationPeer();
            return new SplashWindowAutomationPeer(inner, this, new SplashCommandHandler());
        }

        class SplashCommandHandler: ImagePathHandler
        {
            public SplashCommandHandler():base()
            {
                Handlers[MainWindowSupportedCommands.GetAppliationTitle.ToString()] =
                   (paras) =>
                   {
                       var peer = this.AutomationPeer as SplashWindowAutomationPeer;
                       var window = peer.Owner as SplashWindow;
                       return window.Title;
                   };
            }
        }

        class SplashWindowAutomationPeer : InteractiveAutomationPeerWrapper
        {
            public SplashWindowAutomationPeer(AutomationPeer inner, FrameworkElement owner, ImagePathHandler handler):base(inner, owner, handler)
            {
                
            }
        }

        #endregion
    }
}
