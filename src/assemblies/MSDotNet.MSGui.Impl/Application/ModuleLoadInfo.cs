﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ControlParadigm;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
    public class ModuleLoadInfo: IModuleLoadInfo
    {
        private readonly ModuleLoadInfos parent;
        internal ModuleLoadInfo(string typeName_)
        {
            FullTypeName = typeName_;
            Status = ModuleStatus.NotLoaded;
            LoadingTime = -1;
            LoadOrder = -1;
            Type = ModuleType.Normal;
        }

        public string FullTypeName { get; set; }
        public string ModuleName { get; internal set; }
        public int LoadOrder { get; internal set; } 
        internal long LoadStartTime { get; set; }
        public long LoadingTime { get; set; }  
        public Collection<string> DependsOnModuleNames { get;set; }
        public ModuleType Type { get; set; }
        public ModuleStatus Status { get; set; }
        public Exception Error { get; set; }
        public string RejectReason { get; set; }
        public string TypeNotFoundReason { get; set; }
        public bool OutOfProcess { get; set; }
        public bool IsEarlyModule
        {
            get
            {
                if (Status == ModuleStatus.TypeResolutionFailed) return false;
                System.Type type = System.Type.GetType(FullTypeName);
                if (type == null) return false;
                if (type.GetInterface(typeof(IEarlyInitializedFrameworkModule).FullName) == null &&
                     type.GetInterfaces().FirstOrDefault(t_ => t_.Name == "IEarlyInitializedModule") == null)
                {
                    return false;
                }
                return true;
            }
        }
    }
}
