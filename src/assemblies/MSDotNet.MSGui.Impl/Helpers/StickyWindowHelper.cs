﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.BarWithWindow;

namespace MorganStanley.MSDotNet.MSGui.Impl.Helpers
{
    internal class WPFStickyWindow
    {
        #region ResizeDirectionEnum

        [Flags]
        internal enum ResizeDirectionEnum
        {
            None = 0,
            Top = 2,
            Bottom = 4,
            Left = 8,
            Right = 16
        };

        #endregion

        
        #region Declations

        private static readonly List<Window> _stickyWindows=new List<Window>();
        public const string CLASS_NAME = "StickyWindow";

        private static int _stickGap = 20; // distance to stick
        private static int _stickRange = 60; // range of a window to start sticking to it

        private readonly Window _wpfWindow;
        private bool _stickToScreen;
        private bool _stickToOther;
        private bool _stickyEnabled;

        #endregion

        #region Public operations and properties

        /// <summary>
        /// Allow sticking.
        /// </summary>
        internal bool StickyEnabled
        {
            get { return _stickyEnabled; }
            set { _stickyEnabled = value; }
        }

        /// <summary>
        /// Allow sticking to Screen Margins
        /// Default value = false
        /// </summary>
        internal bool StickToScreen
        {
            get { return _stickToScreen; }
            set { _stickToScreen = value; }
        }

        /// <summary>
        /// Allow sticking to other StickWindows
        /// Default value = true
        /// </summary>
        internal bool StickToOther
        {
            get { return _stickToOther; }
            set { _stickToOther = value; }
        }

        /// <summary>
        /// Distance in pixels betwen two forms or a form and the screen where the sticking should start
        /// Default value = 20
        /// </summary>
        protected int StickGap
        {
            get { return _stickGap; }
            set { _stickGap = value; }
        }

        /// <summary>
        /// The range from another window that we need to be before
        /// being infulenced by the other window
        /// </summary>
        protected int StickRange
        {
            get { return _stickRange; }
            set { _stickRange = value; }
        }

        #endregion

        #region StickyWindow Constructor

        /// <summary>
        /// Make the form Sticky
        /// </summary>
        /// <param name="wpfWindow">Form to be made sticky</param>
        public WPFStickyWindow(Window wpfWindow)
        {
            if (_stickyWindows.Contains(wpfWindow)) return;

            _wpfWindow = wpfWindow;

            _stickyEnabled = true;
            _stickToScreen = false;
            _stickToOther = true;

            var windowHandle = (new WindowInteropHelper(wpfWindow)).Handle;
            var hwndSource = HwndSource.FromHwnd(windowHandle);
            hwndSource.AddHook(WndProc);

            _stickyWindows.Add(wpfWindow);

            EventHandler closedHandler = null;
            closedHandler = (sender, args) =>
                {
                    hwndSource.RemoveHook(WndProc);
                    _stickyWindows.Remove(wpfWindow);
                    wpfWindow.Closed -= closedHandler;
                };
            wpfWindow.Closed += closedHandler;
        }

        #endregion

        #region WndProc

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (StickyEnabled && (Keyboard.Modifiers & ModifierKeys.Alt) != ModifierKeys.Alt)
            {
                switch ((uint) msg)
                {
                    case Win32.WM_SIZING:
                        // modify the resize bounds
                        WmSizing(wParam, lParam); 
                        break;
                    case Win32.WM_WINDOWPOSCHANGING:
                        // Moves or resizes by SetWindowPos API (Infragistics DockManager would do that)
                        // Currently, only moves are handled
                        WmWindowPosChanging(hwnd, lParam);
                        break;
                    case Win32.WM_MOVING:
                        // Moves by the user
                        WmMoving(lParam);
                        break;
                }
            }

            handled = false;
            return hwnd;
        }

        #endregion

        #region Resize calculations

        private void WmSizing(IntPtr wParam, IntPtr lParam)
        {
            // get the RECT structure to modify
            int p = wParam.ToInt32();
            var r = (Win32.RECT)Marshal.PtrToStructure(lParam, typeof(Win32.RECT));

            // initialise variables
            var resizeDirection = ResizeDirectionEnum.None;
            Rectangle originalBounds;
            int height = r.GetHeight();
            int width = r.GetLength();
            switch (p)
            {
                case Win32.WMSZ.WMSZ_BOTTOM:
                    if (_wpfWindow.MinHeight < height &&
                        _wpfWindow.MaxHeight > height)
                    {
                       resizeDirection = ResizeDirectionEnum.Bottom;
                    }
                    break;
                case Win32.WMSZ.WMSZ_BOTTOMLEFT:
                    resizeDirection = ResizeDirectionEnum.Bottom | ResizeDirectionEnum.Left;
                    if (_wpfWindow.MinHeight >= height ||
                        _wpfWindow.MaxHeight <= height)
                    {
                        resizeDirection -= ResizeDirectionEnum.Bottom;
                    }
                    if (_wpfWindow.MinWidth >= width ||
                        _wpfWindow.MaxHeight <= height)
                    {
                        resizeDirection -= ResizeDirectionEnum.Left;
                    } 
                    break;
                case Win32.WMSZ.WMSZ_BOTTOMRIGHT:
                    resizeDirection = ResizeDirectionEnum.Bottom | ResizeDirectionEnum.Right;
                    if (_wpfWindow.MinHeight >= height ||
                        _wpfWindow.MaxHeight <= height)
                    {
                        resizeDirection -= ResizeDirectionEnum.Bottom;
                    }
                    if (_wpfWindow.MinWidth >= width ||
                        _wpfWindow.MaxHeight <= height)
                    {
                        resizeDirection -= ResizeDirectionEnum.Right;
                    } 
                    break;
                case Win32.WMSZ.WMSZ_LEFT:
                    if (_wpfWindow.MinWidth < width &&
                        _wpfWindow.MaxWidth > width)
                    {
                        resizeDirection = ResizeDirectionEnum.Left;
                    } 
                    break;
                case Win32.WMSZ.WMSZ_RIGHT:
                    if (_wpfWindow.MinWidth < width &&
                        _wpfWindow.MaxWidth > width)
                    {
                        resizeDirection = ResizeDirectionEnum.Right;
                    } 
                    break;
                case Win32.WMSZ.WMSZ_TOP:
                    if (_wpfWindow.MinHeight < height &&
                        _wpfWindow.MaxHeight > height)
                    {
                        resizeDirection = ResizeDirectionEnum.Top;
                    }
                    break;
                case Win32.WMSZ.WMSZ_TOPLEFT:
                    resizeDirection = ResizeDirectionEnum.Top | ResizeDirectionEnum.Left;
                    if (_wpfWindow.MinHeight >= height ||
                        _wpfWindow.MaxHeight <= height)
                    {
                        resizeDirection -= ResizeDirectionEnum.Top;
                    }
                    if (_wpfWindow.MinWidth >= width ||
                        _wpfWindow.MaxHeight <= height)
                    {
                        resizeDirection -= ResizeDirectionEnum.Left;
                    } 
                    break;
                case Win32.WMSZ.WMSZ_TOPRIGHT:
                    resizeDirection = ResizeDirectionEnum.Top | ResizeDirectionEnum.Right;
                    if (_wpfWindow.MinHeight >= height ||
                        _wpfWindow.MaxHeight <= height)
                    {
                        resizeDirection -= ResizeDirectionEnum.Top;
                    }
                    if (_wpfWindow.MinWidth >= width ||
                        _wpfWindow.MaxHeight <= height)
                    {
                        resizeDirection -= ResizeDirectionEnum.Right;
                    } 

                    break;
                default:
                    return; 
            }

            switch (resizeDirection)
            {
                case ResizeDirectionEnum.Bottom:
                    originalBounds = Rectangle.FromLTRB(r.Left, r.Top, r.Right,
                                                        Control.MousePosition.Y);
                    break;
                case ResizeDirectionEnum.Bottom | ResizeDirectionEnum.Left:  
                    originalBounds = Rectangle.FromLTRB(Control.MousePosition.X, r.Top, r.Right,
                                                        Control.MousePosition.Y);
                    break;
                case ResizeDirectionEnum.Bottom | ResizeDirectionEnum.Right: 
                    originalBounds = Rectangle.FromLTRB(r.Left, r.Top, Control.MousePosition.X,
                                                        Control.MousePosition.Y);
                    break;
                case ResizeDirectionEnum.Left: 
                    originalBounds = Rectangle.FromLTRB(Control.MousePosition.X, r.Top, r.Right,
                                                        r.Bottom);
                    break;
                case ResizeDirectionEnum.Right: 
                    originalBounds = Rectangle.FromLTRB(r.Left, r.Top, Control.MousePosition.X,
                                                        r.Bottom);
                    break;
                case ResizeDirectionEnum.Top: 
                    originalBounds = Rectangle.FromLTRB(r.Left, Control.MousePosition.Y, r.Right,
                                                        r.Bottom);
                    break;
                case ResizeDirectionEnum.Top | ResizeDirectionEnum.Left: 
                    originalBounds = Rectangle.FromLTRB(Control.MousePosition.X,
                                                        Control.MousePosition.Y, r.Right, r.Bottom);
                    break;
                case ResizeDirectionEnum.Top | ResizeDirectionEnum.Right: 
                    originalBounds = Rectangle.FromLTRB(r.Left, Control.MousePosition.Y,
                                                        Control.MousePosition.X, r.Bottom);
                    break;
                default:
                    return;
            }
            var diffs = new[] {int.MaxValue, int.MaxValue, int.MaxValue, int.MaxValue};

            if (_stickToOther)
            {
                // Getthe sphere of influence for the resizing form - other forms
                // outside this range will have no effect on this form
                var bounds = new Rectangle(originalBounds.Location, originalBounds.Size);
                bounds.Inflate(_stickRange, _stickRange);
                // now try to stick to other forms
                foreach (var sw in GetGlobalStickyWindows())
                {
                    if (sw != _wpfWindow)
                    {
                        Rectangle formBounds = GetScreenBounds(sw);
                        if (formBounds.IntersectsWith(bounds))
                        {
                            Resize_Stick(formBounds, resizeDirection, ref r, diffs, originalBounds);
                        }
                    }
                }
            }
            if (StickToScreen)
            {
                Resize_Stick(GetScreenBounds(), resizeDirection, ref r, diffs, originalBounds);
            }
            // reset any unchanged edges
            r.Top = diffs[0] == int.MaxValue ? originalBounds.Top : r.Top;
            r.Bottom = diffs[1] == int.MaxValue ? originalBounds.Bottom : r.Bottom;
            r.Left = diffs[2] == int.MaxValue ? originalBounds.Left : r.Left;
            r.Right = diffs[3] == int.MaxValue ? originalBounds.Right : r.Right;

            //check that the minimum sizes are honoured.
            if ((resizeDirection & ResizeDirectionEnum.Top) == ResizeDirectionEnum.Top)
            {
                if (r.Bottom - r.Top < _wpfWindow.MinHeight)
                {
                    r.Top = r.Bottom - (int) _wpfWindow.MinHeight;
                }
            }
            if ((resizeDirection & ResizeDirectionEnum.Bottom) == ResizeDirectionEnum.Bottom)
            {
                if (r.Bottom - r.Top < _wpfWindow.MinHeight)
                {
                    r.Bottom = r.Top + (int) _wpfWindow.MinHeight;
                }
            }
            if ((resizeDirection & ResizeDirectionEnum.Left) == ResizeDirectionEnum.Left)
            {
                if (r.Right - r.Left < _wpfWindow.MinWidth)
                {
                    r.Left = r.Right - (int) _wpfWindow.MinWidth;
                }
            }
            if ((resizeDirection & ResizeDirectionEnum.Right) == ResizeDirectionEnum.Right)
            {
                if (r.Right - r.Left < _wpfWindow.MinWidth)
                {
                    r.Right = r.Left + (int) _wpfWindow.MinWidth;
                }
            }

            // modify the message
            Marshal.StructureToPtr(r, lParam, true);
        }

        /// <summary>
        /// Actually modify the bounds if the given rectangle is within range
        /// </summary>
        /// <param name="toRect"></param>
        /// <param name="resizeDirection"></param>
        /// <param name="rect"></param>
        /// <param name="diffs"></param>
        /// <param name="originalBounds"></param>
        private void Resize_Stick(Rectangle toRect, ResizeDirectionEnum resizeDirection, ref Win32.RECT rect,
                                  IList<int> diffs, Rectangle originalBounds)
        {
            if ((resizeDirection & ResizeDirectionEnum.Top) == ResizeDirectionEnum.Top)
            {
                if (Math.Abs(originalBounds.Top - toRect.Top) < Math.Min(StickGap, diffs[0]))
                {
                    diffs[0] = Math.Abs(originalBounds.Top - toRect.Top);
                    rect.Top = toRect.Top;
                }
                else if (Math.Abs(originalBounds.Top - toRect.Bottom) < Math.Min(StickGap, diffs[0]))
                {
                    diffs[0] = Math.Abs(originalBounds.Top - toRect.Bottom);
                    rect.Top = toRect.Bottom;
                }
            }

            if ((resizeDirection & ResizeDirectionEnum.Bottom) == ResizeDirectionEnum.Bottom)
            {
                if (Math.Abs(originalBounds.Bottom - toRect.Top) < Math.Min(StickGap, diffs[1]))
                {
                    diffs[1] = Math.Abs(originalBounds.Bottom - toRect.Top);
                    rect.Bottom = toRect.Top;
                }
                else if (Math.Abs(originalBounds.Bottom - toRect.Bottom) < Math.Min(StickGap, diffs[1]))
                {
                    diffs[1] = Math.Abs(originalBounds.Bottom - toRect.Bottom);
                    rect.Bottom = toRect.Bottom;
                }
            }

            if ((resizeDirection & ResizeDirectionEnum.Left) == ResizeDirectionEnum.Left)
            {
                if (Math.Abs(originalBounds.Left - toRect.Left) < Math.Min(StickGap, diffs[2]))
                {
                    diffs[2] = Math.Abs(originalBounds.Left - toRect.Left);
                    rect.Left = toRect.Left;
                }
                else if (Math.Abs(originalBounds.Left - toRect.Right) < Math.Min(StickGap, diffs[2]))
                {
                    diffs[2] = Math.Abs(originalBounds.Left - toRect.Right);
                    rect.Left = toRect.Right;
                }
            }

            if ((resizeDirection & ResizeDirectionEnum.Right) == ResizeDirectionEnum.Right)
            {
                if (Math.Abs(originalBounds.Right - toRect.Left) < Math.Min(StickGap, diffs[3]))
                {
                    diffs[3] = Math.Abs(originalBounds.Right - toRect.Left);
                    rect.Right = toRect.Left;
                }
                else if (Math.Abs(originalBounds.Right - toRect.Right) < Math.Min(StickGap, diffs[3]))
                {
                    diffs[3] = Math.Abs(originalBounds.Right - toRect.Right);
                    rect.Right = toRect.Right;
                }
            }
        }

        #endregion

        #region Move Computing

        private bool IsMouseButtonReleased()
        {
            // Reset the mouseOffset if we are no longer moving the window
            if (Mouse.LeftButton == MouseButtonState.Released &&
                Mouse.MiddleButton == MouseButtonState.Released &&
                Mouse.RightButton == MouseButtonState.Released &&
                Mouse.XButton1 == MouseButtonState.Released &&
                Mouse.XButton2 == MouseButtonState.Released)
            {
                return true;
            }

            return false;
        }

        private void WmWindowPosChanging(IntPtr hwnd, IntPtr lParam)
        {
            var windowPos = (Win32.WINDOWPOS)Marshal.PtrToStructure(lParam, typeof(Win32.WINDOWPOS));

            if (IsMouseButtonReleased()
                && (windowPos.flags & Win32.SetWindowsPosFlags.SWP_NOMOVE) != 0)
            {
                return;
            }

            if ((windowPos.flags & Win32.SetWindowsPosFlags.SWP_NOMOVE) == 0)
            {
                // We actually got a window moving
                var currentRect = new Win32.RECT();
                Win32.GetWindowRect(hwnd, ref currentRect);

                var resizeOperation = (windowPos.flags & Win32.SetWindowsPosFlags.SWP_NOSIZE) == 0;

                var proposedRect = new Win32.RECT(
                    windowPos.x,
                    windowPos.y,
                    windowPos.x + (resizeOperation ? windowPos.cx : currentRect.GetLength()),
                    windowPos.y + (resizeOperation ? windowPos.cy : currentRect.GetHeight()));

                if (!resizeOperation)
                {
                    var newRect = HandleWindowMoving(proposedRect);

                    windowPos.x = newRect.Left;
                    windowPos.y = newRect.Top;

                    Marshal.StructureToPtr(windowPos, lParam, true);
                }
            }
        }


        private Win32.RECT HandleWindowMoving(Win32.RECT proposedRect)
        {
            proposedRect = AdjustToMouseCursor(proposedRect);

            // where the bounds would be if we hadn't modified them
            var unmodifiedBounds = new Rectangle(
                proposedRect.Left,
                proposedRect.Top,
                proposedRect.Right - proposedRect.Left,
                proposedRect.Bottom - proposedRect.Top);

            var diffs = new[] {int.MaxValue, int.MaxValue};
            if (_stickToOther)
            {
                var bounds = new Rectangle(unmodifiedBounds.Location, unmodifiedBounds.Size);
                bounds.Inflate(_stickRange, _stickRange);

                // now try to stick to other forms
                foreach (var sw in GetGlobalStickyWindows().Where(sw => !Equals(sw, _wpfWindow)))
                {
                    var formBounds = GetScreenBounds(sw);
                    if (formBounds.IntersectsWith(bounds))
                    {
                        Move_Stick(GetScreenBounds(sw), ref proposedRect, unmodifiedBounds, diffs);
                    }
                }
            }

            if (_stickToScreen)
            {
                Move_Stick(GetScreenBounds(), ref proposedRect, unmodifiedBounds, diffs);
            }
            if (diffs[0] == int.MaxValue)
            {
                proposedRect.Top = unmodifiedBounds.Top;
                proposedRect.Bottom = unmodifiedBounds.Bottom;
            }
            if (diffs[1] == int.MaxValue)
            {
                proposedRect.Left = unmodifiedBounds.Left;
                proposedRect.Right = unmodifiedBounds.Right;
            }

            return proposedRect;
        }

        private Win32.RECT AdjustToMouseCursor(Win32.RECT proposedRect)
        {
            Win32.POINT cursor;
            Win32.GetCursorPos(out cursor);
            var handleBarArea = new Rectangle(
                proposedRect.Left,
                proposedRect.Top,
                proposedRect.Right - proposedRect.Left,
                20);
            if (!IsMouseButtonReleased() && !handleBarArea.Contains(cursor.X, cursor.Y))
            {
                var height = proposedRect.Bottom - proposedRect.Top;
                var width = proposedRect.Right - proposedRect.Left;
                if (cursor.Y < handleBarArea.Top)
                {
                    proposedRect.Top = cursor.Y;
                    proposedRect.Bottom = cursor.Y + height;
                }
                if (handleBarArea.Bottom < cursor.Y)
                {
                    proposedRect.Top = cursor.Y + 20;
                    proposedRect.Bottom = cursor.Y + 20 + height;
                }
                if (cursor.X < handleBarArea.Left)
                {
                    proposedRect.Left = cursor.X;
                    proposedRect.Right = cursor.X + width;
                }
                if (handleBarArea.Right < cursor.X)
                {
                    proposedRect.Right = cursor.X;
                    proposedRect.Left = cursor.X - width;
                }
            }
            return proposedRect;
        }

        private void WmMoving(IntPtr lParam)
        {
            if (IsMouseButtonReleased())
            {
                return;
            }

            var r = (Win32.RECT) Marshal.PtrToStructure(lParam, typeof (Win32.RECT));
            var newPosition = HandleWindowMoving(r);
            
            Marshal.StructureToPtr(newPosition, lParam, true);
        }

        /// <summary>
        /// Actually modify the move rectangle if the given rectangle is within range. 
        /// </summary>
        private void Move_Stick(Rectangle toRect, ref Win32.RECT rect, Rectangle unmodBounds, int[] diffs)
        {
            // compare distance from toRect to formRect
            int nearNearGap = Math.Abs(unmodBounds.Top - toRect.Top);
            int nearFarGap = Math.Abs(unmodBounds.Top - toRect.Bottom);
            int farNearGap = Math.Abs(unmodBounds.Bottom - toRect.Top);
            int farFarGap = Math.Abs(unmodBounds.Bottom - toRect.Bottom);
            if (nearNearGap < Math.Min(StickGap, diffs[0])
                || nearFarGap < Math.Min(StickGap, diffs[0])
                || farNearGap < Math.Min(StickGap, diffs[0])
                || farFarGap < Math.Min(StickGap, diffs[0]))
            {
                int height = rect.Bottom - rect.Top;
                int smallestGap = Math.Min(nearNearGap, Math.Min(nearFarGap, Math.Min(farNearGap, farFarGap)));
                diffs[0] = smallestGap; // new smallest gap

                if (nearNearGap == smallestGap)
                {
                    rect.Top = toRect.Top;
                    rect.Bottom = rect.Top + height;
                }
                else if (nearFarGap == smallestGap)
                {
                    rect.Top = toRect.Bottom;
                    rect.Bottom = rect.Top + height;
                }
                else if (farNearGap == smallestGap)
                {
                    rect.Bottom = toRect.Top;
                    rect.Top = rect.Bottom - height;
                }
                else if (farFarGap == smallestGap)
                {
                    rect.Bottom = toRect.Bottom;
                    rect.Top = rect.Bottom - height;
                }
            }

            nearNearGap = Math.Abs(unmodBounds.Left - toRect.Left);
            nearFarGap = Math.Abs(unmodBounds.Left - toRect.Right);
            farNearGap = Math.Abs(unmodBounds.Right - toRect.Left);
            farFarGap = Math.Abs(unmodBounds.Right - toRect.Right);
            if (nearNearGap < Math.Min(StickGap, diffs[1])
                || nearFarGap < Math.Min(StickGap, diffs[1])
                || farNearGap < Math.Min(StickGap, diffs[1])
                || farFarGap < Math.Min(StickGap, diffs[1]))
            {
                int width = rect.Right - rect.Left;
                int smallestGap = Math.Min(nearNearGap, Math.Min(nearFarGap, Math.Min(farNearGap, farFarGap)));
                diffs[1] = smallestGap; // new smallest gap
                if (nearNearGap == smallestGap)
                {
                    rect.Left = toRect.Left;
                    rect.Right = rect.Left + width;
                }
                else if (nearFarGap == smallestGap)
                {
                    rect.Left = toRect.Right;
                    rect.Right = rect.Left + width;
                }
                else if (farNearGap == smallestGap)
                {
                    rect.Right = toRect.Left;
                    rect.Left = rect.Right - width;
                }
                else if (farFarGap == smallestGap)
                {
                    rect.Right = toRect.Right;
                    rect.Left = rect.Right - width;
                }
            }
        }

        #endregion

        #region private helper methods

        private Rectangle GetScreenBounds()
        {
            var screen = Screen.FromHandle(new WindowInteropHelper(this._wpfWindow).Handle);
            return screen.WorkingArea;
        }

        private static Rectangle GetWindowRectangle(Window parentWindow)
        {
            return new Rectangle(
                (int) parentWindow.Left,
                (int) parentWindow.Top,
                (int) parentWindow.Width,
                (int) parentWindow.Height);
        }

        /// <summary>
        /// Gets the screen coordinates bounds for the form
        /// </summary>
        /// <param name="window"></param>
        /// <returns></returns>
        private static Rectangle GetScreenBounds(Window window)
        {
            var rect = new Rectangle();

            window.Dispatcher.Invoke((Action) (() =>
                {
                    var targetWindow = window.Parent == null ? window : (Window) window.Parent;
                    rect = GetWindowRectangle(targetWindow);
                }));

            return rect;
        }

        #endregion

        private readonly IDictionary<Visual, Window> _globalWindows = new ConcurrentDictionary<Visual, Window>();


        public List<Window> GetGlobalStickyWindows()
        {
            var globalStickyWindows = new List<Window>();

            globalStickyWindows.AddRange(_globalWindows.Values);
            globalStickyWindows.AddRange(_stickyWindows);

            // Now, go over all windows, see if something isn't yet in our dictionary, and cache it.
            foreach (var hwndSource in PresentationSource.CurrentSources
                                                         .Cast<HwndSource>())
            {
                var visual = hwndSource.RootVisual;
                if (visual != null && !_globalWindows.ContainsKey(visual))
                {
                    var w = GetVisualWindow(visual); 
                    if (w == System.Windows.Application.Current.MainWindow)
                    {
                        w.Closed += window_Closed;
                        _globalWindows.Add(visual, w);
                        break;
                    } 
                }
            }
                
            return globalStickyWindows.Where(w=>w!=null).ToList();
        }

        private void window_Closed(object sender, EventArgs e)
        {
            // Remove the window from the dictionary
            var senderWindow = sender as Window;
            if (senderWindow != null)
            {
                senderWindow.Closed -= window_Closed;
                var visual = _globalWindows
                    .Where(kvp => Equals(kvp.Value, senderWindow))
                    .Select(kvp => kvp.Key)
                    .FirstOrDefault();

                if (visual != null)
                {
                    _globalWindows.Remove(visual);
                }
            }
        }

        private Window GetVisualWindow(Visual visual)
        {
            Window window = null;
            var visualDispatcher = visual.Dispatcher;

            if(visualDispatcher.Thread.Equals(Thread.CurrentThread))
            {
                window = Window.GetWindow(visual);
            }
            else
            {
                visualDispatcher.Invoke((Action) (() =>
                    {
                        window = Window.GetWindow(visual);
                    }));
            }

            return window;
        }
    }

}
