﻿using System.Reflection;

namespace MorganStanley.MSDotNet.MSGui.Impl.Helpers
{
    public interface IEntryAssemblyProvider
    {
        Assembly GetEntryAssembly();
    }

    internal class EntryAssemblyProvider : IEntryAssemblyProvider
    {
        public Assembly GetEntryAssembly()
        {
            return Assembly.GetEntryAssembly();
        }
    }

    public static class EntryAssemblyHelper
    {
        internal static IEntryAssemblyProvider EntryAssemblyProvider = new EntryAssemblyProvider();
        
        public static Assembly GetEntryAssembly()
        {
            return EntryAssemblyProvider.GetEntryAssembly();
        }
    }
}
