﻿#pragma checksum "..\..\..\ChromeConfig\GesturePicker.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "EFBDAB1AADF9D8C76FD2A3DB914A20AC"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MorganStanley.MSDotNet.MSGui.Impl.ChromeConfig;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeConfig {
    
    
    /// <summary>
    /// GesturePicker
    /// </summary>
    public partial class GesturePicker : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 25 "..\..\..\ChromeConfig\GesturePicker.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox EnabledCheckbox;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/MSDotNet.MSGui.Impl;component/chromeconfig/gesturepicker.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\ChromeConfig\GesturePicker.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.EnabledCheckbox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 24 "..\..\..\ChromeConfig\GesturePicker.xaml"
            this.EnabledCheckbox.Checked += new System.Windows.RoutedEventHandler(this.EnabledChecked);
            
            #line default
            #line hidden
            
            #line 24 "..\..\..\ChromeConfig\GesturePicker.xaml"
            this.EnabledCheckbox.Unchecked += new System.Windows.RoutedEventHandler(this.EnableUnchecked);
            
            #line default
            #line hidden
            return;
            case 2:
            
            #line 27 "..\..\..\ChromeConfig\GesturePicker.xaml"
            ((System.Windows.Controls.ComboBox)(target)).SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ModifierKeySelectionChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            
            #line 30 "..\..\..\ChromeConfig\GesturePicker.xaml"
            ((System.Windows.Controls.ComboBox)(target)).SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.KeySelectionChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

