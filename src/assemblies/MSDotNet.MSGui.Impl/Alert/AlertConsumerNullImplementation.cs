﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Alert/AlertConsumerNullImplementation.cs#8 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using AlertClass = MorganStanley.MSDotNet.MSGui.Core.Alert;

namespace MorganStanley.MSDotNet.MSGui.Impl.Alert
{
    internal class AlertConsumerNullImplementation : IAlertConsumer
    {
        #region IAlertConsumerInternal Members

        public void Consume(AlertClass alert)
        {
            // Do nothing.
        }

        public void Initialize(IUnityContainer unityContainer)
        {
            // Do nothing.
        }

        #endregion
    }
}