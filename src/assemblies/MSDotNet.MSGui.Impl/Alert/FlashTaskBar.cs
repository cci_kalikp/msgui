﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using System.Windows.Threading;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MorganStanley.MSDotNet.MSGui.Impl.Alert
{
  internal static class FlashTaskBar
  {
    /// <summary>
    /// Stop flashing. The system restores the window to its original stae.
    /// </summary>
    public const uint FLASHW_STOP = 0;

    /// <summary>
    /// Flash the window caption.
    /// </summary>
    public const uint FLASHW_CAPTION = 1;

    /// <summary>
    /// Flash the taskbar button.
    /// </summary>
    public const uint FLASHW_TRAY = 2;

    /// <summary>
    /// Flash both the window caption and taskbar button.
    /// This is equivalent to setting the FLASHW_CAPTION | FLASHW_TRAY flags.
    /// </summary>
    public const uint FLASHW_ALL = 3;

    /// <summary>
    /// Flash continuously, until the FLASHW_STOP flag is set.
    /// </summary>
    public const uint FLASHW_TIMER = 4;

    /// <summary>
    /// Flash continuously until the window comes to the foreground.
    /// </summary>
    public const uint FLASHW_TIMERNOFG = 12;

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool FlashWindowEx(ref FLASHWINFO pwfi);

    /// <summary>
    /// Flash the specified Window (Form) until it recieves focus.
    /// </summary>
    /// <param name="form">The Form (Window) to Flash.</param>
    /// <returns></returns>
    public static bool Flash(Form form)
    {
      // Make sure we're running under Windows 2000 or later

      FLASHWINFO fi = Create_FLASHWINFO(form.Handle, FLASHW_ALL | FLASHW_TIMERNOFG, uint.MaxValue, 0);
      return FlashWindowEx(ref fi);
    }

    private static FLASHWINFO Create_FLASHWINFO(IntPtr handle, uint flags, uint
                                                                             count, uint timeout)
    {
      var fi = new FLASHWINFO();
      fi.cbSize = Convert.ToUInt32(Marshal.SizeOf(fi));
      fi.hwnd = handle;
      fi.dwFlags = flags;
      fi.uCount = count;
      fi.dwTimeout = timeout;
      return fi;
    }

    /// <summary>
    /// Flash the specified Window (form) for the specified number of times
    /// </summary>
    /// <param name="form">The Form (Window) to Flash.</param>
    /// <param name="count">The number of times to Flash.</param>
    /// <returns></returns>
    public static bool Flash(Form form, uint count)
    {
      FLASHWINFO fi = Create_FLASHWINFO(form.Handle, FLASHW_ALL, count, 0);
      return FlashWindowEx(ref fi);
    }

    private static Window currentWindow;
    private static DispatcherTimer currentDPT;

    private static void Handle(object sender, EventArgs args)
    {
      currentDPT.IsEnabled = false;
      currentDPT.Stop();
      Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle,
                                               (Action)
                                               (() =>
                                                  {
                                                    try
                                                    {
                                                      FLASHWINFO fi =
                                                        Create_FLASHWINFO(new WindowInteropHelper(currentWindow).Handle,
                                                                          FLASHW_STOP, uint.MaxValue, 0);
                                                      FlashWindowEx(ref fi);
                                                    }
                                                    catch (Exception exception)
                                                    {
                                                      TaskDialog.ShowMessage(exception.ToString());
                                                    }
                                                  }
                                               ));
    }

    /// <summary>
    /// Flash the specified Window (form) for the specified number of times
    /// </summary>
    /// <param name="window">The Form (Window) to Flash.</param>
    /// <param name="count">The number of times to Flash.</param>
    /// <returns></returns>
    public static bool Flash(Window window, uint count)
    {
      FLASHWINFO fi = Create_FLASHWINFO(new WindowInteropHelper(window).Handle, FLASHW_ALL | FLASHW_TIMERNOFG, count, 0);
      FlashWindowEx(ref fi);
      //currentWindow = window;
      //currentDPT = new DispatcherTimer(new TimeSpan(0, 0, 0, 3, 500), DispatcherPriority.ApplicationIdle, new EventHandler(Handle), Dispatcher.CurrentDispatcher);
      //currentDPT.IsEnabled = true;
      //currentDPT.Start();
      //new Thread(
      //  delegate()
      //    {
      //      Thread.Sleep(3500);
      //      Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle,
      //                                               (Action)
      //                                               (() =>
      //                                                  {
      //                                                    try
      //                                                    {
      //                                                      fi =
      //                                                        Create_FLASHWINFO(new WindowInteropHelper(window).Handle,
      //                                                                          FLASHW_STOP, uint.MaxValue, 0);
      //                                                      FlashWindowEx(ref fi);
      //                                                    }
      //                                                    catch(Exception exception)
      //                                                    {
      //                                                      TaskDialog.ShowMessage(exception.ToString());
      //                                                    }
      //                                                  }
      //                                               ));
      //    }
      //  ).Start();

      
      return true;
    }

    /// <summary>
    /// Start Flashing the specified Window (form)
    /// </summary>
    /// <param name="form">The Form (Window) to Flash.</param>
    /// <returns></returns>
    public static bool Start(Form form)
    {
      FLASHWINFO fi = Create_FLASHWINFO(form.Handle, FLASHW_ALL, uint.MaxValue, 0);
      return FlashWindowEx(ref fi);
    }

    /// <summary>
    /// Stop Flashing the specified Window (form)
    /// </summary>
    /// <param name="form"></param>
    /// <returns></returns>
    public static bool Stop(Form form)
    {
      FLASHWINFO fi = Create_FLASHWINFO(form.Handle, FLASHW_STOP, uint.MaxValue, 0);
      return FlashWindowEx(ref fi);
    }

    public static bool Stop(Window window)
    {
      FLASHWINFO fi = Create_FLASHWINFO(new WindowInteropHelper(window).Handle, FLASHW_STOP, uint.MaxValue, 0);
      return FlashWindowEx(ref fi);
    }

    #region Nested type: FLASHWINFO

    [StructLayout(LayoutKind.Sequential)]
    private struct FLASHWINFO
    {
      /// <summary>
      /// The size of the structure in bytes.
      /// </summary>
      public uint cbSize;

      /// <summary>
      /// A Handle to the Window to be Flashed. The window can be either opened or minimized.
      /// </summary>
      public IntPtr hwnd;

      /// <summary>
      /// The Flash Status.
      /// </summary>
      public uint dwFlags;

      /// <summary>
      /// The number of times to Flash the window.
      /// </summary>
      public uint uCount;

      /// <summary>
      /// The rate at which the Window is to be flashed, in milliseconds. If Zero, the function uses the default cursor blink rate.
      /// </summary>
      public uint dwTimeout;
    }

    #endregion
  }
}