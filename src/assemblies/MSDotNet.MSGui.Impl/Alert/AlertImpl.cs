﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Alert/AlertImpl.cs#8 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using MorganStanley.MSDotNet.MSGui.Core;
using AlertClass = MorganStanley.MSDotNet.MSGui.Core.Alert;

namespace MorganStanley.MSDotNet.MSGui.Impl.Alert
{
    internal class AlertImpl : IAlert
    {
        private IAlertConsumer m_consumer;
        public AlertImpl(IAlertConsumer consumer)
        {
            m_consumer = consumer;
        }
        #region IAlert Members

        public void SubmitAlert(AlertClass alert)
        {
            m_consumer.Consume(alert);
        }

        #endregion
    }
}