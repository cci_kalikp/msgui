﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Alert/AlertConsumerStatusBar.cs#8 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using AlertClass = MorganStanley.MSDotNet.MSGui.Core.Alert;

namespace MorganStanley.MSDotNet.MSGui.Impl.Alert
{
  public class AlertConsumerStatusBar : IAlertConsumer
  {
    private IStatusBar m_statusBar;

    private readonly Func<AlertLevel, StatusLevel> m_converter;

    public AlertConsumerStatusBar()
      : this(alertLevel => alertLevel == AlertLevel.Critical || alertLevel == AlertLevel.Blocker
        ? StatusLevel.Critical
        : StatusLevel.Information)
    {
    }

    public AlertConsumerStatusBar(Func<AlertLevel,StatusLevel> converter)
    {
      m_converter = converter;
    }

    #region IAlertConsumerInternal Members

    public void Initialize(IUnityContainer container_)
    {
      m_statusBar = container_.Resolve(typeof (IStatusBar)) as IStatusBar;
    }

    public void Consume(AlertClass alert)
    {
      m_statusBar.MainItem.UpdateStatus(m_converter(alert.AlertLevel), alert.Text);
    }

    #endregion
  }
}