﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Alert/AlertConsumerDialogBox.cs#7 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Windows;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using AlertClass = MorganStanley.MSDotNet.MSGui.Core.Alert;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;


namespace MorganStanley.MSDotNet.MSGui.Impl.Alert
{
  public class AlertConsumerDialogBox : IAlertConsumer
  {
    #region IAlertConsumerInternal Members

    public void Consume(AlertClass alert)
    {
      TaskDialog.ShowMessage(alert.Text);
    }

    public void Initialize(IUnityContainer unityContainer)
    {
    }

    #endregion
  }
}