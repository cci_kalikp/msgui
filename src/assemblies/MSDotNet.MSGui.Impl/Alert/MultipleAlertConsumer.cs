﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Alert/MultipleAlertConsumer.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.Alert
{
  public class MultipleAlertConsumer : IAlertConsumer
  {
    private readonly IList<IAlertConsumer> m_consumers;
    private readonly Func<Core.Alert, IList<IAlertConsumer>, IEnumerable<IAlertConsumer>> m_chooser;

    public MultipleAlertConsumer(Func<Core.Alert, IList<IAlertConsumer>, IEnumerable<IAlertConsumer>> chooser, params IAlertConsumer[] consumers)
    {
      m_chooser = chooser;
      m_consumers = consumers;
    }

    #region IAlertConsumerInternal Members

    public void Consume(Core.Alert alert_)
    {
      foreach (IAlertConsumer alertConsumerMarker in m_chooser(alert_, m_consumers))
      {
          alertConsumerMarker.Consume(alert_);
      }
    }

    public void Initialize(Microsoft.Practices.Unity.IUnityContainer unityContainer_)
    {
      foreach (IAlertConsumer alertConsumerMarker in m_consumers)
      {
        if (alertConsumerMarker != null)
        {
          alertConsumerMarker.Initialize(unityContainer_);
        }
        
      }
    }

    #endregion
  }
}
