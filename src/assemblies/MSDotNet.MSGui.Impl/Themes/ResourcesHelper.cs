﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Themes/ResourcesHelper.cs#12 $
// $Change: 902915 $
// $DateTime: 2014/10/29 03:28:14 $
// $Author: caijin $

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Themes;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes
{
	internal static class ResourcesHelper
	{
		public static void RemoveAllThemeResources(ResourceDictionary dictionary_)
		{
			IEnumerable<Theme> toRemove = new List<Theme>
										 (from th in dictionary_.MergedDictionaries
											where th is Theme
											select (Theme)th);
			foreach (Theme th in toRemove)
			{
                th.ClearDefaultValues();
				if (th.BeforeUnload != null)
				{
					th.BeforeUnload.Invoke();
				}
				dictionary_.MergedDictionaries.Remove(th);
				if (th.AfterUnload != null)
				{
					th.AfterUnload.Invoke();
				}
			}
		}

		public static void AddThemeResource(ResourceDictionary dictionary_, Theme resources_, InitialSettings initialSettings_)
		{
			RemoveAllThemeResources(dictionary_);
			if (resources_ == null)
			{
				//Let's say that adding null theme means to turn off an existing theme and not adding anything new
				return;
			}
            resources_.Initialize(initialSettings_);
			if (resources_.BeforeLoad != null)
			{
				resources_.BeforeLoad.Invoke();
			}
			dictionary_.MergedDictionaries.Add(resources_);

            WalkDictionary(dictionary_, resources_, initialSettings_);

			if (resources_.AfterLoad != null)
			{
				resources_.AfterLoad.Invoke();
			}
		}

        public static void AddApplicationThemeResource(Theme resources_, InitialSettings initialSettings_)
		{
			if (System.Windows.Application.Current.Resources == null)
			{
				System.Windows.Application.Current.Resources = new ResourceDictionary();
			}
            ResourcesHelper.AddThemeResource(System.Windows.Application.Current.Resources, resources_, initialSettings_);
		}

        /// <summary>
        ///  solves a race condition in the baml reader of .net
        /// </summary>
        /// <param name="dictionary_"></param>
        /// <param name="theme_"></param>
        /// <param name="initialSettings_"></param>
        private static void WalkDictionary(ResourceDictionary dictionary_, Theme theme_, InitialSettings initialSettings_)
		{
			var dictionaries = new Queue<ResourceDictionary>();
			dictionaries.Enqueue(dictionary_);

			while (dictionaries.Count > 0)
			{
				var current = dictionaries.Dequeue();
			    IDynamicResourceDictionary dynamicDs = current as IDynamicResourceDictionary;
                if (dynamicDs != null)
                {
                    dynamicDs.Initialize(theme_, initialSettings_);
                } 
				foreach (var rd in current.MergedDictionaries)
					dictionaries.Enqueue(rd);

				Debug.WriteLine(String.Format("WalkDictionary processing: {0}", current.Source));

                foreach (DictionaryEntry entry in current) { }
			}
		}
	}
}
