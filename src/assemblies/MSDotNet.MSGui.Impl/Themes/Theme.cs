﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Themes/Theme.cs#12 $
// $Change: 900918 $
// $DateTime: 2014/10/14 22:54:19 $
// $Author: caijin $

using System;
using System.Windows;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes
{
  public class Theme : ResourceDictionary
  {
    public string Name
    {
      get; set;    
    }

    public ImageSource Icon
    {
      get; set;
    }

    public Action BeforeLoad;
    public Action AfterUnload;
  	public Action AfterLoad;
  	public Action BeforeUnload;

      public virtual  bool IsModernTheme
      {
          get { return false; }
      }

      private bool initialized = false;
      internal void Initialize(InitialSettings settings_)
      {
          SetDefaultValues();
          if (initialized) return;
          InitializeCore(settings_.ShellMode); 
          initialized = true;
      }
       
      internal virtual void ClearDefaultValues()
      { 
      }
      protected virtual void SetDefaultValues()
      {
          if (Core.CoreUtilities.AreClose(ThemeExtensions.HeaderHeightDefault, 14.0))
              ThemeExtensions.HeaderHeightDefault = 22.0;

          ThemeExtensions.DefaultDockContentPadding = 5.0;
          ThemeExtensions.FloatingWindowBorderPadding = new Thickness(2.0);
          ThemeExtensions.ContentPaneFlashBorderThickness = new Thickness(2.0);
          ThemeExtensions.ContentPaneFlashBorderMargin = new Thickness(0, 0, 2, 2);
          ThemeExtensions.FloatingWindowCaptionBarMargin = new Thickness(0, -2, 0, 0);
          ThemeExtensions.ContentPaneMargin = new Thickness(0, 0, 1, 1);
          ThemeExtensions.FloatingWindowBorderCornerRadius = new CornerRadius(2);
          ContentPaneWrapper.TabItemBorderCornerRadius = new CornerRadius(5, 5, 0, 0);
          ThemeExtensions.TileItemBorderThickness = new Thickness(2); 
      }

      protected virtual void InitializeCore(ShellMode shellMode_)
      {
          
      }
  }
}
