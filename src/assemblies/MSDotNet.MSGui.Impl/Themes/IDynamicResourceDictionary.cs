﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;

namespace MorganStanley.MSDotNet.MSGui.Impl.Themes
{
    internal interface IDynamicResourceDictionary
    {
        void Initialize(Theme theme_, InitialSettings initialSettings_);
    }
}
