﻿
namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions.Prism
{
    public static class PrismModuleExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="framework"></param>
        /// <param name="moduleInfo_"></param>
        public static void AddModule(this Framework framework, Microsoft.Practices.Prism.Modularity.ModuleInfo moduleInfo_)
        {
            framework.AddModule(moduleInfo_);
        }
    }
}