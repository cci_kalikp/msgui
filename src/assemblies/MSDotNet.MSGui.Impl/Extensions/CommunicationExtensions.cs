﻿using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.ScreenGroup;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    public static class CommunicationExtensions
    {
        /// <summary>
        /// This is used for backward compatibilities ONLY.
        /// </summary>
        /// <param name="framework"></param>
        public static void DisableVirtualDesktopSupport(this Framework framework)
        {
            InterProcessMessenger.IsVirtualDesktopSupported = false;
        }


        public static void EnableScreenGroupSupport(this Framework framework)
        {
            ScreenGroupManager.IsScreenGroupSupportEnabled = true;
        }


        /// <summary>
        /// Include the screen group selector into the view header
        /// </summary>
        /// <param name="viewContainer"></param>
        public static IWidgetViewContainer AddScreenGroupSelector(this IWindowViewContainer viewContainer)
        {
            var chromeRegistry = ServiceLocator.Current.GetInstance<IChromeRegistry>();
            var headerId = "ScreenGroup_" + viewContainer.ID;
            chromeRegistry.RegisterWidgetFactory(headerId,
                                                 (wvc, _s) =>
                                                     {
                                                         wvc.Content = new ScreenGroupSelector(viewContainer);
                                                         return true;
                                                     });
            return viewContainer.Header.AddWidget(headerId, new InitialWidgetParameters());
        }
    }
}
