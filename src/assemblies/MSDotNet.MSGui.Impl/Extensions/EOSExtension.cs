﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
	public static class EOSExtension
	{
		private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger(typeof(EOSExtension).FullName);

		internal static bool IsEOSEnabled { get; set; }
		internal static IDictionary<string, string> EOSParameters;

		public static void EnableEOS(this Framework framework_, IDictionary<string, string> parameters = null)
		{
			IsEOSEnabled = true;
			EOSParameters = parameters;
		}
	}
}
