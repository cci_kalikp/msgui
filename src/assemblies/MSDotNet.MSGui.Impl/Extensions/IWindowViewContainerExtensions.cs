﻿using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    public static class WindowViewContainerExtensions
    {
        public static void RegisterAsSticky(this IWindowViewContainer viewContainer)
        {
            var parent = DockHelper.GetRootOfFloatingPane(viewContainer);
            if (parent == null || parent.HostWindow == null) return; 
            parent.HostWindow.RegisterAsSticky();
        }
    }
}
