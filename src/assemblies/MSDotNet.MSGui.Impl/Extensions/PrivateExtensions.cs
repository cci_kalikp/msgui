﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Forms;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
  internal static class PrivateExtensions
  {
    internal static IEnumerable<DependencyObject> FindInVisualTree(DependencyObject element, Predicate<DependencyObject> predicate)
    {
      for (int i = 0; i < VisualTreeHelper.GetChildrenCount(element); i++)
      {
        DependencyObject dependencyObject = VisualTreeHelper.GetChild(element, i);

        if (predicate(dependencyObject))
          yield return dependencyObject;


        foreach (DependencyObject child in FindInVisualTree(dependencyObject, predicate))
          yield return child;
      }
    }
  }

  internal static class ScreenSizeHelper
  {
      internal static Rect TotalScreenArea = GetTotalScreenArea();

      private static Rect GetTotalScreenArea()
      {
          Rect screenArea = new Rect();
          foreach (Screen s in Screen.AllScreens)
          {
              if (s.Bounds.Height > screenArea.Height)
                  screenArea.Height = s.Bounds.Height;
              screenArea.Width += s.Bounds.Width;
          }
          return screenArea;
      }
  }
}
