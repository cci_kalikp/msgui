﻿ 
using MorganStanley.MSDotNet.MSGui.Impl.ExceptionHandling;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    public static class MailServiceExtensions
    { 

        public static void EnableMailServiceExceptionHandler(this Framework framework, params string[] toEmail)
        {
            framework.EnableExceptionHandler(toEmail); 

            // Map the MailService DLL
            framework.EnableMailService(); 
        }

        public static void EnableMailServiceExceptionHandler(this Framework framework, UnhandledExceptionCallback handler, params string[] toEmail)
        {
            framework.EnableExceptionHandler(handler,toEmail);

            // Map the MailService DLL
            framework.EnableMailService();
        }
    }
}
