﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Extensions/ApplicationExtension.cs#23 $
// $Change: 898267 $
// $DateTime: 2014/09/25 05:16:06 $
// $Author: caijin $

using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl;
//using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    public static class ApplicationExtension
    {
        public static void SetApplicationName(this Framework framework, string name)
        {
            framework.Bootstrapper.SetApplicationName(name);
        }

        [Obsolete("Use boot.SetApplicationVersion(string version_) instead")]
        public static void SetApplicationVersion(this Framework framework, Version version)
        {
            framework.Bootstrapper.Application.Version = version.ToString();
        }

        public static void SetApplicationVersion(this Framework framework, string version)
        {
            framework.Bootstrapper.SetApplicationVersion(version);
        }

        /// <summary>
        /// Set main window's icon. If you want to set a default icon for all wpf windows in the application
        /// please use assembly's icon property. 
        /// </summary>
        /// <param name="framework"></param>
        /// <param name="icon">Not any ImageSource instance could be used here. Only those that could be assigned to wpf window's Icon property.</param>
        public static void SetApplicationIcon(this Framework framework, ImageSource icon)
        {
            icon.Freeze();
            framework.Bootstrapper.Application.Icon = icon;
            ConcordConfigurationManager.ApplicationIcon = icon;
            SplashWindow.HideIcon = ModernSplashWindow.HideIcon = icon;
        }

        public static void SetApplicationIcon(this Framework framework, System.Drawing.Icon icon)
        { 
            var tempIcon = icon.ToImageSource();
            tempIcon.Freeze();
            framework.Bootstrapper.Application.Icon = tempIcon;
            ConcordConfigurationManager.ApplicationIcon = tempIcon;
            SplashWindow.HideIcon = ModernSplashWindow.HideIcon = tempIcon;
        }

        /// <summary>
        /// For LauncherBarAndFloatingWindow Shell mode only.
        /// </summary>
        /// <param name="framework"></param>
        /// <param name="name"></param>
        public static void SetAppBarTitle(this Framework framework, string name)
        {
            framework.InitialSettings.AppBarTitle = name;
        }

        public static string GetApplicationInstance(this IApplication application)
        {
            //try
            //{
            //    /* 
            //     * Yes, reflection. To our own code. until V2V/M2M is not refactored to _not_ depend on the isolation 
            //     * bits, this is the only way the PI bits can be put into separate assemblies.
            //     */
            //    var isolatedSubsystemSettings = ReflHelper.PropertyGet(application, "IsolatedSubsystemSettings");
            //    var hostProcessId = ReflHelper.PropertyGet(isolatedSubsystemSettings, "HostProcessId");
            //    return ((int)hostProcessId).ToString(CultureInfo.InvariantCulture);
            //}
            //catch
            //{
            //    // it's okay, if the refl failed this is not an isolated app - or someone broke everything.
            //}

            return Process.GetCurrentProcess().Id.ToString(CultureInfo.InvariantCulture);
        }
    }
}
