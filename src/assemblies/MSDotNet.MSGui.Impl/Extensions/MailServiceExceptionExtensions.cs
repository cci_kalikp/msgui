﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Extensions/MailServiceExceptionExtensions.cs#9 $
// $Change: 886728 $
// $DateTime: 2014/07/01 11:32:42 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Threading;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MSDesktop.MailService;
using MorganStanley.MSDotNet.MSGui.Impl.MailAttachment;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    /// <summary>
    /// Delegate for <see cref="Exception"/> <see cref="MailMessage"/> handling callbacks.
    /// </summary>
    /// <param name="exception">The exception.</param>
    /// <param name="fromAddress">The address of the sender.</param>
    /// <param name="subject">The subject.</param>
    /// <param name="attachments">The attachments to attach to the mail.</param>
    /// <param name="toAddresses">The addresses of the recipients.</param>
    public delegate void ExceptionMailMessageCallback(
        Exception exception, string fromAddress, string subject, Attachment[] attachments, string[] toAddresses);

    /// <summary>
    /// Static class for holding extension methods for the <see cref="Exception"/> class.
    /// </summary>
    public static class MailServiceExceptionExtensions
    {
        private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger("MailServiceExceptionExtensions");

        #region Throttling

        //Preventing applications from sending enormous amount of messages in very short period of time (may be caused by wpf)
        //no more then 1 message in this interval of time will be sent

        //Maximum allowed number of sent messages per application run, unlikely to be hit ever
        private const int M_MAXSENT = 100;

        private const string DEFAULT_SMTP_SERVER = "mta-hub.ms.com";
        private const string DEFAULT_MAILSERVICE_NAME = "ExceptionMailService";

        private static readonly TimeSpan _minInterval = new TimeSpan(0, 0, 10);
        private static readonly object _aggregatorLock = new object();
        private static readonly object m_lastSentInfoLock = new object();
        private static readonly Queue<MailServiceMessageGroup> _aggregated = new Queue<MailServiceMessageGroup>();

        // Mail service helper variables
        private static volatile IMailService _mailService;
        private static volatile Func<IMailServiceFactory> _mailServiceFactoryCreator;

        private static DateTime? m_whenLastSent;
        private static int m_numSentMessages;

        private static IMailService MailService
        {
            get
            {
                if (_mailService == null)
                {
                    if (_mailServiceFactoryCreator == null)
                    {
                        throw new InvalidOperationException("Mail Service must be enabled on Framework before attempting to use these extensions.");
                    }

                    var mailServiceFactory = _mailServiceFactoryCreator();
                    if (mailServiceFactory.IsServiceConfigured(DEFAULT_MAILSERVICE_NAME))
                    {
                        _mailService = mailServiceFactory.GetService(DEFAULT_MAILSERVICE_NAME);
                    }
                    else
                    {
                        _mailService = mailServiceFactory.CreateNewService(DEFAULT_SMTP_SERVER);
                    }
                }

                return _mailService;
            }
        }

        private static void AggregateMessage(MsDesktopMailMessage msg, DateTime now)
        {
            string representatonForHashing = msg.Body + msg.Subject + msg.Sender;
            foreach (var to in msg.ToRecipientsAddesses)
            {
                representatonForHashing += to;
            }

            foreach (var att in msg.Attachments)
            {
                representatonForHashing += att.Name; //assuming that equally named attachements are same
            }

            int key = representatonForHashing.GetHashCode();
            var group = _aggregated.FirstOrDefault(g => g.Id == key);
            if (group != null)
            {
                group.Quantity++;
                group.LastMessageTime = now;
            }
            else
            {
                var msgGroup = new MailServiceMessageGroup(msg, now, now, 1, key);
                _aggregated.Enqueue(msgGroup);
            }
        }

        private static void SendOrCache(MsDesktopMailMessage msg)
        {
            lock (_aggregatorLock)
            {
                DateTime now = DateTime.Now;
                if (_aggregated.Count == 0)
                {
                    AggregateMessage(msg, now);
                    if (_aggregated.Count == 1 &&
                        (!m_whenLastSent.HasValue ||
                         (_minInterval + (m_whenLastSent.Value - DateTime.Now)).TotalMilliseconds <= 0))
                    {
                        SendLatestMessage();
                    }
                    if (_aggregated.Count > 0)
                    {
                        ThreadPool.QueueUserWorkItem(
                            delegate
                                {
                                    while (true)
                                    {
                                        if (m_whenLastSent.HasValue)
                                        {
                                            TimeSpan delta = _minInterval + (m_whenLastSent.Value - DateTime.Now);
                                            if (delta.TotalMilliseconds > 0)
                                            {
                                                Thread.Sleep(delta);
                                            }
                                        }
                                        lock (_aggregatorLock)
                                        {
                                            if (SendLatestMessage()) break;
                                        }
                                    }
                                });
                    }
                }
                else
                {
                    lock (_aggregatorLock)
                    {
                        AggregateMessage(msg, now);
                    }
                }
            }
        }

        private static bool SendLatestMessage()
        {
            if (_aggregated.Count == 0)
            {
                return true;
            }
            var msgGroup = _aggregated.Dequeue();
            try
            {
                if (msgGroup.Quantity > 1)
                {
                    msgGroup.Message.Body =
                        string.Format("A group of similar exceptions. Number:{0}. First at:{1} Last at:{2}\n",
                                      msgGroup.Quantity, msgGroup.FirstMessageTime.ToString("O"),
                                      msgGroup.LastMessageTime.ToString("O")) + msgGroup.Message.Body;
                }
                if (_aggregated.Count > 1)
                {
                    msgGroup.Message.Body +=
                        string.Format("\n\nThere are few more groups of exceptions pending. Number: {0}\n",
                                      _aggregated.Count - 1);
                }
                if (m_numSentMessages >= M_MAXSENT)
                {
                    TaskDialog.ShowMessage(
                        "This application has exceeded the maximum allowed number of error reports.\n It's highly recommended to restart the application and contact corresponding support groups.");

                }
                else
                {

                    MailService.Send(msgGroup.Message);

                    lock (m_lastSentInfoLock)
                    // alternative solution: to combine messages (and their attachements) with same to/from
                    {
                        //   the current implementation is simpler, but it's more likely to lose some messages 
                        m_whenLastSent = DateTime.Now; //   if the application is closed  before we send them
                        m_numSentMessages++;
                    }
                }


            }
            catch (Exception ex)
            {

                //this kind of exception happens when the email for the same exception is sent multiple times very quickly,
                //usually due to manual click, instead of fixing the sending email code to regenerate message with new stream in this case
                //we simply ignore it, since this means redundant email sending
                if (msgGroup.Message.Attachments.Any() && ex.InnerException is ObjectDisposedException &&
                    ex.InnerException.Message == "Cannot access a closed Stream.")
                {
                    return false;
                }
                m_logger.Error("Failed to send error report", ex); 
            }
            return false;
        }

        #endregion

        /// <summary>
        /// This event is rased when a <see cref="Exception"/> <see cref="MailMessage"/> is sent.
        /// </summary>
        public static event ExceptionMailMessageCallback ExceptionMailMessageSent;

        /// <summary>
        /// Sends an e-mail containing the specified parameters.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="fromAddress">The address of the sender.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="attachments">The attachments to attach to the mail.</param>
        /// <param name="toAddresses">The addresses of the recipients.</param>
        public static void SendMailServiceMail(this Exception exception, string fromAddress, string subject,
                                               Attachment[] attachments, params string[] toAddresses)
        {
            if (!FrameworkExtensions.MailServiceEnabled)
            {
                throw new InvalidOperationException("Mail service must be enabled on Framework to send exception mails.");
            }

            if (!IsMailServiceConfigured() && toAddresses.Length == 0)
            {
                throw new ArgumentException(
                    "A mail service must be configured or at least one to address must be set in order to send the exception in e-mail.");
            }

            // Create the mail message
            var mailMessage = MailService.CreateMailMessage();

            if (!string.IsNullOrEmpty(fromAddress))
                mailMessage.Sender = fromAddress;

            if (!string.IsNullOrEmpty(subject))
                mailMessage.Subject = subject;

            // Add To recipients and attachments
            foreach (var email in toAddresses)
            {
                mailMessage.AddToRecipient(email);
            }

            bool isBodyHtml = false;
            string body = string.Empty;
 
            if (attachments != null)
            {
                foreach (var attachment in attachments)
                {
                    if (attachment == null) continue;
                    var largeFileAttachment = attachment as LargeFileAttachment;
                    if (largeFileAttachment != null)
                    {
                        body += string.Format("<a href='file:///{0}'>{1}</a></br>", largeFileAttachment.FileLink,
                                              largeFileAttachment.Name);
                        isBodyHtml = true;
                        continue; 
                    }
                    var attachmentStream = attachment.ContentStream;
                    var attachmentData = new byte[attachmentStream.Length];
                    attachment.ContentStream.Read(attachmentData, 0, (int) attachmentStream.Length);
                    mailMessage.AddAttachment(new MsDesktopMailAttachment(attachment.Name, attachmentData));
                }
            }

            if (isBodyHtml)
            {
                DateTime? occurred = exception.GetOccurred();
                if (occurred != null)
                {
                    body += "Occurred at " + occurred.Value.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + "<br/>";
                }
                body += exception.GetDetailedMessage().Replace(Environment.NewLine, "<br/>");
            }
            else
            {
                DateTime? occurred = exception.GetOccurred();
                if (occurred != null)
                {
                    body += "Occurred at " + occurred.Value.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + Environment.NewLine;
                }
                body += exception.GetDetailedMessage();
            }

            mailMessage.Body = body;
            mailMessage.IsBodyHtml = isBodyHtml;

            if (!DisableEmailSend)
            {
                SendOrCache(mailMessage);
            }

            InvokeMailMessageSent(exception, fromAddress, subject, attachments, toAddresses);
        }

        /// <summary>
        /// Get location of assembly.
        /// </summary>
        /// <remarks>
        /// If the assembly is dynamically generated
        /// an exception will be thrown when trying to get the location
        /// property. This is now wrapped in a method 
        /// with <see cref="DebuggerNonUserCodeAttribute"/>
        /// so that users will not break on exception.
        /// </remarks>
        /// <param name="assembly_">Assembly to get location from.</param>
        /// <returns>Location of assembly.</returns>
        [DebuggerNonUserCode]
        [MethodImpl(MethodImplOptions.NoInlining)]
        private static string GetLocation(Assembly assembly_)
        {
            string location = "Cannot be retrieved.";
            try
            {
                if (assembly_ != null && !(assembly_ is AssemblyBuilder) &&
                    assembly_.GetType().FullName != "System.Reflection.Emit.InternalAssemblyBuilder")
                    location = assembly_.Location;
            }
            catch (NotSupportedException)
            {
                // in case of dynamic assemblies the Location property cannot be retrieved,
                // so let's ignore it.
            }
            return location;
        }

        /// <summary>
        /// Invoke the ExceptionMailMessageSent event safely.
        /// </summary>
        /// <param name="exception_">The exception.</param>
        /// <param name="fromAddress_">The address of the sender.</param>
        /// <param name="subject_">The subject.</param>
        /// <param name="attachments_">The attachments to attach to the mail.</param>
        /// <param name="toAddresses_">The addresses of the recipients.</param>
        private static void InvokeMailMessageSent(Exception exception_, string fromAddress_, string subject_,
                                                  Attachment[] attachments_, string[] toAddresses_)
        {
            ExceptionMailMessageCallback sent = ExceptionMailMessageSent;
            if (sent != null)
            {
                sent(exception_, fromAddress_, subject_, attachments_, toAddresses_);
            }
        }

        internal static bool IsMailServiceConfigured()
        {
            return !string.IsNullOrEmpty(MailService.Name);
        }

        internal static void EnableMailService(Func<IMailServiceFactory> factoryCreator)
        {
            _mailServiceFactoryCreator = factoryCreator;
        }

        /// <summary>
        /// Gets or sets a value indicating whether E-mails will be sent.
        /// </summary>
        internal static bool DisableEmailSend { get; set; }

        private sealed class MailServiceMessageGroup
        {
            private readonly MsDesktopMailMessage _message;
            private readonly DateTime _firstMessageTime;
            private readonly int _id;

            public MailServiceMessageGroup(MsDesktopMailMessage message, DateTime firstMessageTime, DateTime lastMessageTime,
                                int quantity, int id)
            {
                _id = id;
                _firstMessageTime = firstMessageTime;
                _message = message;
                Quantity = quantity;
                LastMessageTime = lastMessageTime;
            }

            public MsDesktopMailMessage Message
            {
                get { return _message; }
            }

            public DateTime FirstMessageTime
            {
                get { return _firstMessageTime; }
            }

            public DateTime LastMessageTime { get; set; }
            public int Quantity { get; set; }

            public int Id
            {
                get { return _id; }
            }
        }
    }
}
