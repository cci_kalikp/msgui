﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Extensions/LayoutLoadStrategy.cs#7 $
// $Change: 870322 $
// $DateTime: 2014/03/10 11:02:00 $
// $Author: caijin $

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    /// <summary>
    /// Sets the load strategy for the layouts
    /// </summary>
    public enum LayoutLoadStrategy
    {
        /// <summary>
        /// Checks for changed profile
        /// </summary>
        Default = 0,

        /// <summary>
        /// Always ask before saving layout
        /// </summary>
        AlwaysAsk = 1,

        /// <summary>
        /// Never ask before saving layout
        /// </summary>
        NeverAsk = 2,

        /// <summary>
        /// Use callback for checking whether there is a need for question
        /// </summary>
        UseCallback = 3,

        /// <summary>
        /// Never ask before saving the layout, but do pop up exit window
        /// </summary>
        NeverAskButPromptForExit = 4
    }
}
