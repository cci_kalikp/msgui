﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Extensions/FrameworkExtensions.cs#159 $
// $Change: 902331 $
// $DateTime: 2014/10/24 05:43:11 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using Microsoft.Practices.Unity;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Annotations;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeConfig;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl; 
using MorganStanley.MSDotNet.MSGui.Impl.ExceptionHandling;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.MailAttachment;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Printing;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.BarWithWindow;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ViewModels;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions;
using MSDesktop.MailService;
using CommandBarAdapter = MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.BarAdapters.CommandBarAdapter;
using Control = System.Windows.Forms.Control;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    /// <summary>
    /// Delegate for setting up callback for checking layout loads
    /// </summary>
    /// <returns>true if dialog should be shown</returns>
    public delegate bool LayoutLoadStrategyCallback();

    public static class UnityExtensions
    {
        internal static bool UnitySafeBehavior;

        public static void EnableUnitySafeBehavior()
        {
            UnitySafeBehavior = true;
        }
    }

    /// <summary>
    /// Extension methods for <see cref="Framework"/>.
    /// </summary>
    public static class FrameworkExtensions
    {
        // Mail service and exception mail factory helper variables
        private static volatile bool _mailServiceEnabled;
        private static UnhandledExceptionCallback m_exceptionCallback;

        /// <summary>
        /// Register an initial splash loader from the start EXE.
        /// This will cause the initial loader to be closed at the correct point
        /// and replaced with the WPF loader.
        ///
        /// This object should contain 2 public methods:
        /// void Stop();
        /// System.Drawing.Point GetLocation();
        /// </summary>
        /// <param name="framework">Framework to apply splash screen to.</param>
        /// <param name="splash">Initial splash screen.</param>
        public static void RegisterInitialSplash(this Framework framework, object splash)
        {
            if (splash != null)
                new InitialSplashWindowFrameworkWrapper(framework, splash);
        }

        /// <summary>
        /// Enable reflection loading of <see cref="IModule"/>s defined
        /// in the entry assembly. (Call before .Start());
        /// </summary>
        /// <param name="fw_">The framework instance to start with.</param>
        public static void EnableReflectionModuleLoad(this Framework fw_)
        {
            fw_.Bootstrapper.IsUseReflection = true;
        }

        public static void EnforceModuleLoadingDependency(this Framework framework)
        {
            ResolverConfigurationModuleCatalog.EnforceModuleLoadingDependency = true;
        }

        /// <summary>
        /// Allows add module programatically.
        /// </summary>
        /// <param name="framework">The framework instance to start with.</param>
        /// <param name="moduleType">The type of the module to be added.</param>
        public static void AddModule(this Framework framework, Type moduleType)
        {
            framework.AddModule(moduleType);
        }

        /// <summary>
        /// Allows add module programatically.
        /// </summary>
        /// <param name="framework">The framework instance to start with.</param>
        /// <param name="moduleType">The type of the module to be added.</param>
        /// <param name="moduleName"></param>
        public static void AddModule(this Framework framework, Type moduleType, string moduleName)
        {
            framework.AddModule(moduleType, moduleName);
        }

        public static void AddModule<T>(this Framework framework)
        {
            framework.AddModule<T>();
        }

        public static void AddModule<T>(this Framework framework, string moduleName)
        {
            Debug.Assert(typeof (T).GetInterface("imodule", true) != null);
            framework.AddModule(typeof (T), moduleName);
        }

        public static void EnableLogging(this Framework fw, string applicationName = null, string directory = null,
                                         string logbaseName = null, IDictionary<string, string> loggingLevels = null)
        {
            fw.Bootstrapper.LoggingParameter = new Bootstrapper.LoggingParameterInfo
                {
                    Directory = directory,
                    Basename = logbaseName,
                    LoggingLevels = loggingLevels,
                    ApplicationName = applicationName
                };
            fw.Bootstrapper.EnableLogging = true;
        }

        //public static IApplication GetApplication(this Framework fw_)
        //{
        //    return fw_.BootstrapperInternal.Application;
        //}

        public static void EnableExceptionHandler(this Framework framework, [NotNull] UnhandledExceptionCallback handler, params string[] toEmail)
        {
            Guard.ArgumentNotNull(handler, "callback");


            var fromEmail = Environment.UserName + "@morganstanley.com";
 
            ApplicationUnhandledExceptions.AddDefaultHandler(
                framework,
                handler, fromEmail,
                 String.Format("UI Thread Exception in {0}:{1}", framework.Bootstrapper.Application.Name, framework.Bootstrapper.Application.Version), //  If subject is set elsewhere it will be used;
                //  otherwise default subject will be used
                null, //  No attachments
                toEmail);
            AppDomainUnhandledExceptions.AddDefaultHandler(
                framework,
                handler, //  No action
                fromEmail,
                 String.Format("Background Thread Exception in {0}:{1}", framework.Bootstrapper.Application.Name, framework.Bootstrapper.Application.Version), //  If subject is set elsewhere it will be used;
                //  set elsewhere and used directly
                null, //  No attachments
                toEmail);
        }

        /// <summary>
        /// Enable top level exception handler.
        /// </summary>
        /// <param name="framework">The framework instance to start with.</param>
        /// <param name="toEmail">E-Mail address to send to</param>
        public static void EnableExceptionHandler(this Framework framework, params string[] toEmail)
        {
            Guard.ArgumentNotNull(toEmail, "toEmail");
            EnableExceptionHandler(framework, (exception, window) => { }, toEmail);
        }



        /// <summary>
        /// Setting up the layout loading strategy
        /// </summary>
        /// <param name="framework_">The framwork instance to start with.</param>
        /// <param name="layoutLoadStrategy">Strategy to use</param>
        public static void SetLayoutLoadStrategy(this Framework framework_, LayoutLoadStrategy layoutLoadStrategy)
        {
            framework_.InitialSettings.LayoutLoadStrategy = layoutLoadStrategy;
            PersistenceProfileService.LayoutLoadStrategy = layoutLoadStrategy;
        }

        /// <summary>
        /// Setting up the layout loading strategy
        /// </summary>
        /// <param name="framework_">The framwork instance to start with.</param>
        /// <param name="layoutLoadStrategyCallback">Callback to be used</param>
        public static void SetLayoutLoadStrategy(this Framework framework_,
                                                 LayoutLoadStrategyCallback layoutLoadStrategyCallback)
        {
            framework_.InitialSettings.LayoutLoadStrategy = LayoutLoadStrategy.UseCallback;
            framework_.InitialSettings.LayoutLoadStrategyCallback = layoutLoadStrategyCallback;
            PersistenceProfileService.LayoutLoadStrategy = LayoutLoadStrategy.UseCallback;
        }

        /// <summary>
        /// Sets the rename gesture for view headings
        /// </summary>
        /// <param name="framework">The framwork instance to start with.</param>
        /// <param name="keyGesture">The rename gesture</param>
        public static void SetViewRenameKeyGesture(this Framework framework, KeyGesture keyGesture)
        {
            framework.InitialSettings.WindowRenameGesture = keyGesture;
        }

        public static void SetAlertConsumer(this Framework framework_, IAlertConsumer alertConsumer)
        {
            framework_.InitialSettings.AlertConsumer = alertConsumer;
        }

        [Obsolete("Use EnableShutdownTimer instead.", true)]
        public static void SetQuitDialogAutomaticBehaviour(this Framework framework_, int seconds, bool save)
        {
        }

        /// <summary>
        /// Enable non-owned floating windows in the XamDockManager instances (non-owned windows can appear behind their parent window).
        /// </summary>
        /// <param name="f">The framework instance to start with.</param>
        public static void EnableNonOwnedWindows(this Framework f)
        {
            WindowViewContainer.DefaultIsOwnedWindowValue = false;
            f.Bootstrapper.EnableNonOwnedWindows();
        }

        /// <summary>
        /// Set condition to skip the showing of the save on exit dialog/saving the profile automatically according the application settings, if save on exit is enabled 
        /// </summary>
        /// <param name="f"></param>
        public static void SkipSaveProfileOnExitWhen(this Framework f, SkipSaveProfileOnExitCondition onExitCondition_)
        {
            f.Bootstrapper.SkipSaveProfileOnExitWhen(onExitCondition_); 
        }

        /// <summary>
        /// Enables workspace-like behaviour for floating views. Floating views will be tied to the active tab at their creation and be visible only when that tab is active.
        /// </summary>
        /// <param name="f">The framework instance to start with.</param>
        public static void EnableTabsOwnFloatingWindows(this Framework f)
        {
            ChromeManagerBase.TabsOwnFloatingWindows = true;
        }

        /// <summary>
        /// Calls SetCompatibleTextRenderingDefault with the specified value.
        /// </summary>
        /// <param name="f">The framework instance to start with.</param>
        /// <param name="value">The new value.</param>
        public static void EnableCompatibleTextRendering(this Framework f, bool value = true)
        {
            System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(value);
        }

        public static void EnableNonQuietCloseOfViews(this Framework f, bool value = true)
        { 
                DockViewModel.EnableNonQuietCloseOfViews = value; 
        }

        /// <summary>
        /// Sets Control.CheckForIllegalCrossThreadCalls to the specified value.
        /// </summary>
        /// <param name="f">The framework instance to start with.</param>
        /// <param name="value">the new value.</param>
        public static void EnableCheckForIllegalCrossThreadCalls(this Framework f, bool value = true)
        {
            Control.CheckForIllegalCrossThreadCalls = value;
        }

        public static void SetLoginBehavior(this Framework f, bool showDialog)
        {
        }

        public static void SetLoginBehavior(this Framework f, bool showDialog, bool enableImpersonation)
        {
            UserNameProfileWindow.IsImpersonationEnabled = enableImpersonation;
            f.SetLoginBehavior(true);
        }

        /// <summary>
        /// Sets the unhandled exception dialog's initial settings.
        /// </summary>
        /// <param name="f">The framework instance to start with.</param>
        /// <param name="copyButtonVisible">Indicates whether the Copy button is visible.</param>
        /// <param name="emailButtonCloses">If true, pressing the Email button automatically closes the exception dialog.</param>
        /// <param name="suppressDialog">If true, the dialog will not be shown and the email will be sent silently.</param>
        public static void SetExceptionDialogBehaviour(this Framework f, bool copyButtonVisible = true,
                                                       bool emailButtonCloses = true, bool suppressDialog = false)
        {
            ApplicationUnhandledExceptions.IsCopyVisibleByDefault = copyButtonVisible;
            ApplicationUnhandledExceptions.EmailButtonCloses = emailButtonCloses;
            ApplicationUnhandledExceptions.SuppressDialog = suppressDialog;
        }

        public static void SetExceptionDialogBehaviour(this Framework f, bool copyButtonVisible = true,
                                                       bool emailButtonCloses = true,
                                                       bool suppressDialog = false,
                                                       string exceptionMessage = null,
                                                       bool attachScreenshot = false,
                                                       bool attachLog = false,
                                                       bool attachFullLog = false)
        {
            f.SetExceptionDialogBehaviour(copyButtonVisible, emailButtonCloses, suppressDialog, exceptionMessage); 
            ApplicationUnhandledExceptions.AttachScreenshot = attachScreenshot;
            ApplicationUnhandledExceptions.AttachLog = attachLog;
            ApplicationUnhandledExceptions.AttachFullLog = attachFullLog;
            if (attachFullLog)
            {
                ExtensionsUtils.RequireAssembly(ExternalAssembly.SharpZipLib, ExternalAssembly.AttachLink);
            }
        }

        public static void DontAttachMinimizedScreensToExceptionReport(this Framework f)
        {
            ApplicationUnhandledExceptions.AttachMinimizedScreens = AppDomainUnhandledExceptions.AttachMinimizedScreens = false;
        }

        public static void DisallowContinueInExceptionDialog(this Framework f)
        {
            ApplicationUnhandledExceptions.IsContinueEnabledByDefault = false; 
        }

        public static void SendExceptionReportAutomatically(this Framework f)
        {
            ApplicationUnhandledExceptions.SendEmailAutomatically = true;
        }
        /// <summary>
        /// Sets the unhandled exception dialog's initial settings.
        /// </summary>
        /// <param name="f">The framework instance to start with.</param>
        /// <param name="copyButtonVisible">Indicates whether the Copy button is visible.</param>
        /// <param name="emailButtonCloses">If true, pressing the Email button automatically closes the exception dialog.</param>
        /// <param name="suppressDialog">If true, the dialog will not be shown and the email will be sent silently.</param>
        public static void SetExceptionDialogBehaviour(this Framework f, bool copyButtonVisible = true,
                                                       bool emailButtonCloses = true, bool suppressDialog = false,
                                                       string exceptionMessage = null)
        {
            f.SetExceptionDialogBehaviour(copyButtonVisible, emailButtonCloses, suppressDialog);
            ApplicationUnhandledExceptions.NonDefaultUserInfoText = exceptionMessage;  
        }

        /// <summary>
        /// Sets the unhandled exception dialog's initial settings.
        /// </summary>
        /// <param name="f">The framework instance to start with.</param>
        /// <param name="copyButtonVisible">Indicates whether the Copy button is visible.</param>
        /// <param name="emailButtonCloses">If true, pressing the Email button automatically closes the exception dialog.</param>
        /// <param name="suppressDialog">If true, the dialog will not be shown and the email will be sent silently.</param>
        public static void SetExceptionDialogBehaviour(this Framework f, bool copyButtonVisible = true,
                                                       bool emailButtonCloses = true, bool suppressDialog = false,
                                                       object exceptionMessageFrameworkElement = null)
        {
            f.SetExceptionDialogBehaviour(copyButtonVisible, emailButtonCloses, suppressDialog);
            ApplicationUnhandledExceptions.NonDefaultUserInfoFrameworkElement = exceptionMessageFrameworkElement;

        }

        /// <summary>
        /// Set up the mail attachment threshold when sending exception log, used when attachFullLog is enabled for SetExceptionDialogBehaviour method. 
        /// Should reference \\san01b\DevAppsGML\dist\msdotnet\PROJ\sharpziplib\0.86.0\assemblies\ICSharpCode.SharpZipLib.dll and 
        /// \\san01b\DevAppsGML\dist\deskapps\PROJ\attachlink-api\1.4.1\assemblies\DeskApps.attachlink-api.dll in the project in this case
        /// </summary>
        /// <param name="f_">The framework instance to start with</param>
        /// <param name="largeFileStorageDirectory_">the shared directory to share the large file to be shared when the file to be attached is even larger than the attachlink threshold, if not set, would be in a local temp directory</param>
        /// <param name="maxOutlookAttachFileSize_">the maximum size of the file in bytes to be attached directly in email, default is 19MB</param>
        /// <param name="maxAttachlinkFileSize_">the maximum size of the file in bytes to be attached as attachlink in email, default is 70MB</param> 
        public static void ConfigMailAttachmentService(this Framework f_, string largeFileStorageDirectory_, int? maxOutlookAttachFileSize_=null, int? maxAttachlinkFileSize_=null)
        {
            
            if (maxOutlookAttachFileSize_ != null)
            {
                MailAttachmentService.MaxOutlookAttachFileSize = maxOutlookAttachFileSize_.Value;

            }
            if (maxAttachlinkFileSize_ != null)
            {
                MailAttachmentService.MaxAttachlinkFileSize = maxAttachlinkFileSize_.Value;
            }
            MailAttachmentService.LargeFileStorageDirectory = largeFileStorageDirectory_;
        }

        public static void SetMaxAttachedLogEntryCount(this Framework fw, int maxLogEntryCount_ = 200)
        {
            fw.Bootstrapper.MaxMemoryLogEntryCount = maxLogEntryCount_;
        }

        public static void SetBackgroundThreadExceptionDialogBehavior(this Framework f, bool copyButtonVisible = true,
            bool suppressDialog = false, bool continueEnabled = true, string errorMessage = null,
            bool attachScreenshot = false, bool attachLog = false, bool attachFullLog = false)
        {
             
            AppDomainUnhandledExceptions.SuppressDialog = suppressDialog;
            AppDomainUnhandledExceptions.ContinueEnabled = continueEnabled;
            AppDomainUnhandledExceptions.CopyButtonEnabled = copyButtonVisible;
            AppDomainUnhandledExceptions.AttachScreenshot = attachScreenshot;
            AppDomainUnhandledExceptions.AttachLog = attachLog;
            AppDomainUnhandledExceptions.AttachFullLog = attachFullLog;
            if (!string.IsNullOrEmpty(errorMessage))
            {
                AppDomainUnhandledExceptions.NonDefaultUserInfoText = errorMessage;   
            }
        } 
        /// <summary>
        /// Sets the quit prompt dialog's timeout.
        /// </summary>
        /// <param name="f">The framework instance to start with.</param>
        /// <param name="timeout">The timeout value in seconds. If null or zero, the dialog will not time out.</param>
        [Obsolete("Use SetQuitPromptDialogBehaviour instead.")]
        public static void EnableShutdownTimer(this Framework f, int? timeout)
        {
            if (f.EnableLegacyDialog)
            {
                ExitDialog.DefaultTimeout = timeout;
            }
            else
            {
                ExitTaskDialogLauncher.DefaultTimeout = timeout;
            }
        }

        /// <summary>
        /// Enables size-to-content behaviour on concord-based forms.
        /// </summary>
        /// <param name="f">The framework instance to start with.</param>
        public static void EnableAutosizeConcordForms(this Framework f)
        {
            WindowAdapter.AutosizeConcordForms = true;
        }

        /// <summary>
        /// Sets the quit prompt dialog's initial settings.
        /// </summary>
        /// <param name="f">The framework instance to start with.</param>
        /// <param name="showDontAskAgainOption">Indicates whether the "Don't show this again" option is visible.</param>
        /// <param name="showSaveOption">Indicates whether the "Save and quit" button is visible.</param>
        /// <param name="timeout">The dialog will dismiss itself after this many seconds (without saving). If null or zero, the dialog will never time out.</param>
        public static void SetQuitPromptDialogBehaviour(this Framework f, bool showDontAskAgainOption = true,
                                                        bool showSaveAndQuitOption = true, int? timeout = null,
                                                        string warningprompt = null)
        {
            if (f.EnableLegacyDialog)
            {
                ExitDialog.ShowDontAskAgainOption = showDontAskAgainOption;
                ExitDialog.ShowSaveAndQuitOption = showSaveAndQuitOption;
                ExitDialog.DefaultTimeout = timeout;
                ExitDialog.WarningBlockText = warningprompt;
            }
            else
            {
                ExitTaskDialogLauncher.ShowDontAskAgainOption = showDontAskAgainOption;
                ExitTaskDialogLauncher.ShowSaveAndQuitOption = showSaveAndQuitOption;
                ExitTaskDialogLauncher.DefaultTimeout = timeout;
                ExitTaskDialogLauncher.WarningBlockText = warningprompt;
            }
        }

        /// <summary>
        /// Restricts pane closing to the X button in the pane header. All other methods, for example Alt+F4, right-click menu on the taskbar won't work.
        /// </summary>
        /// <param name="f">The framework instance to start with.</param>
        /// <param name="value">True if restricted.</param>
        public static void RestrictWindowClosingToHeaderButton(this Framework f, bool value = true)
        {
            FloatingWindowUtil.RestrictClosingToHeaderButton = value;
        }

        public static void SetFocusHackEnabled(this Framework f, bool enabled=true)
        {
            XamDockManagerExtensions.FocusHackEnabled = enabled;
        }

        public static void SetViewTitleRestoreNoOverwrite(this Framework f, bool noOverwrite=true)
        {
            ChromeManagerBase.ViewTitleRestoreNoOverwrite = noOverwrite;
        }

        public static void SetViewLocationNoOverwrite(this Framework f, bool noOverwrite=true)
        {
            ChromeManagerBase.ViewLocationNoOverwrite = noOverwrite;
        }

        /// <summary>
        /// As long as true, all PlaceWidget calls will be altered and routed into ChromeArea.Locker. Disabling it will
        /// NOT restore the registrations made previously, thus enabling placement from one centralized location after
        /// turning the feature off.
        /// </summary>
        /// <param name="f">The framework instance to start with.</param>
        /// <param name="enabled">True to enable the locker.</param>
        /// <param name="mainAreasOnly">True to enable the locker only for the ribbon, qat and launcherbar areas.</param>
        public static void ForceLockerRegistrations(this Framework f, bool enabled=true, bool mainAreasOnly = true)
        {
            ChromeManagerBase.LockerBlackHoleEnabled = enabled;
            ChromeManagerBase.LockerAffectsMainAreasOnly = mainAreasOnly;
        }

        public static void ForceLockerRegistrations(this Framework f, bool enabled=true)
        {
            ForceLockerRegistrations(f, enabled, true);
        }

        /// <summary>
        /// When called, changes ChromeManager behaviour so that widgets will be moved to the end of the placement
        /// queue when changed. Combined with the locker, this can be used to mask the order used to register the
        /// widgets by calling chromeManager.PlaceWidget with the parameters attribute set to null.
        /// </summary>
        /// <param name="f">The framework instance to start with.</param>
        public static void EnableChromeReordering(this Framework f)
        {
            ChromeManagerBase.ChromeReorderingEnabled = true;
        }

        /// <summary>
        /// When set, the application will try to acquire user input focus after startup is complete.
        /// </summary>
        /// <param name="f">The framework instance to start with.</param>
        public static void AcquireFocusAfterStartup(this Framework f)
        {
            ChromeManagerBase.AcquireFocusAfterStartup = true;
        }

        public static void SuppressMainWindowFocusGrabAtStartup(this Framework f)
        {
            ChromeManagerBase.MainWindowFocusGrabAtStartupSuppressed = true;
        }
        /// <summary>
        /// Sets the default window parameters for concord windows.
        /// Note: this will only set the InitialLocation and EnforceSizeRestrictions properties.
        /// </summary>
        /// <param name="framework">The framework.</param>
        /// <param name="parameters">the default parameters.</param>
        public static void SetDefaultWindowParameters(this Framework framework, InitialWindowParameters parameters)
        {
            InitialWindowParameters.Defaults = parameters;
        }

        public static void SetDefaultTileViewParameters(this Framework framework, InitialTileParameters parameters)
        {
            InitialTileParameters.Defaults = parameters;
        }
        /// <summary>
        /// Enables immediate pane dragging.
        /// </summary>
        /// <param name="framework">The framework.</param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void UseImmediatePaneDragging(this Framework framework)
        {
            TabbedDock.UseImmediatePaneDragging = true;
            FloatingOnlyDock.UseImmediatePaneDragging = true;
        }

        /// <summary>
        /// Enables system based pane dragging.
        /// </summary>
        /// <param name="framework">The framework.</param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void UseSystemWindowDrag(this Framework framework)
        {
            TabbedDock.UseSystemWindowDrag = true;
            FloatingOnlyDock.UseSystemWindowDrag = true;
        }

        public static void EnableTopmostFloatingPanes(this Framework framework)
        {
            DockViewModel.EnableTopmost = true;
        }

        public static void EnablePrintSupport(this Framework framework)
        {
            if (!framework.Bootstrapper.HasModule(typeof (PrintModule)))
                framework.AddModule<PrintModule>();
        }

        public static void DisableExpandedLauncherBar(this Framework framework)
        {
            BarWindow.CanToggleState = false;
        }

        public static void DisableUserNameOnLauncherBar(this Framework framework)
        {
            BarWindow.UserNameOnLauncherBar = false;
        }

        public static void HideLayoutNameOnLauncherBar(this Framework framework)
        {
            BarWindow.ShowLayoutNameOnLauncherBar = false;
        }
        public static void EnableOneClickMinimizeLauncherBar(this Framework framework)
        {
            BarWindow.CanMinimize = true;
        }

        /// <summary>
        /// Allow user to manually resize the launcher bar so that it can be narrower or wider
        /// </summary>
        /// <param name="framework"></param>
        /// <param name="autoScroll">If the launcher bar is not wide enough to show all content, should the scroll bar appear automatically?</param>
        public static void EnableHorizontalResizeLauncherBar(this Framework framework, bool autoScroll)
        {
            BarWindow.CanResizeHorizontally = true;
            BarWindow.AutoScrollHorizontally = autoScroll;

        }

        /// <summary>
        /// called so that horizontal scrollbar won't appear in the launcher bar even if it's not wide enough to show all content
        /// </summary>
        /// <param name="framework"></param>
        public static void DisableLauncherBarHorizontalAutoScroll(this Framework framework)
        {
            BarWindow.AutoScrollHorizontally = false; 
        }
        /// <summary>
        /// Enables cancellable WindowDeactivating notification.
        /// </summary>
        /// <param name="framework_">The framework.</param>
        /// <param name="enabled">New state of the switch.</param>
        public static void SetWindowDeactivatingNotificationEnabled(this Framework framework_, bool enabled=true)
        {
            TabbedDock.WindowDeactivationNotificationEnabled = enabled;
            FloatingOnlyDock.WindowDeactivationNotificationEnabled = enabled;
        }

        /// <summary>
        /// Sets height of the launcher bar. Affects buttons' size. Will only work in horizontal orientation.
        /// </summary>
        /// <param name="framework">The framework.</param>
        /// <param name="shrinked">Height when the bar is shrinked.</param>
        /// <param name="expanded">Height when the bar is expanded.</param>
        public static void SetLauncherBarHeight(this Framework framework, int shrinked, int expanded)
        {
            ExpandableBar.HeightShrinked = shrinked;
            ExpandableBar.HeightExpanded = expanded;
            BarStateToHeightConverter.HeightShrinked = shrinked;
            BarStateToHeightConverter.HeightExpanded = expanded;
        }

        public static void SetLauncherBarHeight(this Framework framework, int? shrinked=null, int? expanded=null)
        {
            if (shrinked != null)
            {
                ExpandableBar.HeightShrinked = shrinked.Value;
                BarStateToHeightConverter.HeightShrinked = shrinked.Value;
            }
            if (expanded != null)
            {
                ExpandableBar.HeightExpanded = expanded.Value;
                BarStateToHeightConverter.HeightExpanded = expanded.Value;           
            }
        }

        
        public static void SetLauncherBarButtonMaxHeight(this Framework framework_, double maxHeight)
        {
            LauncherBarButton.MaxHeight = maxHeight;
        }

        [Obsolete("Use HideOrLockShellElements framework extension instead", false)]
        public static void DisableViewPinning(this Framework framework)
        {
            WindowViewModel.IsPinningAllowedSetting = false;
        }

        public static void SetTabGroupTabStripPlacement(this Framework framework, Dock dock)
        {
            DockViewModel.TabGroupTabStripPlacement = dock;
        }

        public static void EnableTabDragAndDrop(this Framework framework)
        {
            ExtensionsUtils.RequireAssembly(ExternalAssembly.Infragistics, ExternalAssembly.InfragisticsDragAndDrop);
            CustomXamTabControl.DragAndDropEnabled = true;
        }

        public static void EnableBackwardCompatibilityDropDownHandling(this Framework framework)
        {
            CommandBarAdapter.BackwardCompatibilityDropDownHandling = true;
        }

        public static void EnableAutoResolveMethodNameForLogging(this Framework framework)
        {
            BootstrapperBase.InjectCustomMsLoggerProvider = true;
        }

        public static void EnableLoadedWorkspaceStickyTitle(this Framework framework)
        {
            ChromeManagerBase.LoadedWorkspaceStickyTitle = true;
        }

        #region Legacy Workspace
        public static void EnableWorkspaceSaveLoad(this Framework framework)
        { 
            ChromeManagerBase.WorkspaceSaveLoadEnabled = true; 
        }
        #endregion

        public static void EnableDragDropTargetEventsOnShell(this Framework framework)
        {
            DockViewModel.FireDragDropEvents = true;
            TabbedDock.FireDragDropEvents = true;
        }

        /// <summary>
        /// Will pass the tab title into the state passed to the InitializeWindowHandler
        /// </summary>
        /// <param name="framework"></param>
        public static void EnableInjectContainingTabIntoLayout(this Framework framework)
        {
            ChromeManagerBase.OwnerTabInjectToLayout = true;
        }

        /// <summary>
        /// Allows to change the status bar height in the RibbonShell mode.
        /// </summary>
        /// <param name="framework"></param>
        /// <param name="height">New status bar height. The default is 25.8.</param>
        public static void SetStatusBarHeight(this Framework framework, double height)
        {
            ThemeExtensions.StatusBarHeight = height; 
        }

        /// <summary>
        /// By default, if a window is maximized, it's color changes to white. The
        /// behaviour can be disabled using this switch.
        /// </summary>
        /// <param name="framework"></param>
        public static void DisableTitlebarColorChange(this Framework framework)
        {
            if (framework.InitialSettings.ShellMode == ShellMode.RibbonWindow)
            {
                TitleBarPlaceHolder.ChangeTitlebarColorOnMaximize = false;
            }
        }

        /// <summary>
        /// This setting allows to change the size of the gap between views docked
        /// next to each other.
        /// </summary>
        /// <param name="framework"></param>
        /// <param name="gapSize">The size of the gap</param>
        /// <param name="shadowSize">The size of the shadow</param>
        public static void SetViewsSpacingAndShadowSize(this Framework framework, double gapSize, double shadowSize)
        {
            WindowViewModel.ShadowOffset = shadowSize;
            DockViewModel.InitialViewSpacingSize = gapSize;
             TileViewModel.ShadowOffset = shadowSize;
        }

        public static void SetViewsSpacingAndShadowSize(this Framework framework, double? gapSize=null, double? shadowSize=null)
        {
            if (gapSize != null)
            {
                DockViewModel.InitialViewSpacingSize = gapSize.Value;
            }
            if (shadowSize != null)
            {
                WindowViewModel.ShadowOffset = shadowSize.Value;
                 TileViewModel.ShadowOffset = shadowSize.Value;          
            }
        }


        public static void EnableWidgetUserConfig(this Framework framework, bool global=false)
        {

            ChromeManagerBase.WidgetUserConfigEnabled = true;
            ChromeManagerBase.DefaultWidgetFactoriesGlobal = global;
            framework.AddModule<ChromeConfigModule>(); 
        }
 
        public static void SetDockedWindowPadding(this Framework framework, double padding)
        {
            WindowViewModel.ContentPadding = new Thickness(padding);
        }

        public static void SetTileItemBorderThickness(this Framework framework, double thickness_)
        {
            ThemeExtensions.TileItemBorderThickness = new Thickness(thickness_);
        }

        internal static void EnableMailService(this Framework framework)
        {
            MailServiceExceptionExtensions.EnableMailService(() =>
                {
                    var container = framework.Container;
                    container.RegisterType<IMailServiceFactory, MailServiceFactory>(
                        new ContainerControlledLifetimeManager());
                    return container.Resolve<IMailServiceFactory>();
                });

            _mailServiceEnabled = true;
        }

        internal static bool MailServiceEnabled
        {
            get { return _mailServiceEnabled; }
        }


        public static void SetStatusBarPadding(this Framework framework_, double padding_)
        {
            ShellWindow.StatusBarPadding = new Thickness(padding_);
        }

        /// <summary>
        /// Reduces views' margins, paddings, borders and shadows to minimum in order
        /// to save real estate. Also removes background gradients.
        /// 
        /// Only applies to Blue, Black and White themes.
        /// </summary>
        public static void EnableFlatMode(this Framework framework_)
        {
            framework_.SetViewsSpacingAndShadowSize(1, 0);
            WindowViewModel.FlatModeEnabledSetting = true;
            DockViewModel.FlatModeEnabledProperty = true; 
        }

        /// <summary>
        /// By default, both document pane tab items and regular pane tab items
        /// have a uniform look. The switch allows you to restore the original
        /// Infragistics look for document tabs.
        /// </summary>
        /// <param name="framework_"></param>
        public static void EnableNonuniformLookForDocumentTabs(this Framework framework_)
        {
            CustomContentPane.NonuniformDocumentTabsMode = true;
        }
        public static void UseClassicTheme(this Framework framework_, PopupDialogType dialogType_ = PopupDialogType.All)
        {
            if ((dialogType_ & PopupDialogType.OptionsWindow) == PopupDialogType.OptionsWindow)
            {
                framework_.UseClassicOptionsWindow();
            }
            if ((dialogType_ & PopupDialogType.TaskDialog) == PopupDialogType.TaskDialog)
            {
               TaskDialog.UseClassicTheme();
            }
        }
        public static void UsePrism(this Framework framework_)
        {
            BootstrapperBase.UsePrismAsInfrasturcture = true;
        }

        /// <summary>
        /// Add a context menu to the status bar which will allow the user to control
        /// the visibility of some status bar items. In order for the item to be controlled by this
        /// context menu it must inherit from <see cref="HidableStatusBarItemBase"/>.
        /// </summary>
        /// <param name="framework_"></param>
        public static void EnableStatusBarContextMenu(this Framework framework_)
        {
            ShellWindow.StatusBarContextMenuEnabled = true;
        }


        internal static StickyWindowOptions DefaultStickWindowOptions = StickyWindowOptions.None;

        /// <summary>
        /// If enabled, all windows will be sticky by default.
        /// </summary>
        /// <param name="framework_"></param>
        public static void EnableStickinessForAllWindows(this Framework framework_)
        {
            DefaultStickWindowOptions = StickyWindowOptions.StickToOther;
        }

        /// <summary>
        /// If enabled, all windows will be sticky by default.
        /// </summary>
        /// <param name="framework_"></param>
        /// <param name="stickyWindowOptions_">option to stick to other window boundaries and/or screen boundaries</param>
        public static void EnableStickinessForAllWindows(this Framework framework_, StickyWindowOptions stickyWindowOptions_)
        { 
            DefaultStickWindowOptions = stickyWindowOptions_;
        }
        /// <summary>
        /// Enable to keep windows hidden during the layout load until the processed is finished.
        /// </summary>
        /// <param name="framework"></param>
        public static void EnableHiddenWindowsOnLayoutLoading(this Framework framework)
        {
            ChromeManagerBase.HideWindowsOnLayoutLoading = true; 
        }

        /// <summary>
        /// If disabled, the chrome manager will not remove empty ribbon groups.
        /// </summary>
        /// <param name="framework"></param>
        /// <param name="enabled"></param>
        public static void EnableRibbonCleaning(this Framework framework, bool enabled)
        {
            framework.InitialSettings.PreventRibbonCleaning = !enabled;
        }

        /// <summary>
        /// By default, a floating window group's title is bound to the title of the active pane within this group.
        /// This call allows to provide a function capable of generating the window group's title given the titles
        /// of contained windows.
        /// </summary>
        /// <param name="framework"></param>
        /// <param name="titleGenerator"></param>
        public static void EnableCustomWindowGroupTitle(this Framework framework, Func<string[], string> titleGenerator)
        {
            framework.InitialSettings.CustomToolWindowTitleGenerator = titleGenerator;
        }

        /// <summary>
        /// By default, a floating window group's title is bound to the title of the active pane within this group.
        /// This call allows to provide a separator that will be used when concatenating the list of contained panes to produce
        /// an aggregated window group's title.
        /// </summary>
        /// <param name="framework"></param>
        /// <param name="separator"></param>
        public static void EnableAggregatedWindowGroupTitle(this Framework framework, string separator)
        {
            framework.InitialSettings.CustomToolWindowTitleGenerator = strings => String.Join(separator, strings);
        }
         
        /// <summary>
        /// By default, header items of windows grouped into a floating window is hidden, only the header items of active pane is displayed on the floating window header.
        /// If this method is called, header items would show on its own pane, and the floating window header wouldn't have any header item.
        /// </summary>
        /// <param name="framework"></param>
        public static void ShowSeparateHeaderItemsInFloatingWindow(this Framework framework)
        { 
            FloatingWindowUtil.SeparateHeaderItemsInFloatingWindow = true;
        }

        /// <summary>
        /// Sets the trimming strategy to apply when view's title doesn't fit into the header bar.
        /// </summary>
        /// <param name="framework"></param>
        /// <param name="enableCharacterElipsis"></param>
        /// <param name="enableWordElipsis"></param>
        public static void SetViewTitleTextTrimming(this Framework framework, bool enableCharacterElipsis=false, bool enableWordElipsis=false)
        {
            framework.InitialSettings.ViewTitleTextTrimming = enableCharacterElipsis
                                                                  ? TextTrimming.CharacterEllipsis
                                                                  : (enableWordElipsis
                                                                         ? TextTrimming.WordEllipsis
                                                                         : TextTrimming.None);
        }

        public static void DisableQATPlacementHack(this Framework framework)
        {
            framework.InitialSettings.DisableQATHack = true;
        }

        public static void EnableAsynchronousLayoutLoading(this Framework framework)
        {
            ChromeManagerBase.AsynchronousLayoutLoading = true;
        }

        public static void EnableSequencialViewLoad(this Framework framework)
        {
            ChromeManagerBase.AsynchronousLayoutLoading = true;
            ChromeManagerBase.SequentialViewLoad = true;
            //ChromeManagerBase.HideWindowsOnLayoutLoading = false;
        }

        public static void EnablePerWindowHeaderVisibility(this Framework f)
        {
            TabbedDock.DisableHeaderTogglePersistor = true;
        }

        public static void DisableQATPersistence(this Framework f)
        {
            RibbonQATArea.UseObservableChildren = false;
        }

        public static void DisableEventAggregatorBridge(this Framework f)
        {
            f.Bootstrapper.DisableEventAggregatorBridge = true;
        }

        public static void DisableTileItemAdjustmentPreview(this Framework f)
        {
            TileView.ShowPreview = false;
        }
    }



    [Flags]
    public enum PopupDialogType
    {
        TaskDialog = 1,
        OptionsWindow = 2,
        All = TaskDialog | OptionsWindow
    }


    [Flags]
    public enum StickyWindowOptions
    {
        /// <summary>
        /// No sticky
        /// </summary>
        None = 0,
        /// <summary>
        /// stick to boundaries of other windows
        /// </summary>
        StickToOther = 1,
        /// <summary>
        /// stick to boundaries of screens
        /// </summary>
        StickToScreen = 2,
        /// <summary>
        /// stick to boundaries of other windows and screens
        /// </summary>
        StickToAll = StickToOther | StickToScreen
    } 

    public class UnitySafeBehaviorExtension : UnityDefaultBehaviorExtension
    {
        protected override void Initialize()
        {
            Context.RegisteringInstance += PreRegisteringInstance;

            base.Initialize();
        }

        private static void PreRegisteringInstance(object sender, RegisterInstanceEventArgs e)
        {
            if (e.LifetimeManager is SynchronizedLifetimeManager)
                e.LifetimeManager.GetValue();
        }
    }

    public class UnityClearBuildPlanStrategies : UnityContainerExtension
    {
        protected override void Initialize()
        {
            Context.BuildPlanStrategies.Clear();
        }
    }


    public class ModuleInitializeArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewEventArgs"/> class.
        /// </summary>
        /// <param name="viewContainer_">viewContainer associated with this event.</param>
        public ModuleInitializeArgs(string moduleType)
        {
            ModuleType = moduleType;
        }

        /// <summary>
        /// <see cref="IViewContainer"/> associated with this event.
        /// </summary>
        public string ModuleType { get; private set; }
    }
}



