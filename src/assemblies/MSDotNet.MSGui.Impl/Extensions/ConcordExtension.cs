﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Extensions/ConcordExtension.cs#31 $
// $Change: 886670 $
// $DateTime: 2014/07/01 05:24:47 $
// $Author: milosp $


using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.Win7;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions.EnvironmentExtractor;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
	internal static class ConcordExtensionStorage
	{
		internal static readonly Dictionary<object, InitialViewSettings> ConcordInitialViewSettings =
			new Dictionary<object, InitialViewSettings>();

		internal static readonly Stack<string> ConcordCommands = new Stack<string>();
		internal static string LogFolderName;

		/// <summary>
		/// Holds the shortcuts that need to be registered for the ribbon controls.
		/// </summary>
		internal static readonly Dictionary<object, ShortcutMetadata> KeyBindings = new Dictionary<object, ShortcutMetadata>();

		internal static readonly Dictionary<string, string> ViewContainerByWindowGuidStore = new Dictionary<string, string>();

        internal static readonly Dictionary<string, ConcordExtension.WindowViewContainerInitializer>
            ViewContainerInitializers = new Dictionary<string, ConcordExtension.WindowViewContainerInitializer>();

		#region Nested type: ShortcutMetadata

		internal class ShortcutMetadata
		{
			public Shortcut Shortcut { get; set; }
			public ICommand Command { get; set; }
		}

		#endregion
	}

	public static class ConcordExtension
	{
		private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger(typeof(ConcordExtension).FullName);

		//internal static Dictionary<object, InitialViewSettings> ConcordInitialViewSettings
		//{
		//  get { return ConcordExtensionStorage.ConcordInitialViewSettings; }
		//}
		//internal static Stack<string> ConcordCommands { get { return ConcordExtensionStorage.ConcordCommands; } }
		//internal static string LogFolderName { get { return ConcordExtensionStorage.LogFolderName; } }

		///// <summary>
		///// Holds the shortcuts that need to be registered for the ribbon controls.
		///// </summary>
		//internal static Dictionary<object, ConcordExtensionStorage.ShortcutMetadata> KeyBindings { get { return ConcordExtensionStorage.KeyBindings; } }

		//internal static Dictionary<string, string> ViewContainerByWindowGuidStore { get { return ConcordExtensionStorage.ViewContainerByWindowGuidStore; } }

        public static void RemoveDefaultProfileElementFromAdvancedLoginScenario(this Framework framework)
        {
            ConcordConfigurationManager.AddDefaultProfile = false;
        }

		public static void EnableConcord(this Framework framework_)
		{
			framework_.InitialSettings.ConcordEnabled = true;
			// Concord Dummy Form to host duplex
			Form fakeForm = new ConcordMSNetLoopForm
												{
													Visible = false,
													Width = 1,
													Height = 1,
													WindowState = FormWindowState.Minimized,
													FormBorderStyle = FormBorderStyle.None,
													Opacity = 0.00,
													ShowInTaskbar = false,
													ShowIcon = false,
													MinimizeBox = false,
													MaximizeBox = false,
													ControlBox = false,
													Region = new Region(new Rectangle(10, 10, 1, 1))
												};
			fakeForm.Show();
			fakeForm.Hide();
			fakeForm.SendToBack();
			Bootstrapper.ConcordFakeFormShortcut = fakeForm;
			framework_.AddModule<AddMenuItemModuleImpl>();
			framework_.AddModule<ConcordExtensionHelperModule>();
		}

        public static void PreventDoubleWindowCreation(this Framework f)
        {
            ConcordWindowManager.PreventDoubleWindowCreation = true;
        }

		public static void DoNotReparseVersionNumber(this Framework framework_)
		{
			ConcordApplication.DoNotReparseVersionNumber = true;
		}

		public static void AddToUIOnlyAppletList(this Framework framework_, string appletName)
		{
			ConcordApplication.AddToUIOnlyList(appletName);
		}

		//public static void AddConcordAppletFQNToMSDesktopCreatorIDMapping(this Framework framework_, string concordAppletFQN, string creatorID)
		//{
		//  ViewManager.appletcreatormapping.Add(concordAppletFQN.ToLower(), creatorID);
		//}

		//public static void AddConcordAppletFQNToMSDesktopCreatorIDMapping(this Framework framework_, string concordAppletFQN, string creatorID, Func<XElement, XElement> converter)
		//{
		//  ViewManager.appletcreatormapping.Add(concordAppletFQN.ToLower(), creatorID);
		//  ViewManager.appletconvertermapping.Add(creatorID, converter);
		//}

		//public static void EnableConcord(this Framework framework_, string defaultProfile)
		//{
		//  EnableConcord(framework_);
		//  framework_.InitialSettings.ConcordDefaultProfile = defaultProfile;
		//}

		public static void EnableConfigEnvironmentVariableExtractor(this Framework framework_)
		{
			framework_.Bootstrapper.EnableConfigEnvironmentVariableExtractor();
		}

		public static void EnableConfigEnvironmentVariableExtractor(this Framework framework_,
																	IVersionConfigurationReader configurationReader_,
																	IEnvironmentVariableDetailsConfigurationReader variableDetailsConfigurationReader_)
		{
			framework_.Bootstrapper.EnableConfigEnvironmentVariableExtractor(configurationReader_, variableDetailsConfigurationReader_);
		}

		public static void ExecuteConfigEnvironmentVariableExtraction(IVersionConfigurationReader configurationReader_,
                                                                      IEnvironmentVariableDetailsConfigurationReader variableDetailsConfigurationReader_)
		{
			new Framework().Bootstrapper.EnableConfigEnvironmentVariableExtractor(configurationReader_, variableDetailsConfigurationReader_);
		}

		public static void ExecuteConfigEnvironmentVariableExtraction()
		{
			new Framework().Bootstrapper.EnableConfigEnvironmentVariableExtractor();
		}

		/// <summary>
		/// Sets the name of the concord log folder.
		/// </summary>
		/// <param name="framework_">The framework.</param>
		/// <param name="folderName_">The folder name.</param>
		public static void SetConcordLogFolderName(this Framework framework_, string folderName_)
		{
			ConcordExtensionStorage.LogFolderName = folderName_;
		}


		public static IViewContainer GetViewContainer(this Form form_)
		{
			Label WindowGuidStore = form_.Controls.Find("WindowGuidStore", true).OfType<Label>().FirstOrDefault();
			if (WindowGuidStore != null &&
					ConcordExtensionStorage.ViewContainerByWindowGuidStore.ContainsKey(WindowGuidStore.Tag.ToString()))
			{
				string id = ConcordExtensionStorage.ViewContainerByWindowGuidStore[WindowGuidStore.Tag.ToString()];
				IViewContainer result =
					ConcordExtensionHelperModule.viewManager.Views.FirstOrDefault(vc => vc.ID == id);
                
				if (result == null)
				{
					m_logger.Error(string.Format("View with ID #{0} not found.", id));
				}

				return result;
			}

            return null;
		}

		public static IWindowViewContainer GetWindowViewContainer(this Form form_)
		{
		    return form_.GetViewContainer() as IWindowViewContainer;
		}

        public static void SetWindowViewContainerInitializer(this Form form, WindowViewContainerInitializer initializer)
        {
            var currentInitializerStore = form.Controls.Find("WindowInitializerGuidStore", true).OfType<Label>().FirstOrDefault();
            if (currentInitializerStore != null) throw new Exception("The form already has an initializer");

            var initializerId = Guid.NewGuid().ToString();
            var initializerGuidStore = new Label
                {
                    Tag = initializerId,
                    Name = "WindowInitializerGuidStore",
                    Visible = false
                };
            form.Controls.Add(initializerGuidStore);
            ConcordExtensionStorage.ViewContainerInitializers[initializerId] = initializer;
        }

		#region Nested type: ConcordExtensionHelperModule

        private class ConcordExtensionHelperModule : IMSDesktopModule
		{
			internal static IViewManager viewManager;

			public ConcordExtensionHelperModule(IViewManager vm)
			{
				viewManager = vm;
			}

			#region IModule Members

			public void Initialize()
			{
			}

			#endregion
		}

		#endregion

		public static void EnableWindowsFormHostRestyleOptions(this Framework framework_, bool enableMargin = false, bool enableFontSize = false, bool enableFontFamily = false, bool enableOnlyOnWin7 = true)
		{
			if (!enableOnlyOnWin7 || TaskDialog.Supported)
			{
				AppletFormHost.SetMargin = enableMargin;
				AppletFormHost.SetFontSize = enableFontSize;
				AppletFormHost.SetFontFamily = enableFontFamily;
			}
		}

        public static void EnableWindowsFormHostRestyleOptions(this Framework framework_, bool enableMargin, bool enableFontSize, bool enableFontFamily, bool enableOnlyOnWin7, double fontSize)
        {
            if (!enableOnlyOnWin7 || TaskDialog.Supported)
            {
                AppletFormHost.SetMargin = enableMargin;
                AppletFormHost.SetFontSize = enableFontSize;
                AppletFormHost.SetFontFamily = enableFontFamily;
                AppletFormHost.DefaultFontSize = fontSize * (96.0 / 72.0); // see http://msdn.microsoft.com/en-us/library/ms751565(v=vs.110).aspx
            }
        }

        public static void EnableSuppressLogSetupForConcord(this Framework framework_)
        {
            framework_.EnableSuppressLogSetupForConcord(false);
        }

        public static void EnableSuppressLogSetupForConcord(this Framework framework, bool force)
        {
            ConcordApplication.EnableSuppressLogSetup = true;
            ConcordApplication.EnableForceSuppressLogSetup = force;
        }
        
        public static void EnableActivateConcordWindowWhenOpenedFromMenu(this Framework framework_)
        {
            ConcordWindowManager.ActivateConcordWindowWhenOpenedFromMenu = true;
        }
        public class WindowViewContainerInitializer
        {
            private IWindowViewContainer windowViewContainer;
            public IWindowViewContainer WindowViewContainer
            {
                get
                {
                    return windowViewContainer;
                }
                internal set
                {
                    Debug.Assert(windowViewContainer == null);
                    windowViewContainer = value;
                    
                    // notify client that the container has been created
                    OnWindowViewContainerCreated();
                }
            }

            public event EventHandler WindowViewContainerCreated;

            private void OnWindowViewContainerCreated()
            {
                var handler = WindowViewContainerCreated;
                if (handler != null) handler(this, EventArgs.Empty);
            }
        }
	}
}