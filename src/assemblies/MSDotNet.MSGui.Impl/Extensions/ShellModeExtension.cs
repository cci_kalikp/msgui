﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Extensions/ShellModeExtension.cs#49 $
// $Change: 902256 $
// $DateTime: 2014/10/23 11:14:14 $
// $Author: caijin $

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using Microsoft.WindowsAPICodePack.Taskbar;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.UserPreference;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    /// <summary>
    /// This is only for backwards compatibility.
    /// </summary>
    [Flags]
    [Obsolete("Please use HideLockUIGranularity from Core assembly instead")]
    public enum HideUIGranularity
    {
        None = 0,
        HideViewMinimizeButton = 1 << 0,
        HideViewMaximizeButton = 1 << 1,
        HideViewTearOffButton = 1 << 2,
        HideViewPinButton = 1 << 3,
        LockViewDragDrop = 1 << 4,
        HideRibbon = 1 << 5,
        HideLauncherBar = 1 << 6,
        HideStatusBar = 1 << 7,
        DontAppendProfileToWindowTitle = 1 << 8,
        DisableViewFloatingOnly = 1 << 9,
        HideNewTabButton = 1 << 10,
        DisableTabwellContextMenu = 1 << 11,
        HideViewCloseButton = 1 << 12,
        DisableViewHeaderContextMenuClose = 1 << 13,
        DisableSubtabContextMenuMove = 1 << 14,
        DisableViewHeaderContextMenuRename = 1 << 15,
        DisableViewTearOff = (1 << 16) | HideViewTearOffButton,
        DisableViewMaximize = (1 << 17) | HideViewMaximizeButton,
        DisableTabClose = 1 << 18,
        DisableTabMove = 1 << 19,
        DisableTabRename = 1 << 20,
        HideMainTabwell = 1 << 21,
        DisableViewDocking = 1 << 22,
        HideHeaderIcon = 1 << 23,
        HideEnvironmentLabelling = 1 << 24,
        DisableViewClose = DisableViewHeaderContextMenuClose | HideViewCloseButton,
        /// <summary>
        /// Reflects the obsolete SetLockedLayoutMode setting.
        /// </summary>
        LegacyLockedLayoutMode = HideViewMaximizeButton |
                            HideViewMinimizeButton |
                            HideViewPinButton |
                            HideViewTearOffButton |
                            LockViewDragDrop |
                            HideRibbon |
                            HideLauncherBar |
                            DontAppendProfileToWindowTitle |
                            DisableViewFloatingOnly |
                            HideNewTabButton |
                            DisableTabwellContextMenu |
                            HideViewCloseButton,
        /// <summary>
        /// Completely locked layout. A default layout file should be provided.
        /// </summary>
        LockedLayoutMode = (1 << 23) - 1, // everything apart from HideMainTabwell
        /// <summary>
        /// Allows the user to create and close views. Initial location should be provided for views.
        /// </summary>
        LockedLayoutSoftMode = LockedLayoutMode
            - DisableViewClose
            - HideRibbon
            - HideLauncherBar
            - HideStatusBar
    }

	public static class ShellModeExtension
	{
	    internal static HideLockUIGranularity HideUIGranularity;

        internal static Visibility GetVisibilityForElement(HideLockUIGranularity element)
        {
            return HideUIGranularity.HasFlag(element) ? Visibility.Collapsed : Visibility.Visible;
        }

        internal static WindowButtonState GetStateForElement(HideLockUIGranularity element)
        {
            return HideUIGranularity.HasFlag(element) ? WindowButtonState.None : WindowButtonState.Normal;
        }
        public static void HideOrLockShellElements(this Framework framework, HideLockUIGranularity granularity)
        {
            HideUIGranularity = granularity;
        }

        [Obsolete("Please use HideLockUIGranularity from Core assembly instead")]
        public static void HideOrLockShellElements(this Framework framework, HideUIGranularity granularity)
        {
            HideUIGranularity = (HideLockUIGranularity) granularity;
        }
        
        /// <summary>
        /// Specify which shell mode to use
        /// </summary>
        /// <param name="framework_"></param>
        /// <param name="mode_"></param>
        /// <param name="useHarmonia_"></param>
		public static void SetShellMode(this Framework framework_, ShellMode mode_, bool useHarmonia_=false)
		{
            if (mode_ == ShellMode.LauncherBarAndWindow)
            {
                ExtensionsUtils.RequireAssembly(ExternalAssembly.Interactivity);
            }
			framework_.InitialSettings.ShellMode = mode_; 
            if (mode_ == ShellMode.LauncherBarAndFloatingWindows || 
                mode_ == ShellMode.RibbonAndFloatingWindows)
            {
                DockViewModel.UseHarmonia = useHarmonia_;
            }
		}

		public static void EnableLegacyControlPlacement(this Framework framework_)
		{
			framework_.InitialSettings.LegacyControlPlacementEnabled = true;
		}

        [Obsolete("Use HideOrLockShellElements extension method instead")]
		public static void SetLockedLayoutMode(this Framework framework_)
		{
		    framework_.SetShellMode(ShellMode.RibbonWindow);
            HideUIGranularity = HideLockUIGranularity.LegacyLockedLayoutMode;
		}

        [Obsolete("Use HideOrLockShellElements extension method instead")]
        public static void HideRibbon(this Framework framework_)
		{
            HideUIGranularity |= HideLockUIGranularity.HideRibbon;
		}

        [Obsolete("Use HideOrLockShellElements extension method instead")]
        public static void HideLauncherBar(this Framework framework_)
        {
            HideUIGranularity |= HideLockUIGranularity.HideLauncherBar;
        }

        [Obsolete("Use HideOrLockShellElements extension method instead")]
		public static void HideStatusBar(this Framework framework_)
		{
		    HideUIGranularity |= HideLockUIGranularity.HideStatusBar;
		}

        [Obsolete("Use HideOrLockShellElements extension method instead")]
        public static void HideMainTabwell(this Framework framework_)
        {
            HideUIGranularity |= HideLockUIGranularity.HideMainTabwell;
        }

		/// <summary>
		/// Hide main ribbon window or launcher. Application should either have a provided default layout having floating windows or using ITrayIconViewContainer
		/// </summary>
		/// <param name="framework_"></param>
		public static void SetInvisibleMainControllerMode(this Framework framework_)
		{
			framework_.SetInvisibleMainControllerMode(InvisibleMainControllerModeEnum.HideWindow);
		}

        /// <summary>
        /// Hide main ribbon window or launcher. Application should either have a provided default layout having floating windows or using ITrayIconViewContainer
        /// </summary>
        /// <param name="framework_"></param>
        public static void SetInvisibleMainControllerMode(this Framework framework_, InvisibleMainControllerModeEnum mode_)
        {
            InvisibleMainControllerMode = mode_;
        }

        internal static InvisibleMainControllerModeEnum InvisibleMainControllerMode { get; set; }

        [Flags]
        public enum InvisibleMainControllerModeEnum
        {
            None = 0,
            HideWindow = 1 << 0, 
            MoveWindow = 1 << 1,
            DontShowWindow = 1 << 2,
        }

		public static void EnableRemoveEmptyTabsOnLayoutLoad(this Framework framework_)
		{
			TabbedDockViewModel.RemoveEmptyTabsOnLayoutLoad = true;
		}

		/// <summary>
		/// Enables automatically removing of empty tabs.
		/// </summary>
		/// <param name="framework_"></param>
		/// <param name="afterLayoutLoad">If true, all empty tabs will be removed after a layout has been loaded.</param>
		/// <param name="afterLastChildClosed">If true, tabs will be removed if their last child window is closed</param>
		public static void EnableRemoveEmptyTabs(this Framework framework_, bool afterLayoutLoad, bool afterLastChildClosed)
		{
			TabbedDockViewModel.RemoveEmptyTabsOnLayoutLoad = afterLayoutLoad;
			TabbedDockViewModel.RemoveEmptyTabsOnLastChildClosed = afterLastChildClosed;
		}

		/// <summary>
		/// Sets the main window's title.
		/// </summary>
		/// <param name="chromeManager"></param>
		/// <param name="title">The new title to use. Null restores original behaviour.</param>
		/// <param name="appendProfile">If true, the current profile name will be appended to the title.</param>
		public static void SetMainWindowTitle(this IChromeManager chromeManager, string title, bool appendProfile)
		{
            if (!(chromeManager is RibbonChromeManager)) return;
            if (!appendProfile)
            {
                HideUIGranularity |= HideLockUIGranularity.DontAppendProfileToWindowTitle;            
            }
			ShellWindow.SetTitleBase(title);
		}
         
        public static readonly DependencyProperty TitleBarColourProperty =
            DependencyProperty.RegisterAttached("TitleBarColour", typeof(Color?), typeof(ShellModeExtension), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// Sets the main window's titlebar color. Null reverts to default.
        /// </summary>
        /// <param name="chromeManager_"></param>
        /// <param name="color_"></param>
        public static void SetMainWindowTitleBarColour(this IChromeManager chromeManager_, Color? color_)
        {
            var shell = ((ChromeManagerBase) chromeManager_).Shell as ShellWithRibbon;
            if (shell == null) return;
            shell.ShellWindow.ribbon.SetValue(TitleBarColourProperty, color_); 
        }

        public static void ToggleOverlayMode(this IChromeManager chromeManager_, bool enable_, 
            Brush brush_ = null, Func<FrameworkElement> contentFactory_ = null)
        {
            var shell = ((ChromeManagerBase)chromeManager_).Shell as ShellWithRibbon;
            if (shell == null) return;
            var shellWindow = shell.ShellWindow;
            var windowManager = (TabbedDockViewModel)shell.WindowManager;
            if (enable_)
            {
                shellWindow.ToggleOverlay(true, brush_, contentFactory_);
                windowManager.ToggleOverlayMode(true, brush_, contentFactory_);
            }
            else
            {
                shellWindow.ToggleOverlay(false);
                windowManager.ToggleOverlayMode(false, brush_, contentFactory_);
            }
        }

        internal static bool enableParallelBasedEventing = true;

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void DisableParallelBasedEventing(this Framework framework)
        {
            enableParallelBasedEventing = false;
        }

        public static void DisableRenameTab(this Framework framework, bool isDisabled)
        {
            DockViewModel.RenameDisabled = isDisabled;
            TabItemMenu.RenameDisabled = isDisabled;
        }

        public static void EnableCompactTabGroups(this Framework framework, bool isEnabled)
        {
            CustomContentPane.CompactTabGroupMode = isEnabled;
        }

	    internal static bool ContentHostModeEnabled = false;

        /// <summary>
        /// If enabled, tabbed panes created with InitialLocationTarget==null will be placed 
        /// inside a DocumentContentHost which always resides in the center of the layout.
        /// Other docked panes will be located relative to the DocumentContentHost.
        /// 
        /// Ignored when the layout is other then ShellWithRibbon or LauncherWithWindow.
        /// </summary>
        public static void EnableContentHostMode(this Framework framework_)
        {
            ContentHostModeEnabled = true;
        }

	    internal static bool NoTaskbarPin;

        public static void EnableNoTaskbarPin(this Framework framework_)
        {
            ExtensionsUtils.RequireAssembly(ExternalAssembly.WindowsApiCodePackShell);

            try
            {
                if (TaskbarManager.IsPlatformSupported)
                {
                    NoTaskbarPin = true;
                    //WindowProperties.SetWindowProperty(this, SystemProperties.System.AppUserModel.PreventPinning, "1");
                }

            }
            catch
            {
            }

        }

	    public static void SetRibbonIcon(this Framework framework_, ImageSource icon_)
	    {
	        framework_.Bootstrapper.SetRibbonIcon(icon_);
	    }

	    internal static bool MenuPopupAnimationDisabled;
        public static void DisableMenuPopupAnimation(this Framework framework_)
        {
            MenuPopupAnimationDisabled = true;
        }

        public static void SetConfigurationDependentShellMode(this Framework framework_, ShellMode defaultMode_, 
            bool shellModeUserConfigurable_,  bool useHarmnia_=false, params ShellMode[] availableShellModes_)
        {
            framework_.Bootstrapper.ConfigurationDependentShellMode = true;
            Bootstrapper.ShellModeUserConfigurable = shellModeUserConfigurable_;
            UserPreferenceInitialSettings.DefaultShellMode = defaultMode_;
            if (availableShellModes_ != null && availableShellModes_.Length > 0)
            {
                UserPreferenceInitialSettings.AvailableShellModes = availableShellModes_;
            }
            framework_.InitialSettings.ShellMode = defaultMode_;
            DockViewModel.UseHarmonia = useHarmnia_;
        } 
	}

    public enum ShellMode
    {
        RibbonWindow = 0,
        LauncherBarAndFloatingWindows = 1,
        LauncherBarAndWindow = 2,
        External = 3,
        RibbonMDI = 4,
        RibbonAndFloatingWindows = 5,  
    };
}
