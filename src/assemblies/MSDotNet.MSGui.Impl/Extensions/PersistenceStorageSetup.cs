﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Extensions/PersistenceStorageSetup.cs#9 $
// $Change: 870088 $
// $DateTime: 2014/03/07 05:58:28 $
// $Author: milosp $


using System;
using System.ComponentModel;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.SmartApi;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    public static class PersistenceStorageSetup
    {
        public static void SetupPersistenceStorage(this Framework framework, IPersistenceStorage storage)
        {
            if (
                !((storage is FilePersistenceStorage) || (storage is MultipleFilePersistenceStorage) ||
                  (storage is ConcordPersistenceStorage)))
            {
                throw new ArgumentOutOfRangeException("storage", storage.GetType().Name,
                                                      "Use Concord.Config instead of creating your own IPersistenceStorage");
            }

            if (!(storage is ConcordPersistenceStorage) && !SuppressExtraAR)
            {
                framework.EnableSmartApi();
            }

            framework.Bootstrapper.SetPersistenceStorage(storage);
        }

        internal static bool SuppressExtraAR;

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void EnableSuppressExtraAR(this Framework framework)
        {
            SuppressExtraAR = true;
        }

        public static void EnableCallNullPersistence(this Framework framework)
        {
            PersistenceService.CallNullPersistence = true;
        }
    }
}
