﻿using System.Collections.Specialized;
using System.Configuration;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions.EnvironmentExtractor
{
    public static class EnvironmentVariableConfigurationReader
    {
        private const string AppSettingsName = "environmentVariablesToAdd";

        public static EnvironmentVariables Read()
        {
            var environmentVariableDetails = ConfigurationManager.GetSection(AppSettingsName) as NameValueCollection;
            
            var environmentVariables = new EnvironmentVariables();

            if (environmentVariableDetails != null)
            {
                foreach (string environmentVariableName in environmentVariableDetails.AllKeys)
                {
                    string environmentVariableValue = environmentVariableDetails[environmentVariableName];
                    
                    environmentVariables.AddVariable(environmentVariableName, environmentVariableValue);
                }
            }

            return environmentVariables;
        }
    }
}