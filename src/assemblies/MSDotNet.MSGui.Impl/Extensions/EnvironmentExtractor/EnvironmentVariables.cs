using System.Collections;
using System.Collections.Generic;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions.EnvironmentExtractor
{
    public class EnvironmentVariables : IEnumerable<EnvironmentVariable>
    {
        private readonly List<EnvironmentVariable> _environmentVariables = new List<EnvironmentVariable>();

        public void AddVariable(string name_, string value_)
        {
            var environmentVariable = new EnvironmentVariable(name_, value_);
            environmentVariable.Validate();

            _environmentVariables.Add(environmentVariable);
        }

        public void RegisterVariablesInSystem()
        {
            foreach (EnvironmentVariable environmentVariable in _environmentVariables)
            {
                environmentVariable.RegisterVariableInSystem();
            }
        }

        public IEnumerator<EnvironmentVariable> GetEnumerator()
        {
            return _environmentVariables.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}