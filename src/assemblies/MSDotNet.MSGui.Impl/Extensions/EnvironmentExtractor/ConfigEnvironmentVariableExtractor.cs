﻿using System.Collections.Generic;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions.EnvironmentExtractor
{
    public class ConfigEnvironmentVariableExtractor
    {
        private readonly IVersionConfigurationReader _versionConfigurationReader;
        private readonly IEnvironmentVariableDetailsConfigurationReader _environmentVariableDetailsConfigurationReader;

        public ConfigEnvironmentVariableExtractor(
            IVersionConfigurationReader versionConfigurationReader_,
            IEnvironmentVariableDetailsConfigurationReader environmentVariableDetailsConfigurationReader_)
        {
            _versionConfigurationReader = versionConfigurationReader_;
            _environmentVariableDetailsConfigurationReader = environmentVariableDetailsConfigurationReader_;
        }

        public EnvironmentVariables Read()
        {
            IEnumerable<EnvironmentVariableDetails> allEnvironmentVariableDetails =
                _environmentVariableDetailsConfigurationReader.Read();

            Dictionary<string, string> versionConfiguration = _versionConfigurationReader.Read(allEnvironmentVariableDetails);

            var environmentVariables = new EnvironmentVariables();

            foreach (KeyValuePair<string, string> versionNameAndValue in versionConfiguration)
            {
                environmentVariables.AddVariable(versionNameAndValue.Key, versionNameAndValue.Value);
            }

            return environmentVariables;
        }
    }
}