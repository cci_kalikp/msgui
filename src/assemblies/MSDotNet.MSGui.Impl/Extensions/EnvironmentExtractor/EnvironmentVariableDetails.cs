﻿namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions.EnvironmentExtractor
{
    public class EnvironmentVariableDetails
    {
        public string Name { get; private set; }
        public string PathPrefix { get; private set; }

        public EnvironmentVariableDetails(string name_, string pathPrefix_)
        {
            Name = name_;
            PathPrefix = pathPrefix_;
        }
    }
}