﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions.EnvironmentExtractor
{
    public class EnvironmentVariableDetailsConfigurationReader : IEnvironmentVariableDetailsConfigurationReader
    {
        private const string AppSettingsName = "environmentVariablesToAdd";

        public IEnumerable<EnvironmentVariableDetails> Read()
        {
            var allEnvironmentVariableDetails = new List<EnvironmentVariableDetails>();

            var environmentVariableDetails = ConfigurationManager.GetSection(AppSettingsName) as NameValueCollection;
            if (environmentVariableDetails != null)
            {
                foreach (string environmentVariableName in environmentVariableDetails.AllKeys)
                {
                    string environmentVariableValue = environmentVariableDetails[environmentVariableName];

                    var variableDetails
                        = new EnvironmentVariableDetails(name_: environmentVariableName, pathPrefix_: environmentVariableValue);

                    allEnvironmentVariableDetails.Add(variableDetails);
                }
            }

            return allEnvironmentVariableDetails;
        }
    }
}