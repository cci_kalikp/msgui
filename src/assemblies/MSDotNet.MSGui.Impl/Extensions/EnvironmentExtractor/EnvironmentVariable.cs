﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions.EnvironmentExtractor
{
    public class EnvironmentVariable
    {
        public EnvironmentVariable(string name_, string value_)
        {
            Name = name_;
            Value = value_;
        }

        public string Name { get; set; }
        public string Value { get; set; }

        public void RegisterVariableInSystem()
        {
            System.Environment.SetEnvironmentVariable(Name, Value, EnvironmentVariableTarget.Process);
        }

        public override string ToString()
        {
            return string.Format("EnvironmentVariable - {0} = {1}", Name, Value);
        }

        public void Validate()
        {
            if (hasUppercaseCharacters(Name))
            {
                const string message = "Ensure these environment variable names are all lowercase as any application instance started with ProcessStartInfo (such as dotTrace) will set these to lowercase and there is a issue in a later case-sensitive MS class which will fail when the case of the variable names do not match";
                throw new Exception(message);
            }

            if (string.IsNullOrEmpty(Value))
            {
                throw new Exception("Environment variable " + Name + " has no value");
            }
        }

        private static bool hasUppercaseCharacters(string stringToCheck_)
        {
          if (stringToCheck_.ToLower() == stringToCheck_)
          {
            return false;
          }

          return true;
        }

    }
}