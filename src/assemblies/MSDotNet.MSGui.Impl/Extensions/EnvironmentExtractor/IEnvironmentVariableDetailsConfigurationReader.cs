﻿using System.Collections.Generic;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions.EnvironmentExtractor
{
    public interface IEnvironmentVariableDetailsConfigurationReader
    {
        IEnumerable<EnvironmentVariableDetails> Read();
    }
}