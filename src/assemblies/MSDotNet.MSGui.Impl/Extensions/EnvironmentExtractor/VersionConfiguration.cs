﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions.EnvironmentExtractor
{
    public class VersionConfiguration
    {
        public string T2Version { get; set; }
        public string TxVersion { get; set; }

        public VersionConfiguration(string t2Version_, string txVersion_)
        {
            T2Version = t2Version_;
            TxVersion = txVersion_;
        }

        public void Validate()
        {
            if (string.IsNullOrEmpty(T2Version))
            {
                throw  new Exception("T2 Version invalid");
            }

            if (string.IsNullOrEmpty(TxVersion))
            {
                throw  new Exception("Tx Version invalid");
            }
        }
    }
}