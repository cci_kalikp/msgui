﻿using MorganStanley.MSDotNet.MSGui.Impl.Helpers;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions.EnvironmentExtractor
{
    public class MsdeConfigFinder : IConfigFinder
    {
        public string GetConfigPath()
        {
            return EntryAssemblyHelper.GetEntryAssembly().Location + ".msde.config";
        }
    }
}