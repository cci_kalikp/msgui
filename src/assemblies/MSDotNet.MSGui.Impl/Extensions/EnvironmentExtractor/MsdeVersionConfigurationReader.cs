﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions.EnvironmentExtractor
{
    public class MsdeVersionConfigurationReader : IVersionConfigurationReader
    {
        private readonly IConfigFinder _configFinder;

        public MsdeVersionConfigurationReader(IConfigFinder configFinder_)
        {
            _configFinder = configFinder_;
        }

        public Dictionary<string, string> Read(IEnumerable<EnvironmentVariableDetails> allEnvironmentVariableDetails_)
        {
            IEnumerable<string> includeFilePaths = GetIncludeFilePaths();
            var environmentVariableDetails = new Dictionary<string, string>();

            foreach (EnvironmentVariableDetails environmentVariableDetail in allEnvironmentVariableDetails_)
            {
                string version = GetVersion(includeFilePaths, environmentVariableDetail.PathPrefix);
                environmentVariableDetails.Add(environmentVariableDetail.Name, version);
            }
            
            return environmentVariableDetails;
        }

        private IEnumerable<string> GetIncludeFilePaths()
        {
            XmlDocument msdeConfigDocument = LoadMsdeConfigDocument();
            
            var namespaceManager = new XmlNamespaceManager(msdeConfigDocument.NameTable);
            namespaceManager.AddNamespace("msde", "http://xml.ms.com/eai/msde/dotnet/dependency/config/2.0");

            XmlNodeList includeFileElements = msdeConfigDocument.SelectNodes(@"//msde:IncludeFile", namespaceManager);
            var includeFileNodes = includeFileElements.Cast<XmlNode>();

            return includeFileNodes.Select(x_ => x_.Attributes["Name"].Value);
        }

        private XmlDocument LoadMsdeConfigDocument()
        {
            string msdeConfigPath = _configFinder.GetConfigPath();
            var msdeConfigDocument = new XmlDocument();
            msdeConfigDocument.Load(msdeConfigPath);
            return msdeConfigDocument;
        }

        private static string GetVersion(IEnumerable<string> includeFilePaths_, string startOfPathAttributeValue_)
        {
            string versionPath = includeFilePaths_.Single(x_ => x_.StartsWith(startOfPathAttributeValue_));
            string versionPartialPath = versionPath.Replace(startOfPathAttributeValue_, string.Empty);
            string version = versionPartialPath.Substring(0, versionPartialPath.IndexOf(@"\"));
            return version;
        }
    }
}