﻿using System.Collections.Generic;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions.EnvironmentExtractor
{
    public interface IVersionConfigurationReader
    {
        Dictionary<string, string> Read(IEnumerable<EnvironmentVariableDetails> allEnvironmentVariableDetails_);
    }
}