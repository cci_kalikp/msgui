﻿namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions.EnvironmentExtractor
{
    public interface IConfigFinder
    {
        string GetConfigPath();
    }
}