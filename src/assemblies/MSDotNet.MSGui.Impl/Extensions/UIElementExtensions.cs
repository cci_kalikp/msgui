﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection; 
using System.Windows; 
using System.Windows.Forms.Integration;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.ExceptionHandling; 
using PixelFormat = System.Drawing.Imaging.PixelFormat;
using Size = System.Windows.Size;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    internal static class UIElementExtensions
    {
        private const int IMAGE_DPI = 96;

        private delegate Image GetImage(UIElement control, bool shootActualSize);
        private static readonly GetImage GetContentScreenshot = GetScreenShot;
        static readonly List<Type> windowsTypeHandled = new List<Type>();
        private static readonly List<CoerceValueCallback> updatedCallbacks = new List<CoerceValueCallback>(); 

        #region Take screenshot before the window is minimized
        public static void EnableMinimizeScreenshotSupport(this Window win_)
        {
            if (!ApplicationUnhandledExceptions.AttachMinimizedScreens) return;
            lock (windowsTypeHandled)
            {
                if (!windowsTypeHandled.Contains(win_.GetType()))
                { 
                    var propertyMetadata = Window.WindowStateProperty.GetMetadata(win_.GetType());
                    if (propertyMetadata != null)
                    {
                        var oldCallback = propertyMetadata.CoerceValueCallback;
                        if (oldCallback == null || !updatedCallbacks.Contains(oldCallback))
                        {
                            var fieldInfo = typeof(PropertyMetadata).GetField("_coerceValueCallback",
                            BindingFlags.FlattenHierarchy | BindingFlags.Instance | BindingFlags.NonPublic);
                            if (fieldInfo != null)
                            {
                                var newCallback = new CoerceValueCallback((o_, value_) =>
                                    {
                                        value_ = oldCallback == null ? value_ : oldCallback(o_, value_);
                                        return CoerceWindowState(o_, value_);
                                    });
                                fieldInfo.SetValue(propertyMetadata, newCallback);
                                updatedCallbacks.Add(newCallback);
                            }
                        }
 
                    }
                    else
                    {
                        propertyMetadata = new FrameworkPropertyMetadata(WindowState.Normal, null, CoerceWindowState);
                        Window.WindowStateProperty.OverrideMetadata(win_.GetType(), propertyMetadata);
                        updatedCallbacks.Add(CoerceWindowState);
                    }
                    windowsTypeHandled.Add(win_.GetType()); 
                } 
            }

            win_.Closed -= win__Closed;
            win_.Closed += win__Closed;
            IntPtr handle = (new WindowInteropHelper(win_)).Handle;
            if (handle.ToInt64() == 0)
            {
                win_.SourceInitialized -= win__SourceInitialized;
                win_.SourceInitialized += win__SourceInitialized;
            }
            else
            {
                HwndSource source = HwndSource.FromHwnd(handle);
                if (source != null)
                {
                    source.RemoveHook(WmMinimizeWindow);
                    source.AddHook(WmMinimizeWindow); 
                } 
            }
        }


        private static object CoerceWindowState(DependencyObject dependencyObject_, object baseValue_)
        {
            if ((WindowState)baseValue_ == WindowState.Minimized)
            {
                try
                {
                    //add the screenshot when the window is minimized by setting WindowState directly
                    if (!elementsToSkip.Remove((Window)dependencyObject_))
                    {
                        ((Window)dependencyObject_).AddScreenShot(true);
                    }

                }
                catch
                {
                }
            }
            return baseValue_;
        }

        static void win__SourceInitialized(object sender, EventArgs e)
        {
            IntPtr handle = (new WindowInteropHelper((Window)sender)).Handle;
            HwndSource source = HwndSource.FromHwnd(handle);
            if (source != null)
            {
                source.RemoveHook(WmMinimizeWindow);
                source.AddHook(WmMinimizeWindow);
            } 
            ((Window)sender).SourceInitialized -= win__SourceInitialized;
        }

        static void win__Closed(object sender, EventArgs e)
        {
            Window window = (Window)sender;
            window.RemoveScreenShot();
            window.Closed -= win__Closed;
        }

        private static IntPtr WmMinimizeWindow(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch ((uint)msg)
            { 
                case Win32.WM_SYSCOMMAND:
                    // minimize the window through minimize command, 
                    // when the window state is set to Minimize, it's too late to take a good screenshot
                    if ((int)wParam == Win32.SC_MINIMIZE) 
                    {
                        AddScreenShot(hwnd);
                    }
                    break;
                case Win32.WM_SHOWWINDOW:
                    // hide the window because parent is minimized
                    //in this case, the window state won't be set to minimized
                    if ((int)wParam == 0 && (int)lParam == Win32.SW_PARENTCLOSING) 
                    {
                        AddScreenShot(hwnd);
                    }
                    break; 

            }
            return IntPtr.Zero;
        }

        private static List<UIElement> elementsToSkip = new List<UIElement>();  
        private static void AddScreenShot(IntPtr hwnd)
        {
            try
            {
                var source = HwndSource.FromHwnd(hwnd);
                if (source != null)
                {
                    Window window = source.RootVisual as Window;
                    if (window != null && window.WindowState != WindowState.Minimized)
                    {
                        window.AddScreenShot(true); 
                    }
                } 

            }
            catch  
            { 
            }
        }


        private static void RemoveScreenShot(this UIElement source)
        {
            savedActualSizeScreenShots.Remove(source);
            savedDesiredSizeScreenShots.Remove(source);
            elementsToSkip.Remove(source);
        }
        #endregion

        /// <summary>
        /// Returns an Image representation of the rendering of the specified UI element.
        /// </summary>
        /// <param name="element">UI element to get screenshot of</param>
        /// <param name="shootActualSize">When true, takes a shot of element's actual size, otherwise its desired size</param>
        /// <returns>Image object containing the requested snapshot</returns>
        public static Image GetScreenShotSync(this UIElement element, bool shootActualSize)
        {
            var res = element.Dispatcher.BeginInvoke(GetContentScreenshot, new object[] { element, shootActualSize });
            res.Wait();

            return res.Result as Image;
        }

        public static byte[] GetScreenShotBytes(this UIElement source, bool shootActualSize)
        {
            byte[] result;

            Window w = Window.GetWindow(source);
            if (w != null && w.WindowState == WindowState.Minimized)
            { 
                if (shootActualSize)
                {
                    savedActualSizeScreenShots.TryGetValue(w, out result);
                }
                else
                {
                    savedDesiredSizeScreenShots.TryGetValue(w, out result);
                }
                return result;
            }

            using (var stream = new MemoryStream())
            {
                GetScreenShot(source, stream, shootActualSize);
                stream.Flush();
                result = stream.GetBuffer();
            }
            return result;

        }

        private static Image GetScreenShot(UIElement source, bool shootActualSize)
        {
            using (var stream = new MemoryStream())
            {
                GetScreenShot(source, stream, shootActualSize);
                return Image.FromStream(stream);
            }
        }

        private static Size RetrieveDesiredSize<T>(T control) where T : UIElement
        {
            var size = control.DesiredSize;
            if (Equals(size.Width, double.NaN) || Equals(size.Height, double.NaN))
            {
                //Make sure the control has measured first:
                control.Measure(new Size(double.MaxValue, double.MaxValue));

                //default size, large enough to include all 
                return control.DesiredSize;
            }

            return new Size(size.Width, size.Height);
        }

        private static Dictionary<UIElement,byte[]> savedActualSizeScreenShots = new Dictionary<UIElement, byte[]>();
        private static Dictionary<UIElement, byte[]> savedDesiredSizeScreenShots = new Dictionary<UIElement, byte[]>();
        internal static void AddScreenShot(this Window source, bool shootActualSize)
        {
            IFloatingWindow paneToolWindow = source.Content as IFloatingWindow;
           if (shootActualSize)
           {
               savedActualSizeScreenShots[source] = GetScreenShotBytes((paneToolWindow as UIElement)?? source, true);
           }
           else
           {
               savedDesiredSizeScreenShots[source] = GetScreenShotBytes((paneToolWindow as UIElement) ?? source, false);
           }
        }


        private static void GetScreenShot(UIElement source, Stream stream, bool shootActualSize)
        { 
            bool renderByGDI = source.Visibility == Visibility.Visible && 
                (source is WindowsFormsHost || source.FindLogicalChildren<WindowsFormsHost>().Any());
            //if has windowsformcontrol, switch to gdi printwindow
            if (renderByGDI)
            {
                CaptureScreen(source as FrameworkElement, stream);
            }
            else
            {
                var size = shootActualSize
            ? source.RenderSize
            : RetrieveDesiredSize(source as FrameworkElement);

                if (size.Width <= 0 || size.Height <= 0 || CoreUtilities.AreClose(size.Width, double.MaxValue) || CoreUtilities.AreClose(size.Height, double.MaxValue))
                {
                    size.Width = 100;
                    size.Height = 100;
                }

                var renderTarget = new RenderTargetBitmap((int)size.Width, (int)size.Height, IMAGE_DPI, IMAGE_DPI,
                                                          PixelFormats.Pbgra32);
                renderTarget.Render(source);

                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(renderTarget));
                encoder.Save(stream); 
            }

        }

 

        public static void CaptureScreen(FrameworkElement element, Stream stream)
        {
            bool needsCrop = false;
            Window w = element as Window;
            if (w != null)
            {
                if (w.Content is IFloatingWindow)
                {
                    element = w.Content as FrameworkElement;
                    needsCrop = true;
                }
            }
            else
            {
                w = Window.GetWindow(element);
                needsCrop = true;
            } 

            if (w == null) return;
            var rc = new Win32.RECT();
            var hwnd = new WindowInteropHelper(w).Handle;
            Win32.GetWindowRect(hwnd, ref rc);
            int wWidth = rc.GetLength();
            int wHeight = rc.GetHeight();
            using (var bmp = new Bitmap(wWidth, wHeight, PixelFormat.Format32bppArgb))
            {
                using (var gfxBmp = Graphics.FromImage(bmp))
                {
                    var hdcBitmap = gfxBmp.GetHdc();
                    Win32.PrintWindow(hwnd, hdcBitmap, 0);
                    gfxBmp.ReleaseHdc(hdcBitmap);
                }
                
                if (needsCrop)
                {
                    var location = element.TranslatePoint(new System.Windows.Point(0, 0), w);
                    int left = location.X < 0 ? 0 : (int)location.X;
                    int top = location.Y < 0 ? 0 : (int)location.Y;
                    int width = element.ActualWidth > wWidth - left
                                    ? wWidth - left : (int)element.ActualWidth;
                    int height = element.ActualHeight > wHeight - top
                                    ? wHeight - top : (int)element.ActualHeight;

                    Rectangle cropArea = new Rectangle(left, top, width, height);
                    using (Bitmap bmp2 = bmp.Clone(cropArea, bmp.PixelFormat))
                    {
                        bmp2.Save(stream, ImageFormat.Png);
                    }
                }
                else
                {
                    bmp.Save(stream, ImageFormat.Png);
                }

            }  

        } 

    }
}
