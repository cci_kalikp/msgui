﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Extensions/ThemeExtensions.cs#29 $
// $Change: 902331 $
// $DateTime: 2014/10/24 05:43:11 $
// $Author: caijin $

using System;
using System.Windows;
using System.Windows.Media;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Controls.Controls;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
  /// <summary>
  /// Extension methods for <see cref="Framework"/>.
  /// </summary>
  public static class ThemeExtensions
  {
      public const string MSGuiColorPaletteViewId = "MSGuiColorPaletteViewId";

      internal static double HeaderHeightDefault = 22.0;

      internal static double HeaderHeightFloatingDefault = 22.0;

      internal static bool HeaderItemPositionBeforeTitle = false;

      internal static bool SubTabHeaderTooltipsEnabled;

      internal static double DefaultDockContentPadding = 5.0;
      internal static double? RibbonCaptionButtonAreaFixedWidth = null;

      internal static double StatusBarHeight = 25.8;

      public static double FloatingWindowHeaderSeperatorHeight = 1.0;
      public static Thickness FloatingWindowCaptionBarMargin = new Thickness(0, -2, 0, 0);
      public static Thickness FloatingWindowBorderMargin = new Thickness(0.0);
      public static Thickness FloatingWindowBorderPadding = new Thickness(2.0);
      public static Thickness FloatingWindowFlatModeBorderPadding = new Thickness(0.0); 
      public static Thickness FloatingWindowBorderBorderThickness = new Thickness(1.0);
      public static Thickness ContentPaneMargin = new Thickness(0, 0, 1, 1);
      public static Thickness ContentPaneFlatModeMargin = new Thickness(0.0);
      public static Thickness ContentPaneFlashBorderThickness = new Thickness(2.0);
      public static Thickness ContentPaneFlatModeFlashBorderThickness = new Thickness(1.0);
      public static Thickness ContentPaneFlashBorderMargin = new Thickness(0, 0, 2, 2);
      public static Thickness ContentPaneFlatModeFlashBorderMargin = new Thickness(0.0); 
      public static CornerRadius FloatingWindowBorderCornerRadius = new CornerRadius(5);
     
      public static Thickness TileItemBorderThickness = new Thickness(2.0);
      public static CornerRadius TileItemBorderCornerRadius = new CornerRadius(2);

 
      /// <summary>
      /// Allows to set the object used for tooltip contents while a view is presented in a tab group
      /// pane and user hovers mouse over the tab.
      /// </summary>
      public static void EnableSubTabHeaderTooltips(this Framework framework)
      {
          SubTabHeaderTooltipsEnabled = true;
      }

    [Obsolete("Use AddTheme(Theme theme) instead", true)]
    public static void AddTheme(this Framework framework_, string name, ImageSource icon, Theme theme)
    {
      framework_.Bootstrapper.AddTheme(name, icon, theme);
    }

    public static void AddTheme(this Framework framework_, Theme theme)
    {
      framework_.Bootstrapper.AddTheme(theme);
      framework_.Bootstrapper.SetTheme(theme.Name);
    }

    [Obsolete("AddTheme sets theme as well now")]
    public static void SetTheme(this Framework framework_, string name)
    {
      framework_.Bootstrapper.SetTheme(name);
    }

    [Obsolete("Theme changer is diabled by default now")]
    public static void DisableThemeChanger(this Framework framework_)
    {
      framework_.Bootstrapper.DisableThemeChanger();
    }

    [Obsolete("Default themes are suppressed by default now")]
    public static void SuppressDefaultThemes(this Framework framework_)
    {
      framework_.Bootstrapper.SuppressDefaultThemes();
    }

    public static void EnableSoftwareRendering(this Framework framework_)
    {
      framework_.Bootstrapper.EnableSoftwareRendering();
    }

    public static void DisableEnforcedSizeRestrictions(this Framework framework_)
    {
      framework_.Bootstrapper.DisableEnforcedSizeRestrictions();
    }

      public static void SetViewHeaderHeight(this Framework framework_, Double height)
      {
          HeaderHeightDefault = height;
          HeaderHeightFloatingDefault = height;
          PaneContainer.PaneHeaderHeightDefault = height; 
      }

      public static void SetViewHeaderHeight(this Framework framework_, Double dockedHeaderHeight, Double floatingWindowHeaderHeight)
      {
          HeaderHeightDefault = dockedHeaderHeight;
          HeaderHeightFloatingDefault = PaneContainer.PaneHeaderHeightDefault = floatingWindowHeaderHeight;  
      }

      public static void SetHeaderItemPositionBeforeTitle(this Framework framework_)
      {
          HeaderItemPositionBeforeTitle = true;
          PaneHeaderControlBase.Structure = HeaderStructure.Office2007;
      }

      public static void EnableCaptionButtonWidthHack(this Framework framework)
      {
          RibbonCaptionButtonAreaFixedWidth = 110.0;
      }

      internal static void RegisterMsGuiColorPalette(IUnityContainer container_)
      {
          IChromeRegistry chromeRegistry = container_.Resolve(typeof(IChromeRegistry), null) as IChromeRegistry;
          if (chromeRegistry != null)
          {
              chromeRegistry.RegisterWindowFactory(MSGuiColorPaletteViewId, 
                  ((w_, s_) =>
                      {
                          w_.Parameters.SizingMethod = SizingMethod.Custom;
                          MSGuiColorPalette content = new MSGuiColorPalette();
                          w_.Content = content;
                          w_.Parameters.Width = 
                          w_.Width = content.InitialSize.Width;
                          w_.Parameters.Height =
                          w_.Height = content.InitialSize.Height;
                          w_.Title = "MSGuiColor Palette";  
                          return true;
                      }));
          }
      }
  }
}
