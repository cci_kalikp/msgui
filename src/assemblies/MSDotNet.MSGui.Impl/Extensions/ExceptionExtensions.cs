﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Extensions/ExceptionExtensions.cs#24 $
// $Change: 888143 $
// $DateTime: 2014/07/14 01:18:26 $
// $Author: caijin $

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Xaml;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Impl.MailAttachment;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{

    /// <summary>
    /// Static class for holding extension methods for the <see cref="Exception"/> class.
    /// </summary>
    public static class ExceptionExtensions
    {
        private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger("ExceptionExtensions");

        #region Throttling

        //Preventing applications from sending enormous amount of messages in very short period of time (may be caused by wpf)
        private static readonly TimeSpan m_minInterval = new TimeSpan(0, 0, 10);
                                         //no more then 1 message in this interval of time will be sent

        private const int M_MAXSENT = 100;
                          //Maximum allowed number of sent messages per application run, unlikely to be hit ever

        private static DateTime? m_whenLastSent = null;
        private static int m_numSentMessages = 0; 

        private class MessageGroup
        {
            public MailMessage Message { get; set; }
            public DateTime FirstMessageTime { get; set; }
            public DateTime LastMessageTime { get; set; }
            public int Quantity { get; set; }

            public int Id { get; set; }
        }

        private static readonly object m_aggregatorLock = new object();
        private static readonly object m_lastSentInfoLock = new object();
        private static readonly Queue<MessageGroup> m_aggregated = new Queue<MessageGroup>();

        private static void AggregateMessage(MailMessage msg, DateTime now)
        {
            string representatonForHashing = msg.Body + msg.Subject + msg.From;
            representatonForHashing = msg.To.Aggregate(representatonForHashing, (current, to) => current + to);

            representatonForHashing = msg.Attachments.Where(att => att != null).Aggregate(representatonForHashing, (current, att) => current + att.Name);

            int key = representatonForHashing.GetHashCode();
            MessageGroup group = m_aggregated.FirstOrDefault(group_ => group_.Id == key);
            if (group != null)
            {
                group.Quantity++;
                group.LastMessageTime = now;
            }
            else
            {
                var msgGroup = new MessageGroup()
                    {
                        FirstMessageTime = now,
                        LastMessageTime = now,
                        Message = msg,
                        Quantity = 1,
                        Id = key
                    };
                m_aggregated.Enqueue(msgGroup);
            }
        }

        private static void SendOrCache(MailMessage msg)
        {

            lock (m_aggregatorLock)
            {
                DateTime now = DateTime.Now;
                if (m_aggregated.Count == 0)
                {
                    AggregateMessage(msg, now);
                    if (m_aggregated.Count == 1 &&
                        (!m_whenLastSent.HasValue ||
                         (m_minInterval + (m_whenLastSent.Value - DateTime.Now)).TotalMilliseconds <= 0))
                    {
                        SendLatestMessage();
                    }
                    if (m_aggregated.Count > 0)
                    {
                        ThreadPool.QueueUserWorkItem(
                            delegate
                                {
                                    while (true)
                                    {
                                        if (m_whenLastSent.HasValue)
                                        {
                                            TimeSpan delta = m_minInterval + (m_whenLastSent.Value - DateTime.Now);
                                            if (delta.TotalMilliseconds > 0)
                                            {
                                                Thread.Sleep(delta);
                                            }
                                        }
                                        lock (m_aggregatorLock)
                                        {
                                            if (SendLatestMessage()) break;
                                        }
                                    }
                                });
                    }
                }
                else
                {
                    lock (m_aggregatorLock)
                    {
                        AggregateMessage(msg, now);
                    }
                }
            }
        }

        private static bool SendLatestMessage()
        {
            if (m_aggregated.Count == 0)
            {
                return true;
            }
            MessageGroup msgGroup = m_aggregated.Dequeue();
            try
            {
                if (msgGroup.Quantity > 1)
                {
                    msgGroup.Message.Body =
                        string.Format("A group of similar exceptions. Number:{0}. First at:{1} Last at:{2}\n",
                                      msgGroup.Quantity, msgGroup.FirstMessageTime.ToString("O"),
                                      msgGroup.LastMessageTime.ToString("O")) + msgGroup.Message.Body;
                }
                if (m_aggregated.Count > 1)
                {
                    msgGroup.Message.Body +=
                        string.Format("\n\nThere are few more groups of exceptions pending. Number: {0}\n",
                                      m_aggregated.Count - 1);
                }
                if (m_numSentMessages >= M_MAXSENT)
                {
                    TaskDialog.ShowMessage(
                        "This application has exceeded the maximum allowed number of error reports.\nIt's highly recommended to restart the application and contact corresponding support groups.");

                }
                else
                {
                    using (var client = new SmtpClient("mta-hub.ms.com"))
                    {
                        client.Send(msgGroup.Message); //
                    }

                    lock (m_lastSentInfoLock)
                        // alternative solution: to combine messages (and their attachements) with same to/from
                    {
                        //   the current implementation is simpler, but it's more likely to lose some messages 
                        m_whenLastSent = DateTime.Now; //   if the application is closed  before we send them
                        m_numSentMessages++;
                    }
                }
            } 
            catch(Exception ex)
            {
                //this kind of exception happens when the email for the same exception is sent multiple times very quickly,
                //usually due to manual click, instead of fixing the sending email code to regenerate message with new stream in this case
                //we simply ignore it, since this means redundant email sending
                if (msgGroup.Message.Attachments.Count > 0 && ex.InnerException is ObjectDisposedException &&
                    ex.InnerException.Message == "Cannot access a closed Stream.")
                {
                    return false;
                }
                m_logger.Error("Failed to send error report", ex); 
            }
            finally
            {
                foreach (var attachment in msgGroup.Message.Attachments)
                {
                    attachment.Dispose();
                }
            }
            return false;

        }

        #endregion

        public static void SetOccurred(this Exception exception_)
        {
            exception_.Data["OccurredTime"] = DateTime.Now;
        }

        public static DateTime? GetOccurred(this Exception exception_)
        {
            return exception_.Data.Contains("OccurredTime")
                       ? (DateTime)exception_.Data["OccurredTime"]
                       : default(DateTime?);
        }
        /// <summary>
        /// This event is rased when a <see cref="Exception"/> <see cref="MailMessage"/> is sent.
        /// </summary>
        public static event ExceptionMailMessageCallback ExceptionMailMessageSent;

        /// <summary>
        /// Sends an e-mail containing the specified parameters.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="fromAddress">The address of the sender.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="attachments">The attachments to attach to the mail.</param>
        /// <param name="toAddresses">The addresses of the recipients.</param>
        public static void SendMail(this Exception exception, string fromAddress, string subject,
                                    Attachment[] attachments, params string[] toAddresses)
        {
            if (toAddresses == null || toAddresses.Length == 0)
            {
                throw new ArgumentException(
                    "At least one to address must be set in order to send the exception in e-mail.");
            }

            var msg = new MailMessage();
            msg.From = new MailAddress(fromAddress);
            foreach (string toAddress in toAddresses)
            {
                msg.To.Add(new MailAddress(toAddress));
            }

            bool isBodyHtml = false;
            string body = string.Empty;
            if (attachments != null)
            {
                foreach (Attachment attachment in attachments)
                {
                    if (attachment == null) continue;
                    var largeFileAttachment = attachment as LargeFileAttachment;
                    if (largeFileAttachment != null)
                    {
                        body += string.Format("<a href='file:///{0}'>{1}</a></br>", largeFileAttachment.FileLink,
                                              largeFileAttachment.Name);
                        isBodyHtml = true;
                        continue;
                    }
                    msg.Attachments.Add(attachment);
                }
            }
            if (isBodyHtml)
            {
                DateTime? occurred = exception.GetOccurred();
                if (occurred != null)
                {
                    body += "Occurred at " + occurred.Value.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + "<br/>";
                }
                body += exception.GetDetailedMessage().Replace(Environment.NewLine, "<br/>");
            }
            else
            {
                DateTime? occurred = exception.GetOccurred();
                if (occurred != null)
                {
                    body += "Occurred at " + occurred.Value.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + Environment.NewLine;
                }
                body += exception.GetDetailedMessage();
            }

            msg.Subject = subject;
            msg.IsBodyHtml = isBodyHtml;
            msg.Body = body;

            if (!DisableEmailSend)
            {
                SendOrCache(msg);
            }
            InvokeMailMessageSent(exception, fromAddress, subject, attachments, toAddresses);
        }


        /// <summary>
        /// Returns a detailed message of this exception.
        /// </summary>
        /// <remarks>
        /// It creates a long message containing all inner-exceptions, their stacktraces, and all the loaded assemblies.
        /// </remarks>
        /// <param name="exception">The exception.</param>
        /// <returns>The detailed exception message.</returns>
        public static string GetDetailedMessage(this Exception exception)
        {
            var sb = new StringBuilder();
            if (exception is TargetInvocationException)
            {
                sb.AppendLine("Exception was wrapped into a TargetInvocationException");
                exception = exception.InnerException;
            }

            var xamlParseException = exception as XamlParseException;
            if (xamlParseException!=null)
            {
                try
                {
                    sb.AppendFormat("LN: {0}, LP: {1}{2}{3}{2}", xamlParseException.LineNumber,
                                    xamlParseException.LinePosition, Environment.NewLine,
                                    xamlParseException.Message);
                }
                catch
                {
                }
            }

            sb.AppendLine(exception.ToString());

            sb.AppendLine(Environment.NewLine + "------------------" + Environment.NewLine);
            try
            {
                sb.AppendFormat("Username: {0} Machine: {1} OS: {4} CLR: {5}{6} CmdLine: {2}{6} CD: {3}{6}",
                                Environment.UserName, Environment.MachineName, Environment.CommandLine,
                                Environment.CurrentDirectory,
                                Environment.OSVersion, Environment.Version, Environment.NewLine);
                sb.AppendLine(Environment.NewLine + "------------------" + Environment.NewLine);
                var envs = Environment.GetEnvironmentVariables();
                var sortedenv = new SortedDictionary<string, string>();
                foreach (DictionaryEntry dictionaryEntry in envs)
                {
                    sortedenv.Add(dictionaryEntry.Key.ToString(), dictionaryEntry.Value.ToString());
                }

                foreach (KeyValuePair<string, string> dictionaryEntry in sortedenv)
                {
                    sb.AppendFormat("{0} : {1} {2}", dictionaryEntry.Key, dictionaryEntry.Value, Environment.NewLine);
                }
                sb.AppendLine(Environment.NewLine + "------------------" + Environment.NewLine);
            }
            catch (Exception exc)
            {
                // This would just mean a part of information would not be contained in the mail
            }
            sb.AppendLine("This is the list of the currently loaded assemblies: ");
            sb.AppendLine(
                "Please note that this listing only includes assemblies already loaded thus some referenced assemblies may be missing");
            sb.AppendLine(
                "Please also take into consideration that it lists assemblies only from the current Application Domain");
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                sb.AppendLine("Name: " + assembly.GetName().Name);
                sb.AppendLine("Version: " + assembly.GetName().Version);
                sb.AppendLine("Global cache: " + assembly.GlobalAssemblyCache);
                string location = GetLocation(assembly);
                sb.AppendLine("Location: " + location);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Get location of assembly.
        /// </summary>
        /// <remarks>
        /// If the assembly is dynamically generated
        /// an exception will be thrown when trying to get the location
        /// property. This is now wrapped in a method 
        /// with <see cref="DebuggerNonUserCodeAttribute"/>
        /// so that users will not break on exception.
        /// </remarks>
        /// <param name="assembly">Assembly to get location from.</param>
        /// <returns>Location of assembly.</returns>
        [DebuggerNonUserCode]
        [MethodImpl(MethodImplOptions.NoInlining)]
        private static string GetLocation(Assembly assembly)
        {
            string location = "Cannot be retrieved.";
            try
            {
                if (assembly != null && !(assembly is AssemblyBuilder) &&
                    assembly.GetType().FullName != "System.Reflection.Emit.InternalAssemblyBuilder")
                    location = assembly.Location;
            }
            catch (NotSupportedException)
            {
                // in case of dynamic assemblies the Location property cannot be retrieved,
                // so let's ignore it.
            }
            return location;
        }

        /// <summary>
        /// Invoke the ExceptionMailMessageSent event safely.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="fromAddress">The address of the sender.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="attachments">The attachments to attach to the mail.</param>
        /// <param name="toAddresses">The addresses of the recipients.</param>
        private static void InvokeMailMessageSent(Exception exception, string fromAddress, string subject,
                                                  Attachment[] attachments, string[] toAddresses)
        {
            var sent = ExceptionMailMessageSent;
            if (sent != null)
            {
                sent(exception, fromAddress, subject, attachments, toAddresses);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether E-mails will be sent.
        /// </summary>
        internal static bool DisableEmailSend { get; set; }
    }
}
