﻿namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions.Composite
{
    public static class CompositeModuleExtensions
    {
        public static void AddModule(this Framework framework, Microsoft.Practices.Composite.Modularity.ModuleInfo moduleInfo_)
        {
            framework.AddModule(moduleInfo_);
        }
    }
}


