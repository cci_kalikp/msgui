﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Extensions/SplashScreenExtensions.cs#15 $
// $Change: 878686 $
// $DateTime: 2014/04/28 07:58:24 $
// $Author: milosp $

using System.Windows;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
	/// <summary>
	/// Extension methods for Splash Window Customization
	/// </summary>
	public static class SplashScreenExtensions
	{
        public static void UseLegacySplash(this Framework f)
        {
            Framework.UseLegacySplash = true;
        }

        /// <summary>
        /// Sets the splash screen to stay as top most.
        /// </summary>
        /// <param name="source_">The source.</param>
        /// <param name="isTopMost_">if set to <c>true</c> show the name.</param>
        public static void SetSplashScreenTopMost(this Framework source_, bool isTopMost_)
        {
            SplashLauncher<SplashWindow>.IsTopMost = isTopMost_;
            SplashLauncher<ModernSplashWindow>.IsTopMost = isTopMost_;
        }


		/// <summary>
		/// Sets the splash screen picture.
		/// </summary>
		/// <param name="source_">The source.</param>
		/// <param name="filePath_">The file path.</param>
		public static void SetSplashScreenPicture(this Framework source_, string filePath_)
		{
		    UseLegacySplash(source_);
			SplashLauncher<SplashWindow>.BackgroundImage = filePath_;
		}

		/// <summary>
		/// Sets the splash screen application name to be shown.
		/// </summary>
		/// <param name="source_">The source.</param>
		/// <param name="show_">if set to <c>true</c> show the name.</param>
		public static void SetSplashScreenApplicationNameToBeShown(this Framework source_, bool show_)
		{
			SplashLauncher<SplashWindow>.IsApplicationNameShown = show_;
		    SplashLauncher<ModernSplashWindow>.IsApplicationNameShown = show_;
		}
         

		/// <summary>
		/// Sets the splash screen bitmap transparent.
		/// </summary>
		/// <param name="source_">The source.</param>
		/// <param name="transparent_">if set to <c>true</c> make it transparent.</param>
		public static void SetSplashScreenBitmapTransparent(this Framework source_, bool transparent_)
		{
			SplashLauncher<SplashWindow>.IsBitmapTransparent = transparent_;
		    SplashLauncher<ModernSplashWindow>.IsBitmapTransparent = transparent_;
		}
 
		/// <summary>
		/// Sets the splash screen status position.
		/// </summary>
		/// <param name="source_">The source.</param>
		/// <param name="position_">The position.</param>
		public static void SetSplashScreenStatusPosition(this Framework source_, Point position_)
		{
			SplashLauncher<SplashWindow>.StatusPosition = position_;
            SplashLauncher<ModernSplashWindow>.StatusPosition = position_;
        }

		/// <summary>
		/// Sets the splash screen name position.
		/// </summary>
		/// <param name="source_">The source.</param>
		/// <param name="position_">The position.</param>
		public static void SetSplashScreenNamePosition(this Framework source_, Point position_)
        { 
			SplashLauncher<SplashWindow>.NamePosition = position_;
            SplashLauncher<ModernSplashWindow>.NamePosition = position_;
        }

		/// <summary>
		/// Sets the color of the splash screen status.
		/// </summary>
		/// <param name="source_">The source.</param>
		/// <param name="color_">The color.</param>
		public static void SetSplashScreenStatusColor(this Framework source_, Color color_)
		{
			SplashLauncher<SplashWindow>.StatusColor = color_;
            SplashLauncher<ModernSplashWindow>.StatusColor = color_;
		}

		/// <summary>
		/// Sets the splash screen status font family.
		/// </summary>
		/// <param name="source_">The source.</param>
		/// <param name="fontFamily_">The font family.</param>
		public static void SetSplashScreenStatusFontFamily(this Framework source_, FontFamily fontFamily_)
		{
			SplashLauncher<SplashWindow>.StatusFontFamily = fontFamily_;
            SplashLauncher<ModernSplashWindow>.StatusFontFamily = fontFamily_;
        }

		/// <summary>
		/// Sets the splash screen version position.
		/// </summary>
		/// <param name="source_">The source.</param>
		/// <param name="position_">The position.</param>
		public static void SetSplashScreenVersionPosition(this Framework source_, Point position_)
        {
			SplashLauncher<SplashWindow>.VersionPosition = position_;
            SplashLauncher<ModernSplashWindow>.VersionPosition = position_;
        }

		/// <summary>
		/// Sets the color of the splash screen version.
		/// </summary>
		/// <param name="source_">The source.</param>
		/// <param name="color_">The color.</param>
		public static void SetSplashScreenVersionColor(this Framework source_, Color color_)
		{
			SplashLauncher<SplashWindow>.VersionColor = color_;
            SplashLauncher<ModernSplashWindow>.VersionColor = color_;
        }

		/// <summary>
		/// Sets the splash screen name font family.
		/// </summary>
		/// <param name="source_">The source.</param>
		/// <param name="fontFamily_">The font family.</param>
		public static void SetSplashScreenNameFontFamily(this Framework source_, FontFamily fontFamily_)
		{
			SplashLauncher<SplashWindow>.NameFontFamily = fontFamily_;
            SplashLauncher<ModernSplashWindow>.NameFontFamily = fontFamily_;
        }

		/// <summary>
		/// Sets the color of the splash screen name.
		/// </summary>
		/// <param name="source_">The source.</param>
		/// <param name="color_">The color.</param>
		public static void SetSplashScreenNameColor(this Framework source_, Color color_)
		{
			SplashLauncher<SplashWindow>.NameColor = color_;
            SplashLauncher<ModernSplashWindow>.NameColor = color_;
        }

		/// <summary>
		/// Sets the splash screen autosize to its image.
		/// </summary>
		/// <param name="source_">The source.</param>
		/// <param name="autosize_">if set to <c>true</c> use autosize.</param>
		public static void SetSplashScreenAutosize(this Framework source_, bool autosize_)
		{
			SplashLauncher<SplashWindow>.IsAutosize = autosize_;
		}

		/// <summary>
		/// Sets the splash screen close button position.
		/// </summary>
		/// <param name="source_">The source.</param>
		/// <param name="position_">The position from top right.</param>
		public static void SetSplashScreenCloseButtonPosition(this Framework source_, Point position_)
		{
			SplashLauncher<SplashWindow>.XButtonPosition = position_;
            SplashLauncher<ModernSplashWindow>.XButtonPosition = position_;
        }

		/// <summary>
		/// Sets the splash screen close button size.
		/// </summary>
		/// <param name="source_">The source.</param>
		/// <param name="size_">The size.</param>
		public static void SetSplashScreenCloseButtonSize(this Framework source_, Size size_)
		{
			SplashLauncher<SplashWindow>.XButtonSize = size_;
            SplashLauncher<ModernSplashWindow>.XButtonSize = size_; 
        }

		/// <summary>
		/// Sets the splash screen close button visible.
		/// </summary>
		/// <param name="source_">The framework.</param>
		/// <param name="isVisible_">if set to <c>true</c> close is visible.</param>
		public static void SetSplashScreenCloseButtonVisible(this Framework source_, bool isVisible_)
		{
			SplashLauncher<SplashWindow>.IsXButtonShown = isVisible_;
            SplashLauncher<ModernSplashWindow>.IsXButtonShown = isVisible_;
        }

		public static void SetSplashScreenDoubleClickDismiss(this Framework source_, bool enable)
		{
			SplashWindow.EnableDoubleClickDismiss = enable;
		    ModernSplashWindow.EnableDoubleClickDismiss = enable;
		}

		public static void SetSplashScreenMinimizeOnDismiss(this Framework source_, bool enable)
		{
			SplashWindow.EnableMinimizeOnHide = enable;
		    ModernSplashWindow.EnableMinimizeOnHide = enable;
		}

        public static void DisableSplashScreen(this Framework source_)
        {
            Framework.SkipSplashScreen = true;
        }

        public static void EnableLayoutLoadProgressTracking(this Framework framework)
        {
            DockViewModel.TrackLayoutLoadStatus = true;
            PersistenceProfileService.ShowLayoutLoadingScreen = true;
        }
	}
}
