﻿using System.Windows;
using System.Windows.Forms;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    public static class WindowExtensions
    {
        public static void RegisterAsSticky(this Form winForm)
        {
            var viewContainer = winForm.GetViewContainer();
            var wpfWindow = Window.GetWindow((DependencyObject)viewContainer.Content);
            wpfWindow.RegisterAsSticky();
        }

        public static void RegisterAsSticky(this Window window)
        {
            new WPFStickyWindow(window);
        }
    }
}
