﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;  
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Impl.Options;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    public static class OptionsExtensions
    {
        internal static bool EnableTheme = true;
        internal static bool EnableTypeaheadDropDownSearch = false;
        public static void UseClassicOptionsWindow(this Framework framework_)
        {
            EnableTheme = false;
        }

        public static void EnableTypeaheadDropdownOptionsSearch(this Framework framework_)
        {
            EnableTypeaheadDropDownSearch = true;
        }

        public static void SetOptionGroupPriority(this Framework framework_, string group_, int priority_)
        {
            ApplicationOptions.SetPriority(group_, null, priority_);
        }

        public static void SetOptionPagePriority(this Framework framework_, string path_, string title_, int priority_)
        {
            ApplicationOptions.SetPriority(path_, title_, priority_);
        }
        
        public static void Highlight(this FrameworkElement control_)
        {
            if (control_ == null) return;
            control_.Focus();
            if (control_ is Control || control_ is Border)
            {
                EventTrigger flashTrigger = System.Windows.Application.Current.Resources["FlashTrigger"] as EventTrigger;
                if (flashTrigger == null) return; 
                if (!control_.Triggers.Contains(flashTrigger))
                {
                    control_.Triggers.Add(flashTrigger);
                }
                RaiseNeedsFocusEvent(control_); 
            }

            
        }
        public static readonly RoutedEvent NeedsFocusEvent = EventManager.RegisterRoutedEvent("NeedsFocusEvent", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(OptionsExtensions));
        public static void AddNeedsFocusEventHandler(DependencyObject d, RoutedEventHandler handler)
        {
            UIElement uie = d as UIElement;
            if (uie != null)
            {
                uie.AddHandler(OptionsExtensions.NeedsFocusEvent, handler);
            }
        }
        public static void RemoveNeedsFocusEventHandler(DependencyObject d, RoutedEventHandler handler)
        {
            UIElement uie = d as UIElement;
            if (uie != null)
            {
                uie.RemoveHandler(OptionsExtensions.NeedsFocusEvent, handler);
            } 
        }
        internal static void RaiseNeedsFocusEvent(UIElement target_)
        {
            Brush oldBrush = target_.GetValue(Border.BorderBrushProperty) as Brush;
            Thickness oldThickness = (Thickness) target_.GetValue(Border.BorderThicknessProperty);
            target_.SetValue(Border.BorderBrushProperty, new SolidColorBrush(Colors.Red));
            target_.SetValue(Border.BorderThicknessProperty, new Thickness(2)); 
            var args = new RoutedEventArgs(NeedsFocusEvent);
            target_.RaiseEvent(args);
            var timer = new DispatcherTimer();
            timer.Tag = new object[] { target_, oldBrush, oldThickness };
            timer.Interval = new TimeSpan(0, 0, 4);
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start(); 

        }

        static void timer_Tick(object sender, EventArgs e)
        {
            DispatcherTimer timer = sender as DispatcherTimer;
            if (timer != null)
            {
                timer.Stop();
                timer.Tick -= new EventHandler(timer_Tick);
            }
            object[] tag = timer.Tag as object[];
            FrameworkElement control = tag[0] as FrameworkElement; 
            Brush brush = tag[1] as Brush;
            Thickness thickness = (Thickness)tag[2]; 
            if (control != null)
            {
                control.SetValue(Border.BorderBrushProperty, brush);
                control.SetValue(Border.BorderThicknessProperty, thickness); 

            }
        }  
 
    }
}
