﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Extensions/HideEditorBrowsable.cs#3 $
// $Change: 735328 $
// $DateTime: 2010/12/08 21:29:33 $
// $Author: yomajul $

using System;
using System.ComponentModel;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public interface IHideObjectMembers
    {
        [EditorBrowsable(EditorBrowsableState.Never)]
        Type GetType();
        [EditorBrowsable(EditorBrowsableState.Never)]
        int GetHashCode();
        [EditorBrowsable(EditorBrowsableState.Never)]
        string ToString();
        [EditorBrowsable(EditorBrowsableState.Never)]
        bool Equals(object other);
    }

}
