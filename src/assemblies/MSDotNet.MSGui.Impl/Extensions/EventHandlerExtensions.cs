﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea
{
    public static class EventHandlerExtensions
    {
        public static EventHandler<ToE> Convert<FromE, ToE>(this EventHandler<FromE> eventHandler_)
            where FromE : EventArgs
            where ToE : EventArgs
        {
            return new EventHandlerAdapter1<FromE, ToE>(eventHandler_).Adapt();
        }

        public static EventHandler<ToE> Convert<ToE>(this EventHandler eventHandler_)
            where ToE : EventArgs
        {
            return new EventHandlerAdapter2<ToE>(eventHandler_).Adapt();
        }

        public static EventHandler Convert<FromE>(this EventHandler<FromE> eventHandler_)
            where FromE : EventArgs 
        {
            return new EventHandlerAdapter3<FromE>(eventHandler_).Adapt();
        }

        public static RoutedEventHandler ConvertToRouted(this EventHandler eventHandler_)
        {
            return new EventHandlerAdapter4(eventHandler_).Adapt();
        }

        public static EventHandler Convert(this Action action_)
        {
            return new EventHandlerAdapter5(action_).Adapt2();
        }

        public static RoutedEventHandler ConvertToRouted(this Action action_)
        {
            return new EventHandlerAdapter5(action_).Adapt1();
        }

        public static EventHandler<ToE> Convert<ToE>(this Action action_) where ToE : EventArgs
        {
            return new EventHandlerAdapter5(action_).Adapt3<ToE>();
        }

        private class EventHandlerAdapter1<FromE, ToE>
            where FromE : EventArgs
            where ToE : EventArgs
        {
            private readonly EventHandler<FromE> handler;
            public EventHandlerAdapter1(EventHandler<FromE> handler_)
            {
                handler = handler_;
            }

            public EventHandler<ToE> Adapt()
            {
                if (handler == null) return null;
                return (sender_, e_) => handler(sender_, e_ as FromE);
            }
        }

        private class EventHandlerAdapter2<ToE>
            where ToE : EventArgs
        {
            private readonly EventHandler handler;
            public EventHandlerAdapter2(EventHandler handler_)
            {
                handler = handler_;
            }

            public EventHandler<ToE> Adapt()
            {
                if (handler == null) return null;
                return (sender_, e_) => handler(sender_, e_);
            }
        }


        private class EventHandlerAdapter3<FromE> where FromE : EventArgs
        {
            private readonly EventHandler<FromE> handler;
            public EventHandlerAdapter3(EventHandler<FromE> handler_)
            {
                handler = handler_;
            }

            public EventHandler Adapt()
            {
                if (handler == null) return null;
                return (sender_, e_) => handler(sender_, e_ as FromE);
            }
        }


        private class EventHandlerAdapter4
        {
            private readonly EventHandler handler;
            public EventHandlerAdapter4(EventHandler handler_)
            {
                handler = handler_;
            }

            public RoutedEventHandler Adapt()
            {
                if (handler == null) return null;
                return (sender_, e_) => handler(sender_, e_);
            } 
        }


        private class EventHandlerAdapter5
        {
            private readonly Action handler;
            public EventHandlerAdapter5(Action handler_)
            {
                handler = handler_;
            }

            public RoutedEventHandler Adapt1()
            {
                if (handler == null) return null;
                return (sender_, args_) => handler();
            }

            public EventHandler Adapt2()
            {
                if (handler == null) return null;
                return (sender_, args_) => handler();
            }

            public EventHandler<ToE> Adapt3<ToE>() where ToE : EventArgs
            {
                if (handler == null) return null;
                return (sender_, e_) => handler();
            }


        }
    }




}
