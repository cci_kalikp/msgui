﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon.Widgets;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    public class GalleryEventArgs : EventArgs
    {
        public RibbonGalleryViewContainer Gallery { get; set; }
        public RibbonGalleryGroupViewContainer Group { get; set; }
        public RibbonGalleryItemViewContainer Item { get; set; }
    }
}
