﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Infragistics.Windows.Ribbon;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    internal class RibbonButtonGroupArea : WidgetViewContainer, IButtonGroupViewContainer
    {
        private ButtonGroup _buttonGroup;

        public RibbonButtonGroupArea(string ID, InitialWidgetParameters parameters)
            : base(ID, parameters)
        {
            this.ContentReady += (s, e) =>
            {
                _buttonGroup = this.Content as ButtonGroup;
            };
        }

        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            // TODO: Need to wait for ContentReady?
            _buttonGroup.Children.Add(child.Content as UIElement);
            return base.DoAddVisual(child);
        }

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            _buttonGroup.Children.Remove(child.Content as UIElement);
            base.DoRemoveVisual(child);
        }
    }
}
