﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Infragistics.Windows.Ribbon;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    internal class RibbonTabArea : ObservableWidgetViewContainer, IRibbonTab
	{
		public RibbonTabArea(string id, InitialWidgetParameters parameters) : base(id, parameters)
		{
            ObservableChildren.CollectionChanged += ObservableChildrenOnCollectionChanged;
		}

        private void ObservableChildrenOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            if (notifyCollectionChangedEventArgs.Action == NotifyCollectionChangedAction.Remove &&
                ObservableChildren.Count == 0)
            {
                ObservableChildren.CollectionChanged -= ObservableChildrenOnCollectionChanged;
                chromeManager.RemoveWidget(this);
            }
        }

		protected override bool DoAddVisual(IWidgetViewContainer child)
		{
		    if (!child.ContentOfType<RibbonGroup>()) return false;
            var tab = (RibbonTabItem)Content;
            tab.RibbonGroups.Add(child.Content as RibbonGroup);
            return base.DoAddVisual(child);

		}

		protected override InitialWidgetParameters DoAddDefault(ref string ID)
		{
			var newGroup = new InitialRibbonGroupParameters { Text = ID };
			ID = Guid.NewGuid().ToString();
			return newGroup;
		}

		protected override bool DoContains(string index)
		{
			return (m_children.Values.FirstOrDefault(vc => vc.Parameters.Text == index) != null) || m_children.ContainsKey(index);
		}

		protected override IWidgetViewContainer DoGetWidget(string index)
		{
			return m_children.Values.FirstOrDefault(vc => vc.Parameters.Text == index) ?? m_children[index];
		}

		protected override bool DoCanAddwidget(IWidgetViewContainer widget)
		{
			return widget is RibbonGroupArea;
		}

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {

            base.DoRemoveVisual(child);
            if (child is RibbonGroupArea)
            {
                ((RibbonTabItem) Content).RibbonGroups.Remove((RibbonGroup) child.Content);
            } 
        }

        public override void SwapChildren(IWidgetViewContainer a, IWidgetViewContainer b)
        {
            base.SwapChildren(a, b);
            if (a is RibbonGroupArea && b is RibbonGroupArea)
            {
                var ribbonTabItem = Content as RibbonTabItem;
                var aRibbonGroup = a.Content as RibbonGroup;
                var bRibbonGroup = b.Content as RibbonGroup;
                var indexA = ribbonTabItem.RibbonGroups.IndexOf(aRibbonGroup);
                var indexB = ribbonTabItem.RibbonGroups.IndexOf(bRibbonGroup);

                ribbonTabItem.RibbonGroups.Remove(aRibbonGroup);
                ribbonTabItem.RibbonGroups.Remove(bRibbonGroup);
                bool insertAFirst = indexA < indexB; 
                ribbonTabItem.RibbonGroups.Insert(insertAFirst ? indexA : indexB, insertAFirst ? bRibbonGroup : aRibbonGroup );
                ribbonTabItem.RibbonGroups.Insert(insertAFirst ? indexB : indexA, insertAFirst ? aRibbonGroup : bRibbonGroup);
                
            }
        }

        public override void Rename(string newName)
        {
            base.Rename(newName);
            (Content as RibbonTabItem).Header = newName;
        }

        public override string NodeName
        {
            get
            {
                return "Tab";
            }
        }

        #region IRibbonTab Members

		public new IRibbonGroup this[string groupName]
		{
			get
			{
				return ((WidgetViewContainer)this)[groupName] as IRibbonGroup;
			}
		}

        public void Activate()
        {
            ((RibbonTabItem)Content).IsSelected = true;
        }

		public IRibbonGroup AddGroup(string groupName)
		{
			return this.AddWidget(groupName, new InitialRibbonGroupParameters() { Text = groupName }) as IRibbonGroup;
		}

      

        public IRibbonGroup AddGroup(string groupName, IEnumerable<GroupVariant> groupVariants)
        {
            var group =  this.AddWidget(groupName, new InitialRibbonGroupParameters(){Text=groupName}) as IRibbonGroup;
            foreach (var groupVariant in groupVariants)
            {
                group.AddVariantGroup(groupVariant.ResizeAction, groupVariant.Priority);
            }
            return group;

        }

		#endregion
	}
}
