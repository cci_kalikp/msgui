﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Infragistics.Windows.Ribbon;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    [DesignTimeVisible(false), Serializable]
    internal sealed class CustomButtonTool : ButtonTool
    {
        private static bool _eventHandlersAttached;

        public CustomButtonTool()
        {
        }

        protected override void OnClick()
        {
            base.OnClick();
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
        }

        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            var baseSize = base.ArrangeOverride(arrangeBounds);
            return baseSize;
        }

        protected override Size MeasureOverride(Size constraint)
        {
            if (!_eventHandlersAttached)
            {
                // First measurement - add visibility changed handlers
                var chrome = (RibbonButtonChrome)GetVisualChild(0);
                var grid = (Grid)chrome.Child;
                grid.Children[2].IsVisibleChanged += CustomButtonTool_IsVisibleChanged;
            }

            var baseSize=base.MeasureOverride(constraint);
            return baseSize;
        }

        void CustomButtonTool_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }
    }
}
