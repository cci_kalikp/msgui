﻿using System.Windows;
using Infragistics.Windows.Ribbon;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    internal sealed class ContextRibbonTabArea : RibbonTabArea, IContextRibbonTab
    {
        public ContextRibbonTabArea(string id, InitialWidgetParameters parameters) : base(id, parameters)
        {
        }

        public string Name
        {
            get
            {
                return ((RibbonTabItem) Content).Header.ToString();
            }
        }

        public bool IsVisible
        {
            get { return ((RibbonTabItem) Content).Visibility == Visibility.Visible; }
            set
            {
                var newVisibility = value ? Visibility.Visible : Visibility.Hidden;
                ((RibbonTabItem) Content).Visibility = newVisibility;
            }
        }

    }
}
