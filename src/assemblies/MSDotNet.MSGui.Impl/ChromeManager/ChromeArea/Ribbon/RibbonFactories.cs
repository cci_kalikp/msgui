﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Threading;
using System.Xml.Linq;
using Infragistics.Windows.Ribbon;
using Infragistics.Windows.Ribbon.Events;
using MorganStanley.MSDotNet.MSGui.Controls;
using MorganStanley.MSDotNet.MSGui.Controls.BarMenu;
using MorganStanley.MSDotNet.MSGui.Controls.SplitButton;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon.Widgets;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls; 
using MorganStanley.MSDotNet.My;
using ToolMenuItem = Infragistics.Windows.Ribbon.ToolMenuItem;


namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    internal sealed class RibbonFactories : ICommonFactories
    {
        private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<RibbonFactories>();
        private static IChromeManager _chromeManager;
        private static IShell _shell;
        private static bool _staticFactoriesInitialized;

        private static readonly IDictionary<ButtonViewContainer, PropertyChangedEventHandler>
            ButtonContainerPropertyChangedHandlers = new Dictionary<ButtonViewContainer, PropertyChangedEventHandler>();

        private static readonly KeyGestureConverter GestureConverter = new KeyGestureConverter();

        private readonly IChromeRegistry _registry;
        private readonly IDictionary<string, ICommand> _commandCache = new Dictionary<string, ICommand>();

        public RibbonFactories(IChromeRegistry registry)
        {
            _registry = registry;
            _registry.RegisterWidgetFactoryMapping<InitialButtonParameters>(ButtonFactory);
            _registry.RegisterWidgetFactoryMapping<InitialCommandButtonParameters>(CommandButtonFactory);
            _registry.RegisterWidgetFactoryMapping<InitialSplitButtonParameters>(SplitButtonFactory);
            _registry.RegisterWidgetFactoryMapping<InitialShowWindowButtonParameters>(ShowWindowButtonFactory); 
            _registry.RegisterWidgetFactoryMapping<InitialExistingElementWidgetParameters>(ShowExistingElementFactory);
            _registry.RegisterWidgetFactoryMapping<InitialDropdownButtonParameters>(DropdownButtonFactory);
            _registry.RegisterWidgetFactoryMapping<InitialComboBoxParameters>(ComboBoxFactory);
            _registry.RegisterWidgetFactoryMapping<InitialPopupParameters>(PopupFactory);
            _registry.RegisterWidgetFactoryMapping<InitialDropdownSeparatorParameters>(DropdownSeparatorFactory);
            _registry.RegisterWidgetFactoryMapping<InitialPanelParameters>(PanelFactory);
            _registry.RegisterWidgetFactoryMapping<InitialButtonGroupParameters>(ButtonGroupFactory);

            if (ChromeManagerBase.DefaultWidgetFactoriesGlobal.HasValue)
            {
                _registry.RegisterWidgetDehydrateCallbackMapping<InitialShowWindowButtonParameters>(ShowWindowButtonDehydrateCallbackMethod);
                _registry.RegisterWidgetDehydrateCallbackMapping<InitialButtonParameters>(ButtonDehydrateCallbackMethod);
                _registry.RegisterWidgetDehydrateCallbackMapping<InitialCommandButtonParameters>(CommandButtonDehydrateCallbackMethod);
            }
        }

        internal void Initialize(IChromeManager chromeManager)
        {
            _chromeManager = chromeManager;
            if (!_staticFactoriesInitialized)
            {
                InitializeRibbonFactories(_registry);
            }
        }

        internal static void Initialize(RibbonChromeManager chromeManager, IShell shell)
        {
            _chromeManager = chromeManager;
            _shell = shell;

            if (!_staticFactoriesInitialized)
            {
                InitializeRibbonFactories(chromeManager.Registry);
            }
        }

        private static void InitializeRibbonFactories(IChromeRegistry chromeRegistry)
        {
            chromeRegistry.RegisterWidgetFactoryMapping<InitialRibbonParameters>(RibbonRootFactory);
            chromeRegistry.RegisterWidgetFactoryMapping<InitialRibbonTabParameters>(RibbonTabFactory);
            chromeRegistry.RegisterWidgetFactoryMapping<InitialContextRibbonTabParameters>(ContextRibbonTabFactory);
            chromeRegistry.RegisterWidgetFactoryMapping<InitialQatParameters>(RibbonQATFactory);
            chromeRegistry.RegisterWidgetFactoryMapping<InitialRibbonGroupParameters>(RibbonGroupFactory);
            chromeRegistry.RegisterWidgetFactoryMapping<InitialRibbonContextParameters>(RibbonContextFactory);
            chromeRegistry.RegisterWidgetFactoryMapping<InitialRibbonGalleryParameters>(GalleryFactoryMethod);
            chromeRegistry.RegisterWidgetFactoryMapping<InitialRibbonGalleryGroupParameters>(GalleryGroupFactoryMethod);
            chromeRegistry.RegisterWidgetFactoryMapping<InitialRibbonGalleryItemParameters>(GalleryItemFactoryMethod);
            chromeRegistry.RegisterWidgetFactoryMapping<InitialTopContentParameters>(TopContentFactory);
            _staticFactoriesInitialized = true;
        }

        #region ICommonFactories Members

        public InitializeWidgetHandler ButtonFactory
        {
            get { return ButtonFactoryMethod; }
        }

        public InitializeWidgetHandler CommandButtonFactory
        {
            get { return CommandButtonFactoryMethod; }
        }

        public InitializeWidgetHandler SplitButtonFactory
        {
            get { return SplitButtonFactoryMethod; }
        }

        public InitializeWidgetHandler ShowWindowButtonFactory
        {
            get { return ShowWindowButtonFactoryMethod; }
        } 

        public InitializeWidgetHandler ShowExistingElementFactory
        {
            get { return ShowExistingElementFactoryMethod; }
        }

        public InitializeWidgetHandler DropdownButtonFactory
        {
            get { return DropdownButtonFactoryMethod; }
        }

        public InitializeWidgetHandler ComboBoxFactory
        {
            get { return ComboBoxFactoryMethod; }
        }

        public InitializeWidgetHandler PopupFactory
        {
            get { return PopupFactoryMethod; }
        }

        public InitializeWidgetHandler DropdownSeparatorFactory
        {
            get { return DropdownSeparatorMethod; }
        }

        public InitializeWidgetHandler PanelFactory
        {
            get { return PanelFactoryMethod; }
        }

        public InitializeWidgetHandler ButtonGroupFactory
        {
            get { return ButtonGroupFactoryMethod; }
        }

        #endregion


        [FactoryOptions(typeof(TopContentRootArea))]
        private static bool TopContentFactory(IWidgetViewContainer vc, XDocument state)
        {
            var shellWindow = _shell.MainWindow as ShellWindow; 
            if (shellWindow == null) return false;
            vc.Content = shellWindow.TopContentArea;
            return true; 
        }

        [FactoryOptions(typeof (RibbonRootArea))]
        private static bool RibbonRootFactory(IWidgetViewContainer vc, XDocument state)
        {
            vc.Content = ((RibbonChromeManager) _chromeManager).RibbonChromeManagerRibbon;
            return true;
        }

        [FactoryOptions(typeof(RibbonTabArea))]
        private static bool RibbonTabFactory(IWidgetViewContainer vc, XDocument state)
        {
            var parameters = vc.Parameters as InitialRibbonTabParameters;

            if (parameters != null &&
                (vc.Parent.ContentOfType<XamRibbon>() || vc.Parent.ContentOfType<ContextualTabGroup>()))
            {
                var ribbonTab = new RibbonTabItem { Header = vc.Parameters.Text };
                vc.Content = ribbonTab;
                return true;
            }

            return false;
        }

        [FactoryOptions(typeof(ContextRibbonTabArea))]
        private static bool ContextRibbonTabFactory(IWidgetViewContainer vc, XDocument state)
        {
            var parameters = vc.Parameters as InitialContextRibbonTabParameters;

            if (parameters != null &&
                (vc.Parent.ContentOfType<XamRibbon>() || vc.Parent.ContentOfType<ContextualTabGroup>()))
            {
                var ribbonTab = new RibbonTabItem { Header = vc.Parameters.Text };
                vc.Content = ribbonTab;
                return true;
            }

            return false;
        }

        [FactoryOptions(typeof (RibbonContextualArea))]
        private static bool RibbonContextFactory(IWidgetViewContainer vc, XDocument state)
        {
            var parameters = vc.Parameters as InitialRibbonContextParameters;
            if (vc.Parent.ContentOfType<XamRibbon>())
            {
                var ribbonContext = new ContextualTabGroup(parameters.Text)
                    {
                        Key = parameters.Key,
                        Caption = parameters.Text,
                        BaseBackColor = parameters.Color,
                        IsVisible = false
                    };

                vc.Content = ribbonContext;

                return true;
            }

            return false;
        }

        [FactoryOptions(typeof(WidgetViewContainer))]
        private bool ShowExistingElementFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialExistingElementWidgetParameters;
            emptyWidget.Content = parameters.Element;
            return true;
        }

        [FactoryOptions(typeof (RibbonGroupArea))]
        private static bool RibbonGroupFactory(IWidgetViewContainer vc, XDocument state)
        {
            var parameters = vc.Parameters as InitialRibbonGroupParameters;
            if (vc.Parent.ContentOfType<RibbonTabItem>())
            {
                var ribbonGroup = new RibbonGroup {Caption = vc.Parameters.Text};
                if (parameters != null)
                {
                    foreach (var groupVariant in parameters.GroupVariants)
                    {
                        Infragistics.Windows.Ribbon.GroupVariantResizeAction resizeAction;
                        Enum.TryParse(groupVariant.ResizeAction.ToString(), out resizeAction);
                        ribbonGroup.Variants.Add(new Infragistics.Windows.Ribbon.GroupVariant
                            {
                                ResizeAction = resizeAction,
                            });
                    }
                }
                if(parameters != null && parameters.SmallImage != null)
                {
                    ribbonGroup.SmallImage = parameters.SmallImage;
                }
                vc.Content = ribbonGroup;
                return true;
            }

            return false;
        }


        [FactoryOptions(typeof (RibbonQATArea))]
        private static bool RibbonQATFactory(IWidgetViewContainer vc, XDocument state)
        {
            vc.Content = ((RibbonChromeManager) _chromeManager).RibbonChromeManagerRibbon;
            ((RibbonQATArea)vc).DisableHack = ((RibbonChromeManager) _chromeManager).Settings.DisableQATHack;
            return true;
        }



        [FactoryOptions(typeof (ButtonViewContainer))]
        private bool ButtonFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialButtonParameters;
            if (parameters != null)
            {
                object button;
                if (emptyWidget.Content == null)
                {
                    if (emptyWidget.Parent is IDropdownButtonViewContainer)
                    {
                        MenuItem menuItem = null;
                        if (emptyWidget.Parent.Parent is HeaderItemsHolderWidgetContainer)
                        {
                            menuItem = new Controls.BarMenu.ToolMenuItem
                            {
                                Header = parameters.Text,
                                ToolTip = parameters.ToolTip,
                                Icon = parameters.Image == null ? null : new Image() { Source = parameters.Image },
                                IsEnabled = parameters.Enabled,
                                IsCheckable = parameters.IsCheckable,
                                IsChecked = parameters.IsChecked,
                                InputGestureText = parameters.Gesture == null ? string.Empty : parameters.Gesture.GetDisplayStringForCulture(Thread.CurrentThread.CurrentUICulture)
                            };
                        }
                        else
                       {
                           menuItem = new ToolMenuItem
                           {
                               Header = parameters.Text,
                               ToolTip = parameters.ToolTip,
                               Icon = parameters.Image == null ? null : new Image() { Source = parameters.Image },
                               IsEnabled = parameters.Enabled,
                               IsCheckable = parameters.IsCheckable,
                               IsChecked = parameters.IsChecked,
                               InputGestureText = parameters.Gesture == null ? string.Empty : parameters.Gesture.GetDisplayStringForCulture(Thread.CurrentThread.CurrentUICulture)
                           };
                       }
                      
                       if (parameters.Click != null)
                       {
                           menuItem.Click += parameters.Click.ConvertToRouted();
                       }

                       emptyWidget.Content = menuItem;
                        button = menuItem;

                    }
                    else if (emptyWidget.Parent is ExtraContextMenuWidgetContainer)
                    {
                        var menuItem = new MenuItem
                            {
                                Header = parameters.Text,
                                ToolTip = parameters.ToolTip,
                                Icon = parameters.Image == null ? null : new Image() { Source = parameters.Image },
                                IsEnabled = parameters.Enabled,
                                IsCheckable = parameters.IsCheckable,
                                IsChecked = parameters.IsChecked,
                                InputGestureText = parameters.Gesture == null ? string.Empty : parameters.Gesture.GetDisplayStringForCulture(Thread.CurrentThread.CurrentUICulture)
                            };
                        if (parameters.Click != null)
                        {
                            menuItem.Click += parameters.Click.ConvertToRouted();
                        }

                        emptyWidget.Content = menuItem;
                        button = menuItem;
                    }
                    else if (emptyWidget.Parent is TopContentRootArea)
                    {
                        button = new LauncherBarButton(new MSGuiButton()
                        {
                            OnClick = parameters.Click,
                            Text = parameters.Text,
                            ToolTip = parameters.ToolTip,
                            Image = parameters.Image,
                            IsEnabled = parameters.Enabled,
                            ButtonSize = ButtonSize.Small
                        });
                        emptyWidget.Content = button;
                    }
                    else
                    {
                        if (parameters.IsCheckable)
                        {
                            var toggleButton = new ToggleButtonTool()
                                {
                                    Id = emptyWidget.FactoryID,
                                    Content = parameters.Text,
                                    ToolTip = parameters.ToolTip,
                                    LargeImage = parameters.Image,
                                    SmallImage = parameters.SmallImage,
                                    IsEnabled = parameters.Enabled,
                                    IsChecked = parameters.IsChecked
                                };
                            switch (parameters.Size)
                            {
                                case ButtonSize.Large:
                                    RibbonGroup.SetMinimumSize(toggleButton, RibbonToolSizingMode.ImageAndTextLarge);
                                    RibbonGroup.SetMaximumSize(toggleButton, RibbonToolSizingMode.ImageAndTextLarge);
                                    break;
                                case ButtonSize.Small:
                                    RibbonGroup.SetMinimumSize(toggleButton, RibbonToolSizingMode.ImageAndTextNormal);
                                    RibbonGroup.SetMaximumSize(toggleButton, RibbonToolSizingMode.ImageAndTextNormal);
                                    break;
                                case ButtonSize.ImageOnly:
                                    RibbonGroup.SetMinimumSize(toggleButton, RibbonToolSizingMode.ImageOnly);
                                    RibbonGroup.SetMaximumSize(toggleButton, RibbonToolSizingMode.ImageOnly);
                                    break;
                            }

                            if (parameters.Click != null)
                            {
                                toggleButton.Click += parameters.Click.ConvertToRouted();
                            }

                            emptyWidget.Content = toggleButton;
                            button = toggleButton;
                        }
                        else
                        {
                            var buttonTool = new ButtonTool
                                {
                                    Id = emptyWidget.FactoryID,
                                    Content = parameters.Text,
                                    ToolTip = parameters.ToolTip,
                                    LargeImage = parameters.Image,
                                    SmallImage = parameters.SmallImage,
                                    IsEnabled = parameters.Enabled,
                                };
                            switch (parameters.Size)
                            {
                                case ButtonSize.Large:
                                    RibbonGroup.SetMinimumSize(buttonTool, RibbonToolSizingMode.ImageAndTextLarge);
                                    RibbonGroup.SetMaximumSize(buttonTool, RibbonToolSizingMode.ImageAndTextLarge);
                                    break;
                                case ButtonSize.Small:
                                    RibbonGroup.SetMinimumSize(buttonTool, RibbonToolSizingMode.ImageAndTextNormal);
                                    RibbonGroup.SetMaximumSize(buttonTool, RibbonToolSizingMode.ImageAndTextNormal);
                                    break;
                                case ButtonSize.ImageOnly:
                                    RibbonGroup.SetMinimumSize(buttonTool, RibbonToolSizingMode.ImageOnly);
                                    RibbonGroup.SetMaximumSize(buttonTool, RibbonToolSizingMode.ImageOnly);
                                    break;
                            }

                           if (parameters.Click != null)
                           {
                               buttonTool.Click += parameters.Click.ConvertToRouted();
                           }

                           emptyWidget.Content = buttonTool;
                           button = buttonTool;
                        } 
                    } 
                }
                else
                {
                    button = emptyWidget.Content;
                }

                var buttonContainer = emptyWidget as ButtonViewContainer;
                if (buttonContainer != null)
                {
                    if (!_commandCache.ContainsKey(emptyWidget.FactoryID))
                    {
                        _commandCache[emptyWidget.FactoryID] = new DelegateCommand(p =>
                            {
                                if (parameters.Click != null)
                                    parameters.Click(button, EventArgs.Empty);
                            });
                    }
                    var command = _commandCache[emptyWidget.FactoryID];

                    SetupKeyboardGesture(buttonContainer, command, parameters, state);
                }

                return true;
            }

            return false;
        }

 

        [FactoryOptions(typeof (ButtonViewContainer))]
        private static bool CommandButtonFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialCommandButtonParameters;
            if (parameters != null)
            {
                if (emptyWidget.Content == null)
                {
                    var button = new ButtonTool
                        {
                            Id = emptyWidget.FactoryID,
                            Content = parameters.Text,
                            ToolTip = parameters.ToolTip,
                            LargeImage = parameters.Image,
                            SmallImage = parameters.Image,
                            IsEnabled = parameters.Enabled,

                        };
                    switch (parameters.Size)
                    {
                        case ButtonSize.Large:
                            RibbonGroup.SetMinimumSize(button, RibbonToolSizingMode.ImageAndTextLarge);
                            RibbonGroup.SetMaximumSize(button, RibbonToolSizingMode.ImageAndTextLarge);
                            break;
                        case ButtonSize.Small:
                            RibbonGroup.SetMinimumSize(button, RibbonToolSizingMode.ImageAndTextNormal);
                            RibbonGroup.SetMaximumSize(button, RibbonToolSizingMode.ImageAndTextNormal);
                            break;
                        case ButtonSize.ImageOnly:
                            RibbonGroup.SetMinimumSize(button, RibbonToolSizingMode.ImageOnly);
                            RibbonGroup.SetMaximumSize(button, RibbonToolSizingMode.ImageOnly);
                            break;
                    }

                    //TODO:
                    // there is a BUG !!
                    // if both CommandBinding and Command are NOT null
                    // you have to call 
                    //   Button.CommandBindings.Add(CommandBinding)
                    //   Button.Command = Command;
                    // but not the reverse order
                    // the reason could be 
                    // --- code from ButtonTool from InfragisticsWPF4.Ribbon.v14.1, Version=12.1.20121.2059, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb
                    // // Nested Types
                    //protected class ButtonToolProxy : RibbonToolProxy<ButtonTool>
                    //{
                    //    // Fields
                    //    private static DependencyProperty[] _bindProperties = new DependencyProperty[] { ContentControl.ContentProperty, ButtonBase.CommandTargetProperty, ButtonBase.CommandParameterProperty, ButtonBase.CommandProperty };
                    //    internal static readonly ButtonTool.ButtonToolProxy Instance = new ButtonTool.ButtonToolProxy();
                    // ...

                    if (parameters.CommandBinding != null)
                    {
                        button.CommandBindings.Add(parameters.CommandBinding);
                    }
                    button.SetValue(ButtonBase.CommandProperty, parameters.Command);
                    button.SetValue(ButtonBase.CommandParameterProperty, parameters.CommandParameter);
                    emptyWidget.Content = button;
                }

                var buttonContainer = emptyWidget as ButtonViewContainer;
                if (buttonContainer != null)
                {
                    var commands = new List<ICommand>();
                    if (parameters.Gesture != null)
                    {
                        if (parameters.CommandParameter != null)
                        {
                            m_logger.WarningWithFormat("CommandParameter is not bound to gesture for {0}", parameters.Text);
                        }
                        if (parameters.Command != null) commands.Add(parameters.Command);
                        if (parameters.CommandBinding != null && parameters.CommandBinding.Command != null)
                            commands.Add(parameters.CommandBinding.Command);
                    }

                    SetupKeyboardGesture(buttonContainer, commands, parameters, state);
                }

                return true;
            }

            return false;
        }


        [FactoryOptions(typeof(RibbonSplitDropdownArea))]
        private static bool SplitButtonFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialSplitButtonParameters;
            if (parameters != null)
            {
                var splitButton = new SplitButton
                    {
                        Usage = (emptyWidget.Parent is HeaderItemsHolderWidgetContainer) ? SplitButtonUsage.LauncherBarButton : SplitButtonUsage.RibbonButton,
                         ButtonSize = (emptyWidget.Parent is HeaderItemsHolderWidgetContainer) ? ButtonSize.Small :  parameters.Size 
                    };
                if (parameters.Click != null)
                {
                    splitButton.Click += parameters.Click.ConvertToRouted();
                }

                splitButton.IsEnabled = parameters.Enabled;
                splitButton.ToolTip = parameters.ToolTip;
                splitButton.Caption = parameters.Text;
                splitButton.Image = parameters.Image;
                splitButton.IsDropDownEnabled = parameters.IsDropDownEnabled;
                if (parameters.Size == ButtonSize.Large)
                {
                    RibbonGroup.SetMinimumSize(splitButton, RibbonToolSizingMode.ImageAndTextLarge);
                    RibbonGroup.SetMaximumSize(splitButton, RibbonToolSizingMode.ImageAndTextLarge);
                }
                else
                {
                    RibbonGroup.SetMinimumSize(splitButton, RibbonToolSizingMode.ImageAndTextNormal);
                    RibbonGroup.SetMaximumSize(splitButton, RibbonToolSizingMode.ImageAndTextNormal);
                }
                if (parameters.Opening != null)
                {
                    splitButton.Opening += parameters.Opening;
                } 
                if (parameters.Opened != null)
                {
                    splitButton.Opened += parameters.Opened;
                } 
                emptyWidget.Content = splitButton;
                return true;
            }

            return false;
        }
         

        [FactoryOptions(typeof (ButtonViewContainer))]
        private bool ShowWindowButtonFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialShowWindowButtonParameters;
            if (parameters != null)
            {
                if (emptyWidget.Content == null)
                { 
                    if (emptyWidget.Parent is IDropdownButtonViewContainer)
                    {
                        MenuItem menuItem = null;
                        if (emptyWidget.Parent.Parent is HeaderItemsHolderWidgetContainer)
                        {
                            menuItem = new Controls.BarMenu.ToolMenuItem
                            {
                                Header = parameters.Text,
                                ToolTip = parameters.ToolTip,
                                Icon = parameters.Image == null ? null : new Image() { Source = parameters.Image },
                                IsEnabled = parameters.Enabled, 
                                InputGestureText = parameters.Gesture == null ? string.Empty : parameters.Gesture.GetDisplayStringForCulture(Thread.CurrentThread.CurrentUICulture)
                            };
                        }
                        else
                        {
                            menuItem = new ToolMenuItem
                            {
                                Header = parameters.Text,
                                ToolTip = parameters.ToolTip,
                                Icon = parameters.Image == null ? null : new Image() { Source = parameters.Image },
                                IsEnabled = parameters.Enabled, 
                                InputGestureText = parameters.Gesture == null ? string.Empty : parameters.Gesture.GetDisplayStringForCulture(Thread.CurrentThread.CurrentUICulture)
                            };
                        }

                        menuItem.Click +=
                        new Action(
                            () => _chromeManager.CreateWindow(parameters.WindowFactoryID, parameters.InitialParameters))
                            .ConvertToRouted();
                        emptyWidget.Content = menuItem; 

                    }
                    else
                    {
                        var button = new ButtonTool /*CustomButtonTool*/
                        {
                            Id = emptyWidget.FactoryID,
                            Content = parameters.Text,
                            ToolTip = parameters.ToolTip,
                            LargeImage = parameters.Image,
                            SmallImage = parameters.Image,
                            IsEnabled = parameters.Enabled,
                        };

                        if (parameters.Size == ButtonSize.Large)
                        {
                            RibbonGroup.SetMinimumSize(button, RibbonToolSizingMode.ImageAndTextLarge);
                            RibbonGroup.SetMaximumSize(button, RibbonToolSizingMode.ImageAndTextLarge);
                        }
                        else
                        {
                            RibbonGroup.SetMinimumSize(button, RibbonToolSizingMode.ImageAndTextNormal);
                            RibbonGroup.SetMaximumSize(button, RibbonToolSizingMode.ImageAndTextNormal);
                        }

                        button.Click +=
                            new Action(
                                () => _chromeManager.CreateWindow(parameters.WindowFactoryID, parameters.InitialParameters))
                                .ConvertToRouted();
                        emptyWidget.Content = button; 
                    }
                   
                }

                var buttonContainer = emptyWidget as ButtonViewContainer;
                if (buttonContainer != null)
                {
                    if (!_commandCache.ContainsKey(emptyWidget.FactoryID))
                    {
                        _commandCache[emptyWidget.FactoryID] =
                            new DelegateCommand(p => _chromeManager.CreateWindow(parameters.WindowFactoryID,
                                                                                       parameters.InitialParameters));
                    }
                    var command = _commandCache[emptyWidget.FactoryID];

                    SetupKeyboardGesture(buttonContainer, command, parameters, state);
                }

                return true;
            }

            return false;
        }


        [FactoryOptions(typeof (RibbonDropdownArea))]
        private bool DropdownButtonFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialDropdownButtonParameters;
            if (parameters != null)
            {
                 if (emptyWidget.Parent is HeaderItemsHolderWidgetContainer)
                {
                    var dropdown = new BarMenuTool //I've created a MenuTool with modified foreground
                    {
                        Caption = parameters.Text,
                        IsEnabled = parameters.Enabled,
                        Image = parameters.Image ?? parameters.SmallImage, 
                    };
                    dropdown.SetResourceReference(Control.ForegroundProperty, MSGuiColors.TitlesBrushKey); 
                    if (parameters.Opening != null)
                    {
                        dropdown.Opening += parameters.Opening.ConvertToRouted();
                    }
                    if (parameters.Opened != null)
                    {
                        dropdown.Opened += parameters.Opened.ConvertToRouted();
                    }
                    emptyWidget.Content = dropdown;
                }
                else
                {
                    var dropdown = new MenuTool
                    {
                        Id = emptyWidget.FactoryID,
                        Caption = parameters.Text,
                        ToolTip = parameters.ToolTip,
                        LargeImage = parameters.Image,
                        SmallImage = parameters.SmallImage,
                        IsEnabled = parameters.Enabled
                    };

                    switch (parameters.Size)
                    {
                        case ButtonSize.Large:
                            RibbonGroup.SetMinimumSize(dropdown, RibbonToolSizingMode.ImageAndTextLarge);
                            RibbonGroup.SetMaximumSize(dropdown, RibbonToolSizingMode.ImageAndTextLarge);
                            break;
                        case ButtonSize.Small:
                            RibbonGroup.SetMinimumSize(dropdown, RibbonToolSizingMode.ImageAndTextNormal);
                            RibbonGroup.SetMaximumSize(dropdown, RibbonToolSizingMode.ImageAndTextNormal);
                            break;
                        case ButtonSize.ImageOnly:
                            RibbonGroup.SetMinimumSize(dropdown, RibbonToolSizingMode.ImageOnly);
                            RibbonGroup.SetMaximumSize(dropdown, RibbonToolSizingMode.ImageOnly);
                            break;
                    }

                    if (parameters.Opening != null)
                    {
                        dropdown.Opening += parameters.Opening.Convert<ToolOpeningEventArgs>();
                    }
                    if (parameters.Opened != null)
                    {
                        dropdown.Opened += parameters.Opened.ConvertToRouted();
                    }
                    emptyWidget.Content = dropdown;
                }
               
               
                return true;
            }

            return false;
        }

        [FactoryOptions(typeof(RibbonPanelArea))]
        private bool PanelFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialPanelParameters;
            if (parameters == null) return false;

            if (parameters.Wrap)
            {
                switch (parameters.Orientation)
                {
                    case InitialPanelParameters.Layout.Horizontal:
                        emptyWidget.Content = new ToolHorizontalWrapPanel();
                        break;

                    case InitialPanelParameters.Layout.Vertical:
                        emptyWidget.Content = new ToolVerticalWrapPanel();
                        break;
                }
            }
            else
            {
                emptyWidget.Content = new StackPanel
                {
                    Orientation = (parameters.Orientation == InitialPanelParameters.Layout.Horizontal) ? Orientation.Horizontal : Orientation.Vertical
                };
            }

            return true;
        }

        [FactoryOptions(typeof(RibbonButtonGroupArea))]
        private bool ButtonGroupFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialButtonGroupParameters;
            if (parameters == null) return false;
            
            emptyWidget.Content = new ButtonGroup();

            return true;
        }

        private bool ComboBoxFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialComboBoxParameters;
            if (parameters != null)
            {
                var combo = new ComboEditorTool
                    {
                        Id = emptyWidget.FactoryID,
                        Caption = parameters.Text,
                        ToolTip = parameters.ToolTip,
                        LargeImage = parameters.Image,
                        SmallImage = parameters.Image,
                        IsEnabled = parameters.Enabled,
                        IsEditable = parameters.Editable,
                        ItemsSource = parameters.ItemsSource,
                    };

                if (parameters.DropDownOpened != null)
                    combo.DropDownOpened += parameters.DropDownOpened;

                emptyWidget.Content = combo;
                return true;
            }

            return false;
        }

        private static bool PopupFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialPopupParameters;
            if (parameters != null)
            { 
                var menuTool = new MenuTool
                    {
                        Id = emptyWidget.FactoryID,
                        Caption = parameters.Text,
                        ButtonType = MenuToolButtonType.DropDown,
                        IsEnabled = parameters.Enabled,
                        LargeImage = parameters.Image,
                        SmallImage = parameters.Image,
                        ItemsSource = parameters.ItemsSource
                    };

                menuTool.SetValue(RibbonGroup.MaximumSizeProperty, RibbonToolSizingMode.ImageAndTextNormal);

                emptyWidget.Content = menuTool;
                return true;
            }

            return false;
        }

        [FactoryOptions(typeof (RibbonGalleryViewContainer))]
        private static bool GalleryFactoryMethod(IWidgetViewContainer vc, XDocument state)
        {
            var parameters = vc.Parameters as InitialRibbonGalleryParameters;
            if (parameters == null)
                return false;

            var gallery = vc as RibbonGalleryViewContainer;
            if (gallery == null)
                return false;

            var galleryTool = new GalleryTool
                {
                    MinPreviewColumns = parameters.MinPreviewColumns,
                    MaxPreviewColumns = parameters.MaxPreviewColumns,
                    MinDropDownColumns = parameters.MinDropDownColumns,
                    MaxDropDownColumns = parameters.MaxDropDownColumns
                };

            if (parameters.ItemClicked != null)
                gallery.ItemClicked += parameters.ItemClicked;

            var menuTool = new MenuTool
                {
                    Caption = parameters.Text
                };

            menuTool.Items.Add(galleryTool);

            vc.Content = menuTool;
            return true;
        }

        [FactoryOptions(typeof (RibbonGalleryGroupViewContainer))]
        private static bool GalleryGroupFactoryMethod(IWidgetViewContainer vc, XDocument state)
        {
            var parameters = vc.Parameters as InitialRibbonGalleryGroupParameters;
            if (parameters == null)
                return false;

            var group = new GalleryItemGroup
                {
                    Title = parameters.Text
                };

            vc.Content = group;
            return true;
        }

        [FactoryOptions(typeof (RibbonGalleryItemViewContainer))]
        private static bool GalleryItemFactoryMethod(IWidgetViewContainer vc, XDocument state)
        {
            var parameters = vc.Parameters as InitialRibbonGalleryItemParameters;
            if (parameters == null)
                return false;

            var item = vc as RibbonGalleryItemViewContainer;
            if (item == null)
                return false;

            var galleryItem = new GalleryItem
                {
                    Key = vc.FactoryID,
                    Text = parameters.Text,
                    Image = parameters.Image
                };

            item.Click += parameters.Click;

            vc.Content = galleryItem;
            return true;
        }

        public bool DropdownSeparatorMethod(IWidgetViewContainer viewContainer_, XDocument state_)
        {
            IWidgetViewContainer current = viewContainer_;
            bool isInHeaderItems = false;
            while (current.Parent != null)
            {
                current = current.Parent;
                if (current is HeaderItemsHolderWidgetContainer)
                {
                    isInHeaderItems = true;
                    break;
                }
            }
            object separator;
            if (!isInHeaderItems)
            {
                separator = new Separator();
            }
            else
            {
                separator = new Controls.BarMenu.ToolMenuItem() { Header = new Separator() };
            }
            viewContainer_.Content = separator;
            return true;
        } 

        #region Default dehydration callbacks

        private XDocument ShowWindowButtonDehydrateCallbackMethod(IWidgetViewContainer widgetViewContainer)
        {
            var result = new XElement("ShowWindowButtonViewContainer");
            AddGestureElement(widgetViewContainer, result);
            return new XDocument(result);
        }

        private XDocument ButtonDehydrateCallbackMethod(IWidgetViewContainer widgetViewContainer)
        {
            var result = new XElement("ButtonViewContainer");
            AddGestureElement(widgetViewContainer, result);
            return new XDocument(result);
        }

        private XDocument CommandButtonDehydrateCallbackMethod(IWidgetViewContainer widgetViewContainer)
        {
            var result = new XElement("CommandButtonViewContainer");
            AddGestureElement(widgetViewContainer, result);
            return new XDocument(result);
        }

        private static void AddGestureElement(IWidgetViewContainer widgetViewContainer, XContainer result)
        {
            if (((ButtonViewContainer) widgetViewContainer).Gesture != null)
            {
                var gesture = new XElement("Gesture");
                var gestureAsString = GestureConverter.ConvertToString(((ButtonViewContainer) widgetViewContainer).Gesture);
                gesture.SetAttributeValue("value", gestureAsString);
                result.Add(gesture);
            }
        }

        #endregion

        private static void SetupKeyboardGesture(ButtonViewContainer buttonContainer, ICommand command,
                                                 ISupportsGesture parameters, XDocument state)
        {
            SetupKeyboardGesture(buttonContainer, new[] {command}, parameters, state);
        }

        private static void SetupKeyboardGesture(ButtonViewContainer buttonContainer, IEnumerable<ICommand> commands,
                                                 ISupportsGesture parameters, XDocument state)
        {
            PropertyChangedEventHandler buttonContainerOnPropertyChanged = (sender, args) =>
                {
                    if (args.PropertyName != "Gesture") return;
                    if (buttonContainer.Gesture != null)
                    {
                        foreach (var command in commands)
                        {
                            Action insertBindingsAction = () =>
                                {
                                    var inputBinding = new InputBinding(command, buttonContainer.Gesture);
                                    _shell.MainWindow.InputBindings.Add(inputBinding);
                                };

                            Dispatcher.CurrentDispatcher.Invoke(insertBindingsAction);
                        }
                    }
                    else if (_shell != null)
                    {
                        foreach (var command in commands)
                        {
                            var command1 = command;
                            var removeBindingAction = new Action(() =>
                                {
                                    var bindingToRemove =
                                        _shell.MainWindow.InputBindings.Cast<InputBinding>()
                                              .FirstOrDefault(b => b.Command == command1);
                                    if (bindingToRemove != null)
                                    {
                                        _shell.MainWindow.InputBindings.Remove(bindingToRemove);
                                    }
                                });
                            Dispatcher.CurrentDispatcher.Invoke(removeBindingAction);
                        }
                    }
                };

            if (ButtonContainerPropertyChangedHandlers.ContainsKey(buttonContainer))
            {
                buttonContainer.PropertyChanged -= ButtonContainerPropertyChangedHandlers[buttonContainer];
                ButtonContainerPropertyChangedHandlers.Remove(buttonContainer);
            }
            buttonContainer.PropertyChanged += buttonContainerOnPropertyChanged;
            ButtonContainerPropertyChangedHandlers[buttonContainer] = buttonContainerOnPropertyChanged;

            buttonContainer.Gesture = parameters.Gesture;
            if (state != null)
            {
                var gestureEl = state.Root.Descendants("Gesture").FirstOrDefault();
                if (gestureEl != null)
                {
                    buttonContainer.Gesture =
                        (KeyGesture) GestureConverter.ConvertFromString(gestureEl.Attribute("value").Value);
                }
            }
        }
    }
}
