﻿using System.Windows;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    class TopContentRootArea : ObservableWidgetViewContainer
    {
        private DockPanel topArea; 
        public TopContentRootArea(string ID, InitialWidgetParameters parameters)
			: base(ID, parameters)
		{
			this.ContentReady += (s, e) =>
			    {
                    topArea = (DockPanel)Content;
			        topArea.LastChildFill = false;
			    };
		}
        protected override void DoInsertVisual(int index, IWidgetViewContainer child)
        {
            UIElement content = child.Content as UIElement;
            if (content != null)
            {
                this.topArea.Children.Insert(index, content);
            }
            base.DoInsertVisual(index, child);
        }
        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            FrameworkElement content = child.Content as FrameworkElement;
            if (content != null)
            {
                topArea.Visibility = Visibility.Visible;
                topArea.Children.Add(content);
                return base.DoAddVisual(child);

            } 
            return false;
        }

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            if (child.ContentOfType<UIElement>())
            {
                topArea.Children.Remove((UIElement)child.Content);
                if (topArea.Children.Count == 0)
                {
                    topArea.Visibility = Visibility.Collapsed;
                }
            }
            base.DoRemoveVisual(child);
        }
         

        public override void SwapChildren(IWidgetViewContainer a_, IWidgetViewContainer b_)
        {
            base.SwapChildren(a_, b_);
            UIElement aContent = a_.Content as UIElement;
            UIElement bContent = b_.Content as UIElement;
            var indexA = topArea.Children.IndexOf(aContent);
            var indexB = topArea.Children.IndexOf(bContent); 
            topArea.Children[indexB] = null;
            topArea.Children[indexA] = bContent;
            topArea.Children[indexB] = aContent;
        }

        public override string NodeName
        {
            get
            {
                return "TopContent";
            }
        }
    }
}
