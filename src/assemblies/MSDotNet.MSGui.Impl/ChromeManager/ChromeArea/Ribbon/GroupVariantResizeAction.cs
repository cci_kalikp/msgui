﻿namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    public enum GroupVariantResizeAction
    {
        /// <summary>
        /// The RibbonGroup will be displayed as a dropdown button that can be opened to display the contents of the RibbonGroup in a popup.
        /// </summary>
        CollapseRibbonGroup, 
        /// <summary>
        /// A MenuTool whose MenuTool.ShouldDisplayGalleryPreview is true and contains a GalleryTool (and therefore is displaying a preview of that GalleryTool) will be displayed as a RibbonToolSizingMode.ImageAndTextLarge sized tool without the gallery preview.
        /// </summary>
        HideGalleryPreview, 
        /// <summary>
        /// The number of rows (ToolHorizontalWrapPanel.RowCount) displayed within a ToolHorizontalWrapPanel will be increased from its ToolHorizontalWrapPanel.MinRows towards the ToolHorizontalWrapPanel.MaxRows.
        /// </summary>
        IncreaseHorizontalWrapRowCount, 
        /// <summary>
        /// The number of columns displayed in the preview of a GalleryTool within a MenuTool will be reduced from its GalleryTool.MaxPreviewColumns to the GalleryTool.MinPreviewColumns.
        /// </summary>
        ReduceGalleryPreviewItems, 
        /// <summary>
        /// Consecutive tools whose RibbonToolHelper.SizingModeProperty is resolved to RibbonToolSizingMode.ImageAndTextLarge will be reduced to vertically stacked tools with a SizingMode of RibbonToolSizingMode.ImageAndTextNormal.
        /// </summary>
        ReduceImageAndTextLargeTools, 
        /// <summary>
        /// Consecutive tools whose RibbonToolHelper.SizingModeProperty is resolved to RibbonToolSizingMode.ImageAndTextNormal will be reduced to tools with a SizingMode of RibbonToolSizingMode.ImageOnly.
        /// </summary>
        ReduceImageAndTextNormalTools
    }
}
