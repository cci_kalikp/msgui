﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Media;
using Infragistics.Windows.Ribbon;
using MorganStanley.MSDotNet.MSGui.Annotations;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using GroupVariantResizeAction = MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon.GroupVariantResizeAction;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    public static class RibbonExtensions
    {
        public static void AddVariantGroup(this IRibbonGroup group, GroupVariantResizeAction resizeAction, int priority)
        {
            if (group.Content == null)
            {
                var parameters = (InitialRibbonGroupParameters)group.Parameters;
                parameters.GroupVariants.Add(new GroupVariant() { Priority = priority, ResizeAction = resizeAction });
            }
            else
            {
                var g = group.Content as RibbonGroup;
                Infragistics.Windows.Ribbon.GroupVariantResizeAction igResizeAction;
                Enum.TryParse(resizeAction.ToString(), out igResizeAction);
                g.Variants.Add(new Infragistics.Windows.Ribbon.GroupVariant() { ResizeAction = igResizeAction, Priority = priority });    
            }

            
        }

        public static void ClearVariantGroups(this IRibbonGroup group)
        {
            if (group.Content == null)
            {
                var parameters = (InitialRibbonGroupParameters)group.Parameters;
                parameters.GroupVariants.Clear();
            }
            else
            {
                var g = group.Content as RibbonGroup;
                g.Variants.Clear();     
            }

           
        }

        public static IEnumerable<GroupVariant> GetVariants(this IRibbonGroup group)
        {
            if (group.Content == null)
            {
                var parameters = (InitialRibbonGroupParameters)group.Parameters;
                return parameters.GroupVariants;
            }
            else
            {
                var g = group.Content as RibbonGroup;

                var variants = from groupVariant in g.Variants
                               let resizeAction = (GroupVariantResizeAction)Enum.Parse(typeof(GroupVariantResizeAction), groupVariant.ResizeAction.ToString())
                               select new GroupVariant() { ResizeAction = resizeAction, Priority = groupVariant.Priority };


                return variants;
            }
        }

        public static void SetGroupSmallImage([NotNull]this IRibbonGroup group, [NotNull]ImageSource smallImage)
        {
            
            if (group.Content == null)
            {
                var parameters = (InitialRibbonGroupParameters)group.Parameters;
                parameters.SmallImage = smallImage;
                return;
            }
            else
            {
                var g = group.Content as RibbonGroup;
                g.SmallImage = smallImage;    
            }
            
        }

        public static ImageSource GetGroupSmallImage(this IRibbonGroup group)
        {
            if (group.Content == null)
            {
                var parameters = (InitialRibbonGroupParameters)group.Parameters;
                return parameters.SmallImage;
            }
            else
            {
                var g = group.Content as RibbonGroup;
                return g.SmallImage;
            }
           
        }
    }
}