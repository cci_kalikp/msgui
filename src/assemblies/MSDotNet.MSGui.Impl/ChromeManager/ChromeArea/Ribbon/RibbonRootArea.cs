﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Infragistics.Windows.Ribbon;
using Infragistics.Windows.Ribbon.Events;
using MorganStanley.MSDotNet.MSGui.Controls.Controls;
using MorganStanley.MSDotNet.MSGui.Controls.Interfaces;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    internal sealed class RibbonRootArea : ObservableWidgetViewContainer, IRibbon, IRibbonContexts, ILockerAffectedMainArea
	{
        XamRibbon _ribbon;
        private readonly IList<IRibbonContext> _ribbonContexts = new List<IRibbonContext>();

		public RibbonRootArea(string ID, InitialWidgetParameters parameters) : base(ID, parameters)
		{
		    ContentReady += OnContentReady;
		}

        private void OnContentReady(object s, EventArgs e)
        {
            _ribbon = (XamRibbon)Content;
            if (icon != null)
            {
                Icon = icon;
            }
            _ribbon.RibbonTabItemSelected += HandleRibbonTabSelected;
        }

        private void HandleRibbonTabSelected(object sender, RibbonTabItemSelectedEventArgs args)
        {
            var eventArgs = new ActiveTabChangedEventArgs
                {
                    New = args.NewSelectedRibbonTabItem != null
                              ? args.NewSelectedRibbonTabItem.Header.ToString()
                              : null,
                    Old = args.PreviousSelectedRibbonTabItem != null
                              ? args.PreviousSelectedRibbonTabItem.Header.ToString()
                              : null
                };

            OnActiveTabChanged(eventArgs);
        }

        protected override bool DoAddVisual(IWidgetViewContainer child)
		{
			if (child.ContentOfType<RibbonTabItem>())
			{
				_ribbon.Tabs.Add(child.Content as RibbonTabItem);

                return base.DoAddVisual(child);
			}
            if (child.ContentOfType<ContextualTabGroup>())
			{
                _ribbon.ContextualTabGroups.Add((ContextualTabGroup)child.Content);
			    _ribbonContexts.Add((IRibbonContext) child);

                return base.DoAddVisual(child);
			}
		    return false;
		}

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            if (child.ContentOfType<RibbonTabItem>())
            {
                _ribbon.Tabs.Remove(child.Content as RibbonTabItem); 
            
            }
            else if (child.ContentOfType<ContextualTabGroup>())
            {
                _ribbon.ContextualTabGroups.Remove((ContextualTabGroup)child.Content);
                _ribbonContexts.Remove((IRibbonContext) child);
            }
            base.DoRemoveVisual(child);
        }

        protected override InitialWidgetParameters DoAddDefault(ref string ID)
		{
			var newTab = new InitialRibbonTabParameters { Text = ID };
			ID = Guid.NewGuid().ToString();
			return newTab;
		}

		protected override bool DoCanAddwidget(IWidgetViewContainer widget)
		{
			return widget is RibbonTabArea;
		}

		protected override bool DoContains(string index)
		{
			return (this.m_children.Values.FirstOrDefault(vc => vc.Parameters != null && vc.Parameters.Text == index) != null) || this.m_children.ContainsKey(index);
		}

		protected override IWidgetViewContainer DoGetWidget(string index)
		{
			return this.m_children.Values.FirstOrDefault(vc => vc.Parameters != null && vc.Parameters.Text == index) ?? this.m_children[index];
		}

        public override void SwapChildren(IWidgetViewContainer a_, IWidgetViewContainer b_)
        {
            base.SwapChildren(a_, b_);
            var indexA = _ribbon.Tabs.IndexOf(a_.Content as RibbonTabItem);
            var indexB = _ribbon.Tabs.IndexOf(b_.Content as RibbonTabItem);
            _ribbon.Tabs[indexB] = new RibbonTabItem();
            _ribbon.Tabs[indexA] = b_.Content as RibbonTabItem;
            _ribbon.Tabs[indexB] = a_.Content as RibbonTabItem;
        }

        public override string NodeName
        {
            get
            {
                return "Ribbon";
            }
        }

        protected override string DoPadLocationStringWithDefaults(string location)
		{
			if (location.Contains('/'))
			{
				var path = location.Split('/');
				if (path.Length > 2)
				{
					return location;
				}
				else
				{
					return string.Format("{0}/{1}",
						path[0].Trim() == string.Empty ? "Main" : path[0],
						path[1].Trim() == string.Empty ? "Tools" : path[1]);
				}
			}
			else
			{
				return string.Format("Main/{0}",
					location.Trim() == string.Empty ? "Tools" : location);
			}	
		}
	
		#region IRibbon Members

		IRibbonTab IRibbon.this[string tabName]
		{
			get 
			{
				return this[tabName] as IRibbonTab; 
			}
		}

		IRibbonTab IRibbon.AddTab(string tabName)
		{
			return this.AddWidget(tabName, new InitialRibbonTabParameters() { Text = tabName }) as IRibbonTab;
		}

        private ImageSource icon;
        public ImageSource Icon
        {
            get { return _ribbon == null ? icon: this._ribbon.ApplicationMenu.Image; }
            set
            {
                if (_ribbon != null)
                {
                    this._ribbon.ApplicationMenu.Image = value;
                }
                icon = value;
            }
        }
        

        public IRibbonContexts Contexts
        {
            get
            {
                return this as IRibbonContexts;
            }
        }

        public IRibbonContext AddContext(string id, string caption, System.Windows.Media.Color accentColour)
        {
            return this.AddWidget(Guid.NewGuid().ToString(), new InitialRibbonContextParameters()
            {
                Key = id,
                Text = caption, 
                Color = accentColour
            }) as IRibbonContext;
        }


        public IQuickAccessToolbar QAT 
        { 
            get
            {
                return ChromeManagerBase.Instance[RibbonChromeManager.QAT_COMMANDAREA_NAME] as RibbonQATArea; 
            }
        }
		#endregion

        IRibbonContext IRibbonContexts.this[string index]
        {
            get
            {
                var contextTabGroup = _ribbonContexts.FirstOrDefault(ctg =>
                    {
                        return ctg.Key.Equals(index, StringComparison.OrdinalIgnoreCase);
                    });
                return contextTabGroup;
            }
        }

        public void HideAllContexts()
        {
            foreach (var c in this._ribbon.ContextualTabGroups)
            {
                c.IsVisible = false;
            }
        }

        public void AddHostControl(IHwndHostControl control)
        {
            var ribbon = Ribbon as CustomTopRibbon;
            if (ribbon != null)
            {
                ribbon.AddHostControl(control);
            }
        }

        public bool TryGetRootVisual(out UIElement visual)
        {
            visual = null;

            var ribbon = Ribbon as CustomTopRibbon;
            if (ribbon != null && ribbon.IsPopupVisible())
            {
                visual = ribbon.RootElement;
                return true;
            }

            return false;
        }

        internal XamRibbon Ribbon
        {
            get { return _ribbon; }
        }

        public event EventHandler<ActiveTabChangedEventArgs> ActiveTabChanged;

        private void OnActiveTabChanged(ActiveTabChangedEventArgs e)
        {
            var handler = ActiveTabChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
	}
}
