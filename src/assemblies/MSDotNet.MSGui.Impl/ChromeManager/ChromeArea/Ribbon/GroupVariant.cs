﻿namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    public class GroupVariant
    {
        public GroupVariantResizeAction ResizeAction { get; set; }
        public int Priority { get; set; }
    }
}
