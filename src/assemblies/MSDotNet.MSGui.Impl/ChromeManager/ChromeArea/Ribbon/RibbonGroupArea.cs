﻿using System.Collections.Specialized;
using System.Windows;
using Infragistics.Windows.Ribbon;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    internal sealed class RibbonGroupArea : ObservableWidgetViewContainer, IRibbonGroup
    {
        public RibbonGroupArea(string ID, InitialWidgetParameters parameters)
            : base(ID, parameters)
        {
            ObservableChildren.CollectionChanged += ObservableChildrenOnCollectionChanged;
        }

        private void ObservableChildrenOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs eventArgs)
        {
            if (eventArgs.Action == NotifyCollectionChangedAction.Remove && ObservableChildren.Count == 0)
            {
                ObservableChildren.CollectionChanged -= ObservableChildrenOnCollectionChanged;
                chromeManager.RemoveWidget(this);
            }
        }

        protected override InitialWidgetParameters DoAddDefault(ref string ID)
        {
            return new InitialDropdownButtonParameters {Text = ID};
        }

        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            if (child.Content is DependencyObject)
            {
                RibbonToolHelper.SetId(child.Content as DependencyObject, child.FactoryID);
            }

            var group = (RibbonGroup) Content;
            group.Items.Add(child.Content);
            return base.DoAddVisual(child);
        }

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            var group = (RibbonGroup)Content;
            group.Items.Remove(child.Content);
            base.DoRemoveVisual(child);
        }

        public override void SwapChildren(IWidgetViewContainer a, IWidgetViewContainer b)
        {
            base.SwapChildren(a, b);
            var items = ((RibbonGroup)Content).Items;

            var indexA = items.IndexOf(a.Content);
            var indexB = items.IndexOf(b.Content);
            items[indexB] = null;
            items[indexA] = b.Content;
            items[indexB] = a.Content;
        }

        protected override bool DoCanAddwidget(IWidgetViewContainer widget)
        {
            return !(widget is RibbonTabArea || widget is RibbonRootArea || widget is RibbonGroupArea);
        }

        public override void Rename(string newName)
        {
            base.Rename(newName);
            ((RibbonGroup) Content).Caption = newName;
        }

        public override string NodeName
        {
            get { return "Group"; }
        }
    }
}

