﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Ribbon;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    internal class InitialRibbonParameters : InitialWidgetParameters
    {
        private readonly RibbonInfragisticsImpl _ribbonImpl;

        internal InitialRibbonParameters(RibbonInfragisticsImpl ribbonImpl)
        {
            _ribbonImpl = ribbonImpl;
        }

        public RibbonInfragisticsImpl RibbonImpl
        {
            get { return _ribbonImpl; }
        }
    }

    public class InitialRibbonContextParameters : InitialWidgetParameters
    {
        public Color Color { get; set; }
        public string Key { get; set; }
    }

    public class InitialRibbonTabParameters : InitialWidgetParameters
    {
    }

    public class InitialContextRibbonTabParameters : InitialWidgetParameters
    {
    }

    public class InitialRibbonGroupParameters : InitialWidgetParameters
    {
        private readonly HashSet<GroupVariant> _groupVariants = new HashSet<GroupVariant>();

        public ICollection<GroupVariant> GroupVariants
        {
            get { return _groupVariants; }
        }

        public ImageSource SmallImage { get; set; }
    }


    internal class InitialQatParameters : InitialWidgetParameters
    {
        private readonly RibbonInfragisticsImpl _ribbonImpl;

        internal InitialQatParameters(RibbonInfragisticsImpl ribbonImpl)
        {
            _ribbonImpl = ribbonImpl;
        }

        public RibbonInfragisticsImpl RibbonImpl
        {
            get { return _ribbonImpl; }
        }
    }

    public class InitialRibbonGalleryParameters : InitialWidgetParameters
    {
        public int MinPreviewColumns { get; set; }
        public int MaxPreviewColumns { get; set; }
        public int MinDropDownColumns { get; set; }
        public int MaxDropDownColumns { get; set; }
        public EventHandler<GalleryEventArgs> ItemSelected { get; set; }
        public EventHandler<GalleryEventArgs> ItemActivated { get; set; }
        public EventHandler<GalleryEventArgs> ItemClicked { get; set; }
    }

    public class InitialRibbonGalleryGroupParameters : InitialWidgetParameters
    {

    }

    public class InitialRibbonGalleryItemParameters : InitialWidgetParameters
    {
        public ImageSource Image { get; set; }
        public EventHandler<GalleryEventArgs> Click { get; set; }
    }

    internal class InitialTopContentParameters : InitialWidgetParameters
    {
    }
}
