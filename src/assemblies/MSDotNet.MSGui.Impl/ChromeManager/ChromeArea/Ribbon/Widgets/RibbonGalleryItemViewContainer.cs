﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infragistics.Windows.Ribbon.Events;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using Infragistics.Windows.Ribbon;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon.Widgets
{
    public sealed class RibbonGalleryItemViewContainer : WidgetViewContainer
    {
        private GalleryItem item;

        public RibbonGalleryItemViewContainer(string ID, InitialWidgetParameters parameters)
			: base(ID, parameters)
		{
			this.ContentReady += (s, e) =>
			{
			    this.item = this.Content as GalleryItem;
			    if (this.item == null)
			        return;

                if (this.text == null) //i.e. noone has set it before CreateChrome
                {
                    this.text = this.item.Text;
                }

                if (this.image == null) //i.e. noone has set it before CreateChrome
                {
                    this.image = this.item.Image;
                }
			};
		}

        #region

        private string text = null;
        public string Text
        {
            get
            {
                return this.text;
            }
            set
            {
                this.text = value;

                if (this.item != null)
                    this.item.Text = this.text ?? "";
            }
        }

        private ImageSource image = null;
        public ImageSource Image
        {
            get
            {
                return this.image;
            }
            set
            {
                this.image = value;

                if (this.item != null)
                    this.item.Image = this.image;
            }
        }

        public EventHandler<GalleryEventArgs> Click;

        #endregion

        internal void OnClick(RibbonGalleryViewContainer gallery, RibbonGalleryGroupViewContainer group)
        {
            var handler = this.Click;

            if (handler != null)
            {
                handler(this.item, new GalleryEventArgs()
                {
                    Gallery = gallery,
                    Group = group,
                    Item = this
                });
            }
        }
    }
}
