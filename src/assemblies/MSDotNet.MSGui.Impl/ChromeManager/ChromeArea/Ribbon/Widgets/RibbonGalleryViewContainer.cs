﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using Infragistics.Windows.Ribbon;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon.Widgets
{
    public sealed class RibbonGalleryViewContainer : WidgetViewContainer
    {
        private GalleryTool galleryTool;

        public RibbonGalleryViewContainer(string ID, InitialWidgetParameters parameters)
            : base(ID, parameters)
        {
            this.ContentReady += (s, e) =>
            {
                this.galleryTool = (this.Content as MenuTool).Items[0] as GalleryTool;
                if (this.galleryTool == null)
                    return;

                if (this.text == null) //i.e. noone has set it before CreateChrome
                {
                    this.text = this.galleryTool.Caption;
                }

                this.galleryTool.ItemClicked += galleryTool_ItemClicked;
            };
        }

        void galleryTool_ItemClicked(object sender, Infragistics.Windows.Ribbon.Events.GalleryItemEventArgs e)
        {
            this.OnItemClicked(e);

            RibbonGalleryViewContainer gallery = null;
            RibbonGalleryGroupViewContainer group = null;
            RibbonGalleryItemViewContainer item = null;

            this.ResolveIGEventArgs(e, out gallery, out group, out item);

            item.OnClick(gallery, group);
        }

        private void OnItemClicked(Infragistics.Windows.Ribbon.Events.GalleryItemEventArgs e)
        {
            var handler = this.ItemClicked;

            if (handler != null)
            {
                RibbonGalleryViewContainer gallery = null;
                RibbonGalleryGroupViewContainer group = null;
                RibbonGalleryItemViewContainer item = null;

                this.ResolveIGEventArgs(e, out gallery, out group, out item);

                this.ItemClicked(this, new GalleryEventArgs()
                {
                    Gallery = this,
                    Group = group,
                    Item = item
                });
            }
        }

        private void ResolveIGEventArgs(Infragistics.Windows.Ribbon.Events.GalleryItemEventArgs e, out RibbonGalleryViewContainer gallery,
            out RibbonGalleryGroupViewContainer group, out RibbonGalleryItemViewContainer item)
        {
            gallery = this;
            group = null;
            item = null;

            foreach (var child in this.Children)
            {
                if (child is RibbonGalleryGroupViewContainer)
                {
                    var g = child as RibbonGalleryGroupViewContainer;
                    if (g.Content == e.Group)
                    {
                        group = g;
                        foreach (var i in g.Children)
                        {
                            if (i is RibbonGalleryItemViewContainer && i.Content == e.Item)
                            {
                                item = i as RibbonGalleryItemViewContainer;
                                break; // we found both the group and the item, time to move on
                            }
                        }
                    }
                }

                if (child is RibbonGalleryItemViewContainer && child.Content == e.Item)
                {
                    item = child as RibbonGalleryItemViewContainer;
                    break; // we can break here as we found the item outside any group -> it is not in a group, stop looking for that
                }
            }

        }

        protected override bool DoCanAddwidget(IWidgetViewContainer widget)
        {
            return widget is RibbonGalleryGroupViewContainer || widget is RibbonGalleryItemViewContainer;
        }

        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            if (child is RibbonGalleryGroupViewContainer)
            {
                this.galleryTool.Groups.Add(child.Content as GalleryItemGroup);
                return base.DoAddVisual(child);
            }

            if (child is RibbonGalleryItemViewContainer)
            {
                this.galleryTool.Items.Add(child.Content as GalleryItem);
                return base.DoAddVisual(child);
            }

            return false;
        }

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            base.DoRemoveVisual(child);
            if (child is RibbonGalleryGroupViewContainer)
            {
                this.galleryTool.Groups.Remove(child.Content as GalleryItemGroup);
            }

            if (child is RibbonGalleryItemViewContainer)
            {
                this.galleryTool.Items.Remove(child.Content as GalleryItem);
            }
        }

        #region

        private string text = null;
        public string Text
        {
            get
            {
                return this.text;
            }
            set
            {
                this.text = value;

                if (this.galleryTool != null)
                    this.galleryTool.Caption = this.text ?? "";
            }
        }

        //public event EventHandler<GalleryEventArgs> ItemSelected
        //{
        //    add
        //    {
        //        if (this.galleryTool != null)
        //            this.galleryTool.ItemSelected += value;
        //    }
        //    remove
        //    {
        //        if (this.galleryTool != null)
        //            this.galleryTool.ItemSelected -= value;
        //    }
        //}

        //public event EventHandler<Infragistics.Windows.Ribbon.Events.GalleryItemEventArgs> ItemActivated
        //{
        //    add
        //    {
        //        if (this.galleryTool != null)
        //            this.galleryTool.ItemActivated += value;
        //    }
        //    remove
        //    {
        //        if (this.galleryTool != null)
        //            this.galleryTool.ItemActivated -= value;
        //    }
        //}

        public event EventHandler<GalleryEventArgs> ItemClicked;



        #endregion
    }
}
