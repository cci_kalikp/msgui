﻿using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon.Widgets
{
    /// <summary>
    /// Helper class for putting non-IRibbonTool widgets onto the Ribbon. In the subclass you use for your custom widget do NOT do
    /// any wire-up in the ContentReady as you would normally do, use WireUpEventHandlers instead.
    /// </summary>
    public class CustomRibbonWidgetViewContainer : WidgetViewContainer
    {
        protected object WrappedContent;

        public CustomRibbonWidgetViewContainer(string ID, InitialWidgetParameters parameters)
            : base(ID, parameters)
        {
            ContentReady += (s, e) =>
                {
                    WrappedContent = Content;

                    var wrapper = new ContentControlWrapper(Content);
                    var proxy = wrapper.ToolProxy as ContentControlWrapperProxy;
                    proxy.Wireup = WireUpEventHandlers;

                    m_content = wrapper;
                    WireUpEventHandlers(WrappedContent);
                };
        }

        protected virtual void WireUpEventHandlers(object content)
        {
        }
    }
}
