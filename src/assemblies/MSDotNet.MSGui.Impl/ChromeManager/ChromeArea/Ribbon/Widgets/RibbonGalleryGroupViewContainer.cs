﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using Infragistics.Windows.Ribbon;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon.Widgets
{
    public sealed class RibbonGalleryGroupViewContainer : WidgetViewContainer
    {
        private GalleryItemGroup group;
        private readonly List<GalleryItem> groupItems = new List<GalleryItem>(); 

        public RibbonGalleryGroupViewContainer(string ID, InitialWidgetParameters parameters)
			: base(ID, parameters)
		{
			this.ContentReady += (s, e) =>
			{
			    this.group = this.Content as GalleryItemGroup;
			    if (this.group == null)
			        return;

                this.group.ItemsSource = this.groupItems;

                if (this.text == null) //i.e. noone has set it before CreateChrome
                {
                    this.text = this.group.Title;
                }

			};
        }

        protected override bool DoCanAddwidget(IWidgetViewContainer widget)
        {
            return widget is RibbonGalleryGroupViewContainer;
        }

        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            if (child is RibbonGalleryItemViewContainer)
            {
                this.groupItems.Add(child.Content as GalleryItem);
                return base.DoAddVisual(child);
            }

            return false;
        }

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            if (child.Content is GalleryItem)
            {
                this.groupItems.Remove(child.Content as GalleryItem);
            }
            base.DoRemoveVisual(child);
        }

        #region

        private string text = null;
        public string Text
        {
            get
            {
                return this.text;
            }
            set
            {
                this.text = value;

                if (this.group != null)
                    this.group.Title = this.text ?? "";
            }
        }

        #endregion

    }
}
