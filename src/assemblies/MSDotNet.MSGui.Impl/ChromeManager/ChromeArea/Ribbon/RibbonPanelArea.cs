﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    internal class RibbonPanelArea : WidgetViewContainer, IPanelViewContainer
    {
        private Panel _panel;

        public RibbonPanelArea(string ID, InitialWidgetParameters parameters)
            : base(ID, parameters)
        {
            this.ContentReady += (s, e) =>
            {
                _panel = this.Content as Panel;
            };
        }

        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            // TODO: Need to wait for ContentReady?
            _panel.Children.Add(child.Content as UIElement);
            return base.DoAddVisual(child);
        }

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            _panel.Children.Remove(child.Content as UIElement);
            base.DoRemoveVisual(child);
        }
    }
}
