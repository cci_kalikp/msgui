﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Controls.SplitButton;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    internal class RibbonSplitDropdownArea : WidgetViewContainer, IDropdownButtonViewContainer
    {
        private SplitButton m_splitButton;
        private readonly ObservableCollection<object> items = new ObservableCollection<object>();

        public RibbonSplitDropdownArea(string ID, InitialWidgetParameters parameters)
            : base(ID, parameters)
        {
            this.m_isDropDownEnabled = ((InitialSplitButtonParameters) parameters).IsDropDownEnabled;
            this.ContentReady += (s, e) =>
            {
                this.m_splitButton = this.Content as SplitButton;
                if (m_splitButton == null)
                {
                    ContentControlWrapper wrapper = this.Content as ContentControlWrapper;
                    if (wrapper != null)
                    {
                        m_splitButton = wrapper.Content as SplitButton;
                    }
                }
                if (m_splitButton == null) return;
                if (m_splitButton.IsDropDownEnabled != m_isDropDownEnabled)
                {
                    m_splitButton.IsDropDownEnabled = m_isDropDownEnabled;
                }
                this.m_splitButton.ItemsSource = this.items;
            };
        }


        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            this.items.Add(child.Content);

            return base.DoAddVisual(child);
        }

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            if (this.items.Contains(child.Content))
            {
                this.items.Remove(child.Content);
            }
            base.DoRemoveVisual(child);
        }

        public string Text
        {
            get
            {
                return this.m_splitButton.Caption;
            }
            set
            {
                this.m_splitButton.Caption = value;
            }
        }

        private bool m_isDropDownEnabled = true;
        public bool IsDropDownEnabled
        {
            get { return m_splitButton == null ? m_isDropDownEnabled : m_splitButton.IsDropDownEnabled; }
            set 
            { 
                m_isDropDownEnabled = value;
                if (m_splitButton != null)
                {
                    m_splitButton.IsDropDownEnabled = value;
                }
            }
        }

    }
}
