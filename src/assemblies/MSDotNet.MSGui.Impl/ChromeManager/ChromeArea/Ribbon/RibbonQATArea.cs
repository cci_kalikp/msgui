﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading; 
using Infragistics.Windows.Ribbon;
using Infragistics.Windows.Ribbon.Internal;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Locker;
using MorganStanley.MSDotNet.MSGui.Impl.Ribbon;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    /// <summary>
    /// How To Test: test by running MorganStanley.MSDotNet.MSDesktop.Examples.View2ViewCommunication.TextView, 
    /// 1. check the indices of qat items added
    /// 2. change in Ribbon Config save and restore the profile, see if the saved qat settings are restored correctly
    /// </summary>
    internal sealed class RibbonQATArea : ObservableWidgetViewContainer, ILockerAffectedMainArea, IQuickAccessToolbar
    {
        internal static bool UseObservableChildren = true;
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<RibbonQATArea>();

        private readonly IList<string> _placements;
        private readonly IDictionary<IWidgetViewContainer, int> _preferredIndices = new Dictionary<IWidgetViewContainer, int>();
        private readonly RibbonInfragisticsImpl _ribbonImpl;
        private readonly XamRibbon _ribbon;

        private bool _processCollectionChanged = true;
        private bool _loadingLayout;
        private int _currentIndex;

        internal event EventHandler AllPlacementsProcessed;

        public RibbonQATArea(string ID, InitialWidgetParameters parameters)
            : base(ID, parameters)
        {
            IsAuxiliary = true;
            _placements = new List<string>();
            _ribbonImpl = ((InitialQatParameters)parameters).RibbonImpl;
            _ribbon = _ribbonImpl.Ribbon;

            ContentReady += (s, e) =>
                {
                    ((INotifyCollectionChanged)_ribbon.QuickAccessToolbar.Items).CollectionChanged +=
                        OnCollectionChanged; 
                };
        }
        
        public void AddWidget(IWidgetViewContainer widget, int index)
        {
            widget.AddToQAT();

            if (!_loadingLayout && ChromeManagerBase.StartupCompleted)
            {
                foreach (var item in _ribbon.QuickAccessToolbar.Items)
                {
                    var pt = item as QatPlaceholderTool;
                    if (pt == null || pt.TargetId != widget.FactoryID)
                        continue;
                    if (_ribbon.QuickAccessToolbar.Items.IndexOf(item) != index)
                    {
                        _ribbon.QuickAccessToolbar.Items.Remove(item);
                        _ribbon.QuickAccessToolbar.Items.Insert(index, item);
                    } 
                    break;
                }
            }
            else
            {
                _preferredIndices[widget] = index;
            }
        }

        private void OnCollectionChanged(object sender,
                                         NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            if (!_processCollectionChanged) return;
            if (notifyCollectionChangedEventArgs.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (var newItem in notifyCollectionChangedEventArgs.NewItems)
                {
                    var qatPlaceholderTool = (QatPlaceholderTool)newItem;

                    RibbonToolHelper.SetId(qatPlaceholderTool, qatPlaceholderTool.TargetId);
                    var widget = chromeManager.GetWidget(qatPlaceholderTool.TargetId);
                    if (widget != null)
                    {
                        if (!m_children.Keys.Contains(widget.FactoryID))
                        {
                            m_children.Add(widget.FactoryID, widget);
                            if (UseObservableChildren)
                                ObservableChildren.Insert(notifyCollectionChangedEventArgs.NewStartingIndex, widget);
                            
                        }
                        else if (UseObservableChildren)
                        {
                            var oldIndex = ObservableChildren.IndexOf(widget);
                            if (oldIndex < 0)
                            {
                                ObservableChildren.Insert(notifyCollectionChangedEventArgs.NewStartingIndex, widget);
                            }
                            else if (oldIndex != notifyCollectionChangedEventArgs.NewStartingIndex)
                            {
                                ObservableChildren.Move(oldIndex, notifyCollectionChangedEventArgs.NewStartingIndex);
                            }
                        }
                    }

                }
            }
            else if (notifyCollectionChangedEventArgs.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (var oldItem in notifyCollectionChangedEventArgs.OldItems)
                {
                    var widget = chromeManager.GetWidget(((QatPlaceholderTool)oldItem).TargetId);
                    if (widget != null && m_children.Keys.Contains(widget.FactoryID))
                    {
                        if (_ribbon.ToolsNotInRibbon.Contains(widget.Content))
                        {
                            _processCollectionChanged = false;
                            chromeManager.MoveWidget(widget, chromeManager[Core.ChromeManager.Helpers.ChromeArea.Locker],
                                                     null);
                            _processCollectionChanged = true;
                        }
                        else
                        {
                            RemoveLogical(widget);
                        }
                    }
                }
            }
        }
         
        //internal override IWidgetViewModel AddWidget(string location, string factoryID, InitialWidgetParameters initialParameters)
        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            bool isOnRibbon = false; 

            var originalWidget = chromeManager.GetWidget(child.FactoryID);
            if (originalWidget != null)
            {
                var p = originalWidget.Parent;
                while (p != null)
                {
                    if (p.Parent is RibbonRootArea)
                    {
                        isOnRibbon = true;
                        break;
                    }

                    if (p.Parent is LockerRootArea)
                    { 
                        break;
                    }

                    p = p.Parent;
                }
            }

            //WidgetRegistration ribbonWidgetRegistration = chromeManager.widgetRegistrations.FirstOrDefault(wR => { return wR.FactoryID == child.ID && wR.Root == RibbonChromeManager.QAT_COMMANDAREA_NAME; });
            //var lockerRegistration = chromeManager.widgetRegistrations.FirstOrDefault(wR => { return wR.FactoryID == child.ID && wR.Root.ToLower() == Core.ChromeManager.Helpers.ChromeArea.Locker.ToLower(); });

            //if (isOnRibbon)
            // {
            //this.m_ribbon.SelectedTab = this.m_ribbon.Tabs.FirstOrDefault(t => (string)t.Header == location);

            // This lockerRegistration check will work, because if the widget were placed on the ribbon, it would override the registration in the locker.
            if (isOnRibbon)
            {
                if (!_ribbon.QuickAccessToolbar.Items
                            .Cast<QatPlaceholderTool>()
                            .Any(d => d.TargetId == child.FactoryID)) //(!this.Contains(child.FactoryID))
                {
                    if (ChromeManagerBase.StartupCompleted && !_loadingLayout)
                    {
                        var w = new QatPlaceholderTool(child.FactoryID);
                        RibbonToolHelper.SetId(w, child.FactoryID);
                        _ribbon.QuickAccessToolbar.Items.Add(w);
                    }
                    else
                    {
                        if (!_placements.Contains(child.FactoryID))
                        {
                            _placements.Add(child.FactoryID);

                            IWidgetViewContainer container;
                            if (chromeManager.PlacedWidgets.TryGetValue(child.FactoryID, out container))
                            {
                                var containerDependencyObject = container.Content as DependencyObject;
                                if (RibbonToolHelper.GetId(containerDependencyObject) == null)
                                {
                                    RibbonToolHelper.SetId(containerDependencyObject, child.FactoryID);
                                }

                                var element = container.Content as DependencyObject;
                                var w = new QatPlaceholderTool(
                                    RibbonToolHelper.GetId(element),
                                    element is RibbonGroup
                                        ? QatPlaceholderToolType.RibbonGroup
                                        : QatPlaceholderToolType.Tool);
                                RibbonToolHelper.SetId(w, RibbonToolHelper.GetId(element));
                                _ribbon.QuickAccessToolbar.Items.Add(w);
                            }
                        }
                    }
                }
            }
            else
            {
                if (chromeManager.CreateWidget(child))
                {
                    var content = child.Content as FrameworkElement;
                    if (content != null)
                    {
                        if (LogicalTreeHelper.GetParent(content) is ContentControlWrapper)
                        {
                            ((ContentControlWrapper) LogicalTreeHelper.GetParent(content)).Content = null;
                        }

                        var ribbonItem = content;
                        if (!(content is ButtonTool))
                        {
                            ribbonItem = new ContentControlWrapper(content);
                            child.Content = ribbonItem;
                        }

                        RibbonToolHelper.SetId(ribbonItem, child.FactoryID);
                        _ribbon.ToolsNotInRibbon.Add(ribbonItem);

                        var w = new QatPlaceholderTool(child.FactoryID, QatPlaceholderToolType.Tool);
                        RibbonToolHelper.SetId(w, child.FactoryID);
                        _ribbon.QuickAccessToolbar.Items.Add(w);
                    }
                    else
                    {
                        Logger.Warning(
                            string.Format(
                                "Cannot add to QAT, implicit widget creation resulted in null or not a FrameworkElement for \"{0}\".",
                                child.FactoryID));
                    }
                }
                else
                {
                    Logger.Warning(
                        string.Format(
                            "Cannot add to QAT, implicit widget creation failed for \"{0}\". Did you register the widget factory properly and/or supplied the correct InitialWidgetParameters?",
                            child.FactoryID));
                }
            }

            //cached for restoring the position in the end
            if (_loadingLayout)
            {
                _preferredIndices[child] = _currentIndex;
                _currentIndex++;
            }
            // }
            return base.DoAddVisual(child);
        }

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            object itemToRemove = null;
            foreach (var item in _ribbon.QuickAccessToolbar.Items)
            {
                var itemDependencyObject = item as DependencyObject;
                if (itemDependencyObject != null && RibbonToolHelper.GetId(itemDependencyObject) == child.FactoryID)
                {
                    itemToRemove = item;
                    break; 
                }
            }

            _processCollectionChanged = false;
            if (itemToRemove != null)
            {
                _ribbon.QuickAccessToolbar.Items.Remove(itemToRemove);
            }
            
            var childContent = child.Content as FrameworkElement;
            if (childContent != null)
            {
                _ribbon.ToolsNotInRibbon.Remove(childContent);
            }

            //IG Bug: when removing the item from ToolsNotInRibbon after the ribbon is loaded, it seems that, the tool registration would not take effect until next item is added
            ReflHelper.MethodInvoke(_ribbon, "ForcePendingToolRegistrations", new object[0]);
           

            _processCollectionChanged = true;
            base.DoRemoveVisual(child);
        }

        internal void SuspendLoadLayout()
        {
            _currentIndex = 0;
            _preferredIndices.Clear();
            _loadingLayout = true; 
        }

        internal void ResumeLoadLayout()
        { 
        }

        public override void SwapChildren(IWidgetViewContainer a, IWidgetViewContainer b)
        {
            var items = _ribbon.QuickAccessToolbar.Items;
            var qatPlaceholderTools = items.Cast<QatPlaceholderTool>();
            var toolsList = qatPlaceholderTools.ToList();

            base.SwapChildren(a, b);
            var placeholderA = toolsList.FirstOrDefault(w => RibbonToolHelper.GetId(w) == a.FactoryID);
            var indexA = items.IndexOf(placeholderA);
            var placeholderB = toolsList.FirstOrDefault(w => RibbonToolHelper.GetId(w) == b.FactoryID);
            var indexB = items.IndexOf(placeholderB);
            if (indexA < 0 || indexA >= items.Count ||
                indexB < 0 || indexB >= items.Count)
                return;

            _processCollectionChanged = false;
            items.RemoveAt(indexB);
            items.Insert(indexB, new QatPlaceholderTool());
            items.RemoveAt(indexA);
            items.Insert(indexA, placeholderB);
            items.RemoveAt(indexB);
            items.Insert(indexB, placeholderA);
            _processCollectionChanged = true;
        }

        public override string NodeName
        {
            get { return Core.ChromeManager.Helpers.ChromeArea.QAT; }
        }

        public bool DisableHack { get; set; }

        #region black magic

        internal void InvokeAllPlacementsProcessed(EventArgs e)
        {
            var handler = AllPlacementsProcessed;
            if (handler != null) handler(this, e);
        }

        #endregion
    }

    #region Content control wrapper (thanks to Bryant Longley)

    internal sealed class ContentControlWrapper : ContentControl, IRibbonTool
    {
        private ContentControlWrapperProxy _proxy;
        private readonly object _widget;

        public ContentControlWrapper()
        {
            VerticalAlignment = VerticalAlignment.Stretch;
            HorizontalAlignment = HorizontalAlignment.Stretch;
        }

        public ContentControlWrapper(object widget)
            : this()
        {
            _widget = widget;
        }

        public RibbonToolProxy ToolProxy
        {
            get
            {
                return _proxy ?? (_proxy = new ContentControlWrapperProxy(_widget));
            }
        }

        public object Widget
        {
            get { return _widget; }
        }
    }

    internal sealed class ContentControlWrapperProxy : RibbonToolProxy<ContentControlWrapper>
    {
        private readonly object _widget;
        public WireUpCallback Wireup;

        public ContentControlWrapperProxy(object widget)
        {
            _widget = widget;
        }

        protected override ContentControlWrapper Clone(ContentControlWrapper sourceTool)
        {
            var result = base.Clone(sourceTool);
            result.Content = _widget;

            if (Wireup != null)
                Wireup(result.Content);

            return result;
        }
    }

    public delegate void WireUpCallback(object target);
    #endregion
}
