﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;  
using System.Collections.ObjectModel;
using Infragistics.Windows.Ribbon;
using MorganStanley.MSDotNet.MSGui.Controls.BarMenu;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    internal class RibbonDropdownArea : WidgetViewContainer, IDropdownButtonViewContainer
    {
        private ItemsControl m_dropdown;
        private readonly ObservableCollection<object> items = new ObservableCollection<object>();

        public RibbonDropdownArea(string ID, InitialWidgetParameters parameters)
            : base(ID, parameters)
        {
            this.ContentReady += (s, e) =>
            {
                this.m_dropdown = this.Content as ItemsControl;
                if (m_dropdown != null)
                {
                    this.m_dropdown.ItemsSource = this.items;
                }
            };
            this.IsDropDownEnabled = true;
        }


        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            this.items.Add(child.Content);
            return base.DoAddVisual(child);
        }

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            if (this.items.Contains(child.Content))
            {
                this.items.Remove(child.Content);
            }
            base.DoRemoveVisual(child);
        }

        public string Text
        {
            get
            {
                MenuTool tool = m_dropdown as MenuTool;
                if (tool != null) return tool.Caption;
                BarMenuTool barTool = m_dropdown as BarMenuTool;
                if (barTool != null) return barTool.Caption;
                return null;
            }
            set
            {
                MenuTool tool = m_dropdown as MenuTool;
                if (tool != null)
                {
                    tool.Caption = value;
                    return;
                }
                BarMenuTool barTool = m_dropdown as BarMenuTool;
                if (barTool != null)
                {
                    barTool.Caption = value;
                }
            }
        }

        public bool IsDropDownEnabled { get; set; }

    }



    //internal class RibbonDropdownArea_ : ChromeAreaBase, IWidgetViewModel
    //{
    //    private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<RibbonDropdownArea_>();

    //    internal ChromeManagerBase m_chromeManager;
    //    private MenuTool m_dropdown;
    //    private readonly ObservableCollection<object> items = new ObservableCollection<object>();

    //    public RibbonDropdownArea_()
    //    {
    //        //TODO: RibbonDropdownArea.ctor - use DI instead!
    //        this.m_chromeManager = ChromeManagerBase.Instance;
    //    }

    //    /// <summary>
    //    /// Adds a widget using the specified factory.
    //    /// </summary>
    //    /// <param name="location">specifies where to add the widget</param>
    //    /// <param name="factoryID">The factory to use. If null, nothing gets placed (this is to support ChromeArea creation without placing widgets).</param>
    //    /// <param name="initialParameters">Initial parameters for the widget.</param>
    //    /// <returns>a WidgetViewModel Instance.</returns>
    //    internal override IWidgetViewModel AddWidget(string location, string factoryID, InitialWidgetParameters initialParameters)
    //    {
    //        if (location != null && location != string.Empty)
    //        {
    //            m_logger.InfoWithFormat("Error placing widget '{0}'. Dropdowns can only host widgets, no further location can be specified.", factoryID);
    //            return null;
    //        }
    //        else
    //        {
    //            if (factoryID == null)
    //            {
    //                return null;
    //            }

    //            IWidgetViewModel viewModel = this.m_chromeManager.CreateWidget(factoryID, initialParameters);
    //            this.items.Add(viewModel.ViewContainer.Content);
    //            this.widgets.Add(viewModel.ViewContainer);
    //            return viewModel;
    //        }
    //    }

    //    internal override void RemoveWidget(string location, IWidgetViewContainer widget)
    //    {
    //        if (this.widgets.Contains(widget))
    //        {
    //            this.widgets.Remove(widget);
    //            this.m_dropdown.Items.Remove(widget.Content);
    //        }
    //        else
    //        {
    //            m_logger.InfoWithFormat("Can't remove widget {0} for it is not in the dropdown.", widget.FactoryID);
    //        }
    //    }

    //    //TODO Implement support for nested dropdowns in the ribbon shell.
    //    internal override ChromeAreaBase GetArea(string location)
    //    {
    //        return this;
    //    }

    //    #region IWidgetViewModel Members

    //    private IWidgetViewContainer m_viewContainer = null;
    //    public IWidgetViewContainer ViewContainer 
    //    {
    //        get
    //        {
    //            return this.m_viewContainer;
    //        }
    //        set
    //        {
    //            if (this.m_viewContainer != null)
    //            {
    //                throw new InvalidOperationException("RibbonDropdownArea.ViewContainer can be set only once.");
    //            }

    //            this.m_viewContainer = value;
    //            this.Name = this.m_viewContainer.FactoryID;
    //            this.m_dropdown = this.m_viewContainer.Content as MenuTool;
    //            this.m_dropdown.ItemsSource = this.items;
    //        }
    //    }

    //    #endregion
    //}
}
