﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infragistics.Windows.Ribbon;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon
{
    internal class RibbonContextualArea : ObservableWidgetViewContainer, IRibbonContext
    {
        private ContextualTabGroup _context;
        private readonly IList<IContextRibbonTab> _contextRibbonTabs = new List<IContextRibbonTab>();

        public RibbonContextualArea(string ID, InitialWidgetParameters parameters)
            : base(ID, parameters)
        {
            ContentReady += (s, e) =>
                {
                    _context = Content as ContextualTabGroup;
                };
        }


        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            if (!child.ContentOfType<RibbonTabItem>()) return false;
            _context.Tabs.Add(child.Content as RibbonTabItem);
            _contextRibbonTabs.Add((IContextRibbonTab) child);

            return base.DoAddVisual(child);
        }

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            if (child.ContentOfType<RibbonTabItem>())
            {
                _context.Tabs.Remove((RibbonTabItem) child.Content);
                _contextRibbonTabs.Remove((IContextRibbonTab)child);
            }
            
            base.DoRemoveVisual(child);
        }

        protected override InitialWidgetParameters DoAddDefault(ref string ID)
        {
            var newGroup = new InitialContextRibbonTabParameters { Text = ID };
            ID = Guid.NewGuid().ToString();
            return newGroup;
        }

        protected override bool DoContains(string index)
        {
            return (this.m_children.Values.FirstOrDefault(vc => vc.Parameters.Text == index) != null) || this.m_children.ContainsKey(index);
        }

        protected override IWidgetViewContainer DoGetWidget(string index)
        {
            return this.m_children.Values.FirstOrDefault(vc => vc.Parameters.Text == index) ?? this.m_children[index];
        }

        protected override bool DoCanAddwidget(IWidgetViewContainer widget)
        {
            return widget is RibbonTabArea;
        }

        public override void SwapChildren(IWidgetViewContainer a_, IWidgetViewContainer b_)
        {
            base.SwapChildren(a_, b_);
            var indexA = _context.Tabs.IndexOf(a_.Content as RibbonTabItem);
            var indexB = _context.Tabs.IndexOf(b_.Content as RibbonTabItem);
            _context.Tabs[indexB] = new RibbonTabItem();
            _context.Tabs[indexA] = b_.Content as RibbonTabItem;
            _context.Tabs[indexB] = a_.Content as RibbonTabItem;
        }

        public override string NodeName
        {
            get
            {
                return "Context";
            }
        }

        public string Key
        {
            get { return _context.Key; }
        }

        public new IContextRibbonTab this[string tabName]
        {
            get
            {
                return _contextRibbonTabs.FirstOrDefault(t =>
                    {
                        return t.Name.Equals(tabName, StringComparison.CurrentCulture);
                    });
            }
        }

        public IContextRibbonTab AddTab(string tabName)
        {
            return this.AddWidget(new InitialContextRibbonTabParameters { Text = tabName }) as IContextRibbonTab;
        }

        public void Show(bool hideOthers = true)
        {
            if (hideOthers)
            {
                ((RibbonRootArea)Parent).HideAllContexts();
            }

            this._context.IsVisible = true;
        }

        public void Hide()
        {
            this._context.IsVisible = false;
        }
    }
}
