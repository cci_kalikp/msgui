﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.TabWell
{
	class TabWellEdgeArea : WidgetViewContainer
	{
		private StackPanel m_stackPanel;

		public TabWellEdgeArea(string ID, InitialWidgetParameters parameters)
			: base(ID, parameters)
		{
			this.ContentReady += (s, e) =>
			{
				this.m_stackPanel = this.Content as StackPanel;
			};
		}

		protected override bool DoAddVisual(IWidgetViewContainer child)
		{
			if (child.Content is UIElement)
			{
				this.m_stackPanel.Children.Add(child.Content as UIElement);
                return base.DoAddVisual(child);
            }
		    return false;
		}

		protected override void DoRemoveVisual(IWidgetViewContainer child)
		{
			if (child.Content is UIElement)
			{
				this.m_stackPanel.Children.Remove(child.Content as UIElement);
			}
            base.DoRemoveVisual(child);
		}

	}
}
