﻿using System.Windows.Controls; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.TabWell
{
	internal class InitialTabWellRootParameters : InitialWidgetParameters, ILockerPassThrough
	{
	}

	internal class InitialTabWellEdgeParameters : InitialWidgetParameters, ILockerPassThrough
	{
        public TabControl TabControl { get; set; }
		public HorizontalAlignment Side { get; set; }
	}
}
