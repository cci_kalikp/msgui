﻿using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using Infragistics.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.TabWell
{
	internal sealed class TabWellFactories
	{
		private static ChromeManagerBase _chromeManager;

		internal static void Initialize(ChromeManagerBase chromeManager)
		{
			_chromeManager = chromeManager;
			_chromeManager.Registry.RegisterWidgetFactoryMapping<InitialTabWellRootParameters>(TabWellRootFactory);
			_chromeManager.Registry.RegisterWidgetFactoryMapping<InitialTabWellEdgeParameters>(TabWellEdgeFactory);
		}

		[FactoryOptions(typeof(TabWellRootArea))]
		private static bool TabWellRootFactory(IWidgetViewContainer vc, XDocument state)
		{
			return true;
		}

		[FactoryOptions(typeof(TabWellEdgeArea))]
		private static bool TabWellEdgeFactory(IWidgetViewContainer vc, XDocument state)
		{
			var properties = vc.Parameters as InitialTabWellEdgeParameters;

            if (properties != null && properties.TabControl != null)
			{
                if (properties.Side == HorizontalAlignment.Left)
				{
					vc.Content = ((XamTabControl)properties.TabControl).PreTabItemContent = new StackPanel
					{
						VerticalAlignment = VerticalAlignment.Center,
						Orientation = Orientation.Horizontal
					};
					return true;
				}

				if (properties.Side == HorizontalAlignment.Right)
				{
                    vc.Content = ((XamTabControl)properties.TabControl).PostTabItemContent = new StackPanel
					{
						VerticalAlignment = VerticalAlignment.Center,
						Orientation = Orientation.Horizontal
					};
					return true;
				}

				return false;
			}

			return false;
		}
	}
}
