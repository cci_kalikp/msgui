﻿using System; 
using System.Linq; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 
using System.Windows.Controls; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.TabWell
{
	class TabWellRootArea : WidgetViewContainer, ITabbedDock
	{
		internal const string TabWellLeftEdgeID = "39384103-375D-492E-ADC8-75B4FE90E8F9";
		internal const string TabWellRightEdgeID = "21B84D16-5702-42FE-A821-DD5F6B5D3E08";
         
	    private TabbedDockViewModel dockViewModel;

	    public TabWellRootArea(string ID, InitialWidgetParameters parameters)
			: base(ID, parameters)
		{

		}

	    internal TabbedDockViewModel DockViewModel
	    {
	        get
	        {
	            return this.dockViewModel;
	        }
	        set
	        {
                if (this.dockViewModel != null)
                {
                    this.dockViewModel.ActiveTabChanged -= DockViewModel_OnActiveTabChanged;   
                }
	            
                this.dockViewModel = value;

                if (this.dockViewModel != null)
                {
                    this.dockViewModel.ActiveTabChanged += DockViewModel_OnActiveTabChanged;   
                }
	        }
	    }

	    private void DockViewModel_OnActiveTabChanged(object sender, EventArgs eventArgs)
	    {
	        var handler = this.ActiveTabChanged;
            if (handler != null)
            {
                handler(this, eventArgs);
            }
	    }

	    public override IWidgetViewContainer this[string index]
		{
			get
			{
				if (index.Trim().ToLower() == "left")
				{
					return base[TabWellLeftEdgeID];
				}
				else if (index.Trim().ToLower() == "right")
				{
					return base[TabWellRightEdgeID];
				}
				else
				{
					return this.m_children.Values.FirstOrDefault(w => (w is ShellTabViewModel) && (w as ShellTabViewModel).Title == index);
				}
			}
		}

		protected override bool DoCanAddwidget(IWidgetViewContainer widget)
		{
			return widget is TabWellEdgeArea || widget is ShellTabViewModel;
		}

		#region ITabbedDock Members

		IDockTab ITabbedDock.this[string tabName]
		{
			get 
			{
				return this.m_children.Values.FirstOrDefault(w => (w is ShellTabViewModel) && (w as ShellTabViewModel).Title == tabName) as IDockTab;
			}
		}

		System.Collections.ObjectModel.ReadOnlyObservableCollection<IDockTab> ITabbedDock.Tabs
		{
			get 
			{
				return (DockViewModel as ITabbedDock).Tabs;
			}
		}

        public IDockTab ActiveTab { get { return this.DockViewModel.ActiveTab; } }

		IWidgetViewContainer ITabbedDock.LeftEdge
		{
			get
			{
				return base[TabWellLeftEdgeID];
			}
		}

		IWidgetViewContainer ITabbedDock.RightEdge
		{
			get
			{
				return base[TabWellRightEdgeID];
			}
		}

		IDockTab ITabbedDock.AddTab(string tabName)
		{
			return DockViewModel.AddTab(tabName);
		}

        public IDockTab AddTab(string tabName, object titleData)
        {
            return DockViewModel.AddTab(tabName, titleData);
        }
	    public event EventHandler ActiveTabChanged;

	    #endregion


    }
}
