﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.SearchBar
{
    /// <summary>
    /// Interaction logic for AddToFavoritesControl.xaml
    /// </summary>
    public partial class AddToFavoritesControl : UserControl
    {
        public AddToFavoritesControl()
        {
            InitializeComponent();
        }
    }
}
