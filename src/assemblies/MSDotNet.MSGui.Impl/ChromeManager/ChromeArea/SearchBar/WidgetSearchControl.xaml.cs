﻿using System.Windows;
using System.Windows.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.SearchBar
{
    /// <summary>
    /// Interaction logic for ScreenSelectorControl.xaml
    /// </summary>
    public partial class WidgetSearchControl : UserControl
    {
        public WidgetSearchControl()
        {
            InitializeComponent();
        }

        private void ItemSelectorTextBox_OnGotFocus(object sender, RoutedEventArgs e)
        {
            var control = sender as TextBox;
            control.SelectAll();
        }
    }
}
