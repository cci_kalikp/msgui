﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.SearchBar
{
    public class ToggleFavoriteBackgroundConverter : IMultiValueConverter
    {
        private readonly Brush star = new ImageBrush(
            new BitmapImage(
                new Uri(
                    @"pack://application:,,,/MSDotNet.MSGui.Impl;Component/ChromeManager/ChromeArea/SearchBar/Images/Star.ico",
                    UriKind.RelativeOrAbsolute)));

        private readonly Brush glowingStar = new ImageBrush(
            new BitmapImage(
                new Uri(
                    @"pack://application:,,,/MSDotNet.MSGui.Impl;Component/ChromeManager/ChromeArea/SearchBar/Images/GlowStar.ico",
                    UriKind.RelativeOrAbsolute)));

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var vm = values[0] as WidgetSearchViewModel;
            var item = values[1] as ISearchableItem;
            if (vm == null || item == null)
            {
                return star;
            }

            if (vm.Favorites.Contains(item))
            {
                return glowingStar;
            }
            return star;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[] {};
        }
    }
}