﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Infragistics.Windows.DataPresenter;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.LauncherBar;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.SearchBar
{
    public class WidgetSearchArea : WidgetViewContainer
    {

        private WidgetSearchControl searchControl;
        private readonly ObservableCollection<ISearchableItem> items;
        private readonly IWidgetViewContainer favoritesContainer;
        private WidgetSearchViewModel viewModel;
        private IWindowViewContainer addToFavouritesDialog;

        private const string AddToFavoritesControlWindowFactoryId = "MSDesktop.WidgetSearchArea.Favorite.AddToFavoritesControlWindowFactory";

        public WidgetSearchArea(string ID, InitialWidgetParameters parameters) : base(ID, parameters)
        {
            items = new ObservableCollection<ISearchableItem>();
            ContentReady += OnContentReady;
            favoritesContainer = ((InitialWidgetSearchParameters) parameters).FavoritesContainer;
        }

        private void OnContentReady(object sender, EventArgs eventArgs)
        {
            searchControl = (WidgetSearchControl) Content;
            viewModel = new WidgetSearchViewModel(items)
                {
                    Text = Parameters.Text,
                    Width = ((InitialWidgetSearchParameters) Parameters).Width,
                    ColumnLabelsLocation = ((InitialWidgetSearchParameters)Parameters).HeadersVisible ? LabelLocation.Default : LabelLocation.Hidden,
                    FavoritesContainer = (ObservableWidgetViewContainer) favoritesContainer
                };
            viewModel.OpenItemRequested += ViewModelOnOpenItemRequested;
            viewModel.FavoriteRequested += ViewModelOnFavoriteRequested;
            viewModel.FavoriteGroupSelected += ViewModelOnFavoriteGroupSelected;
            viewModel.NewGroupRequested += ViewModelOnNewGroupRequested;

            searchControl.DataContext = viewModel;

            chromeManager.Registry.RegisterWindowFactory(AddToFavoritesControlWindowFactoryId, (container, state) =>
                {
                    container.Content = new AddToFavoritesControl
                        {
                            DataContext = viewModel
                        };
                    container.Title = "Add to favorites";
                    addToFavouritesDialog = container;
                    return true;
                });
        }

        private void ViewModelOnNewGroupRequested(object sender, EventArgs eventArgs)
        {
            var options = new TaskDialogOptions
                {
                    Title = "Create new favorites group",
                    Content = "Enter group name",
                    CommonButtons = TaskDialogCommonButtons.OKCancel,
                    UserInputEnabled = true
                };
            var result = TaskDialog.Show(options);
            if (result.Result == TaskDialogSimpleResult.Ok)
            {
                IWidgetViewContainer parent;
                if (viewModel.SelectedFavoritesGroup is ObservableWidgetViewContainer)
                {
                    parent = viewModel.SelectedFavoritesGroup;
                }
                else
                {
                    parent = viewModel.SelectedFavoritesGroup.Parent;
                }
                parent.AddWidget(new InitialDropdownButtonParameters
                    {
                        Text = result.UserInput
                    });
            }
        }

        private void ViewModelOnFavoriteGroupSelected(object sender, SearchableItemEventArgs searchableItemEventArgs)
        {
            var item = (ISearchableItem) viewModel.SelectedItem;
            var widget = Children.First(w => ((SearchableItemContainer) w).Content == item);
            viewModel.SelectedFavoritesGroup.AddWidget(string.Format("{0}#{1}", LauncherBarDropdownArea.FavoriteWidgetPrefix, widget.FactoryID), 
                new InitialButtonParameters
                {
                    Text = item.DisplayName,
                    Click = item.Click
                });
            if (addToFavouritesDialog != null)
            {
                addToFavouritesDialog.Close();
                addToFavouritesDialog = null;
            }
            viewModel.Favorites.Add(item);
        }

        private void ViewModelOnFavoriteRequested(object sender, SearchableItemEventArgs searchableItemEventArgs)
        {
            var item = items.FirstOrDefault(it => it.Key == searchableItemEventArgs.ItemKey);
            if (item != null)
            {
                var widget = Children.First(w => ((SearchableItemContainer)w).Content == item);
                if (viewModel.Favorites.Contains(item))
                {
                    chromeManager.RemoveWidget(string.Format("{0}#{1}", LauncherBarDropdownArea.FavoriteWidgetPrefix, widget.FactoryID));
                    viewModel.Favorites.Remove(item);
                }
                else
                {
                    chromeManager.CreateWindow(AddToFavoritesControlWindowFactoryId, new InitialWindowParameters
                    {
                        IsModal = true,
                        Width = 300,
                        Height = 200
                    });
                }
            }
            viewModel.BindingRefreshHack = true;
        }

        private void ViewModelOnOpenItemRequested(object sender, SearchableItemEventArgs searchableItemEventArgs)
        {
            var item = items.FirstOrDefault(it => it.Key == searchableItemEventArgs.ItemKey);
            if (item != null)
            {
                var handler = item.Click;
                if (handler != null)
                {
                    handler(null, null);
                }
            }
        }

        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            var item = child as IButtonViewContainer;
            if (item != null)
            {
                items.Add(new ButtonViewContainerSearchableItem(item));
                return base.DoAddVisual(item);
            }
            var parameters = child.Parameters as InitialSearchableItemParameters;
            if (parameters != null)
            {
                items.Add((parameters.ItemViewModel));
                return base.DoAddVisual(child);
            }
            return false;
        }

        public void LoadFavorite(string factoryId, IWidgetViewContainer parent)
        {
            var searchableItem = (SearchableItemContainer) Children.First(w => w.FactoryID == factoryId);
            parent.AddWidget(string.Format("{0}#{1}", LauncherBarDropdownArea.FavoriteWidgetPrefix, factoryId), new InitialButtonParameters
                {
                    Text = searchableItem.Item.DisplayName,
                    Click = searchableItem.Item.Click
                });
            viewModel.Favorites.Add(searchableItem.Item);
        }
    }

    class ButtonViewContainerSearchableItem : ISearchableItem
    {
        public ButtonViewContainerSearchableItem(IButtonViewContainer button)
        {
            DisplayName = button.Parameters.Text;
            Key = button.Parameters.Text;
            Click = ((InitialButtonParameters) button.Parameters).Click;
        }

        public string DisplayName { get; private set; }

        public string Key { get; private set; }

        public EventHandler Click { get; private set; }
    }
}
