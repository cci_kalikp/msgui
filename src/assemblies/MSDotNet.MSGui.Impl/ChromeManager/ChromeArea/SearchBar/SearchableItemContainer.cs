﻿using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.SearchBar
{
    class SearchableItemContainer : WidgetViewContainer
    {
        public SearchableItemContainer(string ID, InitialWidgetParameters parameters) : base(ID, parameters)
        {
        }

        public ISearchableItem Item
        {
            get
            {
                return ((InitialSearchableItemParameters) Parameters).ItemViewModel;
            }
        }
    }
}
