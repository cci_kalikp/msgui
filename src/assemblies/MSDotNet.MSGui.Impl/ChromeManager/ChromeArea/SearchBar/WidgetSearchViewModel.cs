﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Input;
using Infragistics.Windows.DataPresenter;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.SearchBar
{
    public class SearchableItemEventArgs : EventArgs
    {
        public string ItemKey { get; set; }
    }

    public sealed class WidgetSearchViewModel : ViewModelBase
    {
        private readonly ICollectionView searchableItemCollectionView;
        public ICollectionView SearchableItemCollectionView { get { return searchableItemCollectionView; } }
        private readonly HashSet<ISearchableItem> favorites;

        private ICommand _enterKeyCommand;
        private ICommand _upKeyCommand;
        private ICommand _downKeyCommand;

        public WidgetSearchViewModel(IEnumerable<ISearchableItem> view)
        {
            searchableItemCollectionView = CollectionViewSource.GetDefaultView(view);
            searchableItemCollectionView.Filter += Filter;
            searchableItemCollectionView.SortDescriptions.Add(new SortDescription("DisplayName", ListSortDirection.Ascending));
            favorites = new HashSet<ISearchableItem>();
        }

        public HashSet<ISearchableItem> Favorites { get { return favorites; } }

        public event EventHandler<SearchableItemEventArgs> OpenItemRequested;
        void OnOpenItemRequested(string itemKey)
        {
            if (OpenItemRequested != null)
                OpenItemRequested(this, new SearchableItemEventArgs() { ItemKey = itemKey });
        }

        public event EventHandler<SearchableItemEventArgs> FavoriteRequested;
        private void OnFavoriteRequested(string key)
        {
            if (FavoriteRequested != null)
                FavoriteRequested(this, new SearchableItemEventArgs {ItemKey = key});
        }

        public event EventHandler<SearchableItemEventArgs> FavoriteGroupSelected;
        private void OnFavoriteGroupSelected(SearchableItemEventArgs e)
        {
            var handler = FavoriteGroupSelected;
            if (handler != null) handler(this, e);
        }

        public event EventHandler NewGroupRequested;

        private void OnNewGroupRequested()
        {
            var handler = NewGroupRequested;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        private bool _isItemSelectorPopupOpen;
        public bool IsItemSelectorPopupOpen
        {
            get { return _isItemSelectorPopupOpen; }
            set
            {
                _isItemSelectorPopupOpen = value;
                OnPropertyChanged("IsItemSelectorPopupOpen");
            }
        }

        private object _selectedItem;
        public object SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public bool BindingRefreshHack
        {
            get
            {
                return true;
            }
            set
            {
                OnPropertyChanged("BindingRefreshHack");
            }
        }

        public ObservableWidgetViewContainer FavoritesContainer { get; set; }

        public IWidgetViewContainer SelectedFavoritesGroup { get; set; }

        public string Text { get; internal set; }

        public double Width { get; internal set; }

        public LabelLocation ColumnLabelsLocation { get; internal set; }

        private string filterItemText;
        public string FilterItemText
        {
            get { return filterItemText; }
            set
            {
                filterItemText = value;
                SearchableItemCollectionView.Refresh();
                if (!string.IsNullOrEmpty(filterItemText))
                    IsItemSelectorPopupOpen = true;
                SearchableItemCollectionView.MoveCurrentToFirst();
                SelectedItem = SearchableItemCollectionView.CurrentItem;
                OnPropertyChanged("FilterItemText");
            }
        }
        
        private bool Filter(object o)
        {
            var item = o as ISearchableItem;
            if (o == null) return false;
            if (string.IsNullOrWhiteSpace(FilterItemText)) return true;
            if (string.IsNullOrEmpty(item.DisplayName)) return false;

            return item.DisplayName.ToUpper().Contains(FilterItemText.ToUpper());
        }

        private ICommand _openItemCommand;
        public ICommand OpenItemCommand
        {
            get
            {
                if (_openItemCommand == null)
                    _openItemCommand = new RelayCommand(ExecuteOpenItemCommand, CanExecuteOpenItemCommand);
                return _openItemCommand;
            }
        }

        private bool CanExecuteOpenItemCommand(object o)
        {
            return true;
        }

        private void ExecuteOpenItemCommand(object o)
        {
            var item = o as ISearchableItem;
            if (item == null) return;
            OnOpenItemRequested(item.Key);
            IsItemSelectorPopupOpen = false;
        }

        private ICommand _addFavouriteCommand;

        public ICommand AddFavouriteCommand
        {
            get
            {
                if (_addFavouriteCommand == null)
                    _addFavouriteCommand = new RelayCommand(ExecuteAddFavouriteCommand, CanExecuteAddFavouriteCommand);
                return _addFavouriteCommand;
            }
        }

        private bool CanExecuteAddFavouriteCommand(object o)
        {
            return true;
        }

        private void ExecuteAddFavouriteCommand(object o)
        {
            var item = o as ISearchableItem;
            if (item == null) return;
            OnFavoriteRequested(item.Key);
        }

        public ICommand UpKeyCommand
        {
            get
            {
                if (_upKeyCommand == null)
                    _upKeyCommand = new RelayCommand(OnUpKeyPressedCommand);
                return _upKeyCommand;
            }
        }

        public ICommand DownKeyCommand
        {
            get
            {
                if (_downKeyCommand == null)
                    _downKeyCommand = new RelayCommand(OnDownKeyPressedCommand);
                return _downKeyCommand;
            }
        }

        public ICommand EnterKeyCommand
        {
            get
            {
                if (_enterKeyCommand == null)
                    _enterKeyCommand = new RelayCommand(OnEnterKeyPressedCommand);
                return _enterKeyCommand;
            }
        }

        public ICommand NewGroupCommand
        {
            get
            {
                return new RelayCommand(o => OnNewGroupRequested(), o => SelectedFavoritesGroup != null);
            }
        }

        public ICommand SelectGroupCommand
        {
            get
            {
                return new RelayCommand(o => OnFavoriteGroupSelected(new SearchableItemEventArgs()), 
                    o => SelectedFavoritesGroup is ObservableWidgetViewContainer);
            }
        }
        
        private void OnEnterKeyPressedCommand(object sender)
        {
            if (!IsItemSelectorPopupOpen)
                return;

            var displayField = sender as ISearchableItem;
            if (displayField != null)
            {
                FilterItemText = string.Empty;
                IsItemSelectorPopupOpen = false;
                ExecuteOpenItemCommand(displayField);
            }
        }

        private void OnUpKeyPressedCommand(object sender)
        {
            if (SearchableItemCollectionView.CurrentItem == null || !IsItemSelectorPopupOpen)
                SearchableItemCollectionView.MoveCurrentToFirst();
            else
                SearchableItemCollectionView.MoveCurrentToPrevious();
            SelectedItem = SearchableItemCollectionView.CurrentItem;

            if (!IsItemSelectorPopupOpen)
                IsItemSelectorPopupOpen = true;
        }

        private void OnDownKeyPressedCommand(object sender)
        {
            if (SearchableItemCollectionView.CurrentItem == null || !IsItemSelectorPopupOpen)
                SearchableItemCollectionView.MoveCurrentToFirst();
            else
                SearchableItemCollectionView.MoveCurrentToNext();

            SelectedItem = SearchableItemCollectionView.CurrentItem;

            if (!IsItemSelectorPopupOpen)
                IsItemSelectorPopupOpen = true;
        }
         
    }
}
