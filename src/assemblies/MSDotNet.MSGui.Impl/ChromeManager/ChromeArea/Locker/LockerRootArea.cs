﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Locker
{
    internal class LockerRootArea : ObservableWidgetViewContainer
	{
		public LockerRootArea(string ID, InitialWidgetParameters parameters)
			: base(ID, parameters) 
		{ 
		
		}

		public override IWidgetViewContainer this[string index]
		{
			get
			{
				return this;
			}
		}

		protected override bool DoAddVisual(IWidgetViewContainer child)
		{
			return true;
		}

        public override string NodeName
        {
            get
            {
                return "Locker";
            }
        }
	}
		

	//internal class LockerRootArea : ChromeAreaBase
	//{
	//    private ChromeManagerBase m_chromeManager;

	//    public LockerRootArea(ChromeManagerBase chromeManager_)
	//        : base()
	//    {
	//        this.m_chromeManager = chromeManager_;
	//    }

	//    internal override IWidgetViewModel AddWidget(string location, string factoryID, InitialWidgetParameters initialParameters)
	//    {
	//        var wvm = this.m_chromeManager.CreateWidget(factoryID, initialParameters);
	//        this.widgets.Add(wvm.ViewContainer);
	//        return wvm;
	//    }

	//    internal override void RemoveWidget(string location, Core.ChromeManager.IWidgetViewContainer widget)
	//    {
	//        if (this.widgets.Contains(widget))
	//        {
	//            this.widgets.Remove(widget);
	//        }			
	//    }

	//    internal override ChromeAreaBase GetArea(string location)
	//    {
	//        return this;
	//    }
	//}
}
