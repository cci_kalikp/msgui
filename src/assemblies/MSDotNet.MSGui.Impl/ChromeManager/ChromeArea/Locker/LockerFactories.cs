﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Locker
{
	class LockerFactories
	{
		private static ChromeManagerBase m_chromeManager;

		internal static void Initialize(ChromeManagerBase chromeManager_)
		{
			m_chromeManager = chromeManager_;
			m_chromeManager.Registry.RegisterWidgetFactoryMapping<InitialLockerRootParameters>(LockerRootFactory);
		}

		[FactoryOptions(typeof(ChromeManager.ChromeArea.Locker.LockerRootArea))]
		private static bool LockerRootFactory(IWidgetViewContainer vc_, XDocument state_)
		{
			return true;
		}
	}
}
