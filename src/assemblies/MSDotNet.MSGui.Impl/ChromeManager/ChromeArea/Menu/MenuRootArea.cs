﻿using System.Windows.Controls;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Menu
{
	internal sealed class MenuRootArea : ObservableWidgetViewContainer
	{
		private ItemsControl _menu;
		private int _insertId;

		public MenuRootArea(string id, InitialWidgetParameters parameters)
			: base(id, parameters)
		{
		    ContentReady += (s, e) =>
		        {
		            _menu = Content as ItemsControl;
		        };
		}
         
		protected override bool DoAddVisual(IWidgetViewContainer child)
		{
			_menu.Items.Insert(_insertId++, child.Content);
		    base.DoAddVisual(child);
			return true;
		}

	    protected override bool DoCanAddwidget(IWidgetViewContainer widget)
	    {
	        return !(widget is ObservableWidgetViewContainer);
	    }

	    protected override void DoRemoveVisual(IWidgetViewContainer child)
	    {
	        int currentIndex = _menu.Items.IndexOf(child.Content);
            if (currentIndex >= 0)
            {
                if (currentIndex <= _insertId) _insertId--;
                _menu.Items.Remove(child.Content);
            }

            base.DoRemoveVisual(child);
	    }

        public override void LoadState(XElement state)
        {
            _insertId = 0;
            base.LoadState(state);
        }

	    public override void SwapChildren(IWidgetViewContainer a, IWidgetViewContainer b)
	    {
            base.SwapChildren(a, b);
            var indexA = _menu.Items.IndexOf(a.Content);
            var indexB = _menu.Items.IndexOf(b.Content);
            _menu.Items[indexB] = null;
            _menu.Items[indexA] = b.Content;
            _menu.Items[indexB] = a.Content;
	    }
        
	    internal int CurrentInsertId
	    {
            get
            {
                return _insertId; 
            }
            set
            {
                _insertId = value;
            }
	    }

	    public override string NodeName
	    {
	        get
	        {
	            return "MenuRootArea";
	        }
	    }
	}
}
