﻿using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Menu
{
	internal static class MenuFactories
	{
		private static ChromeManagerBase chromeManager;
		private static IShell shell;

		internal static void Initialize(IShell shell_, ChromeManagerBase chromeManager_)
		{
			chromeManager = chromeManager_;
			shell = shell_;
			chromeManager.Registry.RegisterWidgetFactoryMapping<InitialMenuParameters>(MenuRootFactory);
		}

		[FactoryOptions(typeof(ChromeManager.ChromeArea.Menu.MenuRootArea))]
		private static bool MenuRootFactory(IWidgetViewContainer vc_, XDocument state_)
		{
			vc_.Content = shell.MainMenu;
			return true;
		}
	}
}
