﻿using System;
using System.Linq;
using System.Windows.Media;
using Infragistics.Windows.OutlookBar;
using Infragistics.Windows.OutlookBar.Events;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.NavigationBar
{
    internal class OutlookBarArea : ObservableWidgetViewContainer, IOutlookBar
    {         
        XamOutlookBar outlookBar;

         public OutlookBarArea(string ID, InitialWidgetParameters parameters_)
        : base(ID, parameters_)
		{
		    ContentReady += OnContentReady;
		}

        private void OnContentReady(object sender_, EventArgs eventArgs_)
        {
            outlookBar = (XamOutlookBar) Content;
            outlookBar.IsMinimized = minimized;
            outlookBar.SelectedGroupChanging += outlookBar_SelectedGroupChanging;
            outlookBar.SelectedGroupChanged += outlookBar_SelectedGroupChanged;

        }

        private string oldGroupName;
        private string newGroupName;
        void outlookBar_SelectedGroupChanging(object sender, SelectedGroupChangingEventArgs e)
        {
            oldGroupName = e.CurrentSelectedOutlookBarGroup == null
                ? null : e.CurrentSelectedOutlookBarGroup.Header.ToString();
            newGroupName = e.NewSelectedOutlookBarGroup == null
                               ? null : e.NewSelectedOutlookBarGroup.Header.ToString();
        }

        void outlookBar_SelectedGroupChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            OnActiveGroupChanged(new ActiveGroupChangedEventArgs(){ New = newGroupName, Old = oldGroupName});
            oldGroupName = newGroupName = null;
        }

        public override InitialWidgetParameters AddDefault(ref string ID)
        {
            var newGroup = new InitialOutlookBarGroupParameters() {Text = ID};
            ID = Guid.NewGuid().ToString();
            return newGroup;
        }

        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            if (child.ContentOfType<OutlookBarGroup>())
            {
                outlookBar.Groups.Add(child.Content as OutlookBarGroup);

                return base.DoAddVisual(child);
            } 
            return false;
        }

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            if (child.ContentOfType<OutlookBarGroup>())
            {
                outlookBar.Groups.Remove(child.Content as OutlookBarGroup);

            } 
            base.DoRemoveVisual(child);
        }

        protected override void DoInsertVisual(int index, IWidgetViewContainer child)
        {
            if (child.ContentOfType<OutlookBarGroup>())
            {
                outlookBar.Groups.Insert(index, child.Content as OutlookBarGroup); 
            } 
            base.DoInsertVisual(index, child);
        }

        protected override bool DoCanAddwidget(IWidgetViewContainer widget)
        {
            return widget is OutlookBarGroupArea;
        }

        public override void SwapChildren(IWidgetViewContainer a_, IWidgetViewContainer b_)
        {
            base.SwapChildren(a_, b_);
            var aBar = a_.Content as OutlookBarGroup;
            var bBar = b_.Content as OutlookBarGroup;
            if (aBar == null || bBar == null) return;
            var indexA = outlookBar.Groups.IndexOf(aBar);
            var indexB = outlookBar.Groups.IndexOf(bBar);
            outlookBar.Groups[indexB] = new OutlookBarGroup(); //null?
            outlookBar.Groups[indexA] = bBar;
            outlookBar.Groups[indexB] = aBar; 
        }

        private bool minimized = false;
        public bool IsMinimized
        {
            get { return outlookBar == null ? minimized : outlookBar.IsMinimized; }
            set
            {
                if (outlookBar == null)
                {
                    minimized = value;
                }
                else
                {
                    outlookBar.IsMinimized = value;
                }
            }
        }
        public override string NodeName
        {
            get
            {
                return "Outlookbar";
            }
        }


        IOutlookBarGroup IOutlookBar.this[string groupName_]
        {
            get { return this[groupName_] as IOutlookBarGroup; }
        }

        IOutlookBarGroup IOutlookBar.AddGroup(string groupName_, ImageSource largeImage_=null, ImageSource smallImage_=null)
        {
            return this.AddWidget(groupName_, new InitialOutlookBarGroupParameters() { Text = groupName_, LargeImage =  largeImage_, SmallImage = smallImage_}) as IOutlookBarGroup;
        }

        public event EventHandler<ActiveGroupChangedEventArgs> ActiveGroupChanged;

        protected virtual void OnActiveGroupChanged(ActiveGroupChangedEventArgs e_)
        {
            var handler = ActiveGroupChanged;
            if (handler != null) handler(this, e_);
        }


        public IOutlookBarGroup ActiveGroup
        {
            get
            {
                if (outlookBar == null || outlookBar.SelectedGroup == null) return null;
                return this.Children.FirstOrDefault(c_=>c_.Content == outlookBar.SelectedGroup) as IOutlookBarGroup;
            }
        }
    }
}
