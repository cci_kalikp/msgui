﻿using System.Windows;
using System.Windows.Controls;
using Infragistics.Windows.OutlookBar;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.NavigationBar
{
    class OutlookBarGroupArea : WidgetViewContainer, IOutlookBarGroup
    {
        private OutlookBarGroup group;
        private StackPanel panel;
        public OutlookBarGroupArea(string ID, InitialWidgetParameters parameters)
            : base(ID, parameters)
        {
            ContentReady += (s, e) =>
                {
                    group = ((OutlookBarGroup) Content);
                    panel = new StackPanel() { Orientation = Orientation.Vertical };
                    group.Content = panel;
                }; 
        }

        public void Activate()
        {
            if (group != null)
            {
                group.IsSelected = true;
            }
        }

        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            UIElement content = child.Content as UIElement;
            if (content != null)
            {
                panel.Children.Add(content);
                return base.DoAddVisual(child);
            }
            return false;
        }

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            UIElement content = child.Content as UIElement;
            if (content != null)
            {
                panel.Children.Remove(content); 
            }
            base.DoRemoveVisual(child);
        }
    }
}
