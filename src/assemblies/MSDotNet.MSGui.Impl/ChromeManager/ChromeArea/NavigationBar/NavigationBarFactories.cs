﻿using System.Xml.Linq;
using Infragistics.Windows.OutlookBar;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Navigation;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.NavigationBar
{
    internal static class NavigationBarFactories
    {
        private static ChromeManagerBase chromeManager;
        private static ShellWindow shellWindow;
        private static Application.Application application;

        internal static void Initialize(IShell shell_, ChromeManagerBase chromeManager_, IUnityContainer container_)
        {
            chromeManager = chromeManager_;
            shellWindow = shell_.MainWindow as ShellWindow;
            application = container_.Resolve<IApplication>() as Application.Application;
            chromeManager_.Registry.RegisterWidgetFactoryMapping<InitialNavigationBarParameters>(NavigationBarFactory);
            chromeManager_.Registry.RegisterWidgetFactoryMapping<InitialOutlookBarParameters>(OutlookBarFactory);
            chromeManager_.Registry.RegisterWidgetFactoryMapping<InitialOutlookBarGroupParameters>(OutlookBarGroupFactory);
        }

        [FactoryOptions(typeof(NavigationBarRootArea))]
        private static bool NavigationBarFactory(IWidgetViewContainer emptyViewContainer_, XDocument state_)
        {
            if (shellWindow == null) return false;
            emptyViewContainer_.Content = shellWindow.NavigationArea;
            return true; 
        }


        [FactoryOptions(typeof(OutlookBarArea))]
        private static bool OutlookBarFactory(IWidgetViewContainer vc_, XDocument state_)
        {
            var parameters = vc_.Parameters as InitialOutlookBarParameters;
            if (parameters != null)
            {
                if (vc_.Content == null)
                {
                    var bar = new XamOutlookBar();
                    string theme = GetOutlookBarTheme();
                    if (!string.IsNullOrEmpty(theme))
                    {
                        bar.Theme = theme;
                    }
                    application.ThemeChanged += (sender_, args_) =>
                    {
                        theme = GetOutlookBarTheme();
                        if (!string.IsNullOrEmpty(theme))
                        {
                            bar.Theme = theme;
                        }
                    };
                    if (!double.IsNaN(parameters.MinWidth))
                    {
                        bar.MinWidth = parameters.MinWidth;
                    }
                    if (!double.IsNaN(parameters.Width))
                    {
                        bar.Width = parameters.Width;
                    } 
                    vc_.Content = bar;
                } 
                

                return true;
            }

            return false; 
        }

        private static string GetOutlookBarTheme()
        {
            switch (application.CurrentThemeName)
            {
                case "Blue":
                case "BackgroundModifiedBlue":
                    return "Office2k7Blue";
                case "Black":
                    return "Office2k7Black";
                case "White":
                    return "Office2k7Silver";
            }
            return null;
        }

        [FactoryOptions(typeof(OutlookBarGroupArea))]
        private static bool OutlookBarGroupFactory(IWidgetViewContainer vc_, XDocument state_)
        {
            var parameters = vc_.Parameters as InitialOutlookBarGroupParameters;
            if (parameters != null)
            {
                if (vc_.Parent is OutlookBarArea)
                {

                    if (vc_.Content == null)
                    {
                        vc_.Content = new OutlookBarGroup()
                            {
                                Header = parameters.Text,
                                LargeImage = parameters.LargeImage,
                                SmallImage = parameters.SmallImage
                            };
                        return true;
                    }
                }
                 
            }

            return false;  
        }
    }
}
