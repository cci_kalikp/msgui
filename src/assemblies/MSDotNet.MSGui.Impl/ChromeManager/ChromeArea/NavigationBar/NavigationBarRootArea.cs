﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.NavigationBar
{
    class NavigationBarRootArea:WidgetViewContainer
    {
        private DockPanel navigationBar;
        private GridSplitter splitter;
        private ColumnDefinition navigationColumn;
        public NavigationBarRootArea(string ID, InitialWidgetParameters parameters)
			: base(ID, parameters)
		{
			this.ContentReady += (s, e) =>
			    {
			        navigationBar = (DockPanel)Content;
			        navigationColumn = ((ShellWindow) chromeManager.Shell.MainWindow).NavigationColumn;
			        splitter = ((ShellWindow) chromeManager.Shell.MainWindow).NavigationAreaSplitter;
			    };
		}

        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            FrameworkElement content = child.Content as FrameworkElement;
            if (content != null)
            {
                navigationBar.Visibility = Visibility.Visible;

                
                content.IsVisibleChanged += (s, e) =>
                                                {
                                                    if (!(bool)e.NewValue)
                                                    {
                                                        navigationColumn.Width = GridLength.Auto;
                                                        splitter.Visibility = Visibility.Collapsed;
                                                    }
                                                    else
                                                    {
                                                        splitter.Visibility = Visibility.Visible;
                                                        //splitter.ResizeBehavior = GridResizeBehavior.CurrentAndNext;
                                                        
                                                        //todo: handle multiple children
                                                        var hoster = content as WindowsFormsHost;
                                                        if (hoster != null && hoster.Child != null)
                                                        {
                                                            navigationColumn.Width = new GridLength(hoster.Child.Width);
                                                        }
                                                        else
                                                        {
                                                            if (!double.IsNaN(content.Width))
                                                            {
                                                                navigationColumn.Width = new GridLength(content.Width);
                                                            }
                                                        }
                                                    }
                                                };
                 
                if (!(child is OutlookBarArea))
                {
                    splitter.Visibility = Visibility.Visible;
                }
                
                navigationBar.Children.Add(content);
                return base.DoAddVisual(child);

            } 
            return false;
        }

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            if (child.ContentOfType<UIElement>())
            {
                navigationBar.Children.Remove((UIElement)child.Content);
                if (navigationBar.Children.Count == 0)
                {
                    navigationBar.Visibility =
                    splitter.Visibility = Visibility.Collapsed; 
                }
            }
            base.DoRemoveVisual(child);
        }
    
    }
}
