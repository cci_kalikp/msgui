﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Windows;
using System.Windows.Controls.Primitives;
using MorganStanley.MSDotNet.MSGui.Impl.StatusBar;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Statusbar
{
	internal class StatusbarRegionArea : WidgetViewContainer
	{
		private StackPanel m_panel;
		private readonly Dictionary<string, ItemPack> m_items = new Dictionary<string, ItemPack>();

        public StatusbarRegionArea(string ID, InitialWidgetParameters parameters)
            : base(ID, parameters)
        {
            this.ContentReady += (s, e) =>
            {
                this.m_panel = (this.Content as StatusBarItem).Content as StackPanel;

                ResolveDummies();
                //DoAdd replacements here instead of below.
            };
        }

	    public StatusbarRegionArea(string factoryID) : base(factoryID)
	    {
	    }

	    public StatusbarRegionArea(InitialWidgetParameters parameters, string factoryID) : this(factoryID, parameters)
	    {
	    }

        public ItemCollection ContextMenuCollection { get; set; }


	    internal void AddDummy(string Id, IWidgetViewContainer dummyWidget)
        {
            this.m_children.Add(Id, dummyWidget);
        }

		private void ResolveDummies()
		{
			var keys = this.m_children.Keys.ToArray();

			foreach (var childID in keys)
			{
				if (this.m_children[childID].ContentOfType<StatusbarDummyItem>())
				{
					var child = this.m_children[childID];
					var oldItem = child.Content as StatusbarDummyItem;

					var realChild = oldItem.item as IWidgetViewContainer;
					var region = this;

					realChild.ContentReady += (s, e) =>
					{
						var w = s as IWidgetViewContainer;
						var newItem = w.Content as IStatusBarItem;
						newItem.UpdateStatus(oldItem.level, oldItem.statusText);
						var copy = new List<StatusUpdatedEventHandler>(oldItem.m_statusUpdatedEventHandlers);
						foreach (var handler in copy)
						{
							newItem.StatusUpdated += handler;
							oldItem.StatusUpdated -= handler;
						}

						region.m_children[w.FactoryID] = realChild;
					};

				}
			}
		}

		protected override bool DoAddVisual(IWidgetViewContainer child)
		{
			IStatusBarItem item_ = child.Content as IStatusBarItem;
			if (item_ == null)
			{
				item_ = new WrapperStatusItem(child.Content as DependencyObject);
			}

		    var fe = item_.Control as FrameworkElement;
            if (fe != null && fe.Parent != null)
            {
                var sbi = fe.Parent as StatusBarItem;
                if (sbi != null)
                {
                    sbi.Content = null;
                }
            }

			this.m_panel.Children.Add(new StatusBarItem() 
				{
					Content = fe,
					FlowDirection = FlowDirection.LeftToRight
				}
			);


            if (item_ is HidableStatusBarItemBase)
            {
                ContextMenuCollection.Add(item_);
            }

			return base.DoAddVisual(child);
		}

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            var item =
                this.m_panel.Children.OfType<StatusBarItem>()
                    .Where(
                        (i) =>
                        child.Content is IStatusBarItem
                            ? i.Content == ((IStatusBarItem) child.Content).Control
                            : ((WrapperStatusItem) i.Content).Control == child.Content);
            if (item.Any())
                this.m_panel.Children.Remove(item.First());


            if (child.Content is HidableStatusBarItemBase)
            {
                ContextMenuCollection.Remove(child.Content);
            }

            base.DoRemoveVisual(child);
        }

		protected override bool DoAddLogical(IWidgetViewContainer child)
		{
			if (this.m_children.ContainsKey(child.FactoryID))
			{
				var oldItem = this.m_children[child.FactoryID].Content as StatusbarDummyItem;
				if (oldItem != null)
				{
					oldItem.item = child;
				}
			}

			return base.DoAddLogical(child);
		}

		private class ItemPack
		{
			public ItemPack(IStatusBarItem item_, Control container_, Panel panel_)
			{
				BarItem = item_;
				ControlContainer = container_;
				Panel = panel_;
			}
			public IStatusBarItem BarItem { get; private set; }
			public Control ControlContainer { get; private set; }
			public Panel Panel { get; private set; }
		}

	}
}
