﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.StatusBar;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.StatusBar;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Statusbar
{
    internal partial class StatusbarRootArea : WidgetViewContainer, MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent.IStatusBar
	{
		internal const string MAIN_STATUS_ITEM_NAME = "MainStatus";
		internal const string StatusbarLeftEdgeID = "2CFAB65E-B627-426C-BE52-76E4D01E9C04";
		internal const string StatusbarRightEdgeID = "159645A7-D978-499B-8E52-2FDC38C2CE80";


		private System.Windows.Controls.Primitives.StatusBar m_statusbar;

		public StatusbarRootArea(string ID, InitialWidgetParameters parameters)
			: base(ID, parameters)
		{
			this.ContentReady += (s, e) =>
			{
				this.m_statusbar = this.Content as System.Windows.Controls.Primitives.StatusBar;
			    m_statusbar.Visibility = ShellModeExtension.GetVisibilityForElement(HideLockUIGranularity.HideStatusBar);

			    ((StatusbarRegionArea) this["left"]).ContextMenuCollection = m_statusbar.ContextMenu.Items;
                ((StatusbarRegionArea) this["right"]).ContextMenuCollection = m_statusbar.ContextMenu.Items;
			};
		}
		
		public override IWidgetViewContainer this[string index]
		{
			get
			{
				if (index.Trim().ToLower() == "left")
				{
					return base[StatusbarLeftEdgeID];
				}
				else if (index.Trim().ToLower() == "right")
				{
					return base[StatusbarRightEdgeID];
				}
				else
				{
					return this;
				}
			}
		}

		protected override bool DoCanAddwidget(IWidgetViewContainer widget)
		{
			return widget is StatusbarRegionArea;
		}

		protected override bool DoAddVisual(Core.ChromeManager.IWidgetViewContainer child)
		{
			this.m_statusbar.Items.Add(child.Content);
			return base.DoAddVisual(child);
		}


		protected override void DoRemoveVisual(IWidgetViewContainer child)
		{
			this.m_statusbar.Items.Remove(child.Content);
            base.DoRemoveVisual(child);
		}

		#region IStatusBar Members

		public IWidgetViewContainer LeftSide
		{
			get 
			{
				return this["left"];
			}
		}

		public IWidgetViewContainer RightSide
		{
			get
			{
				return this["right"];
			}
		}

		#endregion
	}
}
