﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;
using MorganStanley.MSDotNet.MSGui.Impl.StatusBar;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Statusbar
{
	public class ProgressBarViewContainer : WidgetViewContainer, IProgressBarViewContainer
	{
		//public ProgressBarViewContainer(string factoryID)
		//    : base(factoryID)
		//{
		//    this.Parameters = new InitialProgressBarParameters();
		//    this.FactoryID = factoryID;
		//}

		//public ProgressBarViewContainer(InitialWidgetParameters parameters, string factoryID)
		//    :base (parameters, factoryID)
		//{
		//    this.Parameters = parameters as InitialProgressBarParameters;
		//    this.FactoryID = factoryID;
		//}

		public ProgressBarViewContainer(string factoryID, InitialWidgetParameters parameters)
			: base(factoryID, parameters)
		{
			this.Parameters = parameters as InitialProgressBarParameters;
			this.FactoryID = factoryID;

			this.ContentReady += (s, e) =>
			{
				this.m_progressBar = this.Content as ProgressBarStatusItem;
			};
		}

		ProgressBarStatusItem m_progressBar; 
	
		#region IProgressBarViewContainer Members

		private double m_minValue;
		public double MinValue
		{
			get
			{
				return this.m_minValue;
			}
			set
			{
				this.m_minValue = value;
				this.OnPropertyChanged("MinValue");
			}
		}

		private double m_maxValue;
		public double MaxValue
		{
			get
			{
				return this.m_maxValue;
			}
			set
			{
				this.m_maxValue = value;
				this.OnPropertyChanged("MaxValue");
			}
		}

		private double m_position;
		public double Position
		{
			get
			{
				return this.m_position;
			}
			set
			{
				this.m_position = value;
				this.m_progressBar.UpdatePosition(value);
				this.OnPropertyChanged("Position");
			}
		}

		#endregion
	}
}
