﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Impl.StatusBar;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Statusbar
{
	internal class InitialStatusbarParameters : InitialWidgetParameters
	{

	}

	internal class InitialStatusbarRegionParameters : InitialWidgetParameters
	{
		public HorizontalAlignment Alignment { get; set; }
	}

}
