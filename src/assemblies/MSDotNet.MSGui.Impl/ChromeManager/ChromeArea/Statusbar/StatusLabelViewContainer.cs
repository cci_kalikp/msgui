﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.StatusBar;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Statusbar
{
	public class StatusLabelViewContainer : WidgetViewContainer, IStatusLabelViewContainer
	{
		//public StatusLabelViewContainer(string factoryID)
		//    : base(factoryID)
		//{
		//    this.Parameters = new InitialStatusLabelParameters();
		//    this.FactoryID = factoryID;
		//}

		//public StatusLabelViewContainer(InitialWidgetParameters parameters, string factoryID)
		//    : base(parameters, factoryID)
		//{
		//    this.Parameters = parameters as InitialStatusLabelParameters;
		//    this.FactoryID = factoryID;
		//}

		private TextStatusItem m_label = null;


		public StatusLabelViewContainer(string factoryID, InitialWidgetParameters parameters)
			: base(factoryID, parameters)
		{
			this.Parameters = parameters as InitialStatusLabelParameters;
			this.FactoryID = factoryID;
			this.ContentReady += (s, e) =>
			{
				this.m_label = this.Content as TextStatusItem;
			};
		}

		#region IStatusLabelViewContainer Members

		private StatusLevel m_level;
		public StatusLevel Level
		{
			get
			{
				return this.m_level;
			}
			set
			{
				this.m_level = value;
                if(m_label != null)
                    this.m_label.UpdateLevel(value);

				this.OnPropertyChanged("Level");
			}
		}

		private string m_text;
		public string Text
		{
			get
			{
				return this.m_text;
			}
			set
			{
				this.m_text = value;
                if(m_label != null)
                    this.m_label.UpdateText(value);

				this.OnPropertyChanged("Text");
			}
		}

		#endregion
	}
}
