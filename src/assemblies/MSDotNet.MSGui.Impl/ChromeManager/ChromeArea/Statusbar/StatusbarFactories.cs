﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Automation;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Impl.StatusBar;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Statusbar
{
	internal class StatusbarHelper : IStatusbarFactories
	{
		#region IStatusbarHelper Members

		public InitializeWidgetHandler LabelFactory { get { return this.LabelFactoryMethod; } }

		public InitializeWidgetHandler ProgressBarFactory { get { return this.ProgressBarFactoryMethod; } }

		public InitializeWidgetHandler SeparatorFactory { get { return this.SeparatorFactoryMethod; } }

		#endregion

		private static IShell m_shell;

		public StatusbarHelper(IShell shell_, ChromeRegistry chromeRegistry_)
		{
			m_shell = shell_;

			chromeRegistry_.RegisterWidgetFactoryMapping<InitialStatusbarParameters>(StatusbarRootFactory);
			chromeRegistry_.RegisterWidgetFactoryMapping<InitialStatusbarRegionParameters>(StatusbarRegionFactory);

			chromeRegistry_.RegisterWidgetFactoryMapping<InitialStatusLabelParameters>(this.LabelFactory);
			chromeRegistry_.RegisterWidgetFactoryMapping<InitialProgressBarParameters>(this.ProgressBarFactory);
			chromeRegistry_.RegisterWidgetFactoryMapping<InitialSeparatorParameters>(this.SeparatorFactory);
		}

		[FactoryOptions(typeof(ChromeManager.ChromeArea.Statusbar.StatusbarRootArea))]
		private static bool StatusbarRootFactory(IWidgetViewContainer vc_, XDocument state_)
		{
			var statusBar = m_shell.StatusBar;
			vc_.Content = statusBar;
			return true;
		}

		[FactoryOptions(typeof(ChromeManager.ChromeArea.Statusbar.StatusbarRegionArea))]
		private static bool StatusbarRegionFactory(IWidgetViewContainer vc_, XDocument state_)
		{
			var parameters = vc_.Parameters as InitialStatusbarRegionParameters;
			if (vc_.Parent.ContentOfType<System.Windows.Controls.Primitives.StatusBar>() && parameters != null)
			{
				var stackPanel = new StackPanel()
				{
					Orientation = Orientation.Horizontal,
					FlowDirection = (parameters.Alignment == HorizontalAlignment.Right) ? FlowDirection.RightToLeft : FlowDirection.LeftToRight
				};

				vc_.Content = new StatusBarItem()
				{
					Content = stackPanel,
					HorizontalAlignment = parameters.Alignment
				};

				return true;
			}

			return false;
		}

		[FactoryOptions(typeof(StatusLabelViewContainer))]
		private bool LabelFactoryMethod(IWidgetViewContainer viewContainer_, XDocument state_)
		{
			var label = new TextStatusItem();


            if (label.Control is DependencyObject)
                AutomationProperties.SetAutomationId(label.Control as DependencyObject, "MSDesktop_Main_StatusLable");
			
			var parameters = viewContainer_.Parameters as InitialStatusLabelParameters;
			if (parameters != null)
			{
				label.UpdateLevel(parameters.Level);
				label.UpdateText(parameters.Text);
			    label.UpdateIsVisible(parameters.IsVisible);
			}

			viewContainer_.Content = label;


			return true;
		}

		[FactoryOptions(typeof(ProgressBarViewContainer))]
		private bool ProgressBarFactoryMethod(IWidgetViewContainer viewContainer_, XDocument state_)
		{
			InitialProgressBarParameters initialParameters = viewContainer_.Parameters as InitialProgressBarParameters;
			if (initialParameters != null)
			{
				viewContainer_.Content = new ProgressBarStatusItem(initialParameters.MinValue, initialParameters.MaxValue);
				var viewContainer = viewContainer_ as ProgressBarViewContainer;
				if (viewContainer != null)
				{
					viewContainer.MaxValue = initialParameters.MaxValue;
					viewContainer.MinValue = initialParameters.MinValue;
				}


				return true;
			}
			else
			{
				return false;
			}
		}

		private bool SeparatorFactoryMethod(IWidgetViewContainer viewContainer_, XDocument state_)
		{
			viewContainer_.Content = new SeparatorStatusItem();
			return true;
		}
	}
}
