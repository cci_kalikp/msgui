﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Statusbar
{
	internal class StatusbarDummyItem : IStatusBarItem
	{
		internal string statusText;
		internal StatusLevel level;
		internal IWidgetViewContainer item;

		#region IStatusBarItem Members

		public void UpdateStatus(StatusLevel level_, string statusText_)
		{
			this.statusText = statusText_;
			this.level = level_;

			this.DoStatusUpdated();
		}

		public void ClearStatus()
		{
			this.statusText = "";
			this.level = StatusLevel.Information;

			this.DoStatusUpdated();
		}

		private void DoStatusUpdated()
		{
			var copy = this.m_statusUpdated;
			if (copy != null)
			{
				copy(this, new StatusUpdatedEventArgs(statusText, level));
			}
		}

		public object Control
		{
			get
			{
				return null;
			}
		}

		internal List<StatusUpdatedEventHandler> m_statusUpdatedEventHandlers = new List<StatusUpdatedEventHandler>();
		private event StatusUpdatedEventHandler m_statusUpdated;
		public event StatusUpdatedEventHandler StatusUpdated
		{
			add
			{
				this.m_statusUpdated += value;
				m_statusUpdatedEventHandlers.Add(value);
			}
			remove
			{
				this.m_statusUpdated -= value;
				m_statusUpdatedEventHandlers.Remove(value);
			}
		}

		#endregion
	}
}
