﻿using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.LauncherBar;
using IStatusBar = MorganStanley.MSDotNet.MSGui.Core.IStatusBar;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
    partial class ChromeManagerBase
    {

        public IRibbon Ribbon
        {
            get { return this[Core.ChromeManager.Helpers.ChromeArea.Ribbon] as IRibbon; }
        }

        public ITabbedDock Tabwell
        {
            get { return this[Core.ChromeManager.Helpers.ChromeArea.TabWell] as ITabbedDock; }
        }

        public IStatusBar StatusBar
        {
            get { return this[Core.ChromeManager.Helpers.ChromeArea.StatusBar] as IStatusBar; }
        }

        public ITitleBar TitleBar
        {
            get { return this[Core.ChromeManager.Helpers.ChromeArea.TitleBar] as ITitleBar; }
        }

        internal LauncherBarRootArea LauncherBar
        {
            get { return this[Core.ChromeManager.Helpers.ChromeArea.LauncherBar] as LauncherBarRootArea; }
        }
    }
}
