﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea
{
    public class ButtonViewContainerBase: WidgetViewContainer, IButtonViewContainer
    {
        protected FrameworkElement button;

        public ButtonViewContainerBase(string factoryID, InitialWidgetParameters parameters)
			: base(factoryID, parameters)
		{
			this.ContentReady += (s, e) =>
			{
                button = this.Content as FrameworkElement;
			};
		}

        protected virtual bool SetText(string text_)
        {
            MenuItem menu = this.button as MenuItem;
            if (menu != null)
            {
                menu.Header = text_;
                return true;
            }
            return false;
        }
        protected virtual bool SetImage(ImageSource image_)
        {
            MenuItem menu = this.button as MenuItem;
            if (menu != null)
            {
                menu.Icon = image_ == null ? null : new Image() {Source = image_};
                return true;
            }
            return false;
        }
		#region IButtonViewContainer Members

		private string m_text;
		public string Text
		{
			get
			{
				return this.m_text;
			}
			set
			{
				this.m_text = value;
				
				if (this.button != null)
				{
				    SetText(m_text);
				}
			}
		}

		private EventHandler m_click;

		public EventHandler Click
		{
			get
			{
				return this.m_click;
			}
			set
			{
				this.m_click = value;
				this.OnPropertyChanged("Click");
			}
		}

		private bool m_enabled;

		public bool Enabled
		{
			get
			{
				return this.m_enabled;
			}
			set
			{
                this.m_enabled = value; 
                
                if (this.button != null)
                    this.button.IsEnabled = value;

				this.OnPropertyChanged("Enabled");
			}
		}

		private string m_toolTip;

		public string ToolTip
		{
			get
			{
				return this.m_toolTip;
			}
			set
			{
                this.m_toolTip = value;

                if (this.button != null)
                    this.button.ToolTip = value;
				
				this.OnPropertyChanged("ToolTip");
			}
		}

		private ImageSource m_image;

		public ImageSource Image
		{
			get
			{
				return this.m_image;
			}
			set
			{
                this.m_image = value;

                if (this.button != null)
                    SetImage(m_image);
				
				this.OnPropertyChanged("Image");
			}
		}

		private ButtonSize m_size;
	    private KeyGesture m_gesture;

	    public ButtonSize Size
		{
			get
			{
				return this.m_size;
			}
			set
			{
                this.m_size = value;
				this.OnPropertyChanged("Size");
			}
		}

	    public KeyGesture Gesture
	    {
	        get
	        {
	            return m_gesture;
	        }
	        set
	        {
	            m_gesture = value;
                OnPropertyChanged("Gesture");                
	        }
	    }

	    #endregion IButtonViewContainer Members
    }
}
