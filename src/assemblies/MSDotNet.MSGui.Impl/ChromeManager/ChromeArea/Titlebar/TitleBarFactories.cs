﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.TitleBar;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Titlebar
{
    internal class TitleBarFactories: ITitleBarFactories
    {
        private static IShell _shell;
        private static IChromeRegistry _chromeRegistry;
        public TitleBarFactories(IShell shell, ChromeRegistry chromeRegistry)
        {
            _shell = shell;
            _chromeRegistry = chromeRegistry;
            
            chromeRegistry.RegisterWidgetFactoryMapping<InitialTitleBarRootParameters>(TitleBarRootFactory);
            chromeRegistry.RegisterWidgetFactoryMapping<InitialTitleBarWidgetParameters>(TitleBarWidgetFactoryMethod);
            
		}


        [FactoryOptions(typeof(ChromeManager.ChromeArea.Titlebar.TitlebarRootArea))]
        private static bool TitleBarRootFactory(IWidgetViewContainer vc_, XDocument state_)
        {
            var parameters = vc_.Parameters as InitialTitleBarRootParameters;
            var titleBar = (TitleBarPlaceHolder) _shell.TitleBar;
            if(parameters != null)
                titleBar.AppIconVisibility = parameters.AppIconVisibility;


            vc_.Content = titleBar;

            
            return true;
        }


        [FactoryOptions(typeof(TitleBarViewContainer))]
        private static bool TitleBarWidgetFactoryMethod(IWidgetViewContainer viewContainer, XDocument state)
        {
            
            var root = (viewContainer.Parent as TitlebarRootArea) as ITitleBar;
            if (root == null)
                return false;

            var parameter = viewContainer.Parameters as InitialTitleBarWidgetParameters;
            if(parameter == null)
                return false;


            return true;
        }
    }
}
