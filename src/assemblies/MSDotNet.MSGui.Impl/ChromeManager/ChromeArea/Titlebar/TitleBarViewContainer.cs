﻿using System.Windows;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Titlebar
{
    internal class TitleBarViewContainer : WidgetViewContainer
    {
        private TitleBarPlaceHolder _container;
        internal TitleBarPlaceHolder Container
        {
            set
            {
                _container = value;
                return;

            }
        }


        internal Location Location { get; set; }

        public TitleBarViewContainer(string factoryID)
            : base(factoryID)
        {
        }

        public TitleBarViewContainer(InitialWidgetParameters parameters, string factoryID)
            : this(factoryID, parameters)
        {
        }

        public TitleBarViewContainer(string ID, InitialWidgetParameters parameters)
            : base(ID, parameters)
        {
            /*ContentReady += (s, e) =>
                                {
                                    _container.Content = this.Content;
                                };*/
        }

        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            var element = child.Content as UIElement;
            if (element == null)
                return false;
            switch (Location)
            {
                case Location.Left:
                    _container.AddToLeft(element);
                    break;
                case Location.Middle:
                    _container.AddToMiddle(element);
                    break;
                case Location.Right:
                    _container.AddToRight(element);
                    break;

            }
            return true;
        }
    }

    enum Location { Left, Middle, Right }
}
