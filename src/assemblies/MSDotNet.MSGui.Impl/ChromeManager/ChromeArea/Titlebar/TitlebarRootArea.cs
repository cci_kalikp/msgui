﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Titlebar
{
    internal class TitlebarRootArea : WidgetViewContainer, ITitleBar
    {
        internal const string TitleBarLeftEdgeID = "491402bc-da78-462e-82ef-fc76e591deae";
        internal const string TitleBarMiddleEdgeID = "600ee5a6-f01e-4d84-823d-c28da20a6d43";
        internal const string TitleBarRightEdgeID = "cdb2c639-4792-4a62-bef7-e40671cfc05d";


        private TitleBarPlaceHolder _titleBarPlaceHolder;
       
       


        public override IWidgetViewContainer this[string index]
        {
            get
            {
                if (index.Trim().ToLower() == "left")
                {
                    return base[TitleBarLeftEdgeID];
                }
                else if (index.Trim().ToLower() == "middle")
                {
                    return base[TitleBarMiddleEdgeID];
                }
                else if(index.Trim().ToLower() == "right")
                {
                    return base[TitleBarRightEdgeID];
                }
                else
                {
                    return base[index];
                }
            }
        }

      /*  public TitlebarRootArea(string factoryID) : base(factoryID)
        {
        }*/

        public TitlebarRootArea(InitialWidgetParameters parameters, string factoryID) : this(factoryID, parameters)
        {
        }

        protected override InitialWidgetParameters DoAddDefault(ref string ID)
        {
           // ID = "SubTitleRegion";
           // return base.DoAddDefault(ref ID);
            return new InitialTitleBarWidgetParameters() {Text = "Default Title"};
        }

        public TitlebarRootArea(string ID, InitialWidgetParameters parameters) :
            base(ID, parameters)
        {

            _height = SystemParameters.CaptionHeight;
           
            this.ContentReady += (s, e) =>
            {

                this._titleBarPlaceHolder = this.Content as TitleBarPlaceHolder;

                var left = (TitleBarViewContainer)(((ITitleBar)this).Left);
                left.Container = _titleBarPlaceHolder;
                left.Location = Location.Left;


                var middle = (TitleBarViewContainer)(((ITitleBar)this).Middle);
                middle.Container = _titleBarPlaceHolder;
                middle.Location = Location.Middle;


                var right = (TitleBarViewContainer)(((ITitleBar)this).Right);
                right.Container = _titleBarPlaceHolder;
                right.Location = Location.Right;

                _titleBarPlaceHolder.Height = this.Height;

/*
                 var binding = new Binding() { Source = ((ITitleBar)this).Left.Content };
                _titleBar.Left.SetBinding(ContentControl.ContentProperty, binding);

                binding = new Binding() { Source = ((ITitleBar)this).Middle.Content, Path = new PropertyPath("Content") };
                _titleBar.Middle.SetBinding(ContentControl.ContentProperty, binding);

                binding = new Binding() { Source = ((ITitleBar)this).Right.Content };
                _titleBar.Right.SetBinding(ContentControl.ContentProperty, binding);*/

            };
        }
        

        IWidgetViewContainer ITitleBar.Left
        {
            get { return this[TitleBarLeftEdgeID]; }
            set {
                this.RemoveWidget(this[TitleBarLeftEdgeID]);

                this.m_children[TitleBarLeftEdgeID] = value;
                this.AddVisual(value);
            }
        }

        IWidgetViewContainer ITitleBar.Middle
        {
            get { return this[TitleBarMiddleEdgeID];}
            set { this.m_children[TitleBarMiddleEdgeID] = value; }
        }

        IWidgetViewContainer ITitleBar.Right
        {
            get { return this[TitleBarRightEdgeID]; }
            set { this.m_children[TitleBarRightEdgeID] = value; }
        }

        private double _height;

        public double Height
        {
            get { return _height; }
            set { _height = value; }
        }
    }
}
