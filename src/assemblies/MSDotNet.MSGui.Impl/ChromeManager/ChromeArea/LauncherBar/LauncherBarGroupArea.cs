﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.LauncherBar
{
	internal class LauncherBarGroupArea : ObservableWidgetViewContainer
	{
		private readonly ObservableCollection<object> _items = new ObservableCollection<object>();


		public LauncherBarGroupArea(string ID, InitialWidgetParameters parameters)
			: base(ID, parameters)
		{
		    ContentReady += (s, e) =>
		        {
		            chromeManager.PropertyChanged += m_chromeManager_PropertyChanged;

		            var groupBox = Content as GroupBox;
		            if (groupBox != null)
		            {
		                var ic = groupBox.Content as ItemsControl;
		                if (ic != null)
		                {
		                    ic.ItemsSource = _items;
		                }
		            }

		            ObservableChildren.CollectionChanged += ObservableChildrenOnCollectionChanged;
		        };
		}

        private void ObservableChildrenOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            if (notifyCollectionChangedEventArgs.Action == NotifyCollectionChangedAction.Remove &&
                ObservableChildren.Count == 0)
            {
                ObservableChildren.CollectionChanged -= ObservableChildrenOnCollectionChanged;
                chromeManager.RemoveWidget(this);
            }
        }


		/// <summary>
		/// Event changed handler to update the LauncherBarState attached property on all children.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void m_chromeManager_PropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if (e.Property == LauncherBarChromeManager.LauncherBarStateProperty)
			{
				foreach (var widget in m_children.Values)
				{
					var uie = widget.Content as UIElement;
					if (uie != null)
					{
					    var launcherBarChromeManager = chromeManager as LauncherBarChromeManager;
					    if (launcherBarChromeManager != null)
					        LauncherBarChromeManager.SetLauncherBarState(uie, launcherBarChromeManager.State);
					}
				}
			}
		}

		protected override bool DoAddVisual(IWidgetViewContainer child)
		{
		    var frameworkElement = child.Content as FrameworkElement;
		    if (frameworkElement != null)
			{
				LauncherBarChromeManager.SetLauncherBarState(frameworkElement, (chromeManager as LauncherBarChromeManager).State);
			}

			_items.Add(child.Content);
		    return base.DoAddVisual(child); 
		}

		protected override void DoRemoveVisual(IWidgetViewContainer child)
		{
			_items.Remove(child.Content);
            base.DoRemoveVisual(child);
		}

        public override void Rename(string newName)
        {
            base.Rename(newName);
            (Content as GroupBox).Header = newName;
        }

	    public override string NodeName
	    {
	        get
	        {
	            return "Group";
	        }
	    }

	    public override void SwapChildren(IWidgetViewContainer a, IWidgetViewContainer b)
        {
            base.SwapChildren(a, b);
            var indexA = _items.IndexOf(a.Content);
            var indexB = _items.IndexOf(b.Content);
            _items[indexB] = null;
            _items[indexA] = b.Content;
            _items[indexB] = a.Content;
        }

	}
}
