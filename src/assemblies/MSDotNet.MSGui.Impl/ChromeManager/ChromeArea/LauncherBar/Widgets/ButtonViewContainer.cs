﻿using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Controls.BarMenu;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;  
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.LauncherBar.Widgets
{
	public class ButtonViewContainer : ButtonViewContainerBase
	{ 

		public ButtonViewContainer(string factoryID, InitialWidgetParameters parameters)
			: base(factoryID, parameters)
		{
			 
		}

        protected override bool SetText(string text_)
        {
            if (base.SetText(text_)) return true;
            BarToggleButton toggleButton = this.button as BarToggleButton;
            if (toggleButton != null)
            {
                toggleButton.Content = text_;
                return true;
            }
            LauncherBarButton launcherButton = this.button as LauncherBarButton;
            if (launcherButton != null)
            {
                launcherButton.MSGuiButton.Text = text_;
                return true;
            }
            return false;
        }
        protected override bool SetImage(ImageSource image_)
        {
            if (base.SetImage(image_)) return true;
            BarToggleButton toggleButton = this.button as BarToggleButton;
            if (toggleButton != null)
            {
                toggleButton.Image = image_;
                return true;
            }
            LauncherBarButton launcherButton = this.button as LauncherBarButton;
            if (launcherButton != null)
            {
                launcherButton.MSGuiButton.Image = image_;
                return true;
            }
            return false;
        }
		 
	}
}
