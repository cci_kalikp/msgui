﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls; 
using System.Collections.ObjectModel;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Controls.BarMenu;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.SearchBar; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.LauncherBar
{
    internal class LauncherBarDropdownArea : ObservableWidgetViewContainer, IDropdownButtonViewContainer
	{
		private LauncherBarChromeManager chromeManager;
        //internal LauncherBarChromeManager ChromeManager
        //{
        //    set
        //    {
        //        this.chromeManager = value;
        //        this.chromeManager.PropertyChanged += new DependencyPropertyChangedEventHandler(m_chromeManager_PropertyChanged);
        //    }
        //}

        private ItemsControl dropdown;
		private readonly ObservableCollection<object> items = new ObservableCollection<object>();

		public LauncherBarDropdownArea(string ID, InitialWidgetParameters parameters) : base(ID, parameters) 
		{
			this.ContentReady += (s, e) =>
			{
                //chromeManager.PropertyChanged += m_chromeManager_PropertyChanged;
                this.dropdown = this.Content as ItemsControl;
				this.dropdown.ItemsSource = this.items;
			};

		    this.chromeManager = ChromeManagerBase.Instance as LauncherBarChromeManager;
		    this.IsDropDownEnabled = true;
		}

		protected override bool DoAddVisual(IWidgetViewContainer child)
		{
			this.items.Add(child.Content);

			var item = child.Content as FrameworkElement;
			if (item != null)
			{
				LauncherBarChromeManager.SetLauncherBarState(item, this.chromeManager.State);
			}

			return base.DoAddVisual(child);
		}

		protected override void DoRemoveVisual(IWidgetViewContainer child)
		{
			if (this.items.Contains(child.Content))
			{
				this.items.Remove(child.Content);
			}

			base.DoRemoveVisual(child);
		}

		void m_chromeManager_PropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
		{

			if (e.Property == LauncherBarChromeManager.LauncherBarStateProperty)
			{
				foreach (var widget in this.m_children.Values)
				{
					var uie = widget.Content as UIElement;
					if (uie != null)
					{
						LauncherBarChromeManager.SetLauncherBarState(uie, this.chromeManager.State);
					}
				}
			}
		}

        private string text;
        public string Text
        {
            get
            {
                return this.text;
            }
            set
            {
                this.text = value;
                if (this.dropdown != null)
                {
                    var menu = dropdown as MenuItem;
                    if (menu != null)
                    {
                        menu.Header = value;
                    }
                    else
                    {
                        var menuTool = dropdown as BarMenuTool;
                        if (menuTool != null)
                        {
                            menuTool.Caption = value;
                        }
                    }
                } 
            }
        }


        public bool IsDropDownEnabled { get; set; }

        public override string NodeName
        {
            get
            {
                return "Dropdown";
            }
        }

        public override void LoadState(System.Xml.Linq.XElement state_)
        {
            if (state_.Name != NodeName) throw new ArgumentException();
            var lockerChildren = ((IWidgetViewContainerConfigNode)chromeManager[Core.ChromeManager.Helpers.ChromeArea.Locker]).ObservableChildren;
            foreach (var childElement in state_.Elements())
            {
                if (childElement.Elements().Count() != 0)
                {
                    var factoryId = childElement.Attribute("name").Value;
                    var currentName = childElement.Attribute("currentName").Value;
                    var widget = lockerChildren.FirstOrDefault(w => w.FactoryID == factoryId);
                    if (widget == null)
                    {
                        widget = chromeManager.AddWidget(currentName, new InitialDropdownButtonParameters { Text = currentName }, this);
                    }
                    else
                    {
                        chromeManager.MoveWidget(widget, this, null);
                    }
                    ((IWidgetViewContainerConfigNode)widget).LoadState(childElement);
                }
                else
                {
                    var factoryId = childElement.Attribute("name").Value;
                    var widget = lockerChildren.FirstOrDefault(w => w.FactoryID == factoryId);
                    if (widget != null)
                    {
                        chromeManager.MoveWidget(widget, this, null);
                    }
                    else if (factoryId.StartsWith(FavoriteWidgetPrefix))
                    {
                        var searchItemFactory = factoryId.Split('#')[1];
                        var searchItemWidget = chromeManager.GetWidget(searchItemFactory);
                        ((WidgetSearchArea)searchItemWidget.Parent).LoadFavorite(searchItemFactory, this);
                    }
                }
            }
        }

        public const string FavoriteWidgetPrefix = "MSDesktop.WidgetSearchArea.Favorite";
	}

    
}
