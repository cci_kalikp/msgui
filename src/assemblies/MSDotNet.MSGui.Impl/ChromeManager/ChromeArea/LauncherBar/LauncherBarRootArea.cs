﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.LauncherBar
{
    internal class LauncherBarRootArea : ObservableWidgetViewContainer, ILockerAffectedMainArea
	{
		private BarItemsHolder _barItemsHolder;

		public LauncherBarRootArea(string ID, InitialWidgetParameters parameters)
			: base(ID, parameters)
		{
			this.ContentReady += (s, e) =>
			{
				chromeManager.PropertyChanged += m_chromeManager_PropertyChanged;

                _barItemsHolder = ((LauncherBarChromeManager)chromeManager)._barItemsHolder;
			};
		}

		/// <summary>
		/// Event changed handler to update the LauncherBarState attached property on all children.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void m_chromeManager_PropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if (e.Property == LauncherBarChromeManager.LauncherBarStateProperty)
			{
				foreach (var widget in this.m_children.Values)
				{
					var uie = widget.Content as UIElement;
					if (uie != null)
					{
						LauncherBarChromeManager.SetLauncherBarState(uie, (chromeManager as LauncherBarChromeManager).State);
					}
				}
			}
		}

        protected override void DoInsertVisual(int index, IWidgetViewContainer child)
        {
            UIElement content = child.Content as UIElement;
            if (content != null)
            {
                this._barItemsHolder.InsertChild(index, content, false);
            }
            base.DoInsertVisual(index, child);
        }
		/// <summary>
		/// Return a new InitialLauncherGroupParameters instance to indicate that unknown children should be treated as
		/// implicitly requested groups.
		/// </summary>
		/// <param name="ID">The name of the new group.</param>
		/// <returns>An InitialLauncherGroupParameters instance representing the group.</returns>
		protected override InitialWidgetParameters DoAddDefault(ref string ID)
		{
			return new InitialLauncherGroupParameters() { Text = ID };
		}

		/// <summary>
		/// Adds a new item to the bar visually and sets the LauncherBarState attached property on it.
		/// </summary>
		/// <param name="child">The newly added child</param>
		/// <returns>True if successful.</returns>
		protected override bool DoAddVisual(IWidgetViewContainer child)
		{
			var item = child.Content as FrameworkElement;
			if (item != null)
			{
				LauncherBarChromeManager.SetLauncherBarState(item, (chromeManager as LauncherBarChromeManager).State);
			}

			if (child.Content is UIElement)
			{
				this._barItemsHolder.AddChild((UIElement)(child.Content), false);
			    base.DoAddVisual(child);
				return true;
			}


			return false;
		}

		protected override void DoRemoveVisual(IWidgetViewContainer child)
		{
			if (child.Content is UIElement)
			{
				this._barItemsHolder.RemoveChild((UIElement)child.Content);
                base.DoRemoveVisual(child);
			}
		}

        public override void SwapChildren(IWidgetViewContainer a_, IWidgetViewContainer b_)
        {
            base.SwapChildren(a_, b_);
            _barItemsHolder.SwapChildren(a_.Content as UIElement, b_.Content as UIElement);
        }

        public override string NodeName
        {
            get
            {
                return "Launcherbar";
            }
        }

        public override void LoadState(System.Xml.Linq.XElement state_)
        {
            if (state_.Name != NodeName) throw new ArgumentException();
            var lockerChildren = ((IWidgetViewContainerConfigNode) chromeManager[Core.ChromeManager.Helpers.ChromeArea.Locker]).ObservableChildren;
            foreach (var childElement in state_.Elements())
            {
                if (childElement.Elements().Count() != 0)
                {
                    var factoryId = childElement.Attribute("name").Value;
                    var currentName = childElement.Attribute("currentName").Value;
                    var widget = lockerChildren.FirstOrDefault(w => w.FactoryID == factoryId);
                    if (widget == null && childElement.Name == "Dropdown")
                    {
                        widget = chromeManager.AddWidget(currentName, new InitialDropdownButtonParameters {Text = currentName}, this);
                    }
                    else if (widget == null)
                    {
                        widget = chromeManager.AddWidget(currentName, new InitialLauncherGroupParameters { Text = currentName }, this);
                    }
                    else
                    {
                        chromeManager.MoveWidget(widget, this, null);
                    }
                    ((IWidgetViewContainerConfigNode) widget).LoadState(childElement);
                }
                else
                {
                    var factoryId = childElement.Attribute("name").Value;
                    var widget = lockerChildren.FirstOrDefault(w => w.FactoryID == factoryId);
                    if (widget != null)
                    {
                        chromeManager.MoveWidget(widget, this, null);
                    }
                }
            }
        }


        public bool TryGetRootVisual(out UIElement visual_)
        {
            visual_ = this._barItemsHolder.ButtonHolder; 
            return true;
        }
	}
}
