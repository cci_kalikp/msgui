﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Controls;
using MorganStanley.MSDotNet.MSGui.Controls.BarMenu;
using MorganStanley.MSDotNet.MSGui.Controls.SplitButton;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.LauncherBar.Widgets;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Menu;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.SearchBar;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls; 
using MorganStanley.MSDotNet.My; 

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.LauncherBar
{
    internal class LauncherBarFactories : ICommonFactories
    {
        private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<LauncherBarFactories>();
        private static IChromeManager _chromeManager;
        private readonly IChromeRegistry _registry;
        private static IShell _shell;
        private readonly IDictionary<string, ICommand> _commandCache = new Dictionary<string, ICommand>();
        private static readonly KeyGestureConverter GestureConverter = new KeyGestureConverter();
        private static bool _staticFactoriesInitialized;

        private static void InitializeLauncherFactories(IChromeRegistry registry)
        {
            registry.RegisterWidgetFactoryMapping<InitialLauncherBarParameters>(LauncherBarFactoryMethod);
            registry.RegisterWidgetFactoryMapping<InitialLauncherGroupParameters>(LauncherGroupFactoryMethod);
        }
        internal void Initialize(IChromeManager chromeManager)
        {
            _chromeManager = chromeManager;
            if (!_staticFactoriesInitialized)
            {
                InitializeLauncherFactories(_registry);
            }
        }

        internal static void Initialize(LauncherBarChromeManager chromeManager, IShell shell)
        {
            _chromeManager = chromeManager;
            _shell = shell;

            if (!_staticFactoriesInitialized)
            {
                InitializeLauncherFactories(chromeManager.Registry);
            }
        }
        public LauncherBarFactories(IChromeRegistry registry)
        {
            _registry = registry;
            _registry.RegisterWidgetFactoryMapping<InitialButtonParameters>(ButtonFactory);
            _registry.RegisterWidgetFactoryMapping<InitialCommandButtonParameters>(CommandButtonFactory);
            _registry.RegisterWidgetFactoryMapping<InitialSplitButtonParameters>(SplitButtonFactory);
            _registry.RegisterWidgetFactoryMapping<InitialShowWindowButtonParameters>(ShowWindowButtonFactory);
            _registry.RegisterWidgetFactoryMapping<InitialExistingElementWidgetParameters>(ShowExistingElementFactory);
            _registry.RegisterWidgetFactoryMapping<InitialDropdownButtonParameters>(DropdownButtonFactory);
            _registry.RegisterWidgetFactoryMapping<InitialComboBoxParameters>(ComboBoxFactory);
            _registry.RegisterWidgetFactoryMapping<InitialPopupParameters>(PopupFactory);
            _registry.RegisterWidgetFactoryMapping<InitialDropdownSeparatorParameters>(DropdownSeparatorFactory);
            _registry.RegisterWidgetFactoryMapping<InitialWidgetSearchParameters>(WidgetSearchFactory);
            _registry.RegisterWidgetFactoryMapping<InitialSearchableItemParameters>(SearchableItemContainerFactory);
            _registry.RegisterWidgetFactoryMapping<InitialLauncherBarSeparatorParameters>(SeparatorFactoryMethod);

            if (ChromeManagerBase.DefaultWidgetFactoriesGlobal.HasValue)
            {
                _registry.RegisterWidgetDehydrateCallbackMapping<InitialShowWindowButtonParameters>(
                    ShowWindowButtonDehydrateCallbackMethod);
                _registry.RegisterWidgetDehydrateCallbackMapping<InitialButtonParameters>(
                    ButtonDehydrateCallbackMethod);
                _registry.RegisterWidgetDehydrateCallbackMapping<InitialCommandButtonParameters>(
                    CommandButtonDehydrateCallbackMethod);
            }
        }

        #region ICommonFactories Members

        public InitializeWidgetHandler ButtonFactory
        {
            get { return ButtonFactoryMethod; }
        }

        public InitializeWidgetHandler CommandButtonFactory
        {
            get { return CommandButtonFactoryMethod; }
        }

        public InitializeWidgetHandler SplitButtonFactory
        {
            get { return SplitButtonFactoryMethod; }
        }

        public InitializeWidgetHandler ShowWindowButtonFactory
        {
            get { return ShowWindowButtonFactoryMethod; }
        }

        public InitializeWidgetHandler ShowExistingElementFactory
        {
            get { return ShowExistingElementFactoryMethod; }
        }

        public InitializeWidgetHandler DropdownButtonFactory
        {
            get { return DropdownButtonFactoryMethod; }
        }

        public InitializeWidgetHandler ComboBoxFactory
        {
            get { return ComboBoxFactoryMethod; }
        }

        public InitializeWidgetHandler PopupFactory
        {
            get { return PopupFactoryMethod; }
        }

        public InitializeWidgetHandler DropdownSeparatorFactory
        {
            get { return DropdownSeparatorMethod;  }
        }

        public IChromeManager ChromeManager
        {
            get { return _chromeManager; }
        }

        #endregion

        //[ViewModel(typeof(GenericWidgetViewModel))]
        //[FactoryOptions(typeof(ChromeManager20.Ribbon.RibbonButtonArea))]
        [FactoryOptions(typeof (ButtonViewContainer))]
        private bool ButtonFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialButtonParameters;
            if (parameters == null)
                return false;
            object button;

            if (emptyWidget.Content == null)
            {
                if (emptyWidget.Parent is LauncherBarGroupArea || emptyWidget.Parent is LauncherBarRootArea)
                {
                    button = new LauncherBarButton(new MSGuiButton()
                    {
                        OnClick = parameters.Click,
                        Text = parameters.Text,
                        ToolTip = parameters.ToolTip,
                        Image = parameters.Image,
                        IsEnabled = parameters.Enabled,
                        ButtonSize = ButtonSize.Small
                    }); 
                }
                else if (emptyWidget.Parent is MenuRootArea)
                {
                    button = new MenuItem()
                        {
                            Header = parameters.Text,
                            ToolTip = parameters.ToolTip,
                            Icon = parameters.Image == null ? null : new Image() { Source = parameters.Image },
                            IsEnabled = parameters.Enabled,
                            Height = 32 ,
                            IsCheckable = parameters.IsCheckable,
                            IsChecked = parameters.IsChecked,
                            InputGestureText = parameters.Gesture == null ? string.Empty : parameters.Gesture.GetDisplayStringForCulture(Thread.CurrentThread.CurrentUICulture)
                        };
                    if (parameters.Click != null)
                    {
                        (button as MenuItem).Click += parameters.Click.ConvertToRouted();
                    }
                }
                else
                {
                    IWidgetViewContainer current = emptyWidget;
                    while (current.Parent != null)
                    {
                        current = current.Parent;
                    }
                    MenuItem item;
                    if (current is MenuRootArea)
                    {
                        item = new MenuItem(); 
                    }
                    else
                    {
                        item = new ToolMenuItem();
                    }
                    item.Header = parameters.Text;
                    item.ToolTip = parameters.ToolTip;
                    item.Icon = parameters.Image == null ? null : new Image() {Source = parameters.Image};
                    item.IsEnabled = parameters.Enabled;
                    item.IsCheckable = parameters.IsCheckable;
                    item.IsChecked = parameters.IsChecked;
                    item.InputGestureText = parameters.Gesture == null
                                                ? string.Empty
                                                : parameters.Gesture.GetDisplayStringForCulture(
                                                    Thread.CurrentThread.CurrentUICulture);
                     
                    if (parameters.Click != null)
                    {
                       item.Click += parameters.Click.ConvertToRouted();
                    }
                    button = item;

                }
                emptyWidget.Content = button;

            }
            else
            {
                button = emptyWidget.Content;
            }

            var buttonContainer = emptyWidget as ButtonViewContainer;
            if (buttonContainer != null)
            {
                if (!_commandCache.ContainsKey(emptyWidget.FactoryID))
                {
                    _commandCache[emptyWidget.FactoryID] = new DelegateCommand((p) =>
                        {
                            if (parameters.Click != null)
                                parameters.Click(button, EventArgs.Empty);
                        });
                }
                var command = _commandCache[emptyWidget.FactoryID];

                SetupKeyboardGesture(buttonContainer, command, parameters, state);
            }

            return true;
        }

        [FactoryOptions(typeof(WidgetViewContainer))]
        private bool ShowExistingElementFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialExistingElementWidgetParameters;
            emptyWidget.Content = parameters.Element;
            return true;
        }

        //[ViewModel(typeof(GenericWidgetViewModel))] 
        private bool CommandButtonFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialCommandButtonParameters;
            if (parameters == null)
                return false;
            if (emptyWidget.Content == null)
            {
                if (emptyWidget.Parent is LauncherBarGroupArea || emptyWidget.Parent is LauncherBarRootArea)
                {
                    var button = new LauncherBarButton(new MSGuiButton()
                    {
                        Text = parameters.Text,
                        ToolTip = parameters.ToolTip,
                        Image = parameters.Image,
                        ButtonSize = ButtonSize.Small,
                        Command = parameters.Command,
                        CommandBinding = parameters.CommandBinding,
                        CommandParameter = parameters.CommandParameter
                    });

                    emptyWidget.Content = button;
                }
                else if (emptyWidget.Parent is MenuRootArea)
                {
                    var button = new MenuItem()
                    {
                        Header = parameters.Text,
                        ToolTip = parameters.ToolTip,
                        Icon = parameters.Image == null ? null : new Image() { Source = parameters.Image },
                        IsEnabled = parameters.Enabled,
                        Command = parameters.Command,
                        Height = 32,
                        CommandParameter = parameters.CommandParameter
                    };
                    button.CommandBindings.Add(parameters.CommandBinding);
                    emptyWidget.Content = button;
                }
                else  
                {
                    var button = new ToolMenuItem()
                        {
                            Header = parameters.Text,
                            ToolTip = parameters.ToolTip,
                            Icon = parameters.Image == null ? null : new Image() { Source = parameters.Image },
                            IsEnabled = parameters.Enabled,
                            Command = parameters.Command,
                            CommandParameter = parameters.CommandParameter
                        };
                    button.CommandBindings.Add(parameters.CommandBinding);
                    emptyWidget.Content = button;
                } 
            }

            var buttonContainer = emptyWidget as ButtonViewContainer;
            if (buttonContainer != null)
            {
                var commands = new List<ICommand>();
                if (parameters.Gesture != null)
                {
                    if (parameters.CommandParameter != null)
                    {
                        m_logger.WarningWithFormat("CommandParameter is not bound to gesture for {0}", parameters.Text);
                    }

                    if (parameters.Command != null) commands.Add(parameters.Command);
                    if (parameters.CommandBinding != null && parameters.CommandBinding.Command != null)
                        commands.Add(parameters.CommandBinding.Command);
                }

                SetupKeyboardGesture(buttonContainer, commands, parameters, state);
            }

            return true;
        }


        [FactoryOptions(typeof(RibbonSplitDropdownArea))]
        private static bool SplitButtonFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialSplitButtonParameters;
            if (parameters != null)
            {
                var splitButton = new SplitButton { Usage = SplitButtonUsage.LauncherBarButton};
                if (parameters.Click != null)
                {
                    splitButton.Click += parameters.Click.ConvertToRouted();
                }

                splitButton.IsEnabled = parameters.Enabled;
                splitButton.ToolTip = parameters.ToolTip;
                splitButton.Caption = parameters.Text;
                splitButton.Image = parameters.Image;
                splitButton.IsDropDownEnabled = parameters.IsDropDownEnabled; 

                //splitButton.SetBinding(RibbonGroup.MinimumSizeProperty, new Binding()
                //{
                //    Path = new PropertyPath(LauncherBarChromeManager.LauncherBarStateProperty),
                //    Mode = BindingMode.OneWay,
                //    Source = splitButton,
                //    Converter = new BarStateToRibbonGroupSizeConverter()
                //});
                if (parameters.Opening != null)
                {
                    splitButton.Opening += parameters.Opening;
                }
                if (parameters.Opened != null)
                {
                    splitButton.Opened += parameters.Opened;
                } 
                emptyWidget.Content = splitButton;
                return true;
            }

            return false;
        }

        //[ViewModel(typeof(GenericWidgetViewModel))]
        [FactoryOptions(typeof (ButtonViewContainer))]
        private bool ShowWindowButtonFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialShowWindowButtonParameters;
            if (parameters != null)
            {
                if (emptyWidget.Content == null)
                {
                    if (emptyWidget.Parent is LauncherBarGroupArea || emptyWidget.Parent is LauncherBarRootArea)
                    {
                        var button = new LauncherBarButton(new MSGuiButton()
                        {
                            OnClick =
                                (a, b) => ChromeManager.CreateWindow(parameters.WindowFactoryID,
                                                                     parameters.InitialParameters),
                            Text = parameters.Text,
                            ToolTip = parameters.ToolTip,
                            Image = parameters.Image,
                            IsEnabled = parameters.Enabled,
                            ButtonSize = ButtonSize.Small
                        });
                        emptyWidget.Content = button;
                    }
                    else if (emptyWidget.Parent is MenuRootArea)
                    {
                        var button = new MenuItem()
                        {
                            Header = parameters.Text,
                            ToolTip = parameters.ToolTip,
                            Icon = parameters.Image == null ? null : new Image() { Source = parameters.Image },
                            IsEnabled = parameters.Enabled,
                            Height = 32
                        };
                        button.Click +=
                            new Action(
                                () =>
                                _chromeManager.CreateWindow(parameters.WindowFactoryID, parameters.InitialParameters))
                                .ConvertToRouted();
                        emptyWidget.Content = button;
                    }
                    else  
                    {
                        var button = new ToolMenuItem()
                            {
                                Header = parameters.Text,
                                ToolTip = parameters.ToolTip,
                                Icon = parameters.Image == null ? null : new Image() { Source = parameters.Image },
                                IsEnabled = parameters.Enabled,
                            };
                        button.Click += new Action(
                                () =>
                                _chromeManager.CreateWindow(parameters.WindowFactoryID, parameters.InitialParameters))
                                .ConvertToRouted();
                        emptyWidget.Content = button;
                    } 
                }
                var buttonContainer = emptyWidget as ButtonViewContainer;
                if (buttonContainer != null)
                {
                    if (!_commandCache.ContainsKey(emptyWidget.FactoryID))
                    {
                        _commandCache[emptyWidget.FactoryID] =
                            new DelegateCommand(
                                p_ =>
                                ChromeManager.CreateWindow(parameters.WindowFactoryID, parameters.InitialParameters));
                    }
                    var command = _commandCache[emptyWidget.FactoryID];

                    SetupKeyboardGesture(buttonContainer, command, parameters, state);
                }
                return true;
            }
            else
            {
                return false;
            }
        }
         

        [FactoryOptions(typeof (LauncherBarDropdownArea))]
        private bool DropdownButtonFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialDropdownButtonParameters;
            if (parameters == null)
                return false;
             
            if (emptyWidget.Parent is MenuRootArea)
            {
                var button = new MenuItem()
                {
                    Header = parameters.Text,
                    ToolTip = parameters.ToolTip,
                    Icon = parameters.Image == null ? null : new Image() { Source = parameters.Image },
                    IsEnabled = parameters.Enabled,
                    Height = 32
                }; 
                emptyWidget.Content = button;
            }
            else if (!(emptyWidget.Parent is IDropdownButtonViewContainer))
            {
                var barMenuTool = new BarMenuTool //I've created a MenuTool with modified foreground
                {
                    Caption = parameters.Text, 
                    IsEnabled = parameters.Enabled,
                    Image = parameters.Image ?? parameters.SmallImage,  
                };

                if (emptyWidget.Parent is HeaderItemsHolderWidgetContainer)
                {
                    barMenuTool.SetResourceReference(Control.ForegroundProperty, MSGuiColors.TitlesBrushKey);
                }
                else
                {
                    barMenuTool.SetResourceReference(Control.ForegroundProperty, MSGuiColors.TextFillBrushKey);
                }
                if (parameters.Opening != null)
                {
                    barMenuTool.Opening += parameters.Opening.ConvertToRouted();
                }
                if (parameters.Opened != null)
                {
                    barMenuTool.Opened += parameters.Opened.ConvertToRouted();
                }
                emptyWidget.Content = barMenuTool;
           
            }
            else
            {
                var menuItem = new ToolMenuItem() 
                {
                    Header = parameters.Text,
                    IsEnabled = parameters.Enabled,
                    Icon = parameters.Image == null  && parameters.SmallImage == null ? 
                    null : new Image() { Source = parameters.Image ?? parameters.SmallImage },
                };


                emptyWidget.Content = menuItem;
            }
           

            return true;
        }

        //[ViewModel(typeof(GenericWidgetViewModel))]
        private bool ComboBoxFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialComboBoxParameters;
            if (parameters == null)
                return false;

            var combo = new ComboBox()
                {
                    ItemsSource = parameters.ItemsSource,
                    IsEnabled = parameters.Enabled,
                    Text = parameters.Text,
                };

            if (parameters.DropDownOpened != null)
                combo.DropDownOpened += parameters.DropDownOpened.Convert();
            emptyWidget.Content = combo;
            return true;
        }

        //[ViewModel(typeof(GenericWidgetViewModel))]
        private bool PopupFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialPopupParameters;
            if (parameters == null)
                return false;
             
            var barMenuTool = new BarMenuTool //I've created a MenuTool with modified foreground
                {
                    Caption = parameters.Text, 
                    IsEnabled = parameters.Enabled,
                    Image = parameters.Image, 
                    ItemsSource = parameters.ItemsSource 
                };

            if (emptyWidget.Parent is HeaderItemsHolderWidgetContainer)
            {
                barMenuTool.SetResourceReference(Control.ForegroundProperty, MSGuiColors.TitlesBrushKey);
            }
            else
            {
                barMenuTool.SetResourceReference(Control.ForegroundProperty, MSGuiColors.TextFillBrushKey);
            }
            emptyWidget.Content = barMenuTool;
            return true;
        }
        
        [FactoryOptions(typeof(SearchableItemContainer))]
        private bool SearchableItemContainerFactory(IWidgetViewContainer container, XDocument state)
        {
            container.Content = ((SearchableItemContainer)container).Item;
            return true;
        }

        [FactoryOptions(typeof(WidgetSearchArea))]
        private bool WidgetSearchFactory(IWidgetViewContainer emptyviewcontainer, XDocument state)
        {
            var widgetSearch = new WidgetSearchControl();
            emptyviewcontainer.Content = widgetSearch;
            return true;
        }

        public bool DropdownSeparatorMethod(IWidgetViewContainer viewContainer_, XDocument state_)
        {
            IWidgetViewContainer current = viewContainer_;
            while (current.Parent != null)
            {
                current = current.Parent;
            }
            object separator;
            if (current is MenuRootArea)
            {
                separator = new Separator();
            }
            else
            {
                separator = new ToolMenuItem() { Header = new Separator() };
            } 
            viewContainer_.Content = separator;
            return true;
        }
         
        private bool SeparatorFactoryMethod(IWidgetViewContainer container, XDocument state)
        {
            var parameters = (InitialLauncherBarSeparatorParameters)container.Parameters;

            container.Content = new VerticalSeparator() { Margin = parameters.Margin };
            //container.Content = new VerticalSeparator();
            return true;
        }

        #region CM20

        [FactoryOptions(typeof (LauncherBarRootArea))]
        internal static bool LauncherBarFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            emptyWidget.Content = null; //hack to fire ContentReady
            return true;
        }

        [FactoryOptions(typeof (LauncherBarGroupArea))]
        internal static bool LauncherGroupFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
        {
            var parameters = emptyWidget.Parameters as InitialLauncherGroupParameters;

            if (parameters != null)
            {
                var groupBox = new GroupBox() {Header = parameters.Text};
                var itemsControl = new ItemsControl();

                var fef = new FrameworkElementFactory(typeof(WrapPanel));
                fef.SetValue(WrapPanel.OrientationProperty, Orientation.Vertical);  
                itemsControl.ItemsPanel = new ItemsPanelTemplate(fef);
                groupBox.Content = itemsControl;
                emptyWidget.Content = groupBox;

                return true;
            }

            return false;
        }

        #endregion


        #region Default dehydration callbacks

        private XDocument ShowWindowButtonDehydrateCallbackMethod(IWidgetViewContainer widgetViewContainer_)
        {
            var result = new XElement("ShowWindowButtonViewContainer");
            AddGestureElement(widgetViewContainer_, result);
            return new XDocument(result);
        }

        private XDocument ButtonDehydrateCallbackMethod(IWidgetViewContainer widgetViewContainer_)
        {
            var result = new XElement("ButtonViewContainer");
            AddGestureElement(widgetViewContainer_, result);
            return new XDocument(result);
        }

        private XDocument CommandButtonDehydrateCallbackMethod(IWidgetViewContainer widgetViewContainer_)
        {
            var result = new XElement("CommandButtonViewContainer");
            AddGestureElement(widgetViewContainer_, result);
            return new XDocument(result);
        }

        private static void AddGestureElement(IWidgetViewContainer widgetViewContainer_, XElement result)
        {
            if (((ButtonViewContainer) widgetViewContainer_).Gesture != null)
            {
                var gesture = new XElement("Gesture");
                gesture.SetAttributeValue("value",
                                          GestureConverter.ConvertToString(
                                              ((ButtonViewContainer) widgetViewContainer_).Gesture));
                result.Add(gesture);
            }
        }

        #endregion

        private void SetupKeyboardGesture(ButtonViewContainer buttonContainer_, ICommand command_,
                                          ISupportsGesture parameters_, XDocument state_)
        {
            SetupKeyboardGesture(buttonContainer_, new[] {command_}, parameters_, state_);
        }

        private void SetupKeyboardGesture(ButtonViewContainer buttonContainer_, IEnumerable<ICommand> commands_,
                                          ISupportsGesture parameters_, XDocument state_)
        {
            PropertyChangedEventHandler buttonContainerOnPropertyChanged = (sender_, args_) =>
                {
                    if (args_.PropertyName != "Gesture") return;
                    if (buttonContainer_.Gesture != null)
                    {
                        foreach (var command in commands_)
                        {
                            _shell.MainWindow.InputBindings.Add(new InputBinding(command, buttonContainer_.Gesture));
                        }
                    }
                    else if (_shell != null)
                    {
                        foreach (var command in commands_)
                        {
                            var command1 = command;
                            var bindingToRemove =
                                _shell.MainWindow.InputBindings.Cast<InputBinding>()
                                      .FirstOrDefault(b_ => b_.Command == command1);
                            if (bindingToRemove != null) _shell.MainWindow.InputBindings.Remove(bindingToRemove);
                        }
                    }
                };
            buttonContainer_.PropertyChanged -= buttonContainerOnPropertyChanged;
            buttonContainer_.PropertyChanged += buttonContainerOnPropertyChanged;
            buttonContainer_.Gesture = parameters_.Gesture;
            if (state_ != null)
            {
                var gestureEl = state_.Root.Descendants("Gesture").FirstOrDefault();
                if (gestureEl != null)
                {
                    buttonContainer_.Gesture =
                        (KeyGesture) GestureConverter.ConvertFromString(gestureEl.Attribute("value").Value);
                }
            }
        }
    }
}
