﻿using System;
using System.Linq;
using System.Windows;
using System.Collections.ObjectModel;
using MorganStanley.Desktop.GUI.Controls.TaskbarIcon;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.SystemTray
{
    internal class SystemTrayIconArea : WidgetViewContainer, ITrayIconViewContainer
    {
        private TaskbarIcon m_icon = null;
        private ObservableCollection<object> m_items = new ObservableCollection<object>();

        public SystemTrayIconArea(string ID, InitialWidgetParameters parameters)
            : base(ID, parameters)
        {
            this.ContentReady += (s, e) =>
            {
                this.m_icon = this.Content as TaskbarIcon;
                CreateContextMenu();
            };
        }
  
        protected override void DoInsertVisual(int index, IWidgetViewContainer child)
        {
            if (this.m_icon != null && child as SystemTrayMenuItemArea != null)
            {
                this.m_items.Insert(index, child.Content);
            }
            base.DoInsertVisual(index, child);
        }

        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            if (this.m_icon != null && child as SystemTrayMenuItemArea != null)
            {
                this.m_items.Add(child.Content);
                return base.DoAddVisual(child);
            }

            return false;
        }

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            if (child.Content != null && child as SystemTrayMenuItemArea != null)
            {
                this.m_items.Remove(child.Content);
            }

            base.DoRemoveVisual(child);
        }
        private void CreateContextMenu()
        {
            if (this.m_icon.ContextMenu == null)
            {
                this.m_icon.ContextMenu = new System.Windows.Controls.ContextMenu();
                this.m_icon.ContextMenu.ItemsSource = this.m_items;
            }
        }

        #region ITrayIconViewContainer Members

        public System.Drawing.Icon Icon
        {
            get
            {
                return this.m_icon.Icon;
            }
            set
            {
                this.m_icon.Icon = value;
            }
        }

        public Visibility Visibility
        {
            get
            {
                return this.m_icon.Visibility;
            }
            set
            {
                this.m_icon.Visibility = value;
            }
        }

        public string Text
        {
            get
            {
                return this.m_icon.ToolTipText;
            }
            set
            {
                this.m_icon.ToolTipText = value;
            }
        }
         
        #endregion
    }
}
