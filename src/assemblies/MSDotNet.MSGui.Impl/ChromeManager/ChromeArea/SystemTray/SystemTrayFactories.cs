﻿using System.Windows.Media;
using System.Xml.Linq;
using MorganStanley.Desktop.GUI.Controls.TaskbarIcon;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.SystemTray;
using System.Windows.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.SystemTray
{
	internal class SystemTrayFactories : ISystemTrayFactories
	{
		#region ISystemTrayHelper Members

		public Core.ChromeManager.InitializeWidgetHandler IconFactory
		{
			get { return this.IconFactoryMethod; }
		}

		public Core.ChromeManager.InitializeWidgetHandler MenuItemFactory
		{
			get { return this.MenuItemFactoryMethod; }
		}


        public Core.ChromeManager.InitializeWidgetHandler MenuSeparatorFactory
        {
            get { return this.MenuSeparatorFactoryMethod; }
        }
		#endregion
		
		public SystemTrayFactories(ChromeRegistry chromeRegistry_)
		{
			chromeRegistry_.RegisterWidgetFactoryMapping<InitialSystemTrayParameters>(this.SystemTrayRootFactoryMethod);

			chromeRegistry_.RegisterWidgetFactoryMapping<InitialTrayIconParameters>(this.IconFactory);
			chromeRegistry_.RegisterWidgetFactoryMapping<InitialTrayMenuItemParameters>(this.MenuItemFactory);
            chromeRegistry_.RegisterWidgetFactoryMapping<InitialTrayMenuSeparatorParameters>(this.MenuSeparatorFactory);
        }

		[FactoryOptions(typeof(SystemTrayRootArea))]
		private bool SystemTrayRootFactoryMethod(IWidgetViewContainer viewContainer_, XDocument state_)
		{
			return true;
		}

		[FactoryOptions(typeof(SystemTrayIconArea))]
		private bool IconFactoryMethod(IWidgetViewContainer viewContainer_, XDocument state_)
		{
			var icon = new TaskbarIcon();
			viewContainer_.Content = icon;

			var parameters = viewContainer_.Parameters as InitialTrayIconParameters;
			if (parameters != null)
			{
				icon.Icon = parameters.Icon;
				icon.Visibility = parameters.Visibility;
				icon.ToolTipText = parameters.Text;

				return true;
			}
			else
			{
				return false;
			}
		}
		
		[FactoryOptions(typeof(SystemTrayMenuItemArea))]
		private bool MenuItemFactoryMethod(IWidgetViewContainer viewContainer_, XDocument state_)
		{
			var menuItem = new MenuItem();
			viewContainer_.Content = menuItem;
			var parameters = viewContainer_.Parameters as InitialTrayMenuItemParameters;

			if (parameters != null)
			{
			    if (parameters.Image != null)
			    {
			        menuItem.Icon = new Image() {Source = parameters.Image, Width = 16, Height =  16, Stretch = Stretch.UniformToFill};
			    }
				if (parameters.Click != null)
				{
					menuItem.Click += parameters.Click;
				}
				menuItem.Header = parameters.Text;

				return true;
			}
			else
			{
				return false;
			}
		}

        [FactoryOptions(typeof(SystemTrayMenuItemArea))]
        private bool MenuSeparatorFactoryMethod(IWidgetViewContainer viewContainer_, XDocument state_)
        {
            var menuItem = new Separator();
            viewContainer_.Content = menuItem;
            return true;
        }
	}
}
