﻿using System;
using System.Collections.Generic;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.SystemTray
{
	internal class SystemTrayRootArea : WidgetViewContainer
	{
		//private ChromeManagerBase m_chromeManager = null;
		private Dictionary<string, SystemTrayIconArea> m_icons = new Dictionary<string, SystemTrayIconArea>();

		public SystemTrayRootArea(string ID, InitialWidgetParameters parameters)
			: base(ID, parameters)
		{
			//this.ContentReady += (s, e) =>
			//{
			//
			//};
		}
	}


	//internal class SystemTrayRootArea_ : ChromeAreaBase
	//{
	//    private ChromeManagerBase m_chromeManager = null;
	//    private Dictionary<string, SystemTrayIconArea> m_icons = new Dictionary<string, SystemTrayIconArea>();

	//    public SystemTrayRootArea_(ChromeManagerBase chromeManager_)
	//    {
	//        this.m_chromeManager = chromeManager_;
	//    }


	//    #region ChromeAreaBase implementation

	//    internal override IWidgetViewModel AddWidget(string location, string factoryID, Core.ChromeManager.InitialWidgetParameters initialParameters)
	//    {
	//        if (location == null || location == string.Empty)
	//        {
	//            var wvm = this.m_chromeManager.CreateWidget(factoryID, initialParameters);

	//            if (wvm != null && wvm.ViewContainer != null)
	//            {
	//                var icon = wvm as SystemTrayIconArea;
	//                if (icon != null)
	//                {
	//                    this.m_icons.Add(icon.Name, icon);
	//                    this.widgets.Add(icon.ViewContainer);
	//                    return wvm;
	//                }
	//                else
	//                {
	//                    return null;
	//                }
	//            }
	//            else
	//            {
	//                return null;
	//            }
	//        }
	//        else
	//        {
	//            var icon = this.GetArea(location);

	//            if (icon != null)
	//            {
	//                string head, tail;
	//                this.SplitLocation(location, out head, out tail);

	//                return icon.AddWidget(tail, factoryID, initialParameters);
	//            }
	//            else
	//            {
	//                return null;//TODO	
	//            }
	//        }
	//    }

	//    internal override void RemoveWidget(string location, Core.ChromeManager.IWidgetViewContainer widget)
	//    {
	//        throw new NotImplementedException();
	//    }

	//    internal override ChromeAreaBase GetArea(string location)
	//    {
	//        if (location == null && location == string.Empty)
	//        {
	//            return null;
	//        }
	//        else
	//        {
	//            var path = location.Contains('/') ? location.Split('/') : new string[] { location };

	//            if (this.m_icons.ContainsKey(path[0]))
	//            {
	//                return this.m_icons[path[0]];
	//            }
	//            else
	//            {
	//                return null;
	//            }
	//        }
	//    } 

	//    #endregion
	//}
}
