﻿using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.SystemTray
{
	public class TrayIconViewContainer : WidgetViewContainer, ITrayIconViewContainer
	{

		public TrayIconViewContainer(string factoryID)
			: base(factoryID)
		{
			this.Parameters = new InitialWidgetParameters();
			this.FactoryID = factoryID;
		}

		public TrayIconViewContainer(InitialWidgetParameters parameters, string factoryID)
			:base (parameters, factoryID)
		{
			this.Parameters = parameters;
			this.FactoryID = factoryID;
		}

	
		#region ITrayIconViewContainer Members

		private System.Drawing.Icon m_icon;
		public System.Drawing.Icon Icon
		{
			get
			{
				return this.m_icon;
			}
			set
			{
				this.m_icon = value;
				this.OnPropertyChanged("Icon");
			}
		}

		private System.Windows.Visibility m_visible;
		public System.Windows.Visibility Visibility
		{
			get
			{
				return this.m_visible;
			}
			set
			{
				this.m_visible = value;
				this.OnPropertyChanged("Visible");
			}
		}

		private string m_text;
		public string Text
		{
			get
			{
				return this.m_text;
			}
			set
			{
				this.m_text = value;
				this.OnPropertyChanged("Text");
			}
		}

		#endregion
	}
}
