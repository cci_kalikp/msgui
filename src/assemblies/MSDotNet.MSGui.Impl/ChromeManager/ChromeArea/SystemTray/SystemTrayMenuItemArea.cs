﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Collections.ObjectModel;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.SystemTray
{
	internal class SystemTrayMenuItemArea : WidgetViewContainer
	{
		private System.Windows.Controls.MenuItem m_menuItem;
		private ObservableCollection<object> m_items = new ObservableCollection<object>();
		
		public SystemTrayMenuItemArea(string factoryID, InitialWidgetParameters parameters)
			: base(factoryID, parameters)
		{
			this.ContentReady += (s, e) =>
			{ 
				this.m_menuItem = this.Content as System.Windows.Controls.MenuItem;
                if (m_menuItem != null)
                {
                    this.m_menuItem.ItemsSource = this.m_items; 
                } 
			};
		}
         
        protected override void DoInsertVisual(int index, IWidgetViewContainer child)
        {
            if (child as SystemTrayMenuItemArea != null)
            {
                this.m_items.Insert(index, child.Content); 
            }
            base.DoInsertVisual(index, child);
        }
		protected override bool DoAddVisual(IWidgetViewContainer child)
		{
		    if (!(child is SystemTrayMenuItemArea)) return false; 
            this.m_items.Add(child.Content);
			return base.DoAddVisual(child);
		}

		protected override void DoRemoveVisual(IWidgetViewContainer child)
		{
			if (this.m_items.Contains(child.Content))
			{
				this.m_items.Remove(child.Content);
			}

			base.DoRemoveVisual(child);
		}

	}
}
