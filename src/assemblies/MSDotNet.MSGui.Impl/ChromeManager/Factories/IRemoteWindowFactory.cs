﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories
{
    internal interface IRemoteWindowFactory
    {
        void CreateRemoteWindow(string factoryId, string viewId);
    }
}
