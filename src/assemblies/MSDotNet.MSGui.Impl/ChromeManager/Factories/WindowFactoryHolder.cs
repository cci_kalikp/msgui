﻿using System;
using System.Collections.Generic;
using System.Threading;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories
{
    internal sealed class WindowFactoryHolder : IWindowFactoryHolderInternal
    {
        public readonly SemaphoreSlim Semaphore = new SemaphoreSlim(1);
        private readonly List<InitializeWindowHandlerStep> chain;
        public DehydrateWindowHandler DehydrateHandler { get; private set; }
        public InitializeWindowHandler InitHandler { get; private set; }
        public string Name { get; internal set; }

        public Type ViewType { get; set; }

        public IEnumerable<InitializeWindowHandlerStep> Chain
        {
            get { return chain; }
        }
 
        public WindowFactoryHolder()
        {
            this.chain = new List<InitializeWindowHandlerStep>(); 
        }

        public IWindowFactoryHolder SetInitHandler(InitializeWindowHandler value)
        {
            if (this.InitHandler == null)
            {
                this.InitHandler = value;
                return this;
            }

            throw new InvalidOperationException("The InitHandler can be set only once.");
        }
        
        public IWindowFactoryHolder SetDehydrateHandler(DehydrateWindowHandler value)
        {
            if (this.DehydrateHandler == null)
            {
                this.DehydrateHandler = value;
                return this;
            }

            throw new InvalidOperationException("The DehydrateHandler can be set only once.");
        }

        public void AddStep(InitializeWindowHandlerStep step)
        {
            if (!this.chain.Contains(step))
            {
                this.chain.Add(step);
            }
        }

 
        public InitializeWindowHandler BuildUp()
        {
            //ReSharper disable ForCanBeConvertedToForeach
            var stack = new Stack<InitializeWindowHandlerStep>();

            InitializeWindowHandlerStep step = this.GetChainTerminator();
            if (step != null)
            {
                stack.Push(step);
            }  
            for (int i = 0; i < this.chain.Count; i++) //foreach does not guarantee in-order iteration!
            {
                stack.Push(this.chain[i]);
            }

            if (stack.Count == 0) return null;
            return (container, state) => stack.Pop()(container, state, stack);
            //ReSharper restore ForCanBeConvertedToForeach
        }

        private InitializeWindowHandlerStep GetChainTerminator()
        {
            if (this.InitHandler == null) return null;
            return (container, state, chain) => this.InitHandler(container, state);
        }

    }
}
