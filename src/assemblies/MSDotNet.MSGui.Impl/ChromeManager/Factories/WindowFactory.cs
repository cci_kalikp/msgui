﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories
{
    internal sealed class WindowFactory : IWindowFactory
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<WindowFactory>();

        private readonly IDictionary<string, Dictionary<string, string>> _singletonViews;
        private readonly IChromeRegistry _registry;
        private readonly IWindowManager _windowManager;

        public WindowFactory(IChromeRegistry registry) : this(registry, null)
        {
        }

        internal WindowFactory(IChromeRegistry registry, IWindowManager windowManager)
        {
            _windowManager = windowManager;
            _registry = registry;
            _singletonViews = new Dictionary<string, Dictionary<string, string>>();
        }

        public IWindowViewContainer CreateWindowInstance(string windowId, InitialWindowParameters parameters, string viewId,
                                                         bool throwExceptionIfNoFactory, XDocument savedState = null, XDocument viewState = null)
        {
            return InternalCreateWindow(windowId, parameters, viewId, throwExceptionIfNoFactory, savedState, viewState);
        }

        public void ReloadSingletonState(XElement singletonElement)
        {
            _singletonViews.Clear();
            if (singletonElement != null)
            {
                foreach (var singletonKvPair in singletonElement.Elements("Singleton"))
                {
                    var factoryId = singletonKvPair.Attribute("CreatorID").Value;
                    var singletonKey = singletonKvPair.Attribute("SingletonKey") == null
                                           ? string.Empty
                                           : singletonKvPair.Attribute("SingletonKey").Value;
                    var viewId = singletonKvPair.Attribute("ViewId").Value;

                    Dictionary<string, string> factoryDictionary;
                    if (!_singletonViews.TryGetValue(factoryId, out factoryDictionary))
                    {
                        factoryDictionary = new Dictionary<string, string>();
                        _singletonViews[factoryId] = factoryDictionary;
                    }

                    factoryDictionary.Add(singletonKey, viewId);
                }
            }
        }

        public bool TryGetSingletonViewInstanceId(string factoryId, string singletonKey,
                                                  out string singletonViewInstanceId)
        {
            singletonViewInstanceId = null;
            Dictionary<string, string> singletonViewInstances;

            return _singletonViews.TryGetValue(factoryId, out singletonViewInstances) &&
                   singletonViewInstances.TryGetValue(singletonKey, out singletonViewInstanceId);
        }

        public void SaveSingletonState(XElement singletionElement)
        {
            foreach (var factoryId in _singletonViews.Keys)
            {
                foreach (var singletonKey in _singletonViews[factoryId].Keys)
                {
                    singletionElement.Add(new XElement("Singleton",
                                                       new XAttribute("CreatorID", factoryId),
                                                       new XAttribute("SingletonKey", singletonKey),
                                                       new XAttribute("ViewId", _singletonViews[factoryId][singletonKey])));
                }
            }
        }

        private IWindowViewContainer InternalCreateWindow(string factoryId, InitialWindowParameters initialParameters,
                                                          string viewId, bool throwExceptionIfNoFactory,  XDocument savedState, XDocument viewState)
        {
            IWindowViewContainer viewContainer = null;
            if (initialParameters.IsModal)
            {
                if (_windowManager != null)
                {
                    var dialog = (DialogWindow)_windowManager.CreateDialog();
                    dialog.FactoryID = factoryId;
                    dialog.Parameters = initialParameters;
                    viewContainer = dialog;
                }
                else throw new InvalidOperationException("Creation of modal windows isn't supported without a valid window manager instance.");
            } 

            if (viewId != null)
            {
                if (_singletonViews.ContainsKey(factoryId) && _singletonViews[factoryId].ContainsValue(viewId))
                {
                    initialParameters.SingletonKey =
                        _singletonViews[factoryId].Keys.FirstOrDefault(
                            k => _singletonViews[factoryId][k] == viewId);
                }
            }

            WindowFactoryHolder factory = null;
            Type containerType = null;
            var elementInitializerRegistry = _registry as IChromeElementInitializerRegistry;
            if (elementInitializerRegistry != null &&
                elementInitializerRegistry.TryGetWindowFactoryHolder(initialParameters.GetType(), out factory))
            {
                containerType = factory.ViewType;
            }
            if (viewContainer == null)
            { 
                var view = containerType == null
                                  ? new WindowViewContainer(initialParameters, factoryId)
                                  : Activator.CreateInstance(containerType, initialParameters, factoryId);

                viewContainer = view as IWindowViewContainer;
                if (viewId != null)
                {
                    ((WindowViewContainer)viewContainer).ID = viewId;
                }
            }
            
            if (initialParameters.Persistable)
            {
                XDocument doc = null;
                if (viewState != null && viewState.Root != null && viewState.Root.FirstNode != null)
                {
                    doc  = new XDocument(viewState.Root.Descendants().FirstOrDefault());
                }

                viewContainer.GetPersistor().LoadState(doc);
            }

            //call the type mapping factory first
            if (factory != null)
            {
                try
                {
                    var initHandler = factory.BuildUp();
                    if (initHandler == null) return null;
                    if (!initHandler(viewContainer, savedState)) 
                        return null;
                }
                catch (Exception exception)
                {
                    Logger.Error(
                        string.Format("Init handler failed for window factory with parameter type {0}", initialParameters.GetType()), exception);
                }

            }

            //if no type mapping factory and no factory id specific factory, throw exception
            if (factory == null && !((IChromeElementInitializerRegistry)_registry).TryGetWindowFactoryHolder(factoryId, out factory))
            {
               if (throwExceptionIfNoFactory)
               {
                   Logger.Error(String.Format("There is no window factory with the name \"{0}\"", factoryId));
                   throw new Exception(String.Format("There is no window factory with the name \"{0}\"", factoryId));

               }
               return null;

                 
            }

             
            //call the factory id specific factory if there is any
            if (((IChromeElementInitializerRegistry) _registry).TryGetWindowFactoryHolder(factoryId, out factory))
            {
                try
                {
                    var initHandler = factory.BuildUp();
                    if (initHandler == null)
                    {
                        return null;
                    }
                    if (!initHandler(viewContainer, savedState))
                    {
                        return null;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(
                        string.Format("Init handler failed for window factory with name {0}", factoryId), exception);
                }

                var singletonKey = initialParameters.SingletonKey;


                // if this is being created as singleton at runtime
                // or at layout load time
                Dictionary<string, string> singletonView = null;
                if (initialParameters.Singleton || (_singletonViews.TryGetValue(factoryId, out singletonView) && singletonView.ContainsValue(viewId)))
                {
                    if (singletonView == null)
                    //if this is not a singleton we loaded from layout, record it
                    {
                        if (!_singletonViews.ContainsKey(factoryId))
                        {
                            var dict = new Dictionary<string, string> { { singletonKey, viewContainer.ID } };
                            _singletonViews.Add(factoryId, dict);
                        }
                        else
                        {
                            _singletonViews[factoryId][singletonKey] = viewContainer.ID;
                        }
                    }
                    else
                    {
                        singletonView[singletonKey] = viewContainer.ID;
                    }

                    EventHandler<WindowEventArgs> singletonClosedHandler = null;
                    singletonClosedHandler = (s, e) =>
                    {
                        Dictionary<string, string> innerSingletonView;
                        if (_singletonViews.TryGetValue(factoryId, out innerSingletonView) &&
                            innerSingletonView.ContainsKey(singletonKey))
                        {
                            innerSingletonView.Remove(singletonKey);
                        }

                        viewContainer.Closed -= singletonClosedHandler;
                        viewContainer.ClosedQuietly -= singletonClosedHandler;
                    };

                    viewContainer.Closed += singletonClosedHandler;
                    viewContainer.ClosedQuietly += singletonClosedHandler;
                } 

            }

            return viewContainer;
        }
       
        
    }
}
