﻿using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories
{
    internal interface IWindowFactory
    {
        IWindowViewContainer CreateWindowInstance(string windowId, InitialWindowParameters parameters, string viewId,
                                                  bool throwExceptionIfNoFactory,
                                                  XDocument savedState = null, XDocument viewState = null);
    }
}
