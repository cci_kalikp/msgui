﻿using System;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories
{
    internal sealed class WidgetFactory : IWidgetFactory
    {
        private readonly IChromeRegistry _registry;

        public WidgetFactory(IChromeRegistry registry)
        {
            _registry = registry;
        }

        public IWidgetViewContainer CreateWidgetInstance(string widgetId, InitialWidgetParameters parameters)
        {
            var t = ResolveFactoryOptions(widgetId, parameters);
            var vc = Activator.CreateInstance(t, widgetId, parameters) as IWidgetViewContainer;
            return vc;
        }

        private Type ResolveFactoryOptions(string factoryId, InitialWidgetParameters initialParameters)
        {
            if (initialParameters != null)
            {
                var iwpType = initialParameters.GetType();

                WidgetFactoryHolder widgetHolder;
                var elementInitializerRegistry = (IChromeElementInitializerRegistry) _registry;

                if (elementInitializerRegistry.TryGetWidgetFactoryHolder(iwpType, out widgetHolder))
                {
                    return widgetHolder.ViewContainerType;
                }
            }

            var chromeRegistry = _registry as ChromeRegistry;
            if (chromeRegistry != null)
            {
                WidgetFactoryHolder factoryHolder;
                if (chromeRegistry.WidgetFactories.TryGetValue(factoryId, out factoryHolder))
                {
                    return factoryHolder.ViewContainerType ?? typeof (WidgetViewContainer);
                }
            }

            return typeof (WidgetViewContainer);
        }
    }
}
