﻿using System;
using System.Collections.Generic;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories
{
    internal sealed class WidgetFactoryHolder : IWidgetFactoryHolder
    {
        public InitializeWidgetHandler InitHandler { get; private set; }
        public DehydrateWidgetHandler DehydrateHandler { get; private set; }
        public DehydrateWidgetHandler GlobalDehydrateHandler { get; set; }
        private Type viewContainerType;
        public Type ViewContainerType
        {
            get 
            { 
                if (viewContainerType != null) return viewContainerType;
                if (InitHandler != null)
                {
                    Delegate[] initHandlers = InitHandler.GetInvocationList();
                    if (initHandlers.Length == 1)
                    {

                        var attr =
                            Attribute.GetCustomAttribute(initHandlers[0].Method, typeof(ViewContainerAttribute), true) as
                            ViewContainerAttribute;
                        if (attr != null)
                        {
                            viewContainerType = attr.ViewContainerType;
                        }

                        if (viewContainerType == null)
                        {
                            var attr2 = Attribute.GetCustomAttribute(initHandlers[0].Method, typeof(FactoryOptionsAttribute), true) as FactoryOptionsAttribute;
                            if (attr2 != null)
                            {
                                viewContainerType = attr2.ViewContainerType;
                            }
                        } 
                    }
                }
                if (viewContainerType == null) viewContainerType = typeof (WidgetViewContainer);
                return viewContainerType;
            }
            set { viewContainerType = value; }
        }
        public string Name { get; internal set; }

        private readonly List<InitializeWidgetHandlerStep> chain;
        public IEnumerable<InitializeWidgetHandlerStep> Chain
        {
            get { return chain; }
        } 

        public WidgetFactoryHolder()
        {
            this.chain = new List<InitializeWidgetHandlerStep>(); 
        }
        public WidgetFactoryHolder(InitializeWidgetHandler initHandler, DehydrateWidgetHandler dehydrateHandler)
        {
            this.chain = new List<InitializeWidgetHandlerStep>(); 
            this.DehydrateHandler = dehydrateHandler;
            this.InitHandler = initHandler;
        }

        public IWidgetFactoryHolder SetInitHandler(InitializeWidgetHandler value)
        {
            if (this.InitHandler == null)
            {
                this.InitHandler = value;
                return this;
            }

            throw new InvalidOperationException("The InitHandler can be set only once.");
        }

        public IWidgetFactoryHolder SetDehydrateHandler(DehydrateWidgetHandler value, bool global=false)
        {
            if (!global)
            {
                if (this.DehydrateHandler == null)
                {
                    this.DehydrateHandler = value;
                    return this;
                }
            }
            else if (this.GlobalDehydrateHandler == null)
            {
                GlobalDehydrateHandler = value;
                return this;
            }


            throw new InvalidOperationException("The DehydrateHandler can be set only once.");
        }

        public void AddStep(InitializeWidgetHandlerStep step)
        {
            if (!this.chain.Contains(step))
            {
                this.chain.Add(step);
            }
        }

 

        public InitializeWidgetHandler BuildUp()
        {
            //ReSharper disable ForCanBeConvertedToForeach
            var stack = new Stack<InitializeWidgetHandlerStep>();
            InitializeWidgetHandlerStep step = this.GetChainTerminator();
            if (step != null)
            {
                stack.Push(step); 
            }
            for (int i = 0; i < this.chain.Count; i++) //foreach does not guarantee in-order iteration!
            {
                stack.Push(this.chain[i]);
            }

            if (stack.Count == 0) return null;
            return (container, state) => stack.Pop()(container, state, stack);
            //ReSharper restore ForCanBeConvertedToForeach
        }

        private InitializeWidgetHandlerStep GetChainTerminator()
        {
            if (InitHandler == null) return null;
            return (container, state, chain) => this.InitHandler(container, state);
        }
    }
}
