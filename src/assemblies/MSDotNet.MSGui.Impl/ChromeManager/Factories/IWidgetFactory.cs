﻿using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories
{
    internal interface IWidgetFactory
    {
        IWidgetViewContainer CreateWidgetInstance(string widgetId, InitialWidgetParameters parameters);
    }
}
