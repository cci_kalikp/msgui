﻿using System; 
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq; 
using System.Xml;
using System.Xml.Linq; 
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.IED.Concord.Configuration; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 
using MorganStanley.MSDotNet.MSGui.Impl.Concord; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
    public partial class ChromeManagerBase:ISubLayoutManager
    {
        internal const string SubLayoutPrefix = "MSDesktopWorkspace_";
        readonly List<IWindowViewContainer> savedWorkspaces = new List<IWindowViewContainer>(); 
        readonly List<IWindowViewContainer> deletedWorkspaces = new List<IWindowViewContainer>();
        readonly List<IWindowViewContainer> unsavedWorkspaces = new List<IWindowViewContainer>(); 
        readonly ObservableCollection<IWindowViewContainer> allOpenedWorkspaces = new ObservableCollection<IWindowViewContainer>(); 
        public ObservableWindowCollection OpenedWorkspaces
        {
            get
            {
                return new ObservableWindowCollection(allOpenedWorkspaces);
            }
        }


        void allOpenedWorkspaces_CollectionChanged(object sender_, NotifyCollectionChangedEventArgs e_)
        {
            if (e_.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (IWindowViewContainer workspace in e_.OldItems)
                {
                    workspace.Closed -= workspace_Closed;
                    workspace.ClosedQuietly -= workspace_ClosedQuietly;
                    savedWorkspaces.Remove(workspace);
                    unsavedWorkspaces.Remove(workspace);
                }
            }
            else if (e_.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (IWindowViewContainer workspace in e_.NewItems)
                { 
                    workspace.Closed += workspace_Closed;
                    workspace.ClosedQuietly += workspace_ClosedQuietly; 
                }
            }
        }

        void workspace_Closed(object sender_, Core.WindowEventArgs e_)
        {
            var oldView = (IWindowViewContainer)sender_;
            allOpenedWorkspaces.Remove(oldView);
             
        }

        void workspace_ClosedQuietly(object sender_, Core.WindowEventArgs e_)
        {
            var oldView = (IWindowViewContainer)sender_;
            allOpenedWorkspaces.Remove(oldView);
        }
           
        public void WriteSubLayoutConfig(string configName_, string value_)
        {
            var workspacesConfig = (Configurator)ConfigurationManager.GetConfig("Workspaces");
            var configRoot = workspacesConfig.GetNode("/Configs", null);
            if (configRoot != null)
            {
                var configNodes = configRoot.SelectNodes("//Item[@Name='" + configName_ + "']");
                if (configNodes != null)
                {
                    foreach (XmlNode existingNode in configNodes)
                    {
                        if (existingNode != null)
                        {
                            configRoot.RemoveChild(existingNode);
                        }
                    }
                    workspacesConfig.SetNode("/Configs", null, new XmlDocument() { InnerXml = configRoot.OuterXml }.DocumentElement);
                }

            }

            var configDoc = new XmlDocument()
            {
                InnerXml = "<Item Name='" + configName_ +
                     "' Value='" + value_ + "'/>"
            };
            workspacesConfig.AddNode("/Configs", null, configDoc.DocumentElement);
            workspacesConfig.Save();

        }

        public string ReadSubLayoutConfig(string configName_)
        {
            var workspacesConfig = (Configurator)ConfigurationManager.GetConfig("Workspaces");
            var configRoot = workspacesConfig.GetNode("/Configs", null);
            if (configRoot == null) return String.Empty;
            var configNodes = configRoot.SelectNodes("//Item[@Name='" + configName_ + "']");
            if (configNodes == null) return string.Empty;
            foreach (XmlNode existingNode in configNodes)
            {
                if (existingNode != null)
                {
                    return XmlHelper.ReadAttribute(existingNode, "Value", string.Empty);
                }
            }
            return String.Empty;
        }

        public void RenameSubLayouts(IDictionary<string, string> namingChanges_)
        {
            //update the sublayout names
            var workspacesConfig = (Configurator)ConfigurationManager.GetConfig("Workspaces");
            var nameRoot = GetWorkspaces(workspacesConfig);
            Dictionary<string, SubLayoutDefinition> oldDeinitions = new Dictionary<string, SubLayoutDefinition>();
            if (nameRoot != null)
            {
                foreach (var namingChange in namingChanges_)
                {
                    var workspaceNodes = nameRoot.SelectNodes("//Workspace[@Name='" + namingChange.Key + "']");
                    if (workspaceNodes == null) continue;
                    foreach (XmlNode existingNode in workspaceNodes)
                    {
                        if (existingNode != null)
                        {
                            oldDeinitions.Add(namingChange.Key, ConvertToDefinition(existingNode));
                            XmlHelper.WriteAttribute(existingNode, "Name", namingChange.Value);
                            break;
                        }
                    }
                } 
            }
             
            //cached the new sublayouts
            Dictionary<string, XmlNode> newXmls = new Dictionary<string, XmlNode>();
            foreach (var namingChange in namingChanges_)
            {
                var oldConfig =
(Configurator)ConfigurationManager.GetConfig(SubLayoutPrefix + XmlConvert.EncodeLocalName(namingChange.Key));
                XmlNode oldXml = oldConfig.GetNode(".", null);
                SubLayoutDefinition oldLayoutDefinition;
                if (!oldDeinitions.TryGetValue(namingChange.Key, out oldLayoutDefinition)) continue;
                //var viewNodes = oldXml.SelectNodes("//IView[@Id='" + oldLayoutDefinition.RootViewId + "']");
                //if (viewNodes != null)
                //{
                //    foreach (XmlNode viewNode in viewNodes)
                //    {
                //        viewNode.Attributes["Title"].Value = namingChange.Value;
                //    }
                //}

                var tileViewNodes = oldXml.SelectNodes("//TileView[@ViewId='" + oldLayoutDefinition.RootViewId + "']");
                if (tileViewNodes != null)
                {
                    foreach (XmlNode tileViewNode in tileViewNodes)
                    { 
                        XmlHelper.WriteAttribute(tileViewNode, "Title", namingChange.Value); 
                    }
                }

                newXmls.Add(namingChange.Value, oldXml);
            }

            //save the sublayout names
            workspacesConfig.SetNode("/Workspaces", null, nameRoot);
            workspacesConfig.Save();
            foreach (string oldSubLayout in oldDeinitions.Keys)
            {
                ClearCachedLayout(oldSubLayout);
            }

            //save the sublayout definitions
            foreach (var newXml in newXmls)
            {
                var config =
   (Configurator)ConfigurationManager.GetConfig(SubLayoutPrefix + XmlConvert.EncodeLocalName(newXml.Key));
                config.SetNode(".", null, new XmlDocument(){InnerXml = newXml.Value.OuterXml}.DocumentElement);
                config.Save();
                ClearCachedLayout(newXml.Key); 
            }


            //trigger sublayout changed event and update the title of affected views
            foreach (var namingChange in namingChanges_)
            {
                SubLayoutDefinition oldLayoutDefinition;
                if (!oldDeinitions.TryGetValue(namingChange.Key, out oldLayoutDefinition)) continue;
                foreach (var windowViewContainer in savedWorkspaces)
                {
                    //if (windowViewContainer.ID == oldLayoutDefinition.RootViewId)
                    //{
                    //    windowViewContainer.Title = namingChange.Value;
                    //}
                    DockHelper.SetRootTitle(windowViewContainer, namingChange.Value);
 
                }
                OnSubLayoutChanged(oldLayoutDefinition, new SubLayoutDefinition(){Category=oldLayoutDefinition.Category, LayoutName =namingChange.Value, RootViewId = oldLayoutDefinition.RootViewId});
            } 
        }

        public void RenameSubLayout(string oldSubLayoutName_, string newSubLayoutName_)
        {
            foreach (var windowViewContainer in savedWorkspaces)
            {
                if (string.Compare(DockHelper.GetRootTitle(windowViewContainer), oldSubLayoutName_, StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    //windowViewContainer.Title = newSubLayoutName_;
                    DockHelper.SetRootTitle(windowViewContainer, newSubLayoutName_); 
                }
            }

            var workspacesConfig = (Configurator)ConfigurationManager.GetConfig("Workspaces");

            var nameRoot = GetWorkspaces(workspacesConfig);

            SubLayoutDefinition oldLayout = GetSubLayout(oldSubLayoutName_);
            SubLayoutDefinition newLayout = new SubLayoutDefinition()
            {
                Category = oldLayout.Category,
                LayoutName = newSubLayoutName_,
                RootViewId = oldLayout.RootViewId
            };

            var oldConfig =
    (Configurator)ConfigurationManager.GetConfig(SubLayoutPrefix + XmlConvert.EncodeLocalName(oldSubLayoutName_));

            if (nameRoot != null)
            {
                var workspaceNodes = nameRoot.SelectNodes("//Workspace[@Name='" + oldSubLayoutName_ + "']");
                if (workspaceNodes != null && workspaceNodes.Count > 0)
                {
                    if (workspaceNodes.Count > 1)
                    {
                        for (int i = 1; i < workspaceNodes.Count; i++)
                        {
                            nameRoot.RemoveChild(workspaceNodes[i]);
                        }
                    }

                    XmlHelper.WriteAttribute(workspaceNodes[0], "Name", newSubLayoutName_); 
                    workspacesConfig.SetNode("/Workspaces", null, new XmlDocument() { InnerXml = nameRoot.OuterXml }.DocumentElement);

                } 
            } 


            ClearCachedLayout(oldSubLayoutName_);

            nameRoot = GetWorkspaces(workspacesConfig);

            if (nameRoot == null || nameRoot.SelectNodes("//Workspace[@Name='" + newSubLayoutName_ + "']").Count == 0)
            {
                var nameDoc = new XmlDocument()
                {
                    InnerXml = "<Workspace Name='" + newSubLayoutName_ +
                        "' Category='" + newLayout.Category  +
                        "' RootViewId='" + newLayout.RootViewId +
                        "'/>"
                };


                workspacesConfig.AddNode("/Workspaces", null, nameDoc.DocumentElement);
            }

            var newConfig =
    (Configurator)ConfigurationManager.GetConfig(SubLayoutPrefix + XmlConvert.EncodeLocalName(newSubLayoutName_));

            XmlNode oldXml = oldConfig.GetNode(".", null);

            var viewNodes = oldXml.SelectNodes("//IView[@Id='" + newLayout.RootViewId + "']");
            if (viewNodes != null)
            {
                foreach (XmlNode viewNode in viewNodes)
                {
                    XmlHelper.WriteAttribute(viewNode, "Title", newSubLayoutName_); 
                }
            }

            var tileViewNodes = oldXml.SelectNodes("//TileView[@ViewId='" + newLayout.RootViewId + "']");
            if (tileViewNodes != null)
            {
                foreach (XmlNode tileViewNode in tileViewNodes)
                {
                    XmlHelper.WriteAttribute(tileViewNode, "Title", newSubLayoutName_); 
                }
            }


            newConfig.SetNode(".", null, new XmlDocument() { InnerXml = oldXml.OuterXml }.DocumentElement);
            newConfig.Save();
            workspacesConfig.Save();
            ClearCachedLayout(newSubLayoutName_); 
            OnSubLayoutChanged(oldLayout, newLayout);
        }

        public void CreateSubLayout(IWindowViewContainer rootView_)
        {
            if (!unsavedWorkspaces.Contains(rootView_))
            {
                unsavedWorkspaces.Add(rootView_);
                allOpenedWorkspaces.Add(rootView_);
            } 
        }

        public void UpdateSubLayout(IWindowViewContainer oldRootView_, IWindowViewContainer newRootView_)
        {
            if (unsavedWorkspaces.Contains(oldRootView_))
            {
                unsavedWorkspaces.Remove(oldRootView_);
                unsavedWorkspaces.Add(newRootView_);
                if (!allOpenedWorkspaces.Contains(newRootView_))
                { 
                    allOpenedWorkspaces.Add(newRootView_);
                }
                allOpenedWorkspaces.Remove(oldRootView_);
            } 
            else if (savedWorkspaces.Contains(oldRootView_))
            {
                savedWorkspaces.Remove(oldRootView_);
                savedWorkspaces.Add(newRootView_);
                if (!allOpenedWorkspaces.Contains(newRootView_))
                {
                    allOpenedWorkspaces.Add(newRootView_);
                }
                allOpenedWorkspaces.Remove(oldRootView_);
            } 

        }
        
        public void SaveSubLayout(IWindowViewContainer rootView_, bool skipLocation_, string subLayoutName_, bool saveAs_)
        {
            if (rootView_ == null) return;
            if (rootView_.Content == null) return;
            var viewsContainer = rootView_ as IViewCollectionContainer;
            if (viewsContainer != null && viewsContainer.Children.Count == 0) return; 

            //Load from CC
            var workspacesConfig = (Configurator)ConfigurationManager.GetConfig("Workspaces");

            var nameRoot = GetWorkspaces(workspacesConfig);
            List<IWindowViewContainer> containers = DockHelper.GetViewsInvolved(rootView_);
            if (saveAs_)
            {
                foreach (WindowViewContainer container in containers)
                {
                    container.ID = WindowViewContainer.GenerateNewID(); 
                    IViewCollectionContainer viewCollectionContainer = container as IViewCollectionContainer;
                    if (viewCollectionContainer != null)
                    {
                        foreach (WindowViewContainer childItem in viewCollectionContainer.Children)
                        {
                            childItem.ID = WindowViewContainer.GenerateNewID();
                        }
                    }
                } 
            }
            DockHelper.SetRootTitle(rootView_, subLayoutName_);
            SubLayoutDefinition oldLayout = GetSubLayout(subLayoutName_);
            SubLayoutDefinition newLayout = new SubLayoutDefinition()
            {
                Category = rootView_.Parameters.WorkspaceCategory,
                LayoutName = subLayoutName_,
                RootViewId = rootView_.ID
            };

            var workspaceNodes = nameRoot == null ? null : nameRoot.SelectNodes("//Workspace[@Name='" + subLayoutName_ + "']");

            if (workspaceNodes != null && saveAs_ && workspaceNodes.Count > 0)
            {  
                if (workspaceNodes.Count > 1)
                { 
                    for (int i = 1; i < workspaceNodes.Count; i++)
                    {
                        nameRoot.RemoveChild(workspaceNodes[i]);
                    }
                }

                XmlHelper.WriteAttribute(workspaceNodes[0], "Category", rootView_.Parameters.WorkspaceCategory ?? String.Empty);
                XmlHelper.WriteAttribute(workspaceNodes[0], "RootViewId", rootView_.ID ?? String.Empty); 
                workspacesConfig.SetNode("/Workspaces", null, new XmlDocument() { InnerXml = nameRoot.OuterXml }.DocumentElement);

            }

            if (workspaceNodes == null || workspaceNodes.Count == 0)
            {
                var nameDoc = new XmlDocument() { InnerXml = "<Workspace Name='" + subLayoutName_ +
                      "' Category='" + (rootView_.Parameters.WorkspaceCategory ?? String.Empty) + 
                     "' RootViewId='" + (rootView_.ID ?? String.Empty) +
                     "'/>" };


                workspacesConfig.AddNode("/Workspaces", null, nameDoc.DocumentElement);
            }

            var xn = WriteSubLayout(containers, true, skipLocation_);
            if (xn == null) return;

            // save to CC
            var config =
                (Configurator)ConfigurationManager.GetConfig(SubLayoutPrefix + XmlConvert.EncodeLocalName(subLayoutName_));
            config.SetNode(".", null, xn.DocumentElement);
            config.Save();

            // CC hack to clear the cache
            ClearCachedLayout(subLayoutName_);
            workspacesConfig.Save();
            AddToSavedWorkspaces(rootView_);
            if (saveAs_ || oldLayout == null)
            {
                OnSubLayoutChanged(oldLayout, newLayout);
            } 

        }

        public bool IsChanged(IWindowViewContainer rootView_, bool skipLocation_)
        {
            if (!IsSaved(rootView_)) return true;
            var config =
             (Configurator)
             ConfigurationManager.GetConfig(SubLayoutPrefix + XmlConvert.EncodeLocalName(DockHelper.GetRootTitle(rootView_)));
            if (config == null) return true;
            var oldLayout = config.GetNode("SubLayout", null);
            if (oldLayout == null) return true;
            var oldLayoutElement = GetCoreElement(oldLayout);
            List<IWindowViewContainer> containers = DockHelper.GetViewsInvolved(rootView_);
            var newLayoutDoc = WriteSubLayout(containers, false, skipLocation_);
            if (newLayoutDoc == null) return false; 
            var newLayoutElement = GetCoreElement(newLayoutDoc.SelectSingleNode("SubLayout", null));

            return String.CompareOrdinal(oldLayoutElement.ToString(), newLayoutElement.ToString()) != 0;
        }

        public void RemoveSubLayout(string subLayoutName_)
        {
            var workspacesConfig = (Configurator)ConfigurationManager.GetConfig("Workspaces");
            var nameRoot = GetWorkspaces(workspacesConfig);
            SubLayoutDefinition oldDefinition = null;
            if (nameRoot != null)
            {
                var workspaceNodes = nameRoot.SelectNodes("//Workspace[@Name='" + subLayoutName_ + "']");
                if (workspaceNodes != null)
                {
                    foreach (XmlNode existingNode in workspaceNodes)
                    {
                        if (existingNode != null)
                        {
                            nameRoot.RemoveChild(existingNode);
                            oldDefinition = ConvertToDefinition(existingNode);
                        }
                    }
                    workspacesConfig.SetNode("/Workspaces", null, nameRoot);
                }

            }
            
            
            ClearCachedLayout(subLayoutName_);
            workspacesConfig.Save();
            if (oldDefinition != null && !String.IsNullOrEmpty(oldDefinition.RootViewId))
            { 
               var workspaceToDelete = savedWorkspaces.FindAll(w_ => w_.ID == oldDefinition.RootViewId); 
                deletedWorkspaces.AddRange(workspaceToDelete);
                foreach (var container in workspaceToDelete)
                {
                    savedWorkspaces.Remove(container);
                    allOpenedWorkspaces.Remove(container);
                    CreateSubLayout(container);
                }
            }
            OnSubLayoutChanged(oldDefinition, null);
        }

        public void LoadSubLayout(string subLayoutName_, SubLayoutLocation location_)
        {
            SubLayoutDefinition definition = GetSubLayout(subLayoutName_);
            if (definition == null)
            {
                TaskDialog.ShowMessage(
                                           String.Format(
                                               "Workspace '{0}' doesn't exist.",
                                               subLayoutName_),
                                           "Load",
                                           TaskDialogCommonButtons.Close,
                                           VistaTaskDialogIcon.Warning);
                return;
            }

            IWindowViewContainer existingContainer = savedWorkspaces.FirstOrDefault(w_ => w_.ID == definition.RootViewId);
            if (existingContainer != null)
            {

                foreach (var windowViewContainer in DockHelper.GetViewsInvolved(existingContainer))
                {
                    windowViewContainer.Close();
                    //not closed

                    //floating window as workspace
                    if (DockHelper.GetRootOfFloatingPane(windowViewContainer) != null ||
                    //root tab as workspace
                        DockHelper.GetRootOfDockedPane(windowViewContainer) != null) return;
                }  
                
            }
            // load from CC
            var config =
                (Configurator)
                ConfigurationManager.GetConfig(SubLayoutPrefix + XmlConvert.EncodeLocalName(subLayoutName_));
            var xmlTemp = config.GetNode("SubLayout", null) as XmlElement;
            if (xmlTemp == null)
                return;

            var subLayoutXml = XElement.Parse(xmlTemp.OuterXml);

            // load the xml
            var viewsElement = subLayoutXml.Elements().FirstOrDefault(xe => xe.Name == "Views");

            var subLayoutElement = subLayoutXml.Elements().FirstOrDefault(xe => xe.Name == this.DockLayoutRootName);
            if (subLayoutElement == null) return;
            object additionalData = null;
            if (location_ == SubLayoutLocation.DockedInNewTab)
            {
                TabbedDockViewModel tabbedDockViewModel = _windowManager as TabbedDockViewModel;
                if (tabbedDockViewModel != null)
                {

                    additionalData = tabbedDockViewModel.AddTab(subLayoutName_);
                }
            }
            // load the views
            var rootPanes = _windowManager.ReadSubState(definition.LayoutName, subLayoutElement,
                                                        (storedId, newId) =>
                                                        {
                                                            if (viewsElement == null) return;
                                                            var viewElement = viewsElement
                                                                .Elements()
                                                                .FirstOrDefault(xe => xe.
                                                                                          Attributes().
                                                                                          Any(
                                                                                              attr =>
                                                                                              attr.Name == "Id" &&
                                                                                              ContentPaneIdConverter
                                                                                                  .ConvertToPaneId
                                                                                                  (attr.Value) ==
                                                                                              storedId));

                                                            if (viewElement == null)
                                                                return;

                                                            newId =
                                                                ContentPaneIdConverter.ConverFromPaneId(newId);

                                                            viewElement.SetAttributeValue("Id", newId);

                                                            ApplyPaneNode(new XDocument(viewElement),
                                                                          additionalData);

                                                        }, false, location_);

            //if (!rootPanes.Any()) return;
            var tileViewElement = subLayoutXml.Elements().FirstOrDefault(xe => xe.Name == "TileView");
            if (tileViewElement != null)
            {
                _tileItemManager.LoadState(tileViewElement, (d, a) => ApplyTileItemNode(d, a));
            }

        }
          
        public SubLayoutDefinition GetSubLayout(string subLayoutName_)
        {
            if (String.IsNullOrWhiteSpace(subLayoutName_)) return null;
            var rt = (Configurator)ConfigurationManager.GetConfig("Workspaces");
            var root = GetWorkspaces(rt);
            if (root == null)
                return null;

            var list = root.SelectNodes("//Workspace");
            if (list == null) return null;
            foreach (var element in list)
            {
                var node = element as XmlNode;
                if (node == null)
                    continue;

                var name = XmlHelper.ReadAttribute(node, "Name");
                if (name != null && name.Trim().ToLower() == subLayoutName_.Trim().ToLower())
                {
                    return ConvertToDefinition(node);
                } 
            }
            return null;
        }

        public IList<SubLayoutDefinition> GetSubLayouts()
        {
            List<SubLayoutDefinition> definitions = new List<SubLayoutDefinition>();
            // load from CC
            var rt = (Configurator)ConfigurationManager.GetConfig("Workspaces");
            var root = GetWorkspaces(rt);
            if (root == null) return definitions;

            var list = root.SelectNodes("//Workspace");
            if (list == null) return definitions;
            List<string> workspaces = new List<string>();
            foreach (var element in list)
            {
                var node = element as XmlNode;
                if (node == null)
                    continue;
                SubLayoutDefinition definition = ConvertToDefinition(node);
                //avoid duplicate nodes
                if (!workspaces.Contains(definition.LayoutName))
                {
                    definitions.Add(definition);
                    workspaces.Add(definition.LayoutName);
                }
            }
            return definitions;
        }

        public bool IsSaved(IWindowViewContainer rootView_)
        {
            return savedWorkspaces.Contains(rootView_); 
        }

        public bool IsDeleted(IWindowViewContainer rootView_)
        {
            return deletedWorkspaces.Contains(rootView_); 
        }

        public bool IsSubLayoutContainer(IWindowViewContainer rootView_)
        {
            return allOpenedWorkspaces.Contains(rootView_);
        }

        public event EventHandler<SubLayoutChangedEventArgs> SubLayoutChanged;

        protected virtual void OnSubLayoutChanged(SubLayoutDefinition oldLayout_, SubLayoutDefinition newLayout_)
        {
            var copy = SubLayoutChanged;
            if (copy != null)
            {
                copy(this, new SubLayoutChangedEventArgs(oldLayout_, newLayout_));
            }
        }


        #region Helper Methods
        private SubLayoutDefinition ConvertToDefinition(XmlNode node_)
        { 
            return new SubLayoutDefinition()
            {
                LayoutName = XmlHelper.ReadAttribute(node_, "Name", string.Empty),
                Category = XmlHelper.ReadAttribute(node_, "Category", string.Empty),
                RootViewId = XmlHelper.ReadAttribute(node_, "RootViewId", string.Empty)
            };

        }

        private void AddToSavedWorkspaces(IWindowViewContainer rootView_)
        {
            unsavedWorkspaces.Remove(rootView_);
            if (savedWorkspaces.Contains(rootView_)) return;
            savedWorkspaces.Add(rootView_);
            if (!allOpenedWorkspaces.Contains(rootView_))
            {
                allOpenedWorkspaces.Add(rootView_);
            }

        }

        private bool IsSavedWorkspace(string rootViewId_)
        {
            var workspacesConfig = (Configurator)ConfigurationManager.GetConfig("Workspaces");
            var nameRoot = GetWorkspaces(workspacesConfig);
            if (nameRoot == null) return false;
            var workspaceNodes = nameRoot.SelectNodes("//Workspace[@RootViewId='" + rootViewId_ + "']");
            return workspaceNodes != null && workspaceNodes.Count > 0; 
        }


        private void ClearCachedLayout(string subLayoutName_)
        {
            ConfigurationManagerHelper.DirtyConfiguration(SubLayoutPrefix + XmlConvert.EncodeLocalName(subLayoutName_));  
        }

        private XmlDocument WriteSubLayout(IList<IWindowViewContainer> containers_, bool withWrapper_, bool skipLocation_)
        {
            var subLayout = DockHelper.GetNormalizedSubLayout(containers_);

            var viewsElement = new XElement("Views");
            var subLayoutElement = new XElement(this.DockLayoutRootName);
            var subLayoutRoot = new XElement("SubLayout", viewsElement, subLayoutElement);
            var xml = new XDocument(subLayoutRoot);

            foreach (WindowViewContainer view in containers_)
            {
                viewsElement.Add(BuildIViewXml(view));
                TileViewContainer tileView = view as TileViewContainer;
                if (tileView == null) continue;
                XElement element = _tileItemManager.SaveState(tileView, BuildIViewXml);
                subLayoutRoot.Add(element);
                break;
            } 

            // get the layout
            var xamLayoutElement = _windowManager.WriteSubState(subLayout, containers_, skipLocation_);
            if (xamLayoutElement == null)
                return null;
             
            
            subLayoutElement.Add(xamLayoutElement);

            var xn = new XmlDocument
            {

                InnerXml = withWrapper_ ? String.Format("<wrapper>{0}</wrapper>", xml) : xml.ToString()
            };
            
            return xn;
        }
         

        private XElement GetCoreElement(XmlNode layout_)
        {
            CleanupXml(layout_);

            var subLayoutXml = XElement.Parse(layout_.OuterXml);
            // load the xml
            var viewsElement = subLayoutXml.Elements().FirstOrDefault(xe => xe.Name == "Views");
            //var subLayoutElement = subLayoutXml.Elements().FirstOrDefault(xe => xe.Name == LayoutConverter.DockLayoutRootName); 
            var tileLayoutElement = subLayoutXml.Elements().FirstOrDefault(xe => xe.Name == "TileView");

            return tileLayoutElement == null ?
                new XElement("SubLayout", viewsElement) :
                new XElement("SubLayout", tileLayoutElement); 
        }

        readonly string[] attributesToIgnore = new string[] { "//@Width", "//@Height", "//@Left", "//@Top", "//@LastRightEdgeMargin", "//@LastBottomEdgeMargin" };
        private void CleanupXml(XmlNode root_)
        {
            foreach (var attributeToIgnore in attributesToIgnore)
            {
                var nodes = root_.SelectNodes(attributeToIgnore);
                if (nodes == null) return;
                foreach (XmlAttribute node in nodes)
                {
                    node.OwnerElement.RemoveAttribute(node.LocalName, node.NamespaceURI);
                }
            } 
        }

        private XmlNode GetWorkspaces(Configurator workspaceConfig_)
        {
            var nameRoot = workspaceConfig_.GetNode("/Workspaces", null);
            if (nameRoot == null || nameRoot.ChildNodes.Count == 0) return nameRoot;
            var workspacesNodes = nameRoot.SelectNodes("//Workspace");
            if (workspacesNodes == null || workspacesNodes.Count == 0) return nameRoot;

            List<string> workspaceNames = new  List<string>();
            foreach (XmlNode workspaceNode in workspacesNodes)
            {
                string workspaceName = XmlHelper.ReadAttribute(workspaceNode, "Name");
                if (string.IsNullOrEmpty(workspaceName))
                {
                    nameRoot.RemoveChild(workspaceNode);
                    continue;
                }
                if (!workspaceNames.Contains(workspaceName.ToLower()))
                {
                    workspaceNames.Add(workspaceName.ToLower());
                }
                else
                {
                    nameRoot.RemoveChild(workspaceNode); 
                }
            }

            return nameRoot;
        }
        #endregion


    }
     
}
