﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl
{
	public class WidgetRegistration
	{
		public bool Auxiliary { get; set; }
		public string FactoryID { get; set; }
		public string Root { get; set; }
		public string Location { get; set; }
		//public IWidgetViewContainer ViewContainer { get; set; }
		public InitialWidgetParameters InitialParameters { get; set; }
		public IWidgetViewContainer ViewContainer { get; set; }

		public override string ToString()
		{
			return string.Format("{0} @ {1}\\{2}", this.FactoryID, this.Root, this.Location);
		}

		/// <summary>
		/// If true, the registration will be ignored. This is to avoid loss of IWP when moving a registration by 
		/// removing and readding by factoryID.
		/// </summary>
		public bool Disabled { get; set; }

		public WidgetRegistration()
		{
			this.Disabled = false;
			this.Auxiliary = false;
		}
	}
}
