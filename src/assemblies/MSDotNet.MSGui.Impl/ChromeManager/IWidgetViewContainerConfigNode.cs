using System.Collections.ObjectModel;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
    internal interface IWidgetViewContainerConfigNode : IWidgetViewContainer
    {
        ObservableCollection<IWidgetViewContainer> ObservableChildren { get; }
        void SwapChildren(IWidgetViewContainer a_, IWidgetViewContainer b_);
        bool CanAddwidget(IWidgetViewContainer widget_);
        InitialWidgetParameters AddDefault(ref string ID);
        void Rename(string newName);
        string CurrentName { get; }
        string NodeName { get; }
        XElement SaveState();
        void LoadState(XElement state_);
    }
}