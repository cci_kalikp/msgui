﻿using System;
using System.Collections.Generic;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
    public static class WidgetViewContainerHelper
    {
        static readonly Dictionary<IWidgetViewContainer, EventHandler> contentReadyHandlers = new Dictionary<IWidgetViewContainer, EventHandler>(); 
        public static void SetIsVisible(this IWidgetViewContainer container_, bool visible_)
        {
            EventHandler contentReadyHandler;
            if (contentReadyHandlers.TryGetValue(container_, out contentReadyHandler))
            {
                container_.ContentReady -= contentReadyHandler; 
            }
            Visibility visibility = visible_ ? Visibility.Visible : Visibility.Collapsed;
            UIElement item = container_.Content as UIElement;
            if (item != null)
            {
                item.Visibility = visibility; 
                if (contentReadyHandler != null)
                {
                    contentReadyHandlers.Remove(container_);
                }
            }
            else
            {
                container_.ContentReady += contentReadyHandler = (sender_, args_) =>
                {
                    UIElement item2 = ((IWidgetViewContainer)sender_).Content as UIElement;
                    if (item2 != null)
                    {
                        item2.Visibility = visibility;
                    }
                };
                contentReadyHandlers[container_] = contentReadyHandler;

            }
        }

        public static void Add(this WidgetViewContainer parentContainer_, IWidgetViewContainer childContainer_)
        {
            parentContainer_.AddLogical(childContainer_);
            parentContainer_.AddVisual(childContainer_);
            ((WidgetViewContainer)childContainer_).Parent = parentContainer_;
        }

        public static void ClearParent(this IWidgetViewContainer container_)
        {
            if (container_.Parent != null)
            {
                container_.Parent.RemoveWidget(container_);
                ((WidgetViewContainer)container_).Parent = null;
            }
        }
        public static void ClearParents(IEnumerable<IWidgetViewContainer> containers_)
        {
            foreach (var container in containers_)
            {
                container.ClearParent();
            }
        }


        public static IWidgetViewContainer CreateOrReAdd<T>(this WidgetViewContainer parent_, T key_,
                                            IDictionary<T, IWidgetViewContainer> childCreated_,
                                            Func<T, IWidgetViewContainer> widgetCreator_)
        {
            IWidgetViewContainer widget ;
            if (!childCreated_.TryGetValue(key_, out widget))
            {
                widget = widgetCreator_(key_);
                childCreated_.Add(key_, widget);
            }
            else
            {
                parent_.Add(widget);
            }
            return widget;
        }

        public static WidgetViewContainer CreateOrReAdd<T>(this WidgetViewContainer parent_, T key_,
                                    IDictionary<T, WidgetViewContainer> childCreated_,
                                    Func<T, WidgetViewContainer> widgetCreator_)
        {
            WidgetViewContainer widget;
            if (!childCreated_.TryGetValue(key_, out widget))
            {
                widget = widgetCreator_(key_);
                childCreated_.Add(key_, widget);
            }
            else
            {
                parent_.Add(widget);
            }
            return widget;
        } 

        public static void CreateOrReAdd(this WidgetViewContainer parent_, 
                                    ref IWidgetViewContainer childCreated_,
                                    Func<IWidgetViewContainer> widgetCreator_)
        {

            if (childCreated_ == null)
            {
                childCreated_ = widgetCreator_(); 
            }
            else
            {
                parent_.Add(childCreated_);
            }
        }

        public static void CreateOrReAdd(this WidgetViewContainer parent_,
                               ref WidgetViewContainer childCreated_,
                               Func<WidgetViewContainer> widgetCreator_)
        {

            if (childCreated_ == null)
            {
                childCreated_ = widgetCreator_();
            }
            else
            {
                parent_.Add(childCreated_);
            }
        }
    }
}
