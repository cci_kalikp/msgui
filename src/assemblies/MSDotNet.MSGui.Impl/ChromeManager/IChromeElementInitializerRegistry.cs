﻿using System;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
    internal interface IChromeElementInitializerRegistry
    {
        bool TryGetWidgetFactoryHolder(Type t, out WidgetFactoryHolder factory);
        bool TryGetWidgetFactoryHolder(string factoryId, out WidgetFactoryHolder factory);
        bool TryGetWindowFactoryHolder(string factoryId, out WindowFactoryHolder factory);
        bool TryGetWindowFactoryHolder(Type t, out WindowFactoryHolder factory); 
    }
}
