﻿using System;
using System.Windows.Media;
using Infragistics.Windows.Ribbon;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.LauncherBar;
using System.Windows.Controls;
using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Impl.View
{
	public class LauncherBarChromeHelper : IChromeHelper
	{
		internal ChromeManager ChromeManager;

		internal LauncherBarChromeHelper(ChromeManager chromeManager)
		{
			this.ChromeManager = chromeManager;
		}

		#region IChromeHelper Members

		public InitializeWidgetHandler ButtonFactory { get { return ButtonFactoryMethod; } }
		public InitializeWidgetHandler ShowWindowButtonFactory { get { return ShowWindowButtonFactoryMethod; } }
		public InitializeWidgetHandler DropdownButtonFactory { get { return DropdownButtonFactoryMethod; } }
		public InitializeWidgetHandler ComboBoxFactory { get { return ComboBoxFactoryMethod; } }
		public InitializeWidgetHandler PopupFactory { get { return PopupFactoryMethod; } }

		#endregion

		private bool ButtonFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
		{
			InitialButtonParameters parameters = emptyWidget.InitialParameters as InitialButtonParameters;
			if (parameters != null)
			{
				var button = new LauncherBarButton(new MSGuiButton()
				{
					OnClick = parameters.Click,
					Text = parameters.Text,
					ToolTip = parameters.ToolTip,
					Image = parameters.Image,
					IsEnabled = parameters.Enabled,
					ButtonSize = ButtonSize.Small
				});
				emptyWidget.Content = button;
				return true;
			}
			else
			{
				return false;
			}
		}

		private bool ShowWindowButtonFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
		{
			InitialShowWindowButtonParameters parameters = emptyWidget.InitialParameters as InitialShowWindowButtonParameters;
			if (parameters != null)
			{
				var button = new LauncherBarButton(new MSGuiButton()
				{
					OnClick = (a, b) => { ChromeManager.CreateWindow(parameters.WindowFactoryID, parameters.InitialParameters); },
					Text = parameters.Text,
					ToolTip = parameters.ToolTip,
					Image = parameters.Image,
					IsEnabled = parameters.Enabled,
					ButtonSize = ButtonSize.Small
				});
				emptyWidget.Content = button;
				return true;
			}
			else
			{
				return false;
			}
		}

		private bool DropdownButtonFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
		{
			InitialDropdownButtonParameters parameters = emptyWidget.InitialParameters as InitialDropdownButtonParameters;
			if (parameters != null)
			{
				var name_ = XamRibbonHelper.SanitizeRibbonItemName(parameters.Text);
				MenuTool barMenuTool = new BarMenuTool           //I've created a MenuTool with modified foreground
				{
					Id = emptyWidget.FactoryID,
					Caption = parameters.Text,
					ButtonType = MenuToolButtonType.DropDown,
					IsEnabled = parameters.Enabled,
					LargeImage = parameters.Image,
					SmallImage = parameters.Image,
					Foreground = Brushes.White,
				};
				barMenuTool.SetValue(RibbonGroup.MaximumSizeProperty, RibbonToolSizingMode.ImageAndTextNormal);

				emptyWidget.Content = barMenuTool;
				return true;
			}
			else
			{
				return false;
			}
		}

		private bool ComboBoxFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
		{
			InitialComboBoxParameters parameters = emptyWidget.InitialParameters as InitialComboBoxParameters;
			if (parameters != null)
			{
				var combo = new ComboBox()
				{
					ItemsSource = parameters.ItemsSource,
					IsEnabled = parameters.Enabled, 
					Text = parameters.Text,
				};
	
				emptyWidget.Content = combo;
				return true;
			}
			else
			{
				return false;
			}
		}

		private bool PopupFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
		{
			InitialPopupParameters parameters = emptyWidget.InitialParameters as InitialPopupParameters;
			if (parameters != null)
			{
				var name_ = XamRibbonHelper.SanitizeRibbonItemName(parameters.Text);
				MenuTool barMenuTool = new BarMenuTool           //I've created a MenuTool with modified foreground
				{
					Id = emptyWidget.FactoryID,
					Caption = parameters.Text,
					ButtonType = MenuToolButtonType.DropDown,
					IsEnabled = parameters.Enabled,
					LargeImage = parameters.Image,
					SmallImage = parameters.Image,
					ItemsSource = parameters.ItemsSource,
					Foreground = Brushes.White,
				};
				barMenuTool.SetValue(RibbonGroup.MaximumSizeProperty, RibbonToolSizingMode.ImageAndTextNormal);

				emptyWidget.Content = barMenuTool;
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
