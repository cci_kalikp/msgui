﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
    public partial class ChromeRegistry : IChromeRegistry
    {
        //internal ViewManager ViewManager = null;
        internal ICommonFactories ChromeHelper { get; set; }
        internal ChromeManagerBase ChromeManager { get; set; }

        private readonly IWidgetViewContainer widgetFactoryScopeRoot = new WidgetViewContainer("5FE41DA2-B7E4-40F7-85B8-FEC5E570E5CF");

        private readonly IDictionary<string, WindowFactoryHolder> _windowFactories = new Dictionary<string, WindowFactoryHolder>();
        private readonly ObservableCollection<string> _windowFactoryIDs = new ObservableCollection<string>(); 
        private readonly IDictionary<string, DialogFactoryHolder> _dialogFactories = new Dictionary<string, DialogFactoryHolder>();
//        private readonly IDictionary<string, WidgetFactoryHolder> _widgetFactories = new Dictionary<string, WidgetFactoryHolder>();
        private readonly IDictionary<string, PrintFactoryHolder> _printFactories = new Dictionary<string, PrintFactoryHolder>();
        private readonly IDictionary<Type, WidgetFactoryHolder> _widgetFactoryMapping = new Dictionary<Type, WidgetFactoryHolder>();
        private readonly ObservableCollection<string> widgetFactoryIDs = new ObservableCollection<string>();  
        private readonly IDictionary<Type, InitializeWindowHandler> _windowInitMapping = new Dictionary<Type, InitializeWindowHandler>();
        private readonly IDictionary<Type, Type> _windowRemaping = new Dictionary<Type, Type>();
        private readonly IDictionary<Type, DehydrateWindowHandler> _windowDehydrateMapping = new Dictionary<Type, DehydrateWindowHandler>();
        private readonly IDictionary<Type, WindowFactoryHolder> _windowFactoryMapping = new Dictionary<Type, WindowFactoryHolder>();

        private readonly IDictionary<IWidgetViewContainer, IDictionary<string, WidgetFactoryHolder>> scopedWidgetFactories = new Dictionary<IWidgetViewContainer, IDictionary<string, WidgetFactoryHolder>>();
        

            #region IChromeRegistry Members

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void RegisterFactory(string ID,
                                    InitializeWindowHandler initWindowHandler = null,
                                    DehydrateWindowHandler dehydrateWindowHandler = null,
                                    InitializeWidgetHandler initWidgetHandler = null,
                                    DehydrateWidgetHandler dehydrateWidgetHandler = null )
        {
            if (initWindowHandler == null || initWidgetHandler == null)
            {
                throw new ArgumentNullException();
            }

            RegisterWindowFactory(ID, initWindowHandler, dehydrateWindowHandler);
            RegisterWidgetFactory(ID, initWidgetHandler, dehydrateWidgetHandler); 
        }

        //public void RegisterWindowFactory(string ID, InitializeWindowHandler initHandler, DehydrateWindowHandler dehydrateHandler = null)
        //{
        //    InitializeWindowHandler2 initWrapper = (vc, state) =>
        //    {
        //        if (!initHandler(vc, state))
        //        {
        //            throw new Exception(string.Format("Window creation using factory \"{0}\" failed.", ID));
        //        }
        //    };

        //    this.RegisterWindowFactory(ID, initWrapper, dehydrateHandler);
        //}

        public void RegisterWindowFactory(string ID, InitializeWindowHandler initHandler,
                                          DehydrateWindowHandler dehydrateHandler = null)
        {
            this.RegisterWindowFactory(ID).SetInitHandler(initHandler).SetDehydrateHandler(dehydrateHandler);
        }

        public IWindowFactoryHolder RegisterWindowFactory(string ID)
        {
            var wfh = new WindowFactoryHolder { Name = ID };
            _windowFactories.Add(ID, wfh);
            _windowFactoryIDs.Add(ID);
            return wfh;
        }

        public void UnregisterWindowFactory(string id)
        {
            _windowFactories.Remove(id);
            _windowFactoryIDs.Remove(id);
        }
         

        //public void RegisterDialogFactory(string ID, InitializeDialogHandler initHandler)
        //{
        //    InitializeDialogHandler2 initWrapper = (vc, state) =>
        //    {
        //        if (!initHandler(vc, state))
        //        {
        //            throw new Exception(string.Format("Window creation using factory \"{0}\" failed.", ID));
        //        }
        //    };

        //    this.RegisterDialogFactory(ID, initWrapper);
        //}

        public void RegisterDialogFactory(string ID, InitializeDialogHandler initHandler)
        {
            _dialogFactories.Add(ID, new DialogFactoryHolder()
                {
                    InitHandler = initHandler
                });
        }

        //public void RegisterWidgetFactory(string ID, InitializeWidgetHandler initHandler = null, DehydrateWidgetHandler dehydrateHandler = null)
        //{
        //    InitializeWidgetHandler2 initWrapper = (vc, state) =>
        //    {
        //        if (!initHandler(vc, state))
        //        {
        //            throw new Exception(string.Format("Widget creation using factory \"{0}\" failed.", ID));
        //        }
        //    };

        //    this.RegisterWidgetFactory(ID, initWrapper, dehydrateHandler);
        //}

        public IWidgetFactoryHolder RegisterWidgetFactory(string ID)
        {
            var result = this.RegisterWidgetFactory(ID, this.widgetFactoryScopeRoot);
            widgetFactoryIDs.Add(ID);
            return result;
        }

        public IWidgetFactoryHolder RegisterWidgetFactory(string ID, IWidgetViewContainer scope)
        {
            var wfh = new WidgetFactoryHolder { Name = ID };
            this.GetOrCreateWidgetFactoryScope(scope).Add(ID, wfh);
            return wfh;
        }

        internal WidgetFactoryHolder GetWidgetFactory(string id)
        {
            return GetWidgetFactory(id, this.widgetFactoryScopeRoot);
        }

        internal WidgetFactoryHolder GetWidgetFactory(string id, IWidgetViewContainer scope)
        {
            while (scope != null)
            {
                if (this.scopedWidgetFactories.ContainsKey(scope))
                {
                    var factories = this.scopedWidgetFactories[scope];
                    if (factories.ContainsKey(id))
                    {
                        return factories[id];
                    }
                }

                scope = scope.Parent;
            }

            /* Above loop does not check the root level factories, as the root is not marked by null in the dictionary (key can not be 
             * null), so we do one final lookup or return null if failed.
             */
            var rootFactories = this.scopedWidgetFactories[this.widgetFactoryScopeRoot];
            return rootFactories.ContainsKey(id) ? rootFactories[id] : null;
        }

        private IDictionary<string, WidgetFactoryHolder> GetOrCreateWidgetFactoryScope(IWidgetViewContainer scope)
        {
            if (!this.scopedWidgetFactories.ContainsKey(scope))
            {
                this.scopedWidgetFactories.Add(scope, new Dictionary<string, WidgetFactoryHolder>());
            }

            return this.scopedWidgetFactories[scope];
        }

        public void RegisterWidgetFactory(string ID, InitializeWidgetHandler initHandler = null,
                                          DehydrateWidgetHandler dehydrateHandler = null)
        {
            RegisterWidgetFactory(ID).SetInitHandler(initHandler).SetDehydrateHandler(dehydrateHandler, false);
        }

        public void RegisterGlobalStoredWidgetFactory(string ID, InitializeWidgetHandler initHandler = null,
                                                      DehydrateWidgetHandler dehydrateHandler = null)
        {
            RegisterWidgetFactory(ID).SetInitHandler(initHandler).SetDehydrateHandler(dehydrateHandler, true);
        }

        public ReadOnlyObservableCollection<string> WindowFactoryIDs
        {
            get { return new ReadOnlyObservableCollection<string>(_windowFactoryIDs); }
        }

        public ReadOnlyObservableCollection<string> WidgetFactoryIDs
        {
            get { return new ReadOnlyObservableCollection<string>(widgetFactoryIDs); }
        }
        #endregion

        internal IDictionary<string, WindowFactoryHolder> WindowFactories
        {
            get { return _windowFactories; }
        }

        internal IDictionary<string, DialogFactoryHolder> DialogFactories
        {
            get { return _dialogFactories; }
        }

        internal IDictionary<string, WidgetFactoryHolder> WidgetFactories
        {
            get { return scopedWidgetFactories[this.widgetFactoryScopeRoot]; }
        }

        internal IDictionary<string, PrintFactoryHolder> PrintFactories
        {
            get { return _printFactories; }
        }

        internal IDictionary<Type, WidgetFactoryHolder> WidgetFactoryMapping
        {
            get { return _widgetFactoryMapping; }
        }

        public IWidgetFactoryHolder RegisterWidgetFactoryMapping<T>() where T : InitialWidgetParameters
        {
            WidgetFactoryHolder holder;
            if (!_widgetFactoryMapping.TryGetValue(typeof(T), out holder))
            {
                holder = new WidgetFactoryHolder();
                _widgetFactoryMapping[typeof(T)] = holder;
            }
            return holder;
        }
        /// <summary>
        /// Registers a factory - InitialWidgetParameters mapping for the implicit factory logic to work.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="factory"></param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public void RegisterWidgetFactoryMapping<T>(InitializeWidgetHandler factory)
            where T : InitialWidgetParameters
        {
            RegisterWidgetFactoryMapping<T>().SetInitHandler(factory);
        }

        public void RegisterWidgetDehydrateCallbackMapping<T>(DehydrateWidgetHandler callback)
            where T : InitialWidgetParameters
        {
            RegisterWidgetFactoryMapping<T>().SetDehydrateHandler(callback);
        }


        /// <summary>
        /// Registers a window factory mapping.
        /// </summary>
        /// <typeparam name="T">Type of initial parameters desired window.</typeparam>
        /// <param name="factory">Factory responsible for creating it.</param>
        public void RegisterWindowFactoryMapping<T>(InitializeWindowHandler factory)
            where T: InitialWindowParameters
        {

            _windowInitMapping[typeof(T)] = factory;


        }

        public void RegisterWindowFactoryMapping<F, T>() where F : InitialWindowParameters where T : F
        {
            _windowRemaping[typeof (T)] = typeof (F);
        }

        public void RegisterWindowDehydrateCallbackMapping<T>(DehydrateWindowHandler callback)
            where T : InitialWindowParameters
        {
            _windowDehydrateMapping[typeof(T)] = callback;

        }
        public void ChainWindowFactoryMapping<T>(InitializeWindowHandlerStep extraHandler_) where T : InitialWindowParameters
        {
            if (extraHandler_ == null) return;
            _windowFactoryMapping[typeof(T)].AddStep(extraHandler_);
        }
        #region Factory holders

        internal sealed class DialogFactoryHolder
        {
            public InitializeDialogHandler InitHandler { get; set; }
        }

        internal sealed class PrintFactoryHolder
        {
            public PrintWindowHandler PrintHandler { get; set; }
            public PreviewPrintWindowHandler PreviewHandler { get; set; }
        }


        public void RegisterPrintFactory(string ID, PrintWindowHandler printHandler = null,
                                         PreviewPrintWindowHandler previewPrintHandler = null)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
