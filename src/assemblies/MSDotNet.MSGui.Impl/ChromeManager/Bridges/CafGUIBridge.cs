﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Bridges
{
    internal static class CafGuiBridge
    {
        internal const string CafGuiWindowFactoryName = "CafGuiToolkitWindowFactory";

        internal static void Initialize(IChromeRegistry chromeRegistry, CafGuiWindowsCollection collection)
        {
            chromeRegistry.RegisterWindowFactory(
                CafGuiWindowFactoryName,
                collection.CreateWindowPlaceholder,
                collection.DehydrateWindow);
        }
    }
}
