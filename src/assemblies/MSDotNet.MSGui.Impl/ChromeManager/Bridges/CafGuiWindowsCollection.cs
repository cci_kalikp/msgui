﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Bridges
{
    internal sealed class CafGuiWindowsCollection
    {
        private readonly IDictionary<string, IWindowViewContainer> _placeholders;
        private readonly IDictionary<string, string> _cafGuiViewIds;

        public CafGuiWindowsCollection()
        {
            _placeholders = new Dictionary<string, IWindowViewContainer>(StringComparer.OrdinalIgnoreCase);
            _cafGuiViewIds = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        }

        internal XDocument DehydrateWindow(IWindowViewContainer container)
        {
            var parameters = container.Parameters as InitialCafGuiWindowParameters;
            var cafGuiViewId = parameters != null 
                ? parameters.CafGuiViewId 
                : _cafGuiViewIds[container.ID];

            var document = new XDocument(new XElement("CafGuiViewId", cafGuiViewId));
            return document;
        }

        internal bool CreateWindowPlaceholder(IWindowViewContainer container, XDocument state)
        {
            var parameters = container.Parameters as InitialCafGuiWindowParameters;
            if (parameters != null)
            {
                // Actual creation.
                container.Content = parameters.CafGuiUiElement;
                _placeholders.Add(parameters.CafGuiViewId, container);
            }
            else
            {
                // Placeholder from persistence. Set a simple content so that the view can be placed properly 
                // Place the container into placeholders collection. We expect it to be in the first node.
                var cafGuiViewId = state.Root.FirstNode.ToString(SaveOptions.None);
                _placeholders[cafGuiViewId] = container;
                _cafGuiViewIds[container.ID] = cafGuiViewId;
                container.Content = new TextBlock();
            }

            return true;
        }

        internal bool TryGetContainer(string cafGuiViewId, out IWindowViewContainer container)
        {
            return _placeholders.TryGetValue(cafGuiViewId, out container);
        }
    }
}
