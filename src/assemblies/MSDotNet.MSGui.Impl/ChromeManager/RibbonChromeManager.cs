﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Infragistics.Windows.Controls;
using Infragistics.Windows.Ribbon;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Navigation;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.NavigationBar;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.TabWell;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
    public partial class RibbonChromeManager : ChromeManagerBase
    {
        private readonly ShellWithRibbon _shell;
        private readonly XamRibbon _ribbon;
        private readonly RibbonInfragisticsImpl _ribbonInfragisticsImpl;
        internal Dictionary<string, int> QATPlacements = new Dictionary<string, int>();
        private readonly InitialSettings _initialSettings;

        internal RibbonChromeManager(
            ChromeRegistry chromeRegistry,
            IShell shell,
            PersistenceProfileService persistenceProfileService,
            PersistenceService persistenceService,
            IUnityContainer container,
            IWindowFactory windowFactory,
            InitialSettings initialSettings) :
                base(chromeRegistry, shell, persistenceProfileService, persistenceService, container, windowFactory)
        {
            
            _initialSettings = initialSettings;
            _shell = (ShellWithRibbon) shell;
            _ribbon = _shell.Ribbon;
            IHidingLockingManager lockManager = container.Resolve<IHidingLockingManager>();
            if (lockManager.Granularity.HasFlag(HideLockUIGranularity.DisableCloseWindowByOrbDoubleClick))
            {
                _ribbon.ApplicationMenu.CommandBindings.Add(new CommandBinding(RibbonWindowCommands.CloseCommand,
                    //do nothing here, let the XamRibbonWindow handle it if enabled
                                                                  (sender_, args_) => { }, (sender_, args_) =>
                                                                  {
                                                                      if (args_.OriginalSource == _ribbon.ApplicationMenu)
                                                                      {
                                                                          args_.CanExecute = false;
                                                                          args_.Handled = true;
                                                                      }
                                                                  }
                                                                    ));
                
            }

            _ribbonInfragisticsImpl = new RibbonInfragisticsImpl(RibbonChromeManagerRibbon);
            persistenceService.AddPersistor(
                _ribbonInfragisticsImpl.PersistorId,
                _ribbonInfragisticsImpl.LoadState,
                _ribbonInfragisticsImpl.SaveState);

            AfterCreateChrome += RibbonChromeManager_AfterCreateChrome;

            #region 2.0

            RibbonFactories.Initialize(this, shell);
            NavigationBarFactories.Initialize(shell, this, container);

            if (_initialSettings.ShellMode != ShellMode.RibbonAndFloatingWindows)
            {
                TabWellFactories.Initialize(this);
            }


            AddRootArea(RIBBON_COMMANDAREA_NAME, new InitialRibbonParameters(_ribbonInfragisticsImpl));
            AddRootArea(QAT_COMMANDAREA_NAME, new InitialQatParameters(_ribbonInfragisticsImpl));
            AddRootArea(WINDOW_TITLE_BAR, new InitialTitleBarRootParameters());
            AddRootArea(NAVIGATIONBAR_COMMANDAREA_NAME, new InitialNavigationBarParameters());
            AddRootArea(TOPCONTENT_COMMANDAREA_NAME, new InitialTopContentParameters());

            if (_initialSettings.ShellMode != ShellMode.RibbonAndFloatingWindows)
            {
                AddRootArea(TABWELL_COMMANDAREA_NAME, new InitialTabWellRootParameters());


                this[TABWELL_COMMANDAREA_NAME].AddWidget(TabWellRootArea.TabWellLeftEdgeID,
                                                         new InitialTabWellEdgeParameters
                                                             {
                                                                 TabControl =
                                                                     LogicalTreeHelper.FindLogicalNode(
                                                                         _shell.MainWindow, "tabControl") as
                                                                     XamTabControl,
                                                                 Side = HorizontalAlignment.Left
                                                             });
                this[TABWELL_COMMANDAREA_NAME].AddWidget(TabWellRootArea.TabWellRightEdgeID,
                                                         new InitialTabWellEdgeParameters
                                                             {
                                                                 TabControl =
                                                                     LogicalTreeHelper.FindLogicalNode(
                                                                         _shell.MainWindow, "tabControl") as
                                                                     XamTabControl,
                                                                 Side = HorizontalAlignment.Right
                                                             });

                var tdvm = _shell.WindowManager as TabbedDockViewModel;
                if (tdvm != null)
                {
                    (this[TABWELL_COMMANDAREA_NAME] as TabWellRootArea).DockViewModel = tdvm;
                }
            }

            this.DefaultChromeArea = this[RIBBON_COMMANDAREA_NAME] as WidgetViewContainer;

            #endregion
        }
         

        public XamRibbon RibbonChromeManagerRibbon
        {
            get { return _ribbon; }
        }

        internal ShellMode ShellMode
        {
            get { return _initialSettings.ShellMode; }
        }

        private void RibbonChromeManager_AfterCreateChrome(object sender, EventArgs e)
        {
            var qatRegistrationCount = Registrations.ContainsKey(this[QAT_COMMANDAREA_NAME])
                                           ? Registrations[this[QAT_COMMANDAREA_NAME]].Count
                                           : 0;
            if (qatRegistrationCount == 0)
            {
                StartupCompleted = true;
            }
            else if (WidgetUserConfigEnabled && WillLoadChromeFromState)
            {
                var qat = this[QAT_COMMANDAREA_NAME] as RibbonQATArea;
                if (qat != null)
                {
                    qat.SuspendLoadLayout();
                    foreach (var widgetViewContainer in qat.Children.ToArray())
                    {
                        qat.RemoveVisual(widgetViewContainer);
                        qat.RemoveLogical(widgetViewContainer);
                    }
                    qat.ResumeLoadLayout();
                }
                StartupCompleted = true;
            }

            if (m_tabOrder.Count > 0)
            {
                int i = 0;
                foreach (var tabName in m_tabOrder.Keys)
                {
                    var tab = RibbonChromeManagerRibbon.Tabs.FirstOrDefault(t => (t.Header as string) == tabName);
                    if (tab != null)
                    {
                        RibbonChromeManagerRibbon.Tabs.Move(RibbonChromeManagerRibbon.Tabs.IndexOf(tab), i++);

                        int j = 0;
                        foreach (string groupName in m_tabOrder[tabName].Keys)
                        {
                            var group = tab.RibbonGroups.FirstOrDefault(g => g.Caption == groupName);
                            if (group != null)
                            {
                                int k = 0;
                                tab.RibbonGroups.Move(tab.RibbonGroups.IndexOf(group), j++);
                                foreach (var itemName in m_tabOrder[tabName][groupName])
                                {
                                    var oc = group.ItemsSource as ObservableCollection<object>;
                                    if (oc != null && PlacedWidgets.ContainsKey(itemName))
                                    {
                                        var widget = PlacedWidgets[itemName];
                                        var oldIndex = oc.IndexOf(widget.Content);
                                        if (oldIndex >= 0)
                                        {
                                            oc.Move(oldIndex, k++);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        public override IWidgetViewContainer PlaceWidget(string factoryId, string location,
                                                         InitialWidgetParameters initialParameters)
        {
            return PlaceWidget(factoryId, null, location, initialParameters);
        }

        private IWidgetViewContainer ParseLocation(string location)
        {
            var path = location.Split('/');
            var parent = this["Ribbon"];
            foreach (var s in path)
            {
                parent = parent[s];
            }

            return parent;
        }

        protected override string GetDefaultArea()
        {
            return RIBBON_COMMANDAREA_NAME;
        }
         
        public override void SetRibbonIcon(System.Windows.Media.ImageSource icon)
        { 
            this.Ribbon.Icon = icon; 
        }

        public const string RIBBON_COMMANDAREA_NAME = "ribbon";
        public const string QAT_COMMANDAREA_NAME = "qat";
        public const string TABWELL_COMMANDAREA_NAME = "tabwell";
        public const string WINDOW_TITLE_BAR = "titlebar";
        public const string NAVIGATIONBAR_COMMANDAREA_NAME = "navigationbar"; 
        public const string TOPCONTENT_COMMANDAREA_NAME = "topcontent";

    }
}
