﻿using System;
using System.Collections.Generic;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
    public partial class ChromeRegistry : IControlParadigmRegistrator, IChromeElementInitializerRegistry
    {
        #region IControlParadigmRegistrator Members

        /*
		 * Below factory delegates replace the InitialParameters passed to it from the ChromeManager. This is 
		 * needed because the ChromeManager needs the button information at placement time, whereas the 
		 * ControlParadigm system requires (and thus provides) this information at register-time.
		 */

        void IControlParadigmRegistrator.AddRibbonItem(string tab, string group, RibbonItem item)
        {
            var ribbonButton = item as RibbonButton;
            if (ribbonButton != null)
            {
                RegisterWidgetFactory(ribbonButton.Text, ChromeHelper.ButtonFactory);

                var buttonSize = ribbonButton.Size == RibbonButtonSize.Small ? ButtonSize.Small : ButtonSize.Large;
                var vc = ChromeManager.PlaceWidget(ribbonButton.Text, string.Format("{0}/{1}", tab, group),
                                                        new InitialButtonParameters()
                                                            {
                                                                Enabled = ribbonButton.IsEnabled,
                                                                Image = ribbonButton.Icon.ToImageSource(),
                                                                Text = ribbonButton.Text,
                                                                ToolTip = ribbonButton.ToolTip.ToString(),
                                                                Click = ribbonButton.Clicked,
                                                                Size = buttonSize
                                                            }) as IButtonViewContainer;

                if (vc != null)
                {
                    ribbonButton.PropertyChanged += (s, e) =>
                        {
                            switch (e.PropertyName)
                            {
                                case "IsEnabled":
                                    vc.Enabled = ribbonButton.IsEnabled;
                                    break;
                            }
                        };
                }
            }
            else
            {
                var ribbonButtonGroup = item as RibbonButtonGroup;
                if (ribbonButtonGroup != null)
                {
                    foreach (var btn in ribbonButtonGroup.Buttons)
                    {
                        ((IControlParadigmRegistrator)this).AddRibbonItem(tab, group, btn);
                    }
                }
                else
                {
                    var wrapperPannelRibbonItem = item as WrapperPannelRibbonItem;
                    if (wrapperPannelRibbonItem != null)
                    {
                        foreach (var btn in wrapperPannelRibbonItem.Items)
                        {
                            ((IControlParadigmRegistrator)this).AddRibbonItem(tab, group, btn);
                        }
                    }
                    else
                        System.Diagnostics.Trace.WriteLine(string.Format("{0} @'{1}':'{2}'", item.GetType().FullName, tab, group));
                }
            }
        }


        void IControlParadigmRegistrator.AddMenuItem(RibbonButton ribbonButton)
        {
            RegisterWidgetFactory(ribbonButton.Text, ChromeHelper.ButtonFactory);

            var buttonSize = ribbonButton.Size == RibbonButtonSize.Small ? ButtonSize.Small : ButtonSize.Large;
            var vc = ChromeManager.PlaceWidget(ribbonButton.Text, string.Empty, new InitialButtonParameters
                {
                    Enabled = ribbonButton.IsEnabled,
                    Image = ribbonButton.Icon.ToImageSource(),
                    Text = ribbonButton.Text,
                    ToolTip = ribbonButton.ToolTip.ToString(),
                    Click = ribbonButton.Clicked,
                    Size = buttonSize
                }) as IButtonViewContainer;

            if (vc != null)
            {
                ribbonButton.PropertyChanged += (s, e) =>
                    {
                        switch (e.PropertyName)
                        {
                            case "IsEnabled":
                                vc.Enabled = ribbonButton.IsEnabled;
                                break;
                        }
                    };
            }
        }

        void IControlParadigmRegistrator.Register(string key, CreateControlHandler createControlHandler)
        {
            RegisterWidgetFactory(key, (wvc, state) =>
                {
                    if (wvc.Content != null) return true;
                    IWidget w = new Widget(wvc.Parent);
                    createControlHandler(w);
                     
                    if (w.State == WidgetState.Minimized)
                        wvc.Content = w.MinimizedView ?? w.MaximizedView;
                    else
                        wvc.Content = w.MaximizedView ?? w.MinimizedView;

                    return true;
                });
        }

        internal void RegisterDropdown(string key, CreateControlHandler createControlHandler)
        {
            WidgetFactories.Add(key, new WidgetFactoryHolder()
                {
                     ViewContainerType = typeof (RibbonDropdownArea)
                }.SetInitHandler(
                (wvc, state) =>
                    {
                        var w = new Widget(wvc.Parent);
                        createControlHandler(w);
                        if (w.State == WidgetState.Minimized)
                            wvc.Content = w.MinimizedView ?? w.MaximizedView;
                        else
                            wvc.Content = w.MaximizedView ?? w.MinimizedView;

                        return true;
                    }) as WidgetFactoryHolder); 
             
        }

        void IControlParadigmRegistrator.Register(string key, MSGuiButton button)
        {
            //System.Diagnostics.Debugger.Launch();
            RegisterWidgetFactory(key, (wvc, state) =>
                {
                    (wvc as WidgetViewContainer).Parameters = new InitialButtonParameters()
                        {
                            Enabled = button.IsEnabled,
                            Image = button.Image,
                            Text = button.Text,
                            ToolTip = button.ToolTip,
                            Click = button.OnClick,
                            Size = button.ButtonSize
                        };

                    return ChromeHelper.ButtonFactory(wvc, state);
                });
        }

        void IControlParadigmRegistrator.Register(string key, System.Windows.UIElement control)
        {
            RegisterWidgetFactory(key, (wvc, state) =>
                {
                    wvc.Content = control;
                    return true;
                });
        }

        bool IControlParadigmRegistrator.IsRegistered(string key)
        {
            return WidgetFactories.ContainsKey(key);
        }

        public static string CreateCommandBarKey(string controlRegKey, string key)
        {
            return string.Format("{{{0}}}/{1}", controlRegKey, key);
        }

        #endregion

        bool IChromeElementInitializerRegistry.TryGetWindowFactoryHolder(string factoryId, out WindowFactoryHolder factory)
        {
            return _windowFactories.TryGetValue(factoryId, out factory);
        }

        bool IChromeElementInitializerRegistry.TryGetWindowFactoryHolder(Type t, out WindowFactoryHolder factory)
        {
            if (_windowRemaping.ContainsKey(t))
            {
                t = _windowRemaping[t];
            }
            if (_windowFactoryMapping.TryGetValue(t, out factory)) return true;
            InitializeWindowHandler initHandler;
            if (!_windowInitMapping.TryGetValue(t, out initHandler)) return false;
            DehydrateWindowHandler dehydrateHandler;
            _windowDehydrateMapping.TryGetValue(t, out dehydrateHandler);

            factory =
                new WindowFactoryHolder().SetInitHandler(initHandler).SetDehydrateHandler(dehydrateHandler) as
                WindowFactoryHolder;
            var initMethod = initHandler.GetInvocationList();
            if (initMethod.Length == 1)
            {
                var attr = Attribute.GetCustomAttribute(
                    initHandler.Method,
                    typeof(FactoryOptionsAttribute),
                    true) as FactoryOptionsAttribute;
                if (attr != null)
                {
                    factory.ViewType = attr.ViewContainerType;
                }
            }
            _windowFactoryMapping[t] = factory;
            return true;
        }

        bool IChromeElementInitializerRegistry.TryGetWidgetFactoryHolder(Type t, out WidgetFactoryHolder factory)
        {
            return _widgetFactoryMapping.TryGetValue(t, out factory);
        }

        bool IChromeElementInitializerRegistry.TryGetWidgetFactoryHolder(string factoryId,
                                                                          out WidgetFactoryHolder factory)
        {
            return WidgetFactories.TryGetValue(factoryId, out factory);

        }
         
    }
}
