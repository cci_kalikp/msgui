﻿using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ControlParadigm
{
    internal class CommandArea : ICommandArea
    {
        protected ChromeManagerBase m_chromeManager;
        protected string m_root;
        protected string m_location;

        internal CommandArea(ChromeManagerBase chromeManager, string root, string location)
        {
            m_chromeManager = chromeManager;
            m_root = root;
            m_location = location;
        }

        #region ICommandArea Members

        public void AddItem(string itemId)
        {
            AddItem(itemId, -1);
        }

        public void AddItem(string itemId, int index)
        {
            AddItem(itemId, index, new InitialViewSettings());
        }

        public void AddItem(string itemId, InitialViewSettings ivs)
        {
            AddItem(itemId, -1, ivs);
        }

        public void AddItem(string itemId, int index, InitialViewSettings ivs)
        {
            this.AddItemCore(itemId, index, ivs);
            if (!ConcordExtensionStorage.ConcordInitialViewSettings.ContainsKey(itemId))
            {
                ConcordExtensionStorage.ConcordInitialViewSettings.Add(itemId, ivs);
            }
        }

        protected virtual void AddItemCore(string itemId_, int index_, InitialViewSettings ivs_)
        {
            var path = this.m_location.Split('/', '\\');
            if (path.Count() >= 2)
            {
                if (m_chromeManager.m_tabOrder.ContainsKey(path[0]) &&
                    m_chromeManager.m_tabOrder[path[0]].ContainsKey(path[1]))
                {
                    m_chromeManager.m_tabOrder[path[0]][path[1]].Add(itemId_);
                }
            }

            //TODO: FARM Add IVS support to ControlParadigm wrapper
            //TODO: done, but this may not be the way to do it.
            var root = m_chromeManager[m_root];
            foreach (var step in path)
            {
                if (root != null && !string.IsNullOrEmpty(step))
                    root = root[step];
            }

            var widget = m_chromeManager.GetWidget(itemId_);

            if (widget != null && root != null)
            {
                m_chromeManager.AddWidget(itemId_, null, root);
            }
            else
            {
                m_chromeManager.PlaceWidget(itemId_, this.m_root, this.m_location, null);
            }
        }

        public void RemoveItem(object item)
        {
            if (item != null)
            {
                RemoveItem(item.ToString());
            }
        }

        public void RemoveItem(string item)
        {
            var widget = m_chromeManager[this.m_root];
            var path = m_location.Split('\\', '/');

            foreach (var step in path)
            {
                if (widget == null)
                    return;

                if (step != string.Empty && widget.Contains(step))
                    widget = widget[step];
            }

            if (widget.Contains(item))
            {
                widget.RemoveWidget(item);
            }
        }

        //TODO Implement Clear on CP CommandArea wrapper
        public void Clear()
        {
            if (!m_chromeManager.CreateChromeCalled && this.m_root == "ribbon" && this.m_location == "Main/Tools")
            {
                /* 
				 * When someone clears the Tibbon/Main/Tools it's likely to remove the implicitly placed stuff. Since
				 * we have not placed it as of now, let's disable the implicit placement.
				 */
                ChromeManagerBase.MainToolsCleared = true;
            }


            var widget = m_chromeManager[this.m_root];

            foreach (var s in m_location.Split('/', '\\'))
            {
                if (widget == null)
                    break;

                widget = widget[s];
            }

            if (widget != null && m_chromeManager.Registrations.ContainsKey(widget))
                m_chromeManager.Registrations[widget].Clear();
        }

        #endregion
    }
}
