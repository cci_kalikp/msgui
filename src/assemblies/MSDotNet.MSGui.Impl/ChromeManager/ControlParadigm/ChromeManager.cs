﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ControlParadigm;
using MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea;
using System.Diagnostics;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
    partial class ChromeManagerBase : IControlParadigmImplementator, IHostRegistry
    {
        protected Dictionary<string, ICommandArea> m_commandAreas = new Dictionary<string, ICommandArea>();
        #region IControlParadigmImplementator Members

        public void SetOnClosingAction(Action OnClosing)
        {
            this._shell.OnClosed = OnClosing;
        }

        public void PlaceAllControls()
        {
            this.CreateChrome();
        }

        public void SetRibbonIcon(Icon icon)
        {
            SetRibbonIcon(icon.ToImageSource());
        }

        public virtual void SetRibbonIcon(ImageSource icon)
        {
            // Implemented in RibbonChromeManager below.
        }

        ICommandArea IControlParadigmImplementator.this[string id_]
        {
            get
            {
                if (id_ == Core.ChromeManager.Helpers.ChromeArea.QAT)
                {
                    //System.Diagnostics.Debugger.Break();
                    return new CommandArea(this, RibbonChromeManager.QAT_COMMANDAREA_NAME, "");
                }
                else if (id_.ToLower() == "menu")
                {
                    //System.Diagnostics.Debugger.Break();
                    return new CommandArea(this, MENU_COMMANDAREA_NAME, "");
                }
                else
                {
                    var wR = this.SplitHostId(id_);
                    if (wR != null)
                    {
                        var path = wR.Location.Split('/', '\\');
                        if (path.Count() >= 2)
                        {
                            if (!this.m_tabOrder.ContainsKey(path[0]))
                            {
                                this.m_tabOrder.Add(path[0], new Dictionary<string, List<string>>());
                            }

                            if (!this.m_tabOrder[path[0]].ContainsKey(path[1]))
                            {
                                this.m_tabOrder[path[0]].Add(path[1], new List<string>());
                            }
                        }

                        return new CommandArea(this, wR.Root, wR.Location);
                    }
                    else
                    {
                        //System.Diagnostics.Debugger.Break();
                        return new CommandArea(this, "", "");
                    }
                }
            }
        }

        #endregion


        protected string GetUnusedId(string suggestedId_)
        {
            return GetUnusedId(suggestedId_, x => m_commandAreas.ContainsKey(x));
        }

        protected static string GetUnusedId(string suggestedId_, Predicate<string> predicate_)
        {
            string id = suggestedId_;
            while (predicate_(id))
            {
                id += "_";
            }
            return id;
        }

        protected virtual string GetDefaultLocation()
        {
            return "";
        }

        #region IHostRegistry implementation

        /// <summary>
        /// ChromeRegistry's IControlParadigmRegistrator implementation will write into this.
        /// </summary>
        internal Dictionary<string, WidgetRegistration> ControlParadigmPlacements = new Dictionary<string, WidgetRegistration>();

        protected virtual WidgetRegistration SplitHostId(string hostID_)
        {
            return null;
        }

        #region IHostRegistry Members

        //Dictionary<string, ChromeAreaBase> legacyAreas = new Dictionary<string, ChromeAreaBase>();
        protected Dictionary<string, string> legacyAreaMapping = new Dictionary<string, string>();

        string IHostRegistry.AddHost(IButtonsHost host_, string suggestedId_)
        {
            return GetUnusedId(suggestedId_, x => m_commandAreas.ContainsKey(x));
            //if (key != string.Empty)
            //{
            //    var location = key.Split('/');
            //    if (location.Length > 0)
            //    {
            //        //this.chromeAreas[location[0]].GetArea(key.Substring(key.IndexOf('/')));
            //    }
            //}
            //return key;
        }

        public string AddRootHost(string name_)
        {
            string name = this.GetUnusedId(name_);
            //this.legacyAreas.Add(name_, AddRootHostCore(name_));
            return name;
        }

        public string AddMenuRootHost(string name_)
        {
            string name = this.GetUnusedId(name_);
            //this.legacyAreas.Add(name_, AddMenuRootHostCore(name));
            return name_;
        }

        internal virtual void AddMenuRootHostCore(string name_)
        {
            //return null;
        }

        internal virtual void AddRootHostCore(string name_)
        {
            //return null;
        }

        internal Dictionary<string, Dictionary<string, List<string>>> m_tabOrder = new Dictionary<string, Dictionary<string, List<string>>>();

        public void AddRecommendation(string childId_, string hostId_, InitialWidgetParameters parameters=null)
        {
            if (hostId_.StartsWith("{"))
            {
                if (!this.Registry.WidgetFactories.ContainsKey(childId_))
                {
                    this.Registry.RegisterWidgetFactory(childId_);
                }

                #region CM20
                //TODO: FARM	CM20 needs testing - host may not exist at this time

                var targetID = hostId_.Substring(1, hostId_.IndexOf('}') - 1);
                var host = this.GetWidget(targetID);

                //if (host != null )
                //{
                this.AddWidget(childId_, null, host, true);
                //}

                #endregion
                //this.AddWidgetRegistration(new WidgetRegistration()
                //{
                //    FactoryID = childId_,
                //    Location = hostId_,
                //    Root = null,
                //    ViewContainer = new WidgetViewContainer(childId_),
                //    Disabled = false
                //});

            }
            else if (this.legacyAreaMapping.ContainsKey(hostId_))
            //else if (this.legacyAreas.ContainsKey(hostId_))
            {
                bool isNew = false;
                if (!this.Registry.WidgetFactories.ContainsKey(childId_))
                {
                    this.Registry.RegisterWidgetFactory(childId_);
                    isNew = true;
                }

                //var root = from ca in this.chromeAreas.Keys
                //           where chromeAreas[ca] == this.defaultChromeArea
                //           select ca;

                #region CM20
                //TODO: FARM	CM20 needs testing - host may not exist at this time

                IWidgetViewContainer host = this.DefaultChromeArea;
                var route = this.legacyAreaMapping[hostId_].Split('/');
                foreach (var step in route)
                {
                    if (host == null)
                        break;

                    if (step != String.Empty)
                    {
                        host = host[step];
                    }
                }

                if (host != null)
                {
                    this.AddWidget(childId_, parameters, host, true);
                }

                #endregion

                //this.AddWidgetRegistration(new WidgetRegistration()
                //{
                //    FactoryID = childId_,
                //    Location = this.legacyAreaMapping[hostId_],
                //    Root = this.defaultChromeArea.ID, //root.First(),
                //    ViewContainer = new WidgetViewContainer(childId_),
                //    Disabled = isNew
                //});
            }
            else
            {
                //System.Diagnostics.Debugger.Launch();
                WidgetRegistration widgetRegistration = this.SplitHostId(hostId_);

                #region CM20
                //TODO: FARM	CM20 needs testing - host may not exist at this time

                IWidgetViewContainer host = this[widgetRegistration.Root];
                if (!this.legacyAreaMapping.ContainsKey(widgetRegistration.Location))
                {
                    this.legacyAreaMapping[hostId_] = widgetRegistration.Location;
                }

                var route = this.legacyAreaMapping[hostId_].Split('/');
                foreach (var step in route)
                {
                    if (host == null)
                        break;

                    if (step != String.Empty)
                    {
                        host = host[step];
                    }
                }

                if (host != null)
                {
                    // last parameter: if there is a registration like this, disable implicit creation, we want to use the registered factory
                    this.AddWidget(childId_, parameters, host, this.Registry.WidgetFactories.ContainsKey(childId_));
                }

                #endregion


                //if (widgetRegistration != null)
                //{
                //    widgetRegistration.FactoryID = childId_;
                //    this.AddWidgetRegistration(widgetRegistration);
                //}
            }
        }

        public void RemoveRecommendation(string childId_)
        {
            var widget = this.GetWidget(childId_);
            if (widget != null)
            {
                this.MoveWidget(widget, this.GetWidget(LOCKER_COMMANDAREA_NAME), null);
            }

            //var reg = this.widgetRegistrations.FirstOrDefault(wr => wr.FactoryID == childId_);
            //if (reg != null)
            //{
            //    reg.Disabled = true;
            //}
            //this.widgetRegistrations.RemoveAll((wr) => { return wr.FactoryID == childId_; });
        }

        //TODO: FARM this needs testing
        public void ClearRecommendationsForHost(string hostId_)
        {
            WidgetRegistration widgetRegistration = this.SplitHostId(hostId_);

            var widget = this[widgetRegistration.Root];

            foreach (var s in widgetRegistration.Location.Split('/', '\\'))
            {
                widget = widget[s];
            }

            this.Registrations[widget].Clear();
            //commented this as the above "this.registrations[widget].Clear();" handles this in CM2
            //this.widgetRegistrations.RemoveAll((wr) => { return wr.Root == widgetRegistration.Root && wr.Location == widgetRegistration.Location; });
        }

        #endregion

        #endregion
    }

    public partial class RibbonChromeManager
    {
        protected override string GetDefaultLocation()
        {
            return @"Ribbon\Main\Tools";
        }
          
        internal override IWidgetViewContainer GetRootContainer()
        {
            return Ribbon as IWidgetViewContainer;
        }

        protected override WidgetRegistration SplitHostId(string hostID_)
        {
            if (hostID_.StartsWith("Ribbon/") || hostID_.StartsWith(@"Ribbon\"))
            {
                return new WidgetRegistration()
                {
                    Root = RIBBON_COMMANDAREA_NAME,
                    Location = hostID_.Substring(7)
                };
            }
            else if (hostID_.Trim() != string.Empty)
            {
                return new WidgetRegistration()
                {
                    Root = RIBBON_COMMANDAREA_NAME,
                    Location = (hostID_.Contains(@"\") || hostID_.Contains("/"))
                                   ? hostID_
                                   : hostID_ + "/Tools"
                };
            }

            return null;
        }

        internal override void AddMenuRootHostCore(string name_)
        {
            //FARM: TODO
            this.legacyAreaMapping.Add(name_, "Main/Tools/" + name_);
            //return null; (this.defaultChromeArea as RibbonRootArea).GetAreaRecursively("Main/Tools/" + name_);
        }

        internal override void AddRootHostCore(string name_)
        {
            //FARM: TODO
            this.legacyAreaMapping.Add(name_, "Main/" + name_);
            //return null; (this.defaultChromeArea as RibbonRootArea).GetArea("Main/" + name_);
        }
    }

    public partial class LauncherBarChromeManager
    {
        protected override string GetDefaultLocation()
        {
            return LAUNCHERBAR_COMMANDAREA_NAME;
        }

        protected override WidgetRegistration SplitHostId(string hostID_)
        {
            if (hostID_.ToLower().StartsWith("launcherbar/") || hostID_.ToLower().StartsWith(@"launcherbar\"))
            {
                return new WidgetRegistration()
                {
                    Root = LAUNCHERBAR_COMMANDAREA_NAME,
                    Location = hostID_.Substring(7)
                };
            }
            
            //if (hostID_.Trim() != string.Empty)
            {
                return new WidgetRegistration()
                {
                    Root = LAUNCHERBAR_COMMANDAREA_NAME,
                    Location = hostID_ // launcherbar supports but doesn't require grouping, let's not enforce it (see RibbonChromeManager's same override)
                };
            }

            return null;
        }

        internal override IWidgetViewContainer GetRootContainer()
        {
            return LauncherBar;
        }
    }
}
