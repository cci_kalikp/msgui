﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infragistics.Windows.Ribbon;
using System.Windows;
using System.Reflection;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Windows.Threading;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ControlParadigm
{
	internal class QATCommandArea : CommandArea
	{
		public QATCommandArea(ChromeManagerBase chromeManager_, string root_, string location_)
			: base(chromeManager_, root_, location_)
		{
			
		}
	
		
		protected override void AddItemCore(string itemId_, int index_, Core.InitialViewSettings ivs_)
		{
			var cM = this.m_chromeManager as RibbonChromeManager;
			if (cM != null) cM.QATPlacements.Add(itemId_, index_);
		}
	}
}
