﻿using System;
using System.Collections.Generic;
using MorganStanley.MSDotNet.MSGui.Core;
using Fluent = MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.StatusBar;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Statusbar
{
	internal partial class StatusbarRootArea : IStatusBar
	{
		#region IStatusBar Members

        private List<string> autoRegisteredFactories = new List<string>();

		ICollection<string> IStatusBar.Keys
		{
			get 
			{
				var keys = new List<string>();
				foreach (var widget in m_children)
				{
					keys.Add(widget.Key);
				}
				return keys;
			}
		}

		IStatusBarItem IStatusBar.this[string statusBarItemKey]
		{
			get 
			{
				IWidgetViewContainer wvc = null;
				if (this.m_children.ContainsKey(statusBarItemKey))
				{
					wvc = this.m_children[statusBarItemKey];
				}
				else
				{
					foreach (var child in this.m_children)
					{
						if ((child.Value as WidgetViewContainer).Contains(statusBarItemKey))
						{
							wvc = child.Value[statusBarItemKey];
						}
					}
				}

				return (wvc == null) 
					? null 
					: wvc.Content as IStatusBarItem;
			}
		}

		IStatusBarItem IStatusBar.MainItem
		{
			get 
			{
				return (this as IStatusBar)[MAIN_STATUS_ITEM_NAME];
			}
		}

		void IStatusBar.AddItem(string key_, IStatusBarItem item_, StatusBarItemAlignment alignment_)
		{
		    if (!chromeManager.Registry.WidgetFactories.ContainsKey(key_))
		    {
		        chromeManager.Registry.RegisterWidgetFactory(key_, this)
                    .SetInitHandler((vc, s) =>
		                {
		                    vc.Content = item_;
		                    return true;
		                });
		    }
		    var side = alignment_ == StatusBarItemAlignment.Left
						   ? Fluent.Extensions.Statusbar(chromeManager).LeftSide
						   : Fluent.Extensions.Statusbar(chromeManager).RightSide;
            side.AddWidget(key_, new InitialWidgetParameters());
            m_children.Add(key_, new WidgetViewContainer(key_) { Content = new StatusbarDummyItem() });
		}

		string IStatusBar.AddItem(IStatusBarItem item, StatusBarItemAlignment alignment)
		{
			string key = Guid.NewGuid().ToString("N");
			(this as IStatusBar).AddItem(key, item, alignment);
			return key;
		}

		void IStatusBar.AddTextStatusItem(string key, StatusBarItemAlignment alignment)
		{
            if (!autoRegisteredFactories.Contains(key))
            {
                autoRegisteredFactories.Add(key);
                chromeManager.Registry.RegisterWidgetFactory(key, chromeManager.LabelFactory());
            }

            if (!ChromeManagerBase.Instance.CreateChromeCalled)
            {
                var p = (alignment == StatusBarItemAlignment.Left ? this.LeftSide : this.RightSide) as StatusbarRegionArea;
                p.AddDummy(key, new WidgetViewContainer(key) { Content = new StatusbarDummyItem() });
            }

			chromeManager.PlaceWidget(key, ChromeManagerBase.STATUSBAR_COMMANDAREA_NAME, alignment == StatusBarItemAlignment.Left ? "left" : "right", new InitialStatusLabelParameters());
		}

		string IStatusBar.AddTextStatusItem(StatusBarItemAlignment alignment)
		{
			var guid = Guid.NewGuid().ToString();
			chromeManager.Registry.RegisterWidgetFactory(guid, chromeManager.LabelFactory());

			var p = (alignment == StatusBarItemAlignment.Left ? this.LeftSide : this.RightSide) as StatusbarRegionArea;
			p.AddDummy(guid, new WidgetViewContainer(guid) { Content = new StatusbarDummyItem() });
			
			chromeManager.PlaceWidget(guid, ChromeManagerBase.STATUSBAR_COMMANDAREA_NAME, alignment == StatusBarItemAlignment.Left ? "left" : "right", new InitialStatusLabelParameters());
			return guid;
		}

		void IStatusBar.AddProgressBarStatusItem(string key, double @from, double to, StatusBarItemAlignment alignment)
		{
            if (!autoRegisteredFactories.Contains(key))
            {
                autoRegisteredFactories.Add(key);
                chromeManager.Registry.RegisterWidgetFactory(key, chromeManager.ProgressBarFactory());
            }

            chromeManager.PlaceWidget(key, ChromeManagerBase.STATUSBAR_COMMANDAREA_NAME, alignment == StatusBarItemAlignment.Left ? "left" : "right", new InitialProgressBarParameters()
			{
				MinValue = @from,
				MaxValue = to
			});
		}

		string IStatusBar.AddProgressBarStatusItem(double @from, double to, StatusBarItemAlignment alignment)
		{
			var guid = Guid.NewGuid().ToString();
			chromeManager.Registry.RegisterWidgetFactory(guid, chromeManager.ProgressBarFactory());
			chromeManager.PlaceWidget(guid, ChromeManagerBase.STATUSBAR_COMMANDAREA_NAME, alignment == StatusBarItemAlignment.Left ? "left" : "right", new InitialProgressBarParameters()
				{
					MinValue = @from,
					MaxValue = to					
				});
			return guid;
		}

		void IStatusBar.AddSeparator(string key, StatusBarItemAlignment alignment)
		{
            if (!autoRegisteredFactories.Contains(key))
            {
                autoRegisteredFactories.Add(key);
                chromeManager.Registry.RegisterWidgetFactory(key, chromeManager.SeparatorFactory());
            }

            chromeManager.PlaceWidget(key, ChromeManagerBase.STATUSBAR_COMMANDAREA_NAME, alignment == StatusBarItemAlignment.Left ? "left" : "right", new InitialWidgetParameters());
		}

		string IStatusBar.AddSeparator(StatusBarItemAlignment alignment)
		{
			var guid = Guid.NewGuid().ToString();
			chromeManager.Registry.RegisterWidgetFactory(guid, chromeManager.SeparatorFactory());
			chromeManager.PlaceWidget(guid, ChromeManagerBase.STATUSBAR_COMMANDAREA_NAME, alignment == StatusBarItemAlignment.Left ? "left" : "right", new InitialWidgetParameters());
			return guid;
		}

		void IStatusBar.RemoveItem(string key)
		{
			chromeManager.RemoveWidget(key);
			m_children.Remove(key);
		}

		bool IStatusBar.ContainsItem(string key)
		{
			return this.Contains(key);
		}

		#endregion
	}
}
