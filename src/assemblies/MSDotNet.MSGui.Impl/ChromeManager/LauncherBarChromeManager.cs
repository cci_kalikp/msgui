﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media; 
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.LauncherBar;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.TabWell;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
	public partial class LauncherBarChromeManager : ChromeManagerBase
	{
	    public const string WINDOW_TITLE_BAR = "titlebar";

        private readonly ShellWithLauncher _shell;
        internal BarItemsHolder _barItemsHolder;

		internal LauncherBarChromeManager(ChromeRegistry chromeRegistry, IShell shell, PersistenceProfileService persistenceProfileService, PersistenceService persistenceService, IUnityContainer container, IWindowFactory windowFactory) :
			base(chromeRegistry, shell, persistenceProfileService, persistenceService, container, windowFactory)
		{
			_shell = shell as ShellWithLauncher;
			_barItemsHolder = _shell.Holder;
		    _shell.m_launcherExpandableBar.BarStateChanged += (s, e) =>
		        {
		            var oldValue = _shell.m_launcherExpandableBar.BarState == LauncherBarState.Expanded
		                               ? LauncherBarState.Shrinked
		                               : LauncherBarState.Expanded;
		            OnPropertyChanged(new DependencyPropertyChangedEventArgs(LauncherBarStateProperty, oldValue,
		                                                                     this._shell.m_launcherExpandableBar.BarState));
		        };

			//this.defaultChromeArea = new LauncherBarRootArea(this.m_barItemsHolder, this);
			//this.chromeAreas.Add(LAUNCHERBAR_COMMANDAREA_NAME, this.defaultChromeArea);
			AfterCreateChrome += LauncherBarChromeManager_AfterCreateChrome;

			LauncherBarFactories.Initialize(this, shell);
		    var settings = container.Resolve<InitialSettings>();
		    if (settings.ShellMode == ShellMode.LauncherBarAndWindow)
		    {
		        TabWellFactories.Initialize(this);
		    }
		    this.AddRootArea(LAUNCHERBAR_COMMANDAREA_NAME, new InitialLauncherBarParameters());
			this.DefaultChromeArea = this[LAUNCHERBAR_COMMANDAREA_NAME] as WidgetViewContainer;
		    if (settings.ShellMode == ShellMode.LauncherBarAndWindow)
		    {
		        this.AddRootArea(Core.ChromeManager.Helpers.ChromeArea.TabWell, new InitialTabWellRootParameters());
		    }
		    this.AddRootArea(WINDOW_TITLE_BAR, new InitialTitleBarRootParameters());

		    if (settings.ShellMode == ShellMode.LauncherBarAndWindow)
		    {
		        this[Core.ChromeManager.Helpers.ChromeArea.TabWell].AddWidget(TabWellRootArea.TabWellLeftEdgeID,
		                                                                      new InitialTabWellEdgeParameters()
		                                                                          {
		                                                                              TabControl =
		                                                                                  LogicalTreeHelper.FindLogicalNode(
		                                                                                      this._shell.MainWindow,
                                                                                              "tabControl") as TabControl,
		                                                                              Side = HorizontalAlignment.Left
		                                                                          });
		        this[Core.ChromeManager.Helpers.ChromeArea.TabWell].AddWidget(TabWellRootArea.TabWellRightEdgeID,
		                                                                      new InitialTabWellEdgeParameters()
		                                                                          {
		                                                                              TabControl =
		                                                                                  LogicalTreeHelper.FindLogicalNode(
		                                                                                      this._shell.MainWindow,
                                                                                              "tabControl") as TabControl,
		                                                                              Side = HorizontalAlignment.Right
		                                                                          });

		        var tdvm = _shell.WindowManager as TabbedDockViewModel;
		        if (tdvm != null)
		        {
		            (this[Core.ChromeManager.Helpers.ChromeArea.TabWell] as TabWellRootArea).DockViewModel = tdvm;
		        }
		    }

		}

		void LauncherBarChromeManager_AfterCreateChrome(object sender, EventArgs e)
		{
			ChromeManagerBase.StartupCompleted = true;
		}

        public override void SetRibbonIcon(ImageSource icon)
        {
            this._shell.m_optionsButton.Image.Source = icon;
        } 

	    #region IChromeManager members

		public override IWidgetViewContainer PlaceWidget(string factoryId, string location, InitialWidgetParameters initialParameters)
		{
			return this.PlaceWidget(factoryId, null, location, initialParameters);
		}

		//protected override WidgetRegistration RegisterWidgetPlacement(string factoryID, string root, string location, InitialWidgetParameters initialParameters)
		//{
		//    if (root == null || root.Trim() == "" || root.ToLower() == LAUNCHERBAR_COMMANDAREA_NAME)
		//    {
		//        var widgetRegistration = new WidgetRegistration() 
		//        { 
		//            FactoryID = factoryID, 
		//            Root = LAUNCHERBAR_COMMANDAREA_NAME, 
		//            Location = location, 
		//            InitialParameters = initialParameters,
		//            ViewContainer = CreateViewContainer(factoryID, initialParameters)
		//        };
		//        this.AddWidgetRegistration(widgetRegistration);
		//        return widgetRegistration;
		//    }
		//    else
		//        return base.RegisterWidgetPlacement(factoryID, root, location, initialParameters);
		//}
		
		#endregion

		public const string LAUNCHERBAR_COMMANDAREA_NAME = "launcherbar";

		public LauncherBarState State
		{
			get
			{
				return this._shell.m_launcherExpandableBar.BarState;
			}
		}

		#region Attached dependency property & binding converters
		
		internal static DependencyPropertyKey LauncherBarStatePropertyKey = DependencyProperty.RegisterAttachedReadOnly
		(
			"LauncherBarState",
			typeof(LauncherBarState), 
			typeof(LauncherBarChromeManager), 
			new PropertyMetadata(LauncherBarState.Shrinked)
		);
		
		public static DependencyProperty LauncherBarStateProperty
		{
			get
			{
				return LauncherBarStatePropertyKey.DependencyProperty;
			}
		}

		public static void SetLauncherBarState(UIElement element, LauncherBarState value)
		{
			element.SetValue(LauncherBarStatePropertyKey, value);
		}

		public static LauncherBarState GetLauncherBarState(UIElement element)
		{
			return (LauncherBarState)element.GetValue(LauncherBarStateProperty);
		}

		#endregion


		#region ChromeManager 20

		protected override string GetDefaultArea()
		{
			return LAUNCHERBAR_COMMANDAREA_NAME;
		}

		#endregion

	}

}
