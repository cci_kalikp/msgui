﻿using System;
using System.ComponentModel;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
	public class ViewContainerBase : IViewContainerBase, INotifyPropertyChanged
	{
		#region IViewContainerBase Members

		public InitialViewParameters Parameters { get; protected set; }
		public string FactoryID { get; protected set; }

		#endregion

		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;
		protected void OnPropertyChanged(string name_)
		{
			PropertyChangedEventHandler copy = PropertyChanged;
			if (copy != null)
			{
				copy(this, new PropertyChangedEventArgs(name_));
			}
		}

		#endregion

        #region  For View to view communication registration clean up


	    internal event EventHandler<EventArgs> ReleaseResources;
        protected void InvokeReleaseRsource()
	    {

            EventHandler<EventArgs> clean = ReleaseResources;
            if (clean != null)
            {
                clean(this, new EventArgs());
            }
	       
	    }

	    #endregion
	}
}
