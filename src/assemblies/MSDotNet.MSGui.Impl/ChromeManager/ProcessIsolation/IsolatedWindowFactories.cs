﻿using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ProcessIsolation
{
    internal static class IsolatedWindowFactories
    {
        internal const string IsolatedProcessWindowFactoryName = "IsolatedWindowFactory";

        internal static void Initialize(IChromeRegistry chromeRegistry)
        {
            var stateHolder = new OriginalStateHolder();

            chromeRegistry.RegisterWindowFactory(
                IsolatedProcessWindowFactoryName,
                stateHolder.CreateIsolatedWindow,
                stateHolder.DehydrateWindow);
        }
    }
}
