﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ProcessIsolation
{
    internal sealed class OriginalStateHolder
    {
        private readonly IDictionary<string, XDocument> _originalStates = new ConcurrentDictionary<string, XDocument>();

        internal bool CreateIsolatedWindow(IWindowViewContainer container, XDocument state)
        {
            var windowViewContainer = (WindowViewContainer) container;
            var isolatedViewParameters = (InitialIsolatedViewParameters) windowViewContainer.Parameters;

            var viewId = isolatedViewParameters.ViewId;
            if (!string.IsNullOrEmpty(viewId))
            {
                windowViewContainer.ID = viewId;

                // We can have actual state either in State parameter or in InitialState on the parameters
                if (state != null)
                {
                    _originalStates[viewId] = state;
                }
                else if (isolatedViewParameters.State != null)
                {
                    _originalStates[viewId] = isolatedViewParameters.State;
                }
            }

            windowViewContainer.Content = isolatedViewParameters.IsolatedControlHost;

            return true;
        }

        internal XDocument DehydrateWindow(IWindowViewContainer windowViewContainer)
        {
            var host = windowViewContainer.Content as IDehydratableElement;
            XDocument state;
            if (host != null)
            {
                try
                {
                    state = host.Dehydrate();
                }
                catch (Exception)
                {
                    // Failed to dehydrate from isolated process. Roll back to the original state.
                    var viewId = ((InitialIsolatedViewParameters) windowViewContainer.Parameters).ViewId;
                    state = _originalStates[viewId];
                }
            }
            else
            {
                state = new XDocument();
            }
            return state;
        }
    }
}
