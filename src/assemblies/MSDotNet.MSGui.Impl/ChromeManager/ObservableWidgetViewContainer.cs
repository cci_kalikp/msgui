﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
    public abstract class ObservableWidgetViewContainer : WidgetViewContainer, IWidgetViewContainerConfigNode
    {
        protected ObservableWidgetViewContainer(string ID, InitialWidgetParameters parameters)
            : base(ID, parameters)
        {
            ObservableChildren = new ObservableCollection<IWidgetViewContainer>();
            m_currentName = parameters.Text;
        }

        public ObservableCollection<IWidgetViewContainer> ObservableChildren { get; private set; }

        protected override bool DoAddLogical(IWidgetViewContainer child)
        {
            if (base.DoAddLogical(child))
            {
                if (DoCanAddwidget(child))
                {
                    ObservableChildren.Add(child);
                }
                return true;
            }
            return false;
        }

        protected override void DoRemoveLogical(IWidgetViewContainer child)
        {
            base.DoRemoveLogical(child);
            if (DoCanAddwidget(child))
            {
                ObservableChildren.Remove(child);
            }
        }

        public virtual void SwapChildren(IWidgetViewContainer a_, IWidgetViewContainer b_)
        {
            var indexA = ObservableChildren.IndexOf(a_);
            var indexB = ObservableChildren.IndexOf(b_);
            ObservableChildren[indexA] = b_;
            ObservableChildren[indexB] = a_;
        }

        public virtual void Rename(string newName)
        {
            CurrentName = newName;
        }

        private string m_currentName;
        public string CurrentName
        {
            get
            {
                return m_currentName;
            }
            private set
            {
                m_currentName = value;
                OnPropertyChanged("CurrentName");
            }
        }

        public virtual XElement SaveState()
        {
            var result = new XElement(NodeName);
            result.SetAttributeValue("name", FactoryID);
            result.SetAttributeValue("currentName", CurrentName);
            foreach (var child in ObservableChildren)
            {
                if (child is IWidgetViewContainerConfigNode)
                {                    
                    result.Add(((IWidgetViewContainerConfigNode)child).SaveState());
                }
                else
                {
                    var widgetNode = new XElement("Widget");
                    widgetNode.SetAttributeValue("name", child.FactoryID);
                    result.Add(widgetNode);
                }
            }
            return new XElement(result);
        }

        public virtual void LoadState(XElement state_)
        {
            if (state_.Name != NodeName) throw new ArgumentException();
            var lockerChildren = (chromeManager[Core.ChromeManager.Helpers.ChromeArea.Locker] as IWidgetViewContainerConfigNode).ObservableChildren;
            foreach (var childElement in state_.Elements())
            {
                if (childElement.Elements().Count() != 0)
                {
                    var currentName = childElement.Attribute("currentName").Value;
                    var parameters = AddDefault(ref currentName);
                    var widget = chromeManager.AddWidget(currentName, parameters, this);
                    (widget as IWidgetViewContainerConfigNode).LoadState(childElement);
                }
                else
                {
                    var factoryId = childElement.Attribute("name").Value;                
                    var widget = lockerChildren.FirstOrDefault(w_ => w_.FactoryID == factoryId);
                    if (widget != null)
                    {
                        chromeManager.MoveWidget(widget, this, null);
                    }
                    else if (IsAuxiliary)
                    {
                        this.AddWidget(factoryId, null);
                    }
                }
            }
        }

        public abstract string NodeName { get; }
    }
}
