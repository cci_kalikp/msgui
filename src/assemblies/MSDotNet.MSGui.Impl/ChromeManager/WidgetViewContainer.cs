﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
	/// <summary>
	/// Base class for widget view containers. 
	/// </summary>
	public class WidgetViewContainer : ViewContainerBase, IWidgetViewContainer
	{
	    readonly ObservableCollection<object> _visualChildren = new ObservableCollection<object>();
        private IWidgetViewContainer _parent;
        internal protected object m_content;
        protected readonly Dictionary<string, IWidgetViewContainer> m_children = new Dictionary<string, IWidgetViewContainer>();

        internal static ChromeManagerBase chromeManager;


        public WidgetViewContainer(string factoryID)
        {
            Parameters = new InitialWidgetParameters();
            FactoryID = factoryID;
            IsAuxiliary = false;
        }

        public WidgetViewContainer(InitialWidgetParameters parameters, string factoryID)
        {
            Parameters = parameters;
            FactoryID = factoryID;
            IsAuxiliary = false;
        }

        public WidgetViewContainer(string ID, InitialWidgetParameters parameters)
        {
            Parameters = parameters;
            FactoryID = ID;
            IsAuxiliary = false;
        }


		#region IWidgetViewContainer Members

		public new InitialWidgetParameters Parameters
		{
			get
			{
				return (InitialWidgetParameters)base.Parameters;
			}
			internal set
			{
				base.Parameters = value;
			}
		}

		public IWidgetViewContainer Parent
		{
			get
			{
				return _parent;
			}
			set
			{
				_parent = value;
			}
		}

		public virtual IWidgetViewContainer this[string index]
		{
		    get
		    {
		        if (Contains(index))
		        {
		            return GetWidget(index);
		        }

		        var vc = chromeManager.AddWidget(index, null, this);
		        return vc;
		    }
		}

        public IEnumerable<IWidgetViewContainer> Children
        {
            get
            {
                return m_children.Values;
            }
        }

		public bool Contains(string index)
		{
			return DoContains(index);
		}
		protected virtual bool DoContains(string index)
		{
			return m_children.ContainsKey(index);
		}

		public IWidgetViewContainer GetWidget(string index)
		{
			return DoGetWidget(index);
		}
		protected virtual IWidgetViewContainer DoGetWidget(string index)
		{
			return m_children[index];
		}

		public bool CanAddwidget(IWidgetViewContainer widget)
		{
			return DoCanAddwidget(widget) && widget != this;
		}
		protected virtual bool DoCanAddwidget(IWidgetViewContainer widget)
		{
			return true;
		}


		public object Content
		{
			get
			{
				return m_content;
			}
			set
			{
				m_content = value;

				var contentReadyHandler = this.ContentReady;
				if (contentReadyHandler != null)
				{
					contentReadyHandler(this, EventArgs.Empty);
				}
			}
		}

		public event EventHandler ContentReady;

		#endregion


		#region Add
		/// <summary>
		/// Adds an element as a logical child. This will not perform an update of the UI. 
		/// </summary>
		/// <param name="child"></param>
		public void AddLogical(IWidgetViewContainer child)
		{
			DoAddLogical(child);
		}

		/// <summary>
		/// Perform adding a child widget to the UI of the current widget. You can assume that the child has it's parent already set and it's UI created.
		/// </summary>
		/// <param name="child">The child to add.</param>
		/// <returns>True if succeeded.</returns>
		protected virtual bool DoAddLogical(IWidgetViewContainer child)
		{
			if (!this.m_children.ContainsKey(child.FactoryID))
			{
				this.m_children.Add(child.FactoryID, child);
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Adds a child to the widget. The child and it's UI are already cerated at this point.
		/// </summary>
		/// <param name="child">The new child</param>
		/// <returns>True if successful.</returns>
		public bool AddVisual(IWidgetViewContainer child)
		{
			if (child != null)
			{
				return DoAddVisual(child);
			}

			return false;
		}


        public void Insert(int index_, IWidgetViewContainer child)
        {
            DoAddLogical(child);
            DoInsertVisual(index_, child);
            ((WidgetViewContainer)child).Parent = this;
        }

	    protected virtual void DoInsertVisual(int index, IWidgetViewContainer child)
        {
            if (child.Content != null)
            {
                _visualChildren.Insert(index, child.Content);
            }
	    }

        public ReadOnlyObservableCollection<object> VisualChildren
        {
            get
            {
                return new ReadOnlyObservableCollection<object>(_visualChildren);
            }
        }
		/// <summary>
		/// Perform adding a child widget to the UI of the current widget. You can assume that the child has it's parent already set and it's UI created.
		/// Returning true will add the child to the m_children collection.
		/// </summary>
		/// <param name="child">The child to add.</param>
		/// <returns>True if succeeded.</returns>
		protected virtual bool DoAddVisual(IWidgetViewContainer child)
		{
            if (child.Content != null)
            {
                this._visualChildren.Add(child.Content);
                return true;
            }
		    return false;
		}
		
		#endregion

		#region Remove

		/// <summary>
		/// Removes a child from the widget without removing it from the UI.
		/// </summary>
		/// <param name="childID">The ID of the child to remove</param>
		public void RemoveLogical(string childID)
		{
			if (childID != null && this.m_children.ContainsKey(childID))
			{
				this.DoRemoveLogical(this.m_children[childID]);
			}
		}

		/// <summary>
		/// Removes a child from the widget without removing it from the UI.
		/// </summary>
		/// <param name="child">The child to remove</param>
		public void RemoveLogical(IWidgetViewContainer child)
		{
			if (child != null && this.m_children.ContainsKey(child.FactoryID))
			{
				this.DoRemoveLogical(child);
			}
		}

		/// <summary>
		/// Perform removing a child widget from the current widget. 
		/// 
		/// Warning: Do not remove from the UI here, do that in DoRemoveVisual instead! ChromeManager will keep these 
		/// in sync and use the separation for the locker and pre-CreateChrome registrations.
		/// </summary>
		/// <param name="child">The child to remove.</param>
		protected virtual void DoRemoveLogical(IWidgetViewContainer child)
		{
			this.m_children.Remove(child.FactoryID);
		}

		/// <summary>
		/// Removes a child from the widget's UI.
		/// </summary>
		/// <param name="childID">The ID of the child to remove</param>
		public void RemoveVisual(string childID)
		{
			if (childID != null && this.m_children.ContainsKey(childID))
			{
				this.DoRemoveVisual(this.m_children[childID]);
			}
		}

		/// <summary>
		/// Removes a child from the widget's UI.
		/// </summary>
		/// <param name="child">The child to remove</param>
		public void RemoveVisual(IWidgetViewContainer child)
		{
			if (child != null && this.m_children.ContainsKey(child.FactoryID))
			{
				this.DoRemoveVisual(child);
			}
		}

		/// <summary>
		/// Perform removing a child widget from the UI of the current widget.
		/// 
		/// Warning: Do not remove from the m_children collection here, do that in DoRemoveLogical instead! ChromeManager will keep these 
		/// in sync and use the separation for the locker and pre-CreateChrome registrations.
		/// </summary>
		/// <param name="child">The child to remove.</param>
		/// <returns>True if succeeded.</returns>
		protected virtual void DoRemoveVisual(IWidgetViewContainer child)
		{
            if (child.Content != null && this._visualChildren.Contains(child.Content))
            {
                this._visualChildren.Remove(child.Content);
            }
			return;
		}

		#endregion

		public bool IsAuxiliary { get; set; }

		public virtual InitialWidgetParameters AddDefault(ref string ID)
		{
			return DoAddDefault(ref ID);
		}

		/// <summary>
		/// Add an instance of the default child (for example a ribbon group into a tab). Used for implicit child 
		/// creation.
		/// </summary>
		/// <param name="ID">The ID of the new child.</param>
		/// <returns>The created parameters instance describing the child area.</returns>
		protected virtual InitialWidgetParameters DoAddDefault(ref string ID)
		{
			return null;
		}

		#region ChromeManager 10 compatibility
		public string PadLocationStringWithDefaults(string location)
		{
			return DoPadLocationStringWithDefaults(location);
		}

		/// <summary>
		/// Pads a location string with defaults if necessary. For example, "" => "Main/Tools" and "Foo" => "Main/Foo" 
		/// for the ribbon. This only has to be overridden in root areas.
		/// </summary>
		/// <param name="location">The string to pad if necessary.</param>
		/// <returns>The padded location string</returns>
		protected virtual string DoPadLocationStringWithDefaults(string location)
		{
			return location;
		}
		#endregion

		internal bool IsPlaced { get; set; }

		public override string ToString()
		{
			return GetType().Name + " : " + FactoryID;
		}
	}
}
