﻿using System; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{ 
    internal sealed class TileItemViewContainer : WindowViewContainer, ITileItemViewContainer 
    { 
        private double left;
        private double top; 
        
        public TileItemViewContainer(string factoryId_):base(factoryId_)
        {
             
        }

        public TileItemViewContainer(InitialTileItemParameters parameters_, string factoryId_):base(parameters_, factoryId_)
        { 
            if (!double.IsNaN(parameters_.Width))
            {
                Width = parameters_.Width;
            }
            if (!double.IsNaN(parameters_.Height))
            {
                Height = parameters_.Height;
            }
            this.RelativeLeft = parameters_.Left;
            this.RelativeTop = parameters_.Top;
        }
         
        public double RelativeLeft
        {
            get { return left; }
            set { left = value; OnPropertyChanged("RelativeLeft"); }
        }

        public double RelativeTop
        {
            get { return top; }
            set { top = value; OnPropertyChanged("RelativeTop"); }
        }

        public double? LastRightEdgeMargin { get; set; }

        public double? LastBottomEdgeMargin { get; set; }

        public new InitialTileItemParameters Parameters
        {
            get { return base.Parameters as InitialTileItemParameters; }
        }

        private IViewCollectionContainer parent;
        public IViewCollectionContainer Parent
        {
            get { return parent; }
            set
            {
                if (parent != value)
                {
                    parent = value;
                    var copy = ParentChanged;
                    if (copy != null)
                    {
                        copy(this, new WindowEventArgs(parent));
                    }
                    
                } 
            }
        }


        public event EventHandler<WindowEventArgs> ParentChanged;

    }
}
