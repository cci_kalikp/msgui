﻿using System; 
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized; 
using System.Linq; 
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using System.Windows.Media; 
using System.Windows.Threading;
using System.Xml;
using System.Xml.Linq; 
using Microsoft.Practices.Unity;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Bridges;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Locker;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Menu;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Statusbar;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.SystemTray;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Workspaces;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ProcessIsolation;
using MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence.LayoutLoadingScreen;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters; 
using MorganStanley.MSDotNet.MSGui.Impl.View;
using MorganStanley.MSDotNet.My;
using IStatusBar = MorganStanley.MSDotNet.MSGui.Core.IStatusBar;
using IWindowFactory = MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories.IWindowFactory;
using IWindowManager = MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.IWindowManager;
using NativeMethods = MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell.Standard.NativeMethods;
using StatusLevel = MorganStanley.MSDotNet.MSGui.Core.StatusLevel;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
    public static class ChromeManagerExtensions
    {
        public static void SetCABNavigationControl(this IChromeManager cm, Type t)
        {
            ChromeManagerBase.CABNavigationControl = t;
        }

        public static IWidgetViewContainer GetParentContainer(this IChromeManager chromeManager_, string groupName_, string subGroupName_)
        {
            ChromeManagerBase chromeManager = chromeManager_ as ChromeManagerBase;
            if (chromeManager != null)
            {
                if (string.IsNullOrEmpty(groupName_) && string.IsNullOrEmpty(subGroupName_) && ChromeManagerBase.LockerBlackHoleEnabled)
                {
                    return chromeManager.RootWidgets[Core.ChromeManager.Helpers.ChromeArea.Locker.ToLower()];
                }
                if (chromeManager.RootWidgets.ContainsKey(Core.ChromeManager.Helpers.ChromeArea.Ribbon.ToLower()))
                {
                   return string.IsNullOrEmpty(subGroupName_) ?  
                   chromeManager[Core.ChromeManager.Helpers.ChromeArea.Ribbon][groupName_]: 
                    chromeManager[Core.ChromeManager.Helpers.ChromeArea.Ribbon][groupName_][subGroupName_];
                }
                if (chromeManager.RootWidgets.ContainsKey(Core.ChromeManager.Helpers.ChromeArea.LauncherBar.ToLower()))
                {
                    return string.IsNullOrEmpty(groupName_) ? 
                        chromeManager[Core.ChromeManager.Helpers.ChromeArea.LauncherBar]:
                        chromeManager[Core.ChromeManager.Helpers.ChromeArea.LauncherBar][groupName_];
                }
            }
            return null;
        }

        public static InitialLocation ConvertToInitialLocation(this IChromeManager chromeManager_, 
            InitialWindowLocation location_, string tabName_)
        { 
            switch (location_)
            {
                case InitialWindowLocation.FloatingAnywhere:
                case InitialWindowLocation.FloatingOnlyAnywhere:
                case InitialWindowLocation.FloatingAtCursor:
                case InitialWindowLocation.FloatingOnlyAtCursor:
                case InitialWindowLocation.DockInNewTab:
                    return (InitialLocation)location_; 
                case InitialWindowLocation.DockLeft:
                case InitialWindowLocation.DockTop:
                case InitialWindowLocation.DockRight:
                case InitialWindowLocation.DockBottom: 
                    if (chromeManager_ is LauncherBarChromeManager)
                    {
                        return InitialLocation.Floating;;
                    }
                    if (!string.IsNullOrEmpty(tabName_))
                    {
                        var tab = chromeManager_.Tabwell.Tabs.FirstOrDefault(t_ => t_.Title == tabName_);
                        if (tab != null) tab.Activate();
                        else
                        {
                            chromeManager_.Tabwell.AddTab(tabName_);
                        }
                    } 
                    return InitialLocation.DockInActiveTab | (InitialLocation)location_;
                default:
                    return InitialLocation.Floating; 
            }
        }
    }

    public abstract partial class ChromeManagerBase : DependencyObject, IChromeManager, IPersistable
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<ChromeManagerBase>();
        private static ChromeManagerBase _instance;
        private static bool _startupCompleted;
        private const string StateElementName = "State";
        private const string ViewStateElementName = "ViewState";
        private readonly ObservableCollection<IWindowViewContainer> _windows =
            new ObservableCollection<IWindowViewContainer>();

        private readonly ObservableCollection<IWindowViewContainer> _tileItems =
            new ObservableCollection<IWindowViewContainer>();

        private readonly Dictionary<string, List<Exception>> _layoutExceptions =
            new Dictionary<string, List<Exception>>();

        private readonly List<string> _topmostViews = new List<string>();
        private readonly IUnityContainer _container;
        private readonly IWindowManager _windowManager;
        private readonly ITileItemManager _tileItemManager;
        private readonly Object _isLoadingOrSaving = new Object();
        private readonly IShell _shell;
        private readonly ChromeRegistry _registry;
        private readonly IWidgetFactory _widgetFactory;
        private readonly IWindowFactory _windowFactory;
        private readonly CafGuiWindowsCollection _cafGuiWindowsCollection = new CafGuiWindowsCollection();

        private readonly Dictionary<string, IWidgetViewContainer> _pendingPlacements =
            new Dictionary<string, IWidgetViewContainer>();

        private readonly Dictionary<string, IWidgetViewContainer> _placedWidgets =
            new Dictionary<string, IWidgetViewContainer>();

        private readonly Dictionary<string, IWidgetViewContainer> _rootWidgets =
            new Dictionary<string, IWidgetViewContainer>();

        private readonly Dictionary<object, ObservableCollection<IWidgetViewContainer>> _registrations =
            new Dictionary<object, ObservableCollection<IWidgetViewContainer>>();

        private ConcordWindowManager _concordWindowManager;
        private IViewContainer _activeViewContainerContainer;
        private PersistenceProfileService _persistenceProfileService;


        internal static bool MainToolsCleared = false;
        internal static bool MainWindowFocusGrabAtStartupSuppressed = false;
        internal static bool AcquireFocusAfterStartup = false;
        internal static Type CABNavigationControl;
        internal static bool EnableImplicitWidgetCreation = false;
        internal static bool LockerBlackHoleEnabled = false;
        internal static bool LockerAffectsMainAreasOnly = true;
        internal static bool ChromeReorderingEnabled = false;
        internal static bool OwnerTabInjectToLayout = false;
        internal static bool WillLoadChromeFromState;
        internal static bool WidgetUserConfigEnabled = false;
        internal static bool HideWindowsOnLayoutLoading;

        private volatile bool _isLoadingLayout;

        internal WidgetViewContainer DefaultChromeArea = null;
        internal static StringDictionary AppletCreatorMappings = new StringDictionary();
        internal static List<string> AppletCreatorMappedSingletons = new List<string>();

        internal static Dictionary<string, Func<XElement, XElement>> AppletConverterMappings =
            new Dictionary<string, Func<XElement, XElement>>();

        internal static Dictionary<string, Func<string, XElement, string>> AppletCreatorFunctionMappings =
            new Dictionary<string, Func<string, XElement, string>>();

        internal static Dictionary<string, Func<string, XElement, Tuple<string, bool>>> AppletCreatorSingletonFunctionMappings =
                new Dictionary<string, Func<string, XElement, Tuple<string, bool>>>();

        internal static bool ViewTitleRestoreNoOverwrite = false;
        internal static bool ViewLocationNoOverwrite = false;

        public static bool TabsOwnFloatingWindows = false;

        internal static ChromeManagerBase Instance
        {
            get { return _instance; }
            private set
            {
                _instance = value;
                WidgetViewContainerExtensions.chromeManager = _instance;
                WidgetViewContainer.chromeManager = _instance;
            }
        }

        private PersistenceProfileService PersistenceProfileService
        {
            get
            {
                if (_persistenceProfileService == null)
                {
                    _persistenceProfileService = _container.Resolve<PersistenceProfileService>();
                }
                return _persistenceProfileService;
            }

        }

        internal IShell Shell
        {
            get { return _shell; }
        }

        internal InitialSettings Settings
        {
            get
            {
                return _container.Resolve<InitialSettings>();
            }
        }


        internal static bool StartupCompleted
        {
            get { return _startupCompleted; }
            set
            {
                _startupCompleted = value;
                if (value && AcquireFocusAfterStartup)
                {
                    Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Input,
                                                             (Action) (
                                                                          () =>
                                                                              {
                                                                                  Instance._shell.MainWindow.Focus();
                                                                                  Instance._shell.MainWindow.Activate();
                                                                              }));
                }
            }
        }

        internal IWindowManager WindowManager
        {
            get { return _windowManager; }
        }
        internal ChromeManagerBase(ChromeRegistry chromeRegistry, IShell shell,
                                   PersistenceProfileService persistenceProfileService,
                                   PersistenceService persistenceService, IUnityContainer container, IWindowFactory windowFactory)
        {
            var lockerBuf = LockerBlackHoleEnabled;
            LockerBlackHoleEnabled = false;
            Instance = this;
            _registry = chromeRegistry;
            _container = container; 
            _windowManager = shell.WindowManager;
            _tileItemManager = shell.TileItemManager;
            _shell = shell;
            _shell.OnClosed = OnApplicationClosed;
            _widgetFactory = new WidgetFactory(_registry);
            _windowFactory = windowFactory;

            persistenceService.ProfileProssessingComplete += HandleProfileProssessingComplete;
            persistenceService.BeginProfileProcessing += HandleBeginProfileProcessing;


            // TODO (hrechkin): Bad practice - object is not yet initialized. Remove these.
            MenuFactories.Initialize(shell, this);
            LockerFactories.Initialize(this);
            IsolatedWindowFactories.Initialize(_registry);

            _windowManager.ActivePaneChanged += OnActivePaneChanged;
            _windowManager.ActivePaneChanging += OnActivePaneChanging;

            ShellTabViewModel.TabWellLeftEvent += ShellTabViewModel_TabWellLeftEvent;
            ShellTabViewModel.TabWellRightEvent += ShellTabViewModel_TabWellRightEvent;

            //chromeAreas.Add(LOCKER_COMMANDAREA_NAME, new LockerRootArea(this));
            //chromeAreas.Add(MENU_COMMANDAREA_NAME, new MenuRootArea(m_shell, this));
            //chromeAreas.Add(STATUSBAR_COMMANDAREA_NAME, new StatusbarRootArea(m_shell, this));
            //chromeAreas.Add(SYSTEMTRAY_COMMANDAREA_NAME, new SystemTrayRootArea(this));
            AddRootArea(STATUSBAR_COMMANDAREA_NAME, new InitialStatusbarParameters());
            var statusbar = this[STATUSBAR_COMMANDAREA_NAME];
            statusbar.AddWidget(StatusbarRootArea.StatusbarLeftEdgeID,
                                new InitialStatusbarRegionParameters {Alignment = HorizontalAlignment.Left});
            statusbar.AddWidget(StatusbarRootArea.StatusbarRightEdgeID,
                                new InitialStatusbarRegionParameters {Alignment = HorizontalAlignment.Right});

            // directly add a dummy item for statusbar updates during startup
            ((WidgetViewContainer) statusbar["left"]).AddLogical(
                new WidgetViewContainer(StatusbarRootArea.MAIN_STATUS_ITEM_NAME)
                    {
                        Content = new StatusbarDummyItem()
                    });

            // add a pending real item registration (this will replace above dummy during CreateChrome)
            statusbar["left"].AddWidget(StatusbarRootArea.MAIN_STATUS_ITEM_NAME, new InitialStatusLabelParameters
                {
                    IsVisible = !container.Resolve<IHidingLockingManager>().Granularity.HasFlag(HideLockUIGranularity.HideStatusBarNotifications)
                });
            
            AddRootArea(MENU_COMMANDAREA_NAME, new InitialMenuParameters());
            AddRootArea(SYSTEMTRAY_COMMANDAREA_NAME, new InitialSystemTrayParameters());
            AddRootArea(LOCKER_COMMANDAREA_NAME, new InitialLockerRootParameters());


            // todo: A PROBLEM HERE !!!
            //  on-demand initialization of the print command
            InitializePrintCommands();
            LockerBlackHoleEnabled = lockerBuf;

            _windowManager.DragOrDropOnWindow += (sender, args) => RouteDragOrDropOnWindowEvent(args);
            _windowManager.WindowGroupDroppedAtScreenEdge +=
                (sender, args) => InvokeWindowGroupDroppedOnScreenEdge(args);

            _windowManager.PaneDroppedAtScreenEdge += (o, eventArgs) => InvokeViewDroppedOnScreenEdge(
                new ViewDroppedOnScreenEdgeEventArgs
                    {
                        Edge = eventArgs.Edge,
                        View = eventArgs.ViewContainer
                    });
            _workspaces = new Workspaces(this);
            DockLayoutRootName = DockViewModel.DockLayoutRootName;

            this.allOpenedWorkspaces.CollectionChanged += allOpenedWorkspaces_CollectionChanged;
            
        }

        public string DockLayoutRootName { get; private set; }
        private void HandleProfileProssessingComplete(object sender, EventArgs e)
        {
            OnProfileProcessingComplete();
        }

        private void HandleBeginProfileProcessing(object sender, EventArgs e)
        {
            OnBeginProfileProcessing();
        }

        private void ShellTabViewModel_TabWellRightEvent(object sender, EventArgs<ShellTabViewModel> e)
        {
            if (_container.Resolve<IShell>().WindowManager is TabbedDockViewModel)
            {
                var tdvm = _container.Resolve<IShell>().WindowManager as TabbedDockViewModel;
                tdvm.MoveTabRight(e.Value1);
            }
        }

        private void ShellTabViewModel_TabWellLeftEvent(object sender, EventArgs<ShellTabViewModel> e)
        {
            if (_container.Resolve<IShell>().WindowManager is TabbedDockViewModel)
            {
                var tdvm = (TabbedDockViewModel) _container.Resolve<IShell>().WindowManager;
                tdvm.MoveTabLeft(e.Value1);
            }
        }

        #region IChromeManager Members

        IWindowViewContainer IChromeManager.CreateWindow(string factoryId, InitialWindowParameters initialParameters)
        { 
            return CreateWindow(factoryId, initialParameters);
        }


        private void AddNativeWpfView(WindowViewContainer viewContainer)
        {
            Window window;

            if (viewContainer.Background is SolidColorBrush &&
                (viewContainer.Background as SolidColorBrush).Color == Colors.Transparent)
            {
                window = new Window
                    {
                        WindowStyle = WindowStyle.None,
                        ResizeMode = ResizeMode.NoResize,
                        AllowsTransparency = true,
                    };
                EventHandler sourceInitialized = null;
                window.SourceInitialized +=
                    sourceInitialized =
                    (sender_, args_) =>
                        {
                            FloatingWindowUtil.FixResize(window); 
                            window.EnableMinimizeScreenshotSupport();
                            if (FrameworkExtensions.DefaultStickWindowOptions != StickyWindowOptions.None)
                            {
                                new WPFStickyWindow(window)
                                {
                                    StickToScreen = FrameworkExtensions.DefaultStickWindowOptions.HasFlag(StickyWindowOptions.StickToScreen),
                                    StickToOther = FrameworkExtensions.DefaultStickWindowOptions.HasFlag(StickyWindowOptions.StickToOther)
                                };
                            }
                            window.SourceInitialized -= sourceInitialized;
                        };

            }
            else
            {
                window = new Window();
            }

            window.Background = viewContainer.Background;

            WindowViewContainerToWindow(viewContainer, window);

            viewContainer.PropertyChanged += (s, e) =>
                {
                    switch (e.PropertyName)
                    {
                        case "Icon":
                            window.Icon = viewContainer.Icon.ToImageSource();
                            break;
                        case "Topmost":
                            if (window.Topmost != viewContainer.Topmost)
                                window.Topmost = viewContainer.Topmost;
                            break;

                        case "Width":
                            if (window.Width != viewContainer.Width)
                                window.Width = viewContainer.Width;
                            break;

                        case "Height":
                            if (window.Height != viewContainer.Height)
                                window.Height = viewContainer.Height;
                            break;

                        case "ShowInTaskbar":
                            if (window.ShowInTaskbar != viewContainer.ShowInTaskbar)
                                window.ShowInTaskbar = viewContainer.ShowInTaskbar;
                            break;

                        case "Background":
                            if (window.Background != viewContainer.Background)
                                window.Background = viewContainer.Background;
                            break;

                        case "Title":
                            if (window.Title != viewContainer.Title)
                                window.Title = viewContainer.Title;
                            break;
                    }
                };

            EventHandler hack = null;
            hack = (s, e) =>
                {
                    _windows.Remove(viewContainer);
                    window.Closed -= hack;
                };

            window.Closed += hack;

            EventHandler closeHandler = null;
            closeHandler = (sender_, args_) =>
                {
                    window.Close();
                    viewContainer.CloseRequested -= closeHandler;
                };

            viewContainer.CloseRequested += closeHandler;

            _windows.Add(viewContainer);

            window.Show();
        }

        private static void WindowViewContainerToWindow(IWindowViewContainer viewContainer, Window window)
        {
            if (!Double.IsNaN(viewContainer.Parameters.Width))
                window.Width = viewContainer.Parameters.Width;

            if (!Double.IsNaN(viewContainer.Parameters.Height))
                window.Height = viewContainer.Parameters.Height;

            //note here viewContainer.Topmost is not set to viewContainer.Parameters.Topmost to avoid windows not using DockManager to be saved when saving the layout
            window.Topmost = viewContainer.Parameters.Topmost;

            window.ShowInTaskbar = viewContainer.Parameters.ShowInTaskbar;

            if (viewContainer.Parameters.Resources != null)
                window.Resources.MergedDictionaries.Add(viewContainer.Parameters.Resources);

            if ((viewContainer.Parameters.InitialLocation & InitialLocation.Custom) == InitialLocation.Custom &&
                viewContainer.Parameters.InitialLocationCallback != null)
            {
                window.WindowStartupLocation = WindowStartupLocation.Manual;
                window.Loaded += (s, e) =>
                    {
                        var location = viewContainer.Parameters.InitialLocationCallback(window.ActualWidth,
                                                                                        window.ActualHeight);
                        window.Top = location.Top;
                        window.Left = location.Left;

                        if (window.ActualWidth != location.Width)
                            window.Width = location.Width;
                        if (window.ActualHeight != location.Height)
                            window.Height = location.Height;
                    };
            }

            window.SizeToContent = (viewContainer.Parameters.SizingMethod == SizingMethod.SizeToContent)
                                       ? SizeToContent.WidthAndHeight
                                       : SizeToContent.Manual;

            window.MaxHeight = 1080;
            window.Icon = viewContainer.Icon.ToImageSource();

            window.SizeChanged += (s, e) =>
                {
                    if (e.WidthChanged)
                        viewContainer.Width = e.NewSize.Width;

                    if (e.HeightChanged)
                        viewContainer.Height = e.NewSize.Height;
                };

            window.Title = viewContainer.Title;
            window.Content = viewContainer.Content;
        }

        IDialogWindow IChromeManager.CreateDialog(string factoryId)
        {
            if (_registry.DialogFactories.ContainsKey(factoryId))
            {
                var dialog = _windowManager.CreateDialog() as IDialogWindow;

                var factory = _registry.DialogFactories[factoryId];
                var factoryResult = false;
                try
                {
                    factoryResult = factory.InitHandler(dialog);
                }
                catch (Exception exc)
                {
                    Logger.Error("Error when call ChromeManagerBase.CreateDialog", exc);

                }
                return factoryResult ? dialog : null;
            }

            return null;
        }

        /*
         * TODO:
         * 
         * This is a little more restrictive than it should be w/ the semaphore: a singleton can not create two instances 
         * simultaneously even if the key is different.
         */

        internal IWindowViewContainer CreateWindow(string factoryId, InitialWindowParameters initialParameters,
                                                   XDocument savedState = null, XDocument viewState=null)
        {
            if (!CreateChromeCalled)
            {
                return null;
            }

            WindowFactoryHolder factoryHolder = null;
            if (initialParameters != null)
            {
                ((IChromeElementInitializerRegistry) _registry).TryGetWindowFactoryHolder(initialParameters.GetType(),
                                                                                          out factoryHolder);
            }

            if (!String.IsNullOrEmpty(factoryId) && _registry.WindowFactories.ContainsKey(factoryId))
            {
                factoryHolder = _registry.WindowFactories[factoryId];
            }
            if (factoryHolder == null) return null;

            if (factoryHolder.Semaphore.CurrentCount > 0)
            {
                try
                {
                    factoryHolder.Semaphore.Wait();

                    IWindowViewContainer wvc;

                    string singletonViewInstanceId;
                    if (initialParameters != null && initialParameters.Singleton &&
                        ((WindowFactory) _windowFactory).TryGetSingletonViewInstanceId(factoryId,
                                                                                       initialParameters.SingletonKey,
                                                                                       out singletonViewInstanceId))
                    {
                        wvc = _windows.FirstOrDefault(vc => vc.ID == singletonViewInstanceId);

                        if (wvc != null)
                        {
                            wvc.Activate();
                            return wvc;
                        }

                        Logger.WarningWithFormat(
                            "ChromeManagerBase.CreateWindow: Singleton window with factoryId '{0}' and view ID '{1}' does not exist.",
                            factoryId, singletonViewInstanceId);
                    }

                    // Not singleton or singleton not found
                    var moduleGroup = _container.Resolve<IModuleGroup>();
                    if (moduleGroup.IsWindowFactoryInGroup(factoryId))
                    {
                        wvc = moduleGroup.CreateGroupWindow(factoryId, initialParameters);
                    }
                    else
                    {
                        wvc = _windowFactory.CreateWindowInstance(factoryId, initialParameters, null, true, savedState,
                                                                  viewState);
                    }

                    if (wvc != null)
                    {
                        wvc.Topmost = _topmostViews.Contains(wvc.ID);

                        var newContainer = wvc is TileItemViewContainer
                                               ? AddTileItem(wvc,
                                                             (initialParameters as InitialTileItemParameters)
                                                                 .InitialLocationTarget as ITileViewContainer)
                                               : AddView(wvc, null);
                        var nC = newContainer as WindowViewContainer;
                        if (nC != null)
                        {
                            nC.InvokeCreated();
                        }

                        return newContainer;
                    }
                }
                finally
                {
                    factoryHolder.Semaphore.Release();
                }
            }

            return null;
        }



        public virtual IWidgetViewContainer PlaceWidget(string factoryId, string location,
                                                        InitialWidgetParameters initialParameters)
        {
            Logger.Warning(
                "Controls can not be placed on the default command area by ChromeManagerBase. Use the overloaded PlaceWidget() method and specify root.");
            return null;
        }

        public IWidgetViewContainer PlaceWidget(string factoryId, string root, string location,
                                                InitialWidgetParameters initialParameters)
        {
            if (factoryId == null)
            {
                Logger.Warning("Null is not a valid factoryID for ChromeManager.PlaceWidget.");
                return null;
            }

            var rootWidget = this[root] as WidgetViewContainer;

            if (rootWidget != null)
            {
                var paddedLocation = rootWidget.PadLocationStringWithDefaults(location);

                if (paddedLocation != String.Empty)
                {
                    var path = paddedLocation.Split('/');
                    foreach (var step in path)
                    {
                        rootWidget = rootWidget[step] as WidgetViewContainer;
                    }
                }
            }

            var result = AddWidget(factoryId, initialParameters, rootWidget, true);

            if (rootWidget == null)
                _pendingPlacements.Add(root + "/" + location, result);

            return result;
        }

        private void ProcessPendingPlacements()
        {
            foreach (var placement in _pendingPlacements)
            {
                var path = placement.Key.Split('/', '\\').ToList();

                var root = this[path[0]];
                path.RemoveAt(0);
                foreach (string step in path)
                {
                    if (root != null && !String.IsNullOrEmpty(step))
                        root = root[step];
                }

                if (root == null)
                    continue;

                var parent = root as WidgetViewContainer;
                parent.AddLogical(placement.Value);
                parent.AddVisual(placement.Value);
            }
        }

        protected WidgetViewContainer CreateViewContainer(string factoryID, InitialWidgetParameters initialParameters)
        {
            return
                Activator.CreateInstance(ResolveViewContainerType(initialParameters), initialParameters, factoryID) as
                WidgetViewContainer;
        }

        public IWidgetViewContainer GetWidget(string factoryId = null)
        {
            return GetWidgets(factoryId).FirstOrDefault() ??
                   _pendingPlacements.Values.AsEnumerable().FirstOrDefault(w => w.FactoryID == factoryId);
        }

        public IEnumerable<IWidgetViewContainer> GetWidgets(string factoryId = null)
        {
            if (factoryId == null)
                return new List<IWidgetViewContainer>();
            IWidgetViewContainer widgetViewContainer = null;
            if (CreateChromeCalled)
            {
                if (_placedWidgets.TryGetValue(factoryId, out widgetViewContainer))
                {
                    return new List<IWidgetViewContainer> {widgetViewContainer};
                }
            }

            foreach (var key in _registrations.Keys)
            {
                var retval = (from host in _registrations.Values
                              from wvc in host
                              where wvc.FactoryID.Equals(factoryId, StringComparison.InvariantCultureIgnoreCase)
                              select wvc).ToList();
                if (retval.Count == 0)
                    if (_rootWidgets.TryGetValue(factoryId, out widgetViewContainer))
                        return new List<IWidgetViewContainer> {widgetViewContainer};
                return retval;
            }

            return new List<IWidgetViewContainer>();
        }

        public void RemoveWidget(IWidgetViewContainer placedWidget)
        {
            if (placedWidget == null || placedWidget.Parent as WidgetViewContainer == null)
                return;

            var parent = placedWidget.Parent as WidgetViewContainer;

            if (CreateChromeCalled)
                parent.RemoveVisual(placedWidget);

            parent.RemoveLogical(placedWidget);

            if (_registrations.ContainsKey(parent) && _registrations[parent].Contains(placedWidget))
                //TODO: Fugly. Should we keep stuff placed after CreateChrome in registrations? Or should we not remove it?
            {
                _registrations[parent].Remove(placedWidget);
            }

            if (_placedWidgets.ContainsKey(placedWidget.FactoryID))
                _placedWidgets.Remove(placedWidget.FactoryID);
        }

        public void RemoveWidget(string factoryId)
        {
            foreach (var wvc in GetWidgets(factoryId))
                RemoveWidget(wvc);
        }

        public void ClearWidget(IWidgetViewContainer placedWidget)
        {
            var widget = placedWidget as WidgetViewContainer;
            if (widget == null)
                return;

            foreach (var child in widget.Children.ToArray())
            {
                RemoveWidget(child);
            }
        }

        public ObservableWindowCollection Windows
        {
            get { return new ObservableWindowCollection(_windows); }
        }


        public IEnumerable<IWindowViewContainer> GetViews()
        {
            foreach (var window in Windows)
            {
                IViewCollectionContainer collectionContainer = window as IViewCollectionContainer;
                if (collectionContainer != null)
                {
                    foreach (var childWindow in collectionContainer.Children)
                    {
                        yield return childWindow;
                    }
                }
                else
                {
                    yield return window;
                }
            }
        }

        public event EventHandler LayoutLoaded;
        public event EventHandler ProfileProcessingComplete;
        public event EventHandler BeginProfileProcessing;

        private void OnLayoutLoaded()
        {
            var handler = LayoutLoaded;
            if (handler != null)
            {
                handler(this, null);
            }
        }

        private void OnBeginProfileProcessing()
        {
            var handler = BeginProfileProcessing;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        private void OnProfileProcessingComplete()
        {
            var handler = ProfileProcessingComplete;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public event EventHandler<WindowEventArgs> WindowActivated;

        private void OnWindowActivated(IWindowViewContainer view)
        {
            var handler = WindowActivated;
            if (handler != null)
            {
                handler(this, new WindowEventArgs(view));
            }
        }

        public event EventHandler<WindowEventArgs> WindowDeactivated;

        private void OnWindowDeactivated(IWindowViewContainer view)
        {
            var handler = WindowDeactivated;
            if (handler != null)
            {
                handler(this, new WindowEventArgs(view));
            }
        }

        public event EventHandler<CancellableWindowEventArgs> WindowDeactivating;

        private void OnWindowDeactivating(IWindowViewContainer view, out bool cancel)
        {
            var handler = WindowDeactivating;
            if (handler != null)
            {
                var e = new CancellableWindowEventArgs(view);
                handler(this, e);
                cancel = e.Cancel;
            }
            else
            {
                cancel = false;
            }
        }

        public event EventHandler ApplicationClosed;

        private void OnApplicationClosed()
        {
            var handler = ApplicationClosed;
            if (handler != null)
            {
                handler(this, null);
            } 
        }

        public event EventHandler<DragOrDropOnWindowEventArgs> DropOnShell;
        public event EventHandler<DragOrDropOnWindowEventArgs> PreviewDropOnShell;
        public event EventHandler<DragOrDropOnWindowEventArgs> DragEnterOnShell;
        public event EventHandler<DragOrDropOnWindowEventArgs> DragOverOnShell;
        public event EventHandler<DragOrDropOnWindowEventArgs> DragLeaveOnShell;
        public event EventHandler<DragOrDropOnWindowEventArgs> PreviewDragEnterOnShell;
        public event EventHandler<DragOrDropOnWindowEventArgs> PreviewDragOverOnShell;
        public event EventHandler<DragOrDropOnWindowEventArgs> PreviewDragLeaveOnShell;

        internal void RouteDragOrDropOnWindowEvent(GenericDragOrDropOnWindowEventArgs e)
        {
            EventHandler<DragOrDropOnWindowEventArgs> handler;
            if (e.InternalEventType == UIElement.DropEvent)
            {
                handler = DropOnShell;
            }
            else if (e.InternalEventType == UIElement.PreviewDropEvent)
            {
                handler = PreviewDropOnShell;
            }
            else if (e.InternalEventType == UIElement.DragEnterEvent)
            {
                handler = DragEnterOnShell;
            }
            else if (e.InternalEventType == UIElement.DragOverEvent)
            {
                handler = DragOverOnShell;
            }
            else if (e.InternalEventType == UIElement.DragLeaveEvent)
            {
                handler = DragLeaveOnShell;
            }
            else if (e.InternalEventType == UIElement.PreviewDragOverEvent)
            {
                handler = PreviewDragOverOnShell;
            }
            else if (e.InternalEventType == UIElement.PreviewDragEnterEvent)
            {
                handler = PreviewDragEnterOnShell;
            }
            else if (e.InternalEventType == UIElement.PreviewDragLeaveEvent)
            {
                handler = PreviewDragLeaveOnShell;
            }
            else
            {
                return;
            }
            if (handler != null)
            {
                handler(this, new DragOrDropOnWindowEventArgs
                    {
                        DragEventArgs = e.DragEventArgs,
                        Window = e.Window
                    });
            }
        }

        #endregion

        #region Window handling internals

        /// <summary>
        /// Adds a CafGUI window. This method isn't used in MSDesktop directly and is invoked via reflection 
        /// from CafGUI.Toolkit DLL.
        /// </summary>
        /// <param name="initialParameters"></param>
        /// <returns></returns>
        internal IWindowViewContainer AddCafGuiWindow(InitialCafGuiWindowParameters initialParameters)
        {
            IWindowViewContainer container;
            if (!_cafGuiWindowsCollection.TryGetContainer(initialParameters.CafGuiViewId, out container))
            {
                container = CreateWindow(CafGuiBridge.CafGuiWindowFactoryName, initialParameters);
            }
            else
            {
                // Placeholder created from a profile. We won't have Content set on it.
                container.Content = initialParameters.CafGuiUiElement;
            }

            return container;
        }
        
        internal IWindowViewContainer AddView(IWindowViewContainer viewContainer, object additionalData)
        {
            if (viewContainer == null) return null;
            if (viewContainer.Parameters.UseDockManager)
            {
                var s = _container.Resolve<IShell>();
                var dockInNewTab = viewContainer.Parameters.InitialLocation == InitialLocation.DockInNewTab;
                var dockInExisting = viewContainer.Parameters.InitialLocationTargetTab != null;

                if ((dockInNewTab ||
                     viewContainer.Parameters.InitialLocation.HasFlag(InitialLocation.DockInActiveTab)) &&
                    s.WindowManager is TabbedDockViewModel)
                {
                    int tabPosition = -1;
                    string title = viewContainer.Title ?? String.Empty;
                    var tdvm = s.WindowManager as TabbedDockViewModel;
                    ShellTabViewModel tabCandidate;
                    var _continue = true;
                    if (dockInNewTab)
                    {
                        if (dockInExisting)
                        {
                            tabCandidate = (ShellTabViewModel)viewContainer.Parameters.InitialLocationTargetTab;
                        }
                        else
                        {
                            tabCandidate = tdvm.Tabs.Cast<ShellTabViewModel>()
                                               .Where((t, idx) => (0 == tdvm.Workspaces.Count(w => w.Host == t)))
                                               .FirstOrDefault();
                            _continue = tdvm.Tabs.Cast<ShellTabViewModel>().All(t => t.Title != title);
                        }
                    }
                    else
                    {
                        tabCandidate = (ShellTabViewModel)tdvm.CurrentTab;
                    }

                    var i = -1;
                    if (!_continue)
                    {
                        _continue = true;
                        i = 1;
                        while (_continue)
                        {
                            _continue = false;
                            foreach (ShellTabViewModel t in tdvm.Tabs)
                            {
                                if (t.Title == String.Format("{1} {0}", i, title))
                                {
                                    _continue = true;
                                    i++;
                                    break;
                                }
                            }
                        }
                    }
                    if (tabCandidate == null)
                    {

                        tabCandidate = tdvm.AddTab(i == -1 ? title : String.Format("{1} {0}", i, title));
                        tabPosition = tdvm.Tabs.Cast<ShellTabViewModel>().Count() - 1;
                    }
                    else if (dockInNewTab && !dockInExisting)
                    {
                        tabCandidate.Title = i == -1 ? title : String.Format("{1} {0}", i, title);
                        tabPosition =
                            tdvm.Tabs.Cast<ShellTabViewModel>().TakeWhile(t => t == tabCandidate).Count() - 1;
                    }

                    _windows.Add(viewContainer);
                    _windowManager.AddNewView(viewContainer as WindowViewContainer, tabCandidate);

                    tdvm.Tabs.Refresh();
                    if (dockInNewTab && !dockInExisting)
                    {
                        tdvm.Tabs.MoveCurrentToPosition(tabPosition);
                    }
                }
                else
                {
                    ShellTabViewModel hostTab = null;
                    if ((viewContainer.Parameters.InitialLocation == InitialLocation.DockTabbed ||
                        viewContainer.Parameters.InitialLocation == InitialLocation.DockRight ||
                        viewContainer.Parameters.InitialLocation == InitialLocation.DockLeft ||
                        viewContainer.Parameters.InitialLocation == InitialLocation.DockTop ||
                        viewContainer.Parameters.InitialLocation == InitialLocation.DockBottom) &&
                        s.WindowManager is TabbedDockViewModel)
                    {
                        var targetVc = viewContainer.Parameters.InitialLocationTarget;
                        var tdvm = s.WindowManager as TabbedDockViewModel;
                        hostTab = tdvm.Tabs.Cast<ShellTabViewModel>().FirstOrDefault(t =>
                                                                                     t.Windows.Any(
                                                                                         w => w.Equals(targetVc))
                            );
                        // if hostTab != null than target window is tabbed; set host to the same tab
                    }

                    _windows.Add(viewContainer);
                    _windowManager.AddNewView(viewContainer as WindowViewContainer, additionalData ?? hostTab);
                }

                if (HideWindowsOnLayoutLoading && _isLoadingLayout)
                {
                    viewContainer.IsHidden = true;
                }
                if (((WindowViewContainer)viewContainer).ViewContainerType == ViewContainerType.Concord)
                {
                    //System.Diagnostics.Debugger.Break();
                    AppletFormHostSizeChangeRegistration(viewContainer, PersistenceProfileService);
                }
                //m_windows.Add(viewContainer_);

                viewContainer.Closed += ViewContainerClosed;
                viewContainer.ClosedQuietly += ViewContainerClosed;
                InvokeViewAdded(viewContainer);
            }
            else if (viewContainer.Parameters.IsModal)
            {
                var dialogWindow = ((DialogWindow)viewContainer);
                dialogWindow.ResizeMode = viewContainer.Parameters.ResizeMode;
                if (viewContainer.Parameters is InitialDialogWindowParameters)
                {
                    dialogWindow.Owner = (viewContainer.Parameters as InitialDialogWindowParameters).ModalViewOwner;
                }
                WindowViewContainerToWindow(viewContainer, dialogWindow);
                dialogWindow.ShowDialog();
                return viewContainer;
            }
            else
            {
                AddNativeWpfView(viewContainer as WindowViewContainer);
                InvokeViewAdded(viewContainer);
            }

            if (IsSavedWorkspace(viewContainer.ID))
            {
                AddToSavedWorkspaces(viewContainer);
            }

            if (viewContainer.Parameters.InjectRootRegionManager)
            {
                _container.Resolve<IModuleFrameworkHandler>().InjectRootRegionManger(viewContainer);
            }

            return viewContainer;
        }

        private static void AppletFormHostSizeChangeRegistration(IWindowViewContainer viewContainer,
                                                                 PersistenceProfileService persistenceProfileService)
        {
            var afh = GetAppletFormHost(viewContainer);
            if (afh != null)
            {
                afh.Window.Form.SizeChanged += persistenceProfileService.ConcordFormSizeChanged;
            }
        }

        private static void AppletFormHostSizeChangeDeregistration(IWindowViewContainer viewContainer,
                                                                   PersistenceProfileService persistenceProfileService)
        {
            var afh = GetAppletFormHost(viewContainer);
            if (afh != null)
            {
                afh.Window.Form.SizeChanged -= persistenceProfileService.ConcordFormSizeChanged;
            }
        }

        private static AppletFormHost GetAppletFormHost(IWindowViewContainer viewContainer)
        {
            return viewContainer.Content as AppletFormHost
                   ?? (viewContainer.Content is DockPanel &&
                       (viewContainer.Content as DockPanel).Children[0] is AppletFormHost
                           ? (viewContainer.Content as DockPanel).Children[0] as AppletFormHost
                           : null);
        }

        private void ViewContainerClosed(object sender, WindowEventArgs e)
        {
            var windowViewContainer = e.ViewContainer as WindowViewContainer;
            if (windowViewContainer != null &&
                windowViewContainer.ViewContainerType == ViewContainerType.Concord)
            {
                AppletFormHostSizeChangeDeregistration(windowViewContainer, PersistenceProfileService);
            }

            e.ViewContainer.Closed -= ViewContainerClosed;

            if (windowViewContainer != null)
            {
                windowViewContainer.ClosedQuietly -= ViewContainerClosed;
            }

            _windows.Remove(e.ViewContainer);
        }

        private void InvokeViewAdded(IWindowViewContainer viewContainer)
        {
            var copy = ViewAdded;
            if (copy != null)
            {
                ViewAdded(this, new ViewEventArgs((WindowViewContainer) viewContainer));
            }
            ((WindowViewContainer)viewContainer).InvokeCreated();
        }

        private void OnActivePaneChanged(object sender, WindowManagerActiveChangedEventArgs e)
        {
            if (e.OldValue != null)
            {
                var copy = Deactivated;
                if (copy != null)
                {
                    copy(this, new ViewEventArgs(e.OldValue));
                }

                OnWindowDeactivated(e.OldValue as IWindowViewContainer);
            }
            if (e.NewValue != null)
            {
                _activeViewContainerContainer = e.NewValue;

                var copy = Activated;
                if (copy != null)
                {
                    copy(this, new ViewEventArgs(e.NewValue));
                }

                OnWindowActivated(e.NewValue as IWindowViewContainer);
            }
            else
            {
                _activeViewContainerContainer = null;
            }
        }

        private void OnActivePaneChanging(object sender, WindowManagerActiveChangingEventArgs e)
        {
            if (e.OldValue != null)
            {
                bool cancel;
                OnWindowDeactivating(e.OldValue as IWindowViewContainer, out cancel);
                e.Cancel = cancel;
            }
        }

        #endregion

        #region TileItem handling internals

        internal IWindowViewContainer AddTileItem(IWindowViewContainer viewContainer,
                                                  ITileViewContainer tileViewContainer)
        {
            if (viewContainer == null)
            {
                return null;
            }

            TileItemCollectionModel manager = _tileItemManager as TileItemCollectionModel;
            TileViewModel tile = null;
            if (tileViewContainer != null)
            {
                tile = manager.TileViews.FirstOrDefault(t_ => t_.ViewContainer == tileViewContainer);
            }
            if (tile == null)
            {
                //find the tile view with the same workspace if workspace is specified
                //or find the tile view without workspace if workspace is not specified
                var tiles = manager.TileViews.FindAll(
                    t_ => t_.WorkspaceCategory == viewContainer.Parameters.WorkspaceCategory);
                if (tiles.Count > 0)
                {
                    tile = tiles.FirstOrDefault(t_ => t_.IsActive);
                    if (tile == null)
                    {
                        tile = tiles[tiles.Count - 1];
                    }
                }

                if (tile == null)
                {
                    tileViewContainer =
                        ((IChromeManager) this).CreateWindow(Guid.NewGuid().ToString(),
                                                             new InitialTileParameters()
                                                                 {
                                                                     WorkspaceCategory =
                                                                         viewContainer.Parameters.WorkspaceCategory

                                                                 }) as ITileViewContainer;
                    if (tileViewContainer != null)
                    {
                        tile = manager.TileViews.FirstOrDefault(t_ => t_.ViewContainer == tileViewContainer);
                        if (tile != null)
                        {
                            tile.AutomaticCreated = true;
                        }
                    }

                }
            }

            if (tile == null)
            {
                throw new Exception("Failed to find the tile view to create the tile item");
            }
            _tileItems.Add(viewContainer);
            _tileItemManager.AddNewItem(viewContainer as TileItemViewContainer, tile);
            if (tile.AutomaticCreated)
            {
                tile.IsEditing = true;
            }

            return viewContainer;
        }

        #endregion

        #region Widget handling internals

        internal event EventHandler BeforeCreateChrome;

        protected void OnBeforeCreateChrome()
        {
            var handler = BeforeCreateChrome;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        internal event EventHandler AfterCreateChrome;

        protected void OnAfterCreateChrome()
        {
            var handler = AfterCreateChrome;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Indicates whether CreateChrome has been called. 
        /// </summary>
        internal bool CreateChromeCalled = false;

        /// <summary>
        /// Create the user interface.
        /// </summary>
        /// <param name="placeUnassigned">If true, all controls will be placed even if no PlaceWidget calls have been made for some.</param>
        public void CreateChrome(bool placeUnassigned = false)
        {
            if (CreateChromeCalled)
            {
                return;
            }

            CreateChromeCalled = true;
            OnBeforeCreateChrome();

            var placedFactories = new List<string>();

            #region ChromeManager 2.0

            var widgets = Registrations[this].ToArray();
            foreach (var w in widgets)
            {
                placedFactories.Add(w.FactoryID);
               
                if (CreateWidget(w))
                {
                    CreateChromeArea(w, placedFactories);
                }
            }

            #endregion


            if (placeUnassigned && !MainToolsCleared)
            {
                Logger.Info("Legacy placement enabled; placing registered widgets");
                foreach (var wf in Registry.WidgetFactories.Keys)
                {
                    if (placedFactories.Contains(wf))
                        continue;
                    Logger.Info(String.Format("Placing {0}", wf));

                    var vm = LockerBlackHoleEnabled
                                 ? this[LOCKER_COMMANDAREA_NAME].AddWidget(wf, new InitialWidgetParameters())
                                 : DefaultChromeArea.AddWidget(wf, new InitialWidgetParameters());

                    if (vm != null)
                    {
                        placedFactories.Add(wf);
                    }
                }
            }

            OnAfterCreateChrome();
        }

        internal WidgetViewContainer CreateWidget(string factoryID, InitialWidgetParameters initialParameters,
                                                  bool autoRegister = false)
        {
            if (!_registry.WidgetFactories.ContainsKey(factoryID) && !autoRegister)
            {
                Logger.InfoWithFormat("Factory '{0}' unknown. Unable to create widget.", factoryID);
                return null;
            }
            if (!_registry.WidgetFactories.ContainsKey(factoryID) && autoRegister)
                //there is no such factory, autoregister is true -> register the implicit factory logic.
            {
                _registry.RegisterWidgetFactory(factoryID);
            }

            var factory = _registry.WidgetFactories[factoryID];

            var viewContainer = CreateViewContainer(factoryID, initialParameters);

            // Black magic: try to find the factory using the initialParameters
            var initHandler = factory.BuildUp();

            if (initHandler == null)
            {
                if (initialParameters != null)
                {
                    WidgetFactoryHolder factoryHolder;
                    if (_registry.WidgetFactoryMapping.TryGetValue(initialParameters.GetType(), out factoryHolder))
                    {
                        initHandler = factoryHolder.BuildUp();
                    }
                }
            }

            if (initHandler == null) return null;

            bool factoryResult = false;

            try
            {
                factoryResult = initHandler(viewContainer, null);
            }
            catch (Exception exc)
            {
                Logger.Error(
                    String.Format("Init handler failed for widget factory with name {0}", factoryID),
                    exc);
            }

            if (factoryResult)
            {

                return viewContainer;
            }

            return null;
        }

        private Type ResolveViewContainerType(InitialWidgetParameters initialParameters)
        {
            if (initialParameters != null)
            {
                WidgetFactoryHolder factoryHolder;
                if (_registry.WidgetFactoryMapping.TryGetValue(initialParameters.GetType(), out factoryHolder))
                {
                    return factoryHolder.ViewContainerType;
                }
            }
            return typeof (WidgetViewContainer);
        }

        public const string LOCKER_COMMANDAREA_NAME = "locker";
        public const string STATUSBAR_COMMANDAREA_NAME = "statusbar";
        public const string SYSTEMTRAY_COMMANDAREA_NAME = "systemtray";
        public const string MENU_COMMANDAREA_NAME = "menu";

        #endregion

        #region Layout handling

        private static readonly XElement m_cleanLayout = XElement.Parse(String.Format(@"<{3}>
		<{0}>
		<{1} version=" + "\"{2}\">\n" +
                                                                                      @"          <contentPanes />
		</{1}>
		</{0}>
		<Tabs>
		<Tab Name=" + "\"Main\" IsActive=\"true\">\n" +
                                                                                      @"          <Panes />
		</Tab>
		</Tabs>
		<Panes />
	</{3}>", LayoutConverter.DockLayoutRootName, LayoutConverter.DockManagerRootName,
                                                                                      LayoutConverter.DockManagerVersion,
                                                                                      TabbedDockViewModel.NODE_NAME));

        //todo: fix it
        private static void AddNewView(XElementWithSubitems viemManagerState_, XElement viewState_)
        {
            XElement xdmStateXml =
                viemManagerState_.Element(LayoutConverter.DockLayoutRootName)
                                 .Element(LayoutConverter.DockManagerRootName);
            string id = viewState_.Attribute("Id").Value;
            xdmStateXml.Element("contentPanes").Add(
                new XElement("contentPane",
                             new XAttribute("name", ContentPaneIdConverter.ConvertToPaneId(id)),
                             new XAttribute("location", "Floating")));

            XElement panes = xdmStateXml.Element("panes");
            if (panes == null)
            {
                panes = new XElement("panes");
                xdmStateXml.Add(panes);
            }
            panes.Add(
                new XElement("splitPane",
                             new XAttribute("name", "splitPane" + id),
                             new XAttribute("splitterOrientation", "Vertical"),
                             new XAttribute("location", "Floating"),
                             new XElement("contentPane",
                                          new XAttribute("name", ContentPaneIdConverter.ConvertToPaneId(id)))));
            viemManagerState_.Element("Panes").Add(viewState_);
        }

        private ConcordWindowManager ConcordWindowManager
        {
            get
            {
                if (_concordWindowManager == null &&
                    _container.IsRegistered<IConcordApplication>())
                {
                    _concordWindowManager =
                        _container.Resolve<IConcordApplication>().WindowManager
                        as ConcordWindowManager;
                }
                return _concordWindowManager;
            }
        }

        private XElement BuildIViewXml(WindowViewContainer viewContainer)
        {
            // <IView Id="786786-ayui-21312897" CreatorID="blah">      
            var viewElement = new XElement("IView", new XAttribute("Id", viewContainer.ID));

            //var meta = viewContainer_.ViewMeta;

            switch (viewContainer.ViewContainerType)
            {
                case ViewContainerType.WithCreator:
                    viewElement.Add(
                        new XAttribute("ViewContainerType", "WithCreator"),
                        new XAttribute("CreatorID", viewContainer.FactoryID)
                        );

                    WindowFactoryHolder factory;
                    var chromeElementInitializerRegistry = (IChromeElementInitializerRegistry) _registry;

                    if (viewContainer.Parameters != null &&
                        chromeElementInitializerRegistry.TryGetWindowFactoryHolder(
                            viewContainer.Parameters.GetType(), out factory))
                    {
                        viewElement.SetAttributeValue("ParametersType",
                                                      viewContainer.Parameters.GetType().FullName + ", " + viewContainer.Parameters.GetType().Assembly.GetName().Name);
                    }
                    if (!String.IsNullOrEmpty(viewContainer.Parameters.WorkspaceCategory))
                    {
                        viewElement.SetAttributeValue("WorkspaceCategory", viewContainer.Parameters.WorkspaceCategory);
                    }
                    if (viewContainer.Parameters.Persistable)
                    {
                        viewElement.SetAttributeValue("Persistable", true.ToString());
                        if (viewContainer.Parameters.PersistorType != null)
                        {
                            viewElement.SetAttributeValue("PersistorType",
                                                          viewContainer.Parameters.PersistorType.AssemblyQualifiedName);
                        }
                        if (viewContainer.Parameters.DynamicComponentPersistors)
                        {
                            viewElement.SetAttributeValue("DynamicComponentPersistors", true.ToString());
                        }
                    }

                    XDocument stateElem = null;
                    try
                    {

                        stateElem = GetXmlStateNode(chromeElementInitializerRegistry, viewContainer.FactoryID,
                                                    viewContainer);
                    }
                    catch (Exception ex)
                    {
                        AddException(viewContainer.FactoryID, ex);
                    }

                    if (stateElem != null && stateElem.Root != null)
                    {
                        viewElement.Add(new XElement(StateElementName, stateElem.Root));
                    }

                    if (viewContainer.Parameters != null && viewContainer.Parameters.Persistable)
                    {
                        try
                        {
                            var viewState = viewContainer.GetPersistor().SaveState();
                            if (viewState != null)
                            {
                                viewElement.Add(new XElement(ViewStateElementName, viewState.Root));
                            }
                        }
                        catch (Exception e)
                        {
                            AddException(viewContainer.FactoryID, e);
                        }

                    }

                    var tile = viewContainer as TileItemViewContainer;
                    if (tile != null)
                    {
                        
                        viewElement.Add(new XAttribute("Left", tile.RelativeLeft));
                        viewElement.Add(new XAttribute("Top", tile.RelativeTop));
                        viewElement.Add(new XAttribute("Width", tile.Width));
                        viewElement.Add(new XAttribute("Height", tile.Height));
                        if (tile.LastRightEdgeMargin != null)
                        {
                            viewElement.Add(new XAttribute("LastRightEdgeMargin", tile.LastRightEdgeMargin.Value));
                        }
                        if (tile.LastBottomEdgeMargin != null)
                        {
                            viewElement.Add(new XAttribute("LastBottomEdgeMargin", tile.LastBottomEdgeMargin.Value));
                        }
                    }

                    break;
                case ViewContainerType.Concord:
                    BuildConcordViewXml(viewContainer, viewElement);
                    break;
                default:
                    throw new InvalidOperationException("Unsupported IView type");
            }

            viewElement.Add(new XAttribute("Title", viewContainer.Title ?? String.Empty));

            if (TabsOwnFloatingWindows)
            {
                var dvm = (_shell.WindowManager as DockViewModel);
                if (dvm != null)
                {
                    var windowViewModel = dvm.Workspaces.FirstOrDefault(wvm => wvm.ViewContainer == viewContainer);

                    var tab = windowViewModel.Owner as ShellTabViewModel;
                    if (tab != null)
                    {
                        viewElement.Add(new XAttribute("OwnerTab", tab.Title));
                    }
                }
            }

            return viewElement;
        }

        private void AddException(string creatorId, Exception ex)
        {
            if (!_layoutExceptions.ContainsKey(creatorId))
                _layoutExceptions.Add(creatorId, new List<Exception>());

            _layoutExceptions[creatorId].Add(ex);
        }

        private static XDocument GetXmlStateNode(IChromeElementInitializerRegistry chromeElementInitializerRegistry, string creatorId, IWindowViewContainer viewContainer)
        {
            WindowFactoryHolder creator;
            if (chromeElementInitializerRegistry.TryGetWindowFactoryHolder(creatorId, out creator) ||
                chromeElementInitializerRegistry.TryGetWindowFactoryHolder(viewContainer.Parameters.GetType(), out creator))
            {
                if (creator.DehydrateHandler != null)
                {
                       return creator.DehydrateHandler(viewContainer);
                }
            }

            return null;
        }

        private void BuildConcordViewXml(IWindowViewContainer viewContainer, XElement paneElement)
        {
            if (ConcordWindowManager == null) return;
            var appletFormHost = viewContainer.Content as AppletFormHost;
            if (appletFormHost != null)
            {
                var window = appletFormHost.Window;

                paneElement.Add(new XAttribute("ViewContainerType", "Concord"));
                var concordPaneNode = ConcordWindowManager.BuildWindowXml(window, new XmlDocument());
                var paneNodeElem = XDocument.Parse(concordPaneNode.OuterXml);

                paneElement.Add(paneNodeElem.Root);
            }
            else if (viewContainer.Content is DockPanel &&
                     (viewContainer.Content as DockPanel).Children[0] is AppletFormHost)
            {
                var afh =
                    (viewContainer.Content as DockPanel).Children[0] as AppletFormHost;
                var window = afh.Window;

                paneElement.Add(new XAttribute("ViewContainerType", "Concord"));
                var concordPaneNode = ConcordWindowManager.BuildWindowXml(
                    window, new XmlDocument());
                var paneNodeElem = XDocument.Parse(concordPaneNode.OuterXml);

                paneElement.Add(paneNodeElem.Root);
            }
        }

        #region IPersistable Members

        internal static bool AsynchronousLayoutLoading = false;
        internal static bool SequentialViewLoad = false;
        internal static bool LockFocusWhenLoadingConcordWindow = false;
        internal const int ViewLoadingWaitingTime = 1000;//wait for 1 second after the previous view is fully loaded
        internal const int ViewLoadingTimeout = 60000;//wait for at most 1 min for the loading the previous view

        private void CleanViews(XDocument state)
        {
            // Reset the factory state
            _tileItemManager.Clean();
            _windowManager.Clean();
            // singleton views
            var singletonElement = state.Root.Element("Singletons");
            ((WindowFactory) _windowFactory).ReloadSingletonState(singletonElement);

            // topmost views
            _topmostViews.Clear();
            var topmostElement = state.Root.Element("TopmostViews");
            if (topmostElement != null && DockViewModel.EnableTopmost)
            {
                foreach (var viewElement in topmostElement.Elements("View"))
                {
                    _topmostViews.Add(viewElement.Attribute("ViewId").Value);
                }
            }

            //custom views
            foreach (var wvc in _windows.Where(vc => !vc.Parameters.UseDockManager).ToArray())
            {
                wvc.Close();
            }
        }
         
        public void LoadState(XDocument state)
        {
            if (_isLoadingLayout)
            {
                _windowManager.CancelLoad(true);
                _tileItemManager.CancelLoad(true);
                CancelLayoutLoading.Set();
                if (_isLoadingLayout && AsynchronousLayoutLoading)
                {
                    AutoResetEvent loadedEvent = new AutoResetEvent(false);
                    EventHandler layoutLoaded = null;
                    this.LayoutLoaded += layoutLoaded = (sender_, args_) =>
                    {
                        loadedEvent.Set();
                        this.LayoutLoaded -= layoutLoaded;
                    };
                    loadedEvent.WaitOne();
                }
            }
            try
            {
                CancelLayoutLoading.Reset();

                this.PersistenceProfileService.DoDispatchedAction(new Action(() =>
                    {
                        _windowManager.BeforeLoadState();
                        _tileItemManager.BeforeLoadState();

                    }));
                if (state != null)
                {
                    lock (_isLoadingOrSaving)
                    {
                        var layoutLoadingScreen = _container.Resolve<LayoutLoadingScreenRetriever>().GetScreen();
                        try
                        {
                            _isLoadingLayout = true;
                            layoutLoadingScreen.CancelRequested += LayoutLoadingScreenOnCancelRequested; 
                            this.PersistenceProfileService.DoDispatchedAction(new Action(() =>
                                {
                                    if (DockViewModel.TrackLayoutLoadStatus)
                                    {
                                        layoutLoadingScreen.SetStatus("Closing windows");
                                    }
                                    CleanViews(state); 
                                }));

                            _windowManager.LoadState(state, PersistenceProfileService, (d, a) => ApplyPaneNode(d, a));
                            var tileViewsNode = state.Root.Element("TileViews");
                            if (tileViewsNode == null)
                            {
                                return;
                            }

                            _tileItemManager.LoadState(new XDocument(tileViewsNode), (d, a) => ApplyTileItemNode(d, a));
                        }
                        finally
                        {
                            CancelLayoutLoading.Reset();
                            layoutLoadingScreen.CancelRequested -= LayoutLoadingScreenOnCancelRequested;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ((IStatusBar) this[STATUSBAR_COMMANDAREA_NAME]).MainItem.UpdateStatus(StatusLevel.Critical,
                                                                                      "Failed to load view state.");
                Logger.Warning("Failed to load view state", exc);
            }
            finally
            {
                _isLoadingLayout = false;
                this.PersistenceProfileService.DoDispatchedAction(new Action(() =>
                {
                _tileItemManager.AfterLoadState();
                _windowManager.AfterLoadState();
                    foreach (var window in GetViews())
                    { 
                        var content = window.Content;
                        if (content != null)
                        {
                            IFeedbackWindow feedbackWindow = content as IFeedbackWindow;
                            if (feedbackWindow != null)
                            {
                                feedbackWindow.IsLayoutLoadActive = false;
                            }
                            WindowViewContainer container = window as WindowViewContainer;
                            if (container != null && container.ViewContainerType == ViewContainerType.Concord)
                            {
                                WindowsFormsHost host =
                                    ((DependencyObject) container.Content).FindLogicalChildren<WindowsFormsHost>().FirstOrDefault();
                                if (host != null)
                                {
                                    feedbackWindow = host.Child as IFeedbackWindow;
                                    if (feedbackWindow != null)
                                    {
                                        feedbackWindow.IsLayoutLoadActive = false;
                                    }
                                }
                            }
                        }
                        
                    }
                NativeMethods.LockSetForegroundWindow(
NativeMethods.LSFW_UNLOCK); 
                OnLayoutLoaded();
                }));  
            }
        }

        internal static AutoResetEvent CancelLayoutLoading = new AutoResetEvent(false);
        private void LayoutLoadingScreenOnCancelRequested(object sender, EventArgs eventArgs)
        {
            _windowManager.CancelLoad(false);
            _tileItemManager.CancelLoad(false);
            CancelLayoutLoading.Set();
        }

        private bool AppletCreatorIsMapped(string mappingKey)
        {
            return (AppletCreatorMappings.ContainsKey(mappingKey) // Do we have a simple mapping for this FQCN?
                    ||
                    AppletCreatorFunctionMappings.ContainsKey(mappingKey) // Or is it mapped via a function?
                    ||
                    AppletCreatorSingletonFunctionMappings.ContainsKey(mappingKey)); // Or a function for singletons?
        }


        internal void ApplyPaneNode(XDocument paneNode, object additionalData,
                                    InitialWindowParameters initialParameters = null)
        {
            try
            {
                XElement rootElem = paneNode.Root;
                if (rootElem != null && rootElem.Name == "IView")
                {
                    string viewId = XmlHelper.TryGetAttribute(rootElem, "Id") ?? string.Empty;

                    string viewContainerType = XmlHelper.TryGetAttribute(rootElem, "ViewContainerType");
                    if (viewContainerType == null) return;

                    bool isSingleton = false;
                    // This will be set true if the concord FQN has been mapped to a singleton view.

                    #region Concord view container

                    if (viewContainerType == "Concord")
                    {
                        if ((rootElem.Element("ConcordWindow") != null)
                            && (rootElem.Element("ConcordWindow").Element("ConcordForm") != null)
                            && (rootElem.Element("ConcordWindow").Element("ConcordForm").Attribute("type") != null))
                        {
                            string mappingKey =
                                rootElem.Element("ConcordWindow")
                                        .Element("ConcordForm")
                                        .Attribute("type")
                                        .Value.ToLower();

                            if (AppletCreatorIsMapped(mappingKey))
                            {
                                XElement state =
                                    rootElem.Element("ConcordWindow").Element("ConcordForm").Element(StateElementName);
                                string creatorId;

                                if (AppletCreatorMappings.ContainsKey(mappingKey))
                                {
                                    creatorId = AppletCreatorMappings[mappingKey];
                                    isSingleton = AppletCreatorMappedSingletons.Contains(creatorId);
                                }
                                else if (AppletCreatorFunctionMappings.ContainsKey(mappingKey))
                                {
                                    XAttribute titleAttr = rootElem.Attribute("Title");
                                    string title = String.Empty;

                                    if (titleAttr != null)
                                    {
                                        title = titleAttr.Value;
                                    }

                                    try
                                    {
                                        creatorId = AppletCreatorFunctionMappings[mappingKey](title, state);
                                    }
                                    catch (Exception exc)
                                    {
                                        Logger.Error(
                                            String.Format("Applet creator id conversion failed for title {0}", title),
                                            exc
                                            );
                                        return;
                                    }
                                }
                                else /* if (AppletCreatorFunctionMappings.ContainsKey(mappingKey)) */
                                {
                                    string title = XmlHelper.TryGetAttribute(rootElem, "Title") ?? String.Empty;
                                    try
                                    {
                                        var mappingResult = AppletCreatorSingletonFunctionMappings[mappingKey](
                                            title, state);
                                        creatorId = mappingResult.Item1;
                                        isSingleton = mappingResult.Item2;
                                    }
                                    catch (Exception exc)
                                    {
                                        Logger.Error(
                                            String.Format("Applet creator id conversion failed for title {0}", title),
                                            exc
                                            );
                                        return;
                                    }
                                }

                                rootElem.Attribute("ViewContainerType").SetValue("WithCreator");
                                rootElem.Add(new XAttribute("CreatorID", creatorId));

                                if (AppletConverterMappings.ContainsKey(creatorId) &&
                                    AppletConverterMappings[creatorId] != null)
                                {
                                    try
                                    {
                                        state = AppletConverterMappings[creatorId](state);
                                    }
                                    catch (Exception exc)
                                    {
                                        Logger.Error(
                                            String.Format("Applet XML conversion failed for creator {0}", creatorId),
                                            exc
                                            );
                                    }

                                }

                                rootElem.Add(state);
                                viewContainerType = "WithCreator";
                            }
                        }
                    }

                    #endregion

                    switch (viewContainerType)
                    {
                        case "WithCreator":
                            var stateElem = rootElem.Element(StateElementName);
                            var viewState = rootElem.Element(ViewStateElementName) ?? new XElement(ViewStateElementName);
                            string creatorId = XmlHelper.TryGetAttribute(rootElem, "CreatorID");
                            if (string.IsNullOrEmpty(creatorId)) return;
                            XDocument stateToPass = null;
                            //if (stateElem != null && stateElem.HasElements)

                            if (stateElem == null && OwnerTabInjectToLayout)
                            {
                                stateElem = new XElement(StateElementName);
                            }

                            if (stateElem != null)
                            {
                                var root = stateElem.Elements().FirstOrDefault(
                                    (e) => e.NodeType == XmlNodeType.Element);


                                if (OwnerTabInjectToLayout)
                                {
                                    if (root == null)
                                        root = new XElement("Root");

                                    var containingTab = additionalData as ShellTabViewModel;
                                    //rootElem.Attribute("OwnerTab");
                                    if (containingTab != null)
                                        root.SetAttributeValue("Tab", containingTab.Title);

                                }

                                if (root != null)
                                    stateToPass = new XDocument(root);
                            }
                            var parametersTypeName = XmlHelper.TryGetAttribute(rootElem, "ParametersType");
                            Type parametersType = String.IsNullOrEmpty(parametersTypeName) ? null : Type.GetType(parametersTypeName, false); 

                            var parameters = initialParameters
                                             ??
                                             (parametersType == null
                                                  ? new InitialWindowParameters()
                                                  : (InitialWindowParameters) Activator.CreateInstance(parametersType));

                            parameters.Singleton = isSingleton;
                            var workspaceCategoryAttr = rootElem.Attribute("WorkspaceCategory");
                            if (workspaceCategoryAttr != null)
                            {
                                parameters.WorkspaceCategory = workspaceCategoryAttr.Value;
                            }

                            var titleAttr = rootElem.Attribute("Title");

                            #region View Persistence Related
                             
                            parameters.Persistable = (XmlHelper.TryGetAttribute(rootElem, "Persistable") ?? string.Empty).ToLower() == "true";
                            var persistorTypeAttr = rootElem.Attribute("PersistorType");
                            if (persistorTypeAttr != null && !String.IsNullOrEmpty(persistorTypeAttr.Value))
                            {
                                parameters.PersistorType = Type.GetType(persistorTypeAttr.Value);
                            } 
                            parameters.DynamicComponentPersistors = (XmlHelper.TryGetAttribute(rootElem, "DynamicComponentPersistors") ?? string.Empty).ToLower() == "true"; 

                            #endregion

                            var moduleGroup = _container.Resolve<IModuleGroup>();
                            moduleGroup.CreateWindowFromLayout(
                                creatorId,
                                parameters,
                                viewId,
                                stateToPass,
                                new XDocument(viewState),
                                v => OnWindowCreated(additionalData, v as WindowViewContainer, titleAttr));

                            break;
                        case "Concord":
                            ApplyConcordPaneNode(paneNode, viewId, additionalData, _topmostViews.Contains(viewId));
                            break;
                        default:
                            throw new InvalidOperationException("UnsuportedViewType");
                    }
                }
            }
            catch (Exception exception)
            {
                (this[STATUSBAR_COMMANDAREA_NAME] as IStatusBar).MainItem.UpdateStatus(StatusLevel.Information,
                                                                                       "Failed to load window. Error:" +
                                                                                       exception.Message);
                Logger.Warning("Failed to load window", exception);
            }
        }

        private void OnWindowCreated(object additionalData, WindowViewContainer viewContainer, XAttribute titleAttr)
        {
            if (viewContainer == null) return;
            viewContainer.Topmost = _topmostViews.Contains(viewContainer.ID);
            viewContainer.ViewContainerType = ViewContainerType.WithCreator; 
            if (titleAttr != null && 
                (!ViewTitleRestoreNoOverwrite || IsSavedWorkspace(viewContainer.ID) || 
                (!string.IsNullOrEmpty(viewContainer.Parameters.WorkspaceCategory) && viewContainer is TileViewContainer)))
            {
                viewContainer.Title = titleAttr.Value; 
            }

            if (!ViewLocationNoOverwrite)
                if (viewContainer.Parameters.InitialLocation == InitialLocation.DockBottom ||
                    viewContainer.Parameters.InitialLocation == InitialLocation.DockLeft ||
                    viewContainer.Parameters.InitialLocation == InitialLocation.DockRight ||
                    viewContainer.Parameters.InitialLocation == InitialLocation.DockTabbed ||
                    viewContainer.Parameters.InitialLocation == InitialLocation.DockTop
                    )
                {
                    viewContainer.Parameters.InitialLocation = InitialLocation.Floating;
                    viewContainer.Parameters.InitialLocationTarget = null;
                }
            AddView(viewContainer, additionalData);

        }


        private void ApplyConcordPaneNode(XDocument node_, string viewId_, object tab_, bool topmost_)
        {
            if (ConcordWindowManager == null || node_.Root == null || !node_.Root.HasElements) return;
            XmlNode nodeToApply =
                new XmlDocument().ReadNode(
                    node_.Root.Elements().First((e) => e.NodeType == XmlNodeType.Element).CreateReader());
            ConcordWindowManager.ApplyPaneNode(nodeToApply, viewId_, tab_, topmost_);
            //TODO titles are not persisted for Concord windows

        }

        internal void ApplyTileItemNode(XDocument tileNode, TileViewModel tileView,
                                        InitialTileItemParameters initialParameters = null)
        {
            try
            {
                XElement rootElem = tileNode.Root;
                if (rootElem != null && rootElem.Name == "IView")
                {
                    XAttribute idAttribute = rootElem.Attribute("Id");
                    string viewId = String.Empty;
                    if (idAttribute != null)
                    {
                        viewId = idAttribute.Value;
                    }
                    string viewContainerType = XmlHelper.TryGetAttribute(rootElem, "ViewContainerType");
                    if (viewContainerType == null) return;
                    switch (viewContainerType)
                    {
                        case "WithCreator":
                            var stateElem = rootElem.Element(StateElementName);
                            var viewState = rootElem.Element(ViewStateElementName) ?? new XElement(ViewStateElementName);
                            var creatorIdAttr = rootElem.Attribute("CreatorID");
                            var titleAttr = rootElem.Attribute("Title");
                            if (creatorIdAttr != null && !String.IsNullOrEmpty(creatorIdAttr.Value))
                            {
                                string creatorId = creatorIdAttr.Value;
                                if (stateElem == null)
                                {
                                    stateElem = new XElement(StateElementName);
                                }

                                var parameters = initialParameters ?? new InitialTileItemParameters();
                                double width = Double.NaN, left = Double.NaN, top = Double.NaN, height = Double.NaN;

                                var widthAttr = rootElem.Attribute("Width");
                                if (widthAttr != null && !String.IsNullOrEmpty(widthAttr.Value))
                                {
                                    if (Double.TryParse(widthAttr.Value, out width) && !Double.IsNaN(width))
                                    {
                                        parameters.Width = width;
                                        parameters.SizingMethod = SizingMethod.Custom;
                                    }
                                }
                                var heightAttr = rootElem.Attribute("Height");
                                if (heightAttr != null && !String.IsNullOrEmpty(heightAttr.Value))
                                {
                                    if (Double.TryParse(heightAttr.Value, out height) && !Double.IsNaN(height))
                                    {
                                        parameters.Height = height;
                                        parameters.SizingMethod = SizingMethod.Custom;
                                    }
                                }
                                var leftAttr = rootElem.Attribute("Left");
                                if (leftAttr != null && !String.IsNullOrEmpty(leftAttr.Value))
                                {
                                    if (Double.TryParse(leftAttr.Value, out left))
                                    {
                                        parameters.Left = left;
                                        parameters.InitialLocationCallback = null;
                                    }
                                }
                                var topAttr = rootElem.Attribute("Top");
                                if (topAttr != null && !String.IsNullOrEmpty(topAttr.Value))
                                {
                                    if (Double.TryParse(topAttr.Value, out top))
                                    {
                                        parameters.Top = top;
                                        parameters.InitialLocationCallback = null;
                                    }
                                }

                                #region View Persistence Related 
                                parameters.Persistable = (XmlHelper.TryGetAttribute(rootElem, "Persistable") ?? string.Empty).ToLower() == "true";
                                var persistorTypeAttr = rootElem.Attribute("PersistorType");
                                if (persistorTypeAttr != null && !String.IsNullOrEmpty(persistorTypeAttr.Value))
                                {
                                    parameters.PersistorType = Type.GetType(persistorTypeAttr.Value);
                                }
                                parameters.DynamicComponentPersistors = (XmlHelper.TryGetAttribute(rootElem, "DynamicComponentPersistors") ?? string.Empty).ToLower() == "true"; 
                                #endregion

                                var tileItemContainer =
                                    _windowFactory.CreateWindowInstance(creatorId, parameters, viewId, true,
                                                                        new XDocument(stateElem), new XDocument(viewState));
                                if (!Double.IsNaN(left))
                                {
                                    parameters.Left = left;
                                    parameters.InitialLocationCallback = null;
                                }
                                if (!Double.IsNaN(top))
                                {
                                    parameters.Top = top;
                                    parameters.InitialLocationCallback = null;
                                }

                                if (!Double.IsNaN(width))
                                {
                                    parameters.Width = width;
                                    parameters.SizingMethod = SizingMethod.Custom;
                                }
                                if (!Double.IsNaN(height))
                                {
                                    parameters.Height = height;
                                    parameters.SizingMethod = SizingMethod.Custom;
                                }

                                var lastRightEdgeMargin = rootElem.Attribute("LastRightEdgeMargin");
                                if (lastRightEdgeMargin != null && !String.IsNullOrEmpty(lastRightEdgeMargin.Value))
                                {
                                    double rightEdgeMargin;
                                    if (Double.TryParse(lastRightEdgeMargin.Value, out rightEdgeMargin))
                                    {
                                        ((TileItemViewContainer)tileItemContainer).LastRightEdgeMargin = rightEdgeMargin;
                                    }
                                }
                                var lastBottomEdgeMargin = rootElem.Attribute("LastBottomEdgeMargin");
                                if (lastBottomEdgeMargin != null && !String.IsNullOrEmpty(lastBottomEdgeMargin.Value))
                                {
                                    double bottomEdgeMargin;
                                    if (Double.TryParse(lastBottomEdgeMargin.Value, out bottomEdgeMargin))
                                    {
                                        ((TileItemViewContainer)tileItemContainer).LastBottomEdgeMargin = bottomEdgeMargin;
                                    }
                                }

                                if (!ViewTitleRestoreNoOverwrite && titleAttr != null && !String.IsNullOrEmpty(titleAttr.Value))
                                {
                                    tileItemContainer.Title = titleAttr.Value;
                                }

                                if (!SequentialViewLoad)
                                {
                                    PersistenceProfileService.DoDispatchedAction(() => AddTileItem(tileItemContainer, tileView.ViewContainer as ITileViewContainer));
                                }
                                else
                                {
                                    AutoResetEvent loadedEvent = new AutoResetEvent(false);
                                    _persistenceProfileService.DoDispatchedAction(() =>
                                    {
                                        EventHandler<EventArgs> fullyLoaded = null;
                                        IFeedbackWindow feedbackWindow = null;
                                        RoutedEventHandler loaded = null;
                                        FrameworkElement element = null;
                                        try
                                        {
                                            var window = AddTileItem(tileItemContainer, tileView.ViewContainer as ITileViewContainer);
                                            if (window == null)
                                            {
                                                loadedEvent.Set();
                                                return;
                                            }
                                            feedbackWindow = window.Content as IFeedbackWindow;
                                            if (feedbackWindow != null)
                                            {
                                                feedbackWindow.IsLayoutLoadActive = true;
                                                feedbackWindow.FullyLoaded += fullyLoaded = (sender_, args_) =>
                                                {
                                                    loadedEvent.Set();
                                                    feedbackWindow.FullyLoaded -= fullyLoaded;
                                                    fullyLoaded = null;
                                                };
                                            }
                                            else
                                            {
                                                element = window.Content as FrameworkElement;
                                                if (element == null || element.IsLoaded)
                                                {
                                                    loadedEvent.Set();
                                                    return;
                                                }
                                                element.Loaded += loaded = (sender_, args_) =>
                                                {
                                                    loadedEvent.Set();
                                                    element.Loaded -= loaded;
                                                    loaded = null;
                                                };

                                            }
                                        }
                                        catch
                                        {
                                            if (fullyLoaded != null && feedbackWindow != null)
                                            {
                                                feedbackWindow.FullyLoaded -= fullyLoaded;
                                            }
                                            else if (loaded != null && element != null)
                                            {
                                                element.Loaded -= loaded;
                                            }
                                            loadedEvent.Set();
                                            throw;
                                        }
                                    }, false);
                                    if (WaitHandle.WaitAny(new WaitHandle[] { loadedEvent, CancelLayoutLoading }, ViewLoadingTimeout) == 0)
                                    {
                                        //if (loadedEvent.WaitOne(ChromeManagerBase.ViewLoadingTimeout))  
                                        Thread.Sleep(ViewLoadingWaitingTime);
                                    }
                                }
                            }
                            break;

                        default:
                            throw new InvalidOperationException("UnsuportedViewType");
                    }

                }

            }
            catch (Exception exception)
            {
                (this[STATUSBAR_COMMANDAREA_NAME] as IStatusBar).MainItem.UpdateStatus(StatusLevel.Information,
                                                                                       "Failed to load tile item. Error:" +
                                                                                       exception.Message);
                Logger.Warning("Failed to load tile item", exception);
            }
        }

        public XDocument SaveState()
        {
            //when loaded synchronously, this "SaveState" method might be called due to some fix for concord form in the end of the layout loading process
            //we should not wait for the layout loaded event in this case, to avoid a deadlock
            if (_isLoadingLayout && AsynchronousLayoutLoading)
            {
                AutoResetEvent loadedEvent = new AutoResetEvent(false);
                EventHandler layoutLoaded = null;
                this.LayoutLoaded += layoutLoaded = (sender_, args_) =>
                    {
                        loadedEvent.Set();
                        this.LayoutLoaded -= layoutLoaded;
                    };

                WaitHandle.WaitAny(new WaitHandle[] { loadedEvent, CancelLayoutLoading }, ViewLoadingTimeout);
                //loadedEvent.WaitOne();
            }
            try
            {
                _windowManager.BeforeSaveState();
                _tileItemManager.BeforeSaveState();
                lock (_isLoadingOrSaving)
                {
                    _layoutExceptions.Clear();

                    var tempState = _windowManager.SaveState(BuildIViewXml);


                    // singleton windows
                    var singletionElement = new XElement("Singletons");

                    ((WindowFactory) _windowFactory).SaveSingletonState(singletionElement);

                    if (tempState.Root != null)
                        tempState.Root.Add(singletionElement);

                    // topmost windows
                    var topmostElement = new XElement("TopmostViews");
                    foreach (var vc in Windows)
                    {
                        if (((WindowViewContainer) vc).Topmost)
                        {
                            topmostElement.Add(new XElement("View", new XAttribute("ViewId", vc.ID)));
                        }
                    }
                    if (tempState.Root != null)
                        tempState.Root.Add(topmostElement);

                    try
                    {
                        var tempState2 = _tileItemManager.SaveState(BuildIViewXml);
                        if (tempState.Root != null)
                            tempState.Root.Add(tempState2.Root);

                    }
                    catch (Exception exc)
                    {
                        Logger.Error("Error in tile save", exc);
                    }

                    // graceful degradation
                    if (_layoutExceptions.Count > 0)
                    {
                        Logger.Error(
                            String.Format("Saving failed for the following views: {0}",
                                          String.Join(", ", _layoutExceptions.Keys.ToArray())),
                            new AggregateException(_layoutExceptions.Values.SelectMany(list => list)));
                        ((IStatusBar) this[STATUSBAR_COMMANDAREA_NAME]).MainItem.UpdateStatus(
                            StatusLevel.Critical,
                            String.Format(
                                "Saving failed for the following views: {0}",
                                String.Join(", ", _layoutExceptions.Keys.ToArray())));
                        _layoutExceptions.Clear();
                    }



                    return tempState;
                }
            }
            catch (Exception exc)
            {
                Logger.Error("Exception happened during save", exc);
                ((IStatusBar) this[STATUSBAR_COMMANDAREA_NAME]).MainItem.UpdateStatus(StatusLevel.Critical,
                                                                                      "Failed to save view state.");
                return null;
            }
            finally
            {
                _tileItemManager.AfterSaveState();
                _windowManager.AfterSaveState();
            }
        } 

        public void LoadSingleView(XDocument state, InitialWindowParameters parameters)
        {
            ApplyPaneNode(state, null, parameters);
        }

        public string PersistorId
        {
            get { return "MSGui.Internal.ViewManager"; }
        }

        #endregion

        #endregion

        public event DependencyPropertyChangedEventHandler PropertyChanged;

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            var copy = PropertyChanged;
            if (copy != null)
            {
                copy(this, e);
            }
        }


        #region ChromeManager 2.0

        #region Attached property to store factoryID/widget ID

        public static string GetWidgetID(DependencyObject obj)
        {
            return (string) obj.GetValue(WidgetIDProperty);
        }

        public static void SetWidgetID(DependencyObject obj, string value)
        {
            obj.SetValue(WidgetIDProperty, value);
        }

        // Using a DependencyProperty as the backing store for WidgetID.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WidgetIDProperty =
            DependencyProperty.RegisterAttached("WidgetID", typeof (string), typeof (ChromeManagerBase),
                                                new UIPropertyMetadata(String.Empty));


        #endregion


        protected void AddRootArea(string ID, InitialWidgetParameters parameters)
        {
            var widget = AddWidget(ID, parameters);
            if (widget != null)
            {
                this[ID] = widget;
            }
        }

        protected virtual string GetDefaultArea()
        {
            return String.Empty;
        }

        internal abstract IWidgetViewContainer GetRootContainer();

        public IWidgetViewContainer this[string index]
        {
            get { return RootWidgets[(String.IsNullOrWhiteSpace(index) ? GetDefaultArea() : index).ToLower()]; }
            internal set
            {
                if (!RootWidgets.ContainsKey(index.ToLower()))
                {
                    RootWidgets.Add(index.ToLower(), value);
                }
            }
        }

        public IWidgetViewContainer AddWidget(string factoryId, InitialWidgetParameters parameters)
        {
            return AddWidget(factoryId, parameters, null);
        }

        public IWidgetViewContainer AddWidget(string factoryId, InitialWidgetParameters parameters, IWidgetViewContainer parent)
        {
            return AddWidget(factoryId, parameters, parent, false);
        }

        internal IWidgetViewContainer AddWidget(string factoryId, InitialWidgetParameters parameters,
                                                IWidgetViewContainer parent, bool disableImplicitCreation)
        {
            if (LockerBlackHoleEnabled && parent != null && !(parameters is ILockerPassThrough))
            {
                if (LockerAffectsMainAreasOnly)
                {
                    var root = parent;

                    while (!_rootWidgets.ContainsValue(root) && root != null)
                        // shell tabs have no parents for now so allow for parent to be null
                        root = root.Parent;

                    if (root is ILockerAffectedMainArea)
                        parent = this[LOCKER_COMMANDAREA_NAME];
                }
                else
                    parent = this[LOCKER_COMMANDAREA_NAME];
            }

            if (!_rootWidgets.ContainsKey(factoryId))
            {
                var widget = GetWidget(factoryId);

                //if (WillLoadChromeFromState)
                //{
                //    if (this.RootWidgets.ContainsKey(Core.ChromeManager.Helpers.ChromeArea.QAT.ToLower()) && 
                //        parent == this[Core.ChromeManager.Helpers.ChromeArea.QAT])
                //    {
                //        if (widget != null) return null;                    
                //        parent = this[LOCKER_COMMANDAREA_NAME];
                //    }
                //}

                var widgetViewContainerParent = (WidgetViewContainer) parent;

                if (widget != null && parent != null)
                {
                    if (widgetViewContainerParent.IsAuxiliary)
                    {
                        return AddWidgetClone(widget, parent);
                    }

                    if ((widget.Parent == this[LOCKER_COMMANDAREA_NAME] || parameters == null ||
                        String.IsNullOrEmpty(parameters.Text) ||
                         parameters.GetType() == typeof (InitialWidgetParameters)) &&
                        ((WidgetViewContainer) parent).CanAddwidget(widget))
                    {
                        return MoveWidget(widget, parent, parameters);
                    }
                }

                if (parent != null && parameters == null && !disableImplicitCreation)
                {
                    parameters = widgetViewContainerParent.AddDefault(ref factoryId);
                }

                var vc = (WidgetViewContainer) _widgetFactory.CreateWidgetInstance(factoryId, parameters);
                vc.Parent = parent;

                if (CreateChromeCalled)
                {
                    if (CreateWidget(vc))
                    {
                        //this[ID] = vc;
                        if (!(vc is ShellTabViewModel))
                            _placedWidgets.Add(vc.FactoryID, vc);
                        if (parent != null)
                        {
                            widgetViewContainerParent.AddLogical(vc);
                            widgetViewContainerParent.AddVisual(vc);
                        }
                    }
                }
                else
                {
                    var key = (parent == null) ? this : (object) parent;
                    if (!_registrations.ContainsKey(key))
                    {
                        var coll = new ObservableCollection<IWidgetViewContainer>(); 
                        _registrations.Add(key, coll);
                    }

                    _registrations[key].Add(vc);

                    if (parent != null)
                    {
                        ((WidgetViewContainer) parent).AddLogical(vc);
                    }

                }

                return vc;
            }

            return this[factoryId];
        }
         
        internal IWidgetViewContainer MoveWidget(IWidgetViewContainer widget, IWidgetViewContainer newParent,
                                                 InitialWidgetParameters parameters,
                                                 IWidgetViewContainer oldParent = null)
        {
            var widgetViewContainerParent = (WidgetViewContainer) widget.Parent;
            var widgetViewContainerNewParent = (WidgetViewContainer) newParent;
            if (CreateChromeCalled)
            {
                widgetViewContainerParent.RemoveVisual(widget);
                widgetViewContainerParent.RemoveLogical(widget);

                ((WidgetViewContainer) widget).Parent = newParent;
                widgetViewContainerNewParent.AddLogical(widget);
                widgetViewContainerNewParent.AddVisual(widget);
                if (parameters != null)
                    Logger.InfoWithFormat("Widget {0} parameters were tried to set after chrome creation",
                                            widget.FactoryID);
            }
            else
            {
                if (widget.Parent != null)
                {
                    _registrations[widget.Parent].Remove(widget);
                    widgetViewContainerParent.RemoveLogical(widget);
                }

                if ((parameters != null) && (widget.Parameters != null))
                {
                    if (!widget.Parameters.Merge(parameters)) 
                    {
                        Logger.WarningWithFormat("Failed to merge Widget {0} parameter {2} into {1}", widget.FactoryID,
                                                   widget.Parameters.GetType().FullName, parameters.GetType().FullName);
                    } 
                }

                if (!_registrations.ContainsKey(newParent))
                {
                    var coll = new ObservableCollection<IWidgetViewContainer>();
                    _registrations.Add(newParent, coll);
                }

                ((WidgetViewContainer) widget).Parent = newParent;
                _registrations[newParent].Add(widget);
                widgetViewContainerNewParent.AddLogical(widget);
            }

            return widget;
        }

        private IWidgetViewContainer AddWidgetClone(IWidgetViewContainer widget, IWidgetViewContainer newParent)
        {
            var widgetViewContainerNewParent = (WidgetViewContainer) newParent;
            if (CreateChromeCalled)
            {
                widgetViewContainerNewParent.AddLogical(widget);
                widgetViewContainerNewParent.AddVisual(widget);
            }
            else
            {
                if (!_registrations.ContainsKey(newParent))
                {
                    _registrations.Add(newParent, new ObservableCollection<IWidgetViewContainer>());
                }

                _registrations[newParent].Add(widget);
                widgetViewContainerNewParent.AddLogical(widget);
            }

            return widget;
        }

        internal bool CreateWidget(IWidgetViewContainer viewContainer)
        {
            WidgetFactoryHolder factoryHolder;
            InitializeWidgetHandler initHandler = null;
            bool result = false;
            if (viewContainer.Parameters != null &&
                _registry.WidgetFactoryMapping.TryGetValue(viewContainer.Parameters.GetType(), out factoryHolder))
            {  
                initHandler = factoryHolder.BuildUp();
            }
            if (initHandler != null)
            {
                result = initHandler(viewContainer, null);
                if (!result) return false;
            }
             

            factoryHolder = this._registry.GetWidgetFactory(viewContainer.FactoryID, viewContainer.Parent);
            if (factoryHolder != null)
            {
                initHandler = factoryHolder.BuildUp();
                if (initHandler != null)
                {
                    return initHandler(viewContainer, null);
                }
            }

            return result;
        }

        private void CreateChromeArea(IWidgetViewContainer widget, ICollection<string> placedFactories)
        {
            ObservableCollection<IWidgetViewContainer> widgets;
            if (!_registrations.TryGetValue(widget, out widgets)) return;
            var widgetsCopied = widgets.ToArray();
            foreach (var child in widgetsCopied)
            {
                var widgetViewContainer = (WidgetViewContainer)widget;
                var childViewContainer = (WidgetViewContainer)child;

                if (widgetViewContainer.IsAuxiliary)
                {
                    if (child.ContentOfType<DependencyObject>())
                    {
                        SetWidgetID((DependencyObject)child.Content, child.FactoryID);
                    }

                    widgetViewContainer.AddVisual(childViewContainer);
                    widgetViewContainer.AddLogical(childViewContainer);
                    placedFactories.Add(child.FactoryID);
                }
                else if (CreateWidget(child))
                {
                    if (child.ContentOfType<DependencyObject>())
                    {
                        SetWidgetID((DependencyObject)child.Content, child.FactoryID);
                    }

                    widgetViewContainer.AddVisual(childViewContainer);
                    widgetViewContainer.AddLogical(childViewContainer);
                    placedFactories.Add(child.FactoryID);

                    PlacedWidgets.Add(child.FactoryID, child);
                    CreateChromeArea(child, placedFactories);
                }
                else
                {
                    widgetViewContainer.RemoveVisual(childViewContainer);
                    widgetViewContainer.RemoveLogical(childViewContainer);
                }
            }

        }

        #region Widget persistence

        /// <summary>
        /// Null means that there will be no default widget dehydration callback
        /// registrations.
        /// </summary>
        internal static bool? DefaultWidgetFactoriesGlobal { get; set; }

        internal ChromeRegistry Registry
        {
            get { return _registry; }
        }

        internal Dictionary<object, ObservableCollection<IWidgetViewContainer>> Registrations
        {
            get { return _registrations; }
        }

        internal Dictionary<string, IWidgetViewContainer> RootWidgets
        {
            get { return _rootWidgets; }
        }

        internal Dictionary<string, IWidgetViewContainer> PlacedWidgets
        {
            get { return _placedWidgets; }
        }

        public bool LoadWidgetWithState(string id, XDocument state)
        {
            Logger.DebugWithFormat("Creating widget with state: {0}", id);

            var result = false;
            var viewContainer = GetWidget(id);

            WidgetFactoryHolder factoryHolder = null;
            if (Registry.WidgetFactories.TryGetValue(viewContainer.FactoryID, out factoryHolder) ||
                Registry.WidgetFactoryMapping.TryGetValue(viewContainer.Parameters.GetType(), out factoryHolder))
            {
                InitializeWidgetHandler initHandler = factoryHolder.BuildUp();
                if (initHandler != null)
                {
                    result = initHandler(viewContainer, state);
                }
            } 

            return result;
        }

        public IEnumerable<string> GetPersistableWidgets(bool globalDehydrateHandler)
        {
            var results = Registry.WidgetFactories
                                    .Where(
                                        wfh => (globalDehydrateHandler && wfh.Value.GlobalDehydrateHandler != null) ||
                                               (!globalDehydrateHandler && wfh.Value.DehydrateHandler != null))
                                    .Select(wfh => wfh.Key);
            if (globalDehydrateHandler == DefaultWidgetFactoriesGlobal)
            {
                results = results.Union(
                    PlacedWidgets.Values
                                 .Where(
                                     wvc_ =>
                                     wvc_.Parameters != null &&
                                     Registry.WidgetFactoryMapping.ContainsKey(wvc_.Parameters.GetType()) &&
                                      Registry.WidgetFactoryMapping[wvc_.Parameters.GetType()].DehydrateHandler != null)
                                 .Select(wvc_ => wvc_.FactoryID)
                    );
            }
            return results;
        }
 

        public XDocument SaveWidgetState(string id)
        {
            WidgetFactoryHolder factoryHolder = null;
            DehydrateWidgetHandler dehydrateHandler = null;
            if (Registry.WidgetFactories.TryGetValue(id, out factoryHolder))
            {
                dehydrateHandler = factoryHolder.DehydrateHandler ?? factoryHolder.GlobalDehydrateHandler;
            }
            var wvc = GetWidget(id);
            if (dehydrateHandler == null && Registry.WidgetFactoryMapping.TryGetValue(wvc.Parameters.GetType(), out factoryHolder))
            {
                dehydrateHandler = factoryHolder.DehydrateHandler;
            }
            if (dehydrateHandler == null) return null;
            return dehydrateHandler(wvc);
        }

        private IWidgetViewContainer FindWidget(object subtree, string id)
        {
            if (Registrations.ContainsKey(subtree))
            {
                foreach (var child in Registrations[subtree])
                {
                    if (child.FactoryID == id) return child;
                    var childSubtreeResult = FindWidget(child, id);
                    if (childSubtreeResult != null)
                    {
                        return childSubtreeResult;
                    }
                }
            }
            return null;
        }

        #endregion

        #endregion

        public void RegisterFakeFactory(string ID, object content, string header)
        {
            if (_registry.WindowFactories.ContainsKey(ID)) return;
            _registry.RegisterWindowFactory(ID, (vc, s) =>
                {
                    vc.Content = content;
                    vc.Title = header;
                    return true;
                });
        }

        public XDocument GetWindowState(IWindowViewContainer window)
        {
            var windowXml = BuildIViewXml(window as WindowViewContainer);
            var result = new XDocument();
            result.Add(windowXml);
            return result;
        }

        internal event EventHandler<WindowGroupDroppedOnScreenEdgeEventArgs> WindowGroupDroppedOnScreenEdge;
        public event EventHandler<ViewDroppedOnScreenEdgeEventArgs> ViewDroppedOnScreenEdge;

        public void InvokeViewDroppedOnScreenEdge(ViewDroppedOnScreenEdgeEventArgs e)
        {
            var handler = ViewDroppedOnScreenEdge;
            if (handler != null) handler(this, e);
        }

        internal void InvokeWindowGroupDroppedOnScreenEdge(WindowGroupDroppedOnScreenEdgeEventArgs e)
        {
            var handler = WindowGroupDroppedOnScreenEdge;
            if (handler != null) handler(this, e);
        }

        public void UpdateWindowFromState(string factoryID, IWindowViewContainer viewContainer, XDocument savedState)
        {
            WindowFactoryHolder factory;
            if (viewContainer.Parameters != null && ((IChromeElementInitializerRegistry)_registry).TryGetWindowFactoryHolder(viewContainer.Parameters.GetType(), out factory))
            {
                try
                {
                    if (!factory.InitHandler(viewContainer, savedState)) return;

                }
                catch (Exception exception)
                {
                    Logger.Error(String.Format("Init handler failed for window factory with parameter type {0}", viewContainer.Parameters.GetType()),
                                   exception);
                }
            }
            if (!String.IsNullOrEmpty(factoryID) && 
                ((IChromeElementInitializerRegistry) _registry).TryGetWindowFactoryHolder(factoryID, out factory))
            {
                try
                {
                    factory.InitHandler(viewContainer, savedState);
                }
                catch (Exception exception)
                {
                    Logger.Error(String.Format("Init handler failed for window factory with factory name {0}", factoryID),
                                   exception);
                }
            }
        }

    }

    public class ViewDroppedOnScreenEdgeEventArgs : EventArgs
    {
        public IWindowViewContainer View { get; set; }
        public ScreenEdge Edge { get; set; }
    }

    internal class WindowGroupDroppedOnScreenEdgeEventArgs : EventArgs
    {
        private readonly IFloatingWindow _floatingWindow;
        private readonly ScreenEdge _edge;
        private bool _closeAfterProcessing;

        public WindowGroupDroppedOnScreenEdgeEventArgs(IFloatingWindow floatingWindow_, ScreenEdge edge)
        {
            _edge = edge;
            _floatingWindow = floatingWindow_;
        }

        public IFloatingWindow FloatingWindow
        {
            get { return _floatingWindow; }
        }

        public ScreenEdge Edge
        {
            get { return _edge; }
        }

        public bool CloseWindowAfterProcessing
        {
            get { return _closeAfterProcessing; }
        }

        public void MarkWindowForClosing()
        {
            _closeAfterProcessing = true;
        }
    }
}
