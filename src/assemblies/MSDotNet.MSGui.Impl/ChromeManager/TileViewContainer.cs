﻿using System.Collections.ObjectModel;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
#pragma warning disable 612,618
    internal sealed class TileViewContainer : WindowViewContainer, ITileViewContainer
#pragma warning restore 612,618
    { 
        public TileViewContainer(string factoryId_):base(factoryId_)
        {    
        }

        public TileViewContainer(InitialTileParameters parameters_, string factoryId_):
            base(parameters_, factoryId_)
        { 
        }

        private WindowViewModel model;
        public override WindowViewModel GetViewModel(IHidingLockingManager hidingLockingManager_)
        {
            return model ?? (model = new TileViewModel(this, hidingLockingManager_));
        }

        public new InitialTileParameters Parameters
        {
            get { return (InitialTileParameters)base.Parameters; } 
        }

        private readonly ObservableCollection<IWindowViewContainer> children = new ObservableCollection<IWindowViewContainer>(); 
        public ObservableCollection<IWindowViewContainer> Children { get { return children; } }
         

        private double zoom = 1d;
        public double Zoom
        {
            get { return zoom; }
            set
            { 
                zoom = value;
                OnPropertyChanged("Zoom");
            }
        }

        internal override object V2vHost
        {
            get
            {
                return base.V2vHost;
            }
            set
            {
                base.V2vHost = value;
                foreach (WindowViewContainer child in this.Children)
                {
                    child.V2vHost = value;
                }
            }
        }


        private double widthChangeFactor = 1d;
        private double width;
        public override double Width
        {
            get { return width; }
            set
            {
                if (!CoreUtilities.AreClose(width, value))
                {
                    FrameworkElement content = Visual as FrameworkElement;
                    if (content == null || !content.IsLoaded ||
                        CoreUtilities.AreClose(width, 0) ||
                        CoreUtilities.AreClose(value, 0))
                    {
                        widthChangeFactor = 1d;
                    }
                    else
                    {
                        widthChangeFactor = value / width;
                    }

                    width = value;
                    OnPropertyChanged("Width");
                }
            }
        }

        public double WidthChangeFactor
        {
            get { return widthChangeFactor; }
        }

        private double heightChangeFactor = 1d;
        private double height;
        public override double Height
        {
            get { return height; }
            set
            {
                if (!CoreUtilities.AreClose(height, value))
                {
                    FrameworkElement content = Visual as FrameworkElement;
                    if (content == null || !content.IsLoaded ||
                        CoreUtilities.AreClose(height, 0) ||
                        CoreUtilities.AreClose(value, 0))
                    {
                        heightChangeFactor = 1d;
                    }
                    else
                    {
                        heightChangeFactor = value / height;
                    }
                    height = value;
                    OnPropertyChanged("Height");
                }
            }
        }

        public double HeightChangeFactor
        {
            get { return heightChangeFactor; }
        }
        public void ClearChangeFactors()
        {
            heightChangeFactor = widthChangeFactor = 1d;
        }
         
    }
}
