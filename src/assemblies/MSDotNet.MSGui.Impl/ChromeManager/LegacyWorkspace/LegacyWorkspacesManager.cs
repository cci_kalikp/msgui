﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;  
using System.Xml;
using System.Xml.Linq;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent; 
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Workspaces;
using MorganStanley.MSDotNet.MSGui.Impl.Concord; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
    public partial class ChromeManagerBase:ILegacyWorkspacesManager
    {
        private static Workspaces _workspaces;
        public IWorkspaces Workspaces
        {
            get
            {
                if (WorkspaceSaveLoadEnabled)
                    return _workspaces;
                return null;
            }
        }


        /// <summary>
        /// Create new workspace for the island that contains the seed view.
        /// </summary>
        /// <param name="seedView"></param>
        /// <param name="name"></param>
        public IWorkspace CreateWorkspace(IWindowViewContainer seedView, string name)
        {
            if (String.IsNullOrEmpty(name))
                throw new ApplicationException("Workspace name should not be empty");
             
            var root = DockHelper.GetRootOfFloatingPane(seedView); 
            if (root == null) //Not CustomPaneToolWindow
                return null;
            var ws = _workspaces.Cast<Workspace>().FirstOrDefault(wss => wss.Root == root);
            if (ws == null)
            { 
                ws = new Workspace(this, name, root);
                Workspaces.Add(ws);

                root.Title = name;
            }
            return ws;

        }
         

        public IEnumerable<string> GetViewFactoryIds(string workspace)
        {
            // load from CC
            var config =
                (Configurator)
                ConfigurationManager.GetConfig("MSDesktopSubLayout_" + XmlConvert.EncodeLocalName(workspace));
            var xmlTemp = config.GetNode("./SubLayout", null) as XmlElement;
            if (xmlTemp == null)
                return Enumerable.Empty<string>();

            var subLayoutXml = XElement.Parse(xmlTemp.OuterXml);

            // load the xml
            var viewsElement = subLayoutXml.Elements().FirstOrDefault(xe => xe.Name == "Views");

            if (viewsElement == null)
                return new string[0];

            var ids =
                viewsElement.Elements()
                            .Where(element => element.Attribute("CreatorID") != null)
                            .Select(element => element.Attribute("CreatorID").Value);

            return ids;
        }

        public void SaveViews(IEnumerable<IWindowViewContainer> views, string name)
        {
            //Load from CC
            var namesConfig = (Configurator)ConfigurationManager.GetConfig("SubLayoutNames");

            var nameRoot = namesConfig.GetNode("/SubLayoutNames", null);


            if (nameRoot == null || nameRoot.SelectNodes("//Workspace[@Name='" + name + "']").Count == 0)
            {
                var nameDoc = new XmlDocument() { InnerXml = "<Workspace Name='" + name + "'/>" };

                namesConfig.AddNode("/SubLayoutNames", null, nameDoc.DocumentElement);
                namesConfig.Save();
            }
             
            if (views == null)
                return;
            var subLayout = DockHelper.GetNormalizedSubLayout(views);

            var viewsElement = new XElement("Views");

            var subLayoutElement = new XElement(DockLayoutRootName);
            var xml = new XDocument(new XElement("SubLayout", viewsElement, subLayoutElement));

            foreach (WindowViewContainer view in views)
            {
                viewsElement.Add(BuildIViewXml(view));
            }

            // get the layout
            var xamLayoutElement = _windowManager.WriteSubState(subLayout, views);
            if (xamLayoutElement == null)
                return;

            subLayoutElement.Add(xamLayoutElement);

            var xn = new XmlDocument
            {
                InnerXml = String.Format("<wrapper>{0}</wrapper>", xml)
            };

            // save to CC
            var config =
                (Configurator)ConfigurationManager.GetConfig("MSDesktopSubLayout_" + XmlConvert.EncodeLocalName(name));
            config.SetNode(".", null, xn.DocumentElement);
            config.Save();

            // CC hack to clear the cache
            ConfigurationManagerHelper.DirtyConfiguration("MSDesktopSubLayout_" + XmlConvert.EncodeLocalName(name)); 
        }

        public IEnumerable<string> GetSubLayoutNames()
        {
            // load from CC
            var rt = (Configurator)ConfigurationManager.GetConfig("SubLayoutNames");

            var root = rt.GetNode("/*", null);
            if (root == null)
                return Enumerable.Empty<string>();

            var list = root.SelectNodes("//Workspace");


            var nameSet = new HashSet<string>();
            foreach (var element in list)
            {
                var node = element as XmlNode;
                if (node == null)
                    continue;

                var name = node.Attributes["Name"];
                nameSet.Add(name.Value);
            }
            return nameSet;
        }

        public void LoadViews(string subLayoutName)
        {
            // load from CC
            var config =
                (Configurator)
                ConfigurationManager.GetConfig("MSDesktopSubLayout_" + XmlConvert.EncodeLocalName(subLayoutName));
            var xmlTemp = config.GetNode("./SubLayout", null) as XmlElement;
            if (xmlTemp == null)
                return;

            var subLayoutXml = XElement.Parse(xmlTemp.OuterXml);

            // load the xml
            var viewsElement = subLayoutXml.Elements().FirstOrDefault(xe => xe.Name == "Views");

            var subLayoutElement = subLayoutXml.Elements().FirstOrDefault(xe => xe.Name == DockLayoutRootName);
            if (subLayoutElement == null) return;
            // load the views
            var windows = _windowManager.ReadSubState(subLayoutName, subLayoutElement, (storedId, newId) =>
            {
                var viewElement = viewsElement
                    .Elements()
                    .FirstOrDefault(xe => xe.
                                              Attributes().
                                              Any(
                                                  attr =>
                                                  attr.Name == "Id" && ContentPaneIdConverter.ConvertToPaneId(attr.Value) == storedId));

                if (viewElement == null)
                    return;

                newId = ContentPaneIdConverter.ConverFromPaneId(newId);

                viewElement.SetAttributeValue("Id", newId);

                ApplyPaneNode(new XDocument(viewElement), null);
            });

            foreach (var window in windows)
            {
                IFloatingWindow floatingWindow = window as IFloatingWindow;
                if (floatingWindow == null) continue;
                floatingWindow.SubLayoutTitle = subLayoutName;

                var ws = new Workspace(this, subLayoutName, floatingWindow);

                // Check if we have Workspaces saving functionality enabled.
                if (this.Workspaces != null)
                {
                    this.Workspaces.Add(ws);

                }
            } 
        }

        internal static bool WorkspaceSaveLoadEnabled;

        internal void SaveWindow(IFloatingWindow floatingWindow_)
        {
            var tdResult = TaskDialog.Show(new TaskDialogOptions()
            {
                UserInputEnabled = true,
                Content = "Enter a name for the workspace to save:",
                Title = "Save workspace",
                CommonButtons = TaskDialogCommonButtons.OKCancel
            });

            if (tdResult.Result == TaskDialogSimpleResult.Ok)
            {
                string subLayoutName = tdResult.UserInput.Trim();
                if (subLayoutName != string.Empty)
                {
                    SaveViews(floatingWindow_.Windows, subLayoutName);
                    floatingWindow_.SubLayoutTitle = subLayoutName;

                    //CustomContentPane.SetActivePane(this, CustomContentPane.GetActivePane(this));
                    Workspace ws = this.Workspaces.Cast<Workspace>()
                                                .FirstOrDefault(workspace => workspace.Root == floatingWindow_);
                    if (ws == null)
                    {
                        ws = new Workspace(this, subLayoutName, floatingWindow_);
                       this.Workspaces.Add(ws); 
                    }
                    else
                    {
                        ws.Name = subLayoutName;
                    }

                    floatingWindow_.Title = subLayoutName;
                }
                else
                {
                    TaskDialog.ShowMessage("Please enter a name.");
                }
            }
        }
    }
}
