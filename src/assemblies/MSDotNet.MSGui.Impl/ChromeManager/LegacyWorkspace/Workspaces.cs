﻿using System; 
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;  

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Workspaces
{
    internal class Workspaces : ObservableCollection<IWorkspace>, IWorkspaces
    {
        private ILegacyWorkspacesManager m_chromeManager;
        public Workspaces(ILegacyWorkspacesManager chromeManager_)
        {
            m_chromeManager = chromeManager_;
        }
        public IWorkspace CreateWorkspace(IWindowViewContainer seedView_, string name_)
        {
            return m_chromeManager.CreateWorkspace(seedView_, name_);
        }
        public IEnumerable<IAvailableWorkspace> AvailableWorkspaces()
        {
            return from name in m_chromeManager.GetSubLayoutNames()
                   select new AvailableWorkspace(m_chromeManager, name);
        }

        public IEnumerable<string> GetViewFactroyIds(string workspaceName_)
        {
            return m_chromeManager.GetViewFactoryIds(workspaceName_); 
        }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e_)
        {
            base.OnCollectionChanged(e_);
            if (e_.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (var newItem in e_.NewItems)
                {
                    IWorkspace workspace = newItem as IWorkspace;
                    if (workspace != null)
                    {
                        workspace.Closed += new EventHandler(Workspace_Closed);
                    }
                }
            }
            else if (e_.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (var oldItem in e_.OldItems)
                {
                    IWorkspace workspace = oldItem as IWorkspace;
                    if (workspace != null)
                    {
                        workspace.Closed -= new EventHandler(Workspace_Closed);
                    }
                }
            } 
        }

        void Workspace_Closed(object sender, EventArgs e)
        {
            IWorkspace workspace = (IWorkspace)sender;
            workspace.Closed -= Workspace_Closed;
            this.Remove(workspace);
        }
        
    }

    internal class AvailableWorkspace  : IAvailableWorkspace
    {

        private readonly ILegacyWorkspacesManager chromeManager;
        internal AvailableWorkspace(ILegacyWorkspacesManager chromeManager_, string name_)
        {
            chromeManager = chromeManager_;
            Name = name_;
            
        }

        public string Name { get; private set; }

        public IEnumerable<string> ViewFactoryIds
        {
            get { return chromeManager.GetViewFactoryIds(Name); }
        }

        public void Load()
        {
            chromeManager.LoadViews(Name);
        }
    }

    internal class Workspace : IWorkspace
    {
        private readonly ILegacyWorkspacesManager chromeManager;
        private readonly IFloatingWindow window;
        internal Workspace(ILegacyWorkspacesManager chromeManager_, string name_, IFloatingWindow window_)
        {
            this.chromeManager = chromeManager_;
            Name = name_;
            this.window = window_;
            window.Closed += new EventHandler(window_Closed);
        }

        public event EventHandler Closed;

        void window_Closed(object sender_, EventArgs e_)
        {
            IFloatingWindow w = (IFloatingWindow)sender_;
            var copy = Closed;
            if (copy != null)
            {
                copy(this, e_);
            }
            w.Closed -= new EventHandler(window_Closed); 
        }

 
        public string Name { get; set; } 

        public ReadOnlyObservableCollection<IWindowViewContainer> Views
        {
            get
            { 
                return
                    new ReadOnlyObservableCollection<IWindowViewContainer>(
                        new ObservableCollection<IWindowViewContainer>(window.Windows)); 
            }
        }


        public IFloatingWindow Root
        {
            get { return window; }
        }

        public void Save()
        {
            chromeManager.SaveViews(Views, Name);
        } 
      
    }
}
