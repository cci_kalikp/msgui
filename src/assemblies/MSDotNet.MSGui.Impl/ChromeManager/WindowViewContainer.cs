﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.View;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
#pragma warning disable 612,618
    internal class WindowViewContainer : ViewContainerBase, IWindowViewContainerInternal, IViewContainer
#pragma warning restore 612,618
    {
        internal static bool DefaultIsOwnedWindowValue = true;

        private readonly IWidgetViewContainer _header;
        private readonly IContextMenuExtrasHolder _contextMenu;
        private readonly ObservableCollection<IWidgetViewContainer> _extraContextMenuItems;
        private double _width; 
        private System.Drawing.Icon _icon;
        private bool _isHidden;
        private Brush _background;
        private Color _accentColour;
        private Brush _tabColour;
        private Brush _selectedTabColour;
        private bool _transient;
        private bool _topmost;
        private bool _showInTaskbar;
        private string _id;
        private object _v2vHost;
        private bool _isInActiveWorkspace;
        private object _tooltip;
        private bool _visible = true;
        private double _height;
        private string _title;
        private bool _isActive; 
        private System.Windows.Size _defaultSize = System.Windows.Size.Empty;
        private object _content;
        private HeaderStructure _headerStructure = PaneHeaderControlBase.Structure;
        private LoadStage currentLoadStage = LoadStage.Default;
        private WeakReference _visual;

        public WindowViewContainer(string factoryId):this(new InitialWindowParameters(), factoryId)
        { 
        } 

        public WindowViewContainer(InitialWindowParameters parameters, string factoryID)
        {
            Parameters = parameters;
            FactoryID = factoryID;
            this.Title = parameters.Title;
            _header = new HeaderItemsHolderWidgetContainer("", new InitialWidgetParameters(), this.m_headerItems);
            _extraContextMenuItems = new ObservableCollection<IWidgetViewContainer>();
            _contextMenu = new ExtraContextMenuWidgetContainer("", new InitialWidgetParameters(), _extraContextMenuItems);
            
            // this is needed for backwards compatibility
            Closed += (sender, e) =>
                {
                    if (Closed_CompatibilityMode != null)
                        Closed_CompatibilityMode(sender, new ViewEventArgs(e.ViewContainer as IViewContainer));
                };

            //this.FloatingWindowMinimumHeight = 100 - VerticalContentDiff;
            //this.FloatingWindowMinimumWidth = 100 - HorizontalContentDiff;
             
        }

        public virtual WindowViewModel GetViewModel(IHidingLockingManager hidingLockingManager_)
        {
            return new WindowViewModel(this, hidingLockingManager_);
        }

        public double HorizontalContentDiff
        {
            get
            {
                var defaultDiff = DockViewModel.BaseDefaultFloatingWindowContentHorizontalMargin;
                if (this.Parameters.ShowFlashBorder)
                {
                    if (DockViewModel.FlatModeEnabledProperty)
                    {
                        defaultDiff += ThemeExtensions.ContentPaneFlatModeFlashBorderThickness.Left +
                        ThemeExtensions.ContentPaneFlatModeFlashBorderThickness.Right;
                    }
                    else
                    {
                        defaultDiff += ThemeExtensions.ContentPaneFlashBorderThickness.Left +
                        ThemeExtensions.ContentPaneFlashBorderThickness.Right;
                    }
                }

                return defaultDiff;

            }
        }
         
        public double VerticalContentDiff
        {
            get
            {
                 
                var defaultDiff = DockViewModel.BaseDefaultFloatingWindowContentVerticalMargin;
                if (this.Parameters.ShowFlashBorder)
                {
                    if (DockViewModel.FlatModeEnabledProperty)
                    {
                        defaultDiff += ThemeExtensions.ContentPaneFlatModeFlashBorderThickness.Top +
               ThemeExtensions.ContentPaneFlatModeFlashBorderThickness.Bottom;
                    }
                    else
                    {
                        defaultDiff += ThemeExtensions.ContentPaneFlashBorderThickness.Top +
ThemeExtensions.ContentPaneFlashBorderThickness.Bottom;
                    }
                }

                return defaultDiff;
            }
        }
        #region IWindowViewContainer Members

        public new InitialWindowParameters Parameters
        {
            get { return (InitialWindowParameters) base.Parameters; }
            private set { base.Parameters = value; }
        }
         

        public virtual string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged("Title");
            }
        }


        public virtual double Width
        {
            get { return _width; }
            set
            {
                _width = value;
                OnPropertyChanged("Width");
            }
        }

        public virtual double Top
        {
            get { return GetContentAbsolutePosition().Y; }

        }

        public virtual double Left
        {
            get { return GetContentAbsolutePosition().X; }
        }
        private Point GetContentAbsolutePosition()
        {
            var contentAsFwElement = Content as FrameworkElement;
            if (contentAsFwElement == null)
            {
                return new Point();
            }
            var presSource = PresentationSource.FromVisual(contentAsFwElement);
            return presSource == null ? new Point() : contentAsFwElement.PointToScreen(new Point());
        }
        public virtual double Height
        {
            get { return _height; }
            set
            {
                _height = value;
                OnPropertyChanged("Height");
            }
        }

        public double? FloatingWindowMaximumWidth
        {
            get
            {
                return floatingWindowMaximumWidth;
            }
            set
            {
                floatingWindowMaximumWidth = value;
                OnPropertyChanged("FloatingWindowMaximumWidth");
            }
        }

        public double? FloatingWindowMaximumHeight
        {
            get
            {
                return floatingWindowMaximumHeight;
            }
            set
            {
                floatingWindowMaximumHeight = value;
                OnPropertyChanged("FloatingWindowMaximumHeight");
            }
        }

        public double FloatingWindowMinimumWidth
        {
            get
            {
                return floatingWindowMinimumWidth;
            }
            set
            {
                floatingWindowMinimumWidth = value;
                OnPropertyChanged("FloatingWindowMinimumWidth");
            }
        }

        public double FloatingWindowMinimumHeight
        {
            get
            {
                return floatingWindowMinimumHeight;
            }
            set
            {
                floatingWindowMinimumHeight = value;
                OnPropertyChanged("FloatingWindowMinimumHeight");
            }
        }


        public object Content
        {
            get { return _content; }
            set
            {
                var previousValue = _content;
                _content = value;
                CurrentStage = LoadStage.AfterInitialized;
                FrameworkElement element = _content as FrameworkElement;
                if (element != null)
                {
                    element.Loaded += new RoutedEventHandler(element_Loaded);
                }

                OnPropertyChanged("Content");
                FireContentChanged(previousValue, _content);
            }
        }

        private void FireContentChanged(object oldCOntent, object newContent)
        {
            var handler = ContentChanged;
            if (handler != null)
            {
                var eventArgs = new ContentChangedEventArgs(oldCOntent, newContent);
                handler(this, eventArgs);
            }
        }

        void element_Loaded(object sender, RoutedEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            CurrentStage = LoadStage.AfterLoaded;
            element.Loaded -= element_Loaded;
            element.IsVisibleChanged += element_IsVisibleChanged;
        }

        void element_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            CurrentStage = LoadStage.AfterShown; 
            element.IsVisibleChanged -= element_IsVisibleChanged;
        }

        public LoadStage CurrentStage
        {
            get { return currentLoadStage; }
            set
            {
                currentLoadStage = value;
                OnPropertyChanged("CurrentStage");
            }
        }
        private readonly HeaderItemsCollection m_headerItems = new HeaderItemsCollection();
        private bool _ownedByContainer = DefaultIsOwnedWindowValue;
        private double? floatingWindowMaximumWidth;
        private double? floatingWindowMaximumHeight; 
        private double floatingWindowMinimumWidth;
        private double floatingWindowMinimumHeight;
        private bool canMaximize = true;

        public IList<object> HeaderItems
        {
            get { return m_headerItems; }
        }

        public HeaderStructure HeaderStructure
        {
            get { return _headerStructure; }
            set
            {
                _headerStructure = value;
                OnPropertyChanged("HeaderStructure");
            }
        }

        public IContextMenuExtrasHolder ContextMenuExtras
        {
            get
            {
                return _contextMenu;
            }
        }

        public event EventHandler<VisibilityEventArgs> IsVisibleChanged;
        public event EventHandler<ContentChangedEventArgs> ContentChanged;

        public bool IsVisible
        {
            get { return _visible; }
            internal set
            {
                if (_visible != value)
                {
                    var copy = IsVisibleChanged;
                    if (copy != null)
                    {
                        copy(this, new VisibilityEventArgs(value));
                    } 
                }
                _visible = value;
                OnPropertyChanged("IsVisible");
            }
        }

        public bool IsActive
        {
            get { return _isActive; }
            internal set
            {
                _isActive = value;
                OnPropertyChanged("IsActive");
            }
        }

        public bool IsHidden
        {
            get { return _isHidden; }
            set
            {
                _isHidden = value;
                OnPropertyChanged("IsHidden");
            }
        }

 
        public System.Drawing.Icon Icon
        {
            get { return _icon; }
            set
            {
                _icon = value;
                OnPropertyChanged("Icon");
            }
        }

        public Brush Background
        {
            get { return _background; }
            set
            {
                _background = value;
                OnPropertyChanged("Background");
            }
        }

        public Color AccentColour
        {
            get { return _accentColour; }
            set
            {
                _accentColour = value;
                OnPropertyChanged("AccentColour");
            }
        }

        public Brush TabColour
        {
            get { return _tabColour; }
            set
            {
                _tabColour = value;
                OnPropertyChanged("TabColour");
            }
        }

        public Brush SelectedTabColour
        {
            get { return _selectedTabColour; }
            set
            {
                _selectedTabColour = value;
                OnPropertyChanged("SelectedTabColour");
            }
        }
         
        public bool Transient 
        {
            get { return _transient; }
            set
            {
                _transient = value;
                OnPropertyChanged("Transient");
            }
        }

        public bool Topmost
        {
            get { return _topmost; }
            set
            {
                _topmost = value;
                OnPropertyChanged("Topmost");
                OnTopmostChanged();
            }
        }

        public bool ShowInTaskbar
        {
            get { return _showInTaskbar; }
            set
            {
                _showInTaskbar = value;
                OnPropertyChanged("ShowInTaskbar");
            }
        }

        public bool OwnedByContainer
        {
            get
            {
                return _ownedByContainer;
            }
            set
            {
                _ownedByContainer = value;
                OnPropertyChanged("OwnedByContainer");
            }
        }

        public bool CanMaximize
        {
            get
            {
                return canMaximize;
            }
            set
            {
                canMaximize = value;
                OnPropertyChanged("CanMaximize");
            }
        }

        public bool UnpinOnHeaderDoubleClick { get; set; }
        public bool StayPinnedWhenRevealed { get; set; }

        public string ID
        {
            get { return _id ?? (_id = GenerateNewID()); }
            set
            {
                if (value == null)
                {
                    throw new InvalidOperationException("View ID can't be set to null");
                }
                _id = value;
                OnPropertyChanged("ID");
            }
        }

        public static string GenerateNewID()
        {
            return "view" + Guid.NewGuid().ToString("N");
        }
 
         
        public void Close()
        {
            var closeRequestedCopy = CloseRequested;
            if (closeRequestedCopy != null)
            {
                closeRequestedCopy(this, EventArgs.Empty);
            }
            else if (!InvokeClosing().Cancel)
            {
                InvokeClosed();
            }
        }


        public void Activate()
        {
            if (!ChromeManagerBase.StartupCompleted)
            {
                return;
            }

            EventHandler activateRequestedCopy = ActivationRequested;
            if (activateRequestedCopy != null)
            {
                activateRequestedCopy(this, EventArgs.Empty);
            }
        }

        public void Flash()
        {
            Flash(Color.FromRgb(255, 0, 0));
        }

        public void Flash(Color color)
        {
            EventHandler<EventArgs<Color>> flashRequestedCopy = FlashRequested;
            if (flashRequestedCopy != null)
            {
                flashRequestedCopy(this, new EventArgs<Color>(color));
            }
        }

        public event EventHandler<CancelEventArgs> Closing;
        public event EventHandler<WindowEventArgs> Closed;
        public event EventHandler<ViewEventArgs> Closed_CompatibilityMode;

        //invoked when we remove the pane during layout switch 
        public event EventHandler<WindowEventArgs> ClosedQuietly;
        public event EventHandler<WindowEventArgs> Created;
        public event EventHandler TopmostChanged;

        public IWidgetViewContainer Header
        {
            get { return this._header; }
        }

        #endregion

        #region IViewContainer Members

        string IViewContainer.Title
        {
            get { return (this as IWindowViewContainer).Title; }
            set { (this as IWindowViewContainer).Title = value; }
        }

        System.Windows.Size IViewContainer.DefaultSize
        {
            get { return _defaultSize; }
            set
            {
                _defaultSize = value;
                (this as IWindowViewContainer).Parameters.Width = value.Width;
                (this as IWindowViewContainer).Parameters.Height = value.Height;
                OnPropertyChanged("DefaultSize");
            }
        }

        double IViewContainer.Width
        {
            get { return (this as IWindowViewContainer).Width; }
            set { (this as IWindowViewContainer).Width = value; }
        }

        double IViewContainer.Height
        {
            get { return (this as IWindowViewContainer).Height; }
            set { (this as IWindowViewContainer).Height = value; }
        }

        double IViewContainer.Top
        {
            get { return (this as IWindowViewContainer).Top; }
            //set { (this as IWindowViewContainer).Top = value; }
        }

        double IViewContainer.Left
        {
            get { return (this as IWindowViewContainer).Left; }
            //set { (this as IWindowViewContainer).Left = value; }
        }

        object IViewContainer.Content
        {
            get { return (this as IWindowViewContainer).Content; }
            set { (this as IWindowViewContainer).Content = value; }
        }

        IList<object> IViewContainer.HeaderItems
        {
            get { return (this as IWindowViewContainer).HeaderItems; }
        }

        bool IViewContainer.IsVisible
        {
            get { return (this as IWindowViewContainer).IsVisible; }
        }

        bool IViewContainer.IsActive
        {
            get { return (this as IWindowViewContainer).IsActive; }
        }

        System.Drawing.Icon IViewContainer.Icon
        {
            get { return (this as IWindowViewContainer).Icon; }
            set { (this as IWindowViewContainer).Icon = value; }
        }

        System.Windows.Media.Brush IViewContainer.Background
        {
            get { return (this as IWindowViewContainer).Background; }
            set { (this as IWindowViewContainer).Background = value; }
        }

        string IViewContainer.ID
        {
            get { return (this as IWindowViewContainer).ID; }
        }

        InitialViewSettings IViewContainer.InitialViewSettings
        {
            get { return new InitialViewSettings((this as IWindowViewContainer).Parameters); }
        }

        void IViewContainer.Close()
        {
            (this as IWindowViewContainer).Close();
        }

        void IViewContainer.Activate()
        {
            (this as IWindowViewContainer).Activate();
        }

        void IViewContainer.StartFlash()
        {
        }

        void IViewContainer.StopFlash()
        {
        }

        void IViewContainer.Flash()
        {
            (this as IWindowViewContainer).Flash();
        }

        void IViewContainer.Flash(System.Windows.Media.Color color)
        {
            (this as IWindowViewContainer).Flash(color);
        }

        event EventHandler<System.ComponentModel.CancelEventArgs> IViewContainer.Closing
        {
            add { (this as IWindowViewContainer).Closing += value; }
            remove { (this as IWindowViewContainer).Closing -= value; }
        }

        event EventHandler<ViewEventArgs> IViewContainer.Closed
        {
            add { this.Closed_CompatibilityMode += value; }
            remove { this.Closed_CompatibilityMode -= value; }
        }

        #endregion


        internal CancelEventArgs InvokeClosing()
        {
            EventHandler<CancelEventArgs> copy = Closing;
            CancelEventArgs cea = new CancelEventArgs();
            if (copy != null)
            {
                copy(this, cea);
            }
            return cea;
        }

        internal void InvokeClosed()
        {
            InvokeReleaseRsource();
            EventHandler<WindowEventArgs> copy2 = Closed;
            if (copy2 != null)
            {
                copy2(this, new WindowEventArgs(this));
            }
            _v2vHost = null;
        }

        internal void InvokeClosedQuietly()
        {
            InvokeReleaseRsource();
            EventHandler<WindowEventArgs> copy2 = ClosedQuietly;
            if (copy2 != null)
            {
                copy2(this, new WindowEventArgs(this));
            }
            _v2vHost = null;
        }

        internal void InvokeCreated()
        {
            var copy = this.Created;
            if (copy != null)
            {
                copy(this, new WindowEventArgs(this));
            }
        }

        internal event EventHandler CloseRequested;
        internal event EventHandler ActivationRequested;
        internal event EventHandler<EventArgs<System.Windows.Media.Color>> FlashRequested;


        public ViewContainerType ViewContainerType { get; set; }

        #region Auto-hook tabchange

        internal event EventHandler<HostChangedEventArgs> HostChanged;

        internal class HostChangedEventArgs : EventArgs
        {
            public WindowViewContainer Sender { get; set; }
            public object OldHost { get; set; }
            public object NewHost { get; set; }
        }

        internal void InvokeHostChanged(object oldHost_, object newHost_)
        {
            var copy = HostChanged;
            if (copy != null)
            {
                copy(this, new HostChangedEventArgs()
                    {
                        Sender = this,
                        OldHost = oldHost_,
                        NewHost = newHost_
                    });
            }
        }

        #endregion

        #region Print

        public PrintWindowHandler PrintHandler { get; set; }
        public PreviewPrintWindowHandler PreviewPrintHandler { get; set; }
        public PrintWindowParameters PrintWindowParameters
        {
            set { throw new NotImplementedException(); }
            get { throw new NotImplementedException(); }
        }

        #endregion Print

        internal event EventHandler<HostChangedEventArgs> V2vHostChanged;

        private void InvokeV2vHostChanged(object oldV2vHost, object newV2vHost)
        {
            var copy = V2vHostChanged;
            if (copy != null)
            {
                copy(this, new HostChangedEventArgs()
                    {
                        Sender = this,
                        OldHost = oldV2vHost,
                        NewHost = newV2vHost
                    });
            }
        }

        internal virtual object V2vHost
        {
            get { return _v2vHost; }
            set
            {
                var old = _v2vHost;
                _v2vHost = value;
                InvokeV2vHostChanged(old, value);
            }
        }

        private DockState? dockState;
        private IWindowViewContainerHostRoot root;
        internal IWindowViewContainerHostRoot Root
        {
            get { return root; }
            set
            {
                if (root != value)
                {
                    if (value != null && root != null)
                    {
                        int windowCount = value.Windows.Count();
                        if (windowCount > 0)
                        {
                            if (root.Windows.Count() == 1)
                            {
                                var firstWindow = (WindowViewContainer) root.Windows.First();
                                if (firstWindow.dockState != DockState.Undocked)
                                {
                                    firstWindow.InvokeDockStateChanged(root, value, DockState.Undocked);
                                } 
                            }

                            //undock a view/a group view in tab/floating window to another floating window
                            if (windowCount == 1)
                            {
                                InvokeDockStateChanged(root, value, DockState.Undocked);

                            }

                            //dock a floating window(single/multiple) to another tab/floating window
                            if (windowCount > 1)
                            {
                                foreach (WindowViewContainer windowViewContainer in value.Windows)
                                {
                                    if (windowViewContainer != this && windowViewContainer.dockState != DockState.Docked)
                                    {
                                        windowViewContainer.InvokeDockStateChanged(root, value, DockState.Docked);
                                    }
                                }
                                InvokeDockStateChanged(root, value, DockState.Docked); 
                            } 
                        }
                       
                    }
                    
                    
                    root = value;
                }
            }
        }
        public event EventHandler<WindowDockStateChangedEventArgs> DockStateChanged;


        private void InvokeDockStateChanged(IWindowViewContainerHostRoot oldRoot_, IWindowViewContainerHostRoot newRoot_, DockState state_)
        {
            dockState = state_; 
            var copy = DockStateChanged;
            if (copy != null)
            {
                copy(this, new WindowDockStateChangedEventArgs(oldRoot_, newRoot_, state_));
            }
        }

         

        internal bool IsInActiveWorkspace
        {
            get { return _isInActiveWorkspace; }
            set
            {
                _isInActiveWorkspace = value;
                OnPropertyChanged("IsInActiveWorkspace");
            }
        }
         
        public object Tooltip
        {
            get { return _tooltip; }
            set
            {
                _tooltip = value;
                OnPropertyChanged("Tooltip");
            }
        }

        public XDocument ViewState { get; set; }

        public bool? DialogResult
        {
            get { throw new NotImplementedException(); }
        }

        public object Visual
        {
            get
            {
                return _visual.IsAlive ? _visual.Target : null;
            }
            internal set
            {
                _visual = new WeakReference(value);
            }
        }

        public ObservableCollection<IWidgetViewContainer> ExtraContextMenuItems
        {
            get
            {
                return _extraContextMenuItems;
            }
        }
        
        private void OnTopmostChanged()
        {
            var handler = TopmostChanged;
            if (handler != null) handler(this, EventArgs.Empty);
        }
    }
}
