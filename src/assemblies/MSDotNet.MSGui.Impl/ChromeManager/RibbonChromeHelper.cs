﻿using Infragistics.Windows.Ribbon;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Impl.View
{
	public class RibbonChromeHelper : IChromeHelper
	{
		internal ChromeManager ChromeManager;

		internal RibbonChromeHelper(ChromeManager chromeManager)
		{
			this.ChromeManager = chromeManager;
		}

		#region IChromeHelper Members

		public InitializeWidgetHandler ButtonFactory { get { return ButtonFactoryMethod; } }
		public InitializeWidgetHandler ShowWindowButtonFactory { get { return ShowWindowButtonFactoryMethod; } }
		public InitializeWidgetHandler DropdownButtonFactory { get { return DropdownButtonFactoryMethod; } }
		public InitializeWidgetHandler ComboBoxFactory { get { return ComboBoxFactoryMethod; } }
		public InitializeWidgetHandler PopupFactory { get { return PopupFactoryMethod; } }

		#endregion

		private bool ButtonFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
		{
			InitialButtonParameters parameters = emptyWidget.InitialParameters as InitialButtonParameters;
			if (parameters != null)
			{
				var button = new ButtonTool() 
				{
					Id = emptyWidget.FactoryID,
					Content = parameters.Text,
					ToolTip = parameters.ToolTip,
					LargeImage = parameters.Image,
					SmallImage = parameters.Image,
					IsEnabled = parameters.Enabled,
				};
				button.Click += (a, b) => { parameters.Click(a, b); };
				emptyWidget.Content = button;
				return true;
			}
			else
			{
				return false;
			}
		}

		private bool ShowWindowButtonFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
		{
			InitialShowWindowButtonParameters parameters = emptyWidget.InitialParameters as InitialShowWindowButtonParameters;
			if (parameters != null)
			{
				var button = new ButtonTool()
				{
					Id = emptyWidget.FactoryID,
					Content = parameters.Text,
					ToolTip = parameters.ToolTip,
					LargeImage = parameters.Image,
					SmallImage = parameters.Image,
					IsEnabled = parameters.Enabled,
				};
				button.Click += (a, b) => { ChromeManager.CreateWindow(parameters.WindowFactoryID, parameters.InitialParameters); };
				emptyWidget.Content = button;
				return true;
			}
			else
			{
				return false;
			}
		}

		private bool DropdownButtonFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
		{
			InitialDropdownButtonParameters parameters = emptyWidget.InitialParameters as InitialDropdownButtonParameters;
			if (parameters != null)
			{
				var dropdown = new Infragistics.Windows.Ribbon.MenuTool()
				{
					Id = emptyWidget.FactoryID,
					Caption = parameters.Text,
					ToolTip = parameters.ToolTip,
					LargeImage = parameters.Image,
					SmallImage = parameters.Image,
					IsEnabled = parameters.Enabled,
				};
				dropdown.Items.Add(new ButtonTool() { Content = "dummy" });
				emptyWidget.Content = dropdown;
				return true;
			}
			else
			{
				return false;
			}
		}

		private bool ComboBoxFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
		{
			InitialComboBoxParameters parameters = emptyWidget.InitialParameters as InitialComboBoxParameters;
			if (parameters != null)
			{
				var combo = new Infragistics.Windows.Ribbon.ComboEditorTool()
				{
					Id = emptyWidget.FactoryID,
					Caption = parameters.Text,
					ToolTip = parameters.ToolTip,
					LargeImage = parameters.Image,
					SmallImage = parameters.Image,
					IsEnabled = parameters.Enabled,
					ItemsSource = parameters.ItemsSource,
				};

				if (parameters.DropDownOpened != null)
					combo.DropDownOpened += parameters.DropDownOpened;

				emptyWidget.Content = combo;
				return true;
			}
			else
			{
				return false;
			}
		}

		private bool PopupFactoryMethod(IWidgetViewContainer emptyWidget, XDocument state)
		{
			InitialPopupParameters parameters = emptyWidget.InitialParameters as InitialPopupParameters;
			if (parameters != null)
			{
				var name_ = XamRibbonHelper.SanitizeRibbonItemName(parameters.Text);
				MenuTool menuTool = new MenuTool
				{
					Id = emptyWidget.FactoryID,
					Caption = parameters.Text,
					ButtonType = MenuToolButtonType.DropDown,
					IsEnabled = parameters.Enabled,
					LargeImage = parameters.Image,
					SmallImage = parameters.Image,
					ItemsSource = parameters.ItemsSource
				};

				menuTool.SetValue(RibbonGroup.MaximumSizeProperty, RibbonToolSizingMode.ImageAndTextNormal);
				
				emptyWidget.Content = menuTool;
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
