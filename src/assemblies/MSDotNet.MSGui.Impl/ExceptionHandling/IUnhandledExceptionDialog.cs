﻿using System;
namespace MorganStanley.MSDotNet.MSGui.Impl.ExceptionHandling
{
  public interface IUnhandledExceptionDialog
  {
    string UserInfoText { get; set; }
    System.Net.Mail.Attachment[] EmailAttachments { get; set; }
    string EmailFromAddress { get; set; }
    string EmailSubject { get; set; }
    string[] EmailToAddresses { get; set; }
    bool IsContinueEnabled { get; set; }
    bool IsCopyEnabled { get; set; }
    bool IsSendEmailEnabled { get; set; }
    bool IsTerminateEnabled { get; set; }
    bool MailSent { get; }
    bool? ShowDialog();
    string HeaderText { get; set; }
    Exception UnhandledException { get; set; }
    System.Windows.Window Owner { get; set; }
  }
}
