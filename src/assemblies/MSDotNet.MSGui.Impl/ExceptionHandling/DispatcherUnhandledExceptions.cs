﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ExceptionHandling/DispatcherUnhandledExceptions.cs#8 $
// $Change: 869990 $
// $DateTime: 2014/03/06 20:42:53 $
// $Author: smulovic $

using System;
using System.Linq;
using System.Windows.Threading;
using System.Net.Mail;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.ExceptionHandling
{
  /// <summary>
  /// Delegate type for callbacks that are called before the exception window is popped up.
  /// </summary>
  /// <param name="exception_">The exception.</param>
  /// <param name="unhandledExceptionWindow_">The constructed exception window.</param>
  /// <remarks>
  /// <para>
  /// The exception window is created but not shown. In this callback it is possible to
  /// apply a custom template to it, or change its properties, style, etc...
  /// </para>
  /// <para>
  /// Typically in this callback the application wants to log the exception, or send the exception
  /// details via e-mail.
  /// </para>
  /// </remarks>
  public delegate void UnhandledExceptionCallback(Exception exception_, IUnhandledExceptionDialog unhandledExceptionWindow_);

  /// <summary>
  /// Static class that helps with dealing with unhandled exceptions.
  /// </summary>
  public static class DispatcherUnhandledExceptions
  {
      private static readonly IMSLogger _logger = MSLoggerFactory.CreateLogger("ExceptionLogging");
    /// <summary>
    /// Adds the default unhandled-exception handler to the current <see cref="Dispatcher"/>.
    /// </summary>
    public static void AddDefaultHandler()
    {
      AddDefaultHandler(Dispatcher.CurrentDispatcher, null, null, null, new string[0]);
    }

    /// <summary>
    /// Adds the default unhandled-exception handler to the current <see cref="Dispatcher"/>.
    /// </summary>
    /// <param name="action_">The action to execute before the unhandled-exception window pops up.</param>
    public static void AddDefaultHandler(UnhandledExceptionCallback action_)
    {
      AddDefaultHandler(Dispatcher.CurrentDispatcher, action_, null, null, null, new string[0]);
    }

    /// <summary>
    /// Adds the default unhandled-exception handler to the current <see cref="Dispatcher"/>.
    /// </summary>
    /// <param name="fromAddress_">The from address for email-sending.</param>
    /// <param name="subject_">The subject for email-sending.</param>
    /// <param name="attachments_">The attachments to be attached to the e-mail.</param>
    /// <param name="toAddresses_">The to addresses for email-sending.</param>
    public static void AddDefaultHandler(string fromAddress_, string subject_, Attachment[] attachments_, params string[] toAddresses_)
    {
      AddDefaultHandler(Dispatcher.CurrentDispatcher, null, fromAddress_, subject_, attachments_, toAddresses_);
    }

    /// <summary>
    /// Adds the default unhandled-exception handler to the specified <see cref="Dispatcher"/>.
    /// </summary>
    /// <param name="dispatcher_">The <see cref="Dispatcher"/> object.</param>
    /// <param name="fromAddress_">The from address for email-sending.</param>
    /// <param name="subject_">The subject for email-sending.</param>
    /// <param name="attachments_">The attachments to be attached to the e-mail.</param>
    /// <param name="toAddresses_">The to addresses for email-sending.</param>
    public static void AddDefaultHandler(Dispatcher dispatcher_, string fromAddress_, string subject_, Attachment[] attachments_, params string[] toAddresses_)
    {
      AddDefaultHandler(dispatcher_, null, fromAddress_, subject_, attachments_, toAddresses_);
    }

    /// <summary>
    /// Adds the default unhandled-exception handler to the current <see cref="Dispatcher"/>.
    /// </summary>
    /// <param name="action_">The action to execute before the unhandled-exception window pops up.</param>
    /// <param name="fromAddress_">The from address for email-sending.</param>
    /// <param name="subject_">The subject for email-sending.</param>
    /// <param name="attachments_">The attachments to be attached to the e-mail.</param>
    /// <param name="toAddresses_">The to addresses for email-sending.</param>
    public static void AddDefaultHandler(UnhandledExceptionCallback action_, string fromAddress_, string subject_, Attachment[] attachments_, params string[] toAddresses_)
    {
      AddDefaultHandler(Dispatcher.CurrentDispatcher, action_, fromAddress_, subject_, attachments_, toAddresses_);
    }

    /// <summary>
    /// Adds the default unhandled-exception handler to the specified <see cref="Dispatcher"/>.
    /// </summary>
    /// <param name="dispatcher_">The <see cref="Dispatcher"/> object.</param>
    /// <param name="action_">The action to execute before the unhandled-exception window pops up.</param>
    /// <param name="fromAddress_">The from address for email-sending.</param>
    /// <param name="subject_">The subject for email-sending.</param>
    /// <param name="attachments_">The attachments to be attached to the e-mail.</param>
    /// <param name="toAddresses_">The to addresses for email-sending.</param>
    public static void AddDefaultHandler(Dispatcher dispatcher_, UnhandledExceptionCallback action_, string fromAddress_, string subject_, Attachment[] attachments_, params string[] toAddresses_)
    {
      string[] toAddresses = toAddresses_.Where(addr => !string.IsNullOrEmpty(addr)).ToArray();
      dispatcher_.UnhandledException += (sender_, e_) =>
      {
          _logger.Critical("Unhandled exception", e_.Exception);
        UnhandledExceptionWindow window = new UnhandledExceptionWindow
        {
          UnhandledException = e_.Exception,
        };
        if (!String.IsNullOrEmpty(fromAddress_))
        {
          window.EmailFromAddress = fromAddress_;
        }
        if (!String.IsNullOrEmpty(subject_))
        {
          window.EmailSubject = subject_;
        }
        if (toAddresses != null && toAddresses.Length > 0)
        {
          window.EmailToAddresses = toAddresses_;
        }

        if (action_ != null)
        {
          action_(e_.Exception, window);
        }

        // The condition is needed to ensure that the dialog is shown even if the main window is not shown yet
        // (eg. the exception is raised in the constructor)
        if (System.Windows.Application.Current.MainWindow != null && System.Windows.Application.Current.MainWindow.IsVisible)
        {
          window.Owner = System.Windows.Application.Current.MainWindow;
        }
        window.ShowDialog();

        // disables the default WPF error dialog by marking the exception as handled
        e_.Handled = true;
      };
    }
  }
}
