﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ExceptionHandling/UnhandledExceptionWindow.cs#20 $
// $Change: 886728 $
// $DateTime: 2014/07/01 11:32:42 $
// $Author: caijin $

using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows.Input;
using System;
using System.Windows;
using System.Windows.Markup;
using System.Net.Mail;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.ExceptionHandling
{
	/// <summary>
	/// A dialog window that pops up when an unhandled exception happens
	/// </summary>
	public class UnhandledExceptionWindow : Window, IUnhandledExceptionDialog
	{
        private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger<UnhandledExceptionWindow>();
        private const string DefaultHeaderText = @"An error occurred while running the application.";

        private const string DefaultUserInfoText = "We are sorry but the application encountered an unexpected error. Your unsaved data may have been damaged or lost. ";
        private const string DefaultUserInfoTextForSendReport = "Please inform us about this event by clicking the Send email button. ";
		
        private const string DefaultUserInfoTextFoContinue = "You may click Continue button to continue running the application " +
             "but it may result in unexpected behaviour thus it is highly recommended to terminate the application.";


	    private readonly bool backgroundThreadException;

		#region Static constructor
		private static readonly ResourceDictionary s_stylesResourceDictionary;
		private static readonly ResourceDictionary s_templateResourceDictionary;

		/// <summary>
		/// Initializes static members of the <see cref="UnhandledExceptionWindow"/> class.
		/// </summary>
		static UnhandledExceptionWindow()
		{
			// overriding defaults
			TitleProperty.OverrideMetadata(
				typeof(UnhandledExceptionWindow),
				new FrameworkPropertyMetadata("Unhandled exception occurred"));
			WidthProperty.OverrideMetadata(
				typeof(UnhandledExceptionWindow),
				new FrameworkPropertyMetadata(650.0));
			SizeToContentProperty.OverrideMetadata(
				typeof(UnhandledExceptionWindow),
				new FrameworkPropertyMetadata(SizeToContent.Height));
			ShowInTaskbarProperty.OverrideMetadata(
				typeof(UnhandledExceptionWindow),
				new FrameworkPropertyMetadata(false));
			ResizeModeProperty.OverrideMetadata(
				typeof(UnhandledExceptionWindow),
				new FrameworkPropertyMetadata(ResizeMode.NoResize));
			//BackgroundProperty.OverrideMetadata(
			//  typeof(UnhandledExceptionWindow),
			//  new FrameworkPropertyMetadata(SystemColors.WindowBrush));
			// TODO bendel: if the theme changes the background of the exception window does not change.
			DefaultStyleKeyProperty.OverrideMetadata(
				typeof(UnhandledExceptionWindow),
				new FrameworkPropertyMetadata(typeof(UnhandledExceptionWindow)));

			// reading style resources
			s_stylesResourceDictionary = (ResourceDictionary)XamlReader.Load(Assembly.GetExecutingAssembly().GetManifestResourceStream(
				"MorganStanley.MSDotNet.MSGui.Impl.ExceptionHandling.UnhandledExceptionWindowStyles.xaml"));

			// registering other dependency properties that need data from the resources
			DefaultButtonStylePropertyKey = DependencyProperty.RegisterReadOnly(
				"DefaultButtonStyle",
				typeof(Style),
				typeof(UnhandledExceptionWindow),
				new FrameworkPropertyMetadata(s_stylesResourceDictionary["DefaultButtonStyle"]));
			DefaultButtonStyleProperty = DefaultButtonStylePropertyKey.DependencyProperty;

			DefaultDetailsToggleButtonStylePropertyKey = DependencyProperty.RegisterReadOnly(
				"DefaultDetailsToggleButtonStyle",
				typeof(Style),
				typeof(UnhandledExceptionWindow),
				new FrameworkPropertyMetadata(s_stylesResourceDictionary["DefaultDetailsToggleButtonStyle"]));
			DefaultDetailsToggleButtonStyleProperty = DefaultDetailsToggleButtonStylePropertyKey.DependencyProperty;

			DefaultDetailsTextBoxStylePropertyKey = DependencyProperty.RegisterReadOnly(
				"DefaultDetailsTextBoxStyle",
				typeof(Style),
				typeof(UnhandledExceptionWindow),
				new FrameworkPropertyMetadata(s_stylesResourceDictionary["DefaultDetailsTextBoxStyle"]));
			DefaultDetailsTextBoxStyleProperty = DefaultDetailsTextBoxStylePropertyKey.DependencyProperty;

			// reading template resources
			s_templateResourceDictionary = (ResourceDictionary)XamlReader.Load(Assembly.GetExecutingAssembly().GetManifestResourceStream(
				"MorganStanley.MSDotNet.MSGui.Impl.ExceptionHandling.UnhandledExceptionWindowTemplate.xaml"));

			// overriding default template
			TemplateProperty.OverrideMetadata(
				typeof(UnhandledExceptionWindow),
				new FrameworkPropertyMetadata(s_templateResourceDictionary["DefaultUnhandledExceptionWindowTemplate"]));
		}
		#endregion

		#region ContinueProperty
		private static readonly DependencyPropertyKey ContinuePropertyKey = DependencyProperty.RegisterReadOnly(
			"Continue",
			typeof(ICommand),
			typeof(UnhandledExceptionWindow),
			new FrameworkPropertyMetadata(new RoutedUICommand("Continue", "continue", typeof(UnhandledExceptionWindow))));

		/// <summary>
		/// Dependency property for the continue command.
		/// </summary>
		public static readonly DependencyProperty ContinueProperty = ContinuePropertyKey.DependencyProperty;

		/// <summary>
		/// Gets the command that continues execution of the application.
		/// </summary>
		public ICommand Continue
		{
			get
			{
				return (ICommand)GetValue(ContinueProperty);
			}
		}
		#endregion

		#region TerminateProperty
		private static readonly DependencyPropertyKey TerminatePropertyKey = DependencyProperty.RegisterReadOnly(
			"Terminate",
			typeof(ICommand),
			typeof(UnhandledExceptionWindow),
			new FrameworkPropertyMetadata(new RoutedUICommand("Terminate", "terminate", typeof(UnhandledExceptionWindow))));

		/// <summary>
		/// Dependency property for the terminate command.
		/// </summary>
		public static readonly DependencyProperty TerminateProperty = TerminatePropertyKey.DependencyProperty;

		/// <summary>
		/// Gets the command that terminates execution of the application.
		/// </summary>
		public ICommand Terminate
		{
			get
			{
				return (ICommand)GetValue(TerminateProperty);
			}
		}
		#endregion

		#region MailSentProperty
		/// <summary>
		/// Dependency property for mail sent.
		/// </summary>
		public static readonly DependencyProperty MailSentProperty = DependencyProperty.Register(
			"MailSent",
			typeof(bool),
			typeof(UnhandledExceptionWindow),
			new UIPropertyMetadata(false));

		/// <summary>
		/// Gets or sets a value indicating whether a mail has been sent.
		/// </summary>
		public bool MailSent
		{
			get
			{
				return (bool)GetValue(MailSentProperty);
			}
			set
			{
				SetValue(MailSentProperty, value);
			}
		}
		#endregion

		#region SendMailProperty
		private static readonly DependencyPropertyKey SendEmailPropertyKey = DependencyProperty.RegisterReadOnly(
			"SendEmail",
			typeof(ICommand),
			typeof(UnhandledExceptionWindow),
			new FrameworkPropertyMetadata(new RoutedUICommand("Send report", "email", typeof(UnhandledExceptionWindow))));

		/// <summary>
		/// Dependency property for the send e-mail command.
		/// </summary>
		public static readonly DependencyProperty SendEmailProperty = SendEmailPropertyKey.DependencyProperty;

		/// <summary>
		/// Gets the command that sends the exception as e-mail.
		/// </summary>
		public ICommand SendEmail
		{
			get
			{
				return (ICommand)GetValue(SendEmailProperty);
			}
		}
		#endregion

		#region CopyProperty
		private static readonly DependencyPropertyKey CopyPropertyKey = DependencyProperty.RegisterReadOnly(
			"Copy",
			typeof(ICommand),
			typeof(UnhandledExceptionWindow),
			new FrameworkPropertyMetadata(new RoutedUICommand("Copy", "copy", typeof(UnhandledExceptionWindow))));

		/// <summary>
		/// Dependency property for the copy command.
		/// </summary>
		public static readonly DependencyProperty CopyProperty = CopyPropertyKey.DependencyProperty;

		/// <summary>
		/// Gets the command that copies the exception details into the clipboard.
		/// </summary>
		public ICommand Copy
		{
			get
			{
				return (ICommand)GetValue(CopyProperty);
			}
		}
		#endregion

		#region DetailsShownProperty
		/// <summary>
		/// Gets or sets a value indicating whether the details part should be shown or not.
		/// </summary>
		public bool DetailsShown
		{
			get
			{
				return (bool)GetValue(DetailsShownProperty);
			}
			set
			{
				SetValue(DetailsShownProperty, value);
			}
		}

		/// <summary>
		/// Dependency property for the value indicating whether the details part should be shown or not.
		/// </summary>
		public static readonly DependencyProperty DetailsShownProperty = DependencyProperty.Register(
			"DetailsShown",
			typeof(bool),
			typeof(UnhandledExceptionWindow),
			new FrameworkPropertyMetadata(false));
		#endregion

		#region UnhandledExceptionDetailsProperty
		private static readonly DependencyPropertyKey UnhandledExceptionDetailsPropertyKey = DependencyProperty.RegisterReadOnly(
			"UnhandledExceptionDetails",
			typeof(string),
			typeof(UnhandledExceptionWindow),
			new FrameworkPropertyMetadata(null));

		/// <summary>
		/// Dependency property for the detailed message of the unhandled exception
		/// </summary>
		public static readonly DependencyProperty UnhandledExceptionDetailsProperty = UnhandledExceptionDetailsPropertyKey.DependencyProperty;

		/// <summary>
		/// Gets the detailed message of the unhandled exception
		/// </summary>
		public string UnhandledExceptionDetails
		{
			get
			{
				return (string)GetValue(UnhandledExceptionDetailsProperty);
			}
		}
		#endregion

		#region DefaultButtonStyleProperty
		private static readonly DependencyPropertyKey DefaultButtonStylePropertyKey;

		/// <summary>
		/// Dependency property for the default style for the buttons.
		/// </summary>
		public static readonly DependencyProperty DefaultButtonStyleProperty;

		/// <summary>
		/// Gets the default style for the buttons.
		/// </summary>
		public Style DefaultButtonStyle
		{
			get
			{
				return (Style)GetValue(DefaultButtonStyleProperty);
			}
		}
		#endregion

		#region DefaultDetailsToggleButtonStyleProperty
		private static readonly DependencyPropertyKey DefaultDetailsToggleButtonStylePropertyKey;

		/// <summary>
		/// Dependency property for the default style for details-toggle button.
		/// </summary>
		public static readonly DependencyProperty DefaultDetailsToggleButtonStyleProperty;

		/// <summary>
		/// Gets the default style for the details-toggle button.
		/// </summary>
		public Style DefaultDetailsToggleButtonStyle
		{
			get
			{
				return (Style)GetValue(DefaultDetailsToggleButtonStyleProperty);
			}
		}
		#endregion

		#region DefaultDetailsTextBoxStyleProperty
		private static readonly DependencyPropertyKey DefaultDetailsTextBoxStylePropertyKey;

		/// <summary>
		/// Dependency property for the default style for the details textbox.
		/// </summary>
		public static readonly DependencyProperty DefaultDetailsTextBoxStyleProperty;

		/// <summary>
		/// Gets the default style for the details textbox.
		/// </summary>
		public Style DefaultDetailsTextBoxStyle
		{
			get
			{
				return (Style)GetValue(DefaultDetailsTextBoxStyleProperty);
			}
		}
		#endregion

		#region HeaderTextProperty
		/// <summary>
		/// Dependency property for the header text.
		/// </summary>
		public static readonly DependencyProperty HeaderTextProperty = DependencyProperty.Register(
			"HeaderText",
			typeof(string),
			typeof(UnhandledExceptionWindow),
			new UIPropertyMetadata(DefaultHeaderText));

		/// <summary>
		/// Gets or sets the header text.
		/// </summary>
		public string HeaderText
		{
			get
			{
				return (string)GetValue(HeaderTextProperty);
			}
			set
			{
				SetValue(HeaderTextProperty, value);
			}
		}
		#endregion

		#region UserInfoTextProperty
		/// <summary>
		/// Dependency property for the header text.
		/// </summary>
		public static readonly DependencyProperty UserInfoTextProperty = DependencyProperty.Register(
			"UserInfoText",
			typeof(string),
			typeof(UnhandledExceptionWindow),
			new UIPropertyMetadata(DefaultUserInfoText));

		public static readonly DependencyProperty UserInfoControlProperty = DependencyProperty.Register(
			"UserInfoControl",
			typeof(object),
			typeof(UnhandledExceptionWindow),
			new UIPropertyMetadata(null));


		/// <summary>
		/// Gets or sets the user-information text.
		/// </summary>
		public string UserInfoText
		{
			get
			{
				return (string)GetValue(UserInfoTextProperty);
			}
			set
			{
				SetValue(UserInfoTextProperty, value);
			}
		}

		public object UserInfoControl
		{
			get
			{
				return (object)GetValue(UserInfoControlProperty);
			}
			set
			{
				SetValue(UserInfoControlProperty, value);
			}
		}
		

public Visibility UserInfoTextVisbility
{
    get { return (Visibility)GetValue(UserInfoTextVisbilityProperty); }
    set { SetValue(UserInfoTextVisbilityProperty, value); }
}

// Using a DependencyProperty as the backing store for UserInfoTextVisbility.  This enables animation, styling, binding, etc...
public static readonly DependencyProperty UserInfoTextVisbilityProperty = 
    DependencyProperty.Register("UserInfoTextVisbility", typeof(Visibility), typeof(UnhandledExceptionWindow), new UIPropertyMetadata(System.Windows.Visibility.Visible));


		
		#endregion

		#region IsShowHideDetailsEnabledProperty
		/// <summary>
		/// Dependency property for the show-hide details enabled flag.
		/// </summary>
		public static readonly DependencyProperty IsShowHideDetailsEnabledProperty = DependencyProperty.Register(
			"IsShowHideDetailsEnabled",
			typeof(bool),
			typeof(UnhandledExceptionWindow),
			new UIPropertyMetadata(true));

		/// <summary>
		/// Gets or sets a value indicating whether the show/hide details button should be enabled or not.
		/// </summary>
		public bool IsShowHideDetailsEnabled
		{
			get
			{
				return (bool)GetValue(IsShowHideDetailsEnabledProperty);
			}
			set
			{
				SetValue(IsShowHideDetailsEnabledProperty, value);
			}
		}
		#endregion

		#region UnhandledExceptionProperty
		/// <summary>
		/// Dependency property for the unhandled exception.
		/// </summary>
		public static readonly DependencyProperty UnhandledExceptionProperty = DependencyProperty.Register(
			"UnhandledException",
			typeof(Exception),
			typeof(UnhandledExceptionWindow),
			new UIPropertyMetadata(
			null,
			(obj_, e_) =>
			{
				Exception unhandledException = (Exception)e_.NewValue;
				obj_.SetValue(UnhandledExceptionDetailsPropertyKey, unhandledException != null ? unhandledException.GetDetailedMessage() : null);
				CommandManager.InvalidateRequerySuggested();
			}));

		/// <summary>
		/// Gets or sets the unhandled exception.
		/// </summary>
		public Exception UnhandledException
		{
			get
			{
				return (Exception)GetValue(UnhandledExceptionProperty);
			}
			set
			{
				SetValue(UnhandledExceptionProperty, value);
			}
		}
		#endregion

		#region EmailFromAddressProperty
		/// <summary>
		/// Dependency property for the from address for email-sending.
		/// </summary>
		public static readonly DependencyProperty EmailFromAddressProperty = DependencyProperty.Register(
			"EmailFromAddress",
			typeof(string),
			typeof(UnhandledExceptionWindow),
			new UIPropertyMetadata(
			null,
			(obj_, e_) => CommandManager.InvalidateRequerySuggested()));

		/// <summary>
		/// Gets or sets the from address for email-sending.
		/// </summary>
		public string EmailFromAddress
		{
			get
			{
				return (string)GetValue(EmailFromAddressProperty);
			}
			set
			{
				SetValue(EmailFromAddressProperty, value);
			}
		}
		#endregion

		#region EmailSubjectProperty
		/// <summary>
		/// Dependency property for the subject for email-sending.
		/// </summary>
		public static readonly DependencyProperty EmailSubjectProperty = DependencyProperty.Register(
			"EmailSubject",
			typeof(string),
			typeof(UnhandledExceptionWindow),
			new UIPropertyMetadata(
			null,
			(obj_, e_) => CommandManager.InvalidateRequerySuggested()));

		/// <summary>
		/// Gets or sets the subject for email-sending.
		/// </summary>
		public string EmailSubject
		{
			get
			{
				return (string)GetValue(EmailSubjectProperty);
			}
			set
			{
				SetValue(EmailSubjectProperty, value);
			}
		}
		#endregion

		#region EmailAttachmentsProperty
		/// <summary>
		/// Dependency property for the attachments to be attached to the e-mail.
		/// </summary>
		public static readonly DependencyProperty EmailAttachmentsProperty = DependencyProperty.Register(
			"EmailAttachments",
			typeof(Attachment[]),
			typeof(UnhandledExceptionWindow),
			new UIPropertyMetadata(
			null,
			(obj_, e_) => CommandManager.InvalidateRequerySuggested()));

		/// <summary>
		/// Gets or sets the attachments to be attached to the e-mail.
		/// </summary>
		public Attachment[] EmailAttachments
		{
			get
			{
				return (Attachment[])GetValue(EmailAttachmentsProperty);
			}
			set
			{
				SetValue(EmailAttachmentsProperty, value);
			}
		}
		#endregion

		#region EmailToAddressesProperty
		/// <summary>
		/// Dependency property for the to addresses for email-sending.
		/// </summary>
		public static readonly DependencyProperty EmailToAddressesProperty = DependencyProperty.Register(
			"EmailToAddresses",
			typeof(string[]),
			typeof(UnhandledExceptionWindow),
			new UIPropertyMetadata(
			null,
			(obj_, e_) => CommandManager.InvalidateRequerySuggested()));

		/// <summary>
		/// Gets or sets the to addresses for email-sending.
		/// </summary>
		public string[] EmailToAddresses
		{
			get
			{
				return (string[])GetValue(EmailToAddressesProperty);
			}
			set
			{
				SetValue(EmailToAddressesProperty, value);
			}
		}
		#endregion

		#region IsContinueEnabledProperty
		/// <summary>
		/// Dependency property for the value indicating whether the continue command is enabled or not.
		/// </summary>
		public static readonly DependencyProperty IsContinueEnabledProperty = DependencyProperty.Register(
			"IsContinueEnabled",
			typeof(bool),
			typeof(UnhandledExceptionWindow),
			new UIPropertyMetadata(
			true,
			(obj_, e_) => CommandManager.InvalidateRequerySuggested()));

		/// <summary>
		/// Gets or sets a value indicating whether the continue command is enabled or not.
		/// </summary>
		public bool IsContinueEnabled
		{
			get
			{
				return (bool)GetValue(IsContinueEnabledProperty);
			}
			set
			{
				SetValue(IsContinueEnabledProperty, value);
			}
		}
		#endregion

		#region IsTerminateEnabledProperty
		/// <summary>
		/// Dependency property for the value indicating whether the terminate command is enabled or not.
		/// </summary>
		public static readonly DependencyProperty IsTerminateEnabledProperty = DependencyProperty.Register(
			"IsTerminateEnabled",
			typeof(bool),
			typeof(UnhandledExceptionWindow),
			new UIPropertyMetadata(
			true,
			(obj_, e_) => CommandManager.InvalidateRequerySuggested()));

		/// <summary>
		/// Gets or sets a value indicating whether the terminate command is enabled or not.
		/// </summary>
		public bool IsTerminateEnabled
		{
			get
			{
				return (bool)GetValue(IsTerminateEnabledProperty);
			}
			set
			{
				SetValue(IsTerminateEnabledProperty, value);
			}
		}
		#endregion

		#region IsSendEmailEnabledProperty
		/// <summary>
		/// Dependency property for the value indicating whether the send email command is enabled or not.
		/// </summary>
		public static readonly DependencyProperty IsSendEmailEnabledProperty = DependencyProperty.Register(
			"IsSendEmailEnabled",
			typeof(bool),
			typeof(UnhandledExceptionWindow),
			new UIPropertyMetadata(
			true,
			(obj_, e_) => CommandManager.InvalidateRequerySuggested()));

		/// <summary>
		/// Gets or sets a value indicating whether the send email command is enabled or not.
		/// </summary>
		public bool IsSendEmailEnabled
		{
			get
			{
				return (bool)GetValue(IsSendEmailEnabledProperty);
			}
			set
			{
				SetValue(IsSendEmailEnabledProperty, value);
			}
		}
		#endregion

		#region IsCopyVisibleProperty
		public static readonly DependencyProperty IsCopyVisibleProperty = DependencyProperty.Register(
			"IsCopyVisible",
			typeof(System.Windows.Visibility),
			typeof(UnhandledExceptionWindow));

		/// <summary>
		/// Gets or sets a value indicating whether the copy button is visible or not.
		/// </summary>
		public bool IsCopyVisible
		{
			get
			{
				return (System.Windows.Visibility)GetValue(IsCopyVisibleProperty) == System.Windows.Visibility.Visible;
			}
			set
			{
				SetValue(IsCopyVisibleProperty, value ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed);
			}
		}
		#endregion


		#region IsCopyEnabledProperty
		/// <summary>
		/// Dependency property for the value indicating whether the copy command is enabled or not.
		/// </summary>
		public static readonly DependencyProperty IsCopyEnabledProperty = DependencyProperty.Register(
			"IsCopyEnabled",
			typeof(bool),
			typeof(UnhandledExceptionWindow),
			new UIPropertyMetadata(
			true,
			(obj_, e_) => CommandManager.InvalidateRequerySuggested()));

		/// <summary>
		/// Gets or sets a value indicating whether the copy command is enabled or not.
		/// </summary>
		public bool IsCopyEnabled
		{
			get
			{
				return (bool)GetValue(IsCopyEnabledProperty);
			}
			set
			{
				SetValue(IsCopyEnabledProperty, value);
			}
		}

		#endregion

		/// <summary>
		/// Initializes a new instance of the <see cref="UnhandledExceptionWindow"/> class.
		/// </summary>
		public UnhandledExceptionWindow()
		{
			WindowStartupLocation = WindowStartupLocation.CenterOwner;
			DataContext = this;
          
		}

        public UnhandledExceptionWindow(Framework framework_, bool backgroundThreadException_, bool isTerminating_):this()
        {
            framework = framework_;
            this.backgroundThreadException = backgroundThreadException_;
            if (this.backgroundThreadException)
            {
                IsCopyVisible = AppDomainUnhandledExceptions.CopyButtonEnabled;
                IsContinueEnabled = AppDomainUnhandledExceptions.ContinueEnabled && !isTerminating_;
                IsTerminateEnabled = true;
                IsSendEmailEnabled = false;
                UserInfoText = AppDomainUnhandledExceptions.NonDefaultUserInfoText ??
                    (DefaultUserInfoText + (IsContinueEnabled ? DefaultUserInfoTextFoContinue : string.Empty));
                suppressDialog = AppDomainUnhandledExceptions.SuppressDialog;
                attachFullLog = AppDomainUnhandledExceptions.AttachFullLog;
                attachLog = AppDomainUnhandledExceptions.AttachLog;
                attachScreenshot = AppDomainUnhandledExceptions.AttachScreenshot;
                attachMinimizedScreens = AppDomainUnhandledExceptions.AttachMinimizedScreens;

            }
            else
            {
                IsCopyVisible = ApplicationUnhandledExceptions.IsCopyVisibleByDefault;
                IsContinueEnabled = ApplicationUnhandledExceptions.IsContinueEnabledByDefault;
                IsTerminateEnabled = true;
                IsSendEmailEnabled = !ApplicationUnhandledExceptions.SendEmailAutomatically;
                UserInfoText = ApplicationUnhandledExceptions.NonDefaultUserInfoText ??
                    (DefaultUserInfoText + (IsSendEmailEnabled ? DefaultUserInfoTextForSendReport : string.Empty) + (IsContinueEnabled ? DefaultUserInfoTextFoContinue : string.Empty));
                suppressDialog = ApplicationUnhandledExceptions.SuppressDialog;
                attachFullLog = ApplicationUnhandledExceptions.AttachFullLog;
                attachLog = ApplicationUnhandledExceptions.AttachLog;
                attachScreenshot = ApplicationUnhandledExceptions.AttachScreenshot;
                attachMinimizedScreens = ApplicationUnhandledExceptions.AttachMinimizedScreens;
            }

            if (!this.backgroundThreadException && ApplicationUnhandledExceptions.NonDefaultUserInfoFrameworkElement != null)
			{
				UserInfoTextVisbility = Visibility.Collapsed;
                UserInfoControl = ApplicationUnhandledExceptions.NonDefaultUserInfoFrameworkElement;
			}

			// if we cannot continue, we terminate when the window is closed.
			Closed += (sender_, e_) =>
			{
				if (!IsContinueEnabled)
				{
					System.Windows.Application.Current.Shutdown(1);
				}
			};

			// setup continue command
			CommandBindings.Add(
				new CommandBinding(
				Continue,
				(sender_, e_) => DialogResult = true,
				(sender_, e_) => e_.CanExecute = IsContinueEnabled));
			// setup terminate command
			CommandBindings.Add(
				new CommandBinding(
				Terminate,
				(sender_, e_) =>
					{
						System.Windows.Application.Current.Shutdown(1);
						System.Environment.Exit(1);
					},
				(sender_, e_) => e_.CanExecute = IsTerminateEnabled));
			// setup the send mail command
			CommandBindings.Add(
				new CommandBinding(
					SendEmail,
					(sender_, e_) => 
					{ 
						SendMail(UnhandledException, EmailFromAddress, EmailSubject, EmailAttachments, EmailToAddresses);
					    if (!this.backgroundThreadException && ApplicationUnhandledExceptions.EmailButtonCloses)
					    {
					        this.Close();
					        if (!IsContinueEnabled)
					        {
					            Terminate.Execute(null);
					        }
					    }

					},
                    (sender_, e_) => e_.CanExecute = IsSendEmailEnabled &&
													 UnhandledException != null &&
                                                     !String.IsNullOrEmpty(EmailFromAddress) && 
                                                     ((EmailToAddresses != null && EmailToAddresses.Length > 0) ||
                                                      (FrameworkExtensions.MailServiceEnabled && MailServiceExceptionExtensions.IsMailServiceConfigured()))
                ));
			// setup the copy command
			CommandBindings.Add(
				new CommandBinding(
				Copy,
				(sender_, e_) => Clipboard.SetDataObject(UnhandledException.GetDetailedMessage(), true),
				(sender_, e_) => e_.CanExecute = IsCopyEnabled && UnhandledException != null));

			try
			{
        SetResourceReference(BackgroundProperty, Controls.MSGuiColors.WindowBackgroundBrushKey);
        SetResourceReference(ForegroundProperty, Controls.MSGuiColors.TextFillBrushKey);
			}
			catch (Exception)
			{
				Background = System.Windows.Media.Brushes.White;
				Foreground = System.Windows.Media.Brushes.Black;
			}
			if (Background == null || Foreground == null || Foreground == Background)
			{
				Background = System.Windows.Media.Brushes.White;
				Foreground = System.Windows.Media.Brushes.Black;
			}
		}

	    private readonly bool suppressDialog;

	    private readonly bool attachScreenshot;

	    private readonly bool attachLog;

	    private readonly bool attachMinimizedScreens;

	    private readonly bool attachFullLog;
	    private Framework framework;

	    private void SendMail(Exception exception, string fromAddress, string subject, Attachment[] attachments,
	                          params string[] toAddresses)
        {
            //need to check this, since it's used by automatic email sending directly without checking command enablement
            if (!FrameworkExtensions.MailServiceEnabled || !MailServiceExceptionExtensions.IsMailServiceConfigured())
            {
                if (toAddresses == null || toAddresses.Length == 0) return;
            } 
            var streams = new List<MemoryStream>();
            var attachments2 = new List<Attachment>(); 
            if (attachments != null)
            {
                attachments2.AddRange(attachments);
            }
            if (framework != null)
            {
                try
                {
                    if (attachScreenshot)
                    {
                        attachments2.AddRange(framework.GetScreenshotAttachments(streams, Owner, attachMinimizedScreens));
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("Error taking screen shots", ex);
                }

                try
                {
                    if (attachLog)
                    {
                        Attachment attachment = framework.GetLogAttachment(streams, attachFullLog);
                        if (attachment != null)
                        {
                            attachments2.Add(attachment);
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("Error attaching log", ex);
                }
                
            }
	        try
	        {
                if (FrameworkExtensions.MailServiceEnabled)
                    exception.SendMailServiceMail(fromAddress, (suppressDialog && IsContinueEnabled ? "Silent " : "") + subject, attachments2.ToArray(), MailServiceExceptionExtensions.IsMailServiceConfigured() ? new string[] { } : toAddresses);
                else
                    exception.SendMail(fromAddress, (suppressDialog && IsContinueEnabled ? "Silent " : "") + subject, attachments2.ToArray(), toAddresses);
	        }
            finally
            {
                foreach (var attachment in attachments2)
                {
                    attachment.Dispose();
                }

                foreach (var stream in streams)
                {
                    stream.Dispose();
                }  
            }
	        MailSent = true; 

	    }

	    public new bool? ShowDialog()
		{

            //for appdomain unhandled exception, email has already been sent
            if (!backgroundThreadException)
            {
                if (suppressDialog && IsContinueEnabled)
			{
                ((RoutedUICommand)this.SendEmail).Execute(null, this);   
				return true;
			}
                if (ApplicationUnhandledExceptions.SendEmailAutomatically)
            {
                SendMail(UnhandledException, EmailFromAddress, EmailSubject, EmailAttachments, EmailToAddresses);
            }
            }

            return base.ShowDialog();

		}
	}
}
