﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ExceptionHandling/ApplicationUnhandledExceptions.cs#15 $
// $Change: 886728 $
// $DateTime: 2014/07/01 11:32:42 $
// $Author: caijin $

using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Mail;
using System.Windows;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.ExceptionHandling
{
    /// <summary>
    /// Static class that helps with dealing with unhandled exceptions.
    /// </summary>
    public static class ApplicationUnhandledExceptions
    {
        private static readonly IMSLogger _logger = MSLoggerFactory.CreateLogger("ExceptionLogging");
        /// <summary>
        /// Dictionary containing the List of <see cref="DispatcherUnhandledExceptionEventHandler"/>.
        /// </summary>
        private static readonly Dictionary<UnhandledExceptionCallback, List<DispatcherUnhandledExceptionEventHandler>> CallBackHandlers;


        /// <summary>
        /// This field will be used by each instance to initialise the IsCopyVisible property.
        /// </summary>
        internal static bool IsCopyVisibleByDefault = true;

        /// <summary>
        /// This field will be used by each instance to configure the behaviour of the email button.
        /// </summary>
        internal static bool EmailButtonCloses = true;

        /// <summary>
        /// This field will be used by each instance to suppress showing itself and send the email quietly.
        /// </summary>
        internal static bool SuppressDialog = false;

        /// <summary>
        /// This field will be used by each instance to attach screenshot to the exception email.
        /// </summary>
        internal static bool AttachScreenshot = false;

        internal static bool AttachMinimizedScreens = true;

        /// <summary>
        /// This field will be used by each instance to attach last X lines of log to the exception email.
        /// </summary>
        internal static bool AttachLog = false;

        /// <summary>
        /// This field will be used when AttachLog is true to enable not only the exception log, but all other logs
        /// </summary>
        internal static bool AttachFullLog = false;

        /// <summary>
        /// This field will be used by each instance to initialise the IsContinueEnabled property.
        /// </summary>
        internal static bool IsContinueEnabledByDefault = true;

        /// <summary>
        /// If this is true, email would be sent automatically
        /// </summary>
        internal static bool SendEmailAutomatically = false;


        internal static string NonDefaultUserInfoText { get; set; }

        internal static object NonDefaultUserInfoFrameworkElement { get; set; }

        /// <summary>
        /// Lock to use while removing exception handlers.
        /// </summary>
        private static readonly object Lock;

        static ApplicationUnhandledExceptions()
        {
            CallBackHandlers = new Dictionary<UnhandledExceptionCallback, List<DispatcherUnhandledExceptionEventHandler>>();
            Lock = new object();
        }

        /// <summary>
        /// Adds the default unhandled-exception handler to the current <see cref="Application"/>.
        /// </summary>
        public static void AddDefaultHandler(Framework framework_)
        {
            AddDefaultHandler(framework_, System.Windows.Application.Current, null, null, null, new string[0]);
        }

        /// <summary>
        /// Adds the default unhandled-exception handler to the current <see cref="Application"/>.
        /// </summary>
        /// <param name="action_">The action to execute before the unhandled-exception window pops up.</param>
        public static void AddDefaultHandler(Framework framework_, UnhandledExceptionCallback action_)
        {
            AddDefaultHandler(framework_, System.Windows.Application.Current, action_, null, null, null, new string[0]);
        }

        /// <summary>
        /// Adds the default unhandled-exception handler to the current <see cref="Application"/>.
        /// </summary>
        /// <param name="fromAddress_">The from address for email-sending.</param>
        /// <param name="subject_">The subject for email-sending.</param>
        /// <param name="attachments_">The attachments to be attached to the e-mail.</param>
        /// <param name="toAddresses_">The to addresses for email-sending.</param>
        public static void AddDefaultHandler(Framework framework_, string fromAddress_, string subject_, Attachment[] attachments_, params string[] toAddresses_)
        {
            AddDefaultHandler(framework_, System.Windows.Application.Current, null, fromAddress_, subject_, attachments_, toAddresses_);
        }

        /// <summary>
        /// Adds the default unhandled-exception handler to the specified <see cref="Application"/>.
        /// </summary>
        /// <param name="application_">The <see cref="Application"/> object.</param>
        /// <param name="fromAddress_">The from address for email-sending.</param>
        /// <param name="subject_">The subject for email-sending.</param>
        /// <param name="attachments_">The attachments to be attached to the e-mail.</param>
        /// <param name="toAddresses_">The to addresses for email-sending.</param>
        public static void AddDefaultHandler(Framework framework_, System.Windows.Application application_, string fromAddress_, string subject_, Attachment[] attachments_, params string[] toAddresses_)
        {
            AddDefaultHandler(framework_, application_, null, fromAddress_, subject_, attachments_, toAddresses_);
        }

        /// <summary>
        /// Adds the default unhandled-exception handler to the current <see cref="Application"/>.
        /// </summary>
        /// <param name="action_">The action to execute before the unhandled-exception window pops up.</param>
        /// <param name="fromAddress_">The from address for email-sending.</param>
        /// <param name="subject_">The subject for email-sending.</param>
        /// <param name="attachments_">The attachments to be attached to the e-mail.</param>
        /// <param name="toAddresses_">The to addresses for email-sending.</param>
        public static void AddDefaultHandler(Framework framework_, UnhandledExceptionCallback action_, string fromAddress_, string subject_, Attachment[] attachments_, params string[] toAddresses_)
        {
            AddDefaultHandler(framework_, System.Windows.Application.Current, action_, fromAddress_, subject_, attachments_, toAddresses_);
        }

        /// <summary>
        /// Adds the default unhandled-exception handler to the specified <see cref="Application"/>.
        /// </summary>
        /// <param name="application_">The <see cref="Application"/> object.</param>
        /// <param name="action_">The action to execute before the unhandled-exception window pops up.</param>
        /// <param name="fromAddress_">The from address for email-sending.</param>
        /// <param name="subject_">The subject for email-sending.</param>
        /// <param name="attachments_">The attachments to be attached to the e-mail.</param>
        /// <param name="toAddresses_">The to addresses for email-sending.</param>
        public static void AddDefaultHandler(Framework framework_, System.Windows.Application application_, UnhandledExceptionCallback action_, 
            string fromAddress_, string subject_, Attachment[] attachments_, params string[] toAddresses_)
        {
            string[] toAddresses = toAddresses_.Where(addr => !String.IsNullOrEmpty(addr)).ToArray();
            System.Windows.Forms.Application.ThreadException += (sender_, args_) => HandleException(framework_, action_, fromAddress_, subject_, toAddresses,args_.Exception);
            DispatcherUnhandledExceptionEventHandler handler = delegate(object sender_, DispatcherUnhandledExceptionEventArgs e_)
                {
                    HandleException(framework_, action_, fromAddress_, subject_, toAddresses, e_.Exception);
                    e_.Handled = true; 
            };

            application_.DispatcherUnhandledException += handler;
            RegisterHandler(action_, handler);
        }

        private static void HandleException(Framework framework_, UnhandledExceptionCallback action_, string fromAddress_, string subject_,  string[] toAddresses_, Exception exception_)
        {
            _logger.Critical("Unhandled exception", exception_); 
            exception_.SetOccurred();
            IUnhandledExceptionDialog window;
            if (framework_.EnableLegacyDialog)
                window = new UnhandledExceptionWindow(framework_, false, false);
            else
                window = new UnhandledExceptionTaskDialogLauncher(framework_, false, false);

            window.UnhandledException = exception_;
            if (!String.IsNullOrEmpty(fromAddress_))
            {
                window.EmailFromAddress = fromAddress_;
            }
            if (!String.IsNullOrEmpty(subject_))
            {
                window.EmailSubject = subject_;
            }
            if (toAddresses_.Length > 0)
            {
                window.EmailToAddresses = toAddresses_;
            }
            else if (!FrameworkExtensions.MailServiceEnabled || !MailServiceExceptionExtensions.IsMailServiceConfigured())
            {
                window.IsSendEmailEnabled = false;
            }

            if (action_ != null)
            {
                try
                {
                    action_(exception_, window);
                }
                catch (Exception exc)
                {
                    _logger.Critical("Unhandled exception - reentry", exc);
                    //our exception handler shouldn't produce any new exceptions
                    TaskDialog.ShowMessage(exc.Message, "Error");
                }
            }

            // The condition is needed to ensure that the dialog is shown even if the main window is not shown yet
            // (eg. the exception is raised in the constructor)
            // After calling .ShutDown() System.Windows.Application.Current will be null to enable GC to act on resources
            if (System.Windows.Application.Current != null && System.Windows.Application.Current.MainWindow != null && System.Windows.Application.Current.MainWindow.IsVisible)
            {
                window.Owner = System.Windows.Application.Current.MainWindow;
            }
            else
            {
                if (window is UnhandledExceptionWindow)
                    (window as UnhandledExceptionWindow).WindowStartupLocation = WindowStartupLocation.CenterScreen;
            }
            try
            {
                if (SplashLauncher<SplashWindow>.SplashWindowInstance != null)
                { //TODO potential multithreading problems (as in any other place where we use .SplashWindowInstance property)
                    SplashLauncher<SplashWindow>.SplashWindowInstance.Dispatcher.Invoke(
                      (Action)(() => SplashLauncher<SplashWindow>.SplashWindowInstance.Hide()));
                }
                else if (SplashLauncher<ModernSplashWindow>.SplashWindowInstance != null)
                { //TODO potential multithreading problems (as in any other place where we use .SplashWindowInstance property)
                    SplashLauncher<ModernSplashWindow>.SplashWindowInstance.Dispatcher.Invoke(
                      (Action)(() => SplashLauncher<ModernSplashWindow>.SplashWindowInstance.Hide()));
                }

                var initialWindow = InitialSplashWindowFrameworkWrapper.InitialWindow;
                if (initialWindow != null)
                {
                    initialWindow.IsHidden = true;
                }
            }
            catch (Exception exc)
            {
                _logger.Critical("Unhandled exception - reentry", exc);
            }
            try
            {
                window.ShowDialog();
            }
            catch (Exception dialogExc) // Showing error dialog should not break applications
            {
                _logger.Critical("Unhandled exception - reentry", dialogExc);
                //TODO: Log
            }
            try
            {
                var initialWindow = InitialSplashWindowFrameworkWrapper.InitialWindow;
                if (initialWindow != null)
                {
                    initialWindow.IsHidden = false;
                }
                if (SplashLauncher<SplashWindow>.SplashWindowInstance != null)
                {
                    SplashLauncher<SplashWindow>.SplashWindowInstance.Dispatcher.Invoke(
                      (Action)(() => SplashLauncher<SplashWindow>.SplashWindowInstance.Show()));
                }
                else if (SplashLauncher<ModernSplashWindow>.SplashWindowInstance != null)
                {
                    SplashLauncher<ModernSplashWindow>.SplashWindowInstance.Dispatcher.Invoke(
                      (Action)(() => SplashLauncher<ModernSplashWindow>.SplashWindowInstance.Show()));
                }

            }
            catch (Exception exc)
            {
                _logger.Critical("Unhandled exception - reentry", exc);
            } 

        }
 
        /// <summary>
        /// Register handler so that it can be removed.
        /// </summary>
        /// <param name="callback_">User callback.</param>
        /// <param name="handler_">Internal handler.</param>
        private static void RegisterHandler(UnhandledExceptionCallback callback_, DispatcherUnhandledExceptionEventHandler handler_)
        {
            if (!CallBackHandlers.ContainsKey(callback_))
            {
                CallBackHandlers[callback_] = new List<DispatcherUnhandledExceptionEventHandler>();
            }

            CallBackHandlers[callback_].Add(handler_);
        }

        /// <summary>
        /// Removes the default unhandled-exception handler from the current <see cref="Application"/>.
        /// </summary>
        /// <param name="action_">The action to execute before the unhandled-exception window pops up.</param>
        public static void RemoveDefaultHandler(UnhandledExceptionCallback action_)
        {
            RemoveDefaultHandler(System.Windows.Application.Current, action_);
        }

        /// <summary>
        /// Removes the default unhandled-exception handler from the specified <see cref="Application"/>.
        /// </summary>
        /// <param name="application_">The <see cref="Application"/> object.</param>
        /// <param name="action_">The action to execute before the unhandled-exception window pops up.</param>
        public static void RemoveDefaultHandler(System.Windows.Application application_, UnhandledExceptionCallback action_)
        {
            lock (Lock)
            {
                if (CallBackHandlers.ContainsKey(action_))
                {
                    List<DispatcherUnhandledExceptionEventHandler> callbacks = CallBackHandlers[action_];

                    if (callbacks != null)
                    {
                        foreach (DispatcherUnhandledExceptionEventHandler handler in callbacks)
                        {
                            application_.DispatcherUnhandledException -= handler;
                        }
                        CallBackHandlers[action_].Clear();
                        CallBackHandlers.Remove(action_);
                    }
                }
            }
        }
    }
}

