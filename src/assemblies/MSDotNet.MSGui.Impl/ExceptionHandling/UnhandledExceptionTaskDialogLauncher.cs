﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows; 
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop; 
using MorganStanley.MSDotNet.MSGui.Impl.Extensions; 
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.ExceptionHandling
{
    public class UnhandledExceptionTaskDialogLauncher : IUnhandledExceptionDialog
    {
        private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger<UnhandledExceptionTaskDialogLauncher>();

        private AutoResetEvent emailSentEvent = new AutoResetEvent(false);
        private volatile bool sendingEmail = false;
        public UnhandledExceptionTaskDialogLauncher()
        {

        }

        internal UnhandledExceptionTaskDialogLauncher(Framework f, bool backgroundThreadException_, bool isTerminating_) : this()
        {
            _framework = f;
            backgroundThreadException = backgroundThreadException_;
            isTerminating = isTerminating_;
        }

        public bool? ShowDialog()
        {
            if (backgroundThreadException)
            {
                IsCopyEnabled = AppDomainUnhandledExceptions.CopyButtonEnabled;
                IsContinueEnabled = AppDomainUnhandledExceptions.ContinueEnabled && !isTerminating;
                IsTerminateEnabled = true;
                IsSendEmailEnabled = false;
                UserInfoText = AppDomainUnhandledExceptions.NonDefaultUserInfoText ?? 
                    (DefaultUserInfoText + (IsContinueEnabled ? DefaultUserInfoTextFoContinue : string.Empty));
                suppressDialog = AppDomainUnhandledExceptions.SuppressDialog;
                attachFullLog = AppDomainUnhandledExceptions.AttachFullLog;
                attachLog = AppDomainUnhandledExceptions.AttachLog;
                attachScreenshot = AppDomainUnhandledExceptions.AttachScreenshot;
                attachMinimizedScreens = AppDomainUnhandledExceptions.AttachMinimizedScreens;

            }
            else
            {
                IsCopyEnabled = ApplicationUnhandledExceptions.IsCopyVisibleByDefault;
                IsContinueEnabled = ApplicationUnhandledExceptions.IsContinueEnabledByDefault;
                IsTerminateEnabled = true;
                IsSendEmailEnabled = !ApplicationUnhandledExceptions.SendEmailAutomatically;
                UserInfoText = ApplicationUnhandledExceptions.NonDefaultUserInfoText ??
                    (DefaultUserInfoText + (IsSendEmailEnabled ? DefaultUserInfoTextForSendReport : string.Empty) + (IsContinueEnabled ? DefaultUserInfoTextFoContinue : string.Empty));
                suppressDialog = ApplicationUnhandledExceptions.SuppressDialog;
                attachFullLog = ApplicationUnhandledExceptions.AttachFullLog;
                attachLog = ApplicationUnhandledExceptions.AttachLog;
                attachScreenshot = ApplicationUnhandledExceptions.AttachScreenshot;
                attachMinimizedScreens = ApplicationUnhandledExceptions.AttachMinimizedScreens;
            }
            //for appdomain unhandled exception, email has already been sent
            if (!backgroundThreadException)
            {
                if (suppressDialog && IsContinueEnabled)
                {
                    PrepareAndSendMail();
                    return true;
                }
                if (ApplicationUnhandledExceptions.SendEmailAutomatically)
                {
                    PrepareAndSendMail();
                }
            }


            var options = new TaskDialogOptions
                {
                    Title = Title ?? DefaultTitle,
                    MainInstruction = HeaderText ?? DefaultHeaderText,
                    Content = UserInfoText,
                    MainIcon = VistaTaskDialogIcon.Error,
                    ExpandedInfo = UnhandledException == null ? null : UnhandledException.GetDetailedMessage(),
                    Owner = Owner,
                    AllowDialogCancellation = false,
                };

            IList<string> buttonList = new List<string>();
            if (IsContinueEnabled)
                buttonList.Add("Continue");
            if (IsTerminateEnabled)
                buttonList.Add("Terminate");
            if (IsSendEmailEnabled &&
                UnhandledException != null &&
                !String.IsNullOrEmpty(EmailFromAddress) &&
                ((EmailToAddresses != null && EmailToAddresses.Length > 0) || 
                (FrameworkExtensions.MailServiceEnabled && MailServiceExceptionExtensions.IsMailServiceConfigured()))
                )
                buttonList.Add("Send report");
            if (IsCopyEnabled && UnhandledException != null)
                buttonList.Add("Copy");
            options.CustomButtons = buttonList.ToArray();

            options.CustomButtonActions = new Dictionary<string, Action>();

            // Configure Copy button
            options.CustomButtonActions.Add("Copy",
                                            () => Clipboard.SetDataObject(UnhandledException.GetDetailedMessage(), true));
            
            // Configure Send Report button 
            TaskDialogHandle handler = new TaskDialogHandle();
            options.CustomButtonActions.Add("Send report", new Action(() =>
            {
                PrepareAndSendMail();
                if (!backgroundThreadException && ApplicationUnhandledExceptions.EmailButtonCloses)
                {
                    handler.Close();
                    if (!IsContinueEnabled)
                    {
                        if (sendingEmail)
                        {
                            emailSentEvent.WaitOne();
                        }
                        System.Windows.Application.Current.Shutdown(1);
                        Environment.Exit(1);
                    }
                }

            }));


            var result = TaskDialog.Show(options, handler); 
            if (result.CustomButtonResult.HasValue)
            {
                switch (buttonList[result.CustomButtonResult.Value])
                {
                    case "Continue":
                        return true;
                    case "Terminate":
                        if (sendingEmail)
                        {
                            emailSentEvent.WaitOne();
                        }
                        System.Windows.Application.Current.Shutdown(1);
                        System.Environment.Exit(1);
                        break;
                }
            }
            if (!IsContinueEnabled)
            {
                System.Windows.Application.Current.Shutdown(1);
            }
            return true;
        }
 

        private void PrepareAndSendMail()
        {
            var streams = new List<MemoryStream>();
            var attachments = new List<Attachment>();
            try
            {
                if (attachScreenshot)
                {
                    attachments.AddRange(_framework.GetScreenshotAttachments(streams, Owner, attachMinimizedScreens));
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error taking screen shots", ex);
            }


            Task.Factory.StartNew(new Action(() =>
            {
                try
                {
                    sendingEmail = true;
                    try
                    {
                        if (attachLog)
                        {
                            Attachment attachment = _framework.GetLogAttachment(streams, attachFullLog);
                            if (attachment != null)
                            {
                                attachments.Add(attachment);
                            }
                        } 
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Error attaching log", ex);
                    }

                    var addedAttachments = attachments.ToArray();
                    SendMail(
                        UnhandledException,
                        EmailFromAddress,
                        EmailSubject,
                        EmailAttachments == null
                            ? addedAttachments
                            : EmailAttachments.Union(addedAttachments).ToArray(),
                        EmailToAddresses)
                        ;
                     
                }
                finally
                {
                    foreach (var attachment in attachments)
                    {
                        attachment.Dispose();
                    }

                    foreach (var stream in streams)
                    {
                        stream.Dispose();
                    }
                    emailSentEvent.Set();
                    sendingEmail = false;
                }
            })
                );
        }

        #region Properties

        public Exception UnhandledException { get; set; }

        public Attachment[] EmailAttachments { get; set; }

        public bool MailSent { get; private set; }

        public bool IsSendEmailEnabled { get; set; }

        public string EmailFromAddress { get; set; }

        public string[] EmailToAddresses { get; set; }

        public string EmailSubject { get; set; }

        public string Title { get; set; }

        public string HeaderText { get; set; }

        public string UserInfoText { get; set; }

        public bool IsCopyEnabled { get; set; }

        public bool IsTerminateEnabled { get; set; }

        public bool IsContinueEnabled { get; set; }

        public Window Owner { get; set; }

        private readonly bool backgroundThreadException;

        private readonly bool isTerminating;
        private bool suppressDialog;

        private bool attachScreenshot;

        private bool attachLog;

        private bool attachMinimizedScreens;

        private bool attachFullLog;
        #endregion

        #region Settings

        private readonly Framework _framework;

        private const string DefaultTitle = @"Unhandled exception occurred";

        private const string DefaultHeaderText = @"An error occurred while running the application.";

        private const string DefaultUserInfoText = "We are sorry but the application encountered an unexpected error. Your unsaved data may have been damaged or lost. ";

        private const string DefaultUserInfoTextForSendReport =  "Please inform us about this event by clicking the Send email button. ";

        private const string DefaultUserInfoTextFoContinue =  "You may click Continue button to continue running the application " +
             "but it may result in unexpected behaviour thus it is highly recommended to terminate the application.";

        #endregion

        #region Private
 

        private void SendMail(Exception exception, string fromAddress, string subject,
                              IEnumerable<Attachment> attachments, params string[] toAddresses)
        {
            if (!FrameworkExtensions.MailServiceEnabled || !MailServiceExceptionExtensions.IsMailServiceConfigured())
            {
                if (toAddresses == null || toAddresses.Length == 0) return;
            }

            var attachmentAray = attachments != null ? attachments.ToArray() : new Attachment[] {};

            if (FrameworkExtensions.MailServiceEnabled)
            {
                exception.SendMailServiceMail(
                    fromAddress,
                    (suppressDialog && IsContinueEnabled ? "Silent " : "") + subject,
                    attachmentAray,
                    MailServiceExceptionExtensions.IsMailServiceConfigured() ? new string[] {} : toAddresses);
            }
            else
            {
                exception.SendMail(fromAddress, (suppressDialog && IsContinueEnabled ? "Silent " : "") + subject, attachmentAray, toAddresses);
            }



            MailSent = true;
        }

        #endregion
    }
}
