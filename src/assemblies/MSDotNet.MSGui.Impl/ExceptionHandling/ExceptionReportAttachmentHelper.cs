﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Windows; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.MailAttachment; 
using MorganStanley.MSDotNet.MSLog;
using Microsoft.Practices.Unity;

namespace MorganStanley.MSDotNet.MSGui.Impl.ExceptionHandling
{
    internal static class ExceptionReportAttachmentHelper
    {
        public static Attachment GetLogAttachment(this Framework framework_, List<MemoryStream> streams_, bool attachFullLog_)
        {
            // Get the log from in-memory logger
            var memoryDestination =
                (MSLogMemoryRollingDestination)MSLog.MSLog.GetNamedDestination("ExceptionLogger");

            var logFileBuilder = new StringBuilder();
            memoryDestination.Formatter = new MSLogDefaultFormatter();
            foreach (var logMessage in memoryDestination.Messages.Where(logMessage => logMessage != null))
            {
                var messageBuilder = new StringBuilder();
                memoryDestination.Formatter.Format(logMessage, messageBuilder);
                logFileBuilder.AppendLine(messageBuilder.ToString());
            }

            var log = logFileBuilder.ToString();

            var logFiles = new List<string>();
            if (attachFullLog_)
            {
                foreach (var destination in MSLog.MSLog.Destinations)
                {
                    var fileDestination = destination as MSLogFileDestination;
                    if (fileDestination != null && !string.IsNullOrEmpty(fileDestination.FileName))
                    {
                        try
                        {
                            FileInfo file = new FileInfo(fileDestination.FileName);
                            if (file.Exists && file.LastWriteTime.CompareTo(DateTime.Now.AddHours(-1)) > 0)
                            {
                                logFiles.Add(file.FullName);
                            }
                        }
                        catch
                        {
                        }
                    }
                }
            }
            if (logFiles.Count == 0)
            {
                var logfileStream = new MemoryStream(Encoding.UTF8.GetBytes(log));
                streams_.Add(logfileStream);
                return new Attachment(logfileStream, "activitylog.log");
            }
            else
            {
                return GetZippedLogAttachment(framework_, log, logFiles);
            }
        }

        private static Attachment GetZippedLogAttachment(Framework framework_, string log_, List<string> logFiles_)
        {
            string exceptionLogFileName = Path.GetTempFileName() + ".log";
            File.WriteAllText(exceptionLogFileName, log_);
            logFiles_.Insert(0, exceptionLogFileName);

            string name = Path.ChangeExtension(Path.GetTempFileName(), ".log.zip");
            string tempFilePath = Path.Combine(Path.GetDirectoryName(name),
                                               string.Format("{0}.{1}.{2}_", Environment.MachineName, Environment.UserName,
                                                             DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss")) +
                                               Path.GetFileName(name));

            var zipOutputStream = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(File.Create(tempFilePath));
            zipOutputStream.SetLevel(9);

            foreach (var logFile in logFiles_)
            {
                FileInfo file = new FileInfo(logFile);
                string fileName = null;
                if (logFile == exceptionLogFileName)
                {
                    fileName = framework_.Bootstrapper.Application.Name + "_exceptionlog_" + DateTime.Now.ToFileTimeUtc() +
                               ".log";
                }
                else
                {
                    fileName = file.Name;
                }
                if (file.Exists)
                {
                    using (var fs = new FileStream(logFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        var buffer = new Byte[fs.Length];
                        fs.Read(buffer, 0, buffer.Length);

                        var entry = new ICSharpCode.SharpZipLib.Zip.ZipEntry(fileName);
                        entry.DateTime = file.LastWriteTime;
                        zipOutputStream.PutNextEntry(entry);
                        zipOutputStream.Write(buffer, 0, buffer.Length);
                        zipOutputStream.Flush();
                    }
                }
            }
            zipOutputStream.Flush();
            zipOutputStream.Finish();
            zipOutputStream.Close();
            File.Delete(exceptionLogFileName);
            Attachment attachment = framework_.Container.Resolve<IMailAttachmentService>().GenerateAttachment(tempFilePath, "activitylog.zip");
            File.Delete(tempFilePath);
            return attachment;
        }

        public static Attachment[] GetScreenshotAttachments(this Framework framework_, List<MemoryStream> streams_,  Window mainWindow_, bool attachMinimizedScreens_)
        {
            List<Attachment> attachments = new List<Attachment>();
            if (mainWindow_ != null)
            {
                if (attachMinimizedScreens_ || mainWindow_.WindowState != WindowState.Minimized)
                {
                    var imageBytes = mainWindow_.GetScreenShotBytes(true);
                    if (imageBytes != null)
                    {
                        var screenshotStream = new MemoryStream(imageBytes);
                        var screenshotAttachment = new Attachment(screenshotStream, "screenshot-main.png");
                        streams_.Add(screenshotStream);
                        attachments.Add(screenshotAttachment);
                    } 
                }
            }


            // See if we have chrome manager and if so, go over floating islands we can find
            if (framework_ != null)
            {
                // Prepare attachments from images. They will come in PNG
                var counter = 1;
                foreach (var islandImage in GetFloatingIslands(framework_, attachMinimizedScreens_))
                {
                    if (islandImage.Item2 == null) continue;
                    var stream = new MemoryStream(islandImage.Item2);
                    var islandAttachment = new Attachment(stream, string.Format("panel{0}{1}.png", counter, islandImage.Item1 ? "_minimized" : ""));
                    attachments.Add(islandAttachment);
                    streams_.Add(stream);
                    counter++;
                }
            }
            return attachments.ToArray();
        }


        private static IEnumerable<Tuple<bool, byte[]>> GetFloatingIslands(Framework framework_, bool attachMinimizedScreens_)
        {
            var chromeManager = framework_.Container.Resolve<IChromeManager>();
            var includedPanes = new Dictionary<Guid, IFloatingWindow>();
            foreach (var window in chromeManager.Windows)
            {
                var pane = DockHelper.GetRootOfFloatingPane(window);
                if (pane != null && (attachMinimizedScreens_ || pane.WindowState != WindowState.Minimized))
                {
                    if (!includedPanes.ContainsKey(pane.GuidID))
                    {
                        includedPanes[pane.GuidID] = pane;
                    }
                }
            }

            return includedPanes.Values.Select(v => new Tuple<bool, byte[]>(v.WindowState == WindowState.Minimized, ((FrameworkElement)v).GetScreenShotBytes(true)));
        }
    }
}
