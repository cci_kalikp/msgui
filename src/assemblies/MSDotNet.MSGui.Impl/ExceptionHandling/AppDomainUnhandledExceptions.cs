﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ExceptionHandling/AppDomainUnhandledExceptions.cs#18 $
// $Change: 888143 $
// $DateTime: 2014/07/14 01:18:26 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Windows;
using System.Threading;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.My;
using DispatcherPriority = System.Windows.Threading.DispatcherPriority;

namespace MorganStanley.MSDotNet.MSGui.Impl.ExceptionHandling
{
  /// <summary>
  /// Static class that helps with dealing with unhandled exceptions.
  /// </summary>
  public static class AppDomainUnhandledExceptions
  {
      private static readonly IMSLogger _logger = MSLoggerFactory.CreateLogger("ExceptionLogging");
    /// <summary>
    /// Dictionary containing the List of <see cref="UnhandledExceptionEventHandler"/>.
    /// </summary>
    private static readonly Dictionary<UnhandledExceptionCallback, List<UnhandledExceptionEventHandler>> CallBackHandlers;

    /// <summary>
    /// Lock to use while removing exception handlers.
    /// </summary>
    private static readonly object Lock;

      internal static string NonDefaultUserInfoText;

      internal static bool AttachLog = false;
      internal static bool AttachScreenshot = false;
      internal static bool AttachFullLog = false;
      internal static bool AttachMinimizedScreens = true;
      internal static bool SuppressDialog = false;
      internal static bool ContinueEnabled = true;
      internal static bool CopyButtonEnabled = true;
    /// <summary>
    /// Has the error message already been displayed.
    /// </summary>
    private static int m_errorMessageDisplayStatus = 0;

    static AppDomainUnhandledExceptions()
    {
      CallBackHandlers = new Dictionary<UnhandledExceptionCallback, List<UnhandledExceptionEventHandler>>();
      Lock = new object();
    }

    /// <summary>
    /// Adds the default unhandled-exception handler to the <see cref="AppDomain"/>.
    /// </summary>
    public static void AddDefaultHandler()
    {
      AddDefaultHandler(AppDomain.CurrentDomain, null, null, null, new string[0]);
    }

    /// <summary>
    /// Adds the default unhandled-exception handler to the <see cref="AppDomain"/>.
    /// </summary>
    /// <param name="action_">The action to execute before the unhandled-exception window pops up.</param>
    public static void AddDefaultHandler(UnhandledExceptionCallback action_)
    {
      AddDefaultHandler(AppDomain.CurrentDomain, action_, null, null, null, new string[0]);
    }

    /// <summary>
    /// Adds the default unhandled-exception handler to the <see cref="AppDomain"/>.
    /// </summary>
    /// <param name="fromAddress_">The from address for email-sending.</param>
    /// <param name="subject_">The subject for email-sending.</param>
    /// <param name="attachments_">The attachments to be attached to the e-mail.</param>
    /// <param name="toAddresses_">The to addresses for email-sending.</param>
    public static void AddDefaultHandler(string fromAddress_, string subject_, Attachment[] attachments_, params string[] toAddresses_)
    {
      AddDefaultHandler(AppDomain.CurrentDomain, null, fromAddress_, subject_, attachments_, toAddresses_);
    }

    /// <summary>
    /// Adds the default unhandled-exception handler to the <see cref="AppDomain"/>.
    /// </summary>
    /// <param name="appDomain_">The <see cref="AppDomain"/> object.</param>
    /// <param name="fromAddress_">The from address for email-sending.</param>
    /// <param name="subject_">The subject for email-sending.</param>
    /// <param name="attachments_">The attachments to be attached to the e-mail.</param>
    /// <param name="toAddresses_">The to addresses for email-sending.</param>
    public static void AddDefaultHandler(AppDomain appDomain_, string fromAddress_, string subject_, Attachment[] attachments_, params string[] toAddresses_)
    {
      AddDefaultHandler(appDomain_, null, fromAddress_, subject_, attachments_, toAddresses_);
    }

    /// <summary>
    /// Adds the default unhandled-exception handler to the current <see cref="AppDomain"/>.
    /// </summary>
    /// <param name="action_">The action to execute before the unhandled-exception window pops up.</param>
    /// <param name="fromAddress_">The from address for email-sending.</param>
    /// <param name="subject_">The subject for email-sending.</param>
    /// <param name="attachments_">The attachments to be attached to the e-mail.</param>
    /// <param name="toAddresses_">The to addresses for email-sending.</param>
    public static void AddDefaultHandler(UnhandledExceptionCallback action_, string fromAddress_, string subject_, Attachment[] attachments_, params string[] toAddresses_)
    {
      AddDefaultHandler(AppDomain.CurrentDomain, action_, fromAddress_, subject_, attachments_, toAddresses_);
    }

    public static void AddDefaultHandler(Framework framework_, UnhandledExceptionCallback action_, string fromAddress_, string subject_, Attachment[] attachments_, params string[] toAddresses_)
    {
        AddDefaultHandler(framework_, AppDomain.CurrentDomain, action_, fromAddress_, subject_, attachments_, toAddresses_);
    }


    /// <summary>
    /// Adds the default unhandled-exception handler to the <see cref="AppDomain"/>.
    /// </summary>
    /// <param name="appDomain_">The <see cref="AppDomain"/> object.</param>
    /// <param name="action_">The action to execute before the unhandled-exception window pops up.</param>
    /// <param name="fromAddress_">The from address for email-sending.</param>
    /// <param name="subject_">The subject for email-sending.</param>
    /// <param name="attachments_">The attachments to be attached to the e-mail.</param>
    /// <param name="toAddresses_">The to addresses for email-sending.</param>
    public static void AddDefaultHandler(AppDomain appDomain_, UnhandledExceptionCallback action_, string fromAddress_, string subject_, Attachment[] attachments_, params string[] toAddresses_)
    {
        AddDefaultHandler(null, appDomain_, action_, fromAddress_, subject_, attachments_, toAddresses_);
    }
    public static void AddDefaultHandler(Framework framework_, AppDomain appDomain_, UnhandledExceptionCallback action_, string fromAddress_, string subject_, Attachment[] attachments_, params string[] toAddresses_)
    {
 
        UnhandledExceptionEventHandler handler = delegate(object sender_, UnhandledExceptionEventArgs e_)
        {
            HandleException(e_.ExceptionObject as Exception, framework_, e_.IsTerminating, action_, fromAddress_, subject_, attachments_, toAddresses_);
        };

        appDomain_.UnhandledException += handler;
        RegisterHandler(action_, handler);
    }
       
      private static void HandleException(Exception exception_, Framework framework_, bool isTerminating_, UnhandledExceptionCallback action_, string fromAddress_,
                                          string subject_, Attachment[] attachments_, params string[] toAddresses_)
      {
          _logger.Critical("Unhandled exception", exception_);
          exception_.SetOccurred();
          //when the application is going to terminate, always show the dialog to avoid exiting the application silently
          bool silentException = !isTerminating_ && SuppressDialog && ContinueEnabled;

          if (silentException)
          {
              //send email first
              SendMail(exception_, framework_, true, fromAddress_, subject_, attachments_, toAddresses_); 
              return;
          } 
          if (Interlocked.CompareExchange(ref m_errorMessageDisplayStatus, 1, m_errorMessageDisplayStatus) == 0)
          {
              //send email first
              SendMail(exception_, framework_, false, fromAddress_, subject_, attachments_, toAddresses_); 
              System.Windows.Application.Current.Dispatcher.Invoke(new Action(() =>
              { 
                  IUnhandledExceptionDialog window;
                  if (framework_.EnableLegacyDialog)
                      window = new UnhandledExceptionWindow(framework_, true, isTerminating_);
                  else
                      window = new UnhandledExceptionTaskDialogLauncher(framework_, true, isTerminating_);

                  window.UnhandledException = exception_;
                  if (!String.IsNullOrEmpty(fromAddress_))
                  {
                      window.EmailFromAddress = fromAddress_;
                  }
                  if (!String.IsNullOrEmpty(subject_))
                  {
                      window.EmailSubject = subject_;
                  }
                  if (toAddresses_.Length > 0)
                  {
                      window.EmailToAddresses = toAddresses_;
                  }
                  else if (!FrameworkExtensions.MailServiceEnabled || !MailServiceExceptionExtensions.IsMailServiceConfigured())
                  {
                      window.IsSendEmailEnabled = false;
                  }

                  if (action_ != null)
                  {
                      try
                      {
                          action_(exception_, window);
                      }
                      catch (Exception exc)
                      {
                          _logger.Critical("Unhandled exception - reentry", exc);
                          //our exception handler shouldn't produce any new exceptions
                          //TaskDialog.ShowMessage(exc.Message, "Error");
                      }
                  }
                  if (System.Windows.Application.Current.MainWindow != null && System.Windows.Application.Current.MainWindow.IsVisible)
                  {
                      window.Owner = System.Windows.Application.Current.MainWindow;
                  }
                  else
                  {
                      if (window is UnhandledExceptionWindow)
                          (window as UnhandledExceptionWindow).WindowStartupLocation = WindowStartupLocation.CenterScreen;
                  }
                  try
                  {
                      if (SplashLauncher<SplashWindow>.SplashWindowInstance != null)
                      { //TODO potential multithreading problems (as in any other place where we use .SplashWindowInstance property)
                          SplashLauncher<SplashWindow>.SplashWindowInstance.Dispatcher.Invoke(
                            (Action)(() => SplashLauncher<SplashWindow>.SplashWindowInstance.Hide()));
                      }
                      else if (SplashLauncher<ModernSplashWindow>.SplashWindowInstance != null)
                      { //TODO potential multithreading problems (as in any other place where we use .SplashWindowInstance property)
                          SplashLauncher<ModernSplashWindow>.SplashWindowInstance.Dispatcher.Invoke(
                            (Action)(() => SplashLauncher<ModernSplashWindow>.SplashWindowInstance.Hide()));
                      }

                      var initialWindow = InitialSplashWindowFrameworkWrapper.InitialWindow;
                      if (initialWindow != null)
                      {
                          initialWindow.IsHidden = true;
                      }
                  }
                  catch (Exception exc)
                  {
                      _logger.Critical("Unhandled exception - reentry", exc);
                  }
                  try
                  {
                      window.ShowDialog();
                  }
                  catch (Exception dialogExc) // Showing error dialog should not break applications
                  {
                      _logger.Critical("Unhandled exception - reentry", dialogExc);
                  }
                  try
                  {
                      var initialWindow = InitialSplashWindowFrameworkWrapper.InitialWindow;
                      if (initialWindow != null)
                      {
                          initialWindow.IsHidden = false;
                      }
                      if (SplashLauncher<SplashWindow>.SplashWindowInstance != null)
                      {
                          SplashLauncher<SplashWindow>.SplashWindowInstance.Dispatcher.Invoke(
                              (Action) (() => SplashLauncher<SplashWindow>.SplashWindowInstance.Show()));
                      }
                      else if (SplashLauncher<ModernSplashWindow>.SplashWindowInstance != null)
                      {
                          SplashLauncher<ModernSplashWindow>.SplashWindowInstance.Dispatcher.Invoke(
                              (Action) (() => SplashLauncher<ModernSplashWindow>.SplashWindowInstance.Show()));
                      }

                  }
                  catch (Exception exc)
                  {
                      _logger.Critical("Unhandled exception - reentry", exc);
                  }
                  finally
                  {
                      if (!isTerminating_)
                      {
                          Interlocked.CompareExchange(ref m_errorMessageDisplayStatus, 0, m_errorMessageDisplayStatus);      
                      }
                  }
              }));

          }
          else
          {
              Thread.Sleep(15000);
          }
 
      }

      private static void SendMail(Exception exception_, Framework framework_, bool silentException_, string fromAddress_,
                                          string subject_, Attachment[] attachments_, params string[] toAddresses_)
      { 
          // Mail the exception if the user has provided the mailing details
          if (!String.IsNullOrEmpty(fromAddress_) &&
              ((toAddresses_ != null && toAddresses_.Length > 0)) ||
              (FrameworkExtensions.MailServiceEnabled && MailServiceExceptionExtensions.IsMailServiceConfigured()))
          {
              var streams = new List<MemoryStream>();
              var attachments2 = new List<Attachment>();
              try
              {
                  if (framework_ != null)
                  { 
                      if (attachments_ != null)
                      {
                          attachments2.AddRange(attachments_);
                      }
                      try
                      {
                          if (AttachScreenshot && System.Windows.Application.Current != null)
                          {
                              System.Windows.Application.Current.Dispatcher.Invoke(new Action(() =>
                              {
                                  Window mainWindow = null;
                                  if (System.Windows.Application.Current != null &&
                                      System.Windows.Application.Current.MainWindow != null &&
                                      System.Windows.Application.Current.MainWindow.IsVisible)
                                  {
                                      mainWindow = System.Windows.Application.Current.MainWindow;
                                  }
                                  attachments2.AddRange(framework_.GetScreenshotAttachments(streams, mainWindow, AttachMinimizedScreens));

                              }));
                          }
                      }
                      catch (Exception ex)
                      { 
                          _logger.Error("Error taking screen shots", ex);
                      }

                      try
                      {
                          if (AttachLog)
                          {
                              Attachment attachment = framework_.GetLogAttachment(streams, AttachFullLog);
                              if (attachment != null)
                              {
                                  attachments2.Add(attachment);
                              }
                          }

                      }
                      catch (Exception ex)
                      {
                          _logger.Error("Error attaching log", ex);
                      }

                  }
                  if (FrameworkExtensions.MailServiceEnabled)
                      exception_.SendMailServiceMail(fromAddress_, (silentException_ ? "Silent " : "") + subject_,
                                                     attachments2.ToArray(),
                                                     MailServiceExceptionExtensions.IsMailServiceConfigured()
                                                         ? new string[] {}
                                                         : toAddresses_);
                  else
                      exception_.SendMail(fromAddress_, (silentException_ ? "Silent " : "") + subject_,
                                          attachments2.ToArray(),
                                          toAddresses_);

              }
              catch (Exception ex)
              {
                  _logger.Critical("Unhandled exception - reentry", ex);
              }
              finally
              {
                  foreach (var attachment in attachments2)
                  {
                      attachment.Dispose();
                  }

                  foreach (var stream in streams)
                  {
                      stream.Dispose();
                  }  
              }
          }
      } 

    /// <summary>
    /// Register handler so that it can be removed.
    /// </summary>
    /// <param name="callback_">User callback.</param>
    /// <param name="handler_">Internal handler.</param>
    private static void RegisterHandler(UnhandledExceptionCallback callback_, UnhandledExceptionEventHandler handler_)
    {
      if (!CallBackHandlers.ContainsKey(callback_))
      {
        CallBackHandlers[callback_] = new List<UnhandledExceptionEventHandler>();
      }

      CallBackHandlers[callback_].Add(handler_);
    }

    /// <summary>
    /// Removes the default unhandled-exception handler from the current <see cref="AppDomain"/>.
    /// </summary>
    /// <param name="callback_">User callback.</param>
    public static void RemoveDefaultHandler(UnhandledExceptionCallback callback_)
    {
      RemoveDefaultHandler(AppDomain.CurrentDomain, callback_);
    }

    /// <summary>
    /// Removes the default unhandled-exception handler from the specified <see cref="AppDomain"/>.
    /// </summary>
    /// <param name="appDomain_">The <see cref="AppDomain"/> object.</param>
    /// <param name="action_">The action to execute before the unhandled-exception window pops up.</param>
    public static void RemoveDefaultHandler(AppDomain appDomain_, UnhandledExceptionCallback action_)
    {
      lock (Lock)
      {
        if (CallBackHandlers.ContainsKey(action_))
        {
          List<UnhandledExceptionEventHandler> callbacks = CallBackHandlers[action_];

          if (callbacks != null)
          {
            foreach (UnhandledExceptionEventHandler handler in callbacks)
            {
              appDomain_.UnhandledException -= handler;
            }
            CallBackHandlers[action_].Clear();
            CallBackHandlers.Remove(action_);
          }
        }
      }
    }
  }
}
