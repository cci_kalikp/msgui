﻿using System.Collections.Generic;
using System.Windows.Input;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager
{
    public static class PaneContainerCommands
    {
        static readonly List<RoutedCommand> commands = new List<RoutedCommand>(); 
        static PaneContainerCommands()
        { 
            commands.Add(HideHeader = new RoutedUICommand("Hide header", "HideHeader", typeof(PaneContainer))); 
            commands.Add(ShowHeader = new RoutedUICommand("Show header", "ShowHeader", typeof(PaneContainer))); 
            commands.Add(Minimize = new RoutedUICommand("Minimize", "Minimize", typeof(PaneContainer))); 
            commands.Add(Restore = new RoutedUICommand("Restore", "Restore", typeof(PaneContainer))); 
            commands.Add(Lock = new RoutedUICommand("Lock", "Lock", typeof(PaneContainer))); 
            commands.Add(Unlock = new RoutedUICommand("Unlock", "Unlock", typeof(PaneContainer)));
            commands.Add(Close = new RoutedUICommand("Close", "Close", typeof(PaneContainer)));
            commands.Add(Maximize = new RoutedUICommand("Maximize", "Maximize", typeof(PaneContainer)));
            commands.Add(TearOff = new RoutedUICommand("Tear Off", "TearOff", typeof(PaneContainer)));
            commands.Add(Pin = new RoutedUICommand("Stay on top", "Pin", typeof(PaneContainer)));
            commands.Add(Unpin = new RoutedUICommand("Do not stay on top", "Unpin", typeof(PaneContainer)));
            commands.Add(ChangeToFloatingOnly = new RoutedUICommand("Floating", "ChangeToFloatingOnly", typeof(PaneContainer)));
            commands.Add(ChangeToDockable = new RoutedUICommand("Dockable", "ChangeToDockable", typeof(PaneContainer)));
        }

        public static readonly RoutedCommand HideHeader;
        public static readonly RoutedCommand ShowHeader;
        public static readonly RoutedCommand Minimize;
        public static readonly RoutedCommand Maximize;
        public static readonly RoutedCommand Restore;
        public static readonly RoutedCommand Lock;
        public static readonly RoutedCommand Unlock;
        public static readonly RoutedCommand Close;
        public static readonly RoutedCommand TearOff;
        public static readonly RoutedCommand Pin;
        public static readonly RoutedCommand Unpin;
        public static readonly RoutedCommand ChangeToFloatingOnly;
        public static readonly RoutedCommand ChangeToDockable;

        public static IList<RoutedCommand> GetAllCommands()
        {
            return commands;
        }
    }
}
