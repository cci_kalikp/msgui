﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager
{
    public static class DockManagerBrushKeys
    {
        public static readonly ComponentResourceKey CaptionBarUnselectedBrushKey =new MSGuiResourceKey("CaptionBarUnselectedBrush");
        public static readonly ComponentResourceKey CaptionBarActiveBrushKey = new MSGuiResourceKey("CaptionBarActiveBrush");
        public static readonly ComponentResourceKey CaptionBarGroupedBrushKey = new MSGuiResourceKey("CaptionBarGroupedBrush");

        public static readonly ComponentResourceKey UnselectedTitleBrushKey = new MSGuiResourceKey("UnselectedTitleBrush"); //Themes:MSGuiColors.InactiveTitlesBrushKey
        public static readonly ComponentResourceKey ActiveTitleBrushKey = new MSGuiResourceKey("ActiveTitleBrush"); //MSGuiColors.TitlesBrushKey
        public static readonly ComponentResourceKey GroupedTitleBrushKey = new MSGuiResourceKey("GroupedTitleBrush");


        public static readonly ComponentResourceKey ScreenDockingGridBorderBrushKey = new MSGuiResourceKey("ScreenDockingGridBorderBrush");
        public static readonly ComponentResourceKey ScreenDockingGridBackgroundBrushKey = new MSGuiResourceKey("ScreenDockingGridBackgroundBrush");
        public static readonly ComponentResourceKey DockignGridBorderBrushKey = new MSGuiResourceKey("DockingGridBorderBrush");
        public static readonly ComponentResourceKey DockingGridBackgroundBrushKey = new MSGuiResourceKey("DockingGridBackgroundBrush");

        public static readonly ComponentResourceKey TabItemNormalBackgroundBrushKey = new MSGuiResourceKey("TabItemNormalBackgroundBrush");
        public static readonly ComponentResourceKey TabItemSelectedBackgroundBrushKey = new MSGuiResourceKey("TabItemSelectedBackgroundBrush");
        public static readonly ComponentResourceKey TabItemDisabledBackgroundBrushKey = new MSGuiResourceKey("TabItemDisabledBackgroundBrush");
        public static readonly ComponentResourceKey TabItemHotBackgroundBrushKey = new MSGuiResourceKey("TabItemHotBackgroundBrush");

        public static readonly ComponentResourceKey TabItemNormalBorderBrushKey = new MSGuiResourceKey("TabItemNormalBorderBrush"); 
        public static readonly ComponentResourceKey TabItemDisabledBorderBrushKey = new MSGuiResourceKey("TabItemDisabledBorderBrush");
        public static readonly ComponentResourceKey TabItemHotBorderBrushKey = new MSGuiResourceKey("TabItemHotBorderBrush");


        public static readonly ComponentResourceKey TabItemNormalForegroundBrushKey = new MSGuiResourceKey("TabItemNormalForegroundBrush"); //MSGuiColors.TextFillBrushKey
        public static readonly ComponentResourceKey TabItemSelectedForegroundBrushKey = new MSGuiResourceKey("TabItemSelectedForegroundBrush"); //
        public static readonly ComponentResourceKey TabItemDisabledForegroundBrushKey = new MSGuiResourceKey("TabItemDisabledForegroundBrush"); //SystemColors.GrayTextBrushKey
        public static readonly ComponentResourceKey TabItemHotForegroundBrushKey = new MSGuiResourceKey("TabItemHotForegroundBrush");
    }
}
