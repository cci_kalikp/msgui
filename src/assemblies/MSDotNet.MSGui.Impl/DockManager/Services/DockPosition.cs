﻿namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Services
{
    public enum DockPosition
    {
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight,
        Left,
        Right,
        Top,
        Bottom,
        None
    }
}
