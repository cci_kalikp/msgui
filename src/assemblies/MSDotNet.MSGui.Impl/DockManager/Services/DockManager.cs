﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Services
{
    public static class DockManager
    {
        public static event EventHandler<ActivePaneChangedEventArgs> ActivePaneChanged;
        public static event EventHandler<ActiveContainerChangedEventArgs> ActiveContainerChanged; 
        public static event EventHandler<FloatingWindowEventArgs> FloatingWindowLoaded;
        public static event EventHandler<FloatingWindowEventArgs> FloatingWindowCreated; 
        public static event EventHandler<ContainerDockingEventArgs> ContainerDocking;
        //todo: handle tearoff, allowdock toggling with this
        public static event EventHandler<ContainerTearingOffEventArgs> ContainerTearingOff;

        private static PaneContainer activeContainer;
        public static PaneContainer ActiveContainer
        {
            get
            {
                return activeContainer;
            }
            internal set
            {
                var oldActiveContainer = activeContainer;
                if (activeContainer != null)
                {
                    DependencyPropertyDescriptor.FromProperty(PaneContainer.ActivePaneProperty, typeof(PaneContainer)).RemoveValueChanged(
             activeContainer, ActivePaneChangedHandler);  
                    FloatingWindow fw = FloatingWindow.GetFloatingWindow(activeContainer);
                    if (!GroupManager.Contains(fw))
                    {
                        activeContainer.ChangeCaptionBarState(CaptionBarState.Unselected);
                    }

                    if (activeContainer.AllowDockResolved)
                        activeContainer.DockButtonState = WindowButtonState.Normal;
                }
                activeContainer = value;
                if (activeContainer != null)
                {
                    activeContainer.DockButtonState = WindowButtonState.None;  
                    FloatingWindow fw2 = FloatingWindow.GetFloatingWindow(activeContainer);
                    if (!GroupManager.Contains(fw2))
                    {
                        activeContainer.ChangeCaptionBarState(CaptionBarState.Active);
                    }
                    DependencyPropertyDescriptor.FromProperty(PaneContainer.ActivePaneProperty, typeof(PaneContainer)).AddValueChanged(
                                activeContainer, ActivePaneChangedHandler);
                    activeContainer.LastActivatedTime = DateTime.UtcNow; 
                }
                if (activeContainer != null)
                {
                    ActivePane = activeContainer.ActivePane;
                    var parentPane = activeContainer.ParentPane;
                    if (parentPane != null)
                    {
                        ContentPane.SetActiveChildContainer(parentPane, activeContainer);
                    } 
                }
                else
                {
                    ActivePane = null;
                }
                if (oldActiveContainer != activeContainer)
                {
                    var copy = ActiveContainerChanged;
                    if (copy != null)
                    {
                        copy(null, new ActiveContainerChangedEventArgs(oldActiveContainer, activeContainer));
                    }
                }
            }
        }

        private static void ActivePaneChangedHandler(object sender_, EventArgs eventArgs_)
        {
            PaneContainer container = (PaneContainer) sender_;
            ActivePane = container.ActivePane;
        }


        internal static bool IsContainerDockingValid(PaneContainer sourceContainer_, PaneContainer targetContainer_, DockLocation dockLocation_)
        {
            var copy = ContainerDocking;
            if (copy != null)
            {
                var arg = new ContainerDockingEventArgs(sourceContainer_, targetContainer_, dockLocation_);
                copy(null, arg);
                return !arg.Cancel;
            }
            return true;
        }

        internal static bool CanTearOffContainer(PaneContainer container_)
        {
            var copy = ContainerTearingOff;
            if (copy != null)
            {
                var arg = new ContainerTearingOffEventArgs(container_);
                copy(null, arg);
                return !arg.Cancel;
            }
            return true;
        }
        private static ContentPane activePane;
        public static ContentPane ActivePane
        {
            get { return activePane; }
            set
            {
                if (activePane != value)
                {
                    var oldActivePane = activePane;
                    activePane = value;
                    if (activePane != null)
                    {
                        activePane.LastActivatedTime = DateTime.UtcNow; 
                    }
                    var copy = ActivePaneChanged;
                    if (copy != null)
                    {
                        copy(null, new ActivePaneChangedEventArgs(oldActivePane, activePane));
                    }
                }
            }
        }
        private static List<FloatingWindow> allWindows = new List<FloatingWindow>();

        internal static void OnFloatingWindowCreated(FloatingWindow window_)
        {
            var copy = FloatingWindowCreated;
            if (copy != null)
            {
                copy(null, new FloatingWindowEventArgs(window_));
            }
        }

        internal static void AddWindow(FloatingWindow window_)
        {
            DockService.AttachWindow(window_);
            allWindows.Add(window_);
            FloatingWindowUtil.RegisterFloatingWindow(window_);
            var copy = FloatingWindowLoaded;
            if (copy != null)
            {
                copy(null, new FloatingWindowEventArgs(window_));
            }

            var singlePane = window_.PaneContainer.SinglePane;
            if (singlePane != null)
            {
                singlePane.PaneLocation = PaneLocation.Single;
            }
            else
            {
                foreach (var pane in window_.PaneContainer.GetAllPanes())
                {
                    pane.UpdatePaneLocation();
                } 
            }

        }

        internal static void RemoveWindow(FloatingWindow window_)
        {
            allWindows.Remove(window_);
            DockService.DetachWindow(window_);
            var visibleWindows = allWindows.FindAll(w_ => w_.IsVisible && w_.WindowState != WindowState.Minimized);
            if (visibleWindows.Count == 1)
            {
                if (window_.Contains(ActiveContainer))
                {
                    ActiveContainer = visibleWindows[0].PaneContainer;
                }
            } 
        }

        
        internal static List<FloatingWindow> GetAllFloatingWindows()
        {
            var windows = new List<FloatingWindow>(allWindows);
            windows.Sort((w1_, w2_) => w1_.LastActivatedTime.CompareTo(w2_.LastActivatedTime));
            return windows;
        }
         
        public static List<PaneContainer> GetAllContainers()
        {
            var containers = new List<PaneContainer>();
            foreach (var floatingWindow in allWindows)
            {
                PaneContainer root = floatingWindow.PaneContainer;
                if (root != null)
                {
                    CollectPaneContainers(root, containers);
                }
            }
            return containers;
        }

        public static IEnumerable<PaneContainer> EnumerateAllContainers()
        { 
            foreach (var floatingWindow in allWindows)
            {
                PaneContainer root = floatingWindow.PaneContainer;
                var enumerable = EnumeratePaneContainers(root).GetEnumerator();
                while (enumerable.MoveNext())
                {
                    yield return enumerable.Current;
                }
            } 
        }


        public static List<PaneContainer> GetAllContainers(this PaneContainer rootContainer_)
        {
            var containers = new List<PaneContainer>();
            CollectPaneContainers(rootContainer_, containers); 
            return containers;
        }
 
        public static void ExecuteActions(PaneContainer rootContainer_, Action<PaneContainer> action_)
        { 
            foreach (var container in GetAllContainers(rootContainer_))
            {
                action_(container);
            }
        }
        public static void ExecuteActions(Action<PaneContainer> action_)
        {
            foreach (var container in GetAllContainers())
            {
                action_(container);
            }
        }
        private static void CollectPaneContainers(PaneContainer parent_, List<PaneContainer> containers_)
        {
            containers_.Add(parent_);
            foreach (var directChildrenContainer in parent_.GetChildrenContainers())
            {
                CollectPaneContainers(directChildrenContainer, containers_);
            }
        }
        private static IEnumerable<PaneContainer> EnumeratePaneContainers(PaneContainer parent_)
        {
            yield return parent_;
            foreach (var directChildrenContainer in parent_.GetChildrenContainers())
            {
                var enumerable = EnumeratePaneContainers(directChildrenContainer).GetEnumerator();
                while (enumerable.MoveNext())
                {
                    yield return enumerable.Current;
                }
            }
        }

        public static ContentPane FindPane(Func<ContentPane, bool> match_)
        {
            foreach (var paneContainer in EnumerateAllContainers())
            {
                var found = paneContainer.GetChildrenPanes(true).FirstOrDefault(match_);
                if (found != null) return found;
            }
            return null;
        }

        public static ContentPane FindPane(this PaneContainer parentContainer_, Func<ContentPane, bool> match_)
        {
            var enumerable = EnumeratePaneContainers(parentContainer_).GetEnumerator();
            while (enumerable.MoveNext())
            {
                var found = enumerable.Current.GetChildrenPanes(true).FirstOrDefault(match_);
                if (found != null) return found;
            }
            return null;
        }

        public static PaneContainer FindContainer(Func<PaneContainer, bool> match_)
        {
            foreach (var paneContainer in EnumerateAllContainers())
            { 
                if (match_(paneContainer)) return paneContainer;
            }
            return null;
        }


        public static PaneContainer FindContainer(this PaneContainer parentContainer_, Func<PaneContainer, bool> match_)
        {
            var enumerable = EnumeratePaneContainers(parentContainer_).GetEnumerator();
            while (enumerable.MoveNext())
            {
                if (match_(enumerable.Current)) return enumerable.Current;
                 
            }
            return null;
        }
        public static List<ContentPane> GetAllPanes(bool leafOnly_ = false)
        {
            var containers = GetAllContainers();
            var panes = new List<ContentPane>();
            foreach (var paneContainer in containers)
            {
                panes.AddRange(paneContainer.GetChildrenPanes(leafOnly_));
            }
            return panes;
        }

        public static List<ContentPane> GetAllPanes(this PaneContainer rootContainer_, bool leafOnly_ = false)
        {
            var containers = GetAllContainers(rootContainer_);
            var panes = new List<ContentPane>();
            foreach (var paneContainer in containers)
            {
                panes.AddRange(paneContainer.GetChildrenPanes(leafOnly_));
            }
            return panes;
        }

        public static List<ContentPane> GetAllPanes(this ContentPane rootPane_, bool leafOnly_ = false)
        {
            var panes = new List<ContentPane>();
            foreach (var contentPane in rootPane_.GetChildrenPanes())
            {
                if (!leafOnly_ || contentPane.IsLeaf)
                {
                    panes.Add(contentPane);
                }
                if (!contentPane.IsLeaf)
                {
                    panes.AddRange(GetAllPanes(contentPane, leafOnly_));
                }
            }
            return panes;
        }
    }

    public class ActivePaneChangedEventArgs:EventArgs
    {
        public ActivePaneChangedEventArgs(ContentPane oldPane_, ContentPane newPane_)
        {
            this.OldPane = oldPane_;
            this.NewPane = newPane_;
        }

        public ContentPane OldPane { get; private set; }
        public ContentPane NewPane { get; private set; }
    }

    public class FloatingWindowEventArgs:EventArgs
    {
        public FloatingWindowEventArgs(FloatingWindow window_)
        {
            Window = window_;
        }

        public FloatingWindow Window { get; private set; }
    }
     
    public class ActiveContainerChangedEventArgs:EventArgs
    {
        public ActiveContainerChangedEventArgs(PaneContainer oldContainer_, PaneContainer newContainer_)
        {
            OldContainer = oldContainer_;
            NewContainer = newContainer_;
        }

        public PaneContainer OldContainer { get; private set; }
        public PaneContainer NewContainer { get; private set; }
    }

    public class ContainerTearingOffEventArgs:CancelEventArgs
    {
        public ContainerTearingOffEventArgs(PaneContainer container_)
        {
            Container = container_;
        }

        public PaneContainer Container { get; private set; }
    }
}
