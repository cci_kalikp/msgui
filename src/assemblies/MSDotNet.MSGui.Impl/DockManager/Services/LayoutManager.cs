﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Workspaces;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Services
{
    public static class LayoutManager
    {
        public const string LayoutRootName = "harmoniaDockManager";
        #region Save Layout
        public static string SaveLayout()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                SaveLayout(stream);
                stream.Position = 0L;
                StreamReader reader = new StreamReader(stream);
                return reader.ReadToEnd();
            } 
        }


        public static void SaveLayout(Stream stream_)
        {
            SaveLayoutCore(stream_, DockManager.GetAllFloatingWindows()); 
        }


        internal static XElement SaveSubLayout(IEnumerable<IWindowViewContainerHostRoot> roots_, IEnumerable<IWindowViewContainer> containers_ = null, bool skipLocationInfo_ = false)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                List<ContentPane> panesToSave = new List<ContentPane>();
                List<ContentPane> panesToSkip = new List<ContentPane>();
                if (containers_ != null)
                {
                    foreach (WindowViewContainer viewContainer in containers_)
                    {
                        ContentPane pane = viewContainer.Visual as ContentPane; 
                        if (pane != null && !panesToSave.Contains(pane))
                        {
                            panesToSave.Add(pane);
                        }
                    }
                }
                var floatingWindows = roots_.Cast<FloatingWindow>();
                if (panesToSave.Count > 0)
                {
                    foreach (var floatingWindow in floatingWindows)
                    {
                        panesToSkip.AddRange(floatingWindow.PaneContainer.GetAllPanes(true).FindAll(p_ =>
                            {
                                bool skip = p_.SaveInLayout && !panesToSave.Contains(p_);
                                if (skip)
                                {
                                    p_.SaveInLayout = false;
                                }
                                return skip;
                            }));
                    }  
                }
                  
               
                try
                {
                    SaveLayoutCore(stream, floatingWindows);
                    stream.Seek(0, SeekOrigin.Begin);

                    using (var xr = new XmlTextReader(stream))
                    {
                        xr.MoveToContent();
                        return XNode.ReadFrom(xr) as XElement;
                    }

                }
                finally
                {
                    foreach (var pane in panesToSkip)
                    {
                        pane.SaveInLayout = true;
                    }
                }
            } 
        }

        private static void SaveLayoutCore(Stream stream_, IEnumerable<FloatingWindow> windows_)
        {
            XmlTextWriter writer = new XmlTextWriter(stream_, Encoding.UTF8)
            {
                Formatting = Formatting.Indented
            };
            writer.WriteStartDocument();
            writer.WriteStartElement(LayoutRootName);
            writer.WriteStartElement("rootContainers");
            List<ContentPane> leafPanes = new List<ContentPane>();
            foreach (var floatingWindow in windows_)
            {
                var paneContainer = floatingWindow.PaneContainer;
                if (paneContainer == null) continue;
                if (!ShouldSerialize(paneContainer)) continue;
                writer.WriteStartElement("rootContainer");
                if (string.IsNullOrEmpty(floatingWindow.Name))
                {
                    floatingWindow.Name = "window" + Guid.NewGuid().ToString("N");
                }
                writer.WriteAttributeString("name", floatingWindow.Name);
                XmlHelper.WriteAttribute(writer, "lastActivatedTime", floatingWindow.LastActivatedTime);
                writer.WriteAttributeString("location", "Floating");
                XmlHelper.WriteAttributeWithConverter(writer, "floatingLocation", floatingWindow.RestoreBounds.Location);
                XmlHelper.WriteAttributeWithConverter(writer, "floatingSize", floatingWindow.RestoreBounds.Size);
                SavePaneContainer(writer, paneContainer, leafPanes);

                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            writer.WriteStartElement("contentPanes");
            foreach (var contentPane in leafPanes)
            {
                writer.WriteStartElement("contentPane");
                writer.WriteAttributeString("name", contentPane.Name);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Flush();
        }
        private static void SavePaneContainer(XmlTextWriter writer_, PaneContainer container_, List<ContentPane> leafPanes_)
        { 
            if (string.IsNullOrEmpty(container_.Name) || container_.Name == PaneContainer.DefaultName)
            {
                container_.Name = "paneContainer" + Guid.NewGuid().ToString("N");
            }
            writer_.WriteStartElement("paneContainer");
            writer_.WriteAttributeString("name", container_.Name); 
            writer_.WriteAttributeString("state", container_.WindowState.ToString());
            writer_.WriteAttributeString("isLocked", XmlConvert.ToString(container_.IsLocked)); 
            //writer_.WriteAttributeString("lockButtonState", container_.LockButtonState.ToString());
            writer_.WriteAttributeString("showHeader", XmlConvert.ToString(container_.IsHeaderVisible));
            //writer_.WriteAttributeString("hideButtonState", container_.HideButtonState.ToString());
            foreach (ContentPaneWrapper item in container_.Host.Items)
            {
                SaveContentPane(writer_, item.Pane, leafPanes_);  
            }
            writer_.WriteEndElement();
        }

        private static void SaveContentPane(XmlTextWriter writer_, ContentPane pane_, List<ContentPane> leafPanes_)
        {
            if (!ShouldSerialize(pane_)) return;

            
            var dockingGrid = pane_.DockingGrid;
            if (dockingGrid != null && dockingGrid.Root != null)
            {
                var normlaizedSplitter = GetNormalizeSplitPanesForSave(dockingGrid.Root);
                if (normlaizedSplitter == null) return;
                if (normlaizedSplitter.Self != null)
                {
                    foreach (ContentPaneWrapper item in normlaizedSplitter.Self.Host.Items)
                    {
                        SaveContentPane(writer_, item.Pane, leafPanes_);
                    }
                }
                else
                {
                    SaveContentPaneCore(writer_, pane_, () => SaveSplitter(writer_, normlaizedSplitter, leafPanes_));
                }
            }
            else
            {
                SaveContentPaneCore(writer_, pane_, () => leafPanes_.Add(pane_));
                
            }

        }

        private static void SaveContentPaneCore(XmlTextWriter writer_, ContentPane pane_, Action action_)
        {
            writer_.WriteStartElement("contentPane");
            if (string.IsNullOrEmpty(pane_.Name))
            {
                pane_.Name = ContentPaneIdConverter.GeneratePaneId();
            }
            writer_.WriteAttributeString("name", pane_.Name);
            XmlHelper.WriteAttribute(writer_, "lastActivatedTime", pane_.LastActivatedTime);
            XmlHelper.WriteAttributeWithConverter(writer_, "lastFloatingSize", pane_.LastFloatingSize);

            writer_.WriteAttributeString("allowDock", XmlConvert.ToString(pane_.AllowDock));
            action_();
            writer_.WriteEndElement();
        }
        private static SplitPanes GetNormalizeSplitPanesForSave(SplitPanes splitPanes_)
        {
            if (splitPanes_.Self != null)
            {
                if (!ShouldSerialize(splitPanes_.Self))
                {
                    return null;
                }
                return splitPanes_;
            }
            var first = GetNormalizeSplitPanesForSave(splitPanes_.First);
            var second = GetNormalizeSplitPanesForSave(splitPanes_.Second);
            if (first == null && second == null) return null;
            if (first == null)
            {
                return second;
            }
            if (second == null)
            {
                return first;
            }
            return new SplitPanes(first, second, splitPanes_.Orientation);
        }

        private static void SaveSplitter(XmlTextWriter writer_, SplitPanes splitPanes_, List<ContentPane> leafPanes_)
        {
            if (splitPanes_.Self != null)
            {
                SavePaneContainer(writer_, splitPanes_.Self, leafPanes_);
            }
            else
            { 

                writer_.WriteStartElement("splitPane");
                writer_.WriteAttributeString("orientation", splitPanes_.Orientation.ToString());
                XmlHelper.WriteAttributeWithConverter(writer_, "firstLength", splitPanes_.FirstLength); 
                XmlHelper.WriteAttributeWithConverter(writer_, "secondLength", splitPanes_.SecondLength); 
                if (splitPanes_.First != null)
                {
                     SaveSplitter(writer_, splitPanes_.First, leafPanes_); 
                }
                if (splitPanes_.Second != null)
                {
                    SaveSplitter(writer_, splitPanes_.Second, leafPanes_); 
                }
                writer_.WriteEndElement();
            }

        }


        private static bool ShouldSerialize(PaneContainer container_)
        {
            foreach (var pane in container_.GetChildrenPanes())
            {
                if (ShouldSerialize(pane)) return true;
            }
            return false;
        }

        private static bool ShouldSerialize(ContentPane pane_)
        {
            if (!pane_.IsContainerPane) return pane_.SaveInLayout;
            foreach (var childrenPane in pane_.GetChildrenPanes())
            {
                if (ShouldSerialize(childrenPane))
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region LoadLayout
        public static void LoadLayout(string content_)
        {
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(content_)))
            {
                LoadLayout(stream); 
            } 
        }
        public static void LoadLayout(Stream stream_)
        {

            using (BufferedStream stream2 = new BufferedStream(stream_))
            {
                XmlDocument document = new XmlDocument();
                document.Load(stream2);
                LoadLayoutCore(document, (s_, pane_) => pane_ ?? new ContentPane() {Name = s_}); 
            }
        }

        internal static IEnumerable<IWindowViewContainerHostRoot> ReadSubLayout(string subLayoutName_, XElement subLayout_, RehydratePaneDelegate callback_,
                bool reuseExisting_ = false,  SubLayoutLocation locationOverride_ = SubLayoutLocation.SavedLocation)
        {
            var layoutElement = subLayout_.Element(LayoutRootName);
            if (layoutElement == null) return new IWindowViewContainerHostRoot[0];
            var document = new XmlDocument() { InnerXml = layoutElement.ToString() };
            var windows = LoadLayoutCore(document, (s_, pane_) =>
                {
                    var newName = pane_ != null && !reuseExisting_ ? ContentPaneIdConverter.GeneratePaneId() : s_;
                    callback_(s_, newName);
                    return DockManager.FindPane(p_ => p_.Name == newName); 
                });
            foreach (var floatingWindow in windows)
            {
                floatingWindow.SubLayoutTitle = subLayoutName_;
            }
            return windows;
        }

        private static IList<FloatingWindow> LoadLayoutCore(XmlDocument document_, Func<string, ContentPane, ContentPane> paneRehydrator_)
        {
            var windowsLoaded = new List<FloatingWindow>();

            XmlNode node = document_.SelectSingleNode(LayoutRootName);
            if (node == null)
            {
                //todo: throw exception
                return windowsLoaded;
            }
            XmlNode node2 = node.SelectSingleNode("rootContainers");
            if (node2 == null)
            {
                return windowsLoaded;
            }
            var rootContainerNodes = node2.SelectNodes("rootContainer");
            if (rootContainerNodes == null) return windowsLoaded;
            XmlNode node3 = node.SelectSingleNode("contentPanes");
            if (node3 == null) return windowsLoaded;
            var contentPaneNodes = node3.SelectNodes("contentPane");
            if (contentPaneNodes == null) return windowsLoaded;


            Dictionary<string, ContentPane> panes = new Dictionary<string, ContentPane>();
            foreach (ContentPane pane in DockManager.GetAllPanes())
            {
                if (!string.IsNullOrEmpty(pane.Name))
                {
                    panes[pane.Name] = pane;
                }
            }

            Dictionary<string, ContentPane> panesLoaded = new Dictionary<string, ContentPane>();
            foreach (XmlNode contentPaneNode in contentPaneNodes)
            {
                string name = XmlHelper.ReadAttribute(contentPaneNode, "name");
                ContentPane pane;
                panes.TryGetValue(name, out pane);
                pane = paneRehydrator_(name, pane);
                if (pane != null)
                {
                    pane.Detach();
                    panesLoaded[name] = pane;              
                }
            }

            List<FloatingWindow> windows = DockManager.GetAllFloatingWindows();
            foreach (var floatingWindow in windows)
            {
                if (floatingWindow.PaneContainer.GetAllPanes().Count == 0)
                {
                    floatingWindow.PaneContainer.Close(true);
                }
            }

            Dictionary<XmlNode, PaneContainer> rootContainers = new Dictionary<XmlNode, PaneContainer>();
            foreach (XmlNode rootContainerNode in rootContainerNodes)
            {
                string location = XmlHelper.ReadAttribute(rootContainerNode, "location");
                if (location != "Floating") continue;
                var paneContainerNode = rootContainerNode.SelectSingleNode("paneContainer");
                if (paneContainerNode == null)
                {
                    continue;
                }
                PaneContainer rootPaneContainer = LoadPaneContainer(paneContainerNode, panesLoaded);
                rootContainers.Add(rootContainerNode, rootPaneContainer);
            }

            SortedSet<ContentPane> panesSorted = new SortedSet<ContentPane>(ContentPane.Comparer);
            foreach (var contentPane in panesLoaded.Values)
            {
                panesSorted.Add(contentPane);
            }

            SortedSet<FloatingWindow> windowsSorted = new SortedSet<FloatingWindow>(FloatingWindow.Comparer);
            foreach (var rootContainer in rootContainers)
            {
                var rootContainerNode = rootContainer.Key;
                var rootPaneContainer = rootContainer.Value;
                DateTime lastActivatedTime = XmlHelper.ReadAttribute(rootContainerNode, "lastActivatedTime", DateTime.MinValue);
                Size floatingSize = XmlHelper.ReadAttributeWithConverter(rootContainerNode, "floatingSize", Size.Empty);
                Point floatingLocation = XmlHelper.ReadAttributeWithConverter(rootContainerNode, "floatingLocation", new Point());
                if (floatingSize.Width > 0 && floatingSize.Height > 0)
                {
                    rootPaneContainer.PaneHeight = floatingSize.Height;
                    rootPaneContainer.PaneWidth = floatingSize.Width;
                }
                FloatingWindow window = rootPaneContainer.CreateHostingWindow();
                window.Name = XmlHelper.ReadAttribute(rootContainerNode, "name");
                window.LastActivatedTime = lastActivatedTime;
                window.Left = floatingLocation.X;
                window.Top = floatingLocation.Y;
                windowsSorted.Add(window);
            }

            foreach (var floatingWindow in windowsSorted)
            {
                floatingWindow.Show();
                floatingWindow.WindowState = floatingWindow.PaneContainer.WindowState;
            }

            foreach (var contentPane in panesSorted)
            {
                if (contentPane.LastActivatedTime == new DateTime())
                {
                    continue;
                }
                contentPane.ActivateInternal(false);
            }
            return windowsSorted.ToArray();
        }

        private static PaneContainer LoadPaneContainer(XmlNode paneContainerNode_, Dictionary<string, ContentPane> panesLoaded_)
        {
            PaneContainer paneContainer = new PaneContainer();
            paneContainer.Name = XmlHelper.ReadAttribute(paneContainerNode_, "name");
            paneContainer.WindowState = XmlHelper.ReadEnumAttribute(paneContainerNode_, "state", WindowState.Normal);
            //paneContainer.LockButtonState = XmlHelper.ReadEnumAttribute(paneContainerNode_, "lockButtonState", WindowButtonState.Normal);
            paneContainer.IsLocked = XmlHelper.ReadAttribute(paneContainerNode_, "isLocked", false);
            //paneContainer.HideButtonState = XmlHelper.ReadEnumAttribute(paneContainerNode_, "hideButtonState", WindowButtonState.Normal);
            paneContainer.IsHeaderVisible = XmlHelper.ReadAttribute(paneContainerNode_, "showHeader", true);
            var contentPaneNodes2 = paneContainerNode_.SelectNodes("contentPane");
            if (contentPaneNodes2 != null)
            {
                foreach (XmlNode contentPaneNode in contentPaneNodes2)
                {
                    string name = XmlHelper.ReadAttribute(contentPaneNode, "name");
                    ContentPane pane;
                    if (!panesLoaded_.TryGetValue(name, out pane))
                    {
                        pane = new ContentPane() { Name = name };  
                    }
                    pane.LastActivatedTime = XmlHelper.ReadAttribute(contentPaneNode, "lastActivatedTime", DateTime.MinValue);
                    pane.LastFloatingSize = XmlHelper.ReadAttributeWithConverter(contentPaneNode, "lastFloatingSize", Size.Empty);
                    pane.AllowDock = XmlHelper.ReadAttribute(contentPaneNode, "allowDock", true); 

                    paneContainer.AddPane(pane);
                    pane.LastDockLocation = DockLocation.Center;
                    var dockingGrid = pane.Wrapper.DockingGrid;
                    var splitPaneNode = contentPaneNode.SelectSingleNode("splitPane"); 
                    if (splitPaneNode != null)
                    {
                        dockingGrid.Load(LoadSplitter(splitPaneNode, panesLoaded_)); 
                    }
                    var rootPane = dockingGrid.RootPane;
                    if (!panesLoaded_.ContainsKey(rootPane.Name))
                    {
                        panesLoaded_.Add(rootPane.Name, rootPane);
                    }
                }
                if (paneContainer.paneWrapperHost.Items.Count == 1)
                {
                    paneContainer.GetChildrenPanes()[0].LastDockLocation = DockLocation.None;
                }
            }
            return paneContainer;
        }

        private static SplitPanes LoadSplitter(XmlNode splitPaneNode_, Dictionary<string, ContentPane> panesLoaded_)
        {
            List<object> children = new List<object>();
            foreach (var childNode in splitPaneNode_.ChildNodes)
            {
                XmlElement element = childNode as XmlElement;
                if (element == null) continue;
                if (element.Name == "paneContainer")
                {
                    children.Add(LoadPaneContainer(element, panesLoaded_));
                }
                else if (element.Name == "splitPane")
                {
                    children.Add(LoadSplitter(element, panesLoaded_));
                }
            } 
            if (children.Count == 1)
            {
                return new SplitPanes(children[0] as PaneContainer);
            }
             if (children.Count == 2)
            {
                Orientation orientation = XmlHelper.ReadEnumAttribute(splitPaneNode_, "orientation", Orientation.Vertical);
                GridLength firstLength = XmlHelper.ReadAttributeWithConverter(splitPaneNode_, "firstLength", new GridLength(1, GridUnitType.Star));
                GridLength secondLength = XmlHelper.ReadAttributeWithConverter(splitPaneNode_, "secondLength", new GridLength(1, GridUnitType.Star));

                var first = children[0] as SplitPanes;
                if (first == null)
                {
                    first = new SplitPanes(children[0] as PaneContainer);
                }
                var second = children[1] as SplitPanes;
                if (second == null)
                {
                    second = new SplitPanes(children[1] as PaneContainer);
                }
                 return new SplitPanes(first, second, orientation)
                     {
                         FirstLength = firstLength,
                         SecondLength = secondLength,
                     } ;

            }
            return null;
        }
        #endregion 

    }
     
}
