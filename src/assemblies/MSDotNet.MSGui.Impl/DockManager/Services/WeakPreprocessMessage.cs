﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Windows.Interop;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Services
{
    internal class WeakPreprocessMessage : WeakReference, IDisposable
    { 
        private IntPtr _focusHwnd;
         
        internal WeakPreprocessMessage(FloatingWindow owner_)
            : base(owner_)
        {
            ComponentDispatcher.ThreadPreprocessMessage += new ThreadMessageEventHandler(this.OnThreadPreprocessMessage);
        }

        public void Dispose()
        {
            try
            {
                UIPermission permission = new UIPermission(PermissionState.Unrestricted);
                try
                {
                    permission.Assert();
                }
                catch (InvalidOperationException)
                {
                }
                ComponentDispatcher.ThreadPreprocessMessage -= new ThreadMessageEventHandler(this.OnThreadPreprocessMessage);
            }
            catch (SecurityException)
            {
            }
        }

        [SuppressUnmanagedCodeSecurity, DllImport("user32.dll")]
        private static extern IntPtr GetFocus();
        private void OnThreadPreprocessMessage(ref MSG msg_, ref bool handled_)
        {
            IntPtr focus = GetFocus();
            if (focus != this._focusHwnd)
            {
                FloatingWindow weakReferenceTargetSafe = CoreUtilities.GetWeakReferenceTargetSafe(this) as FloatingWindow;
                if (weakReferenceTargetSafe != null)
                {
                    this._focusHwnd = focus;
                    weakReferenceTargetSafe.Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(weakReferenceTargetSafe.OnFocusHwndChanged));
                }
                else
                {
                    this.Dispose();
                }
            }
        }
    }


}
