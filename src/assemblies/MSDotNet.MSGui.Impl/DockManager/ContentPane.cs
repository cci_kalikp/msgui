﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.ComponentModel;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Services;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ViewModels;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager
{
    public class ContentPane : ContentControl, IDisposable, IWindowViewContainerHost
    {
        public override string ToString()
        {
            string children = string.Empty;
            if (this.Wrapper != null && this.Wrapper.DockingGrid != null)
            {
                foreach (var normalContainer in Wrapper.DockingGrid.NormalContainers)
                {
                    children += normalContainer + ",";
                }
                foreach (var minimized in Wrapper.DockingGrid.MinimizedPaneContainers.Containers)
                {
                    children += minimized + ",";
                }
            }
            if (!string.IsNullOrEmpty(children))
            {
                return this.Caption + "(" + children.Substring(0, children.Length - 1) + ")";
            }
            return this.Caption;
        }

        internal LayoutGrid Layouter { get; private set; }

        public bool IsLeaf { get { return Layouter == null; } }

        static ContentPane()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ContentPane), new FrameworkPropertyMetadata(typeof(ContentPane)));
            HorizontalAlignmentProperty.OverrideMetadata(typeof(ContentPane), new FrameworkPropertyMetadata(HorizontalAlignment.Stretch));
            VerticalContentAlignmentProperty.OverrideMetadata(typeof(ContentPane), new FrameworkPropertyMetadata(VerticalAlignment.Stretch)); 
        }

        private readonly WindowViewContainerHostHelper helper;

        private readonly RoutedEventHandler titleNeedChangingHandler = null;

        public ContentPane()
        {
            this.SaveInLayout = true;
            foreach (var command in ContentPaneCommands.GetAllCommands())
            {
                this.CommandBindings.Add(new CommandBinding(command, ExecuteCommand, CanExecuteCommand));
            } 
            this.SizeToContent = SizingMethod.SizeToContent;
            helper = new WindowViewContainerHostHelper(this);
            this.PaneHeaderHeight = PaneContainer.PaneHeaderHeightDefault;
            titleNeedChangingHandler = (sender_, args_) =>
                {
                    var pane = this.PaneResolved;
                    if (pane != null)
                    {
                        pane.ExecuteCommand(ContentPaneCommands.Rename);
                        args_.Handled = true;
                    } 

            };
            Attached.AddTitleNeedChangingHandler(this, this.titleNeedChangingHandler);
            HeaderIconVisibility = ShellModeExtension.GetVisibilityForElement(HideLockUIGranularity.HideHeaderIcon);
            LastDockLocation = DockLocation.None;
            this.OptionsMenuOpening += new EventHandler<PaneOptionsMenuOpeningEventArgs>(ContentPane_OptionsMenuOpening); 
        }


        private readonly MultipaneHeaderValueConverter multipaneHeaderValueConverter;
         

        internal ContentPane(DockingGrid dockingGrid_, DummyWindowViewModel vm_)
            : this()
        {
            Layouter = new LayoutGrid(dockingGrid_);
            this.DockingGrid = dockingGrid_;
            this.Content = Layouter;
            this.DataContext = vm_;
            this.IsContainerPane = true;
            if (vm_.ToolWindowTitleGenerator != null)
            {
                multipaneHeaderValueConverter = new MultipaneHeaderValueConverter(vm_.ToolWindowTitleGenerator);
            }
            this.TitleTextTrimming = vm_.TitleTextTrimming;
            this.Loaded += new RoutedEventHandler(ContentPane_Loaded); 
        }

        void ContentPane_Loaded(object sender_, RoutedEventArgs e_)
        { 
           this.Loaded -= new RoutedEventHandler(ContentPane_Loaded); 
           if (multipaneHeaderValueConverter != null)
           {
               BindCaption();
               this.ChildContainerChanged += (o_, args_) => BindCaption();
           }
           this.Wrapper.Host.UpdateCustomItems();  
        }
         

        private void BindCaption()
        {
            var multipaneHeaderBinding = new MultiBinding();

            foreach (var contentPane in this.GetAllPanes(true))
            {
                multipaneHeaderBinding.Bindings.Add(new Binding("Caption") { Source = contentPane });
                multipaneHeaderBinding.Converter = multipaneHeaderValueConverter;

            }
            BindingOperations.ClearBinding(this, CaptionProperty);
            this.SetBinding(CaptionProperty, multipaneHeaderBinding); 
        }

         

        public object HeaderTooltip
        {
            get { return (object)GetValue(HeaderTooltipProperty); }
            set { SetValue(HeaderTooltipProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HeaderTooltip.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HeaderTooltipProperty =
            DependencyProperty.Register("HeaderTooltip", typeof(object), typeof(ContentPane), new UIPropertyMetadata(null));

        
        public TextTrimming TitleTextTrimming
        {
            get { return (TextTrimming)GetValue(TitleTextTrimmingProperty); }
            set { SetValue(TitleTextTrimmingProperty, value); }
        }

        public static readonly DependencyProperty TitleTextTrimmingProperty =
            DependencyProperty.Register("TitleTextTrimming", typeof(TextTrimming), typeof(ContentPane), new UIPropertyMetadata(TextTrimming.None));


         
        public static readonly RoutedEvent ChildContainerChangedEvent = EventManager.RegisterRoutedEvent(
            "ChildContainerChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ContentPane));

        // Provide CLR accessors for the event 
        public event RoutedEventHandler ChildContainerChanged
        {
            add { AddHandler(ChildContainerChangedEvent, value); }
            remove { RemoveHandler(ChildContainerChangedEvent, value); }
        }
         
        internal protected virtual void OnChildContainerChanged()
        {
            RaiseEvent(new RoutedEventArgs(ChildContainerChangedEvent)); 
        }

        public IList<ContentPane> GetChildrenPanes()
        {
            var panes = new List<ContentPane>();
            if (this.Wrapper != null && Wrapper.DockingGrid != null)
            {
                foreach (var container in Wrapper.DockingGrid.NormalContainers)
                {
                    panes.AddRange(container.GetChildrenPanes());
                }
                foreach (var container in Wrapper.DockingGrid.MinimizedPaneContainers.Containers)
                {
                    panes.AddRange(container.GetChildrenPanes());
                } 
            }
            return panes;
        }
 
        protected override void OnGotFocus(RoutedEventArgs e_)
        { 
            ActivateCurrentPane();
            e_.Handled = true;
            base.OnGotFocus(e_);
        }

        protected override void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs e_)
        {  
            if (!SkipActiveSet(e_.NewFocus))
            {
                ActivateCurrentPane(); 
                e_.Handled = true;
            }

            base.OnGotKeyboardFocus(e_);
        }

        private bool SkipActiveSet(IInputElement newFocus_)
        {
            if (newFocus_ == null) return true;
            if (newFocus_ is WindowButton) return true; 
            if (newFocus_ is PaneContainerHeaderControl) return true;
            FrameworkElement element = newFocus_ as FrameworkElement;
            if (element != null)
            {
                if (element.Name == "captionBarControl") return true; 
            }
            return false;
        }

        protected override void OnContentChanged(object oldContent_, object newContent_)
        {
            FrameworkElement element = newContent_ as FrameworkElement;
            if (element != null && SizeToContent == SizingMethod.SizeToContent)
            {
                this.PaneHeight = element.Height + this.CaptionBarHeight;
                this.PaneWidth = element.Width;
                element.ClearValue(FrameworkElement.HeightProperty);
                element.ClearValue(FrameworkElement.WidthProperty);
            }
            base.OnContentChanged(oldContent_, newContent_);
        }

        public double PaneHeaderHeight
        {
            get { return (double)GetValue(PaneHeaderHeightProperty); }
            set { SetValue(PaneHeaderHeightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CaptionBarHeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PaneHeaderHeightProperty =
            DependencyProperty.Register("PaneHeaderHeight", typeof(double), typeof(ContentPane), new UIPropertyMetadata(PaneContainer.PaneHeaderHeightDefault, PaneHeaderHeightChanged, CoercePaneHeaderHeight));

        private static object CoercePaneHeaderHeight(DependencyObject dependencyObject_, object baseValue_)
        {
            double height = (double) baseValue_;
            return height < 0 ? 0 : height;
        }

        private static void PaneHeaderHeightChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            ContentPane pane = (ContentPane) dependencyObject_;
            pane.CaptionBarHeight = pane.PaneHeaderHeight + 5;
            if (pane.Wrapper != null && pane.Wrapper.Host != null)
            {
                pane.Wrapper.Host.UpdateActivePaneSize();
            }
        }

        public double CaptionBarHeight
        {
            get { return (double)GetValue(CaptionBarHeightProperty); }
            internal set { SetValue(CaptionBarHeightPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey CaptionBarHeightPropertyKey
        = DependencyProperty.RegisterReadOnly("CaptionBarHeight", typeof(double), typeof(ContentPane), new PropertyMetadata(PaneContainer.CaptionBarHeightDefault));

        public static readonly DependencyProperty CaptionBarHeightProperty = CaptionBarHeightPropertyKey.DependencyProperty;


        internal static DependencyPropertyKey ActiveChildContainerPropertyKey = DependencyProperty.RegisterAttachedReadOnly
        (
            "ActiveChildContainer",
            typeof(PaneContainer),
            typeof(ContentPane),
            new PropertyMetadata(null)
        );

        public static DependencyProperty ActiveChildContainerProperty
        {
            get
            {
                return ActiveChildContainerPropertyKey.DependencyProperty;
            }
        }

        public static void SetActiveChildContainer(UIElement element_, PaneContainer value_)
        {
            element_.SetValue(ActiveChildContainerPropertyKey, value_);
        }

        public static PaneContainer GetActiveChildContainer(UIElement element_)
        {
            return (PaneContainer)element_.GetValue(ActiveChildContainerProperty);
        }
         
        public bool IsContainerPane
        {
            get { return (bool)GetValue(IsContainerPaneProperty); }
            internal set { SetValue(IsContainerPanePropertyKey, value); }
        }

        private static readonly DependencyPropertyKey IsContainerPanePropertyKey
        = DependencyProperty.RegisterReadOnly("IsContainerPane", typeof(bool), typeof(ContentPane), new PropertyMetadata(false));

        public static readonly DependencyProperty IsContainerPaneProperty
            = IsContainerPanePropertyKey.DependencyProperty;




        public PaneLocation PaneLocation
        {
            get { return (PaneLocation)GetValue(PaneLocationProperty); }
            set { SetValue(PaneLocationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Location.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PaneLocationProperty =
            DependencyProperty.Register("PaneLocation", typeof(PaneLocation), typeof(ContentPane), new PropertyMetadata(PaneLocation.Unknown, PropertyChangedCallback));

        private static void PropertyChangedCallback(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            ContentPane pane = (ContentPane) dependencyObject_; 
            if (pane.IsContainerPane && !pane.updatePaneLocationInternally)
            {
                foreach (var contentPane in pane.GetAllPanes())
                {
                    contentPane.UpdatePaneLocation();
                }
            }
        }

        private bool updatePaneLocationInternally = false;
        internal void UpdatePaneLocation()
        {
            var oldLocation = this.PaneLocation;
            updatePaneLocationInternally = true;
            this.PaneLocation = PaneLocation.Unknown;
            this.PaneLocation = oldLocation;
            updatePaneLocationInternally = false;
        }

        public DateTime LastActivatedTime { get; set; } 

        public Size LastFloatingSize { get; set; }
         
        public DockLocation LastDockLocation { get; set; }

        private PaneContainer lastDockContainer;
        public PaneContainer LastDockContainer
        {
            get { return lastDockContainer; }
            set
            {
                if (value != lastDockContainer)
                {
                    if (lastDockContainer != null)
                    {
                        lastDockContainer.Closed -= LastDockContainer_Closed; 
                    }
                    lastDockContainer = value; 
                    if (lastDockContainer != null)
                    {
                        lastDockContainer.Closed += LastDockContainer_Closed;
                    }
                }
            }
        }

        void LastDockContainer_Closed(object sender_, EventArgs e_)
        {
            this.LastDockContainer.Closed -= LastDockContainer_Closed;
            this.LastDockContainer = null;
        }

        public double PaneWidth
        {
            get { return (double)GetValue(PaneWidthProperty); }
            set { SetValue(PaneWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PaneWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PaneWidthProperty =
            DependencyProperty.Register("PaneWidth", typeof(double), typeof(ContentPane), new PropertyMetadata(double.NaN));

        public double PaneHeight
        {
            get { return (double)GetValue(PaneHeightProperty); }
            set { SetValue(PaneHeightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PaneHeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PaneHeightProperty =
            DependencyProperty.Register("PaneHeight", typeof(double), typeof(ContentPane), new PropertyMetadata(double.NaN));

        public bool Renaming
        {
            get { return (bool)GetValue(RenamingProperty); }
            set { SetValue(RenamingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Renaming.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RenamingProperty =
            DependencyProperty.Register("Renaming", typeof(bool), typeof(ContentPane), new PropertyMetadata(false)); 

        public Visibility FloatingOnlyVisibility
        {
            get { return (Visibility)GetValue(FloatingOnlyVisibilityProperty); }
            set { SetValue(FloatingOnlyVisibilityProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FloatingOnlyVisibility.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FloatingOnlyVisibilityProperty =
            DependencyProperty.Register("FloatingOnlyVisibility", typeof(Visibility), typeof(ContentPane), new UIPropertyMetadata(Visibility.Visible));

        
        public bool AllowDock
        {
            get { return (bool)GetValue(AllowDockProperty); }
            set { SetValue(AllowDockProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AllowDock.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AllowDockProperty =
            DependencyProperty.Register("AllowDock", typeof(bool), typeof(ContentPane), new UIPropertyMetadata(true, OnAllowDockChanged, CoerceAllowDock));

        private static object CoerceAllowDock(DependencyObject dependencyObject_, object baseValue_)
        {
            ContentPane pane = (ContentPane) dependencyObject_;
            if (pane.FloatingOnlyVisibility != Visibility.Visible)
            {
                return true;
            }
            return baseValue_;
        }

        private static void OnAllowDockChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            ContentPane pane = (ContentPane)dependencyObject_; 
            pane.UpdateAllowDock();
        }
          
        public bool AllowTearOff
        {
            get { return (bool)GetValue(AllowTearOffProperty); }
            set { SetValue(AllowTearOffProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AllowTearOff.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AllowTearOffProperty =
            DependencyProperty.Register("AllowTearOff", typeof(bool), typeof(ContentPane), new UIPropertyMetadata(true));

         

        //todo implement
        private void CanExecuteCommand(object sender_, CanExecuteRoutedEventArgs canExecuteRoutedEventArgs_)
        {
            canExecuteRoutedEventArgs_.CanExecute = true;
            canExecuteRoutedEventArgs_.Handled = true;
        }

        public void ExecuteCommand(RoutedCommand command_)
        {
            if (command_ == ContentPaneCommands.Close)
            {
                Close();
            }
            else if (command_ == ContentPaneCommands.Rename)
            {
                this.Activate();
                this.Renaming = true;
            }
            else if (command_ == ContentPaneCommands.Activate)
            {
                this.Activate();
            }
            else if (command_ == ContentPaneCommands.TearOff)
            {
                this.TearOff();
            }
            else if (command_ == ContentPaneCommands.ChangeToDockable)
            {
                this.AllowDock = true;
            }
            else if (command_ == ContentPaneCommands.ChangeToFloatingOnly)
            {
                this.AllowDock = false;
            }
        }

        private void UpdateAllowDock()
        {
            if (!AllowDock)
            {
                var window = FloatingWindow.GetFloatingWindow(this);
                if (window == null)
                {
                    return;
                }

                var container = IsSingleRootPane ? window : TearOffInternal(true);
                if (container != null)
                {
                    container.PaneContainer.AllowDock = false;
                    if (IsContainerPane)
                    {
                        foreach (var childrenPane in GetChildrenPanes())
                        {
                            childrenPane.AllowDock = false;
                        }
                    }
                }

            }
            else
            {
                if (lastDockContainer != null && LastDockLocation != DockLocation.None &&
                    !lastDockContainer.IsLocked && lastDockContainer.AllowDock && lastDockContainer.MinimizedProxyResolved == null)
                {
                    if (Window.GetWindow(lastDockContainer) == null)
                    {
                        LastDockContainer = null;
                    }

                    DockService.DockContainer(ParentContainer, lastDockContainer, LastDockLocation);
                }
            }
            if (Wrapper.Host != null)
            {
                Wrapper.Host.UpdateTabItems();
            }
        }

        private void ExecuteCommand(object sender_, ExecutedRoutedEventArgs executedRoutedEventArgs_)
        {
            ExecuteCommand(executedRoutedEventArgs_.Command as RoutedCommand);
        }

        public ContentPaneWrapper Wrapper
        {
            get { return (ContentPaneWrapper)GetValue(WrapperProperty); }
            internal set { SetValue(WrapperPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey WrapperPropertyKey
        = DependencyProperty.RegisterReadOnly("Wrapper", typeof(ContentPaneWrapper), typeof(ContentPane), new PropertyMetadata(null, OnWrapperChanged));

        private static void OnWrapperChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            ContentPaneWrapper wrapper = dependencyPropertyChangedEventArgs_.NewValue as ContentPaneWrapper;
            if (wrapper == null) return;
            BindingOperations.ClearBinding(wrapper, ContentPaneWrapper.TitleTextTrimmingProperty);
            wrapper.SetBinding(ContentPaneWrapper.TitleTextTrimmingProperty,
                               new Binding("TitleTextTrimming") { Source = dependencyObject_ }); 
        }

        public static readonly DependencyProperty WrapperProperty
            = WrapperPropertyKey.DependencyProperty;

        public string Caption
        {
            get { return (string)GetValue(CaptionProperty); }
            set { SetValue(CaptionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Caption.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CaptionProperty =
            DependencyProperty.Register("Caption", typeof(string), typeof(ContentPane), new PropertyMetadata(null));


        public ImageSource Icon
        {
            get { return (ImageSource)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Icon.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconProperty =
            DependencyProperty.Register("Icon", typeof(ImageSource), typeof(ContentPane), new PropertyMetadata(null)); 
        
        public ObservableCollection<object> CustomItems
        {
            get { return (ObservableCollection<object>)GetValue(CustomItemsProperty); }
            set { SetValue(CustomItemsProperty, value); }
        }

        private static readonly DependencyProperty CustomItemsProperty = DependencyProperty.Register("CustomItems", typeof(ObservableCollection<object>), typeof(ContentPane), new PropertyMetadata(null));



        public Visibility HeaderIconVisibility
        {
            get { return (Visibility)GetValue(HeaderIconVisibilityProperty); }
            set { SetValue(HeaderIconVisibilityProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HeaderIconVisibility.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HeaderIconVisibilityProperty =
            DependencyProperty.Register("HeaderIconVisibility", typeof(Visibility), typeof(ContentPane), new UIPropertyMetadata(Visibility.Visible));


        public SizingMethod SizeToContent { get; set; }
          
        public bool ShowInTaskbar
        {
            get { return (bool)GetValue(ShowInTaskbarProperty); }
            set { SetValue(ShowInTaskbarProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowInTaskbar.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowInTaskbarProperty =
            DependencyProperty.Register("ShowInTaskbar", typeof(bool), typeof(ContentPane), new UIPropertyMetadata(true, ShowInTaskbarChanged));

        private static void ShowInTaskbarChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            ContentPane pane = (ContentPane) dependencyObject_;
            if (pane.Wrapper == null || pane.Wrapper.Host == null) return;
            pane.Wrapper.Host.UpdateTabItems();
        } 

        public bool Topmost
        {
            get { return (bool)GetValue(TopmostProperty); }
            set { SetValue(TopmostProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Topmost.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TopmostProperty =
            DependencyProperty.Register("Topmost", typeof(bool), typeof(ContentPane), new UIPropertyMetadata(false, TopmostChanged));

        private static void TopmostChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            ContentPane pane = (ContentPane) dependencyObject_;
            FloatingWindow window = FloatingWindow.GetFloatingWindow(pane);
            if (window != null)
            {
                window.UpdateTopmost(pane.Topmost);
            }
            WindowViewModel model = pane.DataContext as WindowViewModel;
            if (model != null)
            {
                model.Topmost = pane.Topmost;
            }
        }


        public bool SaveInLayout { get; set; }
        public bool Close()
        {
            if (Wrapper == null || Wrapper.Host == null) return true;
            return this.Wrapper.Host.Close(this.Wrapper, false);
        }

        public void Activate()
        {
            this.ActivateInternal(true); 
        }

        public PaneContainer TearOff()
        {
            var floatingWindow = TearOffInternal(false);
            return floatingWindow != null ? floatingWindow.PaneContainer : null;
        }

        internal FloatingWindow TearOffInternal(bool recordLastPaneContainer_)
        {
            var parentContainer = this.ParentContainer;
            if (parentContainer == null) return null;
            if (this.IsSingleRootPane) return null;
            if (recordLastPaneContainer_)
            {
                if (LastDockLocation == DockLocation.Center)
                {
                    LastDockContainer = parentContainer;
                }
                else if (LastDockLocation != DockLocation.None)
                {
                    LastDockContainer = parentContainer.FindVisualParent<PaneContainer>();
                }
            }
            MinimizedPaneContainer minimizeProxy = parentContainer.MinimizedProxy;
            var floatingPosition = WPFHelper.GetPositionWithOffset(this.Wrapper.IsSingleTab ? this.Wrapper.Host as FrameworkElement : this.Wrapper);
            PaneContainer paneContainer = null;
            if (this.Wrapper.IsSingleTab) //tear off a single pane, we could utilize the original PaneContainer
            {
                paneContainer = parentContainer;
                var parentDockingGrid = this.ParentDockingGrid;
                if (minimizeProxy != null)
                {
                    minimizeProxy.RemoveFromParent();
                }
                else if (parentDockingGrid != null)
                {
                    parentDockingGrid.Remove(parentContainer);
                }
            }
            else //create a new PaneContainer to host the tab
            {
                paneContainer = new PaneContainer();
                paneContainer.MergePane(this);
            }
            FloatingWindow fw = paneContainer.CreateHostingWindow();
            if (this.LastFloatingSize.Width > 0 && this.LastFloatingSize.Height > 0)
            {
                paneContainer.PaneHeight = this.LastFloatingSize.Height;
                paneContainer.PaneWidth = this.LastFloatingSize.Width;
            }
            else
            {
                paneContainer.PaneHeight = parentContainer.PaneHeight;
                paneContainer.PaneWidth = parentContainer.PaneWidth;
            }
            fw.Top = floatingPosition.Y;
            fw.Left = floatingPosition.X;
            DockService.SetLastPosition(fw, WPFHelper.GetMousePosition());
            fw.Show(); 
            return fw;
        }

        public void Detach()
        {
            var parentContainer = ParentContainer;
            var parentDockingGrid = ParentDockingGrid;
            if (parentContainer == null) return;
            MinimizedPaneContainer minimizeProxy = parentContainer.MinimizedProxy;
            this.Wrapper.Host.RemovePaneWrapper(this.Wrapper); //remove from the host container
            if (minimizeProxy != null)
            {
                minimizeProxy.RemoveFromParent();
            }
            else if (parentDockingGrid != null)
            {
                parentDockingGrid.Remove(parentContainer);
            }
            this.Wrapper.DockingGrid.Content = null;
            this.Wrapper = null;
        }

        public bool IsSingleRootPane
        {
            get 
            { 
                if (Wrapper == null) return false;
                if (!Wrapper.IsSingleTab) return false;
                var parentContainer = ParentContainer;
                if (parentContainer == null) return false;
                return parentContainer.IsRoot;
            }
        }
        public PaneContainer ParentContainer
        {
            get
            {
                if (Wrapper == null) return null;
                if (Wrapper.Host == null) return null;
                return Wrapper.Host.ParentContainer;
            }
        }

        internal ContentPane ParentPane
        {
            get
            {
                var parentContainer = ParentContainer;
                if (parentContainer == null) return null;
                var parentPane = parentContainer.FindVisualParent<ContentPane>();
                return parentPane;
            }
        }

        internal DockingGrid ParentDockingGrid
        {
            get
            {
                var parentPane = ParentPane;
                if (parentPane == null) return null;
                return parentPane.Wrapper.DockingGrid;
            }
        }

        internal DockingGrid DockingGrid { get; private set; }

        internal void OnClosing(ClosingEventArgs arg_)
        {
            var copy = Closing;
            if (copy != null)
            {
                copy(this, arg_);
            }
        }

        internal void OnClosed()
        {
            var copy = Closed;
            if (copy != null)
            {
                copy(this, EventArgs.Empty);
            }
        }

        internal void OnOptionsMenuOpening(ContextMenu menu_)
        {
            var copy = this.OptionsMenuOpening;
            if (copy != null)
            {
                copy(this, new PaneOptionsMenuOpeningEventArgs(menu_.Items));
            }
        }
        public event EventHandler Closed;
        public event EventHandler<ClosingEventArgs> Closing; 
        public event EventHandler<PaneOptionsMenuOpeningEventArgs> OptionsMenuOpening;

        internal bool CanActivate
        {
            get
            {
                return ((base.Visibility == Visibility.Visible) && base.IsEnabled);
            }
        }

        internal bool ActivateInternal(bool bringIntoView_)
        {
            if (!CanActivate) return false;
            if (bringIntoView_)
            {
                var thisContainer = this.ParentContainer;
                var thisWrapper = this.Wrapper;
                while (thisContainer != null)
                {
                    if (thisContainer.WindowState == WindowState.Minimized)
                    {
                        thisContainer.ExecuteCommand(PaneContainerCommands.Restore);
                    }
                    if (thisWrapper != null)
                    {
                        if (thisContainer.Host.SelectedItem != thisWrapper)
                        {
                            thisContainer.Host.SelectedItem = thisWrapper;
                        }
                        thisWrapper = thisContainer.FindVisualParent<ContentPaneWrapper>();
                    }
                    thisContainer = thisContainer.FindVisualParent<PaneContainer>();
                }
            }

            if (!bringIntoView_)
            {
                this.Focus();
                ActivateCurrentPane();
                return true;
            }
            FloatingWindow window = FloatingWindow.GetFloatingWindow(this);
            if (window == null) return false;

            if (window.WindowState == WindowState.Minimized)
            {
                window.WindowState = WindowState.Normal;
            }
            if (!window.Activate())
            {
                return false;
            }
            if (window.WindowState == WindowState.Normal)
            {
                window.BringIntoView();
            }
            this.BringIntoView();
            this.Focus();
            if (this.IsVisible && this.IsFocused)
            {
                ActivateCurrentPane();
                return true;
            }
            return false;

        }

        internal void ActivateCurrentPane()
        {
            Services.DockManager.ActiveContainer = this.ParentContainer;
            this.Wrapper.Host.ActiveWrapper = this.Wrapper;
            this.ParentContainer.ActivePane = this;

            WindowViewModel model = this.DataContext as WindowViewModel;
            if (model != null)
            {
                model.IsActive = true;
            }
        }
        public void Dispose()
        {
            //todo: implement it

        }

        private ContentPane PaneResolved
        {
            get 
            { 
                if (!this.IsContainerPane) return this;
                var container = ContentPane.GetActiveChildContainer(this);
                if (container == null || container.ActivePane == null) return null;
                return container.ActivePane.PaneResolved;
            }
        }

        void ContentPane_OptionsMenuOpening(object sender_, PaneOptionsMenuOpeningEventArgs e_)
        {

            e_.Items.Add(new Separator());
            WindowViewModel model = null;
            ContentPane pane = PaneResolved;
            if (pane != null)
            {
                model = pane.DataContext as WindowViewModel;
                if (model != null)
                {
                    if (!ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableViewHeaderContextMenuRename) && 
                        string.IsNullOrEmpty(model.WorkspaceCategory)) //disable renaming for workspaces
                    {
                        var item = helper.CreateOptionsMenuItem("Rename", RenameMenuItem_Click);

                        if (DockViewModel.RenameDisabled)
                            item.IsEnabled = false;

                        e_.Items.Add(item);
                    }

                    var extraItems = ((WindowViewContainer)model.ViewContainer).ExtraContextMenuItems;
                    if (extraItems.Count > 0)
                    {
                        e_.Items.Add(new Separator());
                        foreach (var extraItem in extraItems)
                        {
                            var logicalParent = LogicalTreeHelper.GetParent((DependencyObject)extraItem.Content) as ContextMenu;
                            if (logicalParent != null)
                            {
                                logicalParent.Items.Remove(extraItem.Content);
                            }
                            e_.Items.Add(extraItem.Content);
                        }
                    }
                }

            }
            

            e_.Items.Add(new Separator());
            if (!this.Wrapper.IsSingleTab)
            {
                int index = this.Wrapper.Host.Items.IndexOf(this.Wrapper);
                if (!ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableSubtabContextMenuMove))
                {
                    if (index > 0)
                    {
                        e_.Items.Add(helper.CreateOptionsMenuItem("Move left", MoveLeftMenuItem_Click));
                    }
                    if (index < this.Wrapper.Host.Items.Count - 1)
                    {
                        e_.Items.Add(helper.CreateOptionsMenuItem("Move right", MoveRightMenuItem_Click));
                    }
                }
                if (!ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableViewHeaderContextMenuClose))
                {
                    e_.Items.Add(helper.CreateOptionsMenuItem("Close", CloseMenuItem_Click));
                    e_.Items.Add(helper.CreateOptionsMenuItem("Close all", CloseAllMenuItem_Click));
                    e_.Items.Add(helper.CreateOptionsMenuItem("Close others", CloseOthersMenuItem_Click));

                }
            }
            #region Legacy Workspace
            if (ChromeManagerBase.WorkspaceSaveLoadEnabled && 
                (model == null || string.IsNullOrEmpty(model.WorkspaceCategory)) &&
                Window.GetWindow(this) != System.Windows.Application.Current.MainWindow) //use new workspace saving menus in this case
            {
                e_.Items.Add(helper.CreateOptionsMenuItem("Save workspace",
                                                  (s, ea) => Command.SaveSublayoutCommand.Execute(null, null)));

            }

            #endregion

            helper.FixOptionsMenuItems(e_.Items);
 
        }

        void RenameMenuItem_Click(object sender_, RoutedEventArgs e_)
        {
            var pane = PaneResolved;
            if (pane != null)
            {
                pane.ExecuteCommand(ContentPaneCommands.Rename);
            }
        }

        void MoveLeftMenuItem_Click(object sender_, RoutedEventArgs e_)
        {
            var wrapper = this.Wrapper;
            var tgp = wrapper.Host;
            if (tgp != null)
            {
                var i = tgp.Items.IndexOf(wrapper);
                if (i > 0)
                {
                    tgp.Items.Remove(wrapper);
                    tgp.Items.Insert(i - 1, wrapper);
                    tgp.Items.Refresh();
                    tgp.SelectedItem = wrapper;
                }
            }
        }

        void MoveRightMenuItem_Click(object sender_, RoutedEventArgs e_)
        {
            var wrapper = this.Wrapper;
            var tgp = wrapper.Host;
            if (tgp != null)
            {
                var i = tgp.Items.IndexOf(wrapper);
                if (i < tgp.Items.Count - 1)
                {
                    tgp.Items.Remove(wrapper);
                    tgp.Items.Insert(i + 1, wrapper);
                    tgp.Items.Refresh();
                    tgp.SelectedItem = wrapper;
                }
            }
        }

        void CloseMenuItem_Click(object sender_, RoutedEventArgs e_)
        {
            this.ExecuteCommand(ContentPaneCommands.Close);
        }

        void CloseAllMenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.ParentContainer.ExecuteCommand(PaneContainerCommands.Close);
        }
        private void CloseOthersMenuItem_Click(object sender, RoutedEventArgs routedEventArgs)
        {
            var panes = this.ParentContainer.GetChildrenPanes();
            foreach (var contentPane in panes)
            {
                if (contentPane != this)
                {
                    contentPane.ExecuteCommand(ContentPaneCommands.Close);
                }
            } 
        }
        #region For MSDesktop

        IWindowViewContainer IWindowViewContainerHost.View
        {
            get
            {
                WindowViewModel model = this.DataContext as WindowViewModel;
                if (model == null) return null;
                return model.ViewContainer as IWindowViewContainer;
            }
        }

        IFloatingWindow IWindowViewContainerHost.FloatingWindow
        {
            get 
            { 
                PaneContainer container = this.ParentContainer;
                if (container == null) return null;
                var minizedProxy = container.MinimizedProxyResolved; 
                if (minizedProxy != null)
                {
                   return Window.GetWindow(minizedProxy) as IFloatingWindow;
                }
                return  Window.GetWindow(this) as IFloatingWindow;
            }
        }

        public bool CanChangeDimensions
        {
            get { return this.IsSingleRootPane && ((WindowViewModel)this.DataContext).InitialParameters.SizingMethod == Core.SizingMethod.Custom; }
        }
         
        internal void SetContentWidth(double width_)
        {
            helper.SetContentWidth(width_); 
        }
        internal void SetContentHeight(double height_)
        {
            helper.SetContentHeight(height_);
        }

        internal void SetContentMaximumWidth(double floatingWindowMaximumWidth_)
        {
            helper.SetContentMaximumWidth(floatingWindowMaximumWidth_);
        }
        internal void SetContentMinimumWidth(double floatingWindowMinimumWidth_)
        {
            helper.SetContentMinimumWidth(floatingWindowMinimumWidth_); 
        }
        internal void SetContentMaximumHeight(double floatingWindowMaximumHeight_)
        {
            helper.SetContentMaximumHeight(floatingWindowMaximumHeight_);  
        }
        public void SetContentMinimumHeight(double floatingWindowMinimumHeight_)
        {
            helper.SetContentMinimumHeight(floatingWindowMinimumHeight_); 
        }

        private bool allowMaximize = true;
        public bool AllowMaxmize
        {
            get { return allowMaximize; }
            set
            {
                if (allowMaximize != value)
                {
                    allowMaximize = value;
                    var window = FloatingWindow.GetFloatingWindow(this);
                    if (window != null)
                    {
                        window.PaneContainer.AllowMaximize = allowMaximize;
                    } 
                }
            }
        }

        private bool allowClose = true;
        public bool AllowClose
        {
            get { return allowClose; }
            set
            {
                if (allowClose != value)
                {
                    allowClose = value;
                    var window = FloatingWindow.GetFloatingWindow(this);
                    if (window != null)
                    {
                        window.PaneContainer.AllowClose = allowClose;
                    }
                }
            }
        }

        private bool isHeaderVisible = true;
        public bool IsHeaderVisible
        {
            get { return isHeaderVisible; }
            set
            {
                if (isHeaderVisible != value)
                {
                    isHeaderVisible = value;
                    PaneContainer container = this.ParentContainer;
                    if (container != null)
                    {
                        container.IsHeaderVisible = value;
                    }
                }
            }
        }

        private bool isOwnedByMainWindow = WindowViewContainer.DefaultIsOwnedWindowValue;
        public bool IsOwnedByMainWindow
        {
            get { return isOwnedByMainWindow; }
            set
            {
                if (isOwnedByMainWindow != value)
                {
                    isOwnedByMainWindow = value;
                    PaneContainer container = this.ParentContainer;
                    if (container != null)
                    {
                        container.IsOwnedByMainWindow = value;
                    }
                    FloatingWindow window = FloatingWindow.GetFloatingWindow(this);
                    if (window != null)
                    {
                        window.IsOwnedByMainWindow = value;
                    }
                }
            }
        }
        #endregion

        internal static readonly IComparer<ContentPane> Comparer = new ActivatedTimeComparer();

        private class ActivatedTimeComparer : IComparer<ContentPane>
        {
            public int Compare(ContentPane x_, ContentPane y_)
            {
                return x_.LastActivatedTime.CompareTo(y_.LastActivatedTime);
            }
        }



    }

    public class ClosingEventArgs:CancelEventArgs
    {
        public ClosingEventArgs(bool emptyContent_)
        {
            this.EmptyContent = emptyContent_;
        }

        public bool EmptyContent { get; private set; }
    }

    public class PaneOptionsMenuOpeningEventArgs : EventArgs
    { 

        public PaneOptionsMenuOpeningEventArgs(ItemCollection items_)
        {

            this.Items = items_;
        }

        public ItemCollection Items { get; private set; }
    }
}
