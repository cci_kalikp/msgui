﻿using System; 
using System.Windows.Data;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls; 
using MorganStanley.MSDotNet.MSGui.Impl.View;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Converters
{
    public class ContainerPaneCustomItemsConverter:IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length < 3) return null;
            ContentPane pane = values[2] as ContentPane;
            if (pane == null) return null;
            if (!pane.IsContainerPane || FloatingWindowUtil.SeparateHeaderItemsInFloatingWindow)
            {
                return GetCustomItem(pane);
            }
            PaneContainer activeContainer = values[0] as PaneContainer;
            if (activeContainer == null || activeContainer.ActivePane == null) return null;
            if (activeContainer.ActivePane.IsContainerPane) return activeContainer.ActivePane.CustomItems;
            return GetCustomItem(activeContainer.ActivePane);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private static HeaderItemsCollection GetCustomItem(ContentPane pane_)
        {
            IWindowViewContainer container = Attached.GetViewContainer(pane_) as IWindowViewContainer;
            if (container == null)
            {
                return null;
            }
            var newHeader = container.Header as HeaderItemsHolderWidgetContainer;
            if (newHeader == null) return null;
            return newHeader.Items;
        }
    }
}
