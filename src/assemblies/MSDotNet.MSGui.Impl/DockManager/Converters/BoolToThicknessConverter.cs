﻿using System;
using System.Windows;
using System.Windows.Data;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Converters
{
    public class BoolToThicknessConverter:IValueConverter
    {
        public Thickness TrueValue { get; set; }
        public Thickness FalseValue { get; set; }
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return System.Convert.ToBoolean(value) ? TrueValue : FalseValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
