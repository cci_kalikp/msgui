﻿ 
using System;
using System.Globalization; 
using System.Windows.Data; 

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Converters
{
    public class HeaderItemsConverter : IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length < 2) return null;
            var pane = values[1] as ContentPane;

            return pane == null ? null : pane.CustomItems;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
