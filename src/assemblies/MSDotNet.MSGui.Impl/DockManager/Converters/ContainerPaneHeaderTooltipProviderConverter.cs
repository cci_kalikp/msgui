﻿using System; 
using System.Windows.Data;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls; 

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Converters
{
    public class ContainerPaneHeaderTooltipConverter:IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (FloatingWindowUtil.SeparateHeaderItemsInFloatingWindow) return null;
            PaneContainer activeContainer = values[0] as PaneContainer;
            if (activeContainer == null || activeContainer.ActivePane == null) return null;
            return activeContainer.ActivePane.HeaderTooltip;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        
    }
}
