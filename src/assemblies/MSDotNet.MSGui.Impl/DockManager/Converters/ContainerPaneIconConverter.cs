﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Converters
{
    public class ContainerPaneIconConverter:IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            PaneContainer activeContainer = values[0] as PaneContainer;
            if (activeContainer == null || activeContainer.ActivePane == null) return null;
            return activeContainer.ActivePane.Icon;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
