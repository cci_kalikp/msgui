﻿using System;
using System.Collections.Generic;
using System.ComponentModel; 
using System.Linq;
using System.Windows;
using System.Windows.Controls; 
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Services;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls; 

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager
{
    //[TemplatePart(Name = "PART_Header", Type = typeof(PaneContainerHeaderControl))]
    //[TemplatePart(Name = "PART_CaptionBar", Type = typeof(Border))]
    //[TemplatePart(Name = "PART_WrapperHost", Type = typeof(ContentPaneWrapperHost))]
    public partial class PaneContainer : UserControl, IPaneContainer, IDisposable
    {
        public static Thickness ContainerBorderThickness = new Thickness(0.8);
        public static double PaneHeaderHeightDefault = 20;
        public static double CaptionBarHeightDefault
        {
            get { return PaneHeaderHeightDefault + ContentPaneWrapperHost.HostPadding.Top; }
        }
        public event EventHandler ContainerStateChanged;
        public event EventHandler<HandledEventArgs> ContainerCloseRequest;
        public event EventHandler ContainerIsLockedChanged;
        public event EventHandler ContainerHeaderVisibleChanged;
        public event EventHandler TopmostChanged;

        public event MouseButtonEventHandler TitleBarClick;
        public event MouseButtonEventHandler TitleBarUnClick;
        public event MouseEventHandler ContainerDragMove;
        public event EventHandler<FloatingWindowEventArgs> FloatingWindowCreated;
        public event EventHandler<ActivePaneChangedEventArgs> ActivePaneChanged;
        public const string DefaultName = "Container";

        ContentPane currentMovingPane;
        public override string ToString()
        {
            string result = string.Empty;
            foreach (var pane in this.GetChildrenPanes())
            {
                result += pane + ","; 
            }

            return string.IsNullOrEmpty(result) ? result : result.Substring(0, result.Length - 1);
        }

        #region Public Properties
         
        public DateTime LastActivatedTime { get; set; }

        public Size LastFloatingSize { get; set; } 
         
        public ContentPane ActivePane
        {
            get { return (ContentPane)GetValue(ActivePaneProperty); }
            internal set { SetValue(ActivePanePropertyKey, value); }
        }

        private static readonly DependencyPropertyKey ActivePanePropertyKey
        = DependencyProperty.RegisterReadOnly("ActivePane", typeof(ContentPane), typeof(PaneContainer), new PropertyMetadata(OnActivePaneChanged));

        private static void OnActivePaneChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            PaneContainer container = (PaneContainer) dependencyObject_;
            if (container.ActivePane != null)
            {
                container.AllowDock = container.ActivePane.AllowDock;
            }
            container.OnActivePaneChanged(dependencyPropertyChangedEventArgs_.OldValue as ContentPane, dependencyPropertyChangedEventArgs_.NewValue as ContentPane);
        }

        protected void OnActivePaneChanged(ContentPane oldActivePane_, ContentPane newActivePane_)
        {
            var copy = ActivePaneChanged;
            if (copy != null)
            {
                copy(this, new ActivePaneChangedEventArgs(oldActivePane_, newActivePane_));
            }
        }
        public static readonly DependencyProperty ActivePaneProperty = ActivePanePropertyKey.DependencyProperty;

    

        internal DockingGrid ActiveGrid
        {
            get
            {
                //if (paneWrapperHost == null) return singlePaneWrapper == null ? null : singlePaneWrapper.DockingGrid;
                var activeWrapper = this.paneWrapperHost.ActiveWrapper;
                if (activeWrapper != null) return activeWrapper.DockingGrid;
                return null;
            }
        }

        public bool IsMoving
        {
            set;
            get;
        }

        public ContentPaneWrapperHost Host
        {
            get { return paneWrapperHost; }
        }
          
        public Double PaneHeight { get; set; }

        public Double PaneWidth { get; set; }

        public WindowState WindowState
        {
            get { return (WindowState)GetValue(WindowStateProperty); }
            set { SetValue(WindowStateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WindowState.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WindowStateProperty =
            DependencyProperty.Register("WindowState", typeof(WindowState), typeof(PaneContainer), new PropertyMetadata(WindowState.Normal, StateChanged));

        private static void StateChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            PaneContainer pane = (PaneContainer)dependencyObject_;
            var copy = pane.ContainerStateChanged;
            if (copy != null)
            {
                copy(pane, EventArgs.Empty);
            }
        }

        public bool AllowDockResolved
        {
            get { return AllowDock && !IsLocked && !Topmost; }
        }
         

        public bool AllowDrag
        {
            get { return (bool)GetValue(AllowDragProperty); }
            set { SetValue(AllowDragProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AllowDrag.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AllowDragProperty =
            DependencyProperty.Register("AllowDrag", typeof(bool), typeof(PaneContainer), new UIPropertyMetadata(true));

        
        public bool AllowDock
        {
            get { return (bool)GetValue(AllowDockProperty); }
            set { SetValue(AllowDockProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AllowDock.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AllowDockProperty =
            DependencyProperty.Register("AllowDock", typeof(bool), typeof(PaneContainer), new UIPropertyMetadata(true, OnAllowDockChanged));


        private static void OnAllowDockChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            PaneContainer container = (PaneContainer) dependencyObject_;
            if (container.ActivePane != null)
            {
                container.ActivePane.AllowDock = container.AllowDock;
            }
        }

        public bool HideTearOff { get; set; }
        public bool AllowTearOff
        {
            get { return (bool)GetValue(AllowTearOffProperty); }
            set { SetValue(AllowTearOffProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AllowTearOff.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AllowTearOffProperty =
            DependencyProperty.Register("AllowTearOff", typeof(bool), typeof(PaneContainer), new UIPropertyMetadata(true, OnAllowTearOffChanged));

        private static void OnAllowTearOffChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            PaneContainer container = (PaneContainer)dependencyObject_;
            if (!container.AllowTearOff)
            {
                container.TearOffButtonState = WindowButtonState.None;
            }
            else
            {
                if (!container.IsRoot && !container.HideTearOff)
                {
                    container.TearOffButtonState = WindowButtonState.Normal;
                }
            }
        }


        public bool IsLocked
        {
            get { return (bool)GetValue(IsLockedProperty); }
            set { SetValue(IsLockedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WindowLockState.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsLockedProperty =
            DependencyProperty.Register("IsLocked", typeof(bool), typeof(PaneContainer), new PropertyMetadata(false, IsLockedChanged));

        private static void IsLockedChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            PaneContainer container = (PaneContainer)dependencyObject_;
            
            if (!container.IsLocked)
            {
                if (container.IsRoot)
                {
                    if (container.AllowMaximize && !container.HideMaximize)
                    {
                        container.MaximizeButtonState = WindowButtonState.Normal;
                    }
                }
                else
                {
                    if (container.AllowTearOff && !container.HideTearOff)
                    { 
                        container.TearOffButtonState = WindowButtonState.Normal;
                    }
                }

                
                if (container.WindowState != WindowState.Minimized)
                {
                    container.MinimizeButtonState = ShellModeExtension.GetStateForElement(HideLockUIGranularity.HideViewMinimizeButton);
                }
                container.CloseButtonState = container.AllowClose ? ShellModeExtension.GetStateForElement(HideLockUIGranularity.HideViewCloseButton) : WindowButtonState.None;
                container.ToggleTopmostButtonState = container.IsRoot && DockViewModel.EnableTopmost ? WindowButtonState.Normal : WindowButtonState.None;
            }
            else
            {
                container.DockButtonState = WindowButtonState.None;
                container.MinimizeButtonState = WindowButtonState.None;
                container.CloseButtonState = WindowButtonState.None; 
                container.MaximizeButtonState = WindowButtonState.None;
                container.ToggleTopmostButtonState = WindowButtonState.None;
                container.TearOffButtonState = WindowButtonState.None;
                if (Services.DockManager.ActiveContainer == container)
                {
                    Services.DockManager.ExecuteActions(container_ =>  container_.DockButtonState = WindowButtonState.None);
                }

            }
            Services.DockManager.ExecuteActions(container, container_ =>
                {
                    if (container_ != container)
                    {
                        container_.LockButtonState = container.IsLocked
                                                         ? WindowButtonState.None
                                                         : ShellModeExtension.GetStateForElement(HideLockUIGranularity.HideViewLockButton);
                        if (!container_.IsHeaderVisible && !container.IsLocked && container_.paneWrapperHost.HeaderControl != null)
                        {
                            container_.paneWrapperHost.HeaderControl.LockButtonVisibility =
                            container_.paneWrapperHost.HeaderControl.UnlockButtonVisibility = Visibility.Collapsed;
                            
                            return;
                        }
                    } 
                    container_.IsLocked = container.IsLocked;
                });

            var copy = container.ContainerIsLockedChanged;
            if (copy != null)
            {
                copy(container, EventArgs.Empty);
            }
        }

        public WindowButtonState MinimizeButtonState
        {
            get { return (WindowButtonState)GetValue(MinimizeButtonStateProperty); }
            set { SetValue(MinimizeButtonStateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MinimizeButtonState.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MinimizeButtonStateProperty =
            DependencyProperty.Register("MinimizeButtonState", typeof(WindowButtonState), typeof(PaneContainer), new PropertyMetadata(WindowButtonState.Normal));
         
        public WindowButtonState MaximizeButtonState
        {
            get { return (WindowButtonState)GetValue(MaximizeButtonStateProperty); }
            set { SetValue(MaximizeButtonStateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MaximizeButtonState.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaximizeButtonStateProperty =
            DependencyProperty.Register("MaximizeButtonState", typeof(WindowButtonState), typeof(PaneContainer), new PropertyMetadata(WindowButtonState.Normal));


        public WindowButtonState CloseButtonState
        {
            get { return (WindowButtonState)GetValue(CloseButtonStateProperty); }
            set { SetValue(CloseButtonStateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CloseButtonState.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CloseButtonStateProperty =
            DependencyProperty.Register("CloseButtonState", typeof(WindowButtonState), typeof(PaneContainer), new PropertyMetadata(WindowButtonState.Normal));

        public WindowButtonState LockButtonState
        {
            get { return (WindowButtonState)GetValue(LockButtonStateProperty); }
            set { SetValue(LockButtonStateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LockButtonState.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LockButtonStateProperty =
            DependencyProperty.Register("LockButtonState", typeof(WindowButtonState), typeof(PaneContainer), new PropertyMetadata(WindowButtonState.Normal));


        public WindowButtonState HideButtonState
        {
            get { return (WindowButtonState)GetValue(HideButtonStateProperty); }
            set { SetValue(HideButtonStateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HideButtonState.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HideButtonStateProperty =
            DependencyProperty.Register("HideButtonState", typeof(WindowButtonState), typeof(PaneContainer), new PropertyMetadata(WindowButtonState.Normal));

        public WindowButtonState DockButtonState
        {
            get { return (WindowButtonState)GetValue(DockButtonStateProperty); }
            set { SetValue(DockButtonStateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DockButtonState.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DockButtonStateProperty =
            DependencyProperty.Register("DockButtonState", typeof(WindowButtonState), typeof(PaneContainer), new PropertyMetadata(WindowButtonState.Normal, DockButtonStateChanged, CoerceDockButtonStateCallback));

        private static object CoerceDockButtonStateCallback(DependencyObject dependencyObject_, object baseValue_)
        {
            PaneContainer container = (PaneContainer)dependencyObject_;
            return container.AllowDockResolved ? baseValue_ : WindowButtonState.None;
        }

        private static void DockButtonStateChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            PaneContainer container = (PaneContainer)dependencyObject_;
            if (container.DockButtonState == WindowButtonState.None)
            {
                container.HideDockButton();
            }
            else
            {
                container.ShowDockButton();
            }
        } 

        public WindowButtonState ToggleTopmostButtonState
        {
            get { return (WindowButtonState)GetValue(ToggleTopmostButtonStateProperty); }
            set { SetValue(ToggleTopmostButtonStateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ToggleTopmostButtonState.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ToggleTopmostButtonStateProperty =
            DependencyProperty.Register("ToggleTopmostButtonState", typeof(WindowButtonState), typeof(PaneContainer), new UIPropertyMetadata(WindowButtonState.None));
         
        public WindowButtonState TearOffButtonState
        {
            get { return (WindowButtonState)GetValue(TearOffButtonStateProperty); }
            set { SetValue(TearOffButtonStateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TearOffButtonState.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TearOffButtonStateProperty =
            DependencyProperty.Register("TearOffButtonState", typeof(WindowButtonState), typeof(PaneContainer), new UIPropertyMetadata(WindowButtonState.Normal));



        
        public bool IsHeaderVisible
        {
            get { return (bool)GetValue(IsHeaderVisibleProperty); }
            set { SetValue(IsHeaderVisibleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsHeaderVisible.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsHeaderVisibleProperty =
            DependencyProperty.Register("IsHeaderVisible", typeof(bool), typeof(PaneContainer), new PropertyMetadata(true, IsHeaderVisibleChanged));

        private static void IsHeaderVisibleChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            PaneContainer container = (PaneContainer)dependencyObject_; 
            Services.DockManager.ExecuteActions(container, container_ =>
            {
                if (container_ != container)
                {
                    container_.HideButtonState = container.IsHeaderVisible
                                                     ? WindowButtonState.Normal
                                                     : WindowButtonState.None;
                }
                container_.IsHeaderVisible = container.IsHeaderVisible;

            });
            var copy = container.ContainerHeaderVisibleChanged;
            if (copy != null)
            {
                copy(container, EventArgs.Empty);
            }
        } 

        public bool ShowInTaskbar
        {
            get { return (bool)GetValue(ShowInTaskbarProperty); }
            set { SetValue(ShowInTaskbarProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowInTaskbar.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowInTaskbarProperty =
            DependencyProperty.Register("ShowInTaskbar", typeof(bool), typeof(PaneContainer), new UIPropertyMetadata(true));



        public bool Topmost
        {
            get { return (bool)GetValue(TopmostProperty); }
            set { SetValue(TopmostProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Topmost.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TopmostProperty =
            DependencyProperty.Register("Topmost", typeof(bool), typeof(PaneContainer), new UIPropertyMetadata(false, OnTopmostChanged));

        private static void OnTopmostChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            PaneContainer container = (PaneContainer) dependencyObject_;
            foreach (var pane in container.GetChildrenPanes())
            {
                pane.Topmost = container.Topmost;
            }
            var copy = container.TopmostChanged;
            if (copy != null)
            {
                copy(container, EventArgs.Empty);
            }
        }

        public bool HideMaximize { get; set; }

        public bool AllowMaximize
        {
            get { return (bool)GetValue(AllowMaximizeProperty); }
            set { SetValue(AllowMaximizeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AllowMaximize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AllowMaximizeProperty =
            DependencyProperty.Register("AllowMaximize", typeof(bool), typeof(PaneContainer), new UIPropertyMetadata(true, OnAllowMaximizeChanged, CoerceAllowMaximizeValue));

        private static object CoerceAllowMaximizeValue(DependencyObject dependencyObject_, object baseValue_)
        {
            if (ShellModeExtension.GetStateForElement(HideLockUIGranularity.DisableViewMaximize) == WindowButtonState.None)
            {
                return false;
            }
            return baseValue_;
        }

        private static void OnAllowMaximizeChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            PaneContainer container = (PaneContainer) dependencyObject_;
            if (!container.AllowMaximize)
            {
                container.MaximizeButtonState = WindowButtonState.None;
                if (container.IsRoot && container.WindowState == WindowState.Maximized)
                {
                    container.WindowState = WindowState.Normal;
                }
            }
            else
            {
                if (container.IsRoot && !container.HideMaximize)
                {
                    container.MaximizeButtonState = WindowButtonState.Normal;
                }
            }
        }

        public bool AllowClose
        {
            get { return (bool)GetValue(AllowCloseProperty); }
            set { SetValue(AllowCloseProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AllowClose.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AllowCloseProperty =
            DependencyProperty.Register("AllowClose", typeof(bool), typeof(PaneContainer), new UIPropertyMetadata(true, OnAllowCloseChanged, CoerceAllowCloseValue));

        private static object CoerceAllowCloseValue(DependencyObject dependencyObject_, object baseValue_)
        {
            if (ShellModeExtension.GetStateForElement(HideLockUIGranularity.DisableViewClose) == WindowButtonState.None)
            {
                return false;
            }
            return baseValue_;
        }

        private static void OnAllowCloseChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            PaneContainer container = (PaneContainer)dependencyObject_;
            if (!container.AllowClose)
            {
                container.CloseButtonState = WindowButtonState.None; 
            }
            else
            {
                container.CloseButtonState = ShellModeExtension.GetStateForElement(HideLockUIGranularity.HideViewCloseButton);
            }
        }
         

        public bool IsOwnedByMainWindow
        {
            get { return (bool)GetValue(IsOwnedByMainWindowProperty); }
            set { SetValue(IsOwnedByMainWindowProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsOwnedByMainWindow.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsOwnedByMainWindowProperty =
            DependencyProperty.Register("IsOwnedByMainWindow", typeof(bool), typeof(PaneContainer), new UIPropertyMetadata(true));

        

        #endregion

        #region Public Methods
        public void ExecuteCommand(RoutedCommand command_)
        {
            if (command_ == PaneContainerCommands.HideHeader)
            {
                IsHeaderVisible = false;
            }
            else if (command_ == PaneContainerCommands.ShowHeader)
            {
                IsHeaderVisible = true;
            }
            else if (command_ == PaneContainerCommands.Minimize)
            {
                WindowState = WindowState.Minimized;
            }
            else if (command_ == PaneContainerCommands.Restore)
            {
                if (WindowState == WindowState.Minimized && this.MinimizedProxy != null)
                {
                    this.MinimizedProxy.Command.Execute(null);
                }
                else if (WindowStateResolved != WindowState.Minimized)
                { 
                    WindowState = WindowState.Normal;
                }
            }
            else if (command_ == PaneContainerCommands.Maximize)
            {
                WindowState = WindowState.Maximized;
            }
            else if (command_ == PaneContainerCommands.Lock)
            {
                IsLocked = true;

            }
            else if (command_ == PaneContainerCommands.Unlock)
            { 
                IsLocked = false; 
            }
            else if (command_ == PaneContainerCommands.Close)
            {
                Close();
            }
            else if (command_ == PaneContainerCommands.TearOff)
            {
                TearOff();
            }
            else if (command_ == PaneContainerCommands.Pin)
            {
                this.ActivePane.Topmost = true;
            }
            else if (command_ == PaneContainerCommands.Unpin)
            {
                this.ActivePane.Topmost = false;
            }
            else if (command_ == PaneContainerCommands.ChangeToFloatingOnly)
            {
                this.AllowDock = false;

            }
            else if (command_ == PaneContainerCommands.ChangeToDockable)
            {
                this.AllowDock = true;
            }
        }

        public void Close()
        {
            Close(false);
        }
         
        internal bool Close(bool emptyContainer_)
        {
            List<ContentPane> panes = this.GetAllPanes();
            ClosingEventArgs arg = new ClosingEventArgs(emptyContainer_);
            OnClosing(arg);
            if (arg.Cancel) return false;
            if (!emptyContainer_)
            {
                List<ContentPane> allChildPanes = this.GetAllPanes(true);
                foreach (var childPane in allChildPanes)
                {
                    if (!childPane.Close())
                    {
                        return false;
                    }
                }
            }

            var copy = ContainerCloseRequest;
            if (copy != null)
            {
                var handledArg = new HandledEventArgs(false);
                copy(this, handledArg);
                if (!handledArg.Handled) return false;
            }
            foreach (var pane in panes)
            {
                pane.OnClosed();
            }
            OnClosed();
            Dispose();
            return true;
        }


        public void MergePanes(PaneContainer secondPaneContainer_)
        { 
            List<ContentPaneWrapper> paneWrappers = secondPaneContainer_.paneWrapperHost.Items.Cast<ContentPaneWrapper>().ToList();
            if (paneWrappers.Count == 0) return;
            foreach (ContentPaneWrapper paneWrapper in paneWrappers)
            {
                bool oldSelected = paneWrapper.IsSelected || paneWrapper.IsSingleTab;
                secondPaneContainer_.paneWrapperHost.RemovePaneWrapper(paneWrapper);
                this.paneWrapperHost.AddPaneWrapper(paneWrapper);
                if (oldSelected)
                {
                    paneWrapper.IsSelected = true;
                }
                paneWrapper.Pane.LastDockLocation = DockLocation.Center;
            } 
            OnChildContainerChanged();
        }
          
        public void MergePane(ContentPane pane_)
        {
            if (pane_.Wrapper == null) return;
            if (pane_.Wrapper.Host == null) return;
            pane_.Wrapper.Host.RemovePaneWrapper(pane_.Wrapper); 
            this.paneWrapperHost.AddPaneWrapper(pane_.Wrapper);
            pane_.Wrapper.IsSelected = true;
            OnChildContainerChanged();
        }

        public void ChangeCaptionBarState(CaptionBarState state_)
        { 
            CaptionBar.SetState(captionBarControl, state_);
            foreach (FrameworkElement item in this.paneWrapperHost.Items)
            {
                CaptionBar.SetState(item, state_);
            }
        }
 
        public void HideDockButton()
        {
            if (paneWrapperHost.HeaderControl != null)
            { 
                paneWrapperHost.HeaderControl.DockButtonVisibility = Visibility.Collapsed;
            }

            foreach (ContentPaneWrapper tabItem in paneWrapperHost.Items)
            {
                tabItem.DockingGrid.HideDockButton();
            }
        }
        public void ShowDockButton()
        {
            if (paneWrapperHost.HeaderControl != null)
            {
                paneWrapperHost.HeaderControl.DockButtonVisibility = Visibility.Visible;
            } 
            foreach (ContentPaneWrapper tabItem in paneWrapperHost.Items)
            {
                tabItem.DockingGrid.ShowDockButton();
            }
        }

        public PaneContainer TearOff()
        {
            var floatingWindow = TearOffInternal();
            return floatingWindow != null ? floatingWindow.PaneContainer : null;
        }

        internal FloatingWindow TearOffInternal()
        {
            if (this.IsRoot) return null;
            if (SinglePane != null)
            {
                return SinglePane.TearOffInternal(false);
            }
            var floatingLocation = WPFHelper.GetPositionWithOffset(this.paneWrapperHost);
            DockingGrid grid = this.FindVisualParent<DockingGrid>();
            if (grid == null) return null;
            grid.Remove(this);
            var fw = CreateHostingWindow();
            if (this.LastFloatingSize.Width > 0 && this.LastFloatingSize.Height > 0)
            {
                this.PaneHeight = this.LastFloatingSize.Height;
                this.PaneWidth = this.LastFloatingSize.Width;
            }
            fw.Top = floatingLocation.Y;
            fw.Left = floatingLocation.X;
            DockService.SetLastPosition(fw, WPFHelper.GetMousePosition());
            fw.Show();
            return fw;
        }


        #endregion
        static PaneContainer()
        {
            //DefaultStyleKeyProperty.OverrideMetadata(typeof(PaneContainer), new FrameworkPropertyMetadata(typeof(PaneContainer)));
            WidthProperty.OverrideMetadata(typeof(PaneContainer), new FrameworkPropertyMetadata(OnWidthChanged, CoerceHeightValue));
            HeightProperty.OverrideMetadata(typeof(PaneContainer), new FrameworkPropertyMetadata(OnHeightChanged, CoerceWidthValue)); 
            
        }

        private static object CoerceHeightValue(DependencyObject dependencyObject_, object baseValue_)
        {
            PaneContainer container = (PaneContainer)dependencyObject_;
            double newValue = (double)baseValue_;
            if (newValue < container.MinHeight) return container.MinHeight;
            if (newValue > container.MaxHeight) return container.MaxHeight;
            return newValue;
        }

        private static void OnHeightChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            PaneContainer container = (PaneContainer)dependencyObject_;
            if (!container.syncingSize && container.IsRoot)
            {
                FloatingWindow w = Window.GetWindow(container) as FloatingWindow;
                if (w != null)
                {
                    w.InternalSyncSize(false, true);
                }
            }
        }
        private static object CoerceWidthValue(DependencyObject dependencyObject_, object baseValue_)
        {
            PaneContainer container = (PaneContainer)dependencyObject_;
            double newValue = (double)baseValue_;
            if (newValue < container.MinWidth) return container.MinWidth;
            if (newValue > container.MaxWidth) return container.MaxWidth;
            return newValue;
        }

        private static void OnWidthChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            PaneContainer container = (PaneContainer)dependencyObject_;
            if (!container.syncingSize && container.IsRoot)
            {
                FloatingWindow w = Window.GetWindow(container) as FloatingWindow;
                if (w != null)
                {
                    w.InternalSyncSize(true, false);
                }
            }
        }

     
        public PaneContainer()
        {
            InitializeComponent();
            PaneWidth = Double.NaN;
            PaneHeight = Double.NaN;
            foreach (var command in PaneContainerCommands.GetAllCommands())
            {
               this.CommandBindings.Add(new CommandBinding(command, ExecuteCommand, CanExecuteCommand)); 
            } 
            PreviewMouseMove += OnPaneMove;
            PreviewMouseUp += OnCaptionBarUnClick;
            PreviewMouseLeftButtonDown += PaneContainer_PreviewMouseLeftButtonDown; 
            MaximizeButtonState = WindowButtonState.None;
            MinimizeButtonState = ShellModeExtension.GetStateForElement(HideLockUIGranularity.HideViewMinimizeButton);
            CloseButtonState = ShellModeExtension.GetStateForElement(HideLockUIGranularity.HideViewCloseButton);
            AllowMaximize = ShellModeExtension.GetStateForElement(HideLockUIGranularity.DisableViewMaximize) ==
                            WindowButtonState.Normal;
            HideMaximize = ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.HideViewMaximizeButton);
            HideTearOff = ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.HideViewTearOffButton);
            TearOffButtonState = ShellModeExtension.GetStateForElement(HideLockUIGranularity.HideViewTearOffButton);
            LockButtonState = ShellModeExtension.GetStateForElement(HideLockUIGranularity.HideViewLockButton);
            AllowDrag = !ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.LockViewDragDrop);
            IsOwnedByMainWindow = WindowViewContainer.DefaultIsOwnedWindowValue;
           
        }
 

        public PaneContainer(ContentPane pane_)
            : this()
        { 
            this.AddPane(pane_);
            this.PaneHeight = pane_.PaneHeight;
            this.PaneWidth = pane_.PaneWidth;
            DockManager.Services.DockManager.ActiveContainerChanged += DockManager_ActiveContainerChanged;
        }

        void DockManager_ActiveContainerChanged(object sender, ActiveContainerChangedEventArgs e)
        {
            if (e.NewContainer != null && !e.NewContainer.AllowDockResolved)
            {
                this.DockButtonState = WindowButtonState.None;
            }
            else if (e.NewContainer == null || 
                (this != e.NewContainer && e.NewContainer.FindContainer(c_=>c_ == this) == null))
            {
                 this.DockButtonState = WindowButtonState.Normal;
            }
        }

        
        protected internal virtual FloatingWindow CreateHostingWindow()
        {
            var window = new FloatingWindow(this);
            this.IsRoot = true;
            var copy = FloatingWindowCreated;
            if (copy != null)
            {
                copy(this, new FloatingWindowEventArgs(window));
            }
            Services.DockManager.OnFloatingWindowCreated(window);
            return window;
        }
        public void Show()
        {
            CreateHostingWindow().Show();
        }

        public void ShowDialog()
        {
            CreateHostingWindow().ShowDialog();
        }
        //public override void OnApplyTemplate()
        //{
        //    headerControl = this.GetTemplateChild("PART_Header") as PaneContainerHeaderControl;
        //    paneWrapperHost = this.GetTemplateChild("PART_WrapperHost") as ContentPaneWrapperHost;
        //    if (singlePaneWrapper != null)
        //    { 
        //        paneWrapperHost.AddPaneWrapper(singlePaneWrapper);
        //    }
        //    captionBarControl = this.GetTemplateChild("PART_CaptionBar") as Border;
        //    captionBarControl.PreviewMouseDown += OnCaptionBarClick;
        //    base.OnApplyTemplate();
        //}

        #region Helper Methods
        internal MinimizedPaneContainer MinimizedProxy { get; set; }
 

        //todo implement
        private void CanExecuteCommand(object sender_, CanExecuteRoutedEventArgs canExecuteRoutedEventArgs_)
        {
            canExecuteRoutedEventArgs_.CanExecute = true;
            canExecuteRoutedEventArgs_.Handled = true;
        }

        private void ExecuteCommand(object sender_, ExecutedRoutedEventArgs executedRoutedEventArgs_)
        {
            ExecuteCommand(executedRoutedEventArgs_.Command as RoutedCommand);
        }

        internal void OnContainerDragMove(MouseEventArgs e_)
        {
            var copy = ContainerDragMove;
            if (copy != null)
            {
                copy(this, e_);
            }
        }
        internal void OnTitleBarClick(MouseButtonEventArgs e_)
        {
            var copy = TitleBarClick;
            if (copy != null)
            {
                copy(this, e_);
            } 
        }

        internal void OnTitleBarUnClick(MouseButtonEventArgs e_)
        {
            var copy = TitleBarUnClick;
            if (copy != null)
            {
                copy(this, e_);
            }

        }
        private Point initialPosistion;
        internal void OnTabClick(object sender_, MouseButtonEventArgs e_)
        { 
            // If window is locked you cannot drag it.
            if (!IsLocked && e_.LeftButton == MouseButtonState.Pressed)
            {
                HandleTabClick(e_, sender_ as ContentPaneWrapper);
            }
        }


        private void HandleTabClick(MouseButtonEventArgs e_, ContentPaneWrapper wrapper_)
        { 
            initialPosistion = WPFHelper.GetMousePosition();
            Services.DockManager.ActiveContainer = this;
            if (paneWrapperHost.Items.Count == 1)
            {
                if (ProcessDoubleClick(e_))
                {
                    return;
                }
                IsMoving = true;
                CaptureMouse(); 
                OnTitleBarClick(e_);
            }
            else
            {
                CaptureMouse(); 
                currentMovingPane = wrapper_.Pane;
            }
            e_.Handled = true;
        }

        public ContentPane SinglePane
        {
            get
            {
                return this.paneWrapperHost.Items.Count == 1
                           ? ((ContentPaneWrapper) paneWrapperHost.Items[0]).Pane
                           : null;
            }
        }

        public ContentPane ParentPane
        {
            get 
            { 
                if (this.IsRoot) return null;
                return  this.FindLogicalParent<ContentPane>();
            }
        }
        public WindowState WindowStateResolved
        {
            get
            {
                var thisContainer = this;
                while (thisContainer != null)
                {
                    if (thisContainer.WindowState == WindowState.Minimized) return WindowState.Minimized;
                    thisContainer = thisContainer.FindLogicalParent<PaneContainer>();
                }
                return this.WindowState;

            }
        } 

        public MinimizedPaneContainer MinimizedProxyResolved
        {
            get
            {
                var thisContainer = this;
                while (thisContainer != null)
                {
                    if (thisContainer.MinimizedProxy != null) return thisContainer.MinimizedProxy;
                    thisContainer = thisContainer.FindLogicalParent<PaneContainer>();
                }
                return null;
            }
        }

        private void UndockPane(ContentPane pane_)
        {
            currentMovingPane = null;
            IsMoving = false;
            var newPaneContainer = pane_.TearOff();
            if (newPaneContainer == null) return;
            newPaneContainer.IsMoving = true;
            ReleaseMouseCapture();
            newPaneContainer.CaptureMouse(); 
        }

        // called whenever window dragged
        protected virtual void OnPaneMove(object sender, MouseEventArgs e)
        { 
            // If window is locked you cannot drag it.
            if (IsLocked || !AllowDrag || Topmost) return;
            Point currentPosition = WPFHelper.GetMousePosition(); 
            Vector diff = new Vector(currentPosition.X - initialPosistion.X, currentPosition.Y - initialPosistion.Y);

            if (e.LeftButton == MouseButtonState.Pressed &&
                (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                  Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                if (currentMovingPane != null&& currentMovingPane.AllowTearOff)
                { 
                    UndockPane(currentMovingPane);
                }
                else if (IsMoving)
                { 
                    OnContainerDragMove(e);
                }
            }
        }

        protected void OnCaptionBarUnClick(object sender_, MouseButtonEventArgs e_)
        {
            if (this.IsLocked || !AllowDrag || Topmost)
            {
                if (IsMouseCaptured)
                    ReleaseMouseCapture();
                currentMovingPane = null; 
                IsMoving = false;
                return;
            }
            if (e_.LeftButton == MouseButtonState.Released)
            {
                if (IsMoving)
                {
                    OnTitleBarUnClick(e_);
                } 
                currentMovingPane = null;

                IsMoving = false;

                if (IsMouseCaptured)
                    ReleaseMouseCapture();
            }
            //when dragging pane with left mouse down, then click the right mouse, means tries to go one level down to search the candidate docking grid
            else if (e_.RightButton == MouseButtonState.Released && e_.LeftButton == MouseButtonState.Pressed)
            {
                // still dragging, so not changing IsMoving and IsTabMoving
                OnTitleBarUnClick(e_);
            }
        }


        internal bool SyncSizeSuspended = false;
        internal void SuspendSyncSize()
        {
            SyncSizeSuspended = true;
        }
        internal void ResumeSyncSize()
        {
            SyncSizeSuspended = false;
        }

        internal void PerformSyncSize()
        {
            SyncSizeSuspended = false;
            Host.UpdateActivePaneSize(); 
        }
        #endregion

        internal void OnChildContainerChanged()
        {
            var parentPane = this.ParentPane;
            if (parentPane != null)
            {
                parentPane.OnChildContainerChanged();
            }
        }
        public void AddPane(ContentPane pane_)
        { 
            this.Host.AddPaneWrapper(new ContentPaneWrapper(pane_));
            OnChildContainerChanged();
        }

        public IList<ContentPane> GetChildrenPanes(bool leafOnly_ = false)
        {
            return (from ContentPaneWrapper wrapper
                    in this.Host.Items
                where wrapper.Pane != null && (!leafOnly_ || wrapper.Pane.IsLeaf)
                select wrapper.Pane).ToList();
        }


        public IList<PaneContainer> GetChildrenContainers()
        {
            var containers = new List<PaneContainer>();
            foreach (ContentPaneWrapper contentPaneWrapper in this.Host.Items)
            {
                if (contentPaneWrapper.DockingGrid != null)
                {
                    containers.AddRange(contentPaneWrapper.DockingGrid.NormalContainers);
                    containers.AddRange(contentPaneWrapper.DockingGrid.MinimizedPaneContainers.Containers);
                }
            }
            return containers;
        }
        internal void OnClosing(ClosingEventArgs arg_)
        {
            var copy = Closing;
            if (copy != null)
            {
                copy(this, arg_);
            }
        }

        internal void OnClosed()
        {
            var copy = Closed;
            if (copy != null)
            {
                copy(this, EventArgs.Empty);
            }
        }
        public event EventHandler Closed;
        public event EventHandler<ClosingEventArgs> Closing;

        //special handling when Areo is disabled
        private void PaneContainer_PreviewMouseLeftButtonDown(object sender_, MouseButtonEventArgs e_)
        { 
            if (IsLocked) return; 
            Point pt = e_.GetPosition(captionBarControl);
            var hitInfo = System.Windows.Media.VisualTreeHelper.HitTest(this, pt);
            if (hitInfo == null || hitInfo.VisualHit == null) return;

            var hit = hitInfo.VisualHit as TabPanelEx;
            if (hit == null)
            {
                var scrollViewer = hitInfo.VisualHit as ScrollViewer;
                if (scrollViewer != null)
                {
                    hit = scrollViewer.Content as TabPanelEx;
                }
            }
            if (hit != null)
            {
                TabControlEx tab = hit.FindVisualParent<ContentPaneWrapperHost>();
                ContentPaneWrapperHost host = tab as ContentPaneWrapperHost;
                if (host == null) return;

                //check if this is the top pane
                if (!IsRoot)
                {
                    host.ParentContainer.HandleTabClick(e_, host.ActiveWrapper);
                }
                else
                {
                    host.ParentContainer.HandleMovePane(e_);
                }

                e_.Handled = true;
            }
        }

 
        internal void OnCaptionBarClick(object sender_, MouseButtonEventArgs e_)
        {
            if (IsLocked) return;
            HandleMovePane(e_);
            e_.Handled = true;
        }

        private void HandleMovePane(MouseButtonEventArgs e)
        {
            e.Handled = true;
            bool handled = ProcessDoubleClick(e);
            Services.DockManager.ActiveContainer = this;
            if (handled)
            {
                return;
            }
            this.IsMoving = true; 
            initialPosistion = WPFHelper.GetMousePosition();

            this.CaptureMouse();

            this.DockButtonState = WindowButtonState.None;
            this.OnTitleBarClick(e);
        }
         
        private bool ProcessDoubleClick(MouseButtonEventArgs e_)
        { 
            if (e_.ClickCount == 2 && AllowMaximize)
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.WindowState = WindowState.Normal;
                }
                else if (this.WindowState == WindowState.Normal && this.IsRoot)
                {
                    this.WindowState = WindowState.Maximized; 
                } 
                return true;
            }

            if (WindowState == WindowState.Minimized)
            {  
                WindowState = WindowState.Normal;
                return true;
            }

            return false;
        } 

        public bool IsRoot
        {
            get { return (bool)GetValue(IsRootProperty); }
            internal set { SetValue(IsRootPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey IsRootPropertyKey
        = DependencyProperty.RegisterReadOnly("IsRoot", typeof(bool), typeof(PaneContainer), new PropertyMetadata(false, OnIsRootChanged));

        private static void OnIsRootChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            PaneContainer container = (PaneContainer)dependencyObject_;
            container.paneWrapperHost.IsRoot =  container.IsRoot;
            if (container.IsRoot)
            {
                if (!container.HideMaximize && container.AllowMaximize && !container.IsLocked)
                {
                    container.MaximizeButtonState = WindowButtonState.Normal;
                }
                if (DockViewModel.EnableTopmost && !container.IsLocked)
                {
                     container.ToggleTopmostButtonState = WindowButtonState.Normal;
                }
                container.TearOffButtonState = WindowButtonState.None;
            }
            else
            {
                container.MaximizeButtonState = WindowButtonState.None;
                container.ToggleTopmostButtonState = WindowButtonState.None;
                if (!container.HideTearOff && container.AllowTearOff && !container.IsLocked)
                {
                    container.TearOffButtonState = WindowButtonState.Normal;
                }
            }
        }

        public static readonly DependencyProperty IsRootProperty = IsRootPropertyKey.DependencyProperty;
         

        public bool IsDockSource
        {
            get { return (bool)GetValue(IsDockSourceProperty); }
            internal set { SetValue(IsDockSourcePropertyKey, value); }
        }

        private static readonly DependencyPropertyKey IsDockSourcePropertyKey
        = DependencyProperty.RegisterReadOnly("IsDockSource", typeof(bool), typeof(PaneContainer), new PropertyMetadata(false, IsDockSourceChanged));

        private static void IsDockSourceChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            PaneContainer container = (PaneContainer)dependencyObject_;
            if (container.IsDockSource)
            {
                DockService.ShowAsDockSource(container); 
            }
            else
            {

               DockService.HideAsDockSource(container);
            }
        }

        public static readonly DependencyProperty IsDockSourceProperty
            = IsDockSourcePropertyKey.DependencyProperty;
         
        public bool IsDockTarget
        {
            get { return (bool)GetValue(IsDockTargetProperty); }
            internal set { SetValue(IsDockTargetPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey IsDockTargetPropertyKey
        = DependencyProperty.RegisterReadOnly("IsDockTarget", typeof(bool), typeof(PaneContainer), new PropertyMetadata(false, IsDockTargetChanged));

        private static void IsDockTargetChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            PaneContainer container = (PaneContainer)dependencyObject_;
            if (container.IsDockTarget)
            {
                DockService.ShowAsDockTarget(container);
            }
            else
            {

                DockService.HideAsDockTarget(container);
            }
        }


        public static readonly DependencyProperty IsDockTargetProperty
            = IsDockTargetPropertyKey.DependencyProperty;

        public void Dispose()
        {
             //todo: implement it
            Services.DockManager.ActiveContainerChanged -= DockManager_ActiveContainerChanged;
            this.Host.Dispose(); 
        }

        internal void RecordLastFloatingSize()
        {
            var singlePane = this.SinglePane;
            if (singlePane != null)
            {
                singlePane.LastFloatingSize = new Size(singlePane.ActualWidth, singlePane.ActualHeight);
                singlePane.LastDockContainer = null;
            }
            this.LastFloatingSize = new Size(this.ActualWidth, this.ActualHeight);
        }

        private bool syncingSize = false;
        internal void InternalSyncSize(bool updateWidth_, bool upateHeight_)
        {
            if (syncingSize) return;
            syncingSize = true;
            Window w = Window.GetWindow(this);
            if (w == null) return;
            if (updateWidth_)
            {
                this.Width = w.ActualWidth;
            }
            if (upateHeight_)
            {
                this.Height = w.ActualHeight;
            }

            syncingSize = false;
        }
         
    }
     
}
