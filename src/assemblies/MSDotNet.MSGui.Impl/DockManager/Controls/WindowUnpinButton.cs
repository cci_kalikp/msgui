﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class WindowUnpinButton: WindowButton
    {
        static WindowUnpinButton()
        {

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof (WindowUnpinButton),
                new FrameworkPropertyMetadata(typeof (WindowUnpinButton)));
        }

        public WindowUnpinButton()
        {
            this.Command = PaneContainerCommands.Unpin;
        }
    }
}
