﻿using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public static class CaptionBar
    {

        public static readonly DependencyProperty StateProperty =
            DependencyProperty.RegisterAttached("State", typeof(CaptionBarState), typeof(CaptionBar), new PropertyMetadata(CaptionBarState.Active, null, CoerceStateChanged));

        public static void SetState(UIElement element_, CaptionBarState value_)
        {
            element_.SetValue(StateProperty, value_);
        }

        public static CaptionBarState GetState(UIElement element_)
        {
            return (CaptionBarState) element_.GetValue(StateProperty);
        }
         

        private static object CoerceStateChanged(DependencyObject dependencyObject_, object baseValue_)
        {
            CaptionBarState newState = (CaptionBarState) baseValue_;
            CaptionBarState oldState = GetState(dependencyObject_ as UIElement);
            switch (newState)
            {
                case CaptionBarState.Unselected:
                    if (oldState != CaptionBarState.Grouped)
                    {
                        return newState; 
                    }
                    return oldState; 
                case CaptionBarState.Active:
                    if (oldState != CaptionBarState.Grouped)
                    {
                        return newState; 
                    }
                    return oldState;
                case CaptionBarState.Grouped:
                    return newState;
                case CaptionBarState.UnGrouped:
                    return CaptionBarState.Active; 
                default:
                    return newState; 
            }
        }
    }

    public enum CaptionBarState
    {
        Unselected,
        Active,
        Grouped, 
        UnGrouped,
    }
     
}
