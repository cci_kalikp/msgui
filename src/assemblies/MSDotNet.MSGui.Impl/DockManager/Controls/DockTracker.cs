﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Helpers;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class DockTracker : Window
    {
        static DockTracker()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
    typeof(DockTracker),
    new FrameworkPropertyMetadata(typeof(DockTracker)));

        }
        public DockTracker()
        {
            this.WindowStyle = WindowStyle.None;
            this.AllowsTransparency = true;
            this.ShowInTaskbar = false;
            this.IsVisibleChanged += DockTracker_IsVisibleChanged;
        }

        void DockTracker_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.IsVisible && this.Container != null)
            {
                Rect? bound = DockingUtils.CalculateAbsoluteBounds(this.Container);
                if (bound != null)
                {
                    this.Left = bound.Value.X;
                    this.Top = bound.Value.Y;
                    this.Height = bound.Value.Height;
                    this.Width = bound.Value.Width;
                }
            }
        }



        public bool IsTarget
        {
            get { return (bool)GetValue(IsTargetProperty); }
            set { SetValue(IsTargetProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsTarget.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsTargetProperty =
            DependencyProperty.Register("IsTarget", typeof(bool), typeof(DockTracker), new UIPropertyMetadata(false));


        public PaneContainer Container
        {
            get { return (PaneContainer)GetValue(ContainerProperty); }
            set { SetValue(ContainerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Container.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContainerProperty =
            DependencyProperty.Register("Container", typeof(PaneContainer), typeof(DockTracker), new UIPropertyMetadata(null));



    }
}
