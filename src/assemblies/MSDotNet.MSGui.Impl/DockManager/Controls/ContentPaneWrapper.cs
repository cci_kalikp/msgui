﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    [TemplatePart(Name = "PART_Close", Type = typeof(Button))]
    [TemplatePart(Name = "Border", Type = typeof(Border))]
    [TemplatePart(Name = "PART_Title", Type = typeof(EditableTextBlock))]
    public class ContentPaneWrapper : TabItemEx, IDisposable
    {
        public static CornerRadius TabItemBorderCornerRadius = new CornerRadius(5, 5, 0, 0); 

        static ContentPaneWrapper()
        {
            //This OverrideMetadata call tells the system that this element wants to provide a style that is different than its base class.
            //This style is defined in themes\generic.xaml
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ContentPaneWrapper),
                new FrameworkPropertyMetadata(typeof(ContentPaneWrapper))); 

        }

        protected override void OnToolTipOpening(ToolTipEventArgs e)
        {
            base.OnToolTipOpening(e);
            if (this.ToolTip == string.Empty)
            {
                e.Handled = true;
            }
        }
 

        public static readonly RoutedEvent CloseTabEvent =
            EventManager.RegisterRoutedEvent("CloseTab", RoutingStrategy.Direct,
                typeof(RoutedEventHandler), typeof(ContentPaneWrapper));

        public event RoutedEventHandler CloseTab
        {
            add { AddHandler(CloseTabEvent, value); }
            remove { RemoveHandler(CloseTabEvent, value); }
        }

        public event MouseButtonEventHandler ClickTab;
        private Button closeButton;
        private Border border;
        private EditableTextBlock title;
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if (closeButton != null)
            {
                closeButton.Click -= closeButton_Click;
            }
            closeButton = base.GetTemplateChild("PART_Close") as Button;
            if (closeButton != null)
            {
                closeButton.Click += closeButton_Click;
            }

            if (border != null)
            {
                border.MouseDown -= header_Click;
            }
            border = base.GetTemplateChild("Border") as Border;
            if (border != null)
            {
                border.MouseDown += header_Click;
            }
            title = base.GetTemplateChild("PART_Title") as EditableTextBlock;
        }


        public bool IsSingleTab
        {
            get { return (bool)GetValue(IsSingleTabProperty); }
            internal set { SetValue(IsSingleTabPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey IsSingleTabPropertyKey
        = DependencyProperty.RegisterReadOnly("IsSingleTab", typeof(bool), typeof(ContentPaneWrapper), new PropertyMetadata(true));

        public static readonly DependencyProperty IsSingleTabProperty
            = IsSingleTabPropertyKey.DependencyProperty;


        void closeButton_Click(object sender_, RoutedEventArgs e_)
        {
            this.RaiseEvent(new RoutedEventArgs(CloseTabEvent, this));
        }

        void header_Click(object sender_, MouseButtonEventArgs e_)
        {
            var copy = ClickTab;
            if (copy != null)
            {
                copy(this, e_);
            }
            this.IsSelected = true;
        }

        public ContentPaneWrapper()
        { 
        }


        public ContentPaneWrapper(ContentPane pane_)
            : this()
        {
            DockingGrid = new DockingGrid(this, pane_);
            this.Content = DockingGrid;
            this.Pane = pane_;
            this.Pane.Wrapper = this;
            this.SetBinding(VisibilityProperty, new Binding("Visibility") { Source = Pane, Mode = BindingMode.OneWay });

        }

        public TextTrimming TitleTextTrimming
        {
            get { return (TextTrimming)GetValue(TitleTextTrimmingProperty); }
            set { SetValue(TitleTextTrimmingProperty, value); }
        }

        public static readonly DependencyProperty TitleTextTrimmingProperty =
            DependencyProperty.Register("TitleTextTrimming", typeof(TextTrimming), typeof(ContentPaneWrapper), new UIPropertyMetadata(TextTrimming.None));

        internal DockingGrid DockingGrid { get; private set; }

        public ContentPaneWrapperHost Host
        {
            get { return (ContentPaneWrapperHost)GetValue(HostProperty); }
            internal set { SetValue(HostPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey HostPropertyKey
        = DependencyProperty.RegisterReadOnly("Host", typeof(ContentPaneWrapperHost), typeof(ContentPaneWrapper), new PropertyMetadata(null));

        public static readonly DependencyProperty HostProperty
            = HostPropertyKey.DependencyProperty;


        public ContentPane Pane
        {
            get { return (ContentPane)GetValue(PaneProperty); }
            internal set { SetValue(PanePropertyKey, value); }
        }

        private static readonly DependencyPropertyKey PanePropertyKey
        = DependencyProperty.RegisterReadOnly("Pane", typeof(ContentPane), typeof(ContentPaneWrapper), new PropertyMetadata(null, PaneChanged));

        public static readonly DependencyProperty PaneProperty
            = PanePropertyKey.DependencyProperty;

        private static void PaneChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            ContentPaneWrapper wrapper = (ContentPaneWrapper)dependencyObject_;
            if (wrapper != null)
            {
                ContentPane pane = dependencyPropertyChangedEventArgs_.NewValue as ContentPane;
                if (pane != null)
                {
                    pane.Wrapper = wrapper;
                    ContentPaneWrapperHost host = wrapper.Parent as ContentPaneWrapperHost;
                    if (host != null && host.ActiveWrapper == wrapper && host.ParentContainer != null)
                    {
                        host.ParentContainer.ActivePane = pane;
                    }
                } 
            }
        }
         
        /// <summary>
        /// OnMouseEnter, Create and Display a Tooltip
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseEnter(System.Windows.Input.MouseEventArgs e)
        {
            if ((ThemeExtensions.SubTabHeaderTooltipsEnabled || this.IsSingleTab) && this.Pane.HeaderTooltip != null)
            {
                ToolTipService.SetIsEnabled(this, true);
                this.ToolTip = Pane.HeaderTooltip; 
            }
            else if (title != null && FrameworkElementHelper.IsTrimmed(title))
            {
                ToolTipService.SetIsEnabled(this, true);
                this.ToolTip = title.Text;
            }
            else
            {
                this.ToolTip = string.Empty;
            }
            base.OnMouseEnter(e);
            e.Handled = true;
        }

        /// <summary>
        /// OnMouseLeave, remove the tooltip
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseLeave(System.Windows.Input.MouseEventArgs e)
        {
            base.OnMouseLeave(e);

            e.Handled = true;
        }
        public void Dispose()
        {
            //todo: implement it
            this.Pane.Dispose();
            this.DockingGrid.Dispose();

        }
    }
}
