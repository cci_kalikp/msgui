﻿using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class WindowNoHeaderButton : WindowButton
    {
     
        static WindowNoHeaderButton()
        {

            DefaultStyleKeyProperty.OverrideMetadata(
        typeof(WindowNoHeaderButton),
        new FrameworkPropertyMetadata(typeof(WindowNoHeaderButton)));
        }

        public WindowNoHeaderButton()
        {
            this.Command = PaneContainerCommands.HideHeader;
        }
    }
}
