﻿using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class WindowDockButton : WindowButton
    {

        static WindowDockButton()
        { 
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof (WindowDockButton),
                new FrameworkPropertyMetadata(typeof (WindowDockButton)));
        }
         
    }
}
