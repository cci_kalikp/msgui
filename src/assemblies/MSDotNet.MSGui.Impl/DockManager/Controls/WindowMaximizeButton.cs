﻿using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class WindowMaximizeButton : WindowButton
    { 
      static WindowMaximizeButton()
      {

          DefaultStyleKeyProperty.OverrideMetadata(
      typeof(WindowMaximizeButton),
      new FrameworkPropertyMetadata(typeof(WindowMaximizeButton)));
      }

        public WindowMaximizeButton()
        {
            this.Command = PaneContainerCommands.Maximize;
        }
    }
}
