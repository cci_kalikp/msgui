﻿using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class WindowRestoreButton : WindowButton
    { 
        static WindowRestoreButton()
        {

            DefaultStyleKeyProperty.OverrideMetadata(
        typeof(WindowRestoreButton),
        new FrameworkPropertyMetadata(typeof(WindowRestoreButton)));
        }

        public WindowRestoreButton()
        {
            this.Command = PaneContainerCommands.Restore;
        }
    }
}
