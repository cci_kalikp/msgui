﻿using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class WindowUnLockButton : WindowButton
    {
        static WindowUnLockButton()
        {

            DefaultStyleKeyProperty.OverrideMetadata(
        typeof(WindowUnLockButton),
        new FrameworkPropertyMetadata(typeof(WindowUnLockButton)));
        }
         

        public WindowUnLockButton()
        {
            this.Command = PaneContainerCommands.Unlock;
        }
    }
}
