﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Services;
using MorganStanley.MSDotNet.MSGui.Impl.Interop;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{

    [TemplatePart(Name = "PART_HintBlock", Type=typeof(DockingHintBlock))]
    public class DockingPlaceholder : Window
    {
        static DockingPlaceholder()
        {
            //This OverrideMetadata call tells the system that this element wants to provide a style that is different than its base class.
            //This style is defined in themes\generic.xaml
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DockingPlaceholder),
                new FrameworkPropertyMetadata(typeof(DockingPlaceholder)));
        }

        public DockingPlaceholder()
        {
            this.AllowsTransparency = true;
            this.ShowInTaskbar = false;
            this.WindowStyle = WindowStyle.None;
            this.IsVisibleChanged += new DependencyPropertyChangedEventHandler(DockingPlaceholder_IsVisibleChanged);
        }

        void DockingPlaceholder_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
             if (!this.IsVisible)
             {
                 Mouse.OverrideCursor = null;
             }
             else
             {
                 if (!this.IsValid)
                 {
                     Mouse.OverrideCursor = Cursors.No;
                 }
             }
        }
        public bool IsGlobalDocker
        {
            get { return (bool)GetValue(IsGlobalDockerProperty); }
            set { SetValue(IsGlobalDockerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsGlobalDocker.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsGlobalDockerProperty =
            DependencyProperty.Register("IsGlobalDocker", typeof(bool), typeof(DockingPlaceholder), new PropertyMetadata(false));

        private DockingHintBlock hintBlock;
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            hintBlock = GetTemplateChild("PART_HintBlock") as DockingHintBlock;
        }



        public bool IsValid
        {
            get { return (bool)GetValue(IsValidProperty); }
            set { SetValue(IsValidProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsValid.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsValidProperty =
            DependencyProperty.Register("IsValid", typeof(bool), typeof(DockingPlaceholder), new UIPropertyMetadata(true, (
                o_, args_) =>
                {
                    if (!Convert.ToBoolean(args_.NewValue))
                    { 
                        Mouse.OverrideCursor = Cursors.No;
                    }
                    else
                    {
                        Mouse.OverrideCursor = null;
                    }
                }));

 
        internal void SetPosition(DockingGrid grid_, PaneContainer sourceContainer_)
        {
            Rect? rect = DockingUtils.CalculateAbsoluteBounds(grid_);
            if (rect == null) return;
            Position(rect.Value); 
            this.Show();

            Point relativePos = WindowsFormIntegrationHelper.GetCurrentPosition(grid_);
            DockLocation location;
            if (relativePos.X < grid_.ActualWidth / 3.0)
            {
                if (relativePos.Y < grid_.ActualHeight / 3.0)
                    location = DockLocation.TopLeft;
                else if (relativePos.Y < grid_.ActualHeight * 2.0 / 3.0)
                    location = DockLocation.Left; 
                else
                    location = DockLocation.BottomLeft;
            }
            else if (relativePos.X < grid_.ActualWidth * 2.0 / 3.0)
            {
                if (relativePos.Y < grid_.ActualHeight / 3.0)
                    location = DockLocation.Top;
                else if (relativePos.Y < grid_.ActualHeight * 2.0 / 3.0)
                    location = DockLocation.Center;
                else
                    location = DockLocation.Bottom;
            }
            else
            {
                if (relativePos.Y < grid_.ActualWidth / 3.0)
                   location = DockLocation.TopRight;
                else if (relativePos.Y < grid_.ActualHeight * 2.0 / 3.0)
                   location = DockLocation.Right;
                else
                   location = DockLocation.Bottom;
            }
            IsValid = Services.DockManager.IsContainerDockingValid(sourceContainer_, grid_.ParentContainer, location);
            switch (location)
            {
                case DockLocation.TopLeft:
                    DockTopLeft(rect.Value);
                    break;
                case DockLocation.Top:
                    DockTop(rect.Value);
                    break;
                case DockLocation.TopRight:
                    DockTopRight(rect.Value);
                    break;
                case DockLocation.Left:
                    DockLeft(rect.Value);
                    break;
                case DockLocation.Center:
                    DockCenter(rect.Value);
                    break;
                case DockLocation.Right:
                    DockRight(rect.Value);
                    break;
                case DockLocation.BottomLeft:
                    DockBottomLeft(rect.Value);
                    break;
                case DockLocation.Bottom:
                    DockBottom(rect.Value);
                    break;
                case DockLocation.BottomRight:
                    DockBottomRight(rect.Value);
                    break;
            }
        }


        private void Position(Rect area_)
        {
            Left = area_.X;
            Top = area_.Y;
            Width = area_.Width;
            Height = area_.Height;
        }

        private void DockTop(Rect area_)
        {
            double width = area_.Width - BorderThickness.Left - BorderThickness.Right;
            double height = area_.Height - BorderThickness.Top - BorderThickness.Bottom;

            hintBlock.SetValue(Canvas.LeftProperty, 0.0);
            hintBlock.SetValue(Canvas.TopProperty, 0.0);

            hintBlock.Width = width;
            hintBlock.Height = height / 2;
            hintBlock.Dock = DockLocation.Top;

        }

        private void DockLeft(Rect area_)
        {

            double width = area_.Width - BorderThickness.Left - BorderThickness.Right;
            double height = area_.Height - BorderThickness.Top - BorderThickness.Bottom;

            hintBlock.SetValue(Canvas.LeftProperty, 0.0);
            hintBlock.SetValue(Canvas.TopProperty, 0.0);

            hintBlock.Width = width / 2;
            hintBlock.Height = height;
            hintBlock.Dock = DockLocation.Left;
        }

        private void DockRight(Rect area_)
        {

            double width = area_.Width - BorderThickness.Left - BorderThickness.Right;
            double height = area_.Height - BorderThickness.Top - BorderThickness.Bottom;

            hintBlock.SetValue(Canvas.LeftProperty, width * 0.5);
            hintBlock.SetValue(Canvas.TopProperty, 0.0);

            hintBlock.Width = width / 2;
            hintBlock.Height = height;
            hintBlock.Dock = DockLocation.Right;
        }

        private void DockBottom(Rect area_)
        {

            double width = area_.Width - BorderThickness.Left - BorderThickness.Right;
            double height = area_.Height - BorderThickness.Top - BorderThickness.Bottom;

            hintBlock.SetValue(Canvas.LeftProperty, 0.0);
            hintBlock.SetValue(Canvas.TopProperty, height * 0.5);

            hintBlock.Width = width;
            hintBlock.Height = height / 2;
            hintBlock.Dock = DockLocation.Bottom;
        }

        private void DockBottomLeft(Rect area_)
        {

            double width = area_.Width - BorderThickness.Left - BorderThickness.Right;
            double height = area_.Height - BorderThickness.Top - BorderThickness.Bottom;

            hintBlock.SetValue(Canvas.LeftProperty, 0.0);
            hintBlock.SetValue(Canvas.TopProperty, height * 0.5);

            hintBlock.Width = width / 2;
            hintBlock.Height = height / 2;
            hintBlock.Dock = DockLocation.BottomLeft;
        }

        private void DockBottomRight(Rect area_)
        {

            double width = area_.Width - BorderThickness.Left - BorderThickness.Right;
            double height = area_.Height - BorderThickness.Top - BorderThickness.Bottom;

            hintBlock.SetValue(Canvas.LeftProperty, width * 0.5);
            hintBlock.SetValue(Canvas.TopProperty, height * 0.5);

            hintBlock.Width = width / 2;
            hintBlock.Height = height / 2;
            hintBlock.Dock = DockLocation.BottomRight;
        }

        private void DockTopLeft(Rect area_)
        {

            double width = area_.Width - BorderThickness.Left - BorderThickness.Right;
            double height = area_.Height - BorderThickness.Top - BorderThickness.Bottom;

            hintBlock.SetValue(Canvas.LeftProperty, 0.0);
            hintBlock.SetValue(Canvas.TopProperty, 0.0);

            hintBlock.Width = width / 2;
            hintBlock.Height = height / 2;
            hintBlock.Dock = DockLocation.TopLeft;
        }

        private void DockTopRight(Rect area_)
        {
            double width = area_.Width - BorderThickness.Left - BorderThickness.Right;
            double height = area_.Height - BorderThickness.Top - BorderThickness.Bottom;

            hintBlock.SetValue(Canvas.LeftProperty, width * 0.5);
            hintBlock.SetValue(Canvas.TopProperty, 0.0);

            hintBlock.Width = width / 2;
            hintBlock.Height = height / 2;
            hintBlock.Dock = DockLocation.TopRight;
        }

        private void DockCenter(Rect area_)
        {
            double width = area_.Width - BorderThickness.Left - BorderThickness.Right;
            double height = area_.Height - BorderThickness.Top - BorderThickness.Bottom;

            hintBlock.SetValue(Canvas.LeftProperty, width * 0.3);
            hintBlock.SetValue(Canvas.TopProperty, height * 0.3);

            hintBlock.Width = width / 3;
            hintBlock.Height = height / 3;
            hintBlock.Dock = DockLocation.Center;
        }

    }
}
