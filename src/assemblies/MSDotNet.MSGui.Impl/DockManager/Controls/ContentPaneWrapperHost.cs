﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading; 
using MorganStanley.MSDotNet.MSGui.Impl.Interop;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls; 

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    [TemplatePart(Name = "PART_ScrollViewer", Type = typeof(ScrollViewer))]
    [TemplatePart(Name = "PART_CustomItemHolder", Type=typeof(HeaderItemsHolder))]
    public class ContentPaneWrapperHost : TabControlEx, IDisposable
    {
        public static Thickness HostPadding = new Thickness(1);
        public static Thickness HostBorderThickness = new Thickness(1);
        public static Thickness HeaderPanelMargin = new Thickness(2, 2, 2, 0);
        static ContentPaneWrapperHost()
        {
            //This OverrideMetadata call tells the system that this element wants to provide a style that is different than its base class.
            //This style is defined in themes\generic.xaml
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ContentPaneWrapperHost),
                new FrameworkPropertyMetadata(typeof(ContentPaneWrapperHost)));
        }
         
        public ContentPaneWrapperHost()
        {
            this.Loaded += ContentPaneWrapperHost_Loaded;
        }

        void ContentPaneWrapperHost_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ContentPaneWrapperHost_Loaded;
            this.SizeChanged += ContentPaneWrapperHost_SizeChanged;
        }

        void ContentPaneWrapperHost_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateActivePaneSize();
            if (customItemsHolder != null && this.customItemsHolder.ItemsSource != null)
            {
                foreach (var item in this.customItemsHolder.ItemsSource)
                {
                    FrameworkElement element = item as FrameworkElement;
                    if (element == null) continue;
                    WindowsFormIntegrationHelper.ClipWindowsFormsHostIfNeeded(element, this); 
                }  
            }
        }

        internal void UpdateActivePaneSize()
        {
            if (ParentContainer.SyncSizeSuspended || !this.IsLoaded || ActiveWrapper == null || ActiveWrapper.Pane == null) return;
            ActiveWrapper.Pane.PaneWidth = this.ParentContainer.ActualWidth;
            ActiveWrapper.Pane.PaneHeight = this.ParentContainer.ActualHeight;

        }

        private FrameworkElement envLabel;
        private PaneContainerHeaderControl headerControl;
        private HeaderItemsHolder customItemsHolder;
        private ContextMenu optionsMenu; 
        
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            if (envLabel != null && ParentContainer != null)
            {
                envLabel.PreviewMouseLeftButtonDown -= ParentContainer.OnCaptionBarClick;
            }
            envLabel = base.GetTemplateChild("ModernEnvLabel") as FrameworkElement;  
            if (headerControl != null)
            {
                headerControl.ButtonVisibilityChanged -= headerControl_ButtonVisibilityChanged;
            }
            headerControl = base.GetTemplateChild("HeaderControl") as PaneContainerHeaderControl;
            if (headerControl != null)
            {
                headerControl.IsRoot = this.IsRoot;
                headerControl.ButtonVisibilityChanged += headerControl_ButtonVisibilityChanged;
            }
            if (customItemsHolder != null)
            {
                DependencyPropertyDescriptor.FromProperty(BackgroundProperty, typeof(HeaderItemsHolder)).RemoveValueChanged(customItemsHolder, BackgroundChanged);
            }
            customItemsHolder = base.GetTemplateChild("PART_CustomItemHolder") as HeaderItemsHolder;
             
            if (optionsMenu != null)
            {
                optionsMenu.Opened -= new RoutedEventHandler(optionsMenu_Opened);
            }
            optionsMenu = base.GetTemplateChild("PART_OptionsMenu") as ContextMenu;
            if (optionsMenu != null)
            {
                optionsMenu.Opened += new RoutedEventHandler(optionsMenu_Opened); 
            }
            HookControls();
        }

        void optionsMenu_Opened(object sender_, RoutedEventArgs e_)
        {
            if (this.ActiveWrapper != null)
            {
                if (optionsMenu.Items.Count > 2)
                {
                    for (int i = optionsMenu.Items.Count - 1; i >= 2; i--)
                    {
                        optionsMenu.Items.RemoveAt(i);
                    }
                }

                this.ActiveWrapper.Pane.OnOptionsMenuOpening(optionsMenu);
            }
        }
         


        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                ContentPaneWrapper wrapper = e.AddedItems[0] as ContentPaneWrapper;
                if (wrapper != null)
                {
                    ActiveWrapper = wrapper;
                }
            }
            base.OnSelectionChanged(e);
        }

        internal PaneContainerHeaderControl HeaderControl { get { return headerControl; } }

        internal HeaderItemsHolder CustomItemsesHolder { get { return customItemsHolder; } }
        public PaneContainer ParentContainer
        {
            get { return (PaneContainer)GetValue(ParentContainerProperty); }
            set { SetValue(ParentContainerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ParentContainer.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ParentContainerProperty =
            DependencyProperty.Register("ParentContainer", typeof(PaneContainer), typeof(ContentPaneWrapperHost), new PropertyMetadata(null, OnParentContainerChanged));

        private static void OnParentContainerChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            ContentPaneWrapperHost host = (ContentPaneWrapperHost) dependencyObject_;
            if (host != null && host.ParentContainer != null)
            {
                host.IsRoot = host.ParentContainer.IsRoot; 
                if (host.envLabel != null)
                {
                    if (dependencyPropertyChangedEventArgs_.OldValue != null)
                    {
                        host.envLabel.PreviewMouseLeftButtonDown -= ((PaneContainer)dependencyPropertyChangedEventArgs_.OldValue).OnCaptionBarClick;
                    } 
                }
                host.HookControls();
            }
        }

        private void HookControls()
        {
            if (customItemsHolder != null && ParentContainer != null)
            {
                BindingOperations.ClearBinding(customItemsHolder, BackgroundProperty);
                BindingOperations.SetBinding(customItemsHolder, BackgroundProperty,
                                             new Binding("Background") {Source = ParentContainer.captionBarControl});
                DependencyPropertyDescriptor.FromProperty(BackgroundProperty, typeof(HeaderItemsHolder)).AddValueChanged(customItemsHolder, BackgroundChanged); 
                if (!FloatingWindowUtil.SeparateHeaderItemsInFloatingWindow)
                {
                    if (!this.IsRoot)
                    { 
                        customItemsHolder.Visibility = Visibility.Collapsed;
                    }
                    else
                    { 
                        customItemsHolder.ClearValue(VisibilityProperty);
                    }
                } 
            }
            if (envLabel != null && ParentContainer != null)
            {
                envLabel.PreviewMouseLeftButtonDown += ParentContainer.OnCaptionBarClick;
            }
        }

        internal void UpdateCustomItems()
        {
            //trigger the refresh of the custom items on parent container
            if (!FloatingWindowUtil.SeparateHeaderItemsInFloatingWindow)
            {
                var parentContainer = this.ParentContainer;
                this.ParentContainer = null;
                this.ParentContainer = parentContainer;
            }
        }
        void headerControl_ButtonVisibilityChanged(object sender, EventArgs e)
        {
            BackgroundChanged(this, e);
        }

        
        private void BackgroundChanged(object sender_, EventArgs eventArgs_)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (customItemsHolder != null && this.customItemsHolder.ItemsSource != null)
                {
                    foreach (var customItem in this.customItemsHolder.ItemsSource)
                    {
                        DependencyObject dpo = customItem as DependencyObject;
                        if (dpo != null)
                        {
                            WindowsFormIntegrationHelper.NotifyBackgroundChanged(dpo);
                        }
                    }
                }
            }), DispatcherPriority.Background);

        }
        public bool IsRoot
        {
            get { return (bool)GetValue(IsRootProperty); }
            internal set { SetValue(IsRootPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey IsRootPropertyKey
        = DependencyProperty.RegisterReadOnly("IsRoot", typeof(bool), typeof(ContentPaneWrapperHost), new PropertyMetadata(false, OnIsRootChanged));

        private static void OnIsRootChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            ContentPaneWrapperHost host = (ContentPaneWrapperHost) dependencyObject_;
            if (host != null)
            {
                if (host.headerControl != null)
                { 
                    host.headerControl.IsRoot = host.IsRoot;
                }
                if (host.customItemsHolder != null)
                {
                    if (!FloatingWindowUtil.SeparateHeaderItemsInFloatingWindow)
                    {
                        if (!host.IsRoot)
                        {
                            host.customItemsHolder.Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            host.customItemsHolder.ClearValue(VisibilityProperty);
                        }
                    } 
                }

            }
        }

        public static readonly DependencyProperty IsRootProperty = IsRootPropertyKey.DependencyProperty;

        public ContentPaneWrapper ActiveWrapper
        {
            get { return (ContentPaneWrapper)GetValue(ActiveWrapperProperty); }
            internal set { SetValue(ActiveWrapperPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey ActiveWrapperPropertyKey
        = DependencyProperty.RegisterReadOnly("ActiveWrapper", typeof(ContentPaneWrapper), typeof(ContentPaneWrapperHost), new PropertyMetadata(null, ActiveWrapperChanged));

        public static readonly DependencyProperty ActiveWrapperProperty
            = ActiveWrapperPropertyKey.DependencyProperty;


        private static void ActiveWrapperChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            ContentPaneWrapperHost host = (ContentPaneWrapperHost)dependencyObject_;
            if (host.ParentContainer != null)
            {
                if (host.ActiveWrapper != null)
                {
                    if (host.IsLoaded)
                    {
                        host.UpdateActivePaneSize();
                    }
                    host.ActiveWrapper.IsSelected = true;
                }
                if (dependencyPropertyChangedEventArgs_.NewValue == null)
                {
                    host.ParentContainer.ActivePane = null;
                }
                else
                {
                    host.ParentContainer.ActivePane = ((ContentPaneWrapper)dependencyPropertyChangedEventArgs_.NewValue).Pane;
                }
            }
        }


        public void AddPaneWrapper(ContentPaneWrapper paneContainer_)
        {
            paneContainer_.Host = this; 
            paneContainer_.CloseTab += OnTabClose;
            paneContainer_.ClickTab += ParentContainer.OnTabClick;
            this.Items.Add(paneContainer_); 
            UpdateTabItems();
             
        }


        public void RemovePaneWrapper(ContentPaneWrapper paneContainer_)
        {
            paneContainer_.CloseTab -= OnTabClose;
            paneContainer_.ClickTab -= ParentContainer.OnTabClick; 
            this.Items.Remove(paneContainer_);
            if (this.ActiveWrapper == paneContainer_)
            {
                this.ActiveWrapper = null;
            }
            paneContainer_.Pane.PaneLocation = PaneLocation.Unknown;
            UpdateTabItems();

        }

        internal void UpdateTabItems()
        {
            bool singleTab = Items.Count < 2;
            bool showInTaskBar = false;
            bool topmost = false;
            bool canMaxmize = false;
            bool canClose = false;
            bool allowTearOff = false;
            bool isHeaderVisible = true;
            bool isOwnedByMainWindow = false;
            foreach (ContentPaneWrapper item in Items)
            {
                if (item.Pane != null)
                {
                    if (item.Pane.ShowInTaskbar)
                    {
                        showInTaskBar = true; 
                    }
                    if (item.Pane.Topmost)
                    {
                        topmost = true;
                    }
                    if (item.Pane.AllowMaxmize)
                    {
                        canMaxmize = true;
                    }
                    if (item.Pane.AllowClose)
                    {
                        canClose = true;
                    }
                    if (item.Pane.AllowTearOff)
                    {
                        allowTearOff = true;
                    } 
                    if (!item.Pane.IsHeaderVisible)
                    {
                        isHeaderVisible = false;
                    }
                    if (item.Pane.IsOwnedByMainWindow)
                    {
                        isOwnedByMainWindow = true;
                    }
                }

                item.IsSingleTab = singleTab;
                if (singleTab || item.IsSelected)
                {
                    ActiveWrapper = item;
                }
                if (item.Pane != null && !singleTab)
                {
                    item.Pane.PaneLocation = PaneLocation.DockedTabbed;        
                }

            }
            if (singleTab)
            {
                this.BorderThickness = new Thickness(HostBorderThickness.Left, 0, HostBorderThickness.Right, HostBorderThickness.Bottom);
            }
            else
            {
                this.BorderThickness = HostBorderThickness;
            }
            this.ParentContainer.ShowInTaskbar = showInTaskBar;
            this.ParentContainer.Topmost = topmost;
            this.ParentContainer.AllowMaximize = canMaxmize;
            this.ParentContainer.AllowClose = canClose;
            this.ParentContainer.AllowTearOff = allowTearOff;
            this.ParentContainer.IsHeaderVisible = isHeaderVisible;
            this.ParentContainer.IsOwnedByMainWindow = isOwnedByMainWindow;
        }


        internal bool Close(ContentPaneWrapper wrapper_, bool emptyWrapper_)
        {
            if (wrapper_ == null || !Items.Contains(wrapper_)
                //|| ParentContainer.IsLocked
                ) return true;
            ClosingEventArgs arg = new ClosingEventArgs(emptyWrapper_);
            wrapper_.Pane.OnClosing(arg);
            if (arg.Cancel) return false;
            RemovePaneWrapper(wrapper_);
            wrapper_.Pane.OnClosed();
            wrapper_.Dispose();

            //closed the last pane
            if (this.Items.Count == 0)
            {
                ParentContainer.Close(emptyWrapper_);
            }
            return true;
        }

        private void OnTabClose(object source_, RoutedEventArgs args_)
        {
            Close(args_.OriginalSource as ContentPaneWrapper, false);
        }


        public void Dispose()
        {
            //todo: implement it
            foreach (ContentPaneWrapper contentPaneWrapper in this.Items)
            {
                contentPaneWrapper.Dispose();
            }
        }
    }
}
