﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class WindowTearOffButton: WindowButton
    { 
        static WindowTearOffButton()
        { 
            DefaultStyleKeyProperty.OverrideMetadata(typeof(WindowTearOffButton), new FrameworkPropertyMetadata(typeof(WindowTearOffButton)));
        }

        public WindowTearOffButton()
        {
            this.Command = PaneContainerCommands.TearOff;
        }
    }
}
