﻿using System.Collections.Generic; 
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class LayoutGrid:Grid
    { 
        public void Clear()
        {
            foreach (var child in this.Children)
            {
                LayoutGrid childGrid = child as LayoutGrid;
                if (childGrid != null)
                {
                    childGrid.Clear(); 
                }  
            }
            this.Children.Clear();
            this.ColumnDefinitions.Clear();
            this.RowDefinitions.Clear();
            Splitter = null;
            firstGrid = null;
            secondGrid = null;
            directGrid = null;
        }
 
        private readonly DockingGrid dockingGrid;
        internal LayoutGrid(DockingGrid dockingGrid_)
        {
            this.dockingGrid = dockingGrid_; 
        }

        private readonly SplitPanes splitPanes;
        private readonly bool? left;
        private readonly bool? top;
        public LayoutGrid(SplitPanes splitPanes_, bool? left_, bool? top_)
        {
            this.splitPanes = splitPanes_;
            this.left = left_;
            this.top = top_;
        }

        private LayoutGrid firstGrid;
        public LayoutGrid FirstGrid
        {
            get { return firstGrid; }
        }

        private LayoutGrid secondGrid;
        public LayoutGrid SecondGrid
        {
            get { return secondGrid; }
        }
        public GridSplitter Splitter { get; private set; }

        private LayoutGrid directGrid;
        public LayoutGrid DirectGrid
        {
            get { return directGrid; }
        }
        public void ArrangeLayout(bool recordLastDockLocation_)
        {
            Clear();
            if (dockingGrid != null && dockingGrid.MinimizedPaneContainers.Children.Count > 0)
            {
                LayoutGrid oldLayoutGrid = dockingGrid.MinimizedPaneContainers.Parent as LayoutGrid;
                if (oldLayoutGrid != null) oldLayoutGrid.Children.Remove(dockingGrid.MinimizedPaneContainers);
                if (dockingGrid.Root != null)
                {
                    this.RowDefinitions.Add(new RowDefinition());
                    RowDefinition row = new RowDefinition();
                    row.Height = GridLength.Auto;
                    this.RowDefinitions.Add(row);
                    this.ColumnDefinitions.Add(new ColumnDefinition());

                    LayoutGrid topGrid = new LayoutGrid(dockingGrid.Root, null, null); 
                    topGrid.ArrangeLayout(recordLastDockLocation_);
                    topGrid.SetValue(Grid.RowProperty, 0);
                    dockingGrid.MinimizedPaneContainers.SetValue(Grid.RowProperty, 1);

                    this.Children.Add(topGrid);
                    this.Children.Add(dockingGrid.MinimizedPaneContainers);
                }
                else
                {

                    RowDefinition row = new RowDefinition();
                    row.Height = GridLength.Auto;
                    this.RowDefinitions.Add(row);
                    this.ColumnDefinitions.Add(new ColumnDefinition());
                    this.Children.Add(dockingGrid.MinimizedPaneContainers);

                }
                return;
            }

            var panes = dockingGrid != null ? dockingGrid.Root : this.splitPanes;
            if (panes == null) return;

            var visibleFirst = panes.VisibleFirst;
            var visibleSecond = panes.VisibleSecond;
            List<PaneContainer> visibleContainers = panes.GetVisibleContainers();
            if (visibleContainers.Count == 1) //already leaf
            {
                var container = visibleContainers[0];
                this.Children.Add(container);
                if (recordLastDockLocation_)
                {
                    var singlePane = container.SinglePane;
                    if (singlePane != null)
                    {
                        if (left != null && top != null)
                        {
                            if (left.Value && top.Value)
                            {
                                singlePane.LastDockLocation = DockLocation.TopLeft;
                            }
                            else if (left.Value)
                            {
                                singlePane.LastDockLocation = DockLocation.BottomLeft;
                            }
                            else if (top.Value)
                            {
                                singlePane.LastDockLocation = DockLocation.TopRight;
                            }
                            else
                            {
                                singlePane.LastDockLocation = DockLocation.BottomRight;
                            }
                        }
                        else if (left != null)
                        {
                            singlePane.LastDockLocation = left.Value ? DockLocation.Left : DockLocation.Right;
                        }
                        else if (top != null)
                        {
                            singlePane.LastDockLocation = top.Value ? DockLocation.Top : DockLocation.Bottom;
                        }
                    }

                }
            }
            else if (visibleFirst == null || visibleSecond == null)
            {
                bool first = visibleFirst != null;

                bool? leftLocal = panes.Orientation == Orientation.Horizontal ? first : this.left;
                bool? topLocal = panes.Orientation == Orientation.Vertical ? first : this.top;
                directGrid = new LayoutGrid(visibleFirst ?? visibleSecond, leftLocal, topLocal);
                directGrid.ArrangeLayout(recordLastDockLocation_);
                this.Children.Add(directGrid);
            }
            else if (panes.Orientation == Orientation.Horizontal)
            {
                RowDefinitions.Add(new RowDefinition());
                var firstColumn = new ColumnDefinition();
                firstColumn.SetBinding(ColumnDefinition.WidthProperty,
                                       new Binding("FirstLength") { Source = panes, Mode = BindingMode.TwoWay });
                ColumnDefinitions.Add(firstColumn);
                ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
                var secondColumn = new ColumnDefinition();
                secondColumn.SetBinding(ColumnDefinition.WidthProperty,
                                        new Binding("SecondLength") { Source = panes, Mode = BindingMode.TwoWay });
                ColumnDefinitions.Add(secondColumn);

                firstGrid = new LayoutGrid(visibleFirst, true, this.top);
                firstGrid.ArrangeLayout(recordLastDockLocation_);
                secondGrid = new LayoutGrid(visibleSecond, false, this.top);
                secondGrid.ArrangeLayout(recordLastDockLocation_);

                firstGrid.SetValue(Grid.ColumnProperty, 0);
                secondGrid.SetValue(Grid.ColumnProperty, 2);

                Children.Add(firstGrid);
                Children.Add(secondGrid);

                Splitter = CreateSplitter(Orientation.Horizontal);
                Children.Add(Splitter);
            }
            else
            {
                var firstRow = new RowDefinition();
                firstRow.SetBinding(RowDefinition.HeightProperty,
                                    new Binding("FirstLength") { Source = panes, Mode = BindingMode.TwoWay });
                RowDefinitions.Add(firstRow);
                RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
                var secondRow = new RowDefinition();
                secondRow.SetBinding(RowDefinition.HeightProperty,
                                     new Binding("SecondLength") { Source = panes, Mode = BindingMode.TwoWay });
                RowDefinitions.Add(secondRow);
                ColumnDefinitions.Add(new ColumnDefinition());

                firstGrid = new LayoutGrid(visibleFirst, this.left, true);
                firstGrid.ArrangeLayout(recordLastDockLocation_);
                secondGrid = new LayoutGrid(visibleSecond, this.left, false);
                secondGrid.ArrangeLayout(recordLastDockLocation_);
                firstGrid.SetValue(Grid.RowProperty, 0);
                secondGrid.SetValue(Grid.RowProperty, 2);

                Children.Add(firstGrid);
                Children.Add(secondGrid);

                Splitter = CreateSplitter(Orientation.Vertical);
                Children.Add(Splitter);
            }
        }

         private GridSplitter CreateSplitter(Orientation orientation_)
         {
             var splitter = new GridSplitter
             {
                 Background = Brushes.Transparent,
                 ResizeBehavior = GridResizeBehavior.PreviousAndNext
             };
             splitter.SetBinding(WidthProperty, new Binding("SpacingSize") { Source = DataContext, FallbackValue = 4 });
            if (orientation_ == Orientation.Horizontal)
            {

                splitter.HorizontalAlignment = HorizontalAlignment.Right;
                splitter.VerticalAlignment = VerticalAlignment.Stretch;
                splitter.SetValue(Grid.ColumnProperty, 1);
            }
            else
            {
                splitter.HorizontalAlignment = HorizontalAlignment.Stretch;
                splitter.VerticalAlignment = VerticalAlignment.Bottom;
                splitter.SetValue(Grid.RowProperty, 1);
            }
            return splitter;
         }
    }


}
