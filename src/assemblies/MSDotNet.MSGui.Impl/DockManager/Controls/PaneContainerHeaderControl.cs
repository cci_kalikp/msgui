﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives; 
using MorganStanley.MSDotNet.MSGui.Controls.Controls; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Services;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls; 

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{ 
    //todo: rewrite it
    [TemplatePart(Name = "PART_EnvLabel", Type = typeof(EnvironmentLabel))]
    public class PaneContainerHeaderControl : ContentControl
    {
        static PaneContainerHeaderControl()
        {
            //This OverrideMetadata call tells the system that this element wants to provide a style that is different than its base class.
            //This style is defined in themes\generic.xaml
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PaneContainerHeaderControl),
                new FrameworkPropertyMetadata(typeof(PaneContainerHeaderControl)));
        }
 
        private DockButtonPopup dockPopUp;
        private Button dockButton; 
        private EnvironmentLabel envLabel;
        private Panel buttonPanel;
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate(); 

            if (envLabel != null)
            {
                envLabel.PreviewMouseLeftButtonDown -= EnvLabel_OnPreviewMouseLeftButtonDown; 
            }
            envLabel = this.GetTemplateChild("PART_EnvLabel") as EnvironmentLabel; 
            if (envLabel != null)
            {
                DockViewModel model = DataContext as DockViewModel;
                if (model == null || !model.EnvironmentSpecified)
                {
                    envLabel.Visibility = Visibility.Collapsed;
                }
                envLabel.PreviewMouseLeftButtonDown += EnvLabel_OnPreviewMouseLeftButtonDown;
            } 
 
            if (dockButton != null)
            {
                dockButton.Click -= OnButtonDock_Click; 
            }
            dockButton = this.GetTemplateChild("DockButton") as Button;
            if (dockButton != null)
            { 
                dockButton.Click += OnButtonDock_Click; 
            } 

            if (dockPopUp != null)
            {
                dockPopUp.Closed -= dockPopUp_Closed;
                dockPopUp.DockLocationSelected -= dockPopUp_DockLocationSelected;
                dockPopUp = null;
            }
            if (dockButton != null)
            {
                dockPopUp = new DockButtonPopup()
                {
                    StaysOpen = false,
                    Placement = PlacementMode.Left,
                    PlacementTarget = dockButton
                };
                dockPopUp.Closed += dockPopUp_Closed;
                dockPopUp.DockLocationSelected += dockPopUp_DockLocationSelected;
            } 
            if (buttonPanel != null)
            {
                foreach (var customItem in buttonPanel.Children)
                {
                    FrameworkElement element = customItem as FrameworkElement;
                    if (element != null)
                    {
                        element.IsVisibleChanged -= element_IsVisibleChanged;
                    }
                }
            }
            buttonPanel = this.GetTemplateChild("ButtonPanel") as Panel;
            if (buttonPanel != null)
            {
                foreach (var customItem in buttonPanel.Children)
                {
                    FrameworkElement element = customItem as FrameworkElement;
                    if (element != null)
                    {
                        element.IsVisibleChanged += element_IsVisibleChanged;
                    }
                }
            }

        }

        public event EventHandler ButtonVisibilityChanged;
        void element_IsVisibleChanged(object sender_, DependencyPropertyChangedEventArgs e_)
        {
            var copy = ButtonVisibilityChanged;
            if (copy != null)
            {
                copy(this, EventArgs.Empty);
            }
        }
         

        

        public bool IsRoot
        {
            get { return (bool)GetValue(IsRootProperty); }
            internal set { SetValue(IsRootPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey IsRootPropertyKey
        = DependencyProperty.RegisterReadOnly("IsRoot", typeof(bool), typeof(PaneContainerHeaderControl), new PropertyMetadata(false));

        public static readonly DependencyProperty IsRootProperty = IsRootPropertyKey.DependencyProperty;


        public PaneContainer PaneContainer
        {
            get { return (PaneContainer)GetValue(PaneContainerProperty); }
            set { SetValue(PaneContainerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Pane.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PaneContainerProperty =
            DependencyProperty.Register("PaneContainer", typeof(PaneContainer), typeof(PaneContainerHeaderControl), new PropertyMetadata(OnPaneContainerChanged));

        private static void OnPaneContainerChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            var header = (PaneContainerHeaderControl) dependencyObject_;
            var oldContainer = dependencyPropertyChangedEventArgs_.OldValue as PaneContainer;
            if (oldContainer != null)
            {
                oldContainer.ContainerIsLockedChanged -= header.PaneContainerIsLockedChanged;
                oldContainer.ContainerStateChanged -= header.PaneContainerContainerStateChanged;
                oldContainer.ContainerHeaderVisibleChanged -= header.PaneContainerContainerHeaderVisibleChanged;
                oldContainer.TopmostChanged -= header.PaneContainerTopmostChanged;
                oldContainer.ActivePaneChanged -= header.PaneContainerActivePaneChanged;
            }
            if (header.PaneContainer == null) return;  
            header.PaneContainerIsLockedChanged(header.PaneContainer, null);
            header.PaneContainerContainerHeaderVisibleChanged(header.PaneContainer, null);
            header.PaneContainerTopmostChanged(header.PaneContainer, null);
            header.BindCustomItem();
            header.PaneContainer.ContainerIsLockedChanged += header.PaneContainerIsLockedChanged;
            header.PaneContainer.ContainerStateChanged += header.PaneContainerContainerStateChanged;
            header.PaneContainer.ContainerHeaderVisibleChanged += header.PaneContainerContainerHeaderVisibleChanged;
            header.PaneContainer.ActivePaneChanged += header.PaneContainerActivePaneChanged;
            header.PaneContainer.TopmostChanged += header.PaneContainerTopmostChanged;
        }


        public Visibility DockButtonVisibility
        {
            get { return (Visibility)GetValue(DockButtonVisibilityProperty); }
            internal set { SetValue(DockButtonVisibilityPropertyKey, value); }
        }
         
        private static readonly DependencyPropertyKey DockButtonVisibilityPropertyKey
        = DependencyProperty.RegisterReadOnly("DockButtonVisibility", typeof(Visibility), typeof(PaneContainerHeaderControl), new PropertyMetadata(Visibility.Visible));

        public static readonly DependencyProperty DockButtonVisibilityProperty = DockButtonVisibilityPropertyKey.DependencyProperty;

        public Visibility LockButtonVisibility
        {
            get { return (Visibility)GetValue(LockButtonVisibilityProperty); }
            internal set { SetValue(LockButtonVisibilityPropertyKey, value); }
        }
         
        private static readonly DependencyPropertyKey LockButtonVisibilityPropertyKey
        = DependencyProperty.RegisterReadOnly("LockButtonVisibility", typeof(Visibility), typeof(PaneContainerHeaderControl), new PropertyMetadata(Visibility.Visible));

        public static readonly DependencyProperty LockButtonVisibilityProperty = LockButtonVisibilityPropertyKey.DependencyProperty;


        public Visibility UnlockButtonVisibility
        {
            get { return (Visibility)GetValue(UnlockButtonVisibilityProperty); }
            internal set { SetValue(UnlockButtonVisibilityPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey UnlockButtonVisibilityPropertyKey
        = DependencyProperty.RegisterReadOnly("UnlockButtonVisibility", typeof(Visibility), typeof(PaneContainerHeaderControl), new PropertyMetadata(Visibility.Collapsed));

        public static readonly DependencyProperty UnlockButtonVisibilityProperty = UnlockButtonVisibilityPropertyKey.DependencyProperty;

        public Visibility HideHeaderButtonVisibility
        {
            get { return (Visibility)GetValue(HideHeaderButtonVisibilityProperty); }
            internal set { SetValue(HideHeaderButtonVisibilityPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey HideHeaderButtonVisibilityPropertyKey
        = DependencyProperty.RegisterReadOnly("HideHeaderButtonVisibility", typeof(Visibility), typeof(PaneContainerHeaderControl), new PropertyMetadata(Visibility.Collapsed));

        public static readonly DependencyProperty HideHeaderButtonVisibilityProperty = HideHeaderButtonVisibilityPropertyKey.DependencyProperty;


        public Visibility ShowHeaderButtonVisibility
        {
            get { return (Visibility)GetValue(ShowHeaderButtonVisibilityProperty); }
            internal set { SetValue(ShowHeaderButtonVisibilityPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey ShowHeaderButtonVisibilityPropertyKey
        = DependencyProperty.RegisterReadOnly("ShowHeaderButtonVisibility", typeof(Visibility), typeof(PaneContainerHeaderControl), new PropertyMetadata(Visibility.Collapsed));

        public static readonly DependencyProperty ShowHeaderButtonVisibilityProperty = ShowHeaderButtonVisibilityPropertyKey.DependencyProperty;


        public Visibility RestoreButtonVisibility
        {
            get { return (Visibility)GetValue(RestoreButtonVisibilityProperty); }
            internal set { SetValue(RestoreButtonVisibilityPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey RestoreButtonVisibilityPropertyKey
        = DependencyProperty.RegisterReadOnly("RestoreButtonVisibility", typeof(Visibility), typeof(PaneContainerHeaderControl), new PropertyMetadata(Visibility.Collapsed));

        public static readonly DependencyProperty RestoreButtonVisibilityProperty = RestoreButtonVisibilityPropertyKey.DependencyProperty;


        public Visibility MaximizeButtonVisibility
        {
            get { return (Visibility)GetValue(MaximizeButtonVisibilityProperty); }
            internal set { SetValue(MaximizeButtonVisibilityPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey MaximizeButtonVisibilityPropertyKey
        = DependencyProperty.RegisterReadOnly("MaximizeButtonVisibility", typeof(Visibility), typeof(PaneContainerHeaderControl), new PropertyMetadata(Visibility.Visible));

        public static readonly DependencyProperty MaximizeButtonVisibilityProperty = MaximizeButtonVisibilityPropertyKey.DependencyProperty;

        public Visibility PinButtonVisibility
        {
            get { return (Visibility)GetValue(PinButtonVisibilityProperty); }
            internal set { SetValue(PinButtonVisibilityPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey PinButtonVisibilityPropertyKey
        = DependencyProperty.RegisterReadOnly("PinButtonVisibility", typeof(Visibility), typeof(PaneContainerHeaderControl), new PropertyMetadata(Visibility.Visible));

        public static readonly DependencyProperty PinButtonVisibilityProperty = PinButtonVisibilityPropertyKey.DependencyProperty;


        public Visibility UnpinButtonVisibility
        {
            get { return (Visibility)GetValue(UnpinButtonVisibilityProperty); }
            internal set { SetValue(UnpinButtonVisibilityPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey UnpinButtonVisibilityPropertyKey
        = DependencyProperty.RegisterReadOnly("UnpinButtonVisibility", typeof(Visibility), typeof(PaneContainerHeaderControl), new PropertyMetadata(Visibility.Collapsed));

        public static readonly DependencyProperty UnpinButtonVisibilityProperty = UnpinButtonVisibilityPropertyKey.DependencyProperty;

        public Visibility TearOffButtonVisibility
        {
            get { return (Visibility)GetValue(TearOffButtonVisibilityProperty); }
            internal set { SetValue(TearOffButtonVisibilityPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey TearOffButtonVisibilityPropertyKey
        = DependencyProperty.RegisterReadOnly("TearOffButtonVisibility", typeof(Visibility), typeof(PaneContainerHeaderControl), new PropertyMetadata(Visibility.Visible));

        public static readonly DependencyProperty TearOffButtonVisibilityProperty = UnpinButtonVisibilityPropertyKey.DependencyProperty;

        internal ObservableCollection<object> CustomItems
        {
            get { return (ObservableCollection<object>)GetValue(CustomItemsProperty); }
            private set { SetValue(CustomItemsPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey CustomItemsPropertyKey
        = DependencyProperty.RegisterReadOnly("CustomItems", typeof(ObservableCollection<object>), typeof(PaneContainerHeaderControl), new PropertyMetadata(null));

        public static readonly DependencyProperty CustomItemsProperty = CustomItemsPropertyKey.DependencyProperty;

        void PaneContainerTopmostChanged(object sender_, EventArgs e_)
        {
            if (PaneContainer.Topmost)
            {
                PinButtonVisibility = Visibility.Collapsed;
                UnpinButtonVisibility = Visibility.Visible;
                TearOffButtonVisibility = Visibility.Collapsed;
            }
            else
            {
                PinButtonVisibility = Visibility.Visible;
                UnpinButtonVisibility = Visibility.Collapsed;
                TearOffButtonVisibility = Visibility.Visible;
            }
        }

        void PaneContainerContainerHeaderVisibleChanged(object sender_, EventArgs e_)
        {
            if (PaneContainer.HideButtonState == WindowButtonState.None) return;
            if (PaneContainer.IsHeaderVisible)
            {
                if (PaneContainer.IsLocked)
                {
                    ShowHeaderButtonVisibility = Visibility.Collapsed;
                    HideHeaderButtonVisibility = Visibility.Visible;
                    UnlockButtonVisibility = Visibility.Visible; 
                }
                else
                {
                    ShowHeaderButtonVisibility = HideHeaderButtonVisibility = Visibility.Collapsed; 
                }
            }
            else
            {
                HideHeaderButtonVisibility = Visibility.Collapsed;
                ShowHeaderButtonVisibility = Visibility.Visible;
                LockButtonVisibility = UnlockButtonVisibility = Visibility.Collapsed; 
            }
        }


        void PaneContainerIsLockedChanged(object sender_, EventArgs e_)
        {
            if (PaneContainer.IsLocked)
            {
                LockButtonVisibility = Visibility.Collapsed;
                UnlockButtonVisibility = Visibility.Visible;
                HideHeaderButtonVisibility = Visibility.Visible;
                ShowHeaderButtonVisibility = Visibility.Collapsed; 
            }
            else
            {
                LockButtonVisibility = Visibility.Visible;
                UnlockButtonVisibility = Visibility.Collapsed; 
                HideHeaderButtonVisibility = Visibility.Collapsed;
                ShowHeaderButtonVisibility = Visibility.Collapsed;  
            }
            RefreshWindowState();
        }


        private void RefreshWindowState()
        { 
            // Normal
            if (PaneContainer.WindowState == WindowState.Normal)
            {
                RestoreButtonVisibility = Visibility.Collapsed;
                MaximizeButtonVisibility = Visibility.Visible;
 
            }
            // Maximized
            else if (PaneContainer.WindowState == WindowState.Maximized)
            {
                MaximizeButtonVisibility = Visibility.Collapsed;
                RestoreButtonVisibility = Visibility.Visible;
            }
            else if (PaneContainer.WindowState == WindowState.Minimized)
            {
                RestoreButtonVisibility = Visibility.Visible;
                MaximizeButtonVisibility = Visibility.Visible; 
            }
        }
        
        public void PaneContainerContainerStateChanged(object sender_, EventArgs e_)
        {
            RefreshWindowState();
        } 

        protected void OnButtonDock_Click(object sender_, RoutedEventArgs e_)
        {
            if (Services.DockManager.ActiveContainer == null || Services.DockManager.ActiveContainer == this.PaneContainer)
            {
                PaneContainer.DockButtonState = WindowButtonState.None;
                return;
            }
            dockPopUp.IsOpen = true;
            Services.DockManager.ActiveContainer.IsDockSource = true;
            PaneContainer.IsDockTarget = true; 
        }

        void dockPopUp_Closed(object sender_, EventArgs e_)
        {
            if (Services.DockManager.ActiveContainer != null)
            {
                Services.DockManager.ActiveContainer.IsDockTarget = Services.DockManager.ActiveContainer.IsDockSource = false;
            }
            PaneContainer.IsDockSource = PaneContainer.IsDockTarget = false;
        }
        void dockPopUp_DockLocationSelected(object sender_, DockLocationSelectedEventArgs e_)
        {
            if (Services.DockManager.ActiveContainer == null || Services.DockManager.ActiveContainer == this.PaneContainer)
            {
                dockPopUp.IsOpen = false;
                PaneContainer.DockButtonState = WindowButtonState.None;
            }
            dockPopUp.IsOpen = false;
            DockService.DockContainer(Services.DockManager.ActiveContainer, this.PaneContainer, e_.Location);  
        }


        protected override void OnPreviewMouseLeftButtonDown(System.Windows.Input.MouseButtonEventArgs e)
        {
            base.OnPreviewMouseLeftButtonDown(e);
            if (e.Source is WindowButton && !(e.Source is WindowDockButton))
            {
                Services.DockManager.ActiveContainer = this.PaneContainer;
                e.Handled = true;
            }
        }

        private void EnvLabel_OnPreviewMouseLeftButtonDown(object sender_, System.Windows.Input.MouseButtonEventArgs e_)
        {
            PaneContainer.OnCaptionBarClick(sender_, e_);
        }


        void PaneContainerActivePaneChanged(object sender_, ActivePaneChangedEventArgs e_)
        { 
            BindCustomItem();
        }

        internal void BindCustomItem()
        {
            this.CustomItems = null;
            if (PaneContainer == null) return;
            if (PaneContainer.ActivePane == null) return;
            IWindowViewContainer container = Attached.GetViewContainer(PaneContainer.ActivePane) as IWindowViewContainer;
            if (container == null)
            {
                return;
            }
            var newHeader = container.Header as HeaderItemsHolderWidgetContainer;
            if (newHeader == null) return;

            this.CustomItems = newHeader.Items; 
        }
         
    }
}
