﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class MinimizedPaneContainer:Button
    { 
        private readonly DockingGrid dockingGrid;
        private readonly MinimizedPaneContainers paneContainers; 
        static MinimizedPaneContainer()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
    typeof(MinimizedPaneContainer),
    new FrameworkPropertyMetadata(typeof(MinimizedPaneContainer)));
            

        }
        internal MinimizedPaneContainer(MinimizedPaneContainers paneContainers_, DockingGrid dockingGrid_, PaneContainer paneContainer_)
        {
            this.paneContainers = paneContainers_;
            this.dockingGrid = dockingGrid_; 
            this.Container = paneContainer_;
            this.Pane = paneContainer_.ActivePane;
            this.Content = new ContentPaneInfo(Pane);
            this.Command = new DelegateCommand(_ => RestorePane());
            Container.ContainerCloseRequest += HandleCloseRequest;
            this.SetBinding(IsEnabledProperty,
                            new Binding("IsLocked")
                                {
                                    Source = paneContainer_,
                                    Converter = new OppositeBoolConverter(),
                                    Mode = BindingMode.OneWay
                                });
            this.OverridesDefaultStyle = false;
        }

        public ContentPane Pane
        {
            get { return (ContentPane)GetValue(PaneProperty); }
            private set { SetValue(PanePropertyKey, value); }
        }

        private static readonly DependencyPropertyKey PanePropertyKey
        = DependencyProperty.RegisterReadOnly("Pane", typeof(ContentPane), typeof(MinimizedPaneContainer), new PropertyMetadata(null));

        public static readonly DependencyProperty PaneProperty
            = PanePropertyKey.DependencyProperty; 

        public PaneContainer Container
        {
            get { return (PaneContainer)GetValue(ContainerProperty); }
            private set { SetValue(ContainerPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey ContainerPropertyKey
        = DependencyProperty.RegisterReadOnly("Container", typeof(PaneContainer), typeof(MinimizedPaneContainer), new PropertyMetadata(null));

        public static readonly DependencyProperty ContainerProperty
            = ContainerPropertyKey.DependencyProperty; 

        private void Detach()
        {
            Content = null;
            Container.ContainerCloseRequest -= HandleCloseRequest;
            this.Container.MinimizedProxy = null;
            this.paneContainers.Children.Remove(this); 
        }

        private void RestorePane()
        {
            Detach();
            Container.WindowState = WindowState.Normal; 
            dockingGrid.ArrangeLayout();
            dockingGrid.AddNewContainer(Container); 

        }

        public void RemoveFromParent()
        {
             Detach();
            this.dockingGrid.Remove(this.Container);
            dockingGrid.ArrangeLayout();
            if (dockingGrid != null && dockingGrid.IsEmpty)
            {
                dockingGrid.Wrapper.Host.Close(dockingGrid.Wrapper, true);
            }
        }
         
        private void HandleCloseRequest(object sender_, HandledEventArgs eventArgs_)
        {
            RemoveFromParent();
            eventArgs_.Handled = true;
        }
    }

    public class ContentPaneInfo
    {
        public ContentPaneInfo()
        {
            
        }
        public ContentPaneInfo(ContentPane pane_)
        {
            this.Icon = pane_.Icon;
            this.Caption = pane_.Caption;
        }
        public ImageSource Icon { get; set; }

        public string Caption { get; set; }
    }
}
