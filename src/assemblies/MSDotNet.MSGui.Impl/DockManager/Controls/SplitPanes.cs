﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class SplitPanes:ViewModelBase
    {
        public SplitPanes Parent;
         
        private SplitPanes first;
        public SplitPanes First
        {
            get { return first; }
            set
            {
                if (value != first)
                {
                    first = value;
                    if (value != null) value.Parent = null;
                    if (first != null) first.Parent = this;
                }
            }
        }

        public SplitPanes VisibleFirst
        {
            get
            {
                if (first != null && first.Self != null)
                    return first.Self.WindowState == WindowState.Minimized ? null : first;
                if (first == null) return first;
                var visibleFirstFirst = first.VisibleFirst;
                var visibleFirstSecond = first.VisibleSecond;
                if (visibleFirstFirst == null) return visibleFirstSecond;
                if (visibleFirstSecond == null) return visibleFirstFirst;
                return first;
            }
        }
        private SplitPanes second;
        public SplitPanes Second
        {
            get { return second; }
            set
            {
                if (value != second)
                {
                    second = value;
                    if (value != null) value.Parent = null;
                    if (second != null) second.Parent = this;
                }
            }
        }

        public SplitPanes VisibleSecond
        {
            get
            {
                if (second != null && second.Self != null)
                    return second.Self.WindowState == WindowState.Minimized ? null : second;
                if (second == null) return null;
                var visibleSecondFirst = second.VisibleFirst;
                var visibleSecondSecond = second.VisibleSecond;
                if (visibleSecondFirst == null) return visibleSecondSecond;
                if (visibleSecondSecond == null) return visibleSecondFirst;
                return second;
            }
        }

        public PaneContainer Self { get; set; }  

        public Orientation Orientation { get; internal set; }

        private GridLength firstLength;
        public GridLength FirstLength 
        { 
            get { return firstLength; }
            set
            {
                if (value != firstLength)
                {

                    firstLength = value;
                    OnPropertyChanged("FirstLength");
                }
            }
        }
        private GridLength secondLength;
        public GridLength SecondLength
        {
            get { return secondLength; }
            set
            {
                if (value != secondLength)
                {
                    secondLength = value; 
                    OnPropertyChanged("SecondLength");
                }
            }
        }
 
        
        public SplitPanes()
        {

        }

        public SplitPanes(PaneContainer p_)
        {
            Self = p_;
        }

        public SplitPanes(SplitPanes first_, SplitPanes second_, Orientation orientation_)
        {
            this.First = first_;
            this.Second = second_;
            this.Orientation = orientation_;
            this.FirstLength = new GridLength(1, GridUnitType.Star);
            this.SecondLength = new GridLength(1, GridUnitType.Star);
        }
 
        public static SplitPanes FindOwnerSplitPanes(SplitPanes root_, PaneContainer container_)
        {
            if (root_.Self == container_) return root_.Parent;
            if (root_.First != null)
            {
                var splitPanes = FindOwnerSplitPanes(root_.First, container_);
                if (splitPanes != null) return splitPanes.Parent;
            }
            if (root_.Second != null)
            {
                var splitPanes = FindOwnerSplitPanes(root_.Second, container_);
                if (splitPanes != null) return splitPanes.Parent;
            }
            return null;
        }

        private void CollectAllVisibleContainers(List<PaneContainer> containers_)
        {
            if (this.Self != null && Self.WindowState != WindowState.Minimized)
            {
                containers_.Add(Self);
            }
            if (this.First != null) First.CollectAllVisibleContainers(containers_);
            if (this.Second != null) Second.CollectAllVisibleContainers(containers_); 
        }

        public List<PaneContainer> GetVisibleContainers()
        {
            List<PaneContainer> visibleContainers = new List<PaneContainer>();
            CollectAllVisibleContainers(visibleContainers);
            return visibleContainers;
        }

        public void HideDockButton()
        {
            if (Self != null)
                Self.DockButtonState = WindowButtonState.None;
            else
            {
                First.HideDockButton();
                Second.HideDockButton();
            }
        }

        public void ShowDockButton()
        {
            if (Self != null)
            {
                if (!Self.IsLocked)
                {
                    Self.DockButtonState = WindowButtonState.Normal;
                }
            }
            else
            {
                First.ShowDockButton();
                Second.ShowDockButton();
            }
        }
    }
}
