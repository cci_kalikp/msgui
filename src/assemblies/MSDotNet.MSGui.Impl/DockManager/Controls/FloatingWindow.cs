﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Interop; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager; 
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Services;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ViewModels;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions; 

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{

    public class FloatingWindow : Window, IFloatingWindow
    {
        private readonly PaneContainer rootPaneContainer;
        private WeakPreprocessMessage processManager;
        public PaneContainer PaneContainer
        {
            get { return rootPaneContainer; }
        }

        public Guid GuidID { get; set; }

        public Window HostWindow { get { return this; } }

        public FloatingWindow()
        {
            GuidID = Guid.NewGuid();
            //AllowsTransparency = true;
            //Background = System.Windows.Media.Brushes.Transparent;
            WindowStyle = WindowStyle.None;
            Loaded += new RoutedEventHandler(OnLoaded);      
            GridServices.Add(this);
            this.SourceInitialized += FloatingWindow_SourceInitialized;
            #region Legacy Workspace
            CommandBindings.Add(new CommandBinding(Command.SaveSublayoutCommand,
                                                        (sender_, args_) => ChromeManagerBase.Instance.SaveWindow(this),
                                                        (s, e) => e.CanExecute = true));
            #endregion
             
            WindowChrome.SetWindowChrome(this,
                                         new WindowChrome()
                                             {
                                                 ResizeBorderThickness = new Thickness(2),
                                                 CaptionHeight = 0,
                                                 CornerRadius = ThemeExtensions.FloatingWindowBorderCornerRadius,
                                                 GlassFrameThickness = new Thickness(0)
                                             });
        } 

        public Window MainWindow
        {
            get { return (Window)GetValue(MainWindowProperty); }
            set { SetValue(MainWindowProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MainWindow.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MainWindowProperty =
            DependencyProperty.Register("MainWindow", typeof(Window), typeof(FloatingWindow), new UIPropertyMetadata(null, OnMainWindowChanged));

        private static void OnMainWindowChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            FloatingWindow window = (FloatingWindow) dependencyObject_;
            if (window.IsOwnedByMainWindow)
            {
                window.Owner = window.MainWindow;
            }
        } 

        public bool IsOwnedByMainWindow
        {
            get { return (bool)GetValue(IsOwnedByMainWindowProperty); }
            set { SetValue(IsOwnedByMainWindowProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsOwnedByMainWindow.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsOwnedByMainWindowProperty =
            DependencyProperty.Register("IsOwnedByMainWindow", typeof(bool), typeof(FloatingWindow), new UIPropertyMetadata(true, OnIsOwnedByMainWindowChanged));

        private static void OnIsOwnedByMainWindowChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            FloatingWindow window = (FloatingWindow)dependencyObject_;
            if (window.IsOwnedByMainWindow)
            {
                window.Owner = window.MainWindow;
            }
            else
            {
                window.Owner = null;
            }
        }


        void FloatingWindow_SourceInitialized(object sender_, EventArgs e_)
        {
            FloatingWindowUtil.FixResize(this);
            FloatingWindowUtil.FixRestoreBounds(this); 
            this.EnableMinimizeScreenshotSupport();
            if (FrameworkExtensions.DefaultStickWindowOptions != StickyWindowOptions.None)
            {
                new WPFStickyWindow(this)
                {
                    StickToScreen = FrameworkExtensions.DefaultStickWindowOptions.HasFlag(StickyWindowOptions.StickToScreen),
                    StickToOther = FrameworkExtensions.DefaultStickWindowOptions.HasFlag(StickyWindowOptions.StickToOther)
                };
            }
            if (FloatingWindowUtil.RestrictClosingToHeaderButton)
            {
                var handle = (new WindowInteropHelper(this)).Handle;
                var hwndSource = HwndSource.FromHwnd(handle);
                hwndSource.AddHook(new HwndSourceHook(MainWinProc));
            } 

        }

        private IntPtr MainWinProc(IntPtr hwnd_, int msg_, IntPtr wParam_, IntPtr lParam_, ref bool handled_)
        {
            if (msg_ == Win32.WM_SYSCOMMAND && (int)wParam_ == Win32.SC_CLOSE)
            {
                this.restrictClosingToHeaderButtonActive = true; 
            }
  
            return IntPtr.Zero;
        }
        public FloatingWindow(PaneContainer rootContainer_)
            : this()
        {
            Services.DockManager.ActiveContainer = rootContainer_;
            rootPaneContainer = rootContainer_;
            Content = rootPaneContainer;
            Title = "Window";
            //this.WindowState = p.WindowState;
            rootPaneContainer.ContainerStateChanged += StateChange;
            rootPaneContainer.ContainerCloseRequest += HandleClose;
            rootPaneContainer.ContainerIsLockedChanged += HandleIsLockedChanged;
        }

       
        public static FloatingWindow GetFloatingWindow(DependencyObject element_)
        {
            return element_.FindVisualParent<FloatingWindow>();
        }

        private bool internalClose = false;
        internal void CloseInternal()
        {
            internalClose = true;
            this.rootPaneContainer.IsRoot = false;
            this.Close();
        }
        private bool restrictClosingToHeaderButtonActive = false;

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            if (!e.Cancel)
            {
                e.Cancel = restrictClosingToHeaderButtonActive;
            }
            restrictClosingToHeaderButtonActive = false; 
        }

        protected override void OnClosed(EventArgs e_)
        {
            base.OnClosed(e_);
            UnRegister();
            if (!internalClose && this.Content != null)
            {
                PaneContainer rootContainer = this.Content as PaneContainer;
                if (rootContainer != null)
                {
                    rootContainer.Close(false);
                }
            }
            Content = null;
            GridServices.Remove(this);
            Services.DockManager.RemoveWindow(this);
            if (processManager != null)
            {
                processManager.Dispose();
            }
        }

        private void UnRegister()
        {
            rootPaneContainer.ContainerStateChanged -= StateChange;
            rootPaneContainer.ContainerCloseRequest -= HandleClose;
        }

        protected void OnLoaded(object sender_, EventArgs e_)
        {
            Loaded -= new RoutedEventHandler(OnLoaded);
            BindRoot(); 
            this.LastActivatedTime = DateTime.UtcNow;
            this.StateChanged += new EventHandler(WindowStateChange);
            Services.DockManager.AddWindow(this);

            if (this.WindowState != WindowState.Minimized && FloatingWindowUtil.HideLoadingWindows)
            {
                var tempLeft = this.Left; 
                this.Left = -5000;
                this.ShowInTaskbar = false; 

                EventHandler hack = null;
                hack = (a, b) =>
                { 
                    this.Left = tempLeft;
                    this.ClearValue(ShowInTaskbarProperty);
                    FloatingWindowUtil.ShowWindows -= hack;
                };
                FloatingWindowUtil.ShowWindows += hack;
            }
            processManager = new WeakPreprocessMessage(this);
        }

        internal void UpdateTopmost(bool topmost_)
        {
            this.Topmost = topmost_;
            foreach (var pane in rootPaneContainer.GetAllContainers())
            {
                pane.Topmost = topmost_; 
            }
        }

        private void BindRoot()
        {
            this.SetBinding(MaxWidthProperty, new Binding("MaxWidth") { Source = rootPaneContainer, Mode = BindingMode.OneWay });
            this.SetBinding(MinWidthProperty, new Binding("MinWidth") { Source = rootPaneContainer, Mode = BindingMode.OneWay });
            this.SetBinding(MaxHeightProperty, new Binding("MaxHeight") { Source = rootPaneContainer, Mode = BindingMode.OneWay });
            this.SetBinding(MinHeightProperty, new Binding("MinHeight") { Source = rootPaneContainer, Mode = BindingMode.OneWay });
            this.SetBinding(VisibilityProperty, new Binding("Visibility") { Source = rootPaneContainer, Mode = BindingMode.OneWay });
            this.SetBinding(ShowInTaskbarProperty, new Binding("ShowInTaskbar") { Source = rootPaneContainer, Mode = BindingMode.OneWay });
            this.SetBinding(TopmostProperty, new Binding("Topmost") { Source = rootPaneContainer, Mode = BindingMode.OneWay });
            this.SetBinding(IsOwnedByMainWindowProperty, new Binding("IsOwnedByMainWindow") { Source = rootPaneContainer, Mode = BindingMode.OneWay });
            this.SetBinding(IconProperty, new Binding("ActivePane.Icon") { Source = rootPaneContainer, Mode = BindingMode.OneWay });
            this.SetBinding(TitleProperty, new Binding("ActivePane.Caption") { Source = rootPaneContainer, Mode = BindingMode.OneWay });
            rootPaneContainer.SuspendSyncSize();

            if (this.WindowState == WindowState.Normal)
            {
                if (double.IsNaN(rootPaneContainer.PaneWidth) && double.IsNaN(rootPaneContainer.PaneHeight))
                {
                    this.SizeToContent = SizeToContent.WidthAndHeight;
                    double width = this.ActualWidth;
                    double height = this.ActualHeight;
                    this.SizeToContent = SizeToContent.Manual;
                    this.Width = width;
                    this.Height = height;
                }
                else if (double.IsNaN(rootPaneContainer.PaneWidth))
                {
                    this.SizeToContent = SizeToContent.Width;
                    double width = this.ActualWidth;
                    this.SizeToContent = SizeToContent.Manual;
                    this.Width = width;
                }
                else if (double.IsNaN(rootPaneContainer.PaneHeight))
                {
                    this.SizeToContent = SizeToContent.Height;
                    double height = this.ActualHeight;
                    this.SizeToContent = SizeToContent.Manual;
                    this.Height = height;
                }
                else
                {
                    this.Width = rootPaneContainer.PaneWidth;
                    this.Height = rootPaneContainer.PaneHeight;
                }
            }
            ContentPane singlePane = rootPaneContainer.SinglePane;
            WindowViewModel wvm = null;
            if (singlePane != null)
            {
                FrameworkElement content = singlePane.Content as FrameworkElement;
                if (content != null)
                {
                    wvm = content.DataContext as WindowViewModel;
                }
            } 
            if (wvm != null)
            {
                if (rootPaneContainer.IsLocked)
                {
                    this.ResizeMode = ResizeMode.NoResize;
                    if (wvm.InitialParameters != null)
                    {
                        oldResizeMode = wvm.InitialParameters.ResizeMode;         
                    }
                    else
                    {
                        oldResizeMode = ResizeMode.CanResize;
                    }
                }
                else
                {
                    if (wvm.InitialParameters != null)
                    {
                        this.ResizeMode = wvm.InitialParameters.ResizeMode;              
                    }
                }
                if (wvm.FloatingWindowMaximumHeight.HasValue)
                {
                    singlePane.SetContentMaximumHeight(wvm.FloatingWindowMaximumHeight.Value);
                }
                if (wvm.FloatingWindowMaximumWidth.HasValue)
                {
                    singlePane.SetContentMaximumWidth(wvm.FloatingWindowMaximumWidth.Value);
                }
                singlePane.SetContentMinimumHeight(wvm.FloatingWindowMinimumHeight);
                singlePane.SetContentMinimumWidth(wvm.FloatingWindowMinimumWidth);
                if (wvm.InitialParameters != null && (wvm.InitialParameters.InitialLocation & InitialLocation.Custom) == InitialLocation.Custom &&
                  wvm.InitialParameters.InitialLocationCallback != null)
                {
                    this.WindowStartupLocation = WindowStartupLocation.Manual;
                    var location = wvm.InitialParameters.InitialLocationCallback(this.ActualWidth, this.ActualHeight);
                    this.Top = location.Top;
                    this.Left = location.Left;
                    if (!CoreUtilities.AreClose(this.ActualWidth,location.Width))
                        this.Width = location.Width;
                    if (!CoreUtilities.AreClose(this.ActualHeight, location.Height))
                        this.Height = location.Height;
                }
            }
            rootPaneContainer.Width = this.Width;
            rootPaneContainer.Height = this.Height;
            rootPaneContainer.PerformSyncSize();
            //SetBinding(WidthProperty, new Binding("Width") { Source = rootPaneContainer, Mode = BindingMode.TwoWay });
            //SetBinding(HeightProperty, new Binding("Height") { Source = rootPaneContainer, Mode = BindingMode.TwoWay });
            this.SizeChanged += new SizeChangedEventHandler(FloatingWindow_SizeChanged);
        }

        private void FloatingWindow_SizeChanged(object sender_, SizeChangedEventArgs e_)
        {
            if (syncingSize || Content == null || this.WindowState == WindowState.Minimized) return;
            syncingSize = true;
            rootPaneContainer.InternalSyncSize(e_.WidthChanged, e_.HeightChanged);
 
            syncingSize = false;
        }
         

        private bool syncingSize;
        internal void InternalSyncSize(bool updateWidth_, bool updateHeight_)
        {
            if (syncingSize) return;
            syncingSize = true;
            if (updateWidth_)
            {
                this.Width = rootPaneContainer.Width;

            }
            if (updateHeight_)
            {
                this.Height = rootPaneContainer.Height;
            }
            syncingSize = false;
        }

        public DateTime LastActivatedTime { get; set; }

        protected void WindowStateChange(object sender_, EventArgs e_)
        {
            rootPaneContainer.WindowState = this.WindowState;
            if (this.WindowState == WindowState.Minimized)
            {
                Services.DockManager.ActiveContainer = null;
            }  
        }

        protected void StateChange(object sender_, EventArgs e_)
        {
            PaneContainer paneContainer = (PaneContainer)sender_;
            WindowState = paneContainer.WindowState;
        }

        protected void HandleClose(object sender_, HandledEventArgs e_)
        {
            CloseInternal();
            e_.Handled = true;
        }

        private ResizeMode oldResizeMode;
        private void HandleIsLockedChanged(object sender_, EventArgs eventArgs_)
        {
           if (rootPaneContainer.IsLocked)
           {
               oldResizeMode = this.ResizeMode;
               this.ResizeMode = ResizeMode.NoResize;
           }
           else
           {
               this.ResizeMode = oldResizeMode;
           }
        }

        internal void OnFocusHwndChanged()
        { 
            foreach (var contentPane in this.rootPaneContainer.GetAllPanes(true))
            {
                 foreach (var host in contentPane.FindLogicalChildren<WindowsFormsHost>())
                 {
                     if (((IKeyboardInputSink) host).HasFocusWithin())
                     {
                         contentPane.ActivateCurrentPane();
                         return;
                     } 
                 }
            }
        }



        #region Legacy Workspace

        private string subLayoutTitle;
        public string SubLayoutTitle
        {
            get { return subLayoutTitle; }
            set 
            { 
                subLayoutTitle = value;
                if (!string.IsNullOrEmpty(subLayoutTitle) && ChromeManagerBase.LoadedWorkspaceStickyTitle)
                {
                    this.Title = subLayoutTitle;
                }
            }
        }

  
        #endregion


        public IEnumerable<IWindowViewContainer> Windows
        {
            get { return Content == null ?
                new IWindowViewContainer[0] : 
                this.rootPaneContainer.GetAllPanes(true).ConvertAll(p_ => ((IWindowViewContainerHost)p_).View) as IEnumerable<IWindowViewContainer>;
            }
        }


        internal static readonly IComparer<FloatingWindow> Comparer = new ActivatedTimeComparer();

        private class ActivatedTimeComparer : IComparer<FloatingWindow>
        {
            public int Compare(FloatingWindow x_, FloatingWindow y_)
            {
                return x_.LastActivatedTime.CompareTo(y_.LastActivatedTime);
            }
        }
    }
}
