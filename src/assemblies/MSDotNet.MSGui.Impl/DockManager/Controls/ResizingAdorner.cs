﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Helpers;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    /*
     * Adorner - each UI element in WPF has a top layer where can be added adorner controls
     * Thumb - used to drag elements, returns drag delta
     */

    // Implements custom window resizing when default resizing is disabled (Aero disabled)
    class WindowResizingAdorner : Adorner
    {
        [Flags]
        enum Position
        {
            Top = 0x1,
            Bottom = 0x2,
            Left = 0x8,
            Right = 0x10
        }

        // Width of thumb resizer
        const int ThumbThickness = 2; //todo: read from WindowResizeAreaThickness or 0 for simple/modern theme

        // Stores Thumbs
        VisualCollection visualChildren;
        WindowThumb[] thumbs;

        Window window;
        Point mouseStartPosition;
        Point windowStartPosition;
        Size windowStartSize;

        /// <summary>
        /// Instantiates WindowResizingAdorner class
        /// </summary>
        /// <param name="element_">Control into which's adorer layer will be this adorner added</param>
        /// <param name="window_"></param>
        public WindowResizingAdorner(UIElement element_, Window window_)
            : base(element_)
        {
            window = window_;

            // must be instantiated first before WindowThumbs are created and added
            visualChildren = new VisualCollection(element_);
            thumbs = new WindowThumb[8];

            // * if you change the order, you have to change indexing in ArrangeOverride() method
            thumbs[0] = CreateThumb(Position.Left | Position.Top, Cursors.SizeNWSE);
            thumbs[1] = CreateThumb(Position.Right | Position.Top, Cursors.SizeNESW);
            thumbs[2] = CreateThumb(Position.Left | Position.Bottom, Cursors.SizeNESW);
            thumbs[3] = CreateThumb(Position.Right | Position.Bottom, Cursors.SizeNWSE);
            thumbs[4] = CreateThumb(Position.Left, Cursors.SizeWE);
            thumbs[5] = CreateThumb(Position.Top, Cursors.SizeNS);
            thumbs[6] = CreateThumb(Position.Right, Cursors.SizeWE);
            thumbs[7] = CreateThumb(Position.Bottom, Cursors.SizeNS);
        }

        /// <summary>
        /// Auxilliary method for creating thumbs
        /// </summary>
        /// <param name="position_">Thumb position in the window</param>
        /// <param name="cursor"></param>
        /// <returns>Returns created WindowThumb</returns>
        WindowThumb CreateThumb(Position position_, Cursor cursor)
        {
            WindowThumb thumb = new WindowThumb();
            thumb.Position = position_;
            thumb.DragStarted += new DragStartedEventHandler(Thumb_DragStarted);
            thumb.DragDelta += new DragDeltaEventHandler(Thumb_DragDelta);
            thumb.Cursor = cursor;

            visualChildren.Add(thumb);

            return thumb;
        }

        // called when thumb drag started (window resize started)
        void Thumb_DragStarted(object sender_, DragStartedEventArgs e_)
        {
            WindowThumb thumb = (WindowThumb)sender_;

            // store settings of the window, will be used to resize and move the window
            mouseStartPosition = WPFHelper.GetMousePosition(); // PointToScreen(Mouse.GetPosition(_window));
            windowStartPosition = new Point(window.Left, window.Top);
            windowStartSize = new Size(window.Width, window.Height);
        }

        // Called whenever thumb dragged (window resizing)
        void Thumb_DragDelta(object sender_, DragDeltaEventArgs e_)
        {
            WindowThumb thumb = (WindowThumb)sender_;

            // calculate mouse delta
            var position = WPFHelper.GetMousePosition();
            //Point position = PointToScreen(Mouse.GetPosition(_window));
            double deltaX = position.X - mouseStartPosition.X;
            double deltaY = position.Y - mouseStartPosition.Y;

            // horizontal resize
            if ((thumb.Position & Position.Left) == Position.Left)
            {
                double leftToMove = -deltaX;
                this.SetWindowWidth(windowStartSize.Width, ref leftToMove);
                window.Left = windowStartPosition.X - leftToMove;
            }
            else if ((thumb.Position & Position.Right) == Position.Right)
                this.SetWindowWidth(windowStartSize.Width, ref deltaX);

            // vertical resize
            if ((thumb.Position & Position.Top) == Position.Top)
            {
                double upToMove = -deltaY;
                this.SetWindowHeight(windowStartSize.Height, ref upToMove);
                window.Top = windowStartPosition.Y - upToMove;
            }
            else if ((thumb.Position & Position.Bottom) == Position.Bottom)
                this.SetWindowHeight(windowStartSize.Height, ref deltaY);
        }
         
        void SetWindowWidth(double oldWidth_, ref double deltaWidth_)
        {
            var newWidth = oldWidth_ + deltaWidth_;
            var newWidthCalculated = newWidth;
            if (newWidthCalculated < 2 * ThumbThickness)
                newWidthCalculated = 2 * ThumbThickness;
            if (newWidthCalculated < window.MinWidth)
            {
                newWidthCalculated = window.MinWidth;
            }
            if (newWidthCalculated > window.MaxWidth)
            {
                newWidthCalculated = window.MaxWidth;
            }
            deltaWidth_ += newWidthCalculated - newWidth;
            window.Width = newWidthCalculated;
        }
         
        void SetWindowHeight(double oldHeight_, ref double deltaHeight_)
        {
            var newHeight = oldHeight_ + deltaHeight_;
            var newHeightCalculated = newHeight;
            if (newHeightCalculated < 2 * ThumbThickness)
                newHeightCalculated = 2 * ThumbThickness;
            if (newHeightCalculated < window.MinHeight)
            {
                newHeightCalculated = window.MinHeight;
            }
            if (newHeightCalculated > window.MaxHeight)
            {
                newHeightCalculated = window.MaxHeight;
            }
            deltaHeight_ += newHeightCalculated - newHeight;
            window.Height = newHeightCalculated;
        }

        // Arrange the Adorners.
        protected override Size ArrangeOverride(Size finalSize_)
        {
            // DesiredWidth and desiredHeight are the width and height of the element that's being adorned.  
            // These will be used to place the ResizingAdorner at the corners of the adorned element.  
            double desiredWidth = AdornedElement.DesiredSize.Width;
            double desiredHeight = AdornedElement.DesiredSize.Height;

            var left = this.DesiredSize.Width - ThumbThickness;
            if (left < 0) left = 0;
            var width = this.DesiredSize.Width - (2 * ThumbThickness);
            if (width < 0) width = 0;
            var top = this.DesiredSize.Height - ThumbThickness;
            if (top < 0) top = 0;
            var height = this.DesiredSize.Height - (2 * ThumbThickness);
            if (height < 0) height = 0;
            thumbs[0].Arrange(new Rect(0, 0, ThumbThickness, ThumbThickness));
            thumbs[1].Arrange(new Rect(left, 0, ThumbThickness, ThumbThickness));
            thumbs[2].Arrange(new Rect(0, top, ThumbThickness, ThumbThickness));
            thumbs[3].Arrange(new Rect(left, top, ThumbThickness, ThumbThickness));
            thumbs[4].Arrange(new Rect(0, ThumbThickness, ThumbThickness, height));
            thumbs[5].Arrange(new Rect(ThumbThickness, 0, width, ThumbThickness));
            thumbs[6].Arrange(new Rect(left, ThumbThickness, ThumbThickness, height));
            thumbs[7].Arrange(new Rect(ThumbThickness, top, width, ThumbThickness));

            // Return the final size.
            return finalSize_;
        }
        // Override the VisualChildrenCount and GetVisualChild properties to interface with 
        // the adorner's visual collection.
        protected override int VisualChildrenCount { get { return visualChildren.Count; } }
        protected override Visual GetVisualChild(int index) { return visualChildren[index]; }


        // Used for resizing a window as a dragging point
        class WindowThumb : Thumb
        {
            public Position Position { get; set; }

            public WindowThumb()
            {
                FrameworkElementFactory borderFactory = new FrameworkElementFactory(typeof(Border));
                borderFactory.SetValue(Border.BackgroundProperty, Brushes.Transparent);

                ControlTemplate template = new ControlTemplate(typeof(WindowThumb));
                template.VisualTree = borderFactory;

                this.Template = template;
            }
        }
    }
}
