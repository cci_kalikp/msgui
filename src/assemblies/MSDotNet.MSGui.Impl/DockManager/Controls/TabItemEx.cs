﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media; 

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class TabItemEx : TabItem
    {

        internal Dimension Dimension { get; set; } 

        public Brush SelectedBackground
        {
            get { return (Brush)GetValue(SelectedBackgroundProperty); }
            set { SetValue(SelectedBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedBackgroundProperty = 
            DependencyProperty.Register("SelectedBackground", typeof(Brush), typeof(TabItemEx), new UIPropertyMetadata(null));


        public Brush NormalBackground
        {
            get { return (Brush)GetValue(NormalBackgroundProperty); }
            set { SetValue(NormalBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NormalBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NormalBackgroundProperty = 
            DependencyProperty.Register("NormalBackground", typeof(Brush), typeof(TabItemEx), new UIPropertyMetadata(null));



        public Brush HoverBackground
        {
            get { return (Brush)GetValue(HoverBackgroundProperty); }
            set { SetValue(HoverBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HoverBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HoverBackgroundProperty =
            DependencyProperty.Register("HoverBackground", typeof(Brush), typeof(TabItemEx), new UIPropertyMetadata(null));




        public Brush SelectedBorderBrush
        {
            get { return (Brush)GetValue(SelectedBorderBrushProperty); }
            set { SetValue(SelectedBorderBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedBorderBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedBorderBrushProperty =
            DependencyProperty.Register("SelectedBorderBrush", typeof(Brush), typeof(TabItemEx), new UIPropertyMetadata(null));


        public Brush NormalBorderBrush
        {
            get { return (Brush)GetValue(NormalBorderBrushProperty); }
            set { SetValue(NormalBorderBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NormalBorderBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NormalBorderBrushProperty =
            DependencyProperty.Register("NormalBorderBrush", typeof(Brush), typeof(TabItemEx), new UIPropertyMetadata(null));



        public Brush HoverBorderBrush
        {
            get { return (Brush)GetValue(HoverBorderBrushProperty); }
            set { SetValue(HoverBorderBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HoverBorderBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HoverBorderBrushProperty =
            DependencyProperty.Register("HoverBorderBrush", typeof(Brush), typeof(TabItemEx), new UIPropertyMetadata(null));


        public Brush SelectedForeground
        {
            get { return (Brush)GetValue(SelectedForegroundProperty); }
            set { SetValue(SelectedForegroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedForegroundProperty =
            DependencyProperty.Register("SelectedForeground", typeof(Brush), typeof(TabItemEx), new UIPropertyMetadata(null));


        public Brush NormalForeground
        {
            get { return (Brush)GetValue(NormalForegroundProperty); }
            set { SetValue(NormalForegroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NormalForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NormalForegroundProperty =
            DependencyProperty.Register("NormalForeground", typeof(Brush), typeof(TabItemEx), new UIPropertyMetadata(null));



        public Brush HoverForeground
        {
            get { return (Brush)GetValue(HoverForegroundProperty); }
            set { SetValue(HoverForegroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HoverForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HoverForegroundProperty =
            DependencyProperty.Register("HoverForeground", typeof(Brush), typeof(TabItemEx), new UIPropertyMetadata(null));



       
    }

    class Dimension
    {
        public double Height;
        public double MaxHeight = double.PositiveInfinity;
        public double MinHeight;
        public double Width;
        public double MaxWidth = double.PositiveInfinity;
        public double MinWidth;
    }
}
