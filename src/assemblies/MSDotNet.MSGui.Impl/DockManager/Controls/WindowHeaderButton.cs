﻿using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class WindowHeaderButton : WindowButton
    {

        static WindowHeaderButton()
        {

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof (WindowHeaderButton),
                new FrameworkPropertyMetadata(typeof (WindowHeaderButton)));
        }

        public WindowHeaderButton()
        {
            this.Command = PaneContainerCommands.ShowHeader;
        }
    }
}
