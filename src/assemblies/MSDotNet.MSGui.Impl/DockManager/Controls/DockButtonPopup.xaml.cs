﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    /// <summary>
    /// Interaction logic for DockButtonPopup.xaml
    /// </summary>
    public partial class DockButtonPopup : Popup
    {
        public DockButtonPopup()
        {
            InitializeComponent();
        }

        
        private void OnPopupClick(object sender_, RoutedEventArgs e_)
        {
            var copy = DockLocationSelected;
            if (copy != null)
            {
                copy(this, new DockLocationSelectedEventArgs(((WindowPlainButton)sender_).PlacementLocation));
            }
        }


        public event EventHandler<DockLocationSelectedEventArgs> DockLocationSelected; 

    }

    public class DockLocationSelectedEventArgs : EventArgs
    {
        public DockLocationSelectedEventArgs(DockLocation location_)
        {
            this.Location = location_;
        }

        public DockLocation Location { get; private set; }
    }
}
