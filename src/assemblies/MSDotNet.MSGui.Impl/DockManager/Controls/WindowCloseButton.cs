﻿using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class WindowCloseButton : WindowButton
    {

        static WindowCloseButton()
        {

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof (WindowCloseButton),
                new FrameworkPropertyMetadata(typeof (WindowCloseButton)));
        }

        public WindowCloseButton()
        {
            this.Command = PaneContainerCommands.Close;
        }
    }
}
