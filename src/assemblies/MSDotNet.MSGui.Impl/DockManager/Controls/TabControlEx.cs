﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class TabControlEx : TabControl
    {

        static TabControlEx()
        {
            TabStripPlacementProperty.AddOwner(typeof(TabControlEx), new FrameworkPropertyMetadata(Dock.Top, new PropertyChangedCallback(OnTabStripPlacementChanged))); 
        }



        public Brush TabItemSelectedBackground
        {
            get { return (Brush)GetValue(TabItemSelectedBackgroundProperty); }
            set { SetValue(TabItemSelectedBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TabItemSelectedBackgroundProperty =
            DependencyProperty.Register("TabItemSelectedBackground", typeof(Brush), typeof(TabControlEx), new FrameworkPropertyMetadata(null));


        public Brush TabItemNormalBackground
        {
            get { return (Brush)GetValue(TabItemNormalBackgroundProperty); }
            set { SetValue(TabItemNormalBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NormalBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TabItemNormalBackgroundProperty =
            DependencyProperty.Register("TabItemNormalBackground", typeof(Brush), typeof(TabControlEx), new FrameworkPropertyMetadata(null));



        public Brush TabItemHoverBackground
        {
            get { return (Brush)GetValue(TabItemHoverBackgroundProperty); }
            set { SetValue(TabItemHoverBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HoverBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TabItemHoverBackgroundProperty =
            DependencyProperty.Register("TabItemHoverBackground", typeof(Brush), typeof(TabControlEx), new FrameworkPropertyMetadata(null));

         
        public Brush TabItemSelectedBorderBrush
        {
            get { return (Brush)GetValue(TabItemSelectedBorderBrushProperty); }
            set { SetValue(TabItemSelectedBorderBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedBorderBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TabItemSelectedBorderBrushProperty =
            DependencyProperty.Register("TabItemSelectedBorderBrush", typeof(Brush), typeof(TabControlEx), new FrameworkPropertyMetadata(null));


        public Brush TabItemNormalBorderBrush
        {
            get { return (Brush)GetValue(TabItemNormalBorderBrushProperty); }
            set { SetValue(TabItemNormalBorderBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NormalBorderBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TabItemNormalBorderBrushProperty =
            DependencyProperty.Register("TabItemNormalBorderBrush", typeof(Brush), typeof(TabControlEx), new FrameworkPropertyMetadata(null));



        public Brush TabItemHoverBorderBrush
        {
            get { return (Brush)GetValue(TabItemHoverBorderBrushProperty); }
            set { SetValue(TabItemHoverBorderBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HoverBorderBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TabItemHoverBorderBrushProperty =
            DependencyProperty.Register("TabItemHoverBorderBrush", typeof(Brush), typeof(TabControlEx), new FrameworkPropertyMetadata(null));


        public Brush TabItemSelectedForeground
        {
            get { return (Brush)GetValue(TabItemSelectedForegroundProperty); }
            set { SetValue(TabItemSelectedForegroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TabItemSelectedForegroundProperty =
            DependencyProperty.Register("TabItemSelectedForeground", typeof(Brush), typeof(TabControlEx), new FrameworkPropertyMetadata(null));


        public Brush TabItemNormalForeground
        {
            get { return (Brush)GetValue(TabItemNormalForegroundProperty); }
            set { SetValue(TabItemNormalForegroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NormalForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TabItemNormalForegroundProperty =
            DependencyProperty.Register("TabItemNormalForeground", typeof(Brush), typeof(TabControlEx), new FrameworkPropertyMetadata(null));



        public Brush TabItemHoverForeground
        {
            get { return (Brush)GetValue(TabItemHoverForegroundProperty); }
            set { SetValue(TabItemHoverForegroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HoverForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TabItemHoverForegroundProperty =
            DependencyProperty.Register("TabItemHoverForeground", typeof(Brush), typeof(TabControlEx), new FrameworkPropertyMetadata(null));



        /// <summary>
        /// defines the Minimum width of a Header
        /// </summary>
        [DefaultValue(18.0)]
        [Category("Layout")]
        [Description("Gets or Sets the minimum Width Constraint shared by all Items in the Control, individual child elements MinWidth property will override this property")]
        public double TabItemMinWidth
        {
            get { return (double)GetValue(TabItemMinWidthProperty); }
            set { SetValue(TabItemMinWidthProperty, value); }
        }
        public static readonly DependencyProperty TabItemMinWidthProperty = DependencyProperty.Register("TabItemMinWidth", typeof(double), typeof(TabControlEx),
            new FrameworkPropertyMetadata(18.0, new PropertyChangedCallback(OnMinMaxChanged), CoerceMinWidth));

        private static object CoerceMinWidth(DependencyObject d, object value)
        {
            TabControlEx tc = (TabControlEx)d;
            double newValue = (double)value;

            if (newValue > tc.TabItemMaxWidth)
                return tc.TabItemMaxWidth;

            return (newValue > 0 ? newValue : 0);
        }

        /// <summary>
        /// defines the Minimum height of a Header
        /// </summary>
        [DefaultValue(20.0)]
        [Category("Layout")]
        [Description("Gets or Sets the minimum Height Constraint shared by all Items in the Control, individual child elements MinHeight property will override this value")]
        public double TabItemMinHeight
        {
            get { return (double)GetValue(TabItemMinHeightProperty); }
            set { SetValue(TabItemMinHeightProperty, value); }
        }
        public static readonly DependencyProperty TabItemMinHeightProperty = DependencyProperty.Register("TabItemMinHeight", typeof(double), typeof(TabControlEx),
            new FrameworkPropertyMetadata(20.0, new PropertyChangedCallback(OnMinMaxChanged), CoerceMinHeight));

        private static object CoerceMinHeight(DependencyObject d, object value)
        {
            TabControlEx tc = (TabControlEx)d;
            double newValue = (double)value;

            if (newValue > tc.TabItemMaxHeight)
                return tc.TabItemMaxHeight;

            return (newValue > 0 ? newValue : 0);
        }

        /// <summary>
        /// defines the Maximum width of a Header
        /// </summary>
        [DefaultValue(double.PositiveInfinity)]
        [Category("Layout")]
        [Description("Gets or Sets the maximum width Constraint shared by all Items in the Control, individual child elements MaxWidth property will override this value")]
        public double TabItemMaxWidth
        {
            get { return (double)GetValue(TabItemMaxWidthProperty); }
            set { SetValue(TabItemMaxWidthProperty, value); }
        }
        public static readonly DependencyProperty TabItemMaxWidthProperty = DependencyProperty.Register("TabItemMaxWidth", typeof(double), typeof(TabControlEx),
            new FrameworkPropertyMetadata(double.PositiveInfinity, new PropertyChangedCallback(OnMinMaxChanged), CoerceMaxWidth));

        private static object CoerceMaxWidth(DependencyObject d, object value)
        {
            TabControlEx tc = (TabControlEx)d;
            double newValue = (double)value;

            if (newValue < tc.TabItemMinWidth)
                return tc.TabItemMinWidth;

            return newValue;
        }

        /// <summary>
        /// defines the Maximum width of a Header
        /// </summary>
        [DefaultValue(double.PositiveInfinity)]
        [Category("Layout")]
        [Description("Gets or Sets the maximum height Constraint shared by all Items in the Control, individual child elements MaxHeight property will override this value")]
        public double TabItemMaxHeight
        {
            get { return (double)GetValue(TabItemMaxHeightProperty); }
            set { SetValue(TabItemMaxHeightProperty, value); }
        }
        public static readonly DependencyProperty TabItemMaxHeightProperty = DependencyProperty.Register("TabItemMaxHeight", typeof(double), typeof(TabControlEx),
            new FrameworkPropertyMetadata(double.PositiveInfinity, new PropertyChangedCallback(OnMinMaxChanged), CoerceMaxHeight));

        private static object CoerceMaxHeight(DependencyObject d, object value)
        {
            TabControlEx tc = (TabControlEx)d;
            double newValue = (double)value;

            if (newValue < tc.TabItemMinHeight)
                return tc.TabItemMinHeight;

            return newValue;
        }

        /// <summary>
        /// OnMinMaxChanged callback responds to any of the Min/Max dependancy properties changing
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnMinMaxChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TabControlEx tc = (TabControlEx)d;
            if (tc.Template == null) return;

            foreach (TabItemEx child in tc.InternalChildren())
            {
                if (child != null)
                    child.Dimension = null;
            }


            //            var tabsCount = tc.GetTabsCount();
            //            for (int i = 0; i < tabsCount; i++)
            //            {
            //                Header ti = tc.GetTabItem(i);
            //                if (ti != null)
            //                    ti.Dimension = null;
            //            }

            TabPanelEx tp = tc.FindVisualChildren<TabPanelEx>().FirstOrDefault();
            if (tp != null)
                tp.InvalidateMeasure();
        }

        /// <summary>
        /// OnTabStripPlacementChanged property callback
        /// </summary>
        /// <remarks>
        ///     We need to supplement the base implementation with this method as the base method does not work when
        ///     we are using virtualization in the tabpanel, it only updates visible items
        /// </remarks>
        private static void OnTabStripPlacementChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TabControlEx tc = (TabControlEx)d;

            foreach (TabItemEx tabItem in tc.InternalChildren())
            {
                if (tabItem != null)
                {
                    tabItem.Dimension = null;
                    tabItem.CoerceValue(System.Windows.Controls.TabItem.TabStripPlacementProperty);
                }
            }
        }

        internal int GetTabsCount()
        {
            if (BindingOperations.IsDataBound(this, ItemsSourceProperty))
            {
                IList list = ItemsSource as IList;
                if (list != null)
                    return list.Count;

                // ItemsSource is only an IEnumerable
                int i = 0;
                IEnumerator enumerator = ItemsSource.GetEnumerator();
                while (enumerator.MoveNext())
                    i++;
                return i;
            }

            if (Items != null)
                return Items.Count;

            return 0;
        }

        internal TabItemEx GetTabItem(int index)
        {
            if (BindingOperations.IsDataBound(this, ItemsSourceProperty) || this.ItemsSource != null)
            {
                IList list = ItemsSource as IList;
                if (list != null)
                    return this.ItemContainerGenerator.ContainerFromItem(list[index]) as TabItemEx;

                // ItemsSource is at least an IEnumerable
                int i = 0;
                IEnumerator enumerator = ItemsSource.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    if (i == index)
                        return this.ItemContainerGenerator.ContainerFromItem(enumerator.Current) as TabItemEx;
                    i++;
                }
                return null;
            }
            return Items[index] as TabItemEx;
        }

        private IEnumerable InternalChildren()
        {
            IEnumerator enumerator = (this.Items as IEnumerable).GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Current is TabItemEx)
                    yield return enumerator.Current;
                else
                    yield return this.ItemContainerGenerator.ContainerFromItem(enumerator.Current) as TabItemEx;
            }
        }
        /// <summary>
        /// IsItemItsOwnContainerOverride
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is TabItemEx;
        }
        /// <summary>
        /// GetContainerForItemOverride
        /// </summary>
        /// <returns></returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TabItemEx();
        }


        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            var tabsCount = GetTabsCount();
            if (tabsCount == 0)
                return;

            TabItem ti = null;

            switch (e.Key)
            {
                case Key.Home:
                    ti = GetTabItem(0);
                    break;

                case Key.End:
                    ti = GetTabItem(tabsCount - 1);
                    break;

                case Key.Tab:
                    if (e.KeyboardDevice.Modifiers == ModifierKeys.Control)
                    {
                        var index = SelectedIndex;
                        var direction = e.KeyboardDevice.Modifiers == ModifierKeys.Shift ? -1 : 1;

                        while (true)
                        {
                            index += direction;
                            if (index < 0)
                                index = tabsCount - 1;
                            else if (index > tabsCount - 1)
                                index = 0;

                            FrameworkElement ui = GetTabItem(index);
                            if (ui != null)
                            {
                                if (ui.Visibility == Visibility.Visible && ui.IsEnabled)
                                {
                                    ti = GetTabItem(index);
                                    break;
                                }
                            }
                        }
                    }
                    break;
            }

            TabPanelEx panel = this.FindVisualChildren<TabPanelEx>().FirstOrDefault();
            if (panel != null && ti != null)
            {
                panel.MakeVisible(ti, Rect.Empty);
                SelectedItem = ti;

                e.Handled = ti.Focus();
            }
            base.OnPreviewKeyDown(e);
        }

        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            base.OnSelectionChanged(e);
            if (e.AddedItems.Count == 0) return;

            var item = e.AddedItems[e.AddedItems.Count - 1];
            TabItem tabItem = item as TabItem;
            if (tabItem == null)
            {
                tabItem = this.ItemContainerGenerator.ContainerFromItem(item) as TabItem;
            }
            if (tabItem == null) return;
            TabPanelEx panel = this.FindVisualChildren<TabPanelEx>().FirstOrDefault();
            if (panel != null)
                panel.MakeVisible(tabItem, Rect.Empty);


        }
 
    }
}
