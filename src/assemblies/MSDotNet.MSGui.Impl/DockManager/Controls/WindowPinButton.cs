﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class WindowPinButton: WindowButton
    {
        static WindowPinButton()
        {

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof (WindowPinButton),
                new FrameworkPropertyMetadata(typeof (WindowPinButton)));
        }

        public WindowPinButton()
        {
            this.Command = PaneContainerCommands.Pin;
        }
    }
}
