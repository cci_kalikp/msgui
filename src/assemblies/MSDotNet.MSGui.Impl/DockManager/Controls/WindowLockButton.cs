﻿using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class WindowLockButton : WindowButton
    {
         
        static WindowLockButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
        typeof(WindowLockButton),
        new FrameworkPropertyMetadata(typeof(WindowLockButton)));
        } 

        public WindowLockButton()
        {
            this.Command = PaneContainerCommands.Lock;
        }
        

    }
}
