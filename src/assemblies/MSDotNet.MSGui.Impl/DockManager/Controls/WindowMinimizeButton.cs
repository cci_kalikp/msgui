﻿using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls
{
    public class WindowMinimizeButton : WindowButton
    { 
        static WindowMinimizeButton()
        {

            DefaultStyleKeyProperty.OverrideMetadata(
        typeof(WindowMinimizeButton),
        new FrameworkPropertyMetadata(typeof(WindowMinimizeButton)));
        }

        public WindowMinimizeButton()
        {
            this.Command = PaneContainerCommands.Minimize;
        }
    }
}
