﻿using System.Collections.Generic;
using System.Windows.Input;

namespace MorganStanley.MSDotNet.MSGui.Impl.DockManager
{
    public static class ContentPaneCommands
    {

        static readonly List<RoutedCommand> commands = new List<RoutedCommand>();
        static ContentPaneCommands()
        { 
            commands.Add(Close = new RoutedUICommand("Close", "Close", typeof(ContentPane)));
            commands.Add(Rename = new RoutedUICommand("Rename", "Rename", typeof(ContentPane)));
            commands.Add(Activate = new RoutedUICommand("Activate", "Activate", typeof(ContentPane)));
            commands.Add(TearOff = new RoutedUICommand("TearOff", "TearOff", typeof(ContentPane)));
            commands.Add(ChangeToFloatingOnly = new RoutedUICommand("Floating", "ChangeToFloatingOnly", typeof(ContentPane)));
            commands.Add(ChangeToDockable = new RoutedUICommand("Dockable", "ChangeToDockable", typeof(ContentPane)));

            commands.Add(AddToNewScreeGroup = new RoutedUICommand("Add to New Screen Group", "AddToNewScreeGroup", typeof(ContentPane)));
        }

        public static readonly RoutedCommand Rename; 
        public static readonly RoutedCommand Close;
        public static readonly RoutedCommand Activate;
        public static readonly RoutedCommand TearOff;
        public static readonly RoutedCommand ChangeToFloatingOnly;
        public static readonly RoutedCommand ChangeToDockable;

        public static readonly RoutedCommand AddToNewScreeGroup;

        public static IList<RoutedCommand> GetAllCommands()
        {
            return commands;
        }
    }
}