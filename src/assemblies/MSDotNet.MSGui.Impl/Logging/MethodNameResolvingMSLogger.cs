﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Logging
{
    public class MethodNameResolvingMSLogger : IMSLogger
    {
        protected string m_className = "noclass";
        protected MSLogLayer m_logLayer;
        protected string m_name;

        protected string ResolveMethodName()
        {
            return new StackTrace().GetFrame(3).GetMethod().Name;
        }

        protected internal MethodNameResolvingMSLogger(string name_)
        {
            this.m_name = name_;
            this.m_logLayer = MSLogLayer.MakeLayer(this.m_name, "/");
        }
        
        public void Alert(string message_)
        {
            this.log(MSLogPriority.Alert, message_);
        }
        
        public void Alert(string message_, System.Exception e_)
        {
            this.log(MSLogPriority.Alert, message_, e_);
        }
        
        public void Alert(string message_, string methodName_)
        {
            this.log(MSLogPriority.Alert, message_, this.m_className, methodName_);
        }
        
        public void Alert(string message_, System.Exception e_, string methodName_)
        {
            this.log(MSLogPriority.Alert, message_, e_, this.m_className, methodName_);
        }
        
        public void Alert(string message_, string className_, string methodName_)
        {
            this.log(MSLogPriority.Alert, message_, className_, methodName_);
        }
        
        public void Alert(string message_, System.Exception e_, string className_, string methodName_)
        {
            this.log(MSLogPriority.Alert, message_, e_, className_, methodName_);
        }

        public void Alert(Func<string> messageFunc_)
        {
            this.Log(MSLogPriority.Alert, messageFunc_);
        }

        public void Alert(Func<string> messageFunc_, Exception e_)
        {
            this.Log(MSLogPriority.Alert, messageFunc_, e_);
        }

        public void Alert(Func<string> messageFunc_, string methodName_)
        {
            this.Log(MSLogPriority.Alert, messageFunc_, m_className, methodName_);
        }

        public void Alert(Func<string> messageFunc_, string className_, string methodName_)
        {
            this.Log(MSLogPriority.Alert, messageFunc_, className_, methodName_);
        }

        public void Alert(Func<string> messageFunc_, Exception e_, string methodName_)
        {
            this.Log(MSLogPriority.Alert, messageFunc_, e_, m_className, methodName_);
        }

        public void Alert(Func<string> messageFunc_, Exception e_, string className_, string methodName_)
        {
            this.Log(MSLogPriority.Alert, messageFunc_, e_, className_, methodName_);
        }

        public void AlertWithFormat(string format_, params object[] params_)
        {
            this.log(MSLogPriority.Alert, string.Format(format_, params_), this.m_className, string.Empty);
        }
        
        public void Critical(string message_)
        {
            this.log(MSLogPriority.Critical, message_);
        }
        
        public void Critical(string message_, System.Exception e_)
        {
            this.log(MSLogPriority.Critical, message_, e_);
        }
        
        public void Critical(string message_, string methodName_)
        {
            this.log(MSLogPriority.Critical, message_, this.m_className, methodName_);
        }
        
        public void Critical(string message_, System.Exception e_, string methodName_)
        {
            this.log(MSLogPriority.Critical, message_, e_, this.m_className, methodName_);
        }
        
        public void Critical(string message_, string className_, string methodName_)
        {
            this.log(MSLogPriority.Critical, message_, className_, methodName_);
        }
        
        public void Critical(string message_, System.Exception e_, string className_, string methodName_)
        {
            this.log(MSLogPriority.Critical, message_, e_, className_, methodName_);
        }

        public void Critical(Func<string> messageFunc_)
        {
            this.Log(MSLogPriority.Critical, messageFunc_);
        }

        public void Critical(Func<string> messageFunc_, Exception e_)
        {
            this.Log(MSLogPriority.Critical, messageFunc_, e_);
        }

        public void Critical(Func<string> messageFunc_, string methodName_)
        {
            this.Log(MSLogPriority.Critical, messageFunc_, m_className, methodName_);
        }

        public void Critical(Func<string> messageFunc_, string className_, string methodName_)
        {
            this.Log(MSLogPriority.Critical, messageFunc_, className_, methodName_);
        }

        public void Critical(Func<string> messageFunc_, Exception e_, string methodName_)
        {
            this.Log(MSLogPriority.Critical, messageFunc_, e_, m_className, methodName_);
        }

        public void Critical(Func<string> messageFunc_, Exception e_, string className_, string methodName_)
        {
            this.Log(MSLogPriority.Critical, messageFunc_, e_, className_, methodName_);
        }

        public void CriticalWithFormat(string format_, params object[] params_)
        {
            this.log(MSLogPriority.Critical, string.Format(format_, params_), this.m_className, string.Empty);
        }
        
        public void Debug(string message_)
        {
            this.log(MSLogPriority.Debug, message_);
        }
        
        public void Debug(string message_, System.Exception e_)
        {
            this.log(MSLogPriority.Debug, message_, e_);
        }
        
        public void Debug(string message_, string methodName_)
        {
            this.log(MSLogPriority.Debug, message_, this.m_className, methodName_);
        }
        
        public void Debug(string message_, System.Exception e_, string methodName_)
        {
            this.log(MSLogPriority.Debug, message_, e_, this.m_className, methodName_);
        }
        
        public void Debug(string message_, string className_, string methodName_)
        {
            this.log(MSLogPriority.Debug, message_, className_, methodName_);
        }
        
        public void Debug(string message_, System.Exception e_, string className_, string methodName_)
        {
            this.log(MSLogPriority.Debug, message_, e_, className_, methodName_);
        }

        public void Debug(Func<string> messageFunc_)
        {
            this.Log(MSLogPriority.Debug, messageFunc_);
        }

        public void Debug(Func<string> messageFunc_, Exception e_)
        {
            this.Log(MSLogPriority.Debug, messageFunc_, e_);
        }

        public void Debug(Func<string> messageFunc_, string methodName_)
        {
            this.Log(MSLogPriority.Debug, messageFunc_, m_className, methodName_);
        }

        public void Debug(Func<string> messageFunc_, string className_, string methodName_)
        {
            this.Log(MSLogPriority.Debug, messageFunc_, className_, methodName_);
        }

        public void Debug(Func<string> messageFunc_, Exception e_, string methodName_)
        {
            this.Log(MSLogPriority.Debug, messageFunc_, e_, m_className, methodName_);
        }

        public void Debug(Func<string> messageFunc_, Exception e_, string className_, string methodName_)
        {
            this.Log(MSLogPriority.Debug, messageFunc_, e_, className_, methodName_);
        }

        public void DebugWithFormat(string format_, params object[] params_)
        {
            this.log(MSLogPriority.Debug, string.Format(format_, params_), this.m_className, string.Empty);
        }
        
        public void Emergency(string message_)
        {
            this.log(MSLogPriority.Emergency, message_);
        }
        
        public void Emergency(string message_, System.Exception e_)
        {
            this.log(MSLogPriority.Emergency, message_, e_);
        }
        
        public void Emergency(string message_, string methodName_)
        {
            this.log(MSLogPriority.Emergency, message_, this.m_className, methodName_);
        }
        
        public void Emergency(string message_, System.Exception e_, string methodName_)
        {
            this.log(MSLogPriority.Emergency, message_, e_, this.m_className, methodName_);
        }
        
        public void Emergency(string message_, string className_, string methodName_)
        {
            this.log(MSLogPriority.Emergency, message_, className_, methodName_);
        }
        
        public void Emergency(string message_, System.Exception e_, string className_, string methodName_)
        {
            this.log(MSLogPriority.Emergency, message_, e_, className_, methodName_);
        }

        public void Emergency(Func<string> messageFunc_)
        {
            this.Log(MSLogPriority.Emergency, messageFunc_);
        }

        public void Emergency(Func<string> messageFunc_, Exception e_)
        {
            this.Log(MSLogPriority.Emergency, messageFunc_, e_);
        }

        public void Emergency(Func<string> messageFunc_, string methodName_)
        {
            this.Log(MSLogPriority.Emergency, messageFunc_, m_className, methodName_);
        }

        public void Emergency(Func<string> messageFunc_, string className_, string methodName_)
        {
            this.Log(MSLogPriority.Emergency, messageFunc_, className_, methodName_);
        }

        public void Emergency(Func<string> messageFunc_, Exception e_, string methodName_)
        {
            this.Log(MSLogPriority.Emergency, messageFunc_, e_, m_className, methodName_);
        }

        public void Emergency(Func<string> messageFunc_, Exception e_, string className_, string methodName_)
        {
            this.Log(MSLogPriority.Emergency, messageFunc_, e_, className_, methodName_);
        }

        public void EmergencyWithFormat(string format_, params object[] params_)
        {
            this.log(MSLogPriority.Emergency, string.Format(format_, params_), this.m_className, string.Empty);
        }
        
        public void Error(string message_)
        {
            this.log(MSLogPriority.Error, message_);
        }
        
        public void Error(string message_, System.Exception e_)
        {
            this.log(MSLogPriority.Error, message_, e_);
        }
        
        public void Error(string message_, string methodName_)
        {
            this.log(MSLogPriority.Error, message_, this.m_className, methodName_);
        }
        
        public void Error(string message_, System.Exception e_, string methodName_)
        {
            this.log(MSLogPriority.Error, message_, e_, this.m_className, methodName_);
        }
        
        public void Error(string message_, string className_, string methodName_)
        {
            this.log(MSLogPriority.Error, message_, className_, methodName_);
        }
        
        public void Error(string message_, System.Exception e_, string className_, string methodName_)
        {
            this.log(MSLogPriority.Error, message_, e_, className_, methodName_);
        }

        public void Error(Func<string> messageFunc_)
        {
            this.Log(MSLogPriority.Error, messageFunc_);
        }

        public void Error(Func<string> messageFunc_, Exception e_)
        {
            this.Log(MSLogPriority.Error, messageFunc_, e_);
        }

        public void Error(Func<string> messageFunc_, string methodName_)
        {
            this.Log(MSLogPriority.Error, messageFunc_, m_className, methodName_);
        }

        public void Error(Func<string> messageFunc_, string className_, string methodName_)
        {
            this.Log(MSLogPriority.Error, messageFunc_, className_, methodName_);
        }

        public void Error(Func<string> messageFunc_, Exception e_, string methodName_)
        {
            this.Log(MSLogPriority.Error, messageFunc_, e_, m_className, methodName_);
        }

        public void Error(Func<string> messageFunc_, Exception e_, string className_, string methodName_)
        {
            this.Log(MSLogPriority.Error, messageFunc_, e_, className_, methodName_);
        }

        public void ErrorWithFormat(string format_, params object[] params_)
        {
            this.log(MSLogPriority.Error, string.Format(format_, params_), this.m_className, string.Empty);
        }
        
        public string GetName()
        {
            return this.m_name;
        }
        
        public void Info(string message_)
        {
            this.log(MSLogPriority.Info, message_);
        }
        
        public void Info(string message_, System.Exception e_)
        {
            this.log(MSLogPriority.Info, message_, e_);
        }
        
        public void Info(string message_, string methodName_)
        {
            this.log(MSLogPriority.Info, message_, this.m_className, methodName_);
        }
        
        public void Info(string message_, System.Exception e_, string methodName_)
        {
            this.log(MSLogPriority.Info, message_, e_, this.m_className, methodName_);
        }
        
        public void Info(string message_, string className_, string methodName_)
        {
            this.log(MSLogPriority.Info, message_, className_, methodName_);
        }
        
        public void Info(string message_, System.Exception e_, string className_, string methodName_)
        {
            this.log(MSLogPriority.Info, message_, e_, className_, methodName_);
        }

        public void Info(Func<string> messageFunc_)
        {
            this.Log(MSLogPriority.Info, messageFunc_);
        }

        public void Info(Func<string> messageFunc_, Exception e_)
        {
            this.Log(MSLogPriority.Info, messageFunc_, e_);
        }

        public void Info(Func<string> messageFunc_, string methodName_)
        {
            this.Log(MSLogPriority.Info, messageFunc_, m_className, methodName_);
        }

        public void Info(Func<string> messageFunc_, string className_, string methodName_)
        {
            this.Log(MSLogPriority.Info, messageFunc_, className_, methodName_);
        }

        public void Info(Func<string> messageFunc_, Exception e_, string methodName_)
        {
            this.Log(MSLogPriority.Info, messageFunc_, e_, m_className, methodName_);
        }

        public void Info(Func<string> messageFunc_, Exception e_, string className_, string methodName_)
        {
            this.Log(MSLogPriority.Info, messageFunc_, e_, className_, methodName_);
        }

        public void InfoWithFormat(string format_, params object[] params_)
        {
            this.log(MSLogPriority.Info, string.Format(format_, params_), this.m_className, string.Empty);
        }
        
        public bool IsAlertLoggable()
        {
            return this.IsLoggable(MSLogPriority.Alert);
        }
        
        public bool IsCriticalLoggable()
        {
            return this.IsLoggable(MSLogPriority.Critical);
        }
        
        public bool IsDebugLoggable()
        {
            return this.IsLoggable(MSLogPriority.Debug);
        }
        
        public bool IsEmergencyLoggable()
        {
            return this.IsLoggable(MSLogPriority.Emergency);
        }
        
        public bool IsErrorLoggable()
        {
            return this.IsLoggable(MSLogPriority.Error);
        }
        
        public bool IsInfoLoggable()
        {
            return this.IsLoggable(MSLogPriority.Info);
        }
        
        protected bool IsLoggable(MSLogPriority priority_)
        {
            return MorganStanley.MSDotNet.MSLog.MSLog.IsLoggable(this.m_logLayer, priority_);
        }
        
        public bool IsNoticeLoggable()
        {
            return this.IsLoggable(MSLogPriority.Notice);
        }
        
        public bool IsWarningLoggable()
        {
            return this.IsLoggable(MSLogPriority.Warning);
        }
        
        protected void log(MSLogPriority priority_, string message_)
        {
            if (this.IsLoggable(priority_))
            {
                MSLogMessage message = new MSLogMessage(priority_);
                message.SetLayer(this.m_logLayer).Location(this.m_className, ResolveMethodName()).Append(message_).Send();
            }
        }
        
        protected void log(MSLogPriority priority_, string message_, System.Exception e_)
        {
            if (this.IsLoggable(priority_))
            {
                MSLogMessage message = new MSLogMessage(priority_);
                message.SetLayer(this.m_logLayer).Location(this.m_className, ResolveMethodName()).Append(message_).Append(" Exception is:" + e_.ToString()).Send();
            }
        }
        
        protected void log(MSLogPriority priority_, string message_, string className_, string methodName_)
        {
            if (this.IsLoggable(priority_))
            {
                MSLogMessage message = new MSLogMessage(priority_);
                message.SetLayer(this.m_logLayer).Location(className_, methodName_).Append(message_).Send();
            }
        }
        
        protected void log(MSLogPriority priority_, string message_, System.Exception e_, string className_, string methodName_)
        {
            if (this.IsLoggable(priority_))
            {
                MSLogMessage message = new MSLogMessage(priority_);
                message.SetLayer(this.m_logLayer).Location(className_, methodName_).Append(message_).Append(" Exception:" + e_.ToString()).Send();
            }
        }
        
        public void Notice(string message_)
        {
            this.log(MSLogPriority.Notice, message_);
        }
        
        public void Notice(string message_, System.Exception e_)
        {
            this.log(MSLogPriority.Notice, message_, e_);
        }
        
        public void Notice(string message_, string methodName_)
        {
            this.log(MSLogPriority.Notice, message_, this.m_className, methodName_);
        }
        
        public void Notice(string message_, System.Exception e_, string methodName_)
        {
            this.log(MSLogPriority.Notice, message_, e_, this.m_className, methodName_);
        }
        
        public void Notice(string message_, string className_, string methodName_)
        {
            this.log(MSLogPriority.Notice, message_, className_, methodName_);
        }
        
        public void Notice(string message_, System.Exception e_, string className_, string methodName_)
        {
            this.log(MSLogPriority.Notice, message_, e_, className_, methodName_);
        }

        public void Notice(Func<string> messageFunc_)
        {
            this.Log(MSLogPriority.Notice, messageFunc_);
        }

        public void Notice(Func<string> messageFunc_, Exception e_)
        {
            this.Log(MSLogPriority.Notice, messageFunc_, e_);
        }

        public void Notice(Func<string> messageFunc_, string methodName_)
        {
            this.Log(MSLogPriority.Notice, messageFunc_, m_className, methodName_);
        }

        public void Notice(Func<string> messageFunc_, string className_, string methodName_)
        {
            this.Log(MSLogPriority.Notice, messageFunc_, className_, methodName_);
        }

        public void Notice(Func<string> messageFunc_, Exception e_, string methodName_)
        {
            this.Log(MSLogPriority.Notice, messageFunc_, e_, m_className, methodName_);
        }

        public void Notice(Func<string> messageFunc_, Exception e_, string className_, string methodName_)
        {
            this.Log(MSLogPriority.Notice, messageFunc_, e_, className_, methodName_);
        }

        public void NoticeWithFormat(string format_, params object[] params_)
        {
            this.log(MSLogPriority.Notice, string.Format(format_, params_), this.m_className, string.Empty);
        }
        
        public void Warning(string message_)
        {
            this.log(MSLogPriority.Warning, message_);
        }
        
        public void Warning(string message_, System.Exception e_)
        {
            this.log(MSLogPriority.Warning, message_, e_);
        }
        
        public void Warning(string message_, string methodName_)
        {
            this.log(MSLogPriority.Warning, message_, this.m_className, methodName_);
        }
        
        public void Warning(string message_, System.Exception e_, string methodName_)
        {
            this.log(MSLogPriority.Warning, message_, e_, this.m_className, methodName_);
        }
        
        public void Warning(string message_, string className_, string methodName_)
        {
            this.log(MSLogPriority.Warning, message_, className_, methodName_);
        }
        
        public void Warning(string message_, System.Exception e_, string className_, string methodName_)
        {
            this.log(MSLogPriority.Warning, message_, e_, className_, methodName_);
        }

        public void Warning(Func<string> messageFunc_)
        {
            this.Log(MSLogPriority.Warning, messageFunc_);
        }

        public void Warning(Func<string> messageFunc_, Exception e_)
        {
            this.Log(MSLogPriority.Warning, messageFunc_, e_);
        }

        public void Warning(Func<string> messageFunc_, string methodName_)
        {
            this.Log(MSLogPriority.Warning, messageFunc_, m_className, methodName_);
        }

        public void Warning(Func<string> messageFunc_, string className_, string methodName_)
        {
            this.Log(MSLogPriority.Warning, messageFunc_, className_, methodName_);
        }

        public void Warning(Func<string> messageFunc_, Exception e_, string methodName_)
        {
            this.Log(MSLogPriority.Warning, messageFunc_, e_, m_className, methodName_);
        }

        public void Warning(Func<string> messageFunc_, Exception e_, string className_, string methodName_)
        {
            this.Log(MSLogPriority.Warning, messageFunc_, e_, className_, methodName_);
        }

        public void WarningWithFormat(string format_, params object[] params_)
        {
            this.log(MSLogPriority.Warning, string.Format(format_, params_), this.m_className, string.Empty);
        }
        
        public string ClassName
        {
            get
            {
                return this.m_className;
            }
            set
            {
                this.m_className = value;
            }
        }
        
        public MSLogLayer LogLayer
        {
            get
            {
                return this.m_logLayer;
            }
        }

        private void Log(MSLogPriority priority, Func<string> messageFunc)
        {
            if (IsLoggable(priority))
            {
                MSLogMessage logMessage = new MSLogMessage(priority);
                logMessage.SetLayer(m_logLayer).Location(m_className, string.Empty).Append(messageFunc()).Send();
            }
        }

        private void Log(MSLogPriority priority, Func<string> messageFunc, Exception exception)
        {
            if (IsLoggable(priority))
            {
                MSLogMessage logMessage = new MSLogMessage(priority);
                logMessage.SetLayer(m_logLayer).Location(m_className, string.Empty).Append(messageFunc()).Append(" Exception is:" + exception.ToString()).Send();
            }
        }

        private void Log(MSLogPriority priority, Func<string> messageFunc, string className, string methodName)
        {
            if (IsLoggable(priority))
            {
                MSLogMessage logMessage = new MSLogMessage(priority);
                logMessage.SetLayer(m_logLayer).Location(className, methodName).Append(messageFunc()).Send();
            }
        }

        private void Log(MSLogPriority priority, Func<string> messageFunc, Exception exception, string className, string methodName)
        {
            if (IsLoggable(priority))
            {
                MSLogMessage logMessage = new MSLogMessage(priority);
                logMessage.SetLayer(m_logLayer).Location(className, methodName).Append(messageFunc()).Append(" Exception:" + exception.ToString()).Send();
            }
        }
    }
}
