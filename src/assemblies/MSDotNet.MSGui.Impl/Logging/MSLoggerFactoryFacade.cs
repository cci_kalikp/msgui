﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Logging
{
    public static class MSLoggerFactoryFacade
    {
        public static IMSLogger CreateLogger<T>(string meta, string project, string release)
        {
            return MSLoggerFactory.Provider.CreateLogger<T>(string.Format("{0}/{1}/{2}", meta, project, release));
        }
    }
}
