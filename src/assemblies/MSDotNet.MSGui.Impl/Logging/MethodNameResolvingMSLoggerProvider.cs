﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDotNet.My;
using System;
using System.Reflection;

namespace MorganStanley.MSDotNet.MSGui.Impl.Logging
{
    public class MethodNameResolvingMSLoggerProvider : IMSLoggerProvider, System.IDisposable
    {
        public MethodNameResolvingMSLoggerProvider()
        {
        }

        public MethodNameResolvingMSLoggerProvider(System.IO.Stream configStream_)
        {
            if (configStream_ == null)
            {
                throw new System.ArgumentNullException("configStream_");
            }
            MSLogConfig.Init(configStream_);
        }

        public MethodNameResolvingMSLoggerProvider(string configPath_)
        {
            if (configPath_ == null)
            {
                throw new System.ArgumentNullException("configPath_");
            }
            MSLogConfig.Init(configPath_);
        }

        public IMSLogger CreateLogger(string name_)
        {
            return new MethodNameResolvingMSLogger(name_);
        }

        public IMSLogger CreateLogger<T>(string name_)
        {
            return new MethodNameResolvingMSLogger(name_) { ClassName = typeof(T).Name };
        }

        public void Dispose()
        {
            MorganStanley.MSDotNet.MSLog.MSLog.Stop();
            System.GC.SuppressFinalize(this);
        }
    }
}
