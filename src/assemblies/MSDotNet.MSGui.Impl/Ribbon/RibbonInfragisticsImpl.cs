﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Ribbon/RibbonInfragisticsImpl.cs#28 $
// $Change: 899478 $
// $DateTime: 2014/10/03 17:40:44 $
// $Author: hrechkin $

using System;
using System.Collections;
using System.Drawing;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using Infragistics.Windows.Ribbon;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon; 
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon;

namespace MorganStanley.MSDotNet.MSGui.Impl.Ribbon
{
    internal class RibbonInfragisticsImpl : IPersistable
    {
        private readonly XamRibbon _ribbon;
        private readonly object _isLoadingOrSaving = new object();

        private bool _isMinimized;
        private Icon _menuIcon;
        private RibbonTabItem _selectedTab;

        #region IRibbon Members

        public bool IsMinimized
        {
            get { return _isMinimized; }
            set
            {
                _isMinimized = value;
                SetRibbonMinimized(value);
            }
        }

        public Icon Icon
        {
            get { return _menuIcon; }
            set
            {
                _menuIcon = value;
                if (_ribbon != null && _menuIcon != null)
                {
                    _ribbon.ApplicationMenu.Image = ConvertFromIcon(_menuIcon);
                }
            }
        }

        public void AddMenuItem(RibbonItem button)
        {
            if (!(button is RibbonButton))
            {
                throw new InvalidOperationException("Currently only RibbonButton accepted");
            }

            ButtonTool bt = GetButtonToolFromRibbonButton((RibbonButton) button);
            if (_ribbon != null) _ribbon.ApplicationMenu.Items.Add(bt);
        }

        public void AddRibbonItem(string tab, string @group, RibbonItem item)
        {
            RibbonGroup rg = GetRibbonGroup(tab, @group);
            HandleRibbonItem(rg.Items, item);
        }

        #endregion

        protected RibbonInfragisticsImpl()
        {
        }

        public RibbonInfragisticsImpl(XamRibbon ribbon)
        {
            if (ribbon == null) throw new ArgumentNullException("ribbon", "The ribbon_ cannot be null.");
            _ribbon = ribbon;
        }

        protected virtual void AddRibbonTab(RibbonTabItem tab)
        {
            _ribbon.Tabs.Add(tab);
        }

        protected virtual void SetRibbonMinimized(bool isMinimized)
        {
            if (_ribbon != null) _ribbon.IsMinimized = isMinimized;
        }

        private RibbonGroup GetRibbonGroup(string tab_, string group_)
        {
            RibbonTabItem tab = null;
            foreach (RibbonTabItem rtab in _ribbon.Tabs)
            {
                if (rtab.Header as string == tab_)
                {
                    tab = rtab;
                    break;
                }
            }
            if (tab == null)
            {
                tab = new RibbonTabItem {Header = tab_};
                AddRibbonTab(tab);
            }

            foreach (RibbonGroup group in tab.RibbonGroups)
            {
                if (group.Caption.Equals(group_)) return group;
            }
            var rg = new RibbonGroup {Caption = group_};
            tab.RibbonGroups.Add(rg);
            return rg;
        }

        private static void HandleRibbonItem(IList container_, RibbonItem item_)
        {
            if (item_ is RibbonButton)
            {
                container_.Add(GetButtonToolFromRibbonButton(item_ as RibbonButton));
                return;
            }

            if (item_ is RibbonButtonGroup)
            {
                RibbonButtonGroup itemGroup = item_ as RibbonButtonGroup;
                var btnGroup = new ButtonGroup();

                foreach (RibbonButton rbtn in itemGroup.Buttons)
                {
                    HandleRibbonItem(btnGroup.Children, rbtn);
                }
                container_.Add(btnGroup);
                return;
            }

            if (item_ is WrapperPannelRibbonItem)
            {
                WrapperPannelRibbonItem wp = item_ as WrapperPannelRibbonItem;
                Panel igPanel;
                if (wp.Orientation == RibbonItemOrientation.Horizontal)
                {
                    igPanel = new ToolHorizontalWrapPanel();
                }
                else
                {
                    igPanel = new ToolVerticalWrapPanel();
                }
                foreach (RibbonItem rbItem in wp.Items)
                {
                    HandleRibbonItem(igPanel.Children, rbItem);
                }
                container_.Add(igPanel);
            }
        }

        private static ButtonTool GetButtonToolFromRibbonButton(RibbonButton button)
        {
            ButtonTool bt = new ButtonTool();
            if (button.Clicked != null) bt.Click += new RoutedEventHandler(button.Clicked);
            if (!string.IsNullOrEmpty(button.Text)) bt.Caption = button.Text;
            if (button.Icon != null)
            {
                ImageSource isrc = ConvertFromIcon(button.Icon);
                bt.SmallImage = isrc;
                bt.LargeImage = isrc;
            }
            if (button.Size == RibbonButtonSize.Large)
            {
                RibbonGroup.SetMinimumSize(bt, RibbonToolSizingMode.ImageAndTextLarge);
                RibbonGroup.SetMaximumSize(bt, RibbonToolSizingMode.ImageAndTextLarge);
            }
            else
            {
                RibbonGroup.SetMinimumSize(bt, RibbonToolSizingMode.ImageAndTextNormal);
                RibbonGroup.SetMaximumSize(bt, RibbonToolSizingMode.ImageAndTextNormal);
            }
            bt.IsEnabled = button.IsEnabled;

            button.PropertyChanged += (sender, e) =>
                {
                    switch (e.PropertyName)
                    {
                        case "IsEnabled":
                            bt.IsEnabled = button.IsEnabled;
                            break;
                    }
                };

            if (button.ToolTip != null)
            {
                var tip = new XamRibbonScreenTip
                    {
                        Header = bt.Caption,
                        FooterSeparatorVisibility = Visibility.Collapsed,
                        Content = button.ToolTip
                    };
                bt.ToolTip = tip;
            }
            return bt;
        }

        private static ImageSource ConvertFromIcon(Icon icon)
        {
            return Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty,
                                                       BitmapSizeOptions.FromEmptyOptions());
        }

        internal void SaveSelectedTab()
        {
            _selectedTab = _ribbon.SelectedTab;
        }

        internal void RestoreSelectedTab()
        {
            _ribbon.SelectedTab = _selectedTab;
        }

        public void LoadState(XDocument state_)
        {
            try
            {
                lock (_isLoadingOrSaving)
                {
                    var mainElem = state_.Root;

                    if (mainElem != null && mainElem.Name.LocalName == "Ribbon")
                    {
                        var minattr = mainElem.Attribute("Minimized");
                        _ribbon.IsMinimized = minattr != null && minattr.Value.ToLower() == "true";

                        var activeTabAttr = mainElem.Attribute("SelectedTab");
                        var tabIndexToActivate = activeTabAttr != null
                                                ? int.Parse(activeTabAttr.Value, CultureInfo.InvariantCulture)
                                                : 0;

                        var contextGroupAttribute = mainElem.Attribute("ContextualGroupKey");
                        RibbonTabItem tabToActivate;
                        if (contextGroupAttribute != null)
                        {
                            var contextualGroupKey = contextGroupAttribute.Value;
                            if (_ribbon.ContextualTabGroups.ContainsKey(contextualGroupKey))
                            {
                                tabToActivate =
                                    _ribbon.ContextualTabGroups[contextualGroupKey].Tabs[tabIndexToActivate];
                                _ribbon.SelectedTab = tabToActivate;
                                _selectedTab = tabToActivate;
                            }
                        }
                        else if (tabIndexToActivate >= 0 && tabIndexToActivate < _ribbon.Tabs.Count)
                        {
                            tabToActivate = _ribbon.Tabs[tabIndexToActivate];
                            _ribbon.SelectedTab = tabToActivate;
                            _selectedTab = tabToActivate;
                        }
                    }
                }
            }
            catch
            {
            }
        }

        public XDocument SaveState()
        {
            try
            {
                lock (_isLoadingOrSaving)
                {
                    var activeTabItem = _ribbon.SelectedTab;
                    int activeTabIndex = 0;
                    string contextualGroupKey = null;

                    if (activeTabItem != null)
                    {
                        activeTabIndex = _ribbon.Tabs.IndexOf(activeTabItem);
                        if (activeTabIndex == -1)
                        {
                            contextualGroupKey = activeTabItem.ContextualTabGroup.Key;
                            activeTabIndex = activeTabItem.ContextualTabGroup.Tabs.IndexOf(activeTabItem);
                        }
                    }

                    var state = new XDocument(
                        new XElement("Ribbon",
                                     new XAttribute("Minimized", _ribbon.IsMinimized ? "true" : "false"),
                                     new XAttribute("SelectedTab", activeTabIndex)
                            )
                        );

                    if (contextualGroupKey != null)
                    {
                        state.Root.SetAttributeValue("ContextualGroupKey", contextualGroupKey);
                    }

                    return state;
                }
            }
            catch
            {
                return null;
            }
        }

        public string PersistorId
        {
            get { return PersistorIdKey; }
        }

        internal XamRibbon Ribbon
        {
            get { return _ribbon; }
        }

        public const string PersistorIdKey = "MSGui.Internal.Ribbon";
    }
}
