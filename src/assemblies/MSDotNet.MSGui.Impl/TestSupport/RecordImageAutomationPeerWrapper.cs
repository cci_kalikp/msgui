﻿using System.Windows;
using System.Windows.Automation.Peers;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;

namespace MorganStanley.MSDotNet.MSGui.Impl.TestSupport
{
    class RecordImageAutomationPeerWrapper: InteractiveAutomationPeerWrapper
    {
        private bool _isRecorded = false;
        protected RecordImageAutomationPeerWrapper(AutomationPeer inner, FrameworkElement owner, ImagePathHandler commandsHandler)
            : base(inner, owner, commandsHandler)
        {
            
        }

        public RecordImageAutomationPeerWrapper(AutomationPeer inner, FrameworkElement owner)
            : this(inner, owner, new ImagePathHandler())
        {
            
        }

        protected void RecordImage()
        {
            if (!_isRecorded)
            {
                VisualTestHelper.RecordImage(this, (ImagePathHandler)this.CommandsHandler);
                _isRecorded = true;
            }
        }


    }
}