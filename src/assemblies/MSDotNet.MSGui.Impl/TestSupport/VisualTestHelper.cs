﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using Point = System.Drawing.Point;
using Size = System.Drawing.Size;

namespace MorganStanley.MSDotNet.MSGui.Impl.TestSupport
{
    internal class VisualTestHelper
    {
        
        internal static bool IsRecording { set; get; }

        static VisualTestHelper()
        {
#if DEBUG
            IsRecording = true;
#else
            IsRecording = false;
#endif

        }

        public static void RecordImage(FrameworkElementAutomationPeer peer, ImagePathHandler pathHandler)
        {
            if(!IsRecording)
                return;

            try
            {
                var path = pathHandler.ImagePath;
                if (System.IO.File.Exists(path))
                {
                    Trace.WriteLine("the image with the same name already existed: " + path);
                }

                var b = peer.GetBoundingRectangle();
                var leftTop = new Point((int) b.Left, (int) b.Top);
                var bounds = new System.Drawing.Rectangle(leftTop,
                                                          new Size((int) b.Width, (int) b.Height));
                using (var bitmap = new Bitmap((int) bounds.Width, (int) bounds.Height))
                {
                    using (var g = Graphics.FromImage(bitmap))
                    {
                        g.CopyFromScreen(leftTop, System.Drawing.Point.Empty, bounds.Size);
                    }
                    bitmap.Save(path);
                }
            }
// ReSharper disable EmptyGeneralCatchClause
            catch (Exception e)
// ReSharper restore EmptyGeneralCatchClause
            {
                // CUIT error, should be caught by the CUIT runner
            }
        }
    }
}
