﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Automation.Peers;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;

namespace MorganStanley.MSDotNet.MSGui.Impl.TestSupport
{
    class InteractiveAutomationPeerWrapper: AutomationPeerWrapper
    {
       
        private readonly CommandsHandler _commandsHandler;

        protected InteractiveAutomationPeerWrapper(AutomationPeer inner, FrameworkElement owner, CommandsHandler commandsHandler) : base(inner, owner)
        {
            _commandsHandler = commandsHandler;
            _commandsHandler.AutomationPeer = this;
        }

        protected CommandsHandler CommandsHandler
        {
            get { return _commandsHandler; }
        }

        public override object GetPattern(PatternInterface patternInterface)
        {
            if (patternInterface == PatternInterface.Value)
            {   
                if(_commandsHandler != null)
                    return _commandsHandler;
            }
            return base.GetPattern(patternInterface);
        }

    }
}
