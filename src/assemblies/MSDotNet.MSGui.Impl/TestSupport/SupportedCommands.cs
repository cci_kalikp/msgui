﻿namespace MorganStanley.MSDotNet.MSGui.Impl.Shell
{
    enum ToolWindowSupportedCommands
    {
        MoveTo
    }

    enum MainWindowSupportedCommands
    {
        GetLastCreatedView, GetAllViews, GetFloatingViews, GetDockedViews, GetAppliationTitle,
        MoveTo, GetProcessId, GetTestConfigFile, 
        SetToPlaybackMode, 
    }

    enum ImagePathCommand
    {
        GetImagePath
    }
}