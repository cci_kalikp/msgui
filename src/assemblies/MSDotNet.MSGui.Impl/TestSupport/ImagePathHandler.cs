﻿using System.IO;
using System.Reflection;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell
{
    
    
    internal class ImagePathHandler : CommandsHandler
    {
        public ImagePathHandler()
        {
            Handlers[ImagePathCommand.GetImagePath.ToString()] = (paras) => ImagePath;
        }

        #region Implementation of IUICommandProvider
       
      

        private string _imageRoot;
        protected string ImageRoot
        {
            get
            {
                if (_imageRoot == null)
                {
                    string currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    _imageRoot = Path.Combine(currentDirectory, "Images");
                    
                    if (!System.IO.Directory.Exists(_imageRoot))
                    {
                        System.IO.Directory.CreateDirectory(_imageRoot);
                    }
                 
                }
                return _imageRoot;
            }
        }


        public virtual string ImagePath
        {
            get
            {
                var fileName = AutomationPeer.GetClassName() + "_" + (AutomationPeer.GetAutomationId() ?? AutomationPeer.GetName());
                fileName = fileName.Replace(';', '_');
                return Path.GetFullPath(Path.Combine(ImageRoot, fileName + ".bmp"));
            }
        }

        #endregion
    }
}