﻿namespace MorganStanley.MSDotNet.MSGui.Impl.Shell
{
   

    public interface IUICommandHandler
    {
        bool IsSupported(string command);
        string Execute(string command, string[] parameters);
    }
}