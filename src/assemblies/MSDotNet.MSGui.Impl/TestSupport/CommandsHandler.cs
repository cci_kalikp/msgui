﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.MVVMDock;
using MorganStanley.MSDotNet.MSGui.Impl.TestSupport;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell
{

    internal class CommandsHandler : IValueProvider, IUICommandHandler
    {
        private string _currentCommand;
        private string _currentResult;
        private string[] _currentParameters;
        const char Separator = ' ';

        private Dictionary<string, Func<string[], string>> _handlers = new Dictionary<string, Func<string[], string>>();

        protected Dictionary<string, Func<string[], string>> Handlers { get { return _handlers; } }

        private IValueProvider _innerValueProvider;

        private InteractiveAutomationPeerWrapper _automationPeer;
        public AutomationPeer AutomationPeer
        {
            protected get { return _automationPeer; } 
            set 
            { 
                _automationPeer = value as InteractiveAutomationPeerWrapper;
                _innerValueProvider =
                    _automationPeer.InnerAutomationPeer.GetPattern(PatternInterface.Value) as IValueProvider;
            }
        }


        #region Implementation of IValueProvider
        /// <summary>
        /// Sets the value of a control.
        /// </summary>
        /// <param name="value"/><exception cref="T:System.InvalidOperationException">If locale-specific information is passed to a control in an incorrect format such as an incorrectly formatted date. </exception><exception cref="T:System.ArgumentException">If a new value cannot be converted from a string to a format the control recognizes.</exception><exception cref="T:System.Windows.Automation.ElementNotEnabledException">When an attempt is made to manipulate a control that is not enabled.</exception>
        public void SetValue(string value)
        {
            _currentCommand = null;

            if (value == null)
            {
                if (_innerValueProvider != null && !_innerValueProvider.IsReadOnly)
                {
                    _innerValueProvider.SetValue(value);
                }
                return;
            }

            var args = value.Split(Separator);
            var command = args[0];
            if (IsSupported(command))
            {
                _currentCommand = args[0];
                _currentParameters = args.Skip(1).ToArray();
                _currentResult = Execute(_currentCommand, _currentParameters);
            }
            else 
            {
                if (_innerValueProvider != null && !_innerValueProvider.IsReadOnly)
                {
                    _innerValueProvider.SetValue(value);
                }
            }
        }

        /// <summary>
        /// Gets the value of the control.
        /// </summary>
        /// <returns>
        /// The value of the control as a string. 
        /// </returns>
        public string Value
        {
            get 
            {
                if (_currentCommand == null && _innerValueProvider != null)
                {
                    return _innerValueProvider.Value;
                }
                _currentCommand = null;
                return _currentResult;
            }
        }

        /// <summary>
        /// Gets a value that specifies whether the value of a control is read-only. 
        /// </summary>
        /// <returns>
        /// true if the value is read-only; false if it can be modified. 
        /// </returns>
        public bool IsReadOnly
        {
            get { return false; }
        }

        #endregion

        #region Implementation of IUICommandHandler

        public bool IsSupported(string command)
        {
            return Handlers.ContainsKey(command);
        }

        public string Execute(string command, string[] parameters)
        {
            if (IsSupported(command))
                return Handlers[command](parameters);
            return "";  //void
        }

        #endregion
    }


    
}