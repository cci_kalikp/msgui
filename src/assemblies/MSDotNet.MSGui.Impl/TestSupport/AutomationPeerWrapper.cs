﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Automation.Peers;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;

namespace MorganStanley.MSDotNet.MSGui.Impl.TestSupport
{
    internal class AutomationPeerWrapper : FrameworkElementAutomationPeer
    {
        
        private readonly AutomationPeer _innerAutomationPeer;

        internal AutomationPeer InnerAutomationPeer{ get { return _innerAutomationPeer; }}

        protected AutomationPeerWrapper(AutomationPeer inner, FrameworkElement owner)
            : base(owner)
        {
            _innerAutomationPeer = inner;
        }
    

        #region  Overrides of AutomationPeer

        protected override string GetLocalizedControlTypeCore()
        {
            return _innerAutomationPeer.GetLocalizedControlType();
            
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.GetChildren"/>.
        /// </summary>
        /// <returns>
        /// The collection of child elements.
        /// </returns>
        protected override List<AutomationPeer> GetChildrenCore()
        {
            return _innerAutomationPeer.GetChildren();
        }


        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.GetBoundingRectangle"/>.
        /// </summary>
        /// <returns>
        /// The bounding rectangle.
        /// </returns>
        protected override Rect GetBoundingRectangleCore()
        {
            return _innerAutomationPeer.GetBoundingRectangle();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.IsOffscreen"/>.
        /// </summary>
        /// <returns>
        /// true if the element is not on the screen; otherwise, false.
        /// </returns>
        protected override bool IsOffscreenCore()
        {
            return _innerAutomationPeer.IsOffscreen();

        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.GetOrientation"/>.
        /// </summary>
        /// <returns>
        /// The orientation of the control.
        /// </returns>
        protected override AutomationOrientation GetOrientationCore()
        {
            return _innerAutomationPeer.GetOrientation();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.GetItemType"/>.
        /// </summary>
        /// <returns>
        /// The kind of item.
        /// </returns>
        protected override string GetItemTypeCore()
        {
            return _innerAutomationPeer.GetItemType();

        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.GetItemStatus"/>.
        /// </summary>
        /// <returns>
        /// The status.
        /// </returns>
        protected override string GetItemStatusCore()
        {
            return _innerAutomationPeer.GetItemStatus();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.IsRequiredForForm"/>.
        /// </summary>
        /// <returns>
        /// true if the element is must be completed; otherwise, false.
        /// </returns>
        protected override bool IsRequiredForFormCore()
        {
            return _innerAutomationPeer.IsRequiredForForm();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.IsKeyboardFocusable"/>.
        /// </summary>
        /// <returns>
        /// true if the element can accept keyboard focus; otherwise, false.
        /// </returns>
        protected override bool IsKeyboardFocusableCore()
        {
            return _innerAutomationPeer.IsKeyboardFocusable();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.HasKeyboardFocus"/>.
        /// </summary>
        /// <returns>
        /// true if the element has keyboard focus; otherwise, false.
        /// </returns>
        protected override bool HasKeyboardFocusCore()
        {
            return _innerAutomationPeer.HasKeyboardFocus();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.IsEnabled"/>.
        /// </summary>
        /// <returns>
        /// true if the automation peer can receive and send events; otherwise, false.
        /// </returns>
        protected override bool IsEnabledCore()
        {
            return _innerAutomationPeer.IsEnabled();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.IsPassword"/>.
        /// </summary>
        /// <returns>
        /// true if the element contains sensitive content; otherwise, false.
        /// </returns>
        protected override bool IsPasswordCore()
        {
            return _innerAutomationPeer.IsPassword();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.GetAutomationId"/>.
        /// </summary>
        /// <returns>
        /// The string that contains the identifier.
        /// </returns>
        protected override string GetAutomationIdCore()
        {
            return _innerAutomationPeer.GetAutomationId();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.GetName"/>.
        /// </summary>
        /// <returns>
        /// The string that contains the label.
        /// </returns>
        protected override string GetNameCore()
        {
            return _innerAutomationPeer.GetName();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.GetClassName"/>.
        /// </summary>
        /// <returns>
        /// The class name.
        /// </returns>
        protected override string GetClassNameCore()
        {
            return _innerAutomationPeer.GetClassName();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.GetAutomationControlType"/>.
        /// </summary>
        /// <returns>
        /// The control type.
        /// </returns>
        protected override AutomationControlType GetAutomationControlTypeCore()
        {
            return _innerAutomationPeer.GetAutomationControlType();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.IsContentElement"/>.
        /// </summary>
        /// <returns>
        /// true if the element is a content element; otherwise, false.
        /// </returns>
        protected override bool IsContentElementCore()
        {
            return _innerAutomationPeer.IsContentElement();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.IsControlElement"/>.
        /// </summary>
        /// <returns>
        /// true if the element is a control; otherwise, false.
        /// </returns>
        protected override bool IsControlElementCore()
        {
            return _innerAutomationPeer.IsControlElement();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.GetLabeledBy"/>.
        /// </summary>
        /// <returns>
        /// The <see cref="T:System.Windows.Automation.Peers.LabelAutomationPeer"/> for the element that is targeted by the <see cref="T:System.Windows.Controls.Label"/>.
        /// </returns>
        protected override AutomationPeer GetLabeledByCore()
        {
            return _innerAutomationPeer.GetLabeledBy();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.GetHelpText"/>.
        /// </summary>
        /// <returns>
        /// The help text.
        /// </returns>
        protected override string GetHelpTextCore()
        {
            return _innerAutomationPeer.GetHelpText();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.GetAcceleratorKey"/>.
        /// </summary>
        /// <returns>
        /// The accelerator key.
        /// </returns>
        protected override string GetAcceleratorKeyCore()
        {
            return _innerAutomationPeer.GetAcceleratorKey();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.GetAccessKey"/>.
        /// </summary>
        /// <returns>
        /// The string that contains the access key.
        /// </returns>
        protected override string GetAccessKeyCore()
        {
            return _innerAutomationPeer.GetAcceleratorKey();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.GetClickablePoint"/>.
        /// </summary>
        /// <returns>
        /// A point within the clickable area of the element.
        /// </returns>
        protected override System.Windows.Point GetClickablePointCore()
        {
            return _innerAutomationPeer.GetClickablePoint();
        }

        /// <summary>
        /// When overridden in a derived class, is called by <see cref="M:System.Windows.Automation.Peers.AutomationPeer.SetFocus"/>.
        /// </summary>
        protected override void SetFocusCore()
        {
            _innerAutomationPeer.SetFocus();
        }

        #endregion
    }

}