﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Xml.Linq;
//using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
    public static class ProcessIsolationExtensions
    {
        internal const string IsolatedProcessWindowFactoryName = "IsolatedWindowFactory";
        private static readonly ConcurrentDictionary<int, object> IsolatedProcesses =
            new ConcurrentDictionary<int, object>();

        public static void SetUpProcessIsolationCloseHandlers(this IWindowViewContainer iwcv)
        {
            EventHandler<Core.WindowEventArgs> handler = null;
            handler = delegate
                {
                    iwcv.Closed -= handler;
                    iwcv.ClosedQuietly -= handler;

                    var control = iwcv.Content as XProcControl;
                    if (control != null)
                        control.OnIWVCClosed(iwcv, EventArgs.Empty);
                };
            iwcv.Closed += handler;
            iwcv.ClosedQuietly += handler;
        }

        public static void SetUpProcessIsolatedWrapperFor<T>(this IWindowViewContainer iwcv,
                                                             IChromeManager chromeManager, XDocument state)
            where T : UserControl
        {
            var control = new XProcControl(typeof (T).AssemblyQualifiedName, state, chromeManager, string.Empty);
            HookUpEvents(iwcv);
            iwcv.Content = control;
        }

        public static void SetUpProcessIsolatedWrapperFor<T>(this IWindowViewContainer iwcv, XDocument state,
                                                             string additionalCommandLine, bool is32bit = false,
                                                             bool needsAR = false,
                                                             string hostdirectory = null, string extraAR = null)
            where T : UserControl
        {
            var control = new XProcControl(typeof (T).AssemblyQualifiedName, state, null, is32bit, string.Empty,
                                           needsAR, hostdirectory, extraAR, additionalCommandLine);
            HookUpEvents(iwcv);
            iwcv.Content = control;
        }

        public static void SetUpProcessIsolatedWrapperFor<T>(this IWindowViewContainer iwcv, XDocument state,
                                                             bool is32bit = false, bool needsAR = false,
                                                             string hostdirectory = null, string extraAR = null)
        {
            var control = new XProcControl(typeof (T).AssemblyQualifiedName, state, null, is32bit, string.Empty, needsAR,
                                           hostdirectory,
                                           extraAR);
            HookUpEvents(iwcv);
            iwcv.Content = control;
        }

        public static void RegisterProcessIsolatedWindowFactory<T>(this IChromeRegistry chromeRegistry, string ID,
                                                                   bool is32bit = false,
                                                                   bool needsAR = false, string hostdirectory = null,
                                                                   string extraAR = null)
        {
            XProcControl control = null;
            chromeRegistry.RegisterWindowFactory(ID, (iwcv, state) =>
                {
                    control = new XProcControl(typeof (T).AssemblyQualifiedName, state, null, is32bit, string.Empty,
                                               needsAR,
                                               hostdirectory,
                                               extraAR);
                    HookUpEvents(iwcv);
                    iwcv.Content = control;
                    control.Load(state);
                    return true;
                },
                                                 vc => control.Save());
        }

        private static void HookUpEvents(IWindowViewContainer iwcv)
        {
            EventHandler<Core.WindowEventArgs> handler = null;
            handler = (s, e) =>
                {
                    iwcv.Closed -= handler;
                    iwcv.ClosedQuietly -= handler;

                    var contentControl = iwcv.Content as XProcControl;
                    if (contentControl != null)
                    {
                        contentControl.OnIWVCClosed(iwcv, EventArgs.Empty);
                    }
                };

            iwcv.Closed += handler;
            iwcv.ClosedQuietly += handler;
        }

        /// <summary>
        /// Sets up and starts a module in its own isolated process.
        /// </summary>
        /// <typeparam name="T">The type of isolated module, if known at compile time. The type must implement either IModule or ISubsystemModuleCollection interface</typeparam>
        /// <param name="framework">Framework instance.</param>
        /// <param name="isolatedPlatform">Target platform for isolated process.</param>
        /// <param name="isolatedSubsystemLocation">Location of isolated subsystem.</param>
        /// <param name="isolatingExecutableLocation">Location of the isolated subsystem executable that will execute the subsystem.</param>
        /// <param name="additionalCommandLine">Additional command line to pass into subsystem.</param>
        /// <param name="needsAssemblyResolution">A flag indicating whether or not additional assembly resolution is required.</param>
        /// <param name="extraAssemblyResolution">Location of an additional MSDE.CONFIG file for assembly resolution.</param>
        /// <param name="isolatedSubsystemConfigFile">Location of a config file to be provided for isolated subsystem.</param>
        /// <param name="subsystemRibbonPath">Specified path on the ribbon where buttons from isolated subsystem go.</param>
        /// <param name="profileName">Specifies persistence profile name to be used in the application.</param>
        /// <param name="usePrism">Specifies whether to use Prism as singleton framework.</param>
        public static void SetUpProcessIsolatedWrapper<T>(
            this Framework framework,
            IsolatedProcessPlatform isolatedPlatform = IsolatedProcessPlatform.CurrentCpu,
            string isolatedSubsystemLocation = null,
            string isolatingExecutableLocation = null,
            string additionalCommandLine = null,
            bool needsAssemblyResolution = false,
            string extraAssemblyResolution = null,
            string isolatedSubsystemConfigFile = null,
            string subsystemRibbonPath = null,
            string profileName = null,
            bool usePrism = false)
        {
            // Validate the type
            var typeInterfaces = typeof (T).GetInterfaces();
            if (typeof (T).GetInterface("IModule", true) == null &&
                typeof (T).GetInterface("IMSDesktopModule", true) == null &&
                !typeInterfaces.Contains(typeof (ISubsystemModuleCollection)))
            {
                throw new ArgumentException("Type argument T must be either of IModule or ISubsystemModuleCollection",
                                            "T");
            }

            framework.SetUpProcessIsolatedWrapperInternal(
                typeof (T).AssemblyQualifiedName,
                isolatedPlatform == IsolatedProcessPlatform.X86,
                isolatedSubsystemLocation,
                isolatingExecutableLocation,
                additionalCommandLine,
                false,
                needsAssemblyResolution,
                extraAssemblyResolution,
                isolatedSubsystemConfigFile,
                subsystemRibbonPath,
                profileName,
                usePrism);
        }

        /// <summary>
        /// Sets up and starts a module in its own isolated process.
        /// </summary>
        /// <param name="framework">Framework instance.</param>
        /// <param name="isolatedType">Full assembly-qualified name of the isolated type.</param>
        /// <param name="isolatedPlatform">Target platform for isolated process.</param>
        /// <param name="isolatedSubsystemLocation">Location of isolated subsystem.</param>
        /// <param name="isolatingExecutableLocation">Location of the isolated subsystem executable that will execute the subsystem.</param>
        /// <param name="additionalCommandLine">Additional command line to pass into subsystem.</param>
        /// <param name="needsAssemblyResolution">A flag indicating whether or not additional assembly resolution is required.</param>
        /// <param name="extraAssemblyResolution">Location of an additional MSDE.CONFIG file for assembly resolution.</param>
        /// <param name="isolatedSubsystemConfigFile">Location of a config file to be provided for isolated subsystem.</param>
        /// <param name="subsystemRibbonPath">Specified path on the ribbon where buttons from isolated subsystem go.</param>
        /// <param name="profileName">Specifies persistence profile name to be used in the application.</param>
        /// <param name="usePrism">Specifies whether to use Prism as singleton framework.</param>
        public static void SetUpProcessIsolatedWrapper(
            this Framework framework,
            string isolatedType,
            IsolatedProcessPlatform isolatedPlatform = IsolatedProcessPlatform.CurrentCpu,
            string isolatedSubsystemLocation = null,
            string isolatingExecutableLocation = null,
            string additionalCommandLine = null,
            bool needsAssemblyResolution = false,
            string extraAssemblyResolution = null,
            string isolatedSubsystemConfigFile = null,
            string subsystemRibbonPath = null,
            string profileName = null,
            bool usePrism = false)
        {
            framework.SetUpProcessIsolatedWrapperInternal(
                isolatedType,
                isolatedPlatform == IsolatedProcessPlatform.X86,
                isolatedSubsystemLocation,
                isolatingExecutableLocation,
                additionalCommandLine,
                false,
                needsAssemblyResolution,
                extraAssemblyResolution,
                isolatedSubsystemConfigFile,
                subsystemRibbonPath,
                profileName,
                usePrism);
        }

        internal static IEnumerable<int> RunningIsolatedProcesses
        {
            get { return IsolatedProcesses.Keys; }
        }

        private static void SetUpProcessIsolatedWrapperInternal(
            this Framework framework,
            string typeName,
            bool force32Bit,
            string isolatedSubsystemLocation,
            string isolatingExecutableLocation,
            string additionalCommandLine,
            bool requireKraken,
            bool needsAssemblyResolution,
            string extraAssemblyResolution,
            string isolatedSubsystemConfigFile,
            string subsystemRibbonPath,
            string profileName,
            bool usePrism)
        {
            var container = framework.Container;

            // Get the chrome manager
            var chromeManager = container.Resolve<IChromeManager>();

            // Get the chrome registry and register a window factory for windows from remote subsystems
            var chromeRegistry = (ChromeRegistry) container.Resolve<IChromeRegistry>();

            WindowFactoryHolder factoryHolder;
            if (!chromeRegistry.WindowFactories.TryGetValue(IsolatedProcessWindowFactoryName, out factoryHolder))
            {
                chromeRegistry.RegisterWindowFactory(IsolatedProcessWindowFactoryName, CreateIsolatedWindow, DehydrateWindow);
            }

            var controlTypeName = typeName;

            var layout=string.Empty;
            if (string.IsNullOrEmpty(profileName))
            {
                var profileService = container.Resolve<PersistenceProfileService>();
                profileName = profileService.CurrentProfile;
                var storage = container.Resolve<IPersistenceStorage>();
                var state = storage.LoadState(profileName);
                if (state != null)
                {
                    layout = state.ToString();
                }
            }

            var subsystemSite = new IsolatedSubsystemSite(
                chromeManager,
                container.Resolve<IModuleLoadInfos>(),
                container.Resolve<HostProcessModuleGroup>(),
                container.Resolve<ICommunicator>(),
                controlTypeName,
                isolatedSubsystemLocation,
                framework.Bootstrapper.Application.Name,
                requireKraken,
                null,
                additionalCommandLine,
                needsAssemblyResolution,
                extraAssemblyResolution,
                isolatedSubsystemConfigFile,
                subsystemRibbonPath,
                profileName,
                layout,
                usePrism);

            var endpointManager = new XProcEndpointManager(subsystemSite, isolatingExecutableLocation,
                                                           additionalCommandLine, force32Bit);
            endpointManager.ChildProcessStarted += endpointManager_ChildProcessStarted;
            endpointManager.ChildProcessCrashed += endpointManager_ChildProcessCrashed;
            endpointManager.StartProcess();
        }

        private static XDocument DehydrateWindow(IWindowViewContainer windowViewContainer)
        {
            var host = windowViewContainer.Content as SubsystemViewHost;
            if (host != null)
            {
                return host.Dehydrate();
            }

            return new XDocument();
        }

        public static IXProcAddInHost SetUp32BitProcessIsolatedWrapperFor<T>(this IWindowViewContainer iwcv,
                                                                             XDocument state,
                                                                             Type payloadType = null)
            where T : UserControl
        {
            var control = new XProcControl(typeof (T).AssemblyQualifiedName, state, null, true, string.Empty,
                                           payloadType: payloadType);

            EventHandler<Core.WindowEventArgs> handler = null;
            handler = delegate
                {
                    iwcv.Closed -= handler;
                    iwcv.ClosedQuietly -= handler;
                    ((XProcControl) iwcv.Content).OnIWVCClosed(iwcv, EventArgs.Empty);
                };

            iwcv.Closed += handler;
            iwcv.ClosedQuietly += handler;
            iwcv.Content = control;

            return control.AddInHost;
        }

        private static bool CreateIsolatedWindow(IWindowViewContainer container, XDocument state)
        {
            return true;
        }

        private static void endpointManager_ChildProcessCrashed(object sender, IsolatedProcessStartedEventArgs e)
        {
            object value;
            IsolatedProcesses.TryRemove(e.ProcessId, out value);
        }

        private static void endpointManager_ChildProcessStarted(object sender, IsolatedProcessStartedEventArgs e)
        {
            IsolatedProcesses.TryAdd(e.ProcessId, null);
        }
    }
}
