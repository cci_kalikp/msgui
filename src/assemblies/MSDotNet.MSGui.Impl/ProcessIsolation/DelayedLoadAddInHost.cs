﻿using System;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation;
using MSDotNet.MSGui.Impl.Isolation.Common;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
    internal sealed class DelayedLoadAddInHost : IXProcAddInHost
    {
        private IXProcAddInHost _host;
        public IXProcUiAddIn AddIn
        {
            get
            {
                if (_host != null)
                {
                    return _host.AddIn;
                }

                throw new InvalidOperationException("We shouldn't call any properties of methods until an add-in is available");
            }
        }

        public event EventHandler<MessageArrivedEventArgs> MessageArrived;

        internal void SetAddInHost(IXProcAddInHost host)
        {
            _host = host;
            _host.MessageArrived += MessageArrivedHandler;
        }

        void MessageArrivedHandler(object sender, MessageArrivedEventArgs e)
        {
            var handler = MessageArrived;
            if (handler != null)
            {
                handler(sender, e);
            }
        }
    }
}
