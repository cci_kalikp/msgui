﻿using System;
using System.IO;
using System.Threading;
using System.Xml.Linq;
using MSDotNet.MSGui.Impl.Isolation.Common;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
    internal sealed class AddInProxy : IXProcAddInHost
    {
        private readonly IXProcUiAddIn _addin;
        private readonly IsolatedSubsystemSite _subsystemSite;

        internal AddInProxy(IXProcUiAddIn addin, IsolatedSubsystemSite subsystemSite)
        {
            _subsystemSite = subsystemSite;
            _addin = addin;
        }

        public event EventHandler<MessageArrivedEventArgs> MessageArrived;

        public IXProcUiAddIn AddIn
        {
            get { return _addin; }
        }

        internal void QueueDestroy()
        {
            QueueCallAddin(() => _addin.ShutDown());
        }

        internal void QueuePaint(byte[] underlyingContentBytes)
        {
            QueueCallAddin(() => _addin.Paint(underlyingContentBytes));
        }

        internal XDocument Save()
        {
            XDocument xdoc;

            var xml = CallAddinSync(() => _addin.Save());
            using (var reader = new StringReader(xml))
            {
                xdoc = XDocument.Load(reader);
            }

            return xdoc;
        }

        internal void Load(XDocument document)
        {
            var xml = document.ToString();
            QueueCallAddin(() => _addin.Load(xml));
        }

        internal void TriggerMessageArrived(object message)
        {
            var handler = MessageArrived;
            if (handler != null)
            {
                handler(this, new MessageArrivedEventArgs(message));
            }
        }

        private void QueueCallAddin(Action action)
        {
            ThreadPool.QueueUserWorkItem(o =>
                {

                    try
                    {
                        action();
                    }
                    catch (Exception pex)
                    {
                        // Trigger process closed exception - pipe should be open.
                        _subsystemSite.OnIsolatedProcessKilled();
                    }
                });
        }

        private T CallAddinSync<T>(Func<T> action)
            where T:class
        {
            try
            {
                var result = action();
                return result;
            }
            catch (PipeException pex)
            {
                // Trigger process closed exception - pipe should be open.
                _subsystemSite.OnIsolatedProcessKilled();
                return null;
            }
        }
    }
}
