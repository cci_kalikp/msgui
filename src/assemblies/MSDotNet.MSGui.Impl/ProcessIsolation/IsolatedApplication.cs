﻿using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
    internal sealed class IsolatedApplication : Application.Application
    {
        private readonly IsolatedSubsystemSettings _settings;

        public IsolatedApplication(IsolatedSubsystemSettings settings)
        {
            _settings = settings;
        }

        public IsolatedSubsystemSettings IsolatedSubsystemSettings
        {
            get { return _settings; }
        }

        public override Dispatcher MainWindowDispatcher
        {
            get { return Dispatcher.CurrentDispatcher; }
        }
    }
}
