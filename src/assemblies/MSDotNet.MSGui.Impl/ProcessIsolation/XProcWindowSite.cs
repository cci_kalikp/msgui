﻿using System;
using System.IO;
using System.Runtime.Remoting.Lifetime;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions;
using MSDotNet.MSGui.Impl.Isolation.Common;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
    /// <summary>
    /// The AddIn object attaches itself to this "host" object. It is separate from IxProcAddInHost because
    /// it needs to be derived from MBR to be remotable.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    internal sealed class XProcWindowSite : IXProcWindowSite, ISponsor
    {
        private readonly BinaryFormatter _formatter = new BinaryFormatter();
        private readonly Dispatcher _dispatcher;
        private readonly SubsystemViewHost _viewHost;

        internal event EventHandler<MessageArrivedEventArgs> MessageArrived;

        internal XProcWindowSite(SubsystemViewHost viewHost)
        {
            _viewHost = viewHost;
            _dispatcher = Dispatcher.CurrentDispatcher;
        }

        /*
        public override object InitializeLifetimeService()
        {
            // Set up the host as the sponsor of the lease.
            var lease = (ILease) base.InitializeLifetimeService();
            if (lease.CurrentState == LeaseState.Initial)
            {
                lease.Register(this);
            }

            return lease;
        }*/

        #region IXProcAddInSite implemenentation

        // Note: These calls arrive on thread pool threads. UI work must be done on the UI thread.
        // We switch easily via the Dispatcher.

        public bool TabOut(TraversalRequest request)
        {
            return
                (bool)
                _dispatcher.Invoke((DispatcherOperationCallback) (tr => _viewHost.MoveFocus((TraversalRequest) tr)),
                                        request);
        }

        public bool TranslateAccelerator(MSG msg)
        {
            return (bool) _dispatcher.Invoke((DispatcherOperationCallback) delegate(object m)
                {
                    IKeyboardInputSink keyboardSink = _viewHost;

                    // We delegate key processing to the containing HwndSource, via IKIS. It will see that 
                    // a child window ("sink") has focus and will do special routing of the input events using
                    // that child window as the forced target. Thus, any element up to the root visual has a 
                    // chance to see and handle the events.
                    if (!(keyboardSink).HasFocusWithin())
                    {
                        return false;
                    }

                    var hostSink = (IKeyboardInputSink)PresentationSource.FromVisual(_viewHost);
                    var m2 = (MSG) m;

                    // - IKIS has special rules about which messages are passed to which method.
                    // Here we assume the add-in bubbles only raw key messages plus mnemonic keys (Alt+something).
                    // - Even though the call has hopped from thread to thread, Keyboard.Modifiers produces
                    // correct result here because the input queue and states of the host's and add-in's UI
                    // threads are synchronized. (The window manager automatically attaches thread input when
                    // a window is parented to a cross-thread one.)
                    if (m2.message == Win32.WM_SYSCHAR || m2.message == Win32.WM_SYSDEADCHAR)
                        return hostSink.OnMnemonic(ref m2, Keyboard.Modifiers);

                    return hostSink.TranslateAccelerator(ref m2, Keyboard.Modifiers);
                }, msg);
        }

        public void Send(byte[] payload)
        {
            var handler = MessageArrived;
            if (handler != null)
            {
                object actualPayload;
                using (var stream = new MemoryStream(payload))
                {
                    actualPayload = _formatter.Deserialize(stream);
                }

                handler(this, new MessageArrivedEventArgs(actualPayload));
            }
        }

        #endregion

        TimeSpan ISponsor.Renewal(ILease lease)
        {
            return /*Handle == IntPtr.Zero ? TimeSpan.Zero : */lease.RenewOnCallTime;
        }
    }
}
