﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MorganStanley.MSDotNet.MSGui.Controls.Controls;
using MorganStanley.MSDotNet.MSGui.Controls.Interfaces;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.LauncherBar;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
    internal sealed class SubsystemControlHost : SubsystemViewHost, IHwndHostControl
    {
        private bool _canRenderChild;
        private readonly AddInProxy _addinProxy;

        internal SubsystemControlHost(IntPtr hwnd, AddInProxy addinProxy) : base(hwnd, addinProxy)
        {
            _addinProxy = addinProxy;
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            var rootContainer = ChromeManagerBase.Instance.GetRootContainer();
            var ribbonArea = rootContainer as RibbonRootArea;
            if (ribbonArea != null)
            {
                var ribbon = ribbonArea.Ribbon as CustomTopRibbon;
                if (ribbon != null)
                {
                    ribbon.AddHostControl(this);
                }
            }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            _canRenderChild = true;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (IsVisible)
            {
                RequestControlRepaint();
            }
        }

        protected override void OnWindowPositionChanged(Rect rcBoundingBox)
        {
            base.OnWindowPositionChanged(rcBoundingBox);
            if (IsVisible)
            {
                RequestControlRepaint();
            }
        }

        private void RequestControlRepaint()
        {
            if (_canRenderChild)
            {
                var backgroundVisual = GetBackgroundVisual();
                var mainWindowBitmap = new RenderTargetBitmap((int) backgroundVisual.RenderSize.Width,
                                                              (int) backgroundVisual.RenderSize.Height, 96, 96,
                                                              PixelFormats.Pbgra32);

                mainWindowBitmap.Render(backgroundVisual);
                var topLeftPoint = TranslatePoint(new Point(0, 0), backgroundVisual);

                if (topLeftPoint.X > 0 && topLeftPoint.Y > 0
                    && RenderSize.Width > 0 && RenderSize.Height > 0)
                {
                    var rect = new Int32Rect((int) topLeftPoint.X, (int) topLeftPoint.Y, (int) RenderSize.Width,
                                             (int) RenderSize.Height);

                    var croppedBitmap = new CroppedBitmap(mainWindowBitmap, rect);

                    // Serialize it into array of bytes
                    var encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(croppedBitmap));

                    byte[] imageBytes;
                    using (var stream = new MemoryStream())
                    {
                        encoder.Save(stream);
                        imageBytes = stream.ToArray();
                    }

                    _addinProxy.QueuePaint(imageBytes);
                }
            }
        }

        private static UIElement GetBackgroundVisual()
        {
            var rootContainer = ChromeManagerBase.Instance.GetRootContainer();
            var ribbonArea = rootContainer as RibbonRootArea;
            if (ribbonArea != null)
            {
                var ribbon = ribbonArea.Ribbon as CustomTopRibbon;
                if (ribbon != null && ribbon.IsPopupVisible())
                {
                    return ribbon.RootElement;
                }
            }

            var launcherBar = rootContainer as LauncherBarRootArea;
            if (launcherBar != null)
            {
                // return (UIElement)launcherBar.Content;
            }

            return ChromeManagerBase.Instance.Shell.MainWindow;
        }

        bool IHwndHostControl.IsVisible
        {
            get { return IsVisible; }
        }

        bool IHwndHostControl.IsMouseOnControl(Point point)
        {
            var result = Win32.SendMessage(
                RemoteHwnd,
                (int)Win32.WM_NCHITTEST,
                0,
                Win32.MAKELPARAM((int)point.X, (int)point.Y));

            // We got HTNOWHERE - meaning that the point isn't on the control.
            return result;
        }

        void IHwndHostControl.MouseMove(MouseEventArgs eventArgs)
        {
            var point = eventArgs.GetPosition(this);
            Win32.SendMessage(RemoteHwnd, (int)Win32.WM_MOUSEMOVE, (int)0, Win32.MAKELPARAM((int)point.X, (int)point.Y));
        }

        void IHwndHostControl.MouseEnter(MouseEventArgs eventArgs)
        {
        }

        void IHwndHostControl.MouseLeave(MouseEventArgs eventArgs)
        {
        }

        void IHwndHostControl.MouseLeftButtonUp(MouseEventArgs eventArgs)
        {
            // TODO (hreckin): Add handling of keys here perhaps
            var point = eventArgs.GetPosition(this);
            Win32.SendMessage(RemoteHwnd, (int)Win32.WM_LBUTTONDOWN, 0, Win32.MAKELPARAM((int)point.X, (int)point.Y));
        }

        void IHwndHostControl.MouseLeftButtonDown(MouseEventArgs eventArgs)
        {
            // TODO (hreckin): Add handling of keys here perhaps
            var point = eventArgs.GetPosition(this);
            Win32.SendMessage(RemoteHwnd, (int)Win32.WM_LBUTTONDOWN, 0, Win32.MAKELPARAM((int)point.X, (int)point.Y));
        }
    }
}
