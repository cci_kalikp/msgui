﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Threading;
using MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation;
using MorganStanley.MSDotNet.Runtime;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
    internal sealed class XProcEndpointManager
    {
        private const string RelativeExecutableDistLocation = @"..\bin\MSDesktop.Isolation\";

        private static int _addinCounter;
        private static int _channelCount;

        private ServiceHost _serviceHost;
        private readonly string _baseUri;
        private readonly IsolatedSubsystemSite _site;
        private readonly string _isolatingExecutableLocation;
        private readonly string _additionalCommandLine;
        private readonly bool _force32Bit;

        internal XProcEndpointManager(IsolatedSubsystemSite site, string isolatingExecutableLocation, string additionalCommandLine, bool force32Bit)
        {
            _force32Bit = force32Bit;
            _additionalCommandLine = additionalCommandLine;
            _isolatingExecutableLocation = isolatingExecutableLocation;
            _site = site;
            _baseUri = RegisterServerChannel("WPFAddInHost");
        }

        internal void StartProcess()
        {
            ThreadPool.QueueUserWorkItem(o => StartIsolatingProcess());
        }

        private string GetIsolatingExecutablePath()
        {
            var hostExecutable = _force32Bit
                                     ? @"MSDesktop.Isolation.X86.exe"
                                     : @"MSDesktop.Isolation.exe";

            string executablePath;
            if (!string.IsNullOrEmpty(_isolatingExecutableLocation))
            {
                executablePath = Path.Combine(_isolatingExecutableLocation, hostExecutable);
            }
            else
            {
                // If the location for isolating executable is not specified, probe current directory and dist directory
                var msdesktopAsmPath = Path.GetDirectoryName(typeof (Framework).Assembly.Location);
                executablePath = Path.Combine(msdesktopAsmPath, hostExecutable);
                if (!File.Exists(executablePath))
                {
                    // Try in relative location
                    var directoryName =
                        new DirectoryInfo(Path.Combine(msdesktopAsmPath, RelativeExecutableDistLocation)).FullName;
                    executablePath = Path.Combine(directoryName, hostExecutable);
                    if (!File.Exists(executablePath))
                    {
                        var errorMessage = string.Format(
                            "Could not locate the isolating executable neither at '{0}' nor at '{1}'",
                            msdesktopAsmPath,
                            directoryName);

                        throw new FileNotFoundException(errorMessage);
                    }
                }
            }


            return executablePath;
        }

        private void StartIsolatingProcess()
        {
            // Register the host object with RemotingServices so that the add-in can connect to it.
            // The URI is passed to addIn.exe's command line.
            var currentAddinCounter = Interlocked.Increment(ref _addinCounter);

            var addinSiteUri = "host" + currentAddinCounter;

            // RemotingServices.Marshal(_site, addinSiteUri);
            addinSiteUri = string.Format("{0}/{1}", _baseUri, addinSiteUri);

            // Register this new host with its new URL.
            RegisterSite(addinSiteUri, _site);

            var subsystemSettings = _site.GetIsolatedSubsystemSettings();

            var startline = !string.IsNullOrEmpty(_additionalCommandLine) 
                                   ? string.Format("{0} {1}", addinSiteUri, _additionalCommandLine) 
                                   : addinSiteUri;

            if (subsystemSettings.IsAdditionalArRequired)
            {
                subsystemSettings.AssemblyResolutionFiles = GetArFiles(_site.ExtraAr).ToArray();
            }

            var addinProcess = Process.Start(GetIsolatingExecutablePath(), startline);
            if (addinProcess != null)
            {
                addinProcess.EnableRaisingEvents = true;
                EventHandler handler = null;
                handler = delegate
                    {
                        addinProcess.Exited -= handler;
                        InvokeChildProcessCrashed(addinProcess.Id);
                        if (!_site.IsControl)
                        {
                            _site.OnIsolatedProcessKilled();
                        }
                    };
                addinProcess.Exited += handler;

                // Trigger started event
                var startedHandler = ChildProcessStarted;
                if (startedHandler != null)
                {
                    var startedEventArgs = new IsolatedProcessStartedEventArgs(addinProcess.Id, null);
                    startedHandler(this, startedEventArgs);
                }
            }
        }

        private static IEnumerable<string> GetArFiles(string extraAr)
        {
            var msdelist = new HashSet<string>();

            if (!string.IsNullOrEmpty(extraAr))
            {
                msdelist.Add(extraAr);
            }

            foreach (var assembly in AssemblyResolver.GetAssemblies())
            {
                // We need to init the variables
                //Console.WriteLine(assembly);
            }

            var fieldInfo = typeof (AssemblyResolver).GetField("m_proxy",
                                                               BindingFlags.NonPublic | BindingFlags.Static);
            if (fieldInfo != null)
            {
                var proxy = fieldInfo.GetValue(null);
                var field = proxy.GetType().GetField("m_impl", BindingFlags.NonPublic | BindingFlags.Instance);
                if (field != null)
                {
                    var impl = field.GetValue(proxy);
                    var info = impl.GetType()
                                   .GetField("m_dependencyManager", BindingFlags.NonPublic | BindingFlags.Instance);
                    if (info != null)
                    {
                        var dep = info.GetValue(impl);
                        var infofield = dep.GetType()
                                           .GetField("m_dependencies",
                                                     BindingFlags.NonPublic | BindingFlags.Instance);
                        if (infofield != null)
                        {
                            var dependencies = infofield.GetValue(dep) as ArrayList;
                            if (dependencies != null)
                            {
                                foreach (
                                    var path in
                                        (from object dependency in dependencies
                                         select
                                             dependency.GetType()
                                                       .GetProperty("IncludePath")
                                                       .GetValue(dependency, null))
                                            .OfType<ArrayList>()
                                            .SelectMany(paths => paths
                                                                     .Cast<string>()
                                                                     .Where(path => !msdelist.Contains(path))))
                                {
                                    msdelist.Add(path);
                                }
                            }
                        }
                    }
                }
            }

            return msdelist;
        }

        internal event EventHandler<IsolatedProcessStartedEventArgs> ChildProcessCrashed;
        internal event EventHandler<IsolatedProcessStartedEventArgs> ChildProcessStarted;

        internal void InvokeChildProcessCrashed(int processId)
        {
            var handler = ChildProcessCrashed;
            if (handler != null)
            {
                handler(this, new IsolatedProcessStartedEventArgs(processId, null));
            }
        }

        // TODO (ahrechkin): This needs to be shared between 
        // isolated process assembly and this one.
        private string RegisterServerChannel(string baseServerName)
        {
            var currentChannelCount = Interlocked.Increment(ref _channelCount);

            // Configure WCF for both server and client here
            var channelUri = string.Format("net.pipe://localhost/{0}-{1}-{2}", baseServerName, Process.GetCurrentProcess().Id,
                                           currentChannelCount);

            return channelUri;
        }

        private void RegisterSite(string hostUri, IsolatedSubsystemSite site)
        {
            // configure binding
            var binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None)
            {
                Name = hostUri
            };

            _serviceHost = new ServiceHost(site, new Uri(hostUri));
            _serviceHost.AddServiceEndpoint(typeof(IIsolatedSubsystemSite), binding, hostUri);
            _serviceHost.Open();

        }
    }
}
