﻿using System;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
    internal class WindowAvailableEventArgs : EventArgs
    {
        private readonly IntPtr _hwnd;
        private readonly InitialWindowParametersProxy _initialParametersProxy;
        private readonly string _title;
        private readonly IXProcAddInHost _addInHost;

        public WindowAvailableEventArgs(IXProcAddInHost addInHost, IntPtr hwnd)
            : this(addInHost, hwnd, null)
        {
        }

        public WindowAvailableEventArgs(IXProcAddInHost addInHost, IntPtr hwnd, InitialWindowParametersProxy initialParametersProxy,
                                        string title = null)
        {
            _addInHost = addInHost;
            _title = title;
            _initialParametersProxy = initialParametersProxy;
            _hwnd = hwnd;
        }

        public IntPtr Hwnd
        {
            get { return _hwnd; }
        }

        public InitialWindowParametersProxy InitialParametersProxy
        {
            get { return _initialParametersProxy; }
        }

        public string Title
        {
            get { return _title; }
        }

        public IXProcAddInHost AddInHost
        {
            get { return _addInHost; }
        }
    }
}
