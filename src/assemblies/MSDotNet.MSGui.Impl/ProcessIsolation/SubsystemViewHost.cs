﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
    internal class SubsystemViewHost : HwndHost
    {
        private readonly IntPtr _remoteHwnd;
        private readonly AddInProxy _addinProxy;

        public SubsystemViewHost(IntPtr hwnd, AddInProxy addinProxy)
        {
            _addinProxy = addinProxy;
            _remoteHwnd = hwnd;
        }

        protected IntPtr RemoteHwnd
        {
            get { return _remoteHwnd; }
        }

        public XDocument Dehydrate()
        {
            return _addinProxy.Save();
        }

        protected override HandleRef BuildWindowCore(HandleRef hwndParent)
        {
            var parentHandle = hwndParent.Handle;
            IntPtr childHwnd;

            if (Win32.IsWindow(_remoteHwnd))
            {
                childHwnd = _remoteHwnd;
            }
            else
            {
                // The isolated process crashed, but rendering of parent control will still require valid HWND, once we are here.
                // Create a sentinel Control, and get its HWND
                var control = new Control
                    {
                        Width = 0,
                        Height = 0,
                    };

                var hwndSource = new HwndSource(new HwndSourceParameters())
                    {
                        RootVisual = control,
                    };

                // Set the new window style
                childHwnd = hwndSource.Handle;
                var presentStyle = Win32.GetWindowLong(childHwnd, Win32.GWL_STYLE);
                presentStyle |= (int)Win32.WS_CHILD;
                const uint u = (Win32.WS_POPUP | Win32.WS_BORDER | Win32.WS_CAPTION);
                presentStyle = (int) (presentStyle & (~u));
                Win32.SetWindowLong(childHwnd, Win32.GWL_STYLE, presentStyle);
            }

            // Check to see if _remoteHwd is still valid. This can happen if the child process is terminated
            Win32.SetParent(childHwnd, parentHandle);
            return new HandleRef(this, childHwnd);
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            InvalidateVisual();
        }

        protected override void OnWindowPositionChanged(Rect rcBoundingBox)
        {
            // See http://support.microsoft.com/kb/969728 for explanation
            Win32.SetWindowPos(Handle, IntPtr.Zero,
                                                (int)rcBoundingBox.X,
                                                (int)rcBoundingBox.Y,
                                                (int)rcBoundingBox.Width,
                                                (int)rcBoundingBox.Height,
                                                Win32.SWP_ASYNCWINDOWPOS
                                                | Win32.SWP_NOZORDER
                                                | Win32.SWP_NOACTIVATE);
        }

        protected override bool OnMnemonicCore(ref MSG msg, System.Windows.Input.ModifierKeys modifiers)
        {
            //TODO: Optionally, forward mnemonics "down" to the add-in's HwndSource (via IKIS).
            // This would look much like TranslateAccelerator but going in the opposite direction.
            return false;
        }

        protected override void DestroyWindowCore(HandleRef hwnd)
        {
            _addinProxy.QueueDestroy();
        }
    }
}
