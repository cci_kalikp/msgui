﻿using System;
using MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
    internal sealed class IsolatedProcessStartedEventArgs : EventArgs
    {
        private readonly int _processId;
        private readonly IsolatedSubsystemSettings _settings;

        public IsolatedProcessStartedEventArgs(int processId, IsolatedSubsystemSettings settings)
        {
            _settings = settings;
            _processId = processId;
        }

        public int ProcessId
        {
            get { return _processId; }
        }

        public IsolatedSubsystemSettings Settings
        {
            get { return _settings; }
        }
    }
}
