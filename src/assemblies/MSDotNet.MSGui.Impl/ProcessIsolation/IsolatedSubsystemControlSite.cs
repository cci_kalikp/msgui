﻿using System;
using System.ServiceModel;
using System.Threading;
using System.Windows;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    internal sealed class IsolatedSubsystemControlSite : IsolatedSubsystemSite
    {
        private readonly XDocument _stateToLoad;
        private readonly Type _payloadType;

        internal event EventHandler<FrameworkElementAddedEventArgs> WindowAvailable;

        internal IsolatedSubsystemControlSite(XDocument stateToLoad, string controlType, Type payloadType,
                                            string applicationName, string isolatedSubsystemLocation,
                                            string additionalCommandLine, bool needsAr, string extraAr)
            : base(controlType, isolatedSubsystemLocation, applicationName,
                   additionalCommandLine: additionalCommandLine, needsAr: needsAr, extraAr: extraAr)
        {
            _payloadType = payloadType;
            _stateToLoad = stateToLoad;
        }

        public override string GetPayloadType()
        {
            return _payloadType == null ? null : _payloadType.AssemblyQualifiedName;
        }

        public override bool IsControl
        {
            get { return true; }
        }

        protected override void PostCreateWindow(IXProcAddInHost addIn, IntPtr hwnd, FrameworkElement element)
        {
            if (_stateToLoad != null)
            {
                ThreadPool.QueueUserWorkItem(o => addIn.AddIn.Load(_stateToLoad.ToString()));
            }

            InvokeWindowAvailable(addIn, hwnd, element);
        }

        protected override IWindowViewContainer CreateWindow(string factoryId, InitialWindowParameters initialParameters)
        {
            return new WindowViewContainer(initialParameters, ProcessIsolationExtensions.IsolatedProcessWindowFactoryName);
        }

        private void InvokeWindowAvailable(IXProcAddInHost addIn, IntPtr hwnd, FrameworkElement element)
        {
            var eventArgs = new FrameworkElementAddedEventArgs(addIn, element, hwnd);

            var handler = WindowAvailable;
            if (handler != null)
            {
                handler(this, eventArgs);
            }
        }
    }
}
