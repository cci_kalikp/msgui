﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Lifetime;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.Windows;
using System.Windows.Threading;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MSDotNet.MSGui.Impl.Isolation.Common;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, IncludeExceptionDetailInFaults = true)]
    internal class IsolatedSubsystemSite : IIsolatedSubsystemSite, ISponsor
    {
        private const string BaseServerName = "XProcWindowSite";

        private readonly IList<IModuleLoadInfo> _isolatedModules = new List<IModuleLoadInfo>();

        private readonly Dispatcher _dispatcher;
        private readonly IChromeManager _chromeManager;
        private readonly IsolatedSubsystemSettings _subsystemSettings;
        private readonly IModuleLoadInfos _moduleLoadInfos;
        private readonly string _extraAr;
        private readonly HostProcessModuleGroup _moduleGroup;
        private readonly ICommunicator _communicator;
        private readonly IFormatter _formatter;
        private IXProcCommunicationAddIn _communicationAddIn;

        protected IsolatedSubsystemSite(string isolatedType, string isolatedSubsystemLocation,
                                        string applicationName, bool needsKraken = false, string krakenCpsConfig = null,
                                        string additionalCommandLine = null, bool needsAr = false, string extraAr = null,
                                        string isolatedSubsystemConfigFile = null, string subsystemRibbonPath = null,
                                        string profileName = null, string layout = null,
                                        bool usePrism = false)
        {
            _extraAr = extraAr;
            _dispatcher = Dispatcher.CurrentDispatcher;
            _formatter = new BinaryFormatter();
            _subsystemSettings = new IsolatedSubsystemSettings
                {
                    HostProcessId = Process.GetCurrentProcess().Id
                };

            if (string.IsNullOrEmpty(isolatedSubsystemLocation))
            {
                isolatedSubsystemLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }
            else
            {
                if (!string.Equals(
                    TrimPath(isolatedSubsystemLocation),
                    TrimPath(AppDomain.CurrentDomain.BaseDirectory),
                    StringComparison.OrdinalIgnoreCase))
                { 
                    AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
                }
            }

            _subsystemSettings.IsolatedSubsystemLocation = isolatedSubsystemLocation;
            _subsystemSettings.IsolatedType = isolatedType;
            _subsystemSettings.CpsConfig = krakenCpsConfig;
            _subsystemSettings.IsCpsRequired = needsKraken;
            _subsystemSettings.ApplicationName = applicationName;
            _subsystemSettings.AdditionalCommandLine = additionalCommandLine;
            _subsystemSettings.IsAdditionalArRequired = needsAr;
            _subsystemSettings.ProfileName = profileName;
            _subsystemSettings.Layout = layout;
            _subsystemSettings.IsolatedSubsystemConfigFile = isolatedSubsystemConfigFile;
            _subsystemSettings.SubsystemRibbonPath = subsystemRibbonPath;
            _subsystemSettings.UsePrism = usePrism;
        }


        internal IsolatedSubsystemSite(IChromeManager chromeManager,
                                       IModuleLoadInfos moduleLoadInfos,
                                       HostProcessModuleGroup moduleGroup,
                                       ICommunicator communicator,
                                       string isolatedType,
                                       string isolatedSubsystemLocation,
                                       string applicationName, bool needsKraken,
                                       string krakenCpsConfig, string additionalCommandLine,
                                       bool needsAr, string extraAr,
                                       string isolatedSubsystemConfigFile,
                                       string subsystemRibbonPath,
                                       string profileName,
                                       string layout,
                                       bool usePrism)
            : this(isolatedType, isolatedSubsystemLocation, applicationName, needsKraken, krakenCpsConfig,
                   additionalCommandLine, needsAr, extraAr, isolatedSubsystemConfigFile, subsystemRibbonPath,
                   profileName, layout, usePrism)
        {
            _communicator = communicator;
            _moduleGroup = moduleGroup;
            _chromeManager = chromeManager;
            _moduleLoadInfos = moduleLoadInfos; 
        }

        private static string TrimPath(string path)
        {
            path = Path.GetFullPath(path).Trim();
            if (path[path.Length - 1] == '\\')
            {
                path = path.Substring(0, path.Length - 1);
            }

            return path;
        }
        
        Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            if (args.Name.Contains(".resources,"))
                return null;
            var asm = new AssemblyName(args.Name);
            try
            {
                return ResolveAssembly(asm);
            }
            catch (InvalidOperationException)
            { 
                return null;
            }
        }

        private Assembly ResolveAssembly(AssemblyName asmName)
        {
            var filePathBase = string.Format(@"{0}\{1}", _subsystemSettings.IsolatedSubsystemLocation, asmName.Name);
            string[] filesToTry = { filePathBase + ".exe", filePathBase + ".dll" };

            var asm = filesToTry
                .Where(File.Exists)
                .Select(Assembly.LoadFrom)
                .FirstOrDefault();

            return asm;
        }

        internal string ExtraAr
        {
            get { return _extraAr; }
        }

        public virtual string GetPayloadType()
        {
            return typeof (object).AssemblyQualifiedName;
        }

        public IsolatedSubsystemSettings GetIsolatedSubsystemSettings()
        {
            return _subsystemSettings;
        }


        public void SubscribeM2M(string messageType, string communicationAddInUrl)
        {
            if (_communicator != null)
            {
                var type = Type.GetType(messageType, true);

                if (_communicationAddIn == null)
                {
                    _communicationAddIn =
                        ProcessIsolationUtility.InitClientChannel<IXProcCommunicationAddIn>(communicationAddInUrl);
                }

                MessagingReflectionUtilities.Subscribe(_communicator, type,
                                                       new Action<object>(OnMessageForIsolatedProcess));
            }
        }

        public void PublishM2M(byte[] objectBytestream)
        {
            object message;
            using (var stream = new MemoryStream(objectBytestream))
            {
                message = _formatter.Deserialize(stream);
            }

            if (_communicator != null)
            {
                var type = message.GetType();
                var publisher = MessagingReflectionUtilities.GetPublisher(_communicator, type);
                MessagingReflectionUtilities.Publish(publisher, message);
            }
        }

        private void OnMessageForIsolatedProcess(object message)
        {
            // Pass the message to the isolated process
            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _formatter.Serialize(stream, message);
                bytes = stream.GetBuffer();
            }

            _communicationAddIn.Receive(bytes);
        }

        public virtual bool IsControl
        {
            get { return false; }
        }

        internal void OnIsolatedProcessKilled()
        {
            foreach (ModuleLoadInfo isolatedModule in _isolatedModules)
            {
                if (isolatedModule.Status == ModuleStatus.Loaded ||
                    isolatedModule.Status == ModuleStatus.NotLoaded)
                {
                    isolatedModule.Status = ModuleStatus.ProcessExited;
                }
            }
        }

        public void SetModuleStatus(string moduleTypeName, ModuleStatus status, Exception exception = null)
        {
            if (_moduleLoadInfos == null || IsControl) return;

            var info = _moduleLoadInfos.GetModuleLoadInfo(moduleTypeName) ??
                       _moduleLoadInfos.AddModuleLoadInfo(moduleTypeName);

            if (!_isolatedModules.Contains(info))
            {
                _isolatedModules.Add(info);
            }

            info.OutOfProcess = true;
            info.Status = status;
            info.Error = exception;
            if (info.Status == ModuleStatus.NotLoaded)
            {
                _moduleLoadInfos.StartLoading(info);
            }
            else if (info.Status == ModuleStatus.Loaded)
            {
                _moduleLoadInfos.EndLoading(info);
            }
        }

        public string AddWidget(string addinUri, string widgetFactoryId, IntPtr hwnd, string parentFactoryId,
                                string widgetXaml)
        {
            // TODO(hrechkin): Don't hold threadpool threads while UI thread processes its logic.
            var addin = ProcessIsolationUtility.InitClientChannel<IXProcUiAddIn>(addinUri);
            var windowSiteWrapper = new XProcWidgetSiteClosure(addin, this, _chromeManager, parentFactoryId,
                                                               widgetFactoryId, hwnd);

            var windowSite =
                (XProcWindowSite) _dispatcher.Invoke((XProcWindowSiteOperation) AddWidgetProc, windowSiteWrapper);
            return ProcessIsolationUtility.RegisterServerChannel<IXProcWindowSite>(BaseServerName, windowSite);
        }

        private static object AddWidgetProc(object obj)
        {
            var closureInternal = (XProcWidgetSiteClosure) obj;
            var proxy = new AddInProxy(closureInternal.Addin, closureInternal.SubsystemSite);
            var parent = closureInternal.ChromeManager.GetWidget(closureInternal.ParentFactoryId);
            var host = new SubsystemControlHost(closureInternal.Hwnd, proxy);
            var windowSite = new XProcWindowSite(host);

            var parameters = new InitialExistingElementWidgetParameters(host);
            closureInternal.ChromeManager.AddWidget(closureInternal.WidgetFactoryId, parameters, parent);

            return windowSite;
        }

        private static object PlaceWidgetProc(object obj)
        {
            var wrapperInternal = (XProcWidgetSiteClosure) obj;
            var proxy = new AddInProxy(wrapperInternal.Addin, wrapperInternal.SubsystemSite);
            var host = new SubsystemControlHost(wrapperInternal.Hwnd, proxy);
            var parameters = new InitialExistingElementWidgetParameters(host);
            var windowSite = new XProcWindowSite(host);

            wrapperInternal.ChromeManager.PlaceWidget(wrapperInternal.WidgetFactoryId, wrapperInternal.Root,
                                                      wrapperInternal.Location, parameters);

            return windowSite;
        }

        public string PlaceWidget(string addinUri, string widgetFactoryId, IntPtr hwnd, string root,
                                  string location,
                                  string widgetXaml)
        {
            // TODO(hrechkin): Don't hold threadpool threads while UI thread processes its logic.
            var addIn = ProcessIsolationUtility.InitClientChannel<IXProcUiAddIn>(addinUri);
            var windowSiteWrapper = new XProcWidgetSiteClosure(addIn, this, _chromeManager, widgetFactoryId, root,
                                                               location, hwnd);

            var windowSite =
                (XProcWindowSite) _dispatcher.Invoke((XProcWindowSiteOperation) PlaceWidgetProc, windowSiteWrapper);

            return ProcessIsolationUtility.RegisterServerChannel<IXProcWindowSite>(BaseServerName, windowSite);
        }

        public string AddWindow(string addinUri, IntPtr hwnd,
                                InitialWindowParametersProxy initialParametersProxy,
                                string windowTitle)
        {
            // TODO(hrechkin): Don't hold threadpool threads while UI thread processes its logic.
            var addIn = ProcessIsolationUtility.InitClientChannel<IXProcUiAddIn>(addinUri);
            var windowSiteWrapper = new XProcWindowSiteClosure(addIn, hwnd, this, windowTitle, initialParametersProxy);

            var windowSite =
                (XProcWindowSite) _dispatcher.Invoke((XProcWindowSiteOperation) AddWindowProc, windowSiteWrapper);

            return ProcessIsolationUtility.RegisterServerChannel<IXProcWindowSite>(BaseServerName, windowSite);
        }

        private static object AddWindowProc(object obj)
        {
            var windowSiteWrapper = (XProcWindowSiteClosure) obj;
            var proxy = new AddInProxy(windowSiteWrapper.Addin, windowSiteWrapper.SubsystemSite);
            var host = new SubsystemViewHost(windowSiteWrapper.Hwnd, proxy);
            var initialParameters = new InitialWindowParameters();

            var viewId = string.Empty;
            var initialParametersProxy = windowSiteWrapper.InitialWindowParametersProxy;

            if (initialParametersProxy != null)
            {
                initialParameters.Transient = initialParametersProxy.Transient;
                initialParameters.InitialLocation = initialParametersProxy.InitialLocation;
                initialParameters.UseDockManager = initialParametersProxy.UseDockManager;
                initialParameters.ShowFlashBorder = initialParametersProxy.ShowFlashBorder;
                initialParameters.IsModal = initialParametersProxy.IsModal;
                initialParameters.SingletonKey = initialParametersProxy.SingletonKey;
                initialParameters.SizingMethod = initialParametersProxy.SizingMethod;
                initialParameters.Topmost = initialParametersProxy.Topmost;
                initialParameters.ResizeMode = initialParametersProxy.ResizeMode;
                initialParameters.EnforceSizeRestrictions = initialParametersProxy.EnforceSizeRestrictions;
                viewId = initialParametersProxy.ViewId;
            }

            var windowSite = new XProcWindowSite(host);
            EventHandler<MessageArrivedEventArgs> onMessageArrived =
                (sender, args) => proxy.TriggerMessageArrived(args.Message);
            windowSite.MessageArrived += onMessageArrived;

            var window = windowSiteWrapper.SubsystemSite.CreateWindow(viewId, initialParameters);
            EventHandler<WindowEventArgs> onWindowClose = null;
            onWindowClose = (sender, args) =>
                {
                    windowSite.MessageArrived -= onMessageArrived;
                    window.Closed -= onWindowClose;
                };

            window.Closed += onWindowClose;
            window.Content = host;
            window.Title = windowSiteWrapper.WindowTitle;

            windowSiteWrapper.SubsystemSite.PostCreateWindow(proxy, windowSiteWrapper.Hwnd, host);

            return windowSite;
        }

        public void ViewLoadedCompletely(string connectionInfo)
        {
            if (!string.IsNullOrEmpty(connectionInfo))
            {
                XDocument doc;
                using (var reader = new StringReader(connectionInfo))
                {
                    doc = XDocument.Load(reader);
                }

                _moduleGroup.ViewLoadedCompletely(doc);
            }
        }

        protected virtual void PostCreateWindow(IXProcAddInHost addIn, IntPtr hwnd, FrameworkElement element)
        {
        }

        protected virtual IWindowViewContainer CreateWindow(string factoryId, InitialWindowParameters initialParameters)
        {
            IWindowViewContainer windowViewContainer;
            if (string.IsNullOrEmpty(factoryId) ||
                !_moduleGroup.TryGetDelayLoadWindow(factoryId, out windowViewContainer))
            {
                windowViewContainer = _chromeManager.CreateWindow(ProcessIsolationExtensions.IsolatedProcessWindowFactoryName, initialParameters);
            }

            return windowViewContainer;
        }

        TimeSpan ISponsor.Renewal(ILease lease)
        {
            return lease.RenewOnCallTime;
        }

        private delegate object XProcWindowSiteOperation(object wrapper);

        private abstract class XprocClosureBase
        {
            private readonly IXProcUiAddIn _addin;
            private readonly IsolatedSubsystemSite _subsystemSite;
            private readonly IntPtr _hwnd;

            protected XprocClosureBase(IXProcUiAddIn addin, IntPtr hwnd, IsolatedSubsystemSite subsystemSite)
            {
                _hwnd = hwnd;
                _subsystemSite = subsystemSite;
                _addin = addin;
            }

            public IXProcUiAddIn Addin
            {
                get { return _addin; }
            }

            public IsolatedSubsystemSite SubsystemSite
            {
                get { return _subsystemSite; }
            }

            public IntPtr Hwnd
            {
                get { return _hwnd; }
            }
        }

        private sealed class XProcWindowSiteClosure : XprocClosureBase
        {
            private readonly InitialWindowParametersProxy _initialWindowParametersProxy;
            private readonly string _windowTitle;

            public XProcWindowSiteClosure(IXProcUiAddIn addin, IntPtr hwnd, IsolatedSubsystemSite subsystemSite,
                                          string windowTitle,
                                          InitialWindowParametersProxy initialWindowParametersProxy)
                : base(addin, hwnd, subsystemSite)
            {
                _windowTitle = windowTitle;
                _initialWindowParametersProxy = initialWindowParametersProxy;
            }

            public InitialWindowParametersProxy InitialWindowParametersProxy
            {
                get { return _initialWindowParametersProxy; }
            }

            public string WindowTitle
            {
                get { return _windowTitle; }
            }
        }

        private sealed class XProcWidgetSiteClosure : XprocClosureBase
        {
            private readonly IChromeManager _chromeManager;
            private readonly string _parentFactoryId;
            private readonly string _widgetFactoryId;
            private readonly string _root;
            private readonly string _location;

            private XProcWidgetSiteClosure(
                IXProcUiAddIn addin,
                IsolatedSubsystemSite subsystemSite,
                IChromeManager chromeManager,
                string parentFactoryId,
                string widgetFactoryId,
                string root,
                string location,
                IntPtr hwnd) : base(addin, hwnd, subsystemSite)
            {
                _location = location;
                _root = root;
                _widgetFactoryId = widgetFactoryId;
                _parentFactoryId = parentFactoryId;
                _chromeManager = chromeManager;
            }

            public XProcWidgetSiteClosure(
                IXProcUiAddIn addin,
                IsolatedSubsystemSite subsystemSite,
                IChromeManager chromeManager,
                string widgetFactoryId,
                string root,
                string location,
                IntPtr hwnd) : this(addin, subsystemSite, chromeManager, null, widgetFactoryId, root, location, hwnd)
            {
            }

            public XProcWidgetSiteClosure(
                IXProcUiAddIn addin,
                IsolatedSubsystemSite subsystemSite,
                IChromeManager chromeManager,
                string parentFactoryId,
                string widgetFactoryId,
                IntPtr hwnd)
                : this(addin, subsystemSite, chromeManager, parentFactoryId, widgetFactoryId, null, null, hwnd)
            {
            }

            public IChromeManager ChromeManager
            {
                get { return _chromeManager; }
            }

            public string ParentFactoryId
            {
                get { return _parentFactoryId; }
            }

            public string WidgetFactoryId
            {
                get { return _widgetFactoryId; }
            }

            public string Root
            {
                get { return _root; }
            }

            public string Location
            {
                get { return _location; }
            }
        }
    }
}
