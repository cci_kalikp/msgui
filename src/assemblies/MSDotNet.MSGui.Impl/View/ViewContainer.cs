﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/View/ViewContainer.cs#25 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using MorganStanley.MSDotNet.MSGui.Core;
using Size = System.Windows.Size;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using Color = System.Windows.Media.Color;

namespace MorganStanley.MSDotNet.MSGui.Impl.View
{
//    internal class ViewContainer : IViewContainer, INotifyPropertyChanged
//    {
//        private WindowViewContainer windowViewContainer;

//        public ViewContainer()
//        {
//            this.windowViewContainer = new WindowViewContainer("");
//            this.windowViewContainer.PropertyChanged += new PropertyChangedEventHandler((s, e) => { InvokePropertyChanged(e.PropertyName); });
//        }

//        public ViewContainer(object view_)
//        {
//            this.windowViewContainer = new WindowViewContainer("");
//            this.windowViewContainer.m_content = view_;
//            this.windowViewContainer.PropertyChanged += new PropertyChangedEventHandler((s, e) => { InvokePropertyChanged(e.PropertyName); });
//        }

//        public ViewContainer(WindowViewContainer windowViewContainer)
//        {
//            this.windowViewContainer = windowViewContainer;
//            this.windowViewContainer.PropertyChanged += new PropertyChangedEventHandler((s, e) => { InvokePropertyChanged(e.PropertyName); });
//        }

//        public string Title
//        {
//            get
//            {
//                return this.windowViewContainer.Title;
//            }
//            set
//            {
//                this.windowViewContainer.Title = value;
//            }
//        }

//        private Size m_defaultSize = Size.Empty;
//        public Size DefaultSize
//        {
//            get
//            {
//                return m_defaultSize;
//            }
//            set
//            {
//                m_defaultSize = value;
//                this.InitialViewSettings.Width = value.Width;
//                this.InitialViewSettings.Height = value.Height;
//                InvokePropertyChanged("DefaultSize");
//            }
//        }

//        public double Width
//        {
//            get
//            {
//                return this.windowViewContainer.Width;
//            }
//            set
//            {
//                this.windowViewContainer.Width = value;
//            }
//        }

//        public double Height
//        {
//            get
//            {
//                return this.windowViewContainer.Height;
//            }
//            set
//            {
//                this.windowViewContainer.Height = value;
//            }
//        }

//        public IList<object> HeaderItems
//        {
//            get
//            {
//                return this.windowViewContainer.HeaderItems;
//            }
//        }

//        public bool IsVisible
//        {
//            get
//            {
//                return this.windowViewContainer.IsVisible;
//            }
//            internal set
//            {
//                this.windowViewContainer.IsVisible = value;
//            }
//        }

//        private System.Windows.Media.Brush m_background;
//        public System.Windows.Media.Brush Background
//        {
//            get
//            {
//                return this.windowViewContainer.Background;
//            }
//            set
//            {
//                this.windowViewContainer.Background = value;
//            }
//        }

//        public string ID
//        {
//            get
//            {
//                return this.windowViewContainer.ID;
//            }
//            set
//            {
//                this.windowViewContainer.ID = value;
//            }
//        }

//        public object Content
//        {
//            get
//            {
//                return this.windowViewContainer.Content;
//            }
//            set
//            {
//                this.windowViewContainer.Content = value;
//            }
//        }

//        public bool IsActive
//        {
//            get
//            {
//                return this.windowViewContainer.IsActive;
//            }
//            internal set
//            {
//                this.windowViewContainer.IsActive = value;
//            }
//        }

//        public Icon Icon
//        {
//            get
//            {
//                return this.windowViewContainer.Icon;
//            }
//            set
//            {
//                this.windowViewContainer.Icon = value;
//            }
//        }

//        private MorganStanley.MSDotNet.MSGui.Core.InitialViewSettings m_initialViewSettings = new InitialViewSettings() 
//        { 
//            InitialLocation = InitialLocation.Floating, 
//            SizingMethod = SizingMethod.SizeToContent
//        };
//        public InitialViewSettings InitialViewSettings 
//        { 
//            get 
//            {
//                return new InitialViewSettings(this.windowViewContainer.InitialParameters);
////				return this.m_initialViewSettings; 
//            } 
//        }

//        public void Close()
//        {
//            EventHandler closeRequestedCopy = CloseRequested;
//            if (closeRequestedCopy != null)
//            {
//                closeRequestedCopy(this, EventArgs.Empty);
//            }
//            else if (!InvokeClosing().Cancel)
//            {
//                InvokeClosed();
//            }
//        }

//        public void Activate()
//        {
//            EventHandler activateRequestedCopy = ActivationRequested;
//            if (activateRequestedCopy != null)
//            {
//                activateRequestedCopy(this, EventArgs.Empty);
//            }
//        }

//        public void Flash()
//        {
//            Flash(Color.FromRgb(255, 0, 0));
//        }

//        public void Flash(Color color_)
//        {
//            EventHandler<EventArgs<Color>> flashRequestedCopy = FlashRequested;
//            if (flashRequestedCopy != null)
//            {
//                flashRequestedCopy(this, new EventArgs<Color>(color_));
//            }
//        }

//        public event EventHandler<CancelEventArgs> Closing;
//        public event EventHandler<ViewEventArgs> Closed;

//        //invoked when we remove the pane during layout switch 
//        public event EventHandler<ViewEventArgs> ClosedQuietly;

//        public event PropertyChangedEventHandler PropertyChanged;
//        private void InvokePropertyChanged(string name_)
//        {
//            PropertyChangedEventHandler copy = PropertyChanged;
//            if (copy != null)
//            {
//                copy(this, new PropertyChangedEventArgs(name_));
//            }
//        }

//        internal event EventHandler CloseRequested;
//        internal event EventHandler ActivationRequested;
//        internal event EventHandler<EventArgs<Color>> FlashRequested;
//        //internal event EventHandler FlashStopRequested;

//        private ViewMetadata m_viewMeta;
//        internal ViewMetadata ViewMeta
//        {
//            get
//            {
//                return m_viewMeta;
//            }
//            set
//            {
//                m_viewMeta = value;
//                InvokePropertyChanged("ViewMeta");
//            }
//        }

//        internal CancelEventArgs InvokeClosing()
//        {
//            EventHandler<CancelEventArgs> copy = Closing;
//            CancelEventArgs cea = new CancelEventArgs();
//            if (copy != null)
//            {
//                copy(this, cea);
//            }
//            return cea;
//        }

//        internal void InvokeClosed()
//        {
//            EventHandler<ViewEventArgs> copy2 = Closed;
//            if (copy2 != null)
//            {
//                copy2(this, new ViewEventArgs(this));
//            }
//        }

//        internal void InvokeClosedQuietly()
//        {
//            EventHandler<ViewEventArgs> copy2 = ClosedQuietly;
//            if (copy2 != null)
//            {
//                copy2(this, new ViewEventArgs(this));
//            }
//        }

//        //public void RequestFlash(EventHandler action)
//        //{
//        //  if (FlashRequested == null)
//        //    FlashRequested += action;
//        //}

//        //public void RequestFlashStop(EventHandler action)
//        //{
//        //    if (FlashStopRequested == null)
//        //        FlashStopRequested += action;
//        //}

//        public void StartFlash()
//        {
//            //EventHandler flashRequestedCopy = FlashRequested;
//            //if (flashRequestedCopy != null)
//            //{
//            //    flashRequestedCopy(this, EventArgs.Empty);
//            //}
//        }

//        public void StopFlash()
//        {
//            //EventHandler flashStopRequestedCopy = FlashStopRequested;
//            //if (flashStopRequestedCopy != null)
//            //{
//            //    flashStopRequestedCopy(this, EventArgs.Empty);
//            //}
//        }

//        /*
//        private object m_view;

//        public ViewContainer()
//        {
//            this.initialParameters = new InitialWindowParameters();
//        }

//        public ViewContainer(object view_)
//        {
//            m_view = view_;
//            this.initialParameters = new InitialWindowParameters();
//        }

//        private string m_title;
//        public string Title
//        {
//            get
//            {
//                return m_title;
//            }
//            set
//            {
//                m_title = value;
//                InvokePropertyChanged("Title");
//            }
//        }

//        private Size m_defaultSize = Size.Empty;
//        public Size DefaultSize
//        {
//            get
//            {
//                return m_defaultSize;
//            }
//            set
//            {
//                m_defaultSize = value;
//                this.InitialViewSettings.Width = value.Width;
//                this.InitialViewSettings.Height = value.Height;
//                InvokePropertyChanged("DefaultSize");
//            }
//        }

//        private double m_width = 0;
//        public double Width
//        {
//            get
//            {
//                return m_width;
//            }
//            set
//            {
//                m_width = value;
//                InvokePropertyChanged("Width");
//            }
//        }

//        private double m_height = 0;
//        public double Height
//        {
//            get
//            {
//                return m_height;
//            }
//            set
//            {
//                m_height = value;
//                InvokePropertyChanged("Height");
//            }
//        }

//        private readonly IList<object> m_headerItems = new HeaderItemsCollection();
//        public IList<object> HeaderItems
//        {
//            get
//            {
//                return m_headerItems;
//            }
//        }

//        private bool m_visible = true;
//        public bool IsVisible
//        {
//            get
//            {
//                return m_visible;
//            }
//            internal set
//            {
//                m_visible = value;
//                InvokePropertyChanged("IsVisible");
//            }
//        }

//        private System.Windows.Media.Brush m_background;
//        public System.Windows.Media.Brush Background
//        {
//            get
//            {
//                return m_background;
//            }
//            set
//            {
//                m_background = value;
//                InvokePropertyChanged("Background");
//            }
//        }

//        private string m_id;
//        public string ID
//        {
//            get
//            {
//                return m_id ?? (m_id = "view" + Guid.NewGuid().ToString("N"));
//            }
//            set
//            {
//                if (value == null)
//                {
//                    throw new InvalidOperationException("View ID can't be set to null");
//                }
//                m_id = value;
//                InvokePropertyChanged("ID");
//            }
//        }

//        public object Content
//        {
//            get
//            {
//                return m_view;
//            }
//            set
//            {
//                m_view = value;
//                InvokePropertyChanged("Content");
//            }
//        }

//        private bool m_isActive;
//        public bool IsActive
//        {
//            get
//            {
//                return m_isActive;
//            }
//            internal set
//            {
//                m_isActive = value;
//                InvokePropertyChanged("IsActive");
//            }
//        }

//        private Icon m_icon;
//        public Icon Icon
//        {
//            get
//            {
//                return m_icon;
//            }
//            set
//            {
//                m_icon = value;
//                InvokePropertyChanged("Icon");
//            }
//        }

//        private MorganStanley.MSDotNet.MSGui.Core.InitialViewSettings m_initialViewSettings = new InitialViewSettings() 
//        { 
//            InitialLocation = InitialLocation.Floating, 
//            SizingMethod = SizingMethod.SizeToContent
//        };
//        public InitialViewSettings InitialViewSettings 
//        { 
//            get 
//            {
//                return new InitialViewSettings(this.initialParameters as InitialWindowParameters);
////				return this.m_initialViewSettings; 
//            } 
//        }

//        public void Close()
//        {
//            EventHandler closeRequestedCopy = CloseRequested;
//            if (closeRequestedCopy != null)
//            {
//                closeRequestedCopy(this, EventArgs.Empty);
//            }
//            else if (!InvokeClosing().Cancel)
//            {
//                InvokeClosed();
//            }
//        }

//        public void Activate()
//        {
//            EventHandler activateRequestedCopy = ActivationRequested;
//            if (activateRequestedCopy != null)
//            {
//                activateRequestedCopy(this, EventArgs.Empty);
//            }
//        }

//        public void Flash()
//        {
//            Flash(Color.FromRgb(255, 0, 0));
//        }

//        public void Flash(Color color_)
//        {
//            EventHandler<EventArgs<Color>> flashRequestedCopy = FlashRequested;
//            if (flashRequestedCopy != null)
//            {
//                flashRequestedCopy(this, new EventArgs<Color>(color_));
//            }
//        }

//        public event EventHandler<CancelEventArgs> Closing;
//        public event EventHandler<ViewEventArgs> Closed;

//        //invoked when we remove the pane during layout switch 
//        public event EventHandler<ViewEventArgs> ClosedQuietly;

//        public event PropertyChangedEventHandler PropertyChanged;
//        private void InvokePropertyChanged(string name_)
//        {
//            PropertyChangedEventHandler copy = PropertyChanged;
//            if (copy != null)
//            {
//                copy(this, new PropertyChangedEventArgs(name_));
//            }
//        }

//        internal event EventHandler CloseRequested;
//        internal event EventHandler ActivationRequested;
//        internal event EventHandler<EventArgs<Color>> FlashRequested;
//        //internal event EventHandler FlashStopRequested;

//        private ViewMetadata m_viewMeta;
//        internal ViewMetadata ViewMeta
//        {
//            get
//            {
//                return m_viewMeta;
//            }
//            set
//            {
//                m_viewMeta = value;
//                InvokePropertyChanged("ViewMeta");
//            }
//        }

//        internal CancelEventArgs InvokeClosing()
//        {
//            EventHandler<CancelEventArgs> copy = Closing;
//            CancelEventArgs cea = new CancelEventArgs();
//            if (copy != null)
//            {
//                copy(this, cea);
//            }
//            return cea;
//        }

//        internal void InvokeClosed()
//        {
//            EventHandler<ViewEventArgs> copy2 = Closed;
//            if (copy2 != null)
//            {
//                copy2(this, new ViewEventArgs(this));
//            }
//        }

//        internal void InvokeClosedQuietly()
//        {
//            EventHandler<ViewEventArgs> copy2 = ClosedQuietly;
//            if (copy2 != null)
//            {
//                copy2(this, new ViewEventArgs(this));
//            }
//        }

//        //public void RequestFlash(EventHandler action)
//        //{
//        //  if (FlashRequested == null)
//        //    FlashRequested += action;
//        //}

//        //public void RequestFlashStop(EventHandler action)
//        //{
//        //    if (FlashStopRequested == null)
//        //        FlashStopRequested += action;
//        //}

//        public void StartFlash()
//        {
//            //EventHandler flashRequestedCopy = FlashRequested;
//            //if (flashRequestedCopy != null)
//            //{
//            //    flashRequestedCopy(this, EventArgs.Empty);
//            //}
//        }

//        public void StopFlash()
//        {
//            //EventHandler flashStopRequestedCopy = FlashStopRequested;
//            //if (flashStopRequestedCopy != null)
//            //{
//            //    flashStopRequestedCopy(this, EventArgs.Empty);
//            //}
//        }*/
//    }
}