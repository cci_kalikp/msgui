﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/View/ViewManager.cs#55 $
// $Change: 896821 $
// $DateTime: 2014/09/15 02:20:20 $
// $Author: caijin $

using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
	#region Let's do [troll]science

	public partial class ChromeManagerBase : IViewManager
	{

		#region IViewManager Members

		public IViewContainer ActiveViewContainer
		{
			get
			{
				return _activeViewContainerContainer;
			}
		}

		private ObservableCollection<IViewContainer> _oldViews;

		ObservableCollection<IViewContainer> IViewManager.Views
		{
			get 
			{
				if (_oldViews == null)
				{
					_oldViews = new ObservableCollection<IViewContainer>();
					
					//this _may_ introduce thread-safety issues
					foreach (var wvc in _windows)
					{
						_oldViews.Add(wvc as IViewContainer);
					}

					_windows.CollectionChanged += (s, e) =>
					{
						switch (e.Action)
						{
							case NotifyCollectionChangedAction.Add:
								foreach (var item in e.NewItems)
								{
									_oldViews.Add(item as IViewContainer);
								}
								break;
							case NotifyCollectionChangedAction.Remove:
								foreach (var item in e.OldItems)
								{
									_oldViews.Remove(item as IViewContainer);
								}
								break;
							case NotifyCollectionChangedAction.Reset:
								_oldViews.Clear();
								break;
						}
					};
				}

				return _oldViews;
			}
		}

		void IViewManager.AddViewCreator(string id, InitialiseViewHandler initialiseViewHandler)
		{
			Registry.RegisterWindowFactory(id, (vc, state) => 
				{ 
					initialiseViewHandler((WindowViewContainer)vc); 
					return true; 
				});
		}


		void IViewManager.AddViewCreator(string id, RestoreViewHandler stateToViewHandler, SaveViewHandler viewtoState)
		{
			Registry.RegisterWindowFactory(id, (vc, state) => 
				{
					stateToViewHandler(state, (WindowViewContainer)vc); 
					return true; 
				},  
				vc => viewtoState((WindowViewContainer)vc));
		}

		IViewContainer IViewManager.CreateView(string creatorId)
		{
			return (WindowViewContainer)CreateWindow(creatorId, new InitialWindowParameters());
		}

		IViewContainer IViewManager.CreateView(string creatorId, XDocument viewState)
		{
			return (WindowViewContainer)CreateWindow(creatorId, new InitialWindowParameters(), viewState);
		}

		IViewContainer IViewManager.CreateSingletonView(string creatorId)
		{
			return (WindowViewContainer)CreateWindow(creatorId, new InitialWindowParameters { Singleton = true});
		}

		IViewContainer IViewManager.CreateSingletonView(string creatorId, XDocument viewState)
		{
			return (WindowViewContainer)CreateWindow(creatorId, new InitialWindowParameters { Singleton = true }, viewState);
		}

		IDialogWindow IViewManager.CreateDialog()
		{
			return _windowManager.CreateDialog() as IDialogWindow;
		}

		void IViewManager.SaveViewContainer(IViewContainer container)
		{
			if (String.IsNullOrEmpty(PersistenceProfileService.CurrentProfile))
			{
				//TODO!!!!!!!!!!!!!!!!!
			}
			else
			{
                //todo: fix it for harmonia
				PersistenceProfileService.SaveSubitem(
				PersistorId, container.ID, BuildIViewXml((WindowViewContainer)container), AddNewView, m_cleanLayout);
			}
		}

		public event EventHandler<ViewEventArgs> Activated;

		public event EventHandler<ViewEventArgs> Deactivated;

		public event EventHandler<ViewEventArgs> ViewAdded;

		#endregion

	    internal static bool LoadedWorkspaceStickyTitle;
	}

	#endregion

}
