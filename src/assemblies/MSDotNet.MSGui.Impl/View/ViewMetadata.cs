﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/View/ViewMetadata.cs#10 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

namespace MorganStanley.MSDotNet.MSGui.Impl.View
{
  internal class ViewMetadata
  {    
    public ViewMetadata()
    {
      Type = ViewContainerType.Concord;      
    }

    public ViewMetadata(string creatorId_)
    {
      Type = ViewContainerType.WithCreator;
      CreatorId = creatorId_;
    }

    public ViewContainerType Type
    {
      get; 
      set;
    }

    public string CreatorId 
    {
      get;
      set;
    }
  }
}
