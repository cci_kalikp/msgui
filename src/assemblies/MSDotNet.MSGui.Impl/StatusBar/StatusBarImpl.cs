﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/StatusBar/StatusBarImpl.cs#10 $
// $Change: 862682 $
// $DateTime: 2014/01/17 16:11:04 $S
// $Author: hrechkin $

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;

namespace MorganStanley.MSDotNet.MSGui.Impl.StatusBar
{
	internal class StatusBarImpl : IStatusBar
	{
		private readonly System.Windows.Controls.Primitives.StatusBar m_statusBar;
		private readonly Dictionary<string, ItemPack> m_items = new Dictionary<string, ItemPack>();
		private readonly StackPanel m_leftStackPanel;
		private readonly StackPanel m_rightStackPanel;

		private const string MAIN_STATUS_ITEM_NAME = "MainStatus";

		public StatusBarImpl(IShell shell_)
		{
			m_statusBar = shell_.StatusBar;
			m_leftStackPanel = new StackPanel()
									 {
										 Orientation = Orientation.Horizontal,
										 FlowDirection = FlowDirection.LeftToRight
									 };
			m_rightStackPanel = new StackPanel()
									 {
										 Orientation = Orientation.Horizontal,
										 FlowDirection = FlowDirection.RightToLeft
									 };

			m_statusBar.Items.Add(new StatusBarItem()
									{
										Content = m_leftStackPanel,
										HorizontalAlignment = HorizontalAlignment.Left
									});
			m_statusBar.Items.Add(new StatusBarItem()
									{
										Content = m_rightStackPanel,
										HorizontalAlignment = HorizontalAlignment.Right
									});


			AddItem(MAIN_STATUS_ITEM_NAME, new TextStatusItem(), StatusBarItemAlignment.Left);
		}

		public IStatusBarItem MainItem
		{
			get { return this[MAIN_STATUS_ITEM_NAME]; }
		}

		public void AddItem(string key, IStatusBarItem item, StatusBarItemAlignment alignment)
		{
			if (m_items.ContainsKey(key))
			{
				throw new InvalidOperationException("Status bar item key \'" + key + "\' already exists");
			}

			//are we going to support winform controls here? we can do this using ItemTemplate
			Control container;
			if (item.Control is Separator)
			{
				container = (Control)item.Control;
			}
			else
			{
				container = new StatusBarItem
							  {
								  Content = item.Control,
								  FlowDirection = FlowDirection.LeftToRight
							  };
			}

			switch (alignment)
			{
				case StatusBarItemAlignment.Left:
					m_items.Add(key, new ItemPack(item, container, m_leftStackPanel));
					m_leftStackPanel.Children.Add(container);
					break;
				case StatusBarItemAlignment.Right:
					m_items.Add(key, new ItemPack(item, container, m_rightStackPanel));
					m_rightStackPanel.Children.Add(container);
					break;
			}
		}

		public string AddItem(IStatusBarItem item, StatusBarItemAlignment alignment)
		{
			string key = Guid.NewGuid().ToString("N");
			AddItem(key, item, alignment);
			return key;
		}

		public void AddTextStatusItem(string key, StatusBarItemAlignment alignment)
		{
			AddItem(key, new TextStatusItem(), alignment);
		}

		public string AddTextStatusItem(StatusBarItemAlignment alignment)
		{
			return AddItem(new TextStatusItem(), alignment);
		}

		public void AddProgressBarStatusItem(string key, double @from, double to, StatusBarItemAlignment alignment)
		{
			AddItem(key, new ProgressBarStatusItem(@from, to), alignment);
		}

		public string AddProgressBarStatusItem(double @from, double to, StatusBarItemAlignment alignment)
		{
			return AddItem(new ProgressBarStatusItem(@from, to), alignment);
		}

		public void AddSeparator(string key, StatusBarItemAlignment alignment)
		{
			AddItem(key, new SeparatorStatusItem(), alignment);
		}

		public string AddSeparator(StatusBarItemAlignment alignment)
		{
			return AddItem(new SeparatorStatusItem(), alignment);
		}

		public void RemoveItem(string key)
		{
			if (!m_items.ContainsKey(key))
			{
				throw new InvalidOperationException("Status bar doesn't has an item with key \'" + key + "\'");
			}

			if (key == MAIN_STATUS_ITEM_NAME)
			{
				throw new InvalidOperationException("Removing main StatusBar item is not allowed");
			}

			ItemPack pack = m_items[key];
			pack.Panel.Children.Remove(pack.ControlContainer);
			m_items.Remove(key);
		}

		public bool ContainsItem(string key)
		{
			return m_items.ContainsKey(key);
		}

		public ICollection<string> Keys
		{
			get { return m_items.Keys; }
		}

		public IStatusBarItem this[string statusBarItemKey]
		{
			get
			{
				return m_items[statusBarItemKey].BarItem;
			}
		}

		private class ItemPack
		{
			public ItemPack(IStatusBarItem item_, Control container_, Panel panel_)
			{
				BarItem = item_;
				ControlContainer = container_;
				Panel = panel_;
			}
			public IStatusBarItem BarItem { get; private set; }
			public Control ControlContainer { get; private set; }
			public Panel Panel { get; private set; }
		}
	}
}