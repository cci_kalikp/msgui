﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/StatusBar/TextStatusItem.cs#10 $
// $Change: 880137 $
// $DateTime: 2014/05/08 04:11:56 $
// $Author: milosp $

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core;


namespace MorganStanley.MSDotNet.MSGui.Impl.StatusBar
{
	internal delegate void UpdateStatusUI();
	internal class TextStatusItem : IStatusBarItem
	{
		private StatusLevel m_level;
		private string m_text;
		private readonly TextBlock m_textBlock;
		public TextStatusItem()
		{
			m_textBlock = new TextBlock()
				  {
					  Background = Brushes.Transparent
				  };
		}

		public void UpdateStatus(StatusLevel level_, string statusText_)
		{
			DoDispatchedAction(() =>
			{
				this.m_level = level_;
				this.m_text = statusText_;
				switch (level_)
				{
					case StatusLevel.Information:
						m_textBlock.ClearValue(TextBlock.ForegroundProperty);
						m_textBlock.Text = statusText_;
						break;
					case StatusLevel.Critical:
						m_textBlock.Foreground = Brushes.Red;
						m_textBlock.Text = statusText_;
						break;
				}
				InvokeStatusUpdated(statusText_, level_);
			});
		}

		public void UpdateText(string statusText_)
		{
			DoDispatchedAction(() =>
			{
				m_textBlock.Text = statusText_;
				InvokeStatusUpdated(statusText_, m_level);
			});
		}

		public void UpdateLevel(StatusLevel level_)
		{
			DoDispatchedAction(() =>
			{
				switch (level_)
				{
					case StatusLevel.Information:
						m_textBlock.ClearValue(TextBlock.ForegroundProperty);
						break;
					case StatusLevel.Critical:
						m_textBlock.Foreground = Brushes.Red;
						break;
				}
				InvokeStatusUpdated(m_text, level_);
			});
		}

        public void UpdateIsVisible(bool isVisible)
        {
            DoDispatchedAction(() =>
                {
                    m_textBlock.Visibility = isVisible ? Visibility.Visible : Visibility.Collapsed;
                });
        }

		public void ClearStatus()
		{
			DoDispatchedAction(() =>
			{
				m_textBlock.Background = Brushes.Transparent;
				m_textBlock.Text = string.Empty;
				InvokeStatusUpdated(string.Empty, StatusLevel.Information);
			});
		}

		public object Control
		{
			get
			{
				return m_textBlock;
			}
		}

		public event StatusUpdatedEventHandler StatusUpdated;

		private void InvokeStatusUpdated(string text_, StatusLevel level_)
		{
			var copy = StatusUpdated;
			if (copy != null)
			{
				copy(this, new StatusUpdatedEventArgs(text_, level_));
			}
		}

		private void DoDispatchedAction(Action action_)
		{
			if (m_textBlock.Dispatcher.CheckAccess())
			{
				action_.Invoke();
			}
			else
			{
				m_textBlock.Dispatcher.Invoke(action_);
			}
		}
	}
}
