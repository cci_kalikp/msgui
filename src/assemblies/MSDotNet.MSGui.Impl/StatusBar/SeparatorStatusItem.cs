﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/StatusBar/SeparatorStatusItem.cs#7 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $S
// $Author: smulovic $

using System;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.StatusBar
{
  internal class SeparatorStatusItem : IStatusBarItem
  {
    private static ResourceDictionary m_stylesDictionary;
    static SeparatorStatusItem()
    {
      //without this style all separators in the status bar will be shown as dots
      //the reason is in the incorrect default controltemplate for separators 
      //(compare vertical and horizontal StackPanels with separators between elements to see the issue that we are solving here)
      m_stylesDictionary = new ResourceDictionary()
                                {
                                  Source =
                                    new Uri("/MSDotNet.MSGui.Impl;component/StatusBar/StatusBarStyles.xaml",
                                            UriKind.RelativeOrAbsolute)
                                };
      
    }
    private readonly object m_control;

    public SeparatorStatusItem()
    {
      m_control = new Separator() {Style = (Style)m_stylesDictionary["StatusBarSeparator"]};
    }

    public void UpdateStatus(StatusLevel level_, string statusText_)
    {
      //do nothing
    }

    public void ClearStatus()
    {
      //what status?
    }

    public object Control
    {
      get
      {        
        return m_control;
      }
    }

    public event StatusUpdatedEventHandler StatusUpdated;
  }
}
