﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/StatusBar/ProgressBarStatusItem.cs#9 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.StatusBar
{
	internal class ProgressBarStatusItem : IStatusBarItem
	{
		private readonly ProgressBar m_progBar;
		private readonly double m_from;
		private readonly double m_to;
		private StatusLevel m_level;
		public ProgressBarStatusItem(double from_, double to_)
		{
			m_progBar = new ProgressBar()
						  {
							  Minimum = from_,
							  Maximum = to_,
							  Width = 100,
							  Height = 20
						  };
			m_from = from_;
			m_to = to_;
		}

		public void UpdateStatus(StatusLevel level_, string statusText_)
		{
			m_level = level_;
			double value;
			Double.TryParse(statusText_, out value);
			DoDispatchedAction(() =>
			{
				m_progBar.Value = value;
				InvokeStatusUpdated(statusText_, level_);
			});
		}

		public void UpdatePosition(double position_)
		{
			DoDispatchedAction(() =>
			{
				m_progBar.Value = position_;
				InvokeStatusUpdated(position_.ToString(), this.m_level);
			});
		}

		public void ClearStatus()
		{
			DoDispatchedAction(() =>
			{
				m_progBar.Value = m_from;
				this.m_level = StatusLevel.Information;
				InvokeStatusUpdated(string.Empty, StatusLevel.Information);
			});
		}

		public object Control
		{
			get
			{
				return m_progBar;
			}
		}

		public event StatusUpdatedEventHandler StatusUpdated;

		private void InvokeStatusUpdated(string text_, StatusLevel level_)
		{
			var copy = StatusUpdated;
			if (copy != null)
			{
				copy(this, new StatusUpdatedEventArgs(text_, level_));
			}
		}

		private void DoDispatchedAction(Action action_)
		{
			if (m_progBar.Dispatcher.CheckAccess())
			{
				action_.Invoke();
			}
			else
			{
				m_progBar.Dispatcher.Invoke(action_);
			}
		}
	}
}
