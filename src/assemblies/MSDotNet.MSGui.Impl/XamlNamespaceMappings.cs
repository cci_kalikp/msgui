﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/XamlNamespaceMappings.cs#7 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Runtime.CompilerServices;
using System.Windows.Markup;

[assembly: XmlnsDefinitionAttribute
  ("http://schemas.morganstanley.com/msdotnet/msgui/xaml/", "MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls")]

    [assembly: InternalsVisibleTo("MSDotnet.MSGui.Testing, PublicKey=0024000004800000940000000602000000240000525341310004000001000100A53E632AB17B4AE228F55617FA35B87735382F10F640472009578FEA68385EB2E18DA0E33ACCED3D638FC4F6405EE021A63D2A6A9FDDA7BA4F8C281211E0C74DCBEC713FA74686DBE7B4606F4464C5669B810FD5951104D6B72779C9E45EDFD512E6400F3C429AFFD681E8BAA8B108908610BD62F76ECAF2DD8807BA32B3A5EC")]
