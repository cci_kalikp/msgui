﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.MailAttachment
{
    public interface IAttachLinkService
    {
        string UploadFile(string filename_);

        string UploadFile(string filename_, byte[] data_);
    }
}
