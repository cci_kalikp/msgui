﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.MailAttachment
{
    interface IMailAttachmentService
    {
        Attachment GenerateAttachment(string fileName_, string attachmentName_); 
    }
     
}
