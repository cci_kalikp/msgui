﻿using System;
using System.Collections.Generic;
using System.Threading;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.MailAttachment
{
    [Flags]
    internal enum TransferResultCode
    {
        /// <summary>
        /// No errors occurred, and no copying was done. The source and destination directory trees are completely synchronized. 
        /// </summary>
        NoFileCopied = 0,
        /// <summary>
        /// One or more files were copied successfully (that is, new files have arrived).
        /// </summary>
        FileCopied = 1,
        /// <summary>
        /// Some Extra files or directories were detected. Examine the output log. Some housekeeping may be needed.
        /// </summary>
        ExtraFileOrDirectoryDetected = 2,
        /// <summary>
        /// Some Mismatched files or directories were detected.
        /// Examine the output log. Some housekeeping may be needed.
        /// </summary>
        FileOrDirectoryMismatched = 4,
        /// <summary>
        /// Some files or directories could not be copied (copy errors occurred and the retry limit was exceeded).
        /// </summary>
        SomeFileOrDirectoryFailed = 8,
        /// <summary>
        /// Serious error. Robocopy did not copy any files. Either a usage error or an error due to insufficient access privileges on the source or destination directories.
        /// </summary> 
        SeriousError = 16,
    }

    internal class LargeFileTransferService: ILargeFileTransferService
    {
        private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger<LargeFileTransferService>();
        private readonly object syncRoot = new object();
        private readonly IDictionary<string, TransferHandler> transfers = new Dictionary<string, TransferHandler>(); 

        public bool SendFile(string source_, string dest_)
        {
            lock (syncRoot)
            {
                TransferHandler handler = null;
                if (!transfers.TryGetValue(dest_, out handler))
                {
                    handler = new TransferHandler(dest_);
                    transfers.Add(dest_, handler);
                }
                AutoResetEvent completeEvent = new AutoResetEvent(false);
                bool succeeded = false;
                handler.SendingComplete += (s_, d_, code_, info_) =>
                {
                    try
                    {
                        if ((code_ & (int)TransferResultCode.FileCopied) != 0)
                        {
                            succeeded = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Debug("Exception in LargeFileTransferService file transfer complete callback.", ex,
                                     "SendingCompleteCallback");
                    }
                    completeEvent.Set();
                };
                
                succeeded = handler.SendFile(source_);
                if (!succeeded)
                {
                    return false;
                }
                if (!completeEvent.WaitOne(new TimeSpan(0, 0, 60, 0))) //wait for at most 1 hour
                {
                    logger.Debug("Time out(1 hour) when using LargeFileTransferService to send file");
                    return false;
                }
                return succeeded;
            }
        }

        public bool BeginSendFile(string source_, string dest_, TransferHandler.SendingCompleteDelegate endCallback_)
        {
            TransferHandler handler = null;
            if (!transfers.TryGetValue(dest_, out handler))
            {
                handler = new TransferHandler(dest_);
                transfers.Add(dest_, handler);
            }
            handler.SendingComplete += (s_, d_, code_, info_) =>
                {
                    try
                    {
                        endCallback_(s_, d_, code_, info_);

                    }
                    catch (Exception ex)
                    {
                        logger.Debug("Exception in LargeFileTransferService file transfer complete callback.", ex,
                                     "SendingCompleteCallback");
                    }
                }  
                ;
            return handler.SendFile(source_);
        }

    }
}
