﻿using System;
using System.IO;
using System.Net.Mail;
using System.Runtime.CompilerServices;
using MorganStanley.DeskApps.AttachLinkAPI;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.ExceptionHandling;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.MailAttachment
{
    internal class MailAttachmentService:IMailAttachmentService
    {
        private volatile IAttachLinkService attachLinkService;
        private volatile ILargeFileTransferService largFileTransferService;
        private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger<MailAttachmentService>();
        private readonly IApplication application;
        private readonly object syncRoot = new object();
        internal static int MaxOutlookAttachFileSize = 19 * 1024 * 1024;
        internal static int MaxAttachlinkFileSize = 70 * 1204 * 1024;
        internal static string LargeFileStorageDirectory = null;

        public MailAttachmentService(IApplication application_)
        {
            application = application_;
        }
        public Attachment GenerateAttachment(string fileName_, string attachmentName_)
        { 
            FileInfo file = new FileInfo(fileName_);
            if (!file.Exists)
            {
                logger.Error(string.Format("File '{0}' not found.", fileName_));
                return null;
            }
            if (file.Length <= MaxOutlookAttachFileSize)
            { 
                return new Attachment(new MemoryStream(File.ReadAllBytes(fileName_)), attachmentName_);
            }
            if (file.Length <= MaxAttachlinkFileSize)
            { 
                string newFileName = AttachLinkService.UploadFile(attachmentName_, File.ReadAllBytes(fileName_));
                if (string.IsNullOrEmpty(newFileName)) return null; 
                return new Attachment(newFileName);
            }
            string storageDirectory = LargeFileStorageDirectoryResolved;
            if (storageDirectory == null) return null;
            if (LargeFileTransferService.SendFile(fileName_, storageDirectory))
            {
                string newFileName = Path.Combine(storageDirectory, file.Name);
                return new LargeFileAttachment(newFileName, attachmentName_);
            }
            return null;
        }

        internal IAttachLinkService AttachLinkService
        {
            get
            {
                if (attachLinkService == null)
                {
                    lock (syncRoot)
                    {
                        if (attachLinkService == null)
                        {
                            attachLinkService = new AttachLinkService();
                        }
                    }
                }
                return attachLinkService;
            }
        } 

        internal ILargeFileTransferService LargeFileTransferService
        {
            get
            {
                if (largFileTransferService == null)
                {
                    lock (syncRoot)
                    {
                        if (largFileTransferService == null)
                        {
                            largFileTransferService = new LargeFileTransferService();
                        }
                    }
                }
                return largFileTransferService;
            }
        }

        /// <summary>
        /// The path to store large files which cannot be send by email attachment
        /// or AttachLink.
        /// </summary>
        public string LargeFileStorageDirectoryResolved
        {
            get
            {

                if (LargeFileStorageDirectory == null)
                {
                    LargeFileStorageDirectory = Path.Combine(System.IO.Path.GetTempPath(), application.Name);
                }
                if (!Directory.Exists(LargeFileStorageDirectory))
                {
                    try
                    {
                        Directory.CreateDirectory(LargeFileStorageDirectory); 
                    }
                    catch (Exception ex)
                    {
                        logger.Error(string.Format("Failed to create file storage directory '{0}' for robocopy.", LargeFileStorageDirectory), ex);
                        return null;
                    }
                }
                return LargeFileStorageDirectory;
            } 
        } 
    }
}
