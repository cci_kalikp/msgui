﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.MailAttachment
{
    interface ILargeFileTransferService
    {
        bool SendFile(string source_, string dest_);
        bool BeginSendFile(string source_, string dest_, TransferHandler.SendingCompleteDelegate endCallback_);
    } 
}
