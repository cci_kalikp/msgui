﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.MailAttachment
{

  
    internal class RobocopyTransferAction
    {
        private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger<RobocopyTransferAction>();

        //for stdout parsing only
        private string lastFilenameFromOutput = string.Empty;
        private double lastpercentageFromOutput = 0;

        private string stdoutlog = string.Empty;
        private string stderrlog = string.Empty;

        private readonly string source;
        private readonly string destination;

        /// <summary>
        /// Gets the string representation of a source of the data to be transferred.
        /// </summary>
        public string Source
        {
            get { return source; }
        }

        /// <summary>
        /// Gets the string representation of the destination for the transferred data.
        /// </summary>
        public string Destination
        {
            get { return destination; }
        }

        /// <summary>
        /// Gets the executable to be called for transferring the data.
        /// </summary>
        internal string CopyExecutable
        {
            get { return @"\\san01b\DevAppsGML\dist\microsoft\PROJ\reskit\5.2\bin\robocopy.exe"; }
        }

        internal Process Proc { get; set; }

        /// <summary>
        /// Constructor.
        /// Calls initializing functions for the process but do not start it.
        /// </summary>
        /// <exception cref="FileNotFoundException"/>
        /// <param name="source_">copy from</param>
        /// <param name="destination_">copy to</param>
        public RobocopyTransferAction(string source_, string destination_)
        {
            this.source = source_;
            this.destination = destination_;

            if (!Directory.Exists(destination_))
            {
                try
                {
                    Directory.CreateDirectory(destination_);
                }
                catch (Exception exc)
                {
                    throw new FileNotFoundException(
                        String.Format("Error: destination directory {0} not found. Couldn't create it: {1}", destination_,
                                      exc.Message));
                }
            }
            if (File.Exists(source_))
            {
                //hard-coded 2Mb/s limit (256 KB)
                CopyFile(source_, destination_, 256);
            }
            else if (Directory.Exists(source_))
            {
                CopyDirectory(source_, destination_, 256);
            }
            else
            {
                throw new FileNotFoundException(String.Format("Error: source file or directory {0} not found.", source_));
            }

            //CopyFile or CopyDirectory with last overloaded argument can be used to slow down copying.
        }

        /// <summary>
        /// Call this to start the process.
        /// </summary>
        public void Start()
        {
            Proc.Start();
            Proc.BeginOutputReadLine();
            Proc.BeginErrorReadLine();
        }

        /// <summary>
        /// Invoked when the process is finished.
        /// </summary>
        public event EventHandler<TransferInfoEventArgs> Finished;

        /// <summary>
        /// Invoked when we get an information from the process 
        /// which can be interpreted as a progress report (percentage for example).
        /// </summary>
        public event EventHandler<TransferInfoEventArgs> ProgressReport;

        //CopyFile, CopyDirectory and RobocopyInvoker methods do not start actual copying!.
        //They only call InitProcess to initialize a Process object but do NOT create a system process.
 
        /// <summary>
        /// Initializes process for copying a single file to the directory.
        /// Copying speed limitation can be set.
        /// </summary>
        /// <param name="filenameFrom_">source filename</param>
        /// <param name="dirnameTo_">destination directory</param>
        /// <param name="maxSpeed_">max speed in KB/sec</param>
        private void CopyFile(string filenameFrom_, string dirnameTo_, double maxSpeed_)
        {
            FileInfo sourceFile = new FileInfo(filenameFrom_);
            string dirnameFrom = sourceFile.DirectoryName;
            string filename = sourceFile.Name;
            RoboCopyInvoker(dirnameFrom, dirnameTo_, filename, maxSpeed_);
        }
 
        /// <summary>
        /// Initializes directory copying with copying speed limitation.
        /// </summary>
        /// <param name="dirnameFrom_">copy this directory</param>
        /// <param name="dirnameTo_">to this directory</param>
        /// <param name="maxSpeed_">max speed in KB/sec</param>
        private void CopyDirectory(string dirnameFrom_, string dirnameTo_, double maxSpeed_)
        {
            RoboCopyInvoker(dirnameFrom_, dirnameTo_, null, maxSpeed_);
        }

        /// <summary>
        /// This function generates commandline arguments for Robocopy tool
        /// and calls InitProcess to initialize Proc object.
        /// </summary>
        /// <param name="dirnameFrom_">copy from this directory</param>
        /// <param name="dirnameTo_">destination directory</param>
        /// <param name="filename_">filename (without full path). null for directory copying</param>
        /// <param name="maxSpeed_">Upper limit for speed in KB/s</param>
        private void RoboCopyInvoker(string dirnameFrom_, string dirnameTo_, string filename_, double maxSpeed_)
        {
            // set delay time in ms after each 64 block of data to keep an average speed below maxSpeed (in KB/s)
            int waitTime = Convert.ToInt32(maxSpeed_ > 0 ? (64*1000)/maxSpeed_ : 0);

            Process process = new Process();

            string arguments;
            // all /N* arguments reduce verbosity of the output
            // R:0 - no retries if can't copy
            if (filename_ != null)
            {
                //we are copying a file
                arguments = String.Format("\"{0}\" \"{1}\" \"{2}\" /IPG:{3} /IS /NJH /NJS /NS /NC /NDL /R:0",
                                          dirnameFrom_, dirnameTo_, filename_, waitTime);
            }
            else
            {
                //a directory
                arguments = String.Format("\"{0}\" \"{1}\" /IPG:{2} /S /E /IS /NJH /NJS /NS /NC /NDL /R:0",
                                          dirnameFrom_, dirnameTo_, waitTime);
            }
            InitProcess(arguments);
        }


        private void InitProcess(string arguments_)
        {
            Proc = new Process();
            Proc.StartInfo = new ProcessStartInfo(CopyExecutable, arguments_);
            Proc.StartInfo.RedirectStandardOutput = true;
            Proc.StartInfo.RedirectStandardError = true;
            Proc.StartInfo.UseShellExecute = false;
            Proc.StartInfo.CreateNoWindow = true;

            Proc.OutputDataReceived += new DataReceivedEventHandler(OutCallback);
            Proc.ErrorDataReceived += new DataReceivedEventHandler(ErrCallback);
            Proc.Exited += new EventHandler(ExitCallback);

            Proc.EnableRaisingEvents = true;
        }

        /// <summary>
        /// This callback is invoked after exit of the process.
        /// Calls InvokeFinished at the end.
        /// </summary>
        /// <param name="sender_">process</param>
        /// <param name="e_">event arguments (not used)</param>
        internal void ExitCallback(object sender_, EventArgs e_)
        {
            Process process = (Process) sender_;
            string resultInfo = String.Format("Robocopy process exited with code {0}. ", process.ExitCode);

            resultInfo += RoboResult(process.ExitCode);

            TimeSpan timeSpent = process.ExitTime - process.StartTime;
            resultInfo += String.Format("Time spent: {0}. ", timeSpent.ToString());

            if (process.ExitCode >= 8)
            {
                resultInfo += " Robocopy output: " + stdoutlog + " " + stderrlog;
            }

            logger.Debug(resultInfo);

            InvokeFinished(this, process.ExitCode, resultInfo);
        }

        /// <summary>
        /// StdErr callback
        /// 
        /// fyi: in fact robocopy prints its error messages to stdout but not stderr
        /// </summary>
        /// <param name="sender_"></param>
        /// <param name="e_">line sent to stderr</param>
        internal void ErrCallback(object sender_, DataReceivedEventArgs e_)
        {
            if (e_.Data != null)
            {
                stderrlog += e_.Data;
                InvokeProgressReport(this, String.Format("Robocopy error {0}", e_.Data));
                logger.DebugWithFormat("Robocopy error {0}", e_.Data);
            }
        }

        /// <summary>
        /// StdOut callback
        /// </summary>
        /// <param name="sender_"></param>
        /// <param name="e">line sent to stdout</param>
        internal void OutCallback(object sender_, DataReceivedEventArgs e_)
        {
            if (e_.Data != null)
            {
                stdoutlog += e_.Data;

                double? percent = GetPercent(e_.Data);
                if (percent != null)
                {
                    if (percent - lastpercentageFromOutput >= 6)
                    {
                        lastpercentageFromOutput = percent.GetValueOrDefault();
                        string progressMessage = String.Format("Current file {1}, current progress:{0}",
                                                               percent, lastFilenameFromOutput);
                        InvokeProgressReport(this, progressMessage);
                    }
                }
                else
                {
                    string file = GetFilename(e_.Data);
                    if (file != "")
                    {
                        lastFilenameFromOutput = file;
                    }
                }
            }
        }

        /// <summary>
        /// A method to invoke Finished event handler.
        /// </summary>
        /// <param name="sender_">info</param>
        /// <param name="exitCode_">exit code</param>
        /// <param name="info_">additional information (exit code, time used, etc.)</param>
        protected void InvokeFinished(object sender_, int exitCode_, string info_)
        {
            var finCall = Finished;
            if (finCall != null)
            {
                finCall(sender_, new TransferInfoEventArgs(exitCode_, info_));
            }
        }

        /// <summary>
        /// A method to invoke ProgressReport event handler.
        /// </summary>
        /// <param name="sender_">sender</param> 
        /// <param name="info_">progress information</param>
        protected void InvokeProgressReport(object sender_, string info_)
        {
            var prCall = ProgressReport;
            if (prCall != null)
            {
                prCall(sender_, new TransferInfoEventArgs(0, info_));
            }
        }

        /// <summary>
        /// Generates information string from the Robocopy exit code.
        /// 
        /// As far as robocopy uses exit code 1 for success, an exit code of interrupted (e.g. killed) process 1 can be misinterpreted.
        /// </summary>
        /// <param name="exitCode_"></param>
        /// <returns></returns>
        private static string RoboResult(int exitCode_)
        {
            
            string res = "";
            if (exitCode_ == (int)TransferResultCode.NoFileCopied)
            {
                res += "No errors occurred, and no files were copied. ";
            }
            if ((exitCode_ & (int)TransferResultCode.FileCopied) != 0)
            {
                res += "One or more files were copied successfully. ";
            }
            if ((exitCode_ & (int)TransferResultCode.ExtraFileOrDirectoryDetected) != 0)
            {
                res += "Some Extra files or directories in the output directory were detected. ";
            }
            if ((exitCode_ & (int)TransferResultCode.FileOrDirectoryMismatched) != 0)
            {
                res += "Some Mismatched files or directories were detected. ";
            }
            if ((exitCode_ & (int)TransferResultCode.SomeFileOrDirectoryFailed) != 0)
            {
                res += "Some files or directories could not be copied. ";
            }
            if ((exitCode_ & (int)TransferResultCode.SeriousError) != 0)
            {
                res += "Serious error. Robocopy did not copy any files. ";
            }
            return res;
        }

        /// <summary>
        /// Parses line to get percentage information
        /// </summary>
        /// <param name="line_"></param>
        /// <returns>part of the string with percentage information</returns>
        private static double? GetPercent(string line_)
        {
            Regex intPattern = new Regex("[0-9.]+%");
            Match match = intPattern.Match(line_);
            if (match.Success)
            {
                return double.Parse(match.Value.TrimEnd('%'));
            }
            return null;
        }

        /// <summary>
        /// Parses line to get current filename information.
        /// Mostly useful for directory copying.
        /// </summary>
        /// <param name="line_">output line</param>
        /// <returns>filename</returns>
        private static string GetFilename(string line_)
        {
            Regex intPattern = new Regex(@"[^\s].*[^\s]");
            Match match = intPattern.Match(line_);
            if (match.Success)
            {
                return match.Value;
            }
            return "";
        }
    }
}
