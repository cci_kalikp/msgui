﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Impl.MailAttachment
{
    /// <summary>
    /// A class for passing argument to events which are used in data transferring.
    /// </summary>
    internal class TransferInfoEventArgs : EventArgs
    {
        /// <summary>
        /// String data.
        /// </summary>
        public string Data { get; private set; }

        public int ExitCode { get; private set; }
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data_"></param>
        internal TransferInfoEventArgs(int exitCode_, string data_)
        {
            Data = data_;
            ExitCode = exitCode_; 
        }
    }

}
