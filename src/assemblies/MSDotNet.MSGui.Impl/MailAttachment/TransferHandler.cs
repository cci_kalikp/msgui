﻿using System;
using System.Collections.Generic;
using System.IO;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.MailAttachment
{
    public class TransferHandler
    {
        private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger<TransferHandler>();

        /// <summary>
        /// Transfer queue. Only one piece of data can be sent at a time.
        /// We use these limitations to prevent overloading of the network and cpu on 
        /// the client-side with the diagnostics sending (which is not a high priority task).
        /// </summary>
        private readonly Queue<RobocopyTransferAction> sendQueue = new Queue<RobocopyTransferAction>();

        /// <summary>
        /// Gets or sets the default copy destination. Can be changed in a client application.
        /// </summary>
        public string BaseDestination { get; set; }

        /// <summary>
        /// Delegate for SendingComplete event.
        /// </summary>
        /// <param name="source_">from</param>
        /// <param name="destination_">to</param>
        /// <param name="exitCode_">exit code</param>
        /// <param name="info_">additional information</param>
        public delegate void SendingCompleteDelegate(string source_, string destination_, int exitCode_, string info_);

        /// <summary>
        /// Invoked when diagnostics transfer was finished (successfully or not).
        /// </summary>
        public event SendingCompleteDelegate SendingComplete;

        /// <summary>
        /// Delegate for SendingRequested event.
        /// </summary>
        /// <param name="source_">from</param>
        /// <param name="destination_">to</param>
        public delegate void SendingRequestedDelegate(string source_, string destination_);

        /// <summary>
        /// Invoked when the transfer of some data was requested.
        /// </summary>
        public event SendingRequestedDelegate SendingRequested;


        /// <summary>
        /// Delegate for ProgressReportGenerated event.
        /// </summary>
        /// <param name="source_">from</param>
        /// <param name="destination_">to</param>
        /// <param name="info_">additional information</param>
        public delegate void SendingProgressReportDelegate(string source_, string destination_, string info_);

        /// <summary>
        /// Invoked when we get any information about the current progress of transferring some data.
        /// </summary>
        public event SendingProgressReportDelegate ProgressReportGenerated;


        /// <summary>
        /// Constructor.  
        /// </summary>
        /// <param name="baseDestination_"></param> 
        public TransferHandler(string baseDestination_)
        {
            BaseDestination = baseDestination_; 
        }

        /// <summary>
        /// Send a file using TransferHandler
        /// </summary>
        /// <param name="fileName_">The file to be sent</param>
        /// <returns>The information message return about the operation</returns>
        public bool SendFile(string fileName_)
        {
            //it is important to call this function before we call constructor because BaseDestination can be changed
            lock (BaseDestination)
            {
                InvokeSendingRequested(fileName_, BaseDestination);
            }
             

            lock (BaseDestination)
            {
                try
                {
                    var copier = new RobocopyTransferAction(fileName_, BaseDestination);
                    TryStartSending(copier);
                    return true;
                }
                catch (FileNotFoundException ex)
                {
                    logger.DebugWithFormat("Could not send data from {0} to {1}. Exception: {2}",
                                              fileName_, BaseDestination, ex.Message);
                    return false;
                }
            } 

        }

 

        /// <summary>
        /// Adds copier object to the queue and calls a function for starting head of the queue 
        /// if it is the queue was empty before adding.
        /// </summary>
        /// <param name="copier_"></param>
        /// <returns>a message to be sent to persons who requested data</returns>
        private void TryStartSending(RobocopyTransferAction copier_)
        {
            lock (sendQueue)
            {
                sendQueue.Enqueue(copier_);
                if (sendQueue.Count == 1)
                {
                    StartNext();
                    logger.DebugWithFormat("Started sending data from {0} to {1}.", copier_.Source, copier_.Destination); 
                }
                logger.DebugWithFormat("Requested data was added to the queue");
             }
        }

        /// <summary>
        /// Starts the process from the head of the queue.
        /// </summary>
        private void StartNext()
        {
            while (sendQueue.Count > 0)
            {
                var next = sendQueue.Peek();
                try
                {
                    next.Finished += OnFinish;
                    next.ProgressReport += OnReport;
                    next.Start();
                    return;
                }
                catch (Exception ex)
                {
                    logger.DebugWithFormat("Could not start sending data from {0} to {1}. Exception: {2}",
                                              next.Source, next.Destination, ex.Message);
                    sendQueue.Dequeue();
                }
            }
        }

        /// <summary>
        /// Callback for finish of event transfer operation.
        /// </summary>
        /// <param name="sender_"></param>
        /// <param name="info_"></param>
        private void OnFinish(object sender_, TransferInfoEventArgs info_)
        {
            lock (sendQueue)
            {
                if (sender_ == sendQueue.Peek())
                {
                    RobocopyTransferAction copier = (RobocopyTransferAction)sender_;
                    logger.DebugWithFormat("Sending data from {0} to {1} was completed. Result: {2}",
                                    copier.Source, copier.Destination, info_.Data);
                    sendQueue.Dequeue();
                    InvokeSendingComplete(copier.Source, copier.Destination, info_.ExitCode, info_.Data);
                    StartNext();
                }
                else
                {
                    //usually this means that an exception was risen in Start method of ICopying instance 
                    //after successfully creation of the process/thread
                    logger.ErrorWithFormat("Unexpected OnFinish event was risen.");
                }
            }
        }

        /// <summary>
        /// Callback for ProgressReport event
        /// </summary>
        /// <param name="sender_"></param>
        /// <param name="info_">progress info</param>
        private void OnReport(object sender_, TransferInfoEventArgs info_)
        {
            var tr = sender_ as RobocopyTransferAction;
            logger.InfoWithFormat(info_.Data);
            if (tr != null)
            {
                InvokeProgressReport(tr.Source, tr.Destination, info_.Data);
            }
        }

        /// <summary>
        /// SendingComplete invoker.
        /// </summary>
        /// <param name="source_">send from</param>
        /// <param name="destination_">send to</param>
        /// <param name="succeeded_">success or not</param>
        /// <param name="info_">result information</param>
        private void InvokeSendingComplete(string source_, string destination_, int exitCode_, string info_)
        {
            SendingCompleteDelegate sc = SendingComplete;
            if (sc != null)
            {
                sc(source_, destination_, exitCode_, info_);
            }
        }

        /// <summary>
        /// SendingRequested invoker.
        /// </summary>
        /// <param name="source_">send from</param>
        /// <param name="destination_">send to</param>
        private void InvokeSendingRequested(string source_, string destination_)
        {
            SendingRequestedDelegate sr = SendingRequested;
            if (sr != null)
            {
                sr(source_, destination_);
            }
        }

        /// <summary>
        /// ProgressReportGenerated invoker.
        /// </summary>
        /// <param name="source_">send from</param>
        /// <param name="destination_">send to</param>
        /// <param name="info_">progress information</param>
        private void InvokeProgressReport(string source_, string destination_, string info_)
        {
            SendingProgressReportDelegate pr = ProgressReportGenerated;
            if (pr != null)
            {
                pr(source_, destination_, info_);
            }
        }
    }
}
