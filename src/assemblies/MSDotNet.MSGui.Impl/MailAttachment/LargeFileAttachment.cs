﻿using System.IO;
using System.Net.Mail;
using System.Net.Mime;

namespace MorganStanley.MSDotNet.MSGui.Impl.MailAttachment
{
    class LargeFileAttachment:Attachment
    {
       public string FileLink;
       public LargeFileAttachment(string fileLink_, string fileName_):base(new MemoryStream(), fileName_)
       {
           FileLink = fileLink_;
       }
    }
}
