﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using MorganStanley.DeskApps.AttachLinkAPI;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.MailAttachment
{
    internal class AttachLinkService : IAttachLinkService
    {
        private readonly AttachLink attachLink = new AttachLink() { ClientId = "SL36" };
        private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger<AttachLinkService>();

        [MethodImpl(MethodImplOptions.NoInlining)]
        public string UploadFile(string filename_)
        {
            try
            {
                AttachLinkFile alFile = attachLink.Upload(filename_);
                return alFile.AlnkFile;
            }
            catch (Exception ex)
            {
                logger.Error("Failed to upload file to AttachLink: \r\n" + ex);
                return string.Empty;
            }
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public string UploadFile(string filename_, byte[] data_)
        {
            try
            {
                AttachLinkFile alFile = attachLink.UploadData(filename_, data_);
                return alFile.AlnkFile;
            }
            catch (Exception ex)
            {
                logger.Error("Failed to upload file to AttachLink: \r\n" + ex);
                return string.Empty;
            }
        }
    }
}
