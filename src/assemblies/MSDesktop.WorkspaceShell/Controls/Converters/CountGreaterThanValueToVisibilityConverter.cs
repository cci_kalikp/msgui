﻿#region File Info Header

/*_____________________________________________________________________________
  Copyright (c) Morgan Stanley, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.


  $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.WorkspaceShell/Controls/Converters/CountGreaterThanValueToVisibilityConverter.cs#1 $
  $DateTime: 2014/01/16 10:53:41 $
  $Change: 862424 $
  $Author: milosp $
_____________________________________________________________________________*/

#endregion File Info Header

using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace MSDesktop.WorkspaceShell.Controls.Converters
{
  public class CountGreaterThanValueToVisibilityConverter : IValueConverter
  {
    public CountGreaterThanValueToVisibilityConverter()
    {
      Value = 0;
    }

    public int Value { get; set; }

    #region Implementation of IValueConverter

    /// <summary>
    /// For a value that is IEnumerable  will return Visible if there are any in the IEnumerable. 
    /// Otherwise returns Collapsed (including for values that aren't IEnumerable).
    /// </summary>
    /// <returns>
    /// A converted value. If the method returns null, the valid null value is used.
    /// </returns>
    public object Convert(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      IEnumerable valuesToCount = value_ as IEnumerable;
      if (valuesToCount != null)
      {
        return valuesToCount.OfType<object>().Skip(Value).Any() ? Visibility.Visible : Visibility.Collapsed;
      }
      return Visibility.Collapsed;
    }

    /// <summary>
    /// Not supported, returns Binding.DoNothing in all cases
    /// </summary>
    public object ConvertBack(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      return Binding.DoNothing;
    }

    #endregion
  }
}
