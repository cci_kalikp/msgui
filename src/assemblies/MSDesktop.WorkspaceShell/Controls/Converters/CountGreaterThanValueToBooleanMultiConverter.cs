﻿#region File Info Header

/*_____________________________________________________________________________
  Copyright (c) Morgan Stanley, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.


  $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.WorkspaceShell/Controls/Converters/CountGreaterThanValueToBooleanMultiConverter.cs#1 $
  $DateTime: 2014/01/16 10:53:41 $
  $Change: 862424 $
  $Author: milosp $
_____________________________________________________________________________*/

#endregion File Info Header

using System;
using System.Collections;
using System.Globalization;
using System.Windows.Data;

namespace MSDesktop.WorkspaceShell.Controls.Converters
{
  /// <summary>
  /// Only looks at the first value of the MultiBinding
  /// </summary>
  public class CountGreaterThanValueToBooleanMultiConverter : IMultiValueConverter
  {
    public CountGreaterThanValueToBooleanMultiConverter()
    {
      Value = 0;
    }

    public int Value { get; set; }

    public object Convert(object[] values_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      ICollection valuesToCount = values_ == null ? null : values_[0] as ICollection;
      if (valuesToCount != null)
      {
        return valuesToCount.Count > Value;
      }
      else
      {
        return false;
      }
    }

    public object[] ConvertBack(object value_, Type[] targetTypes_, object parameter_, CultureInfo culture_)
    {
      return new object[0];
    }
  }
}
