﻿#region File Info Header

/*_____________________________________________________________________________
  Copyright (c) Morgan Stanley, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.


  $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.WorkspaceShell/Controls/Converters/IEnumerableToCountConverter.cs#1 $
  $DateTime: 2014/01/16 10:53:41 $
  $Change: 862424 $
  $Author: milosp $
_____________________________________________________________________________*/

#endregion File Info Header

using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace MSDesktop.WorkspaceShell.Controls.Converters
{
  public class IEnumerableToCountConverter : IValueConverter
  {
    #region Implementation of IValueConverter

    /// <summary>
    /// For a value that is IEnumerable  will return the count of non-null elements in it if there are any. 
    /// Otherwise returns zero (including for values that aren't IEnumerable).
    /// </summary>
    public object Convert(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      IEnumerable valuesToCount = value_ as IEnumerable;
      if (valuesToCount != null)
      {
        return valuesToCount.OfType<object>().Count();
      }
      return 0;
    }

    /// <summary>
    /// Not supported, returns Binding.DoNothing in all cases
    /// </summary>
    public object ConvertBack(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      return Binding.DoNothing;
    }

    #endregion
  }
}
