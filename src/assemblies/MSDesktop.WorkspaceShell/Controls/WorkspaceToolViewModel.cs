﻿using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MSDesktop.WorkspaceShell.Controls
{
    class WorkspaceToolViewModel : INotifyPropertyChanged
    {
        private ViewSelectorViewModel _selectorViewModel;

        public ViewSelectorViewModel SelectorViewModel
        {
            get
            {
                return _selectorViewModel;
            }
            set
            {
                _selectorViewModel = value;
                _selectorViewModel.PropertyChanged += (sender_, args_) =>
                    {
                        if (args_.PropertyName == "SelectedWorkspace")
                        {
                            OnPropertyChanged("InstancesInWorkspace");
                        }
                    };
            }
        }

        public string WindowFactoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string GroupName { get; set; }

        public IEnumerable InstancesInWorkspace
        {
            get
            {
                if (SelectorViewModel.SelectedWorkspace == null) return Enumerable.Empty<object>();
                return ChromeManager.Windows.Where(w_ => w_.FactoryID == WindowFactoryId &&
                                                         ChromeManager.Tabwell[SelectorViewModel.SelectedWorkspace.Title
                                                             ].Windows.Contains(w_));
            }
        }

        public ICommand RemoveFromWorkspace { get { return new RelayCommand(RemoveFromWorkspaceExecute);} }

        private void RemoveFromWorkspaceExecute(object o_)
        {
            ChromeManager.Windows.First(w_ => w_.FactoryID == WindowFactoryId && SelectorViewModel.SelectedWorkspace.Windows.Contains(w_)).Close();
            OnPropertyChanged("InstancesInWorkspace");
        }

        public ICommand AddToWorkspace { get { return new RelayCommand(AddToWorkspaceExecute);} }

        private void AddToWorkspaceExecute(object o_)
        {
            var target =_selectorViewModel.SelectedWorkspace;
            ChromeManager.CreateWindow(WindowFactoryId,
                                       new InitialWindowParameters
                                           {
                                               InitialLocation = InitialLocation.DockInNewTab,
                                               InitialLocationTargetTab = target,
                                               SizingMethod = SizingMethod.Custom,
                                               Height = 200
                                           });
            OnPropertyChanged("InstancesInWorkspace");
        }

        public IChromeManager ChromeManager { get; set; }

        public ImageSource Image { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName_)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName_));
        }
    }
}
