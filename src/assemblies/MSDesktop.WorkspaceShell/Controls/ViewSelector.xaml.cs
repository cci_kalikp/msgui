﻿using System.Windows.Controls;

namespace MSDesktop.WorkspaceShell.Controls
{
    /// <summary>
    /// Interaction logic for ViewSelector.xaml
    /// </summary>
    public partial class ViewSelector : UserControl
    {
        public ViewSelector()
        {
            InitializeComponent();
        }
    }
}
