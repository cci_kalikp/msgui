﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MSDesktop.WorkspaceShell.Controls
{
    sealed class ViewSelectorViewModel : INotifyPropertyChanged
    {
        private readonly TabbedDockViewModel _tabbedDockViewModel;
        private int _instanceCount;
        private readonly ICollectionView _workspaces;
        private readonly List<WorkspaceToolViewModel> _toolViewModels;
        private ShellTabViewModel _selectedWorkspace;
        private IChromeRegistry _chromeRegistry;
        private readonly ICollectionView _toolsCollectionView;

        internal const string WorkspaceShellEmptyWindowFactory = "WorkspaceShellEmptyWindowFactory";

        public ViewSelectorViewModel(TabbedDockViewModel tabbedDockViewModel_)
        {
            _tabbedDockViewModel = tabbedDockViewModel_;
            _instanceCount = 1;
            _workspaces = new CollectionViewSource {Source = _tabbedDockViewModel.Tabs.SourceCollection}.View;
            _workspaces.Filter =
                o_ => ((ShellTabViewModel) o_).Title != TabbedDockViewModel.SpecialTabName; 
            _toolViewModels = new List<WorkspaceToolViewModel>();
            _toolsCollectionView = CollectionViewSource.GetDefaultView(_toolViewModels);
            _toolsCollectionView.GroupDescriptions.Add(new PropertyGroupDescription("GroupName"));

            AddWorkspaceCommand = new RelayCommand(AddWorkspaceCommandExecute);

            // this will indirectly update InstancesInWorkspace in all tools
            _tabbedDockViewModel.Workspaces.CollectionChanged += (sender_, args_) => OnPropertyChanged("SelectedWorkspace");
            _workspaces.CollectionChanged += (sender_, args_) =>
                {
                    if (!_workspaces.Contains(SelectedWorkspace))
                    {
                        SelectedWorkspace = null;
                    }
                };
        }

        private void AddWorkspaceCommandExecute(object o_)
        {
            var tab = _tabbedDockViewModel.AddTab("Workspace " + _instanceCount++, false);
            SelectedWorkspace = tab;
        }

        public ICollectionView Workspaces
        {
            get
            {
                return _workspaces;
            }
        }

        public ICollectionView ToolsCollectionView
        {
            get
            {
                return SelectedWorkspace != null ? _toolsCollectionView : null;
            }
        }

        public ShellTabViewModel SelectedWorkspace
        {
            get
            {
                return _selectedWorkspace;
            }
            set
            {
                _selectedWorkspace = value;
                OnPropertyChanged("SelectedWorkspace");
                OnPropertyChanged("ToolsCollectionView");
            }
        }

        public ICommand AddWorkspaceCommand { get; private set; }

        public IChromeManager ChromeManager { get; set; }

        public IChromeRegistry ChromeRegistry
        {
            get
            {
                return _chromeRegistry;
            }
            set
            {
                _chromeRegistry = value;
                value.RegisterWindowFactory(WorkspaceShellEmptyWindowFactory, (container_, state_) =>
                    { container_.Content = new TextBox();
                        return true;
                    });
            }
        }

        public void AddTool(WorkspaceToolViewModel tool_)
        {
            _toolViewModels.Add(tool_);
            tool_.SelectorViewModel = this;
            tool_.ChromeManager = ChromeManager;
            _toolsCollectionView.Refresh();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName_)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName_));
        }
    }
}
