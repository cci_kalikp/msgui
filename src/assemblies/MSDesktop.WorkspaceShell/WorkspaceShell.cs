﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using System.Xml.Linq;
using Infragistics.Windows.Ribbon;
using MSDesktop.WorkspaceShell.Controls;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MSDesktop.WorkspaceShell
{
    internal class WorkspaceShell : IShell
    {
        private readonly IUnityContainer _container;
        private readonly ShellWindow _shellWindow;
        private readonly MorganStanley.MSDotNet.MSGui.Impl.Application.Application _application;
        private readonly ViewSelectorViewModel _viewModel;

        public WorkspaceShell(IUnityContainer container_, InitialSettings initialSettings_)
        {
            _container = container_;
            _application = container_.Resolve<IApplication>() as MorganStanley.MSDotNet.MSGui.Impl.Application.Application;
            if (_application == null)
            {
                throw new InvalidOperationException("Application must be IApplication");
            }
            _shellWindow = new ShellWindow(container_, initialSettings_);
            container_.RegisterInstance(WindowManager);

            var persistenceService = container_.Resolve<IPersistenceProfileServiceProxy>();
            persistenceService.Shell = this;

            WorkspaceViewSelector = new ViewSelector();
            _viewModel = new ViewSelectorViewModel((TabbedDockViewModel) _shellWindow.WindowManager);
            WorkspaceViewSelector.DataContext = _viewModel;

            _shellWindow.Ribbon.Visibility = Visibility.Collapsed;
            _shellWindow.SpecialTabView = WorkspaceViewSelector;
            _shellWindow.Loaded += ShellWindowOnLoaded;
        }

        private void ShellWindowOnLoaded(object sender_, RoutedEventArgs routedEventArgs_)
        {

        }

        public ViewSelector WorkspaceViewSelector { get; private set; }

        public void AddTheme(string name_, ImageSource icon_, MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes.Theme theme_)
        {
            _shellWindow.AddTheme(name_, icon_, theme_);
        }

        public void SetTheme(string name_)
        {
            _shellWindow.SetTheme(name_);
        }

        public void DisableThemeChanger()
        {
            _shellWindow.DisableThemeChanger();
        }

        public IWindowManager WindowManager
        {
            get
            {
                return _shellWindow.WindowManager;
            }
        }

        public ITileItemManager TileItemManager
        {
            get
            {
                return _shellWindow.TileItemManager;
            }
        }

        public Window MainWindow
        {
            get
            {
                return _shellWindow.MainWindow;
            }
        }

        public System.Windows.Controls.Primitives.StatusBar StatusBar
        {
            get
            {
                return _shellWindow.StatusBar;
            }
        }

        UIElement IShell.TitleBar
        {
            get
            {
                return _shellWindow.TitleBar;
            }
        }

        public Func<bool> TryCloseConcordMainWindow
        {
            set
            {
                _shellWindow.TryCloseConcordMainWindow = value;
            }
        }

        public void Show()
        {
            //if (!ShellModeExtension.InvisibleMainControllerMode)
            _shellWindow.Show();
        }

        public void Close()
        {
            ((IShell) _shellWindow).Close();
        }

        public void LoadProfile(string profile_, bool calledWhenLoading_ = false)
        {
            _shellWindow.LoadProfile(profile_, calledWhenLoading_);
        }

        public void SaveCurrentProfile()
        {
            _shellWindow.SaveCurrentProfile();
        }

        public void DeleteCurrentProfile()
        {
            _shellWindow.DeleteCurrentProfile();
        }

        public void SaveProfileAs()
        {
            _shellWindow.SaveProfileAs();
        }

        public void SetLayoutLoadStrategy(LayoutLoadStrategy layoutLoadStrategy_,
                                          LayoutLoadStrategyCallback layoutLoadStrategyCallback_)
        {
            _shellWindow.SetLayoutLoadStrategy(layoutLoadStrategy_, layoutLoadStrategyCallback_);
        }

        public void SetLayoutLoadStrategy(LayoutLoadStrategy layoutLoadStrategy_)
        {
            _shellWindow.SetLayoutLoadStrategy(layoutLoadStrategy_);
        }

        public void Initialize()
        {
            _shellWindow.Initialize();
            _viewModel.ChromeManager = _container.Resolve<IChromeManager>();
            _viewModel.ChromeRegistry = _container.Resolve<IChromeRegistry>();
        }

        public Func<bool> OnClosing
        {
            set
            {
                ((IShell) _shellWindow).OnClosing = value;
            }
        }

        public Action OnClosed
        {
            set
            {
                ((IShell) _shellWindow).OnClosed = value;
            }
        }

        public XamRibbon Ribbon
        {
            get
            {
                return _shellWindow.Ribbon;
            }
        }

        public ItemsControl QAT
        {
            get
            {
                return Ribbon.QuickAccessToolbar;
            }
        }

        public ItemsControl MainMenu
        {
            get
            {
                return _shellWindow.appmenu;
            }
        }

        public void LoadState(XDocument state_)
        {
            _shellWindow.LoadState(state_);
        }

        public XDocument SaveState()
        {
            return _shellWindow.SaveState();
        }

        public string PersistorId
        {
            get
            {
                return _shellWindow.PersistorId;
            }
        }

        public ShellWindow ShellWindow
        {
            get
            {
                return _shellWindow;
            }
        }

        public Form ConcordMSNetLoopForm
        {
            get
            {
                return _shellWindow.ConcordMSNetLoopForm;
            }
            set
            {
                _shellWindow.ConcordMSNetLoopForm = value;
            }
        }


        public bool DisableClose
        {
            get { return _shellWindow.DisableClose; }
            set { _shellWindow.DisableClose = value; }
        }


        public void SetTheme(MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes.Theme theme_)
        {
            
        }


        public void PostChromeCreationInitialize()
        {
        }
    }
}
