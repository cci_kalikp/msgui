﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MSDesktop.WorkspaceShell.Theme.Converters
{
    public class ThicknessSplitConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is Thickness))
            {
                throw new ArgumentException("Value to convert must be a Thickness");
            }

            var thickness = (Thickness)value;
            var strParameter = parameter as string;
            if (string.IsNullOrWhiteSpace(strParameter))
            {
                return thickness;
            }

            var result = new Thickness();
            var takeFields = strParameter.ToLower().Split(new[] { ',' }, 4);

            foreach (var toTake in takeFields)
            {
                switch (toTake)
                {
                    case "l":
                    case "left":
                        result.Left = thickness.Left;
                        break;
                    case "t":
                    case "top":
                        result.Top = thickness.Top;
                        break;
                    case "r":
                    case "right":
                        result.Right = thickness.Right;
                        break;
                    case "b":
                    case "bottom":
                        result.Bottom = thickness.Bottom;
                        break;
                }
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
