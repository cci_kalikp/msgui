﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MSDesktop.WorkspaceShell.Theme.Converters
{
    public class CornerRadiusSplitConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is CornerRadius))
            {
                throw new ArgumentException("Value to convert must be a CornerRadius");
            }

            var cornerRadius = (CornerRadius)value;
            var strParameter = parameter as string;
            if (string.IsNullOrWhiteSpace(strParameter))
            {
                return cornerRadius;
            }

            var result = new CornerRadius();
            var takeFields = strParameter.ToLower().Split(new[] { ',' }, 4);

            foreach (var toTake in takeFields)
            {
                switch (toTake)
                {
                    case "tl":
                    case "topleft":
                        result.TopLeft = cornerRadius.TopLeft;
                        break;
                    case "bl":
                    case "bottomleft":
                        result.BottomLeft = cornerRadius.BottomLeft;
                        break;
                    case "tr":
                    case "topright":
                        result.TopRight = cornerRadius.TopRight;
                        break;
                    case "br":
                    case "bottomright":
                        result.BottomRight = cornerRadius.BottomRight;
                        break;
                }
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
