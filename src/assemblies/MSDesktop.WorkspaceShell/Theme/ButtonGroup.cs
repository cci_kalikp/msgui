﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace MSDesktop.WorkspaceShell.Theme
{
    public class ButtonGroup : Control
    {
        static ButtonGroup()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ButtonGroup), 
                new FrameworkPropertyMetadata(typeof(ButtonGroup)));
        }

        #region Buttons dependency property 

        public static readonly DependencyProperty ButtonsProperty =
            DependencyProperty.Register("Buttons", typeof (ButtonCollection), typeof (ButtonGroup),
                new FrameworkPropertyMetadata(new ButtonCollection(),
                    FrameworkPropertyMetadataOptions.AffectsRender,
                    OnButtonsPropertyChanged));

        public ButtonCollection Buttons
        {
            get { return (ButtonCollection) GetValue(ButtonsProperty); }
            set { SetValue(ButtonsProperty, value); }
        }

        #endregion

        #region CornerRadius dependency property

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(ButtonGroup),
                new FrameworkPropertyMetadata(
                    new CornerRadius(0), 
                    FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        #endregion

        #region IsMember attached property

        public static readonly DependencyProperty IsMemberProperty =
            DependencyProperty.RegisterAttached(
            "IsMember",
            typeof(bool),
            typeof(ButtonGroup),
            new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender));

        public static bool GetIsMember(UIElement element)
        {
            return (bool)element.GetValue(IsMemberProperty);
        }

        public static void SetIsMember(UIElement element, bool value)
        {
            element.SetValue(IsMemberProperty, value);
        }

        #endregion

        #region IsFirstButton attached property

        public static readonly DependencyProperty IsFirstProperty =
            DependencyProperty.RegisterAttached(
                "IsFirst",
                typeof(bool),
                typeof(ButtonGroup),
                new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender));

        public static bool GetIsFirst(UIElement element)
        {
            return (bool)element.GetValue(IsFirstProperty);
        }

        public static void SetIsFirst(UIElement element, bool value)
        {
            element.SetValue(IsFirstProperty, value);
        }

        #endregion

        #region IsLastButton attached property

        public static readonly DependencyProperty IsLastProperty =
            DependencyProperty.RegisterAttached(
                "IsLast",
                typeof(bool),
                typeof(ButtonGroup),
                new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender));

        public static bool GetIsLast(UIElement element)
        {
            return (bool)element.GetValue(IsLastProperty);
        }

        public static void SetIsLast(UIElement element, bool value)
        {
            element.SetValue(IsLastProperty, value);
        }

        #endregion

        private static void OnButtonsPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var buttonGroup = d as ButtonGroup;
            if (buttonGroup == null) return;

            var oldButtons = e.OldValue as ButtonCollection;
            if (oldButtons != null)
            {
                foreach (var oldButton in oldButtons)
                {
                    ClearAttachedProperties(oldButton);
                }

                oldButtons.CollectionChanged -= OnButtonCollectionChanged;
            }

            var newButtons = e.NewValue as ButtonCollection;
            if (newButtons != null)
            {
                InitialiseCollectionItems(newButtons);
                newButtons.CollectionChanged += OnButtonCollectionChanged;
            }
        }

        private static void OnButtonCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var collection = sender as ButtonCollection;
            if (collection == null) return;

            if ((e.Action == NotifyCollectionChangedAction.Remove ||
                e.Action == NotifyCollectionChangedAction.Replace ||
                e.Action == NotifyCollectionChangedAction.Reset) &&
                e.OldItems != null)
            {
                foreach (var oldItem in e.OldItems.Cast<ButtonBase>())
                {
                    ClearAttachedProperties(oldItem);
                }
            }

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    var previousLastTtem = collection[collection.Count - e.NewItems.Count - 1];
                    previousLastTtem.SetValue(IsLastProperty, false);

                    foreach (var newItem in e.NewItems.Cast<ButtonBase>())
                    {
                        newItem.SetValue(IsMemberProperty, true);
                    }
                    var newLastItem = collection[collection.Count - 1];
                    newLastItem.SetValue(IsLastProperty, true);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    ResetFirstAndLast(collection);
                    break;
                case NotifyCollectionChangedAction.Replace:
                    foreach (var newItem in e.NewItems.Cast<ButtonBase>())
                    {
                        newItem.SetValue(IsMemberProperty, true);
                    }
                    ResetFirstAndLast(collection);
                    break;
                case NotifyCollectionChangedAction.Move:
                    if (e.OldItems != null)
                    {
                        foreach (var oldItem in e.OldItems.Cast<ButtonBase>())
                        {
                            oldItem.SetValue(IsFirstProperty, false);
                            oldItem.SetValue(IsLastProperty, false);
                        }
                    }
                    ResetFirstAndLast(collection);
                    break;
                case NotifyCollectionChangedAction.Reset:
                    InitialiseCollectionItems(collection);
                    break;
            }
        }

        public static void InitialiseCollectionItems(ButtonCollection collection)
        {
            var count = 0;
            foreach (var newButton in collection)
            {
                newButton.SetValue(IsMemberProperty, true);
                newButton.SetValue(IsFirstProperty, count == 0);
                newButton.SetValue(IsLastProperty, count == (collection.Count - 1));

                ++count;
            }
        }

        private static void ClearAttachedProperties(ButtonBase buttonBase)
        {
            buttonBase.SetValue(IsMemberProperty, false);
            buttonBase.SetValue(IsFirstProperty, false);
            buttonBase.SetValue(IsLastProperty, false);
        }

        private static void ResetFirstAndLast(ButtonCollection collection)
        {
            var first = collection.Count > 0 ? collection[0] : null;
            if (first != null)
                first.SetValue(IsFirstProperty, true);

            var last = collection.Count > 0 ? collection[collection.Count - 1] : null;
            if (last != null)
                last.SetValue(IsLastProperty, true);
        }
    }

    public class ButtonCollection : ObservableCollection<ButtonBase>
    {
    }
}
