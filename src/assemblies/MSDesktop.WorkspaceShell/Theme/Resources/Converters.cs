﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Infragistics.Windows.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell;

#pragma warning disable 1591
namespace MSDesktop.WorkspaceShell.Theme.Resources
{
    [ValueConversion(typeof(object), typeof(UIElement))]    
    public  class ShellWindowTitleBarConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var shellWindow = value as ShellWindow;
            if (shellWindow == null) return null;

            return shellWindow.TitleBar;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


     [ValueConversion(typeof(TitleBarPlaceHolder), typeof(Thickness))]  
    public class GlassBorderThicknessConverter: IValueConverter
     {
         public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
         {
             var titlebar = value as TitleBarPlaceHolder;
             var left = SystemParameters2.Current.WindowNonClientFrameThickness.Left;
             return new Thickness(left, left + titlebar.Height, left, left );

         }

         public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
         {
             throw new NotImplementedException();
         }
     }


     [ValueConversion(typeof(TitleBarPlaceHolder), typeof(double))]
     public class GlassCaptionHeightConverter : IValueConverter
     {
         public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
         {
             var titlebar = value as TitleBarPlaceHolder;
             var left = SystemParameters2.Current.WindowNonClientFrameThickness.Left;
             return left + titlebar.Height;

         }

         public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
         {
             throw new NotImplementedException();
         }
     }

    [ValueConversion(typeof(object), typeof(bool))]
    public class NotNullConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    [ValueConversion(typeof(Color?), typeof(Brush))]
    public class AeroTitleBarColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var color = (Color?) value;
            return !color.HasValue ? Brushes.Transparent : new SolidColorBrush(color.Value) {Opacity = 0.4};
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    [ValueConversion(typeof(Color?), typeof(Brush))]
    public class NonAeroTitleBarColorConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var color = (Color?)values[0];
            var control = (XamRibbon)values[1];
            if (!color.HasValue) return control.FindResource(RibbonBrushKeys.CaptionPanelFillKey);
            return new SolidColorBrush(color.Value);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[] {};
        }
    }
}
#pragma warning restore 1591