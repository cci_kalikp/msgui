﻿using System.Windows;

namespace MSDesktop.WorkspaceShell.Theme.Keys
{
    public static class Brushes
    {
        public static ComponentResourceKey DefaultText = new ComponentResourceKey(typeof(Brushes), "DefaultText");
        public static ComponentResourceKey DeemphasizedText = new ComponentResourceKey(typeof(Brushes), "DeemphasizedText");
        public static ComponentResourceKey DefaultControlBackground = new ComponentResourceKey(typeof(Brushes), "DefaultControlBackground");
        public static ComponentResourceKey GlyphStroke = new ComponentResourceKey(typeof(Brushes), "GlyphStroke");
        public static ComponentResourceKey GlyphInactiveFill = new ComponentResourceKey(typeof(Brushes), "GlyphInactiveFill");
        public static ComponentResourceKey GlyphActiveFill = new ComponentResourceKey(typeof(Brushes), "GlyphActiveFill");

        public static ComponentResourceKey ApplicationMainBrandingBrushKey = new ComponentResourceKey(typeof(Brushes), "ApplicationMainBrandingBrush");

        // currently used for Morgan Stanley in the window title
        public static ComponentResourceKey WindowForegroundBrushKey = new ComponentResourceKey(typeof(Brushes), "WindowForegroundBrush");
        // introducing for the header/footer borders
        public static ComponentResourceKey WindowBackgroundBrushKey = new ComponentResourceKey(typeof(Brushes), "WindowBackgroundBrushKey");

        // currently only used by the chrome button glyphs for background on MouseOver (not used in Themes)
        public static ComponentResourceKey MouseOverGlyphButtonBackgroundBrushKey = new ComponentResourceKey(typeof(Brushes), "MouseOverControlBackgroundBrush");

        // used for bg of a border between tab content and header I think.  Gradient from dark->medium color with half opacity
        public static ComponentResourceKey TabContentHeaderBrushKey = new ComponentResourceKey(typeof(Brushes), "TabContentHeaderBrush");

        // Used by SettingsTabContentView for BG and provides it to the border on bottom.  Used for border brush of the 
        // header/footer/tabcontent containing borders as well as a canvas used I believe to draw a line across the top
        // of the tabcontrol. Uses darkColor currently.  
        public static ComponentResourceKey WindowBorderBrushKey = new ComponentResourceKey(typeof(Brushes), "WindowBorderBrushKey");
    }
}
