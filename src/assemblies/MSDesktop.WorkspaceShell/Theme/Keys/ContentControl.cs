﻿using System.Windows;

namespace MSDesktop.WorkspaceShell.Theme.Keys
{
    public class ContentControl
    {
        /* Styles */
        public static ComponentResourceKey ChromeContainerStyle = new ComponentResourceKey(typeof(ContentControl), "ChromeContainerStyle");

        /* Colors */
        public static ComponentResourceKey ShadowColor = new ComponentResourceKey(typeof(ContentControl), "ShadowColor");

        /* Brushes */
        public static ComponentResourceKey FrameBrush = new ComponentResourceKey(typeof(ContentControl), "FrameBrush");
        public static ComponentResourceKey BevelTopBorderBrush = new ComponentResourceKey(typeof(ContentControl), "BevelTopBorderBrush");
        public static ComponentResourceKey BevelRemainderBorderBrush = new ComponentResourceKey(typeof(ContentControl), "BevelRemainderBorderBrush");
        public static ComponentResourceKey MountBackgroundBrush = new ComponentResourceKey(typeof(ContentControl), "MountBackgroundBrush");
        public static ComponentResourceKey MountSideEdgeBrush = new ComponentResourceKey(typeof(ContentControl), "MountSideEdgeBrush");
        public static ComponentResourceKey MountTopBottomEdgeBrush = new ComponentResourceKey(typeof(ContentControl), "MountTopBottomEdgeBrush");
    }
}
