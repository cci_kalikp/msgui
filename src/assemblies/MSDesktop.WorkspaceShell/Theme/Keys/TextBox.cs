﻿using System.Windows;

namespace MSDesktop.WorkspaceShell.Theme.Keys
{
    public static class TextBox
    {
        /* Styles */
        public static ComponentResourceKey DefaultStyle = new ComponentResourceKey(typeof(TextBox), "DefaultStyle");

        /* Foreground & Background */
        public static ComponentResourceKey DefaultForegroundBrush = new ComponentResourceKey(typeof(TextBox), "DefaultForegroundBrush");
        public static ComponentResourceKey DefaultBackgroundBrush = new ComponentResourceKey(typeof(TextBox), "DefaultBackgroundBrush");

        /* Details */
        public static ComponentResourceKey HighlightBorderBrush = new ComponentResourceKey(typeof(TextBox), "HighlightBorderBrush");
        public static ComponentResourceKey MediumBorderBrush = new ComponentResourceKey(typeof(TextBox), "MediumBorderBrush");
        public static ComponentResourceKey ShadowBorderBrush = new ComponentResourceKey(typeof(TextBox), "ShadowBorderBrush");

        public static ComponentResourceKey InsetShadowBackgroundBrush = new ComponentResourceKey(typeof(TextBox), "InsetShadowBackgroundBrush");
    }
}
