﻿using System.Windows;

namespace MSDesktop.WorkspaceShell.Theme.Keys
{
    public static class Button
    {
        /* Styles */
        public static ComponentResourceKey DefaultStyle = new ComponentResourceKey(typeof(Button), "DefaultStyle");
        public static ComponentResourceKey SubmitStyle = new ComponentResourceKey(typeof(Button), "SubmitStyle");
        public static ComponentResourceKey CancelStyle = new ComponentResourceKey(typeof(Button), "CancelStyle");
        public static ComponentResourceKey InfoStyle = new ComponentResourceKey(typeof(Button), "InfoStyle");


        /* Foreground & Background */
        public static ComponentResourceKey DefaultForegroundBrush = new ComponentResourceKey(typeof(Button), "DefaultForegroundBrush");
        public static ComponentResourceKey DefaultBackgroundBrush = new ComponentResourceKey(typeof(Button), "DefaultBackgroundBrush");

        public static ComponentResourceKey SubmitForegroundBrush = new ComponentResourceKey(typeof(Button), "SubmitForegroundBrush");
        public static ComponentResourceKey SubmitBackgroundBrush = new ComponentResourceKey(typeof(Button), "SubmitBackgroundBrush");

        public static ComponentResourceKey CancelForegroundBrush = new ComponentResourceKey(typeof(Button), "CancelForegroundBrush");
        public static ComponentResourceKey CancelBackgroundBrush = new ComponentResourceKey(typeof(Button), "CancelBackgroundBrush");

        public static ComponentResourceKey InfoForegroundBrush = new ComponentResourceKey(typeof(Button), "InfoForegroundBrush");
        public static ComponentResourceKey InfoBackgroundBrush = new ComponentResourceKey(typeof(Button), "InfoBackgroundBrush");


        /* Details */
        public static ComponentResourceKey InsetShadowBorderBrush = new ComponentResourceKey(typeof(Button), "InsetShadowBorderBrush");
        public static ComponentResourceKey InsetHighlightBorderBrush = new ComponentResourceKey(typeof(Button), "InsetHighlightBorderBrush");

        public static ComponentResourceKey EmbossHighlightBorderBrush = new ComponentResourceKey(typeof(Button), "EmbossHighlightBorderBrush");
        public static ComponentResourceKey EmbossMediumBorderBrush = new ComponentResourceKey(typeof(Button), "EmbossMediumBorderBrush");
        public static ComponentResourceKey EmbossShadowBorderBrush = new ComponentResourceKey(typeof(Button), "EmbossShadowBorderBrush");

        public static ComponentResourceKey HoverOverlayBrush = new ComponentResourceKey(typeof(Button), "HoverOverlayBrush");

        public static ComponentResourceKey PressedInsetBrush = new ComponentResourceKey(typeof(Button), "PressedInsetBrush");
    }
}
