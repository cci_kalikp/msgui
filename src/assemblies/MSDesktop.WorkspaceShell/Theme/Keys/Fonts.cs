﻿using System.Windows;

namespace MSDesktop.WorkspaceShell.Theme.Keys
{
  public static class Fonts
  {
    public static ComponentResourceKey Default = new ComponentResourceKey(typeof(Fonts), "Default");
    public static ComponentResourceKey Monospaced = new ComponentResourceKey(typeof(Fonts), "Monospaced");
  }
}
