﻿using System.Windows;

namespace MSDesktop.WorkspaceShell.Theme.Keys
{
    public class ListBox
    {
        /* Styles */
        public static ComponentResourceKey DefaultStyle = new ComponentResourceKey(typeof(ListBox), "DefaultStyle");
        public static ComponentResourceKey DefaultItemContainerStyle = new ComponentResourceKey(typeof(ListBox), "DefaultItemContainerStyle");
        public static ComponentResourceKey ChromeItemContainerStyle = new ComponentResourceKey(typeof(ListBox), "ChromeItemContainerStyle");

        /* Foreground and Background */
        public static ComponentResourceKey DefaultBackgroundBrush = new ComponentResourceKey(typeof(ListBox), "DefaultBackgroundBrush");
    }
}
