﻿using System.Windows;
using System.Windows.Media;

namespace MSDesktop.WorkspaceShell.Theme.Keys
{
  public static class Colors
  {
    public static ComponentResourceKey DefaultText = new ComponentResourceKey(typeof(Colors), "DefaultText");
    public static ComponentResourceKey DeemphasizedText = new ComponentResourceKey(typeof(Colors), "DeemphasizedText");
    public static ComponentResourceKey DefaultControlBackground = new ComponentResourceKey(typeof(Colors), "DefaultControlBackground");

    public static ComponentResourceKey ApplicationMainBrandingColorKey = new ComponentResourceKey(typeof(Color), "ApplicationMainBrandingColor");

    // introduced to bring us towards the themes
    public static ComponentResourceKey DefaultControlBackgroundHighlight = new ComponentResourceKey(typeof(Colors), "DefaultControlBackgroundHighlight");
    public static ComponentResourceKey DefaultControlBackgroundLowlight = new ComponentResourceKey(typeof(Colors), "DefaultControlBackgroundLowlight");
  }
}
