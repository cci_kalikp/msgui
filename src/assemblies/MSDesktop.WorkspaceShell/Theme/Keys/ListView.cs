﻿using System.Windows;

namespace MSDesktop.WorkspaceShell.Theme.Keys
{
  public class ListView
  {
    /* Styles */
    public static ComponentResourceKey DefaultStyle = new ComponentResourceKey(typeof(ListView), "DefaultStyle");
    public static ComponentResourceKey DefaultItemContainerStyle = new ComponentResourceKey(typeof(ListView), "DefaultItemContainerStyle");

    /* Foreground and Background */
    public static ComponentResourceKey DefaultBackgroundBrush = new ComponentResourceKey(typeof(ListView), "DefaultBackgroundBrush");
 
  }
}