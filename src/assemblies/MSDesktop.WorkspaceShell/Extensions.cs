﻿using System;
using System.Collections.Generic;
using MSDesktop.WorkspaceShell.ChromeManager;
using MSDesktop.WorkspaceShell.ChromeManager.ChromeArea;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MSDesktop.WorkspaceShell
{
    public static class Extensions
    {
        public static void SetWorkspaceShellMode(this Framework framework_)
        {
            framework_.SetShellMode(ShellMode.External);
            framework_.Bootstrapper.ConfiguringContainer += (sender_, args_) => args_.Container.RegisterType<IExternalShellProvider, WorkspaceShellProvider>();
        }

        public static void AddWorkspaceShellTheme(this Framework framework_)
        {
            framework_.AddTheme(CreateWorkspaceShellTheme());
            PaneHeaderControl.Structure = HeaderStructure.WindowsModern;
        }

        private static MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes.Theme CreateWorkspaceShellTheme()
        {
            var theme = new MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes.Theme
            {
                Source =
                  new Uri("pack://application:,,,/MSDesktop.WorkspaceShell;component/Theme/Resources/ApplicationStyleBlack.xaml",
                          UriKind.RelativeOrAbsolute),
                Name = "WorkspaceShell"
            };            
            return theme;
        }
    }

    internal class WorkspaceShellProvider : IExternalShellProvider
    {
        public IShell GetShellInstance(IUnityContainer container_, InitialSettings initialSettings_)
        {
            return new WorkspaceShell(container_, initialSettings_);
        }

        public ChromeManagerBase GetChromeManagerInstance(ChromeRegistry chromeRegistry_, IShell shell_,
                                                          PersistenceProfileService persistenceProfileService_,
                                                          PersistenceService persistenceService_, IUnityContainer container_,
                                                          IWindowFactory factory_)
        {
            return new WorkspaceChromeManager(chromeRegistry_, shell_, persistenceProfileService_, persistenceService_, container_, factory_);
        }

        public ICommonFactories GetFactoriesInstance(IChromeRegistry registry_)
        {
            return new WorkspaceFactories(registry_);
        }
    }
}
