﻿using System.Windows;
using Infragistics.Windows.Controls;
using MSDesktop.WorkspaceShell.ChromeManager.ChromeArea;
using MSDesktop.WorkspaceShell.Controls;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.TabWell;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MSDesktop.WorkspaceShell.ChromeManager
{
    class WorkspaceChromeManager : ChromeManagerBase
    {
        internal WorkspaceChromeManager(ChromeRegistry chromeRegistry, IShell shell, PersistenceProfileService persistenceProfileService, PersistenceService persistenceService, IUnityContainer container, IWindowFactory windowFactory) : base(chromeRegistry, shell, persistenceProfileService, persistenceService, container, windowFactory)
        {
            WorkspaceViewSelector = ((WorkspaceShell) shell).WorkspaceViewSelector;

            TabWellFactories.Initialize(this);
            WorkspaceFactories.Initialize(this, shell);

            AddRootArea(MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ChromeArea.TabWell, new InitialTabWellRootParameters());
            AddRootArea(MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ChromeArea.WorkspaceRootArea, new InitialWorkspaceRootAreaParameters());

            this[MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ChromeArea.TabWell].AddWidget(TabWellRootArea.TabWellLeftEdgeID,
                                                     new InitialTabWellEdgeParameters
                                                     {
                                                         TabControl =
                                                             LogicalTreeHelper.FindLogicalNode(
                                                                 shell.MainWindow, "tabControl") as
                                                             XamTabControl,
                                                         Side = HorizontalAlignment.Left
                                                     });
            this[MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ChromeArea.TabWell].AddWidget(TabWellRootArea.TabWellRightEdgeID,
                                                     new InitialTabWellEdgeParameters
                                                     {
                                                         TabControl =
                                                             LogicalTreeHelper.FindLogicalNode(
                                                                 shell.MainWindow, "tabControl") as
                                                             XamTabControl,
                                                         Side = HorizontalAlignment.Right
                                                     });

            var tdvm = shell.WindowManager as TabbedDockViewModel;
            if (tdvm != null)
            {
                ((TabWellRootArea) this[MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ChromeArea.TabWell]).DockViewModel = tdvm;
            }

            DefaultChromeArea = (WidgetViewContainer) this[MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ChromeArea.WorkspaceRootArea];
        }

        public ViewSelector WorkspaceViewSelector { get; set; }

        internal override IWidgetViewContainer GetRootContainer()
        {
            return this[MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ChromeArea.WorkspaceRootArea];
        }
    }
}
