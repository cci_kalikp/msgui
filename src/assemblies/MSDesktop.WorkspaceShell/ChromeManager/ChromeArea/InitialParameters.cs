﻿using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MSDesktop.WorkspaceShell.ChromeManager.ChromeArea
{
    class InitialWorkspaceRootAreaParameters : InitialWidgetParameters
    {
    }

    class InitialWorkspaceGroupParameters : InitialWidgetParameters
    {
        public string Name { get; set; }
    }
}
