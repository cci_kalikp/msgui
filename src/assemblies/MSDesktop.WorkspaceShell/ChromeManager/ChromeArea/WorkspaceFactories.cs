﻿using System.Xml.Linq;
using MSDesktop.WorkspaceShell.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;

namespace MSDesktop.WorkspaceShell.ChromeManager.ChromeArea
{
    internal class WorkspaceFactories : ICommonFactories
    {
        private readonly IChromeRegistry _registry;
        private static IChromeManager _chromeManager;
        private static bool _staticFactoriesInitialized;
        private static IShell _shell;

        public InitializeWidgetHandler ButtonFactory { get; private set; }
        public InitializeWidgetHandler SplitButtonFactory { get; private set; }
        public InitializeWidgetHandler ShowWindowButtonFactory { get
        {
            return ShowWindowButtonFactoryMethod;
        } }
        public InitializeWidgetHandler DropdownButtonFactory { get; private set; }
        public InitializeWidgetHandler ComboBoxFactory { get; private set; }
        public InitializeWidgetHandler PopupFactory { get; private set; }

        public WorkspaceFactories(IChromeRegistry registry_)
        {
            _registry = registry_;
            _registry.RegisterWidgetFactoryMapping<InitialShowWindowButtonParameters>(ShowWindowButtonFactory); 
        }

        internal void Initialize(IChromeManager chromeManager_)
        {
            _chromeManager = chromeManager_;
            if (!_staticFactoriesInitialized)
            {
                InitializeWorkspaceFactories(_registry);
            }
        }

        internal static void Initialize(WorkspaceChromeManager chromeManager_, IShell shell_)
        {
            _chromeManager = chromeManager_;
            _shell = shell_;

            if (!_staticFactoriesInitialized)
            {
                InitializeWorkspaceFactories(chromeManager_.Registry);
            }
        }

        private static void InitializeWorkspaceFactories(IChromeRegistry chromeRegistry_)
        {
            chromeRegistry_.RegisterWidgetFactoryMapping<InitialWorkspaceRootAreaParameters>(RibbonRootFactory);
            chromeRegistry_.RegisterWidgetFactoryMapping<InitialWorkspaceGroupParameters>(WorkspaceGroupFactory);
            _staticFactoriesInitialized = true;
        }

        [FactoryOptions(typeof(WorkspaceGroupArea))]
        private static bool WorkspaceGroupFactory(IWidgetViewContainer vc_, XDocument state_)
        {
            vc_.Content = ((WorkspaceChromeManager)_chromeManager).WorkspaceViewSelector;
            return true;
        }

        [FactoryOptions(typeof(WorkspaceRootArea))]
        private static bool RibbonRootFactory(IWidgetViewContainer vc_, XDocument state_)
        {
            vc_.Content = ((WorkspaceChromeManager)_chromeManager).WorkspaceViewSelector;
            return true;
        }

        [FactoryOptions(typeof (WidgetViewContainer))]
        private bool ShowWindowButtonFactoryMethod(IWidgetViewContainer emptyWidget_, XDocument state_)
        {
            var parameters = (InitialShowWindowButtonParameters) emptyWidget_.Parameters;
            emptyWidget_.Content = new WorkspaceToolViewModel
                {
                    WindowFactoryId = parameters.WindowFactoryID,
                    Name = parameters.Text,
                    Image = parameters.Image,
                    Description = parameters.ToolTip
                };
            return true;
        }
    }
}
