﻿using System;
using MSDesktop.WorkspaceShell.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MSDesktop.WorkspaceShell.ChromeManager.ChromeArea
{
    class WorkspaceRootArea : WidgetViewContainer
    {
        private ViewSelector _viewSelector;

        public WorkspaceRootArea(string id_, InitialWidgetParameters parameters_) : base(id_, parameters_)
        {
            ContentReady += OnContentReady;
        }

        private void OnContentReady(object sender_, EventArgs e_)
        {
            _viewSelector = (ViewSelector)Content;
        }

        protected override InitialWidgetParameters DoAddDefault(ref string ID)
        {
            var parameters = new InitialWorkspaceGroupParameters {Name = ID};
            ID = Guid.NewGuid().ToString();
            return parameters;
        }
    }
}