﻿using System;
using MSDesktop.WorkspaceShell.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MSDesktop.WorkspaceShell.ChromeManager.ChromeArea
{
    class WorkspaceGroupArea : WidgetViewContainer
    {
        private readonly string _name;
        private ViewSelectorViewModel _viewSelector;

        public WorkspaceGroupArea(string factoryId_, InitialWidgetParameters parameters_) : base(factoryId_, parameters_)
        {
            ContentReady += OnContentReady;
            _name = ((InitialWorkspaceGroupParameters) parameters_).Name;
        }

        private void OnContentReady(object sender_, EventArgs e_)
        {
            _viewSelector = (ViewSelectorViewModel)(((ViewSelector)Content).DataContext);
        }

        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            var tool = (WorkspaceToolViewModel) child.Content;
            tool.GroupName = _name;
            _viewSelector.AddTool(tool);
            return true;
        }

        protected override bool DoCanAddwidget(IWidgetViewContainer widget)
        {
            return widget.Content is WorkspaceToolViewModel;
        }
    }
}
