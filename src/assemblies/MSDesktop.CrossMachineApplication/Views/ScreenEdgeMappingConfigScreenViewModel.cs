﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MSDesktop.CrossMachineApplication.Views
{
    class ScreenEdgeMappingConfigScreenViewModel : INotifyPropertyChanged
    {
        private const string ScreenEdgeMappingPersistorId = "{4f91c2fb-b9dd-4210-ac9c-f9b9f7281a2f}.ScreenEdgeMappingPersistor";
        private readonly IWindowTransferManager m_transferManager;
        private readonly IPersistenceService m_persistenceService;

        private string m_leftEdge;
        [DisplayName("Left Edge")]
        [Description("Enter a valid hostname of a machine running an instance of this application")]
        public string LeftEdge
        {
            get
            {
                return m_leftEdge;
            }
            set { 
                m_leftEdge = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LeftEdge"));
            }
        }

        private string m_rightEdge;
        [DisplayName("Right Edge")]
        [Description("Enter a valid hostname of a machine running an instance of this application")]
        public string RightEdge
        {
            get
            {
                return m_rightEdge;
            }
            set
            {
                m_rightEdge = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("RightEdge"));
            }
        }

        private string m_topEdge;
        [DisplayName("Top Edge")]
        [Description("Enter a valid hostname of a machine running an instance of this application")]
        public string TopEdge
        {
            get
            {
                return m_topEdge;
            }
            set
            {
                m_topEdge = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("TopEdge"));
            }
        }

        private string m_bottomEdge;
        [DisplayName("Bottom Edge")]
        [Description("Enter a valid hostname of a machine running an instance of this application")]
        public string BottomEdge
        {
            get
            {
                return m_bottomEdge;
            }
            set
            {
                m_bottomEdge = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("BottomEdge"));
            }
        }

        public ICommand ApplyCommand { get; internal set; }

        public ScreenEdgeMappingConfigScreenViewModel(IWindowTransferManager transferManager, IPersistenceService persistenceService)
        {
            m_transferManager = transferManager;
            m_persistenceService = persistenceService;

            m_persistenceService.AddPersistor(ScreenEdgeMappingPersistorId, RestoreScreenEdgeMapping, SaveScreenEdgeMapping);

            ApplyCommand = new RelayCommand(ExecuteApply, CanExecuteApply);
            GetMappingFromTranferManager();
        }

        private XDocument SaveScreenEdgeMapping()
        {
            return new XDocument(
                new XElement("ScreenEdgeMapping",
                    new XElement("LeftEdge", GetMappingOrEmptyString(ScreenEdge.Left)),
                    new XElement("RightEdge", GetMappingOrEmptyString(ScreenEdge.Right)),
                    new XElement("TopEdge", GetMappingOrEmptyString(ScreenEdge.Top)),
                    new XElement("BottomEdge", GetMappingOrEmptyString(ScreenEdge.Bottom))
                    ));
        }

        private void RestoreScreenEdgeMapping(XDocument xmlState)
        {
            m_transferManager.ScreenEdgeMapping[ScreenEdge.Left] = xmlState.Descendants("LeftEdge").First().Value;
            m_transferManager.ScreenEdgeMapping[ScreenEdge.Right] = xmlState.Descendants("RightEdge").First().Value;
            m_transferManager.ScreenEdgeMapping[ScreenEdge.Top] = xmlState.Descendants("TopEdge").First().Value;
            m_transferManager.ScreenEdgeMapping[ScreenEdge.Bottom] = xmlState.Descendants("BottomEdge").First().Value;
            GetMappingFromTranferManager();
        }

        private void GetMappingFromTranferManager()
        {
            LeftEdge = GetMappingOrEmptyString(ScreenEdge.Left);
            RightEdge = GetMappingOrEmptyString(ScreenEdge.Right);
            BottomEdge = GetMappingOrEmptyString(ScreenEdge.Bottom);
            TopEdge = GetMappingOrEmptyString(ScreenEdge.Top);
        }

        private string GetMappingOrEmptyString(ScreenEdge screenEdge)
        {
            return m_transferManager.ScreenEdgeMapping.ContainsKey(screenEdge) ? m_transferManager.ScreenEdgeMapping[screenEdge] : "";
        }

        private static bool CanExecuteApply(object o)
        {
            return true;
        }

        private void ExecuteApply(object o)
        {
            m_transferManager.ScreenEdgeMapping[ScreenEdge.Left] = LeftEdge.ToLower();
            m_transferManager.ScreenEdgeMapping[ScreenEdge.Right] = RightEdge.ToLower();
            m_transferManager.ScreenEdgeMapping[ScreenEdge.Bottom] = BottomEdge.ToLower();
            m_transferManager.ScreenEdgeMapping[ScreenEdge.Top] = TopEdge.ToLower();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void InvokePropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, e);
        }
    }
}
