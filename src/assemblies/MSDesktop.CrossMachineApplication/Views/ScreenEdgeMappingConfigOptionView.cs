﻿using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core; 

namespace MSDesktop.CrossMachineApplication.Views
{
    class ScreenEdgeMappingConfigOptionView : IOptionViewWithModel
    {
        private readonly ScreenEdgeMappingConfigScreen m_view;
        private readonly ScreenEdgeMappingConfigScreenViewModel m_viewModel;

        public ScreenEdgeMappingConfigOptionView(ScreenEdgeMappingConfigScreen view)
        {
            m_view = view;
            m_viewModel = view.DataContext as ScreenEdgeMappingConfigScreenViewModel;
        } 

        public void OnOK()
        {
            m_viewModel.ApplyCommand.Execute(null);
        }

        public void OnCancel()
        { }

        public void OnDisplay()
        { }

        public void OnHelp()
        { }

        public UIElement Content { get { return m_view; } }


        public object ViewModel
        {
            get { return m_viewModel; }
        }
    }
}
