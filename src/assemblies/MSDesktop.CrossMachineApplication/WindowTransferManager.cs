﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq; 
using System.Windows;
using System.Xml.Linq;
using CrossMachineDD;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDesktop.IPC;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MSDesktop.CrossMachineApplication.Messages;


namespace MSDesktop.CrossMachineApplication
{
    internal sealed class WindowTransferManager : IWindowTransferManager
    {
        private readonly ChromeManagerBase _chromeManager;
        private readonly EventRegistrator _eventRegistrator;
        private readonly IChromeRegistry _chromeRegistry;
        private readonly bool _crossApplicationMode;
        private readonly IPCClient _ipcClient;

        /// <summary>
        /// Maps view container ids to addresses of applications currently hosting given view. Address means
        /// either process id or hostname.
        /// </summary>
        private readonly Dictionary<string, string> _remoteViews = new Dictionary<string, string>();
        private readonly Dictionary<string, IDisposable> _subscriptionForwarders = new Dictionary<string, IDisposable>();
        private readonly string _hostName;

        internal event EventHandler CrossProcessDragEnter;
        internal event EventHandler CrossProcessDragLeave;
        
        public WindowTransferManager(EventRegistrator eventRegistrator, IChromeManager chromeManager, IChromeRegistry chromeRegistry_, bool crossApplicationMode_=false)
        {
            _eventRegistrator = eventRegistrator;
            _chromeRegistry = chromeRegistry_;
            _crossApplicationMode = crossApplicationMode_;
            _chromeManager = chromeManager as ChromeManagerBase;

            _ipcClient = !_crossApplicationMode
                             ? new IPCClient(IPCMode.MachineWide | IPCMode.CPSBased, IPCOptions)
                             : new IPCClient(IPCMode.MachineWide, new IPCOptions());
            _hostName = Environment.MachineName.ToLower();
            ScreenEdgeMapping = new Dictionary<ScreenEdge, string>();

            _eventRegistrator.UnregisteringComponent += (sender_, args_) =>
                {
                    if ((args_.ViewContainer.Parameters is InitialTransferableWindowParameters))
                    {
                        if ((args_.ViewContainer.Parameters as InitialTransferableWindowParameters).Transfered)
                        {
                            args_.Cancel = true;
                        }
                    }
                };

            ((ChromeRegistry) chromeRegistry_).RegisterWindowFactoryMapping<InitialTransferableWindowParameters>(CreateTransferableWindow);
        }

        [FactoryOptions(typeof(TransferableWindowViewContainer))]
        private bool CreateTransferableWindow(IWindowViewContainer emptyViewContainer_, XDocument state_)
        {
            return true;
        }

        public IDictionary<ScreenEdge, string> ScreenEdgeMapping { get; set; }
        public static IPCOptions IPCOptions { get; set; }
        
        public void Listen()
        {
            if (!_crossApplicationMode)
            {
                _ipcClient
                    .GetObservable<TransferWindowMessage>()
                    .ObserveOnDispatcher()
                    .Where(new IPCTarget {Key = "address", Value = _hostName})
                    .Subscribe(ProcessMessage);
                _ipcClient
                    .GetObservable<ForwardedV2VPublicationMessage>()
                    .ObserveOnDispatcher()
                    .Where(new IPCTarget { Key = "address", Value = _hostName })
                    .Subscribe(ProcessForwardedV2VMessage);
                _ipcClient
                    .GetObservable<ForwardedV2VRemotePublicationMessage>()
                    .ObserveOnDispatcher()
                    .Where(new IPCTarget { Key = "address", Value = _hostName })
                    .Subscribe(ProcessForwardedV2VRemoteMessage);
                _ipcClient
                    .GetObservable<CloseWindowMessage>()
                    .ObserveOnDispatcher()
                    .Where(new IPCTarget { Key = "address", Value = _hostName })
                    .Subscribe(ProcessCloseMessage);
            }
            else
            {
                _ipcClient
                    .GetObservable<TransferWindowMessage>()
                    .ObserveOnDispatcher()
                    .Where(new IPCTarget { Key = "address", Value = Process.GetCurrentProcess().Id.ToString() })
                    .Subscribe(ProcessMessage);
                _ipcClient
                    .GetObservable<ForwardedV2VPublicationMessage>()
                    .ObserveOnDispatcher()
                    .Where(new IPCTarget { Key = "address", Value = Process.GetCurrentProcess().Id.ToString() })
                    .Subscribe(ProcessForwardedV2VMessage);
                _ipcClient
                    .GetObservable<ForwardedV2VRemotePublicationMessage>()
                    .ObserveOnDispatcher()
                    .Where(new IPCTarget { Key = "address", Value = Process.GetCurrentProcess().Id.ToString() })
                    .Subscribe(ProcessForwardedV2VRemoteMessage);
                _ipcClient
                    .GetObservable<CloseWindowMessage>()
                    .ObserveOnDispatcher()
                    .Where(new IPCTarget { Key = "address", Value = Process.GetCurrentProcess().Id.ToString() })
                    .Subscribe(ProcessCloseMessage);
                _ipcClient
                    .GetObservable<DragEnterMessage>()
                    .ObserveOnDispatcher()
                    .Where(wrapper_ => wrapper_.Payload.ProcessId == Process.GetCurrentProcess().Id)
                    .Subscribe(wrapper_ => OnCrossProcessDragEnter());
                _ipcClient
                    .GetObservable<DragLeaveMessage>()
                    .ObserveOnDispatcher()
                    .Where(wrapper_ => wrapper_.Payload.ProcessId == Process.GetCurrentProcess().Id)
                    .Subscribe(wrapper_ => OnCrossProcessDragLeave());
            }

            _chromeManager.Windows.CollectionChanged +=
                (o, eventArgs) =>
                    {
                        if (eventArgs.NewItems != null)
                        {
                            foreach (IWindowViewContainer window in eventArgs.NewItems)
                            {
                                if (!(window.Parameters is InitialTransferableWindowParameters)) return;
                                window.Closed += WindowClosed;
                                window.ClosedQuietly += WindowClosed;
                            }
                        }
                        if (eventArgs.OldItems != null)
                        {
                            foreach (IWindowViewContainer window in eventArgs.OldItems)
                            {
                                if (!(window.Parameters is InitialTransferableWindowParameters)) return;
                                window.Closed -= WindowClosed;
                                window.ClosedQuietly -= WindowClosed;
                            }
                        }
                    };
        }

        public void TransferWindowGroup(WindowGroupDroppedOnScreenEdgeEventArgs eventArgs)
        {
            if (eventArgs.FloatingWindow == null) return;

            string edgeHostValue;
            if (ScreenEdgeMapping.TryGetValue(eventArgs.Edge, out edgeHostValue) && !string.IsNullOrEmpty(edgeHostValue))
            {
                if (!_hostName.Equals(edgeHostValue, StringComparison.InvariantCultureIgnoreCase))
                { 
                    var layoutElement =
                        _chromeManager.WindowManager.WriteSubState(new IWindowViewContainerHostRoot[] { eventArgs.FloatingWindow });

                    var windows = eventArgs.FloatingWindow.Windows;
                    var anyViewContainer = windows.First();
                    var origin = ((InitialTransferableWindowParameters) anyViewContainer.Parameters).OriginHost ?? _hostName;

                    var rootSplitPane = DockHelper.GetRootOfFloatingPane(anyViewContainer);

                    var panesElement = new XElement("panes");
                    var xdoc = new XDocument(new XElement(_chromeManager.DockLayoutRootName, layoutElement, panesElement));
                    var panesToClose = new List<IWindowViewContainer>();

                    foreach (var window in windows)
                    {
                        if (!(window.Parameters is InitialTransferableWindowParameters)) return;

                        var windowXml = _chromeManager.GetWindowState(window);

                        if (((InitialTransferableWindowParameters)window.Parameters).OriginHost != edgeHostValue)
                        {
                            _remoteViews[window.ID] = edgeHostValue;
                            SetupSubscriptionForwarding(window);
                            (window.Parameters as InitialTransferableWindowParameters).Transfered = true;
                        }

                        panesToClose.Add(window);
                        panesElement.Add(windowXml.Root);
                    }

                    var messageBuilder = TransferWindowMessage.CreateBuilder()
                                                              .SetWindow(xdoc.ToString())
                                                              .SetOrigin(origin)
                                                              .SetEdge((int) eventArgs.Edge);
                    if (rootSplitPane != null)
                    {
                        messageBuilder.SetRelativeOriginX(rootSplitPane.Left/ScreenSizeHelper.TotalScreenArea.Width)
                                      .SetRelativeOriginY(rootSplitPane.Top/ScreenSizeHelper.TotalScreenArea.Height);
                    }

                    var transferWindowMessage = messageBuilder.Build();

                    _ipcClient.Publish(transferWindowMessage, new IIPCTarget[]
                        {
                            new IPCTarget {Key = "address", Value = edgeHostValue.ToLower()}
                        });

                    foreach (var paneToClose in panesToClose)
                    {
                        paneToClose.Close();
                    }
                }
                else
                {
                    TaskDialog.ShowMessage("Cannot send window to yourself", "Transfer error",
                                           TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
                }
            }
            else
            {
                TaskDialog.ShowMessage("There is no host associated with this edge", "Transfer error",
                                       TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
            }
        }

        public void TransferWindowGroup(IFloatingWindow floatingWindow_, int processId_)
        {
            var panes = floatingWindow_.Windows;
            var layoutElement = _chromeManager.WindowManager.WriteSubState(new[] { floatingWindow_ });

            var anyViewContainer = panes.First();
            var origin = Process.GetCurrentProcess().Id;

            var rootSplitPane = DockHelper.GetRootOfFloatingPane(anyViewContainer);

            var panesElement = new XElement("panes");
            var xdoc = new XDocument(new XElement(_chromeManager.DockLayoutRootName, layoutElement, panesElement));

            var containersToClose = new List<IWindowViewContainer>();
            foreach (var viewContainer in panes)
            {
                if (!(viewContainer.Parameters is InitialTransferableWindowParameters)) return;

                var windowXml = _chromeManager.GetWindowState(viewContainer);

                if (((InitialTransferableWindowParameters)viewContainer.Parameters).OriginHost !=
                    processId_.ToString())
                {
                    _remoteViews[viewContainer.ID] = processId_.ToString();
                    SetupSubscriptionForwarding(viewContainer);
                    (viewContainer.Parameters as InitialTransferableWindowParameters).Transfered = true;
                }

                panesElement.Add(windowXml.Root);
                containersToClose.Add(viewContainer);
            }

            var messageBuilder = TransferWindowMessage.CreateBuilder()
                                                      .SetWindow(xdoc.ToString())
                                                      .SetOrigin(origin.ToString())
                                                      .SetEdge((int) ScreenEdge.Top);
            if (rootSplitPane != null)
            {
                messageBuilder.SetRelativeOriginX(rootSplitPane.Left)
                              .SetRelativeOriginY(rootSplitPane.Top);
            }
            
            var transferWindowMessage = messageBuilder.Build();
            _ipcClient.Publish(transferWindowMessage, new IPCTarget { Key = "address", Value = processId_.ToString()});

            Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    foreach (var windowViewContainer in containersToClose)
                    {
                        windowViewContainer.Close();
                    }
                }));
        }

        public void NotifyProcessDragEnter(int processId_)
        {
            var payload = DragEnterMessage.CreateBuilder()
                                          .SetProcessId(processId_)
                                          .Build();
            _ipcClient.Publish(payload);
        }

        public void NotifyProcessDragLeave(int processId_)
        {
            var payload = DragLeaveMessage.CreateBuilder()
                                          .SetProcessId(processId_)
                                          .Build();
            _ipcClient.Publish(payload);
        }

        private void SetupSubscriptionForwarding(IWindowViewContainer viewContainer)
        {
            foreach (var type in _eventRegistrator.GetSubscribedTypes())
            {
                var getSubscriberMethod = typeof(IEventRegistrator).GetMethod("GetRegisteredSubscriber");
                var subscriber = getSubscriberMethod.MakeGenericMethod(type).Invoke(_eventRegistrator, new object[] { viewContainer });

                if (subscriber != null)
                {
                    var observable = subscriber.GetType().GetMethod("GetObservable").Invoke(subscriber, new object[] {});
                    var subscribeMethod = typeof (ObservableExtensions)
                        .GetMethods()
                        .First(m => m.Name.Equals("Subscribe") && m.GetParameters().Count() == 2)
                        .MakeGenericMethod(type);

                    var handlerFactory = GetType().GetMethod("GetMessageForwarder").MakeGenericMethod(type);
                    var handler = handlerFactory.Invoke(this, new object[] {viewContainer});
                    var subscribtion = subscribeMethod.Invoke(observable, new[] {observable, handler}) as IDisposable;
                    _subscriptionForwarders[viewContainer.ID] = subscribtion;
                }
            }
        }
        
        public Action<T> GetMessageForwarder<T>(IWindowViewContainer viewContainer)
        {
            return message =>
            {
                var payload = ForwardedV2VPublicationMessage.CreateBuilder()
                    .SetViewID(viewContainer.ID)
                    .SetMessage((message as ITransferableV2VMessage).SaveState().ToString())
                    .SetMessageType(message.GetType().AssemblyQualifiedName)
                    .Build();
                _ipcClient.Publish(payload, new IPCTarget { Key = "address", Value = _remoteViews[viewContainer.ID].ToLower() });
            };
        }
        
        private void SetupPublicationForwarding(IWindowViewContainer viewContainer_)
        {
            if (_remoteViews.ContainsKey(viewContainer_.ID)) return; // TODO remove exiting forwarding
            foreach (var type in _eventRegistrator.GetSubscribedTypes())
            {
                var getPublisherMethod = typeof (IEventRegistrator).GetMethod("GetRegisteredPublisher");
                var publisher = getPublisherMethod.MakeGenericMethod(type)
                                                    .Invoke(_eventRegistrator, new object[] {viewContainer_});
                if (publisher != null)
                {
                    var eventInfo = typeof (Publisher<>).MakeGenericType(type).GetEvent("PublisherPublishing");
                    var getPublishingHandlerMethod =
                        typeof (WindowTransferManager).GetMethod("GetPublisherPublishingHandler")
                                                      .MakeGenericMethod(type);
                    var handler = getPublishingHandlerMethod.Invoke(this, new object[] {viewContainer_});
                    eventInfo.AddEventHandler(publisher, (Delegate) handler);

                }
            }
        }

        public EventHandler<PublisherPublishingEventArgs> GetPublisherPublishingHandler<T>(IWindowViewContainer container_)
        {
            return (sender_, args_) =>
                {
                    var payload = ForwardedV2VRemotePublicationMessage
                        .CreateBuilder()
                        .SetMessageType(args_.Message.GetType().AssemblyQualifiedName)
                        .SetViewID(container_.ID)
                        .SetMessage(((ITransferableV2VMessage) args_.Message).SaveState().ToString())
                        .Build();
                    _ipcClient.Publish(payload, new IPCTarget { Key = "address", Value = ((InitialTransferableWindowParameters) container_.Parameters).OriginHost.ToLower() });
                };
        }

        void ProcessMessage(MorganStanley.MSDesktop.IPC.IPCPayloadWrapper<TransferWindowMessage> wrapper)
        { 
            string anyViewId = null;

            TransferWindowMessage message = wrapper.Payload;
            XDocument xdoc = XDocument.Parse(message.Window);
            var viewsElement = xdoc.Root.Elements().FirstOrDefault(xe => xe.Name == "panes");

            _chromeManager.WindowManager.ReadSubState(string.Empty, xdoc.Root, (id, newId) =>
                {
                    var viewElement = viewsElement
                        .Elements()
                        .FirstOrDefault(xe => xe.
                                                  Attributes().
                                                  Any(
                                                      attr =>
                                                      attr.Name == "Id" && ContentPaneIdConverter.ConvertToPaneId(attr.Value) == id));

                    if (viewElement == null)
                        return;

                    newId = ContentPaneIdConverter.ConverFromPaneId(newId);

                    viewElement.SetAttributeValue("Id", newId);

                    var initialWindowParameters = new InitialTransferableWindowParameters
                        {
                            OriginHost = _remoteViews.ContainsKey(newId) ? null : message.Origin
                        };
                   

                    var handler = new EventHandler<RemovingExistingComponentEventArgs>((sender_, args_) =>
                        {
                            args_ .Cancel = true;
                        });
                    _eventRegistrator.RemovingExistingComponent += handler;
                    _chromeManager.ApplyPaneNode(new XDocument(viewElement), viewElement, initialWindowParameters);
                    _eventRegistrator.RemovingExistingComponent -= handler;

                    var vc = _chromeManager.Windows.FirstOrDefault(container_ => container_.ID == newId);
                    SetupPublicationForwarding(vc);
                    
                    if (anyViewId == null) anyViewId = id;
                    if (_remoteViews.ContainsKey(vc.ID))
                    {
                        _remoteViews.Remove(vc.ID);
                    }
                }, true);
            CalculateWindowGroupPosition(anyViewId, message);
        }

        //private static string DecodeViewId(string viewId_)
        //{
        //    return viewId_.Split(Convert.ToChar(":"))[2];
        //}

        private void CalculateWindowGroupPosition(string anyViewId_, TransferWindowMessage message_)
        {
            var someTransferredView =
                _chromeManager.Windows.FirstOrDefault(w_ => ContentPaneIdConverter.ConvertToPaneId(w_.ID) == anyViewId_);
            if (someTransferredView == null) return;
            var pane = DockHelper.GetRootOfFloatingPane(someTransferredView);

            if (_crossApplicationMode)
            {
                pane.Top = message_.RelativeOriginY;
                pane.Left = message_.RelativeOriginX;
                return;
            }

            var location = new Point();
            if (message_.HasRelativeOriginX)
                location.X = message_.RelativeOriginX*ScreenSizeHelper.TotalScreenArea.Width;
            else
                location.X = ScreenSizeHelper.TotalScreenArea.Width/2;
            if (message_.HasRelativeOriginY)
                location.Y = message_.RelativeOriginY*ScreenSizeHelper.TotalScreenArea.Height;
            else
                location.Y = ScreenSizeHelper.TotalScreenArea.Height/2;

            var newLocation = new Point();
            switch ((ScreenEdge) message_.Edge)
            {
                case ScreenEdge.Top:
                    newLocation.Y = ScreenSizeHelper.TotalScreenArea.Height;
                    newLocation.X = location.X;
                    break;
                case ScreenEdge.Bottom:
                    newLocation.Y = 0;
                    newLocation.X = location.X;
                    break;
                case ScreenEdge.Left:
                    newLocation.Y = location.Y;
                    newLocation.X = ScreenSizeHelper.TotalScreenArea.Width;
                    break;
                case ScreenEdge.Right:
                    newLocation.Y = location.Y;
                    newLocation.X = 0;
                    break;
            }
            pane.Top = newLocation.Y;
            pane.Left = newLocation.X;
        }

        private void ProcessForwardedV2VRemoteMessage(
            MorganStanley.MSDesktop.IPC.IPCPayloadWrapper<ForwardedV2VRemotePublicationMessage> wrapper)
        {
            ForwardedV2VRemotePublicationMessage payload = wrapper.Payload;
            var viewID = payload.ViewID;
            var type = Type.GetType(payload.MessageType);

            object m = Activator.CreateInstance(type);
            typeof (ITransferableV2VMessage).GetMethod("LoadState")
                                            .Invoke(m, new object[] {XDocument.Parse(payload.Message)});

            var getPublisherMethod = typeof(EventRegistrator).GetMethod("GetRegisteredPublisher", new[] { typeof(string) });
            var publisher = getPublisherMethod.MakeGenericMethod(m.GetType())
                                                .Invoke(_eventRegistrator, new object[] {viewID});
            if (publisher != null)
            {
                var publishMethod = typeof (IPublisher<>).MakeGenericType(new[] {m.GetType()}).GetMethod("Publish");
                publishMethod.Invoke(publisher,
                                        new[] {m, CommunicationTargetFilter.All, new string[] {}});
            }
        }

        void ProcessForwardedV2VMessage(MorganStanley.MSDesktop.IPC.IPCPayloadWrapper<ForwardedV2VPublicationMessage> wrapper)
        {
            ForwardedV2VPublicationMessage message = wrapper.Payload;
            var viewID = message.ViewID;
            var type = Type.GetType(message.MessageType);

            object m = Activator.CreateInstance(type);
            typeof(ITransferableV2VMessage).GetMethod("LoadState").Invoke(m, new object[] { XDocument.Parse(message.Message) });

            var window = _chromeManager.Windows.FirstOrDefault(vc => vc.ID.Equals(viewID));
            if (window != null) // window might have been closed
            {
                var getSubscriberMethod = typeof(EventRegistrator).GetMethod("GetRegisteredSubscriber", new[] { typeof(IViewContainerBase)});
                var subscriber = getSubscriberMethod.MakeGenericMethod(m.GetType()).Invoke(_eventRegistrator, new object[] { window });

                var receiveMethod = typeof(IObjectSubscriber).GetMethod("Receive");
                receiveMethod.Invoke(subscriber, System.Reflection.BindingFlags.NonPublic, null, new[] {m}, null);
            }
        }

        void WindowClosed(object sender, WindowEventArgs args)
        {
            if (!((InitialTransferableWindowParameters) args.ViewContainer.Parameters).Transfered)
            {
                _ipcClient.Publish(CloseWindowMessage.CreateBuilder().SetViewID(args.ViewContainer.ID).Build(),
                    new IPCTarget { Key = "address", Value = Process.GetCurrentProcess().Id.ToString() });
            }
        }

        void ProcessCloseMessage(MorganStanley.MSDesktop.IPC.IPCPayloadWrapper<CloseWindowMessage> wrapper)
        {
            var id = wrapper.Payload.ViewID;
            var window = _chromeManager.Windows.FirstOrDefault(w => w.ID.Equals(id));
            if (window != null)
            {
                _remoteViews.Remove(window.ID);
                if (_subscriptionForwarders.ContainsKey(window.ID))
                {
                    _subscriptionForwarders[window.ID].Dispose();
                    _subscriptionForwarders.Remove(window.ID);
                }
            }
            // TODO cleanup v2v connections
        }

        private void OnCrossProcessDragEnter()
        {
            var handler = CrossProcessDragEnter;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        private void OnCrossProcessDragLeave()
        {
            var handler = CrossProcessDragLeave;
            if (handler != null) handler(this, EventArgs.Empty);
        }
    }
}
