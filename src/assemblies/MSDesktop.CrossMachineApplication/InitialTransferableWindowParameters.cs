﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MSDesktop.CrossMachineApplication
{
    public class InitialTransferableWindowParameters : InitialWindowParameters
    {
        public string OriginHost { get; set; }
        public bool Transfered { get; set; }
    }
}
