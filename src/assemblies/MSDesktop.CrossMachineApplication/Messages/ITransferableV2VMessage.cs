﻿using System.Xml.Linq;

namespace MSDesktop.CrossMachineApplication.Messages
{
    public interface ITransferableV2VMessage
    {
        XDocument SaveState();
        void LoadState(XDocument state);
    }
}
