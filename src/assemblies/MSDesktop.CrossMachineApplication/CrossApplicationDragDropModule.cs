﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media; 
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

namespace MSDesktop.CrossMachineApplication
{
    class CrossApplicationDragDropModule : IModule
    {
        private readonly IUnityContainer _container;
        private readonly IWindowManager _windowMananger;
        private readonly IChromeManager _chromeManager;
        private WindowTransferManager _windowTransferManager;

        public CrossApplicationDragDropModule(IUnityContainer container_, IWindowManager windowMananger_, IChromeManager chromeManager_)
        {
            _container = container_;
            _windowMananger = windowMananger_;
            _chromeManager = chromeManager_;
        }

        public void Initialize()
        {
            _container.RegisterType<IWindowTransferManager, WindowTransferManager>(
                new ContainerControlledLifetimeManager());
            _windowTransferManager = new WindowTransferManager(_container.Resolve<EventRegistrator>(),
                _container.Resolve<IChromeManager>(), _container.Resolve<IChromeRegistry>(), true);
            _container.RegisterInstance(_windowTransferManager);

            _windowMananger.CrossProcessDragDropAction += (sender_, args_) =>
                {
                    if (args_.Action == CrossProcessDragDropAction.Enter)
                    {
						if (Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt))
						{
						    var panes = args_.FloatingWindow.Windows;
                        	if (!panes.All(p_ => p_.Parameters is InitialTransferableWindowParameters)) return;
                        	_windowTransferManager.NotifyProcessDragEnter(args_.ProcessId);
						}
                    }
                    else if (args_.Action == CrossProcessDragDropAction.Leave)
                    {
                        var panes = XamDockHelper.GetPanes(args_.FloatingWindow);
                        if (!panes.All(
                                p_ =>
                                ((WindowViewContainer) Attached.GetViewContainer(p_)).Parameters is
                                InitialTransferableWindowParameters)) return;
                        _windowTransferManager.NotifyProcessDragLeave(args_.ProcessId);
                    }
                    else
                    {
                        if (Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt))
                        {
                            _windowTransferManager.TransferWindowGroup(args_.FloatingWindow, args_.ProcessId);
                            _windowTransferManager.NotifyProcessDragLeave(args_.ProcessId);
                        }
                    }
                };
            _windowTransferManager.CrossProcessDragEnter += 
                (sender_, args_) => _chromeManager.ToggleOverlayMode(true,
                                                                     new SolidColorBrush { Color = Colors.Green, Opacity = 0.2 },
                                                                     () => new TextBlock
                                                                         {
                                                                             Text = "Drop to transfer",
                                                                             VerticalAlignment = VerticalAlignment.Center,
                                                                             HorizontalAlignment = HorizontalAlignment.Center,
                                                                             FontSize = 32,
                                                                             Foreground = Brushes.White
                                                                         });
            _windowTransferManager.CrossProcessDragLeave +=
                (sender_, args_) => _chromeManager.ToggleOverlayMode(false);

            _windowTransferManager.Listen();
        }
    }
}
