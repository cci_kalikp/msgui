﻿using System.Collections.Generic;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MSDesktop.CrossMachineApplication
{
    public interface IWindowTransferManager
    {
        IDictionary<ScreenEdge, string> ScreenEdgeMapping { get; set; }
    }
}