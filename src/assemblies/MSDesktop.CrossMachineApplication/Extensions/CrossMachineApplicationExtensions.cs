﻿using System;
using System.Collections.Generic;
using MorganStanley.MSDesktop.IPC;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MSDesktop.CrossMachineApplication.Extensions
{
    public static class CrossMachineApplicationExtensions
    {
        public static void EnableCrossMachineDragAndDrop(this Framework f, IPCOptions options)
        {
            WindowTransferManager.IPCOptions = options;  
            EnableCrossApplicationDragAndDrop(f);
        }

        public static void EnableCrossApplicationDragAndDrop(this Framework f)
        {
            ExtensionsUtils.RequireAssembly(ExternalAssembly.MSDesktopIPC, ExternalAssembly.ReactiveThreading, ExternalAssembly.ReactiveInterfaces, ExternalAssembly.ReactiveCore, ExternalAssembly.ReactiveLinq);
            f.AddModule<CrossApplicationDragDropModule>();
            TabbedDock.FirePaneDroppedAtScreenEdge = true;
            TabbedDock.VisualFeedbackOnPaneOverEdge = true; 
            TabbedDock.FireCrossProcessDragDropEvents = true;
        }
    }
}
