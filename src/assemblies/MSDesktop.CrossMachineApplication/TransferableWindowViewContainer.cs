﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MSDesktop.CrossMachineApplication
{
    internal class TransferableWindowViewContainer : WindowViewContainer
    {
        public TransferableWindowViewContainer(string factoryId) : base(factoryId)
        {
        }

        public TransferableWindowViewContainer(InitialWindowParameters parameters, string factoryID) : base(parameters, factoryID)
        {
        }
    }
}
