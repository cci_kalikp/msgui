﻿using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MSDesktop.CrossMachineApplication.Views;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

namespace MSDesktop.CrossMachineApplication
{
    internal class CrossMachineDragDropModule : IModule
    {
        public CrossMachineDragDropModule(IUnityContainer container, 
            IChromeManager chromeManager, 
            IApplicationOptions applicationOptions)
        {
            m_chromeManager = chromeManager as ChromeManagerBase;
            m_container = container;
            m_applicationOptions = applicationOptions;
        }

        public void Initialize()
        {
            m_container.RegisterType<IWindowTransferManager, WindowTransferManager>(
                new ContainerControlledLifetimeManager());
            m_windowTransferManager = new WindowTransferManager(m_container.Resolve<EventRegistrator>(), 
                m_chromeManager,
                m_container.Resolve<IChromeRegistry>());
            m_container.RegisterInstance(m_windowTransferManager);

            m_chromeManager.WindowGroupDroppedOnScreenEdge += 
                (sender, args) => m_windowTransferManager.TransferWindowGroup(args);

            m_windowTransferManager.Listen();

            m_container.RegisterType<ScreenEdgeMappingConfigScreenViewModel, ScreenEdgeMappingConfigScreenViewModel>(new ContainerControlledLifetimeManager());

            InitializeOptionView();
        }

        private void InitializeOptionView()
        {
            var configScreenViewModel = m_container.Resolve<ScreenEdgeMappingConfigScreenViewModel>();
            var configScreen = new ScreenEdgeMappingConfigScreen
                                   {
                                       DataContext = configScreenViewModel
                                   };
            var optionView = new ScreenEdgeMappingConfigOptionView(configScreen);
            m_applicationOptions.AddOptionPage("MSDesktop", "Cross Machine Application", optionView);
        }

        private readonly ChromeManagerBase m_chromeManager;
        private readonly IUnityContainer m_container;
        private readonly IApplicationOptions m_applicationOptions;
        private WindowTransferManager m_windowTransferManager;
    }
}
