﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MSDesktop.HTTPServer
{
    public abstract class HTTPServerSubscriberModule:IModule
    {
        protected readonly IUnityContainer unityContainer;
        protected readonly IChromeManager chromeManager;
        protected readonly IChromeRegistry chromeRegistry; 
        protected HTTPServerSubscriberModule(IUnityContainer unityContainer_)
        {
            unityContainer = unityContainer_;
            chromeManager = unityContainer.Resolve<IChromeManager>();
            chromeRegistry = unityContainer.Resolve<IChromeRegistry>();
        }
        public void Initialize()
        {
            unityContainer.RegisterInstance<HTTPServerSubscriberModule>(ModuleName, this);
            InitializeCore();
        }

        protected abstract void InitializeCore(); 
        
        protected abstract string ModuleName
        {
            get;
        }

        public virtual void ConsumeMessage<T>(T message)
        {
        }
    }
}
