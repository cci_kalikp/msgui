﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Threading;
using System.Xml.Linq; 
using MorganStanley.MSDotNet.MSGui.Core; 
using MorganStanley.MSDotNet.MSGui.Impl.HTTPServer;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.My;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using Microsoft.Practices.Unity.Utility; 
using System.Reflection; 

namespace MSDesktop.HTTPServer
{
    public class HTTPServerModule : IMSDesktopModule
    {
#pragma warning disable 612,618
        private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger<HTTPServerModule>();
        private readonly IViewManager viewManager;
        private readonly IControlParadigmImplementator cpi;
        private readonly IEventRegistrator eventRegistrator;
        private readonly IChromeManager chromeManager;
        private readonly ICommunicator communicator;
        private readonly IUnityContainer unityContainer;
        private readonly Dictionary<string, IWindowViewContainer> viewContainers = new Dictionary<string, IWindowViewContainer>();
        private readonly Dictionary<string, Pair<object, Type>> publishers = new Dictionary<string, Pair<object, Type>>();
        private readonly Dictionary<string, Pair<object, Type>> v2vPublishers = new Dictionary<string, Pair<object, Type>>();
        private readonly Dictionary<string, Pair<object, HttpListenerContext>> responseContexts = new Dictionary<string, Pair<object, HttpListenerContext>>();
        public HTTPServerModule(IUnityContainer unityContainer_)
#pragma warning restore 612,618
        { 
            unityContainer = unityContainer_;
            viewManager = unityContainer_.Resolve<IViewManager>();
            cpi = unityContainer_.Resolve<IControlParadigmImplementator>();
            eventRegistrator = unityContainer_.Resolve<IEventRegistrator>();
            chromeManager = unityContainer_.Resolve<IChromeManager>();
            communicator = unityContainer_.Resolve<ICommunicator>();
        } 

        public void Initialize()
        {
            HttpListener listener = new HttpListener();
            var thread = new Thread(delegate()
                {
                    try
                    {
                        listener.Prefixes.Add(HttpServerExtentions.RequestBuilder.BaseUrl);
                        listener.Start();
                    }
                    catch (Exception)
                    {
                        return;
                    }
                    for (; ; )
                    {
                        if (!listener.IsListening) return;
                        HttpListenerContext ctx = null;
                        try
                        {
                            ctx = listener.GetContext(); 
                        }
                        catch (Exception)
                        {
                        }
                        if (ctx == null)
                            break;

                        Uri uri = HttpServerExtentions.RequestBuilder.UnprotectUrl(ctx.Request);

                        if (uri == null)
                        {
                            InvalidUrl(ctx);
                            continue;
                        }
                        ActionType action = HttpRequestBuilder.ParseActionAPI(uri);
                        bool handled = false;
                        switch (action)
                        {  
                            case ActionType.RegisterPublisher:
                                handled = HandleRegisterPublisherRequest(ctx);
                                break;
                            case ActionType.RegisterSubscriber:
                                handled = HandleRegisterSubscriberRequest(ctx);
                                break;
                            case ActionType.PublishXml:
                            case ActionType.PublishJson:
                                handled = HandlePublishRequest(ctx, action == ActionType.PublishXml ? DataFormat.Xml : DataFormat.Json);
                                break;
                            case ActionType.CreateView:
                                handled = HandleCreateViewRequest(ctx);
                                break;
                            case ActionType.CreatePulisherView:
                                handled = HandleCreatePublisherViewRequest(ctx);
                                break;
                            case ActionType.CreateSubscriberView:
                                handled = HandleCreateSubscriberViewRequest(ctx);
                                break;
                            case ActionType.RegisterV2VPublisher:
                                handled = HandleRegisterV2VPublisherRequest(ctx);
                                break;
                            case ActionType.RegisterV2VSubscriber:
                                handled = HandleRegisterV2VSubscriberRequest(ctx);
                                break;
                            case ActionType.V2VPublishXml:
                            case ActionType.V2VPulishJson:
                                handled = HandleV2VPublishRequest(ctx, action == ActionType.V2VPublishXml ? DataFormat.Xml : DataFormat.Json);
                                break;
                        }  
                        if (!handled)
                        {
                            InvalidUrl(ctx);
                        }
                    }
                });
            thread.IsBackground = true;
            thread.Start();
            
            cpi.SetOnClosingAction(listener.Stop);
        }

        #region Request Handlers

        private bool HandleCreateViewRequest(HttpListenerContext context_)
        {
            return CreateViewCore(context_, null);
        }

        #region M2M Communication Handlers
        public bool HandleRegisterPublisherRequest(HttpListenerContext context_)
        {
            string publisherId = context_.Request.QueryString[HttpRequestBuilder.SessionIdParameter];
            if (string.IsNullOrWhiteSpace(publisherId)) return false;
            string type = context_.Request.QueryString[HttpRequestBuilder.MessageTypeParameter];
            if (string.IsNullOrWhiteSpace(type)) return false;

            CloseWebPage(context_);

            try
            {
                logger.DebugWithFormat("Register publisher for event {0} from a web call", type);
                Type messageType = Type.GetType(type);
                var getPublisherMethod = communicator.GetType().GetMethod("GetPublisher");
                getPublisherMethod = getPublisherMethod.MakeGenericMethod(new[] { messageType });
                object publisher = getPublisherMethod.Invoke(communicator, new object[] { });
                publishers.Add(publisherId, Pair.Make(publisher, messageType));
            }
            catch (Exception ex)
            {
                logger.Warning(string.Format("Failed register publisher for event {0} from a web call", type), ex);
            }
            return true;
        }

        public bool HandleRegisterSubscriberRequest(HttpListenerContext context_)
        {
            string type = context_.Request.QueryString[HttpRequestBuilder.MessageTypeParameter];
            if (string.IsNullOrWhiteSpace(type)) return false;
            string moduleName = context_.Request.QueryString[HttpRequestBuilder.ModuleNameParameter];
            if (string.IsNullOrWhiteSpace(moduleName)) return false;
            string callback = context_.Request.QueryString[HttpRequestBuilder.CallbackParameter]; //if empty, use the default method
            if (!unityContainer.IsRegistered<HTTPServerSubscriberModule>(moduleName))
            {
                logger.Warning(string.Format("Module {0} cannot be found", moduleName));
                InvalidUrl(context_);
            } 

            CloseWebPage(context_); 

            try
            {

                logger.DebugWithFormat("Register subscriber for event {0} from a web call", type);

                var messageType = Type.GetType(type);
                var handlerFactory = GetType()
                    .GetMethod("GetMessageHandler", BindingFlags.Instance | BindingFlags.NonPublic)
                    .MakeGenericMethod(messageType);
                var handler = handlerFactory.Invoke(this, new object[] { moduleName, callback });
                MessagingReflectionUtilities.GetSubscriber(communicator, messageType, handler); 
            }
            catch (Exception ex)
            {
                logger.Warning(string.Format("Failed register subscriber for event {0} from a web call", type), ex);
            }
            return true;
        }

        public bool HandlePublishRequest(HttpListenerContext context_, DataFormat format_)
        {
            string publisherId = context_.Request.QueryString[HttpRequestBuilder.SessionIdParameter];
            if (string.IsNullOrWhiteSpace(publisherId)) return false;
            string messageContent = context_.Request.QueryString[HttpRequestBuilder.MessageParameter];
            if (string.IsNullOrWhiteSpace(messageContent)) return false;
            Pair<object, Type> pair;
            if (!publishers.TryGetValue(publisherId, out pair)) return false;


            var messageType = pair.Second;
            var publisher = pair.First;
            object message = null;
            try
            {
                string zipped = context_.Request.QueryString[HttpRequestBuilder.ZippedParameter];
                if (!string.IsNullOrEmpty(zipped) && zipped == "1")
                {
                    messageContent = HttpRequestBuilder.UnZip(messageContent);
                }
                message = HttpRequestBuilder.DeserializeFromString(messageContent, messageType, format_);

            }
            catch (Exception ex)
            {
                logger.Warning(string.Format("Failed to deserialize event args {0} from a web call", messageType.FullName), ex);      
                InvalidUrl(context_);
            }

            try
            {

                logger.DebugWithFormat("Publishing event {0} from a web call", messageType.FullName);
                 
                var responsiveMessage = message as IMessageWithHttpResponse;
                if (responsiveMessage != null && typeof(IMessageResponse).IsAssignableFrom(responsiveMessage.ResponseType))
                {
                     HookMessageResponse(responsiveMessage, publisherId, context_);
                }
                else
                {
                    CloseWebPage(context_);
                }
                MessagingReflectionUtilities.Publish(publisher, message); 
            }
            catch (Exception ex)
            {
                logger.Warning(string.Format("Failed to publish event {0} from a web call", messageType.FullName), ex);
            }
            return true;
        }

        #endregion

        #region V2V Communication Handler
        private bool HandleCreatePublisherViewRequest(HttpListenerContext context_)
        { 
            string id = context_.Request.QueryString[HttpRequestBuilder.SessionIdParameter];
            if (string.IsNullOrWhiteSpace(id)) return false;
            string messageType = context_.Request.QueryString[HttpRequestBuilder.MessageTypeParameter];
            if (string.IsNullOrWhiteSpace(messageType)) return false;
            return CreateViewCore(context_,
                v =>
                {
                    try
                    { 
                        logger.DebugWithFormat("Register view to view publisher for event {0} from a web call", messageType);
                        var type = Type.GetType(messageType);
                        var publisher = typeof(IEventRegistrator).GetMethod("RegisterPublisher").MakeGenericMethod(type).Invoke(eventRegistrator, new[] { v });
                        v2vPublishers.Add(id, Pair.Make(publisher, type));
                    }
                    catch (Exception ex)
                    {
                        logger.Warning(string.Format("Failed register view to view publisher for event {0} from a web call", messageType), ex);
                    }

                }); 

        }

        private bool HandleRegisterV2VPublisherRequest(HttpListenerContext context_)
        {
            string viewID = context_.Request.QueryString[HttpRequestBuilder.ViewIdParameter];
            if (string.IsNullOrWhiteSpace(viewID)) return false;
            string publisherId = context_.Request.QueryString[HttpRequestBuilder.SessionIdParameter];
            if (string.IsNullOrWhiteSpace(publisherId)) return false;
            string messageType = context_.Request.QueryString[HttpRequestBuilder.MessageTypeParameter];
            if (string.IsNullOrWhiteSpace(messageType)) return false;

            CloseWebPage(context_);

            try
            {
                logger.DebugWithFormat("Register view to view publisher for event {0} from a web call", messageType);
                IWindowViewContainer viewContainer;
                if (!viewContainers.TryGetValue(viewID, out viewContainer))
                {
                    viewContainer = chromeManager.GetViews().FirstOrDefault(w_ => w_.ID == viewID);
                } 
                if (viewContainer == null) throw new ArgumentOutOfRangeException("Invalid viewid");

                var type = Type.GetType(messageType);
                var publisher = typeof(IEventRegistrator).GetMethod("RegisterPublisher").MakeGenericMethod(type).Invoke(eventRegistrator, new[] { viewContainer });
                v2vPublishers.Add(publisherId, Pair.Make(publisher, type));
            }
            catch (Exception ex)
            {
                logger.Warning(string.Format("Failed register view to view publisher for event {0} from a web call", messageType), ex);
            }
            return true;
        }

        private bool HandleCreateSubscriberViewRequest(HttpListenerContext context_)
        {
            string id = context_.Request.QueryString[HttpRequestBuilder.SessionIdParameter];
            if (string.IsNullOrWhiteSpace(id)) return false;
            string messageType = context_.Request.QueryString[HttpRequestBuilder.MessageTypeParameter];
            if (string.IsNullOrWhiteSpace(messageType)) return false;
            string callback = context_.Request.QueryString[HttpRequestBuilder.CallbackParameter];
            if (string.IsNullOrWhiteSpace(callback)) return false;

            return CreateViewCore(context_,
            v =>
            {
                try
                {
                    logger.DebugWithFormat("Register view to view subscriber for event {0} from a web call", messageType);

                    var type = Type.GetType(messageType); 
                    var handler = GetType()
                        .GetMethod("GetV2VMessageHandler", BindingFlags.Instance | BindingFlags.NonPublic)
                        .MakeGenericMethod(type)
                        .Invoke(this, new object[] { v.ID, callback }); 

                    SubscriberHelper.GetSubscriber(eventRegistrator, type, v, handler);  

                }
                catch (Exception ex)
                {
                    logger.Warning(string.Format("Failed register view to view subscriber for event {0} from a web call", messageType), ex);
                }

            }); 
           
        }

        private bool HandleRegisterV2VSubscriberRequest(HttpListenerContext context_)
        {
            string viewID = context_.Request.QueryString[HttpRequestBuilder.ViewIdParameter];
            if (string.IsNullOrWhiteSpace(viewID)) return false;
            string subscriberId = context_.Request.QueryString[HttpRequestBuilder.SessionIdParameter];
            if (string.IsNullOrWhiteSpace(subscriberId)) return false;
            string messageType = context_.Request.QueryString[HttpRequestBuilder.MessageTypeParameter];
            if (string.IsNullOrWhiteSpace(messageType)) return false;
            string callbackMethod = context_.Request.QueryString[HttpRequestBuilder.CallbackParameter];
            if (string.IsNullOrWhiteSpace(callbackMethod)) return false;

            CloseWebPage(context_);

            try
            {
                logger.DebugWithFormat("Register view to view subscriber for event {0} from a web call", messageType);

                IWindowViewContainer viewContainer;
                if (!viewContainers.TryGetValue(viewID, out viewContainer))
                {
                    viewContainer = chromeManager.GetViews().FirstOrDefault(w_ => w_.ID == viewID);
                } 

                if (viewContainer == null) throw new ArgumentOutOfRangeException("Invalid viewid");

                var type = Type.GetType(messageType); 

                var handler = GetType()
                    .GetMethod("GetV2VMessageHandler", BindingFlags.Instance | BindingFlags.NonPublic)
                    .MakeGenericMethod(type)
                    .Invoke(this, new object[] { viewID, callbackMethod });
                 
                SubscriberHelper.GetSubscriber(eventRegistrator, type, viewContainer, handler); 

                 
            }
            catch (Exception ex)
            {
                logger.Warning(string.Format("Failed register view to view subscriber for event {0} from a web call", messageType), ex);
            }
            return true;
        }

        public bool HandleV2VPublishRequest(HttpListenerContext context_, DataFormat format_)
        {
            string publisherId = context_.Request.QueryString[HttpRequestBuilder.SessionIdParameter];
            string messageContent = context_.Request.QueryString[HttpRequestBuilder.MessageParameter];
            Pair<object, Type> pair;
            if (!v2vPublishers.TryGetValue(publisherId, out pair)) return false;

            CloseWebPage(context_);

            var messageType = pair.Second;
            var publisher = pair.First;
            object message = null;
            try
            {
                string zipped = context_.Request.QueryString[HttpRequestBuilder.ZippedParameter];
                if (!string.IsNullOrEmpty(zipped) && zipped == "1")
                {
                    messageContent = HttpRequestBuilder.UnZip(messageContent);
                }
                message = HttpRequestBuilder.DeserializeFromString(messageContent, messageType, format_);

            }
            catch (Exception ex)
            {
                logger.Warning(string.Format("Failed to deserialize event args {0} from a web call", messageType.FullName), ex);
                InvalidUrl(context_);
            }

            try
            {
                logger.DebugWithFormat("View to view Publishing event {0} from a web call", messageType.FullName);
                var responsiveMessage = message as IMessageWithHttpResponse;
                if (responsiveMessage != null)
                {
                    HookMessageResponse(responsiveMessage, publisherId, context_);                    
                }
                else
                {
                    CloseWebPage(context_);
                } 
                typeof(IPublisher<>).MakeGenericType(messageType).GetMethod("Publish").Invoke(publisher,
    new[] { message, CommunicationTargetFilter.All, new string[] { } });
            }
            catch (Exception ex)
            {
                logger.Warning(string.Format("Failed to view to view publish event {0} from a web call", messageType.FullName), ex);
            }
            return true;
        }
        #endregion

        #endregion

        #region Helper Methods
        private void InvalidUrl(HttpListenerContext context_)
        {
            logger.Warning(string.Format("Invalid url '{0}' ", context_.Request.Url));
            WriteToClient(context_, "Invalid URL");
            
        }

        private bool CreateViewCore(HttpListenerContext context_, Action<IWindowViewContainer> viewHandler_)
        {
            String state;
            if (context_.Request.HttpMethod.ToUpper() == "POST")
            {
                var body = context_.Request.InputStream;
                System.Text.Encoding encoding = context_.Request.ContentEncoding;
                using (System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding))
                {
                    state = reader.ReadToEnd(); 
                }
            }
            else
            {
                state = context_.Request.QueryString[HttpRequestBuilder.StateParameter];
            } 
            var factory = context_.Request.QueryString[HttpRequestBuilder.FactoryParameter];
            if (string.IsNullOrWhiteSpace(factory)) return false;
             
            CloseWebPage(context_);

            if ((System.Windows.Application.Current != null) &&
                (System.Windows.Application.Current.Dispatcher != null))
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)(delegate
                {
                    try
                    {
                        XDocument stateXml = null;
                        if (string.IsNullOrWhiteSpace(state))
                        {
                            logger.DebugWithFormat("Creating factory {0} from a web call", factory);
                        }
                        else
                        {
                            logger.DebugWithFormat("Creating factory {0} with state {1} from a web call", factory, state);
                            stateXml = XDocument.Parse(state);
                        }
                        //instead of using chromeManager.CreateWindow, this method would be able to transfer a state xml
                        IWindowViewContainer view = viewManager.CreateView(factory, stateXml) as IWindowViewContainer; 
                        viewContainers[view.ID] = view;
                        if (viewHandler_ != null)
                        {
                            viewHandler_(view);
                        }

                    }
                    catch (Exception ex)
                    {
                        if (string.IsNullOrWhiteSpace(state))
                        {
                            logger.Warning(
                                 string.Format("Failed using factory {0} from a web call", factory),
                                 ex);
                        }
                        else
                        {
                            logger.Warning(
                                string.Format("Failed using factory {0} with state {1} from a web call", factory, state),
                                ex);
                        }
                    }
                }),
                DispatcherPriority.Send);
            }
            return true;
        }

        private static void WriteToClient(HttpListenerContext context_, string content_)
        {
            try
            {
                if (!string.IsNullOrEmpty(content_))
                {
                    byte[] b = Encoding.UTF8.GetBytes(content_);
                    context_.Response.OutputStream.Write(b, 0, b.Length);
                    context_.Response.OutputStream.Flush();            
                }
                context_.Response.OutputStream.Close();      

            }
            catch  //the OutputStream might have been closed already
            {
            }
        }
        private static void CloseWebPage(HttpListenerContext context_)
        {
            WriteToClient(context_, string.Empty);
           // WriteToClient(context_, "<script>window.close();</script>");
        }
         
        private void HookMessageResponse(IMessageWithHttpResponse message_, string publisherId_, HttpListenerContext context_)
        {
            string sequenceId = message_.NewSequence(publisherId_); 

            var handlerFactory = GetType()
                .GetMethod("GetMessageResponseHandler", BindingFlags.Instance | BindingFlags.NonPublic)
                .MakeGenericMethod(message_.ResponseType);
            var handler = handlerFactory.Invoke(this, new object[] {});
  
            object subscription = MessagingReflectionUtilities.GetSubscriber(communicator, message_.ResponseType, handler);
            responseContexts[sequenceId] = Pair.Make(subscription, context_);

        }
        private Action<T> GetV2VMessageHandler<T>(string viewId_,  string callbackMethod_)
        { 
            return m_ =>
            {
                try
                {
                    var viewContainer = chromeManager.GetViews().First(w_ => w_.ID == viewId_);
                    MethodInfo callbackMethod = viewContainer.Content.GetType().GetMethod(callbackMethod_, BindingFlags.Public | BindingFlags.Instance);
                    (viewContainer.Content as DispatcherObject).Dispatcher.Invoke(
                                   new Action(() => callbackMethod.Invoke(viewContainer.Content, new object[] { m_ })));
                }
                catch (Exception ex)
                {
                    logger.Warning(string.Format("Failed consume view to view event {0} from a web call", m_.GetType().FullName), ex);
                }
            };
        }

        private Action<T> GetMessageHandler<T>(string moduleName_, string callbackMethod_)
        {
            return
                m_ =>
                {
                    try
                    {
                        HTTPServerSubscriberModule module = unityContainer.Resolve<HTTPServerSubscriberModule>(moduleName_);
                        if (string.IsNullOrEmpty(callbackMethod_))
                        {
                            module.ConsumeMessage(m_);
                        }
                        else
                        {
                            MethodInfo callbackMethod = module.GetType().GetMethod(callbackMethod_, BindingFlags.Public | BindingFlags.Instance);
                            callbackMethod.Invoke(module, new object[] { m_ });

                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Warning(string.Format("Failed consume event {0} from a web call", m_.GetType().FullName), ex);
                    }
                };
        }

        private Action<T> GetMessageResponseHandler<T>() where T:IMessageResponse
        {
            return response_ =>
                {
                    Pair<object, HttpListenerContext> context;
                    lock (responseContexts)
                    {
                        if (responseContexts.TryGetValue(response_.Sequence, out context))
                        { 
                            responseContexts.Remove(response_.Sequence);
                        }
                    }
                    if (context != null)
                    {
                        WriteToClient(context.Second, response_.Content);
                        IDisposable subscriber = context.First as IDisposable;
                        if (subscriber != null)
                        {
                            subscriber.Dispose();
                        }
                    }
                   
                };

        }
        #endregion

    }
}
