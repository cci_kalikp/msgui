﻿using System;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.HTTPServer;

namespace MSDesktop.HTTPServer
{
    public static class HttpServerExtentions
    {
         
        internal static HttpRequestBuilder RequestBuilder { get; private set; }
        public static void EnableHTTPServer(this Framework framework_, int port_ = 8080, bool enableHash_= true, string hashSeed_=null)
        {
            ExtensionsUtils.RequireAssembly(ExternalAssembly.NewtonSoftJson, ExternalAssembly.ReactiveInterfaces, ExternalAssembly.ReactiveCore, ExternalAssembly.ReactiveLinq);
            RequestBuilder = new HttpRequestBuilder(HttpRequestBuilder.Localhost, port_, enableHash_, 
                string.IsNullOrEmpty(hashSeed_) ?HttpRequestBuilder.GenerateHashSeed() : hashSeed_);
           
            framework_.AddModule(typeof(HTTPServerModule));

        }
         
    }
}
