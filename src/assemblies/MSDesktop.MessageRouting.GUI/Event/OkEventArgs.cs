﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting.GUI.Event
{
    public class OkEventArgs : EventArgs
    {
        public bool IsError { get; private set; }
        public string ErrorMessage { get; private set; }

        public OkEventArgs(bool isError, string errorMessage)
        {
            this.IsError = isError;
            this.ErrorMessage = errorMessage;
        }

    }
}
