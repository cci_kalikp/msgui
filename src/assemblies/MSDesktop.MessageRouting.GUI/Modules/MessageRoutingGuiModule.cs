﻿using System;
using System.Windows;
using System.Windows.Threading;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl;
using MSDesktop.MessageRouting.GUI.Message;
using MSDesktop.MessageRouting.GUI.Util;
using MSDesktop.MessageRouting.GUI.View;
using MSDesktop.MessageRouting.Routing;
using MSDesktop.MessageRouting.ZeroConfig;

namespace MSDesktop.MessageRouting.GUI.Modules
{
    public class MessageRoutingGuiModule : IMSDesktopModule
    {
        internal static uint? AutoSelectRouteWithin = null;
        internal static bool HideOptionsPage = false;

        private readonly IRouteManager _routeManager;
        private readonly IEndpointSelector _endpointSelector;
        private readonly IRoutingCommunicator _routingCommunicator;
        private readonly IApplicationOptions _applicationOptions;
        private readonly IRouteExplorer _scout;
        private readonly IScreenCatpureSink _screenCaptureSink;

        public MessageRoutingGuiModule(IChromeRegistry chromeRegistry, IChromeManager chromeManager,
                                       IRouteManager routeManager, IEndpointSelector endpointSelector,
                                       IRoutingCommunicator routingCommunicator, IApplicationOptions applicationOptions,
                                       IRouteExplorer scout, IApplication application)
        {
            _routeManager = routeManager;
            _endpointSelector = endpointSelector;
            _applicationOptions = applicationOptions;
            _routingCommunicator = routingCommunicator;
            _scout = scout;
            _screenCaptureSink = new ScreenCaptureSinkImpl(routingCommunicator);
            application.ApplicationLoaded += new EventHandler(application_ApplicationLoaded);
            
            
        }

        private void application_ApplicationLoaded(object sender, EventArgs e)
        {
            if (_routeManager.EnableAutoSelect == null)
            {
                _routeManager.EnableAutoSelect = AutoSelectRouteWithin != null && AutoSelectRouteWithin.Value > 0;
                if (_routeManager.EnableAutoSelect.Value)
                {
                    _routeManager.AutoSelectTimeout = AutoSelectRouteWithin.Value;
                }
            }
        }

        public void Initialize()
        { 
            _endpointSelector.AddEndpointSelectionStep(1000, EndpointSelectionStep);

            if (!HideOptionsPage)
            {
                _applicationOptions.AddOptionPage("Routed Communication", "Configuration",
                    new ConfigOptions(_routingCommunicator, _scout, _routeManager));
            }
            _screenCaptureSink.SubscribeScreenCaptureRequest();            

        }

        private EndpointSelection EndpointSelectionStep(EndpointSelectorChain chain)
        {
            if (Application.Current == null) return chain.Next();
           return  Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal,
                                                         (Func<EndpointSelectorChain, EndpointSelection>)
                                                         GetEndpointSelection, chain) as EndpointSelection;
        }

        private EndpointSelection GetEndpointSelection(EndpointSelectorChain chainInternal)
                                                      {
            var routeFinder = new RouteFinder(
                _routeManager,
                _routingCommunicator,
                _scout,
                _screenCaptureSink,
                chainInternal.Message.GetType(),
                chainInternal.Message);

                                                          if (Application.Current.MainWindow != routeFinder)
                                                          {
                                                              routeFinder.Owner = Application.Current.MainWindow;
                                                          }

                                                          if (routeFinder.ShowDialog().GetValueOrDefault())
                                                          {
                        var ep = new Endpoint
                                                              {
                                                                      Application = routeFinder.RouteViewModel.SelectedRoute.Application,
                                                                      Hostname = routeFinder.RouteViewModel.SelectedRoute.Hostname,
                                                                      InstanceName = routeFinder.RouteViewModel.SelectedRoute.InstanceName
                        };

                        if (_scout.IsServerDiscoveryEnabled)
                        {
                            ep.CpsServer = routeFinder.RouteViewModel.SelectedRoute.ServerHostName;
                            ep.Kerberosed = routeFinder.RouteViewModel.SelectedRoute.ServerIsKerberized;
                        }

                        return new EndpointSelection
                        {
                            Endpoint = ep,
                                                                  Persist = routeFinder.RouteViewModel.SaveRoute
                                                              };
                                                          }

            return chainInternal.Next();
        }
    }
}
