﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MSDesktop.MessageRouting.GUI.Model;
using MSDesktop.MessageRouting.GUI.ViewModel;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MSDesktop.MessageRouting.Routing;
using MSDesktop.MessageRouting.ZeroConfig;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Options;

namespace MSDesktop.MessageRouting.GUI.View
{
    public partial class ConfigOptions : UserControl, MorganStanley.MSDotNet.MSGui.Core.IDisposableOptionView, ISubItemAwareSeachablePage
    {
        public ConfigOptionsViewModel ConfigOptionsViewModel { get; private set; }

        private readonly IRouteManager routeManager;
        private readonly IRoutingCommunicator routingCommunicator;
        private readonly IRouteExplorer scout;

        public ConfigOptions(IRoutingCommunicator routingCommunicator, IRouteExplorer scout, IRouteManager routeManager)
        {
            InitializeComponent();
            this.routeManager = routeManager;
            this.routingCommunicator = routingCommunicator;
            this.scout = scout;
        }

        public new UIElement Content
        {
            get 
            { 
                return this; 
            }
        }

        public void OnCancel()
        {
            // TODO
        }

        public void OnDisplay()
        {
            this.ConfigOptionsViewModel = new ConfigOptionsViewModel(routingCommunicator, scout, routeManager);
            this.DataContext = ConfigOptionsViewModel;
        }

        public void OnHelp()
        {
            TaskDialog.ShowMessage("About Kraken", "Releaze ze Kraken!", "For more information please refer to http://kraken", "", "", "", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Information, VistaTaskDialogIcon.Information);
        } 

        public void OnOK()
        {
            ConfigOptionsViewModel.Save();
        }

        public void OnLeave()
        {
            ConfigOptionsViewModel.Dispose();
        }
         
        private void Datagrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ConfigOptionsViewModel.RouteSelectionChanged(Datagrid);
        }

        private void Datagrid_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                ConfigOptionsViewModel.DeleteHosts(null);
            }
        }

        public IList<KeyValuePair<string, object>> GetMatchedItems(string textToSearch_)
        {
            if (ConfigOptionsViewModel == null) this.ConfigOptionsViewModel = new ConfigOptionsViewModel(routingCommunicator, scout, routeManager);
            var matchedRoutes = new List<KeyValuePair<string, object>>();
            foreach (var route in ConfigOptionsViewModel.DefaultRoutes)
            {

                if (route.MessageTypeName.ToLower().Contains(textToSearch_.ToLower()) ||
                    route.TargetHostname.ToLower().Contains(textToSearch_.ToLower()))
                {
                    matchedRoutes.Add(new KeyValuePair<string, object>("Default routes → " + route, route));
                }
            }
            foreach (var route in ConfigOptionsViewModel.Routes)
            {
                if (route.MessageTypeName.ToLower().Contains(textToSearch_.ToLower()) ||
    route.TargetHostname.ToLower().Contains(textToSearch_.ToLower()))
                {
                    matchedRoutes.Add(new KeyValuePair<string, object>("Stored routes → " + route, route));
                }
            }

            return matchedRoutes;
        }

        public void LocateItem(object item_)
        {
            var route = item_ as ConfigRouteModel;
            if (route.DefaultRoute)
            {
                var item =
                    ConfigOptionsViewModel.DefaultRoutes.FirstOrDefault(
                        r_ => r_.MessageTypeName == route.MessageTypeName && r_.CreatedAt.Equals(route.CreatedAt));
                if (item == null) return; 
                DefaultDatagrid.SelectedItem = item;
                var row = DefaultDatagrid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                if (row != null)
                {
                    row.Highlight();
                }
            }
            else
            {
                var item =
                    ConfigOptionsViewModel.Routes.FirstOrDefault(
                        r_ => r_.MessageTypeName == route.MessageTypeName && r_.CreatedAt.Equals(route.CreatedAt));
                if (item == null) return; 
                Datagrid.SelectedItem = item;
                var row = Datagrid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                if (row != null)
                {
                    row.Highlight();
                }
            }
        }


    }
}
