﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input; 
using MSDesktop.MessageRouting.Routing;
using MSDesktop.MessageRouting.ZeroConfig;
using MSDesktop.MessageRouting.GUI.Event;
using MSDesktop.MessageRouting.GUI.ViewModel;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MSDesktop.MessageRouting.GUI.View
{
    public partial class RouteFinder : Window
    {
        public RouteViewModel RouteViewModel { get; private set; } 

        public RouteFinder(IRouteManager routeManager, IRoutingCommunicator routingCommunicator, IRouteExplorer scout, IScreenCatpureSink screenCatpureSink, Type messageType, object message):
            this(new RouteViewModel(routeManager, routingCommunicator, scout, screenCatpureSink, messageType, message))
        {
             
        }

        public RouteFinder(RouteViewModel model_)
        {
            InitializeComponent();
            this.RouteViewModel = model_;
            model_.Initialize();

            this.Closed += OnClosed;
            this.RouteViewModel.OkEvent += OnOk;
            this.RouteViewModel.CancelEvent += OnCancel;
            this.LocationChanged += OnLocationChanged;
            this.SizeChanged += OnSizeChanged;
            this.RouteViewModel.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(RouteViewModel_PropertyChanged);
            this.DataContext = RouteViewModel;
        }

        void RouteViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "AutoSelectTimeoutTextVisiblity")
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    this.MinHeight = 170 + (RouteViewModel.AutoSelectTimeoutTextVisiblity == Visibility.Visible ? 30 : 0);
                    this.MaxHeight = 300 + (RouteViewModel.AutoSelectTimeoutTextVisiblity == Visibility.Visible ? 30 : 0);
                }));
            }
        }
        protected override void OnGotFocus(RoutedEventArgs e)
        {
            RouteViewModel.ResumeTimer();
            base.OnGotFocus(e);
        }

        protected override void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            RouteViewModel.ResumeTimer();
            base.OnGotKeyboardFocus(e);
        }
        protected override void OnLostFocus(RoutedEventArgs e)
        { 
            RouteViewModel.SuspendTimer();
            base.OnLostFocus(e);
        }

        protected override void OnLostKeyboardFocus(KeyboardFocusChangedEventArgs e)
        { 
            RouteViewModel.SuspendTimer();
            base.OnLostKeyboardFocus(e);
        }

        private void OnOk(object sender, OkEventArgs e)
        {
            Dispatcher.Invoke((Action)(() =>
            {
                if (e.IsError)
                {
                    TaskDialog.ShowMessage(e.ErrorMessage, this.RouteViewModel.MessageInfo, TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
                    return;
                }

                this.DialogResult = true;
                this.Close(); 
            })); 

        }

        private void OnCancel(object sender, EventArgs e)
        {
            Dispatcher.Invoke((Action)(() =>
                {
                    this.DialogResult = false;
                    this.Close();
                })); 
        }

 
        private void OnClosed(object sender, EventArgs e)
        {
            RouteViewModel.ClearTimer();
            this.Closed -= OnClosed;
            this.RouteViewModel.OkEvent -= OnOk;
            this.RouteViewModel.CancelEvent -= OnCancel;
            this.LocationChanged -= OnLocationChanged;
            this.SizeChanged -= OnSizeChanged;
        }

        private void OnLocationChanged(object sender, EventArgs e)
        {
            RealignPreviewPopup();
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            RealignPreviewPopup();
        }

        private void RealignPreviewPopup()
        {
            var offset = ScreenCapturePopup.HorizontalOffset;
            ScreenCapturePopup.HorizontalOffset = offset + 1;
            ScreenCapturePopup.HorizontalOffset = offset;
        }

        private void Routes_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Routes.SelectedItem != null)
                OkButton.Command.Execute(null);
        }

        private void RoutesSelectionChanged(object sender, MouseButtonEventArgs e)
        {
            ScreenCapturePopup.IsOpen = true;
        }

        private void RefreshAll_Click(object sender, RoutedEventArgs e)
        {
            ScreenCapturePopup.IsOpen = false;
        }
    }
}
