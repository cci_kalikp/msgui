﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using MSDesktop.MessageRouting.Routing;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Controls;
using MSDesktop.MessageRouting.GUI.Model;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MSDesktop.MessageRouting.ZeroConfig;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel; 

namespace MSDesktop.MessageRouting.GUI.ViewModel
{
    public class ConfigOptionsViewModel : ViewModelBase, IDisposable
    {
        private readonly IRouteManager routeManager;

        public ObservableCollection<ConfigRouteModel> DefaultRoutes { get; private set; }

        public ObservableCollection<ConfigRouteModel> Routes { get; private set; }

        private uint autoSelectTimeout;
        public uint AutoSelectTimeout
        {
            get { return autoSelectTimeout; }
            set
            {
                autoSelectTimeout = value;
                OnPropertyChanged("AutoSelectTimeout");
            }
        }


        private bool enableAutoSelect;

        public bool EnableAutoSelect
        {
            get { return enableAutoSelect; }
            set
            {
                enableAutoSelect = value;
                OnPropertyChanged("EnableAutoSelect");
            }
        }

        public ObservableCollection<string> Hosts { get; private set; }
        public ObservableCollection<string> Apps { get; private set; }

        public ObservableCollection<Endpoint> HostEndpoints { get; private set; }

        public string SelectedHost { get; set; }

        private List<ConfigRouteModel> selectedRoutes;

        public List<ConfigRouteModel> SelectedRoutes
        {
            get { return selectedRoutes; }
            set
            {
                selectedRoutes = value;
                OnPropertyChanged("SelectedRoutes");
            }
        }
         
        private bool useDefaultRoutes;

        public bool UseDefaultRoutes
        {
            get { return useDefaultRoutes; }
            set
            {
                useDefaultRoutes = value;
                OnPropertyChanged("UseDefaultRoutes");
            }
        }

        #region Commands

        public ICommand ChangeHostsCommand { get; private set; }

        public ICommand DeleteHostsCommand { get; private set; }

        public ICommand DeleteAllHostsCommand { get; private set; }

        #endregion

        public ConfigOptionsViewModel(IRoutingCommunicator routingCommunicator, IRouteExplorer scout, IRouteManager routeManager)
        {
            this.routeManager = routeManager;
            AutoSelectTimeout = routeManager.AutoSelectTimeout;
            EnableAutoSelect = routeManager.EnableAutoSelect ?? false; 
            this.ChangeHostsCommand = new DelegateCommand(ChangeHosts);
            this.DeleteHostsCommand = new DelegateCommand(DeleteHosts);
            this.DeleteAllHostsCommand = new DelegateCommand(DeleteAllHosts);

            UseDefaultRoutes = this.routeManager.UseDefaultRouting;
            //AllowRouteByDefault = this.routeManager.AllowRouteByDefault;
            this.DefaultRoutes = new ObservableCollection<ConfigRouteModel>(this.routeManager.DefaultRoutes.Select(route =>
                new ConfigRouteModel(route)).OrderByDescending(a => a.CreatedAt));

            this.Routes = new ObservableCollection<ConfigRouteModel>(this.routeManager.Routes.Select(route =>
                new ConfigRouteModel(route)).OrderByDescending(a=>a.CreatedAt));

            this.Hosts = new ObservableCollection<string>();
            this.Apps = new ObservableCollection<string>();
            this.HostEndpoints = scout.GetAllSubscribedApplications();
            this.HostEndpoints.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(HostEndpoints_CollectionChanged);
        }

        void HostEndpoints_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Action dispatched = () =>
              {
                  this.Hosts.Clear();
                  List<Endpoint> endpoints = new List<Endpoint>(HostEndpoints); 
                  var newItems = endpoints.Select(a => a.Hostname).Distinct();
                  foreach (var item in newItems)
                  {
                      this.Hosts.Add(item);
                  }

                  this.Apps.Clear();

                  var applications = new Dictionary<string, List<string>>();
                  foreach (var hostEndpoint in endpoints)
                  {
                      List<string> instances;
                      if (!applications.TryGetValue(hostEndpoint.Application, out instances))
                      {
                          instances = new List<string>();
                          applications[hostEndpoint.Application] = instances;
                      }
                      if (!string.IsNullOrEmpty(hostEndpoint.InstanceName))
                      {
                          instances.Add(hostEndpoint.InstanceName);
                      }
                  }
                  foreach (var application in applications)
                  {
                      if (application.Value.Count <= 1)
                      {
                          this.Apps.Add(application.Key);
                      }
                      else
                      {
                          foreach (var instanceName in application.Value)
                          {
                              this.Apps.Add(string.IsNullOrEmpty(instanceName) ? application.Key :
                application.Key + " (" + instanceName + ")");
                          }
                      }
                  } 
              };
            if (System.Windows.Application.Current == null) return;
            System.Windows.Application.Current.Dispatcher.Invoke(dispatched);
        }

        public void RouteSelectionChanged(DataGrid dgv)
        {
            if (dgv.SelectedItems != null)
            {
                SelectedRoutes = ((IList<object>)dgv.SelectedItems).Select(obj => obj as ConfigRouteModel).Where(a => a != null).ToList();
            }
            else
            {
                SelectedRoutes = new List<ConfigRouteModel>();
            }
        }

        private void ChangeHosts(object parameter)
        {
            if (!string.IsNullOrEmpty(SelectedHost))
            {
                foreach (var item in SelectedRoutes)
                    item.TargetHostname = SelectedHost;
            }
            else
            {
                TaskDialog.ShowMessage("Please select a target host", "Target host is required", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Warning);
            }
        }

        public void DeleteHosts(object parameter)
        {

            if (TaskDialog.ShowMessage(
                "Are you sure you want to remove the selected routes?",
                "Remove selected routes",
                TaskDialogCommonButtons.YesNo, VistaTaskDialogIcon.Warning) == TaskDialogSimpleResult.Yes)
            {
                foreach (var item in this.SelectedRoutes)
                    this.Routes.Remove(item);
            }
        }

        private void DeleteAllHosts(object parameter)
        {

            if (TaskDialog.ShowMessage(
                "Are you sure you want to remove ALL routes?",
                "Remove ALL routes",
                TaskDialogCommonButtons.YesNo, VistaTaskDialogIcon.Warning) == TaskDialogSimpleResult.Yes)
            {
                this.Routes.Clear();
            }
        }

        public void Save()
        {
            this.routeManager.UseDefaultRouting = this.UseDefaultRoutes;

            this.routeManager.ReplaceAllRoutes(Routes.Select(route =>
                new Route()
                {
                    MessageTypeIdentifier = route.MessageTypeName,
                    Origin = new Endpoint()
                    {
                        Hostname = route.OriginHostname,
                        Application = route.OriginApplication, 
                    },
                    Target = new Endpoint()
                    {
                        Hostname = route.TargetHostname,
                        Application = route.TargetApplication,
                        InstanceName = route.TargetInstanceName
                    },
                    CreatedAt = route.IsDirty ? DateTime.Now : route.CreatedAt
                }));
            this.routeManager.EnableAutoSelect = EnableAutoSelect;
            this.routeManager.AutoSelectTimeout = this.AutoSelectTimeout;
        }
         
        public void Dispose()
        {
            this.HostEndpoints.CollectionChanged -= new System.Collections.Specialized.NotifyCollectionChangedEventHandler(HostEndpoints_CollectionChanged);
        }
    }
}
