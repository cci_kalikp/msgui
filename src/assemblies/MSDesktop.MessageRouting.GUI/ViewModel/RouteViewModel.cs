﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Timers;
using System.Windows.Media.Imaging;
using System.Windows.Input;
using MSDesktop.MessageRouting.ZeroConfig;
using System.Collections.Specialized; 
using MSDesktop.MessageRouting.Routing;
using MSDesktop.MessageRouting.Message;
using System.Windows;
using System.Windows.Threading;
using MSDesktop.MessageRouting.GUI.Event;
using MSDesktop.MessageRouting.GUI.Model;
using MSDesktop.MessageRouting.GUI.Util;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel; 

namespace MSDesktop.MessageRouting.GUI.ViewModel
{

    public class RouteViewModel : ViewModelBase
    {
        private static readonly ScreenCaptureModel defaultPreview = new ScreenCaptureModel();

        private readonly IRoutingCommunicator routingCommunicator;

        private readonly IRouteExplorer scout;

        private readonly IScreenCatpureSink screenCatpureSink;

        private readonly IRouteManager routeManager;
          
        private readonly Type messageType;
          
        private ObservableCollection<Endpoint> subscribedApplications;

        public ObservableCollection<RouteModel> Routes { get; private set; }

        private bool isRoutesLoading;

        public bool IsRoutesLoading
        {
            get { return isRoutesLoading; }
            set
            {
                isRoutesLoading = value;
                OnPropertyChanged("IsRoutesLoading");
            }
        }

        private RouteModel selectedRoute;

        public RouteModel SelectedRoute
        {
            get { return selectedRoute; }
            set
            {
                if (selectedRoute != null)
                    selectedRoute.PropertyChanged -= SelectedRoutePropertyChanged;

                selectedRoute = value;

                if (selectedRoute != null)
                    selectedRoute.PropertyChanged += SelectedRoutePropertyChanged;

                OnPropertyChanged("SelectedRoute");
                LoadPreview();
            }
        }

        private ScreenCaptureModel screenCapture;

        public ScreenCaptureModel ScreenCapture
        {
            get { return screenCapture ?? defaultPreview; }
            set
            {
                screenCapture = value;
                OnPropertyChanged("ScreenCapture");
            }
        }

        private bool saveRoute = true;

        public bool SaveRoute
        {
            get { return saveRoute; }
            set
            {
                saveRoute = value;
                OnPropertyChanged("SaveRoute");
            }
        }

        private bool allowSaveRoute = true;
        public bool AllowSaveRoute
        {
            get { return allowSaveRoute; }
            set 
            { 
                allowSaveRoute = value;
                OnPropertyChanged("AllowSaveRoute");
                if (!allowSaveRoute)
                {
                    SaveRoute = false;
                }
            }
        }

        internal static string DefaultInfoNotFoundText =
            MorganStanley.MSDotNet.MSGui.Impl.Extensions.FrameworkExtensions.DefaultInfoNotFoundText;

        private string messageInfo;

        public string MessageInfo
        {
            get
            {
                return messageInfo ?? DefaultInfoNotFoundText ?? "Info is not found.";
            }
            protected set
            {
                messageInfo = value;
                OnPropertyChanged("MessageInfo");
            }
        }

        private bool isErrorOccured;

        public bool IsErrorOccured
        {
            get { return isErrorOccured; }
            set
            {
                isErrorOccured = value;
                OnPropertyChanged("IsErrorOccured");
            }
        }

        private bool isScreenCaptureErrorOccured;

        public bool IsScreenCaptureErrorOccured
        {
            get { return isScreenCaptureErrorOccured; }
            set
            {
                isScreenCaptureErrorOccured = value;
                OnPropertyChanged("IsScreenCaptureErrorOccured");
            }
        }

        public string ScreenCaptureErrorText { get; set; }

        #region Commands

        public ICommand OkCommand { get; private set; }

        public ICommand CancelCommand { get; private set; }

        //public ICommand RefreshCommand { get; private set; }

        public ICommand RefreshAllCommand { get; private set; }
         

        #endregion

        #region Auto Select Route  

        private string autoSelectTimeoutText;
        public string AutoSelectTimeoutText
        {
            get { return autoSelectTimeoutText; }
            set
            {
                autoSelectTimeoutText = value;
                OnPropertyChanged("AutoSelectTimeoutText");
            }
        }

        private Visibility autoSelectTimeoutTextVisiblity = Visibility.Collapsed;
        public Visibility AutoSelectTimeoutTextVisiblity
        {
            get { return autoSelectTimeoutTextVisiblity; }
            set
            {
                autoSelectTimeoutTextVisiblity = value;
                OnPropertyChanged("AutoSelectTimeoutTextVisiblity");
            }
        }
         
        private const string AutoSelectTimeoutTextFormat = "Current route would be automatically selected in {0} seconds.";

        private readonly uint autoSelectTimeout = 0;
        private decimal timeElapsed = 0;
        private Timer autoSelectTimer;
        private bool enableAutoSelectTimer = false;
        public void ResumeTimer()
        {
            enableAutoSelectTimer = true;
            if (autoSelectTimer != null)
            { 
                autoSelectTimer.Elapsed -= autoSelectTimer_Elapsed;
                autoSelectTimer.Elapsed += autoSelectTimer_Elapsed; 
                autoSelectTimer.Start();
                AutoSelectTimeoutText = string.Format(AutoSelectTimeoutTextFormat, autoSelectTimeout - Math.Floor(timeElapsed));
            }
        }

        void autoSelectTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            timeElapsed += 0.1m;
            if (Math.Ceiling(timeElapsed) == Math.Floor(timeElapsed))
            {
                AutoSelectTimeoutText = string.Format(AutoSelectTimeoutTextFormat, autoSelectTimeout - Math.Floor(timeElapsed));
            } 
            if (timeElapsed >= autoSelectTimeout)
            { 
                ClearTimer(); 
                if (SelectedRoute == null)
                {
                    SelectedRoute = this.Routes.FirstOrDefault(r_ => r_.IsOnline);
                }
                OkCommand.Execute(null);
            }
        }

        public void SuspendTimer()
        {
            enableAutoSelectTimer = false;
            if (autoSelectTimer != null)
            { 
                autoSelectTimer.Elapsed -= autoSelectTimer_Elapsed;
                autoSelectTimer.Stop();
            }

        }

        public void ClearTimer()
        {
            if (autoSelectTimer == null) return;
            SuspendTimer();
            timeElapsed = 0;
            autoSelectTimer.Dispose();
            autoSelectTimer = null;
            AutoSelectTimeoutTextVisiblity = Visibility.Collapsed;
        }

        private void UpdateTimerStatus()
        { 
            ClearTimer();
            if (autoSelectTimeout > 0 && Routes.Count(r_ => r_.IsOnline) == 1)
            {
                autoSelectTimer = new Timer() { Enabled = false };
                if (enableAutoSelectTimer)
                {
                    ResumeTimer();
                }
                AutoSelectTimeoutTextVisiblity = Visibility.Visible;
            } 
        }
        #endregion
        public RouteViewModel(IRouteManager routeManager, IRoutingCommunicator routingCommunicator, IRouteExplorer scout, IScreenCatpureSink screenCatpureSink_, Type messageType, object message)
        {
            this.routeManager = routeManager;
            this.scout = scout;
            this.routingCommunicator = routingCommunicator;
            this.screenCatpureSink = screenCatpureSink_;
            this.messageType = messageType;
            Routes = new ObservableCollection<RouteModel>();
            this.messageInfo = message.GetMessageInfo(); 
            this.ScreenCaptureErrorText = "Error occurred while loading the preview image.";
            this.screenCatpureSink.SubscribeScreenCaptureResponse(HandleScreenCaptureResult);
            if (routeManager != null && routeManager.EnableAutoSelect == true)
            {
                autoSelectTimeout = routeManager.AutoSelectTimeout;
            } 
 
        }

        public virtual void Initialize() 
        {
            IsRoutesLoading = true;
            this.subscribedApplications = ListSubscribedApplications();
            lock (subscribedApplications)
            {
                foreach (var subscribedApplication in subscribedApplications)
                {
                    Routes.Add(ConvertToRouteModel(subscribedApplication));
                }
            }
            UpdateTimerStatus();
            this.subscribedApplications.CollectionChanged += SubscribedApplications_CollectionChanged;

            OkCommand = new DelegateCommand(Ok);

            CancelCommand = new DelegateCommand(o => OnCancelRaised());

            RefreshAllCommand = new DelegateCommand(o => GetSubscribedApplications()); 
        }        

        protected virtual ObservableCollection<Endpoint> ListSubscribedApplications()
        { 
            var result = scout.GetSubscribedApplications(messageType);
            CheckTimeout();
            return result;
        }

        protected virtual ObservableCollection<Endpoint> RefreshSubscribedApplications()
        {
            return scout.GetSubscribedApplications(messageType);
        }

        protected virtual RouteModel ConvertToRouteModel(Endpoint target_)
        {
            return new RouteModel()
                {
                    Application = target_.Application,
                    Hostname = target_.Hostname,
                    InstanceName = target_.InstanceName,
                    DisplayName = string.IsNullOrEmpty(target_.InstanceName) ? target_.Application : 
                                  (target_.Application + " (" + target_.InstanceName + ")"),
                    IsOnline = true,
                    ServerHostName = target_.CpsServer,
                    ServerIsKerberized = target_.Kerberosed
                };
        }

        private void GetSubscribedApplications()
        {
            IsRoutesLoading = true;
            IsErrorOccured = false;
            Routes.Clear();
            ClearTimer();

            if (this.subscribedApplications != null)
                this.subscribedApplications.CollectionChanged -= SubscribedApplications_CollectionChanged;

            this.subscribedApplications = RefreshSubscribedApplications();
            lock (subscribedApplications)
            {
                foreach (var subscribedApplication in subscribedApplications)
                {
                    Routes.Add(ConvertToRouteModel(subscribedApplication));
                }
            }
            this.subscribedApplications.CollectionChanged += SubscribedApplications_CollectionChanged;

            CheckTimeout();
        }

        private void CheckTimeout()
        {
            DispatcherTimer timer = new DispatcherTimer();

            timer.Tick += Timer_Tick;

            timer.Interval = new TimeSpan(0, 0, 5);
            timer.IsEnabled = true;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            var timer = ((DispatcherTimer)sender);

            timer.IsEnabled = false;
            timer.Tick -= Timer_Tick;

            if (Routes.Count == 0)
            {
                ErrorOccured();
            } 
        }

        private void HandleScreenCaptureResult(ScreenCaptureModel result_)
        {
            if (result_ == null)
            {
                PreviewErrorOccured();
                return;
            }

            IsScreenCaptureErrorOccured = false;

            ScreenCapture = result_;

            ScreenCapture.IsPreviewLoading = false;
        }

        private void Ok(object parameter)
        {
            if (SelectedRoute == null)
            {
                OnOkRaised(true, "Please select a route!");
                return;
            }

            if (!SelectedRoute.IsOnline)
            {
                OnOkRaised(true, "The selected route is not accessible!");
                return;
            }
            //don't specify the instance name if only one application qualified for the specific application is found
            if (SelectedRoute != null &&
    Routes.Count(
        r_ => r_.Application == selectedRoute.Application && r_.Hostname == selectedRoute.Hostname) < 2)
            {
                selectedRoute.InstanceName = null;
            }
            OnOkRaised();
        }

        //private void Refresh(object parameter)
        //{
        //    if (parameter is RouteModel)
        //    {
        //        IsRoutesLoading = true;
        //        var model = parameter as RouteModel;
        //        this.scout.BypassSubscribedApplication(model.Hostname, model.Application, messageType);
        //    }
        //    return;
        //}

        //private void Scout_SubscriptionBypassEvent(object sender, SubscriptionBypassEventArgs e)
        //{
        //    var bypassed = this.Routes.FirstOrDefault(r => r.Hostname == e.Bypassed.Hostname && r.Application == e.Bypassed.Application);

        //    if(bypassed != null)
        //        bypassed.IsOnline = e.Status == DeliveryStatus.Delivered;

        //    IsRoutesLoading = false;

        //    if (SelectedRoute != null && e.Bypassed.Hostname == SelectedRoute.Hostname && e.Bypassed.Application == SelectedRoute.Application)
        //        LoadPreview();
        //}

        private void LoadPreview()
        {
            ScreenCapture = null;
            IsScreenCaptureErrorOccured = false;
            ScreenCapture.IsPreviewLoading = true;

            if (SelectedRoute != null)
            {
                if (!SelectedRoute.IsOnline)
                {
                    PreviewErrorOccured();
                    return;
                }

                var deliveryInfo = screenCatpureSink.PublishScreenCaptureRequest(SelectedRoute);
                deliveryInfo.PropertyChanged += DeliveryInfo_PropertyChanged;
            }
        } 

        private void DeliveryInfo_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var deliveryInfo = (DeliveryInfo)sender;

            switch (e.PropertyName)
            {
                case "Status":
                    switch (deliveryInfo.Status)
                    {
                        case DeliveryStatus.Failed:
                            PreviewErrorOccured();
                            deliveryInfo.PropertyChanged -= DeliveryInfo_PropertyChanged;
                            break;
                        case DeliveryStatus.Delivered:
                            IsScreenCaptureErrorOccured = false;
                            deliveryInfo.PropertyChanged -= DeliveryInfo_PropertyChanged;
                            break;
                    }
                    break;
            }
        }

        private void SelectedRoutePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged("SelectedRoute");
        }

        private void SubscribedApplications_CollectionChanged(object sender, NotifyCollectionChangedEventArgs eventArgs)
        {
            Action dispatched = () =>
            {
                switch (eventArgs.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        foreach (Endpoint endpoint in eventArgs.NewItems)
                        {
                            if(!Routes.Any(r => r.Hostname == endpoint.Hostname && r.Application == endpoint.Application && 
                                r.InstanceName == endpoint.InstanceName))
                            {
                                Routes.Add(ConvertToRouteModel(endpoint));

                                IsErrorOccured = false;
                            }
                        }

                        IsRoutesLoading = false;
                        UpdateTimerStatus();
                        break;
                    case NotifyCollectionChangedAction.Remove:
                        foreach (Endpoint endpoint in eventArgs.OldItems)
                        {
                            var offline = Routes.Where(route => route.Hostname == endpoint.Hostname && route.Application == endpoint.Application && 
                                route.InstanceName == endpoint.InstanceName).ToList();

                            foreach (var off in offline)
                            {
                                off.IsOnline = false;

                                if (SelectedRoute != null && off.Hostname == SelectedRoute.Hostname && off.Application == SelectedRoute.Application && 
                                    off.InstanceName == SelectedRoute.InstanceName)
                                    PreviewErrorOccured();
                            }
                        }
                        UpdateTimerStatus();
                        break;
                }
            };

            if (Application.Current != null)
                Application.Current.Dispatcher.Invoke(dispatched);
        }

        private void ErrorOccured()
        {
            IsRoutesLoading = false;
            IsErrorOccured = true;
        }

        private void PreviewErrorOccured()
        {
            ScreenCapture.Source = null;
            ScreenCapture.IsSetout = false;
            ScreenCapture.IsPreviewLoading = false;
            IsScreenCaptureErrorOccured = true;
        }

        #region OkEvent

        public event EventHandler<OkEventArgs> OkEvent;

        private void OnOkRaised(bool isError = false, string errorMessage = null)
        {
            var handler = OkEvent;
            if (handler != null)
                handler(this, new OkEventArgs(isError, errorMessage));
        }

        #endregion

        #region CancelEvent

        public event EventHandler CancelEvent;

        private void OnCancelRaised()
        { 
            var handler = CancelEvent;
            if (handler != null)
                handler(this, new EventArgs());
        }

        #endregion
         
    }
}
