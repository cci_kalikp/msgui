﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using MSDesktop.MessageRouting.Routing;
using System.Globalization;
using MSDesktop.MessageRouting.GUI.Model;

namespace MSDesktop.MessageRouting.GUI.Converter
{
    public class SelectedRouteListToEnabledConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var routes = value as IList<ConfigRouteModel>;

            if (routes == null)
                return false;

            return routes.Count > 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
