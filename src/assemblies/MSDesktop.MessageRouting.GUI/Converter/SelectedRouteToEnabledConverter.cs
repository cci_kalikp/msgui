﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;
using MSDesktop.MessageRouting.GUI.Model;

namespace MSDesktop.MessageRouting.GUI.Converter
{
    public class SelectedRouteToEnabledConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var route = value as RouteModel;

            if (route == null)            
                return false;

            return route.IsOnline;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
