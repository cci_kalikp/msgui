﻿using MorganStanley.MSDotNet.MSGui.Core.SmartApi; 
using MSDesktop.MessageRouting.GUI.Modules;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    public static class FrameworkExtensions
    {
        public static void EnableMessageRoutingDefaultGui(this Framework framework)
        {
            ExtensionsUtils.RequireAssembly(ExternalAssembly.Interactivity);
            framework.AddModule<MessageRoutingGuiModule>();
        }

        internal static string DefaultInfoNotFoundText;

        public static void EnableMessageRoutingDefaultGui(this Framework framework, string infoNotFoundDefaultText = null)
        {
            framework.EnableMessageRoutingDefaultGui();
            DefaultInfoNotFoundText = infoNotFoundDefaultText;
        }
        public static void EnableMessageRoutingDefaultGui(this Framework framework, string infoNotFoundDefaultText = null, uint? autoSelectRouteWithin = null)
        {
            framework.EnableMessageRoutingDefaultGui(infoNotFoundDefaultText);
            MessageRoutingGuiModule.AutoSelectRouteWithin = autoSelectRouteWithin;
        }
    }
}
