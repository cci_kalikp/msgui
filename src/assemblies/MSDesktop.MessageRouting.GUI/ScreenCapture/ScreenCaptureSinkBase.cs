﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using MSDesktop.MessageRouting.GUI.Message;
using MSDesktop.MessageRouting.GUI.Model;
using MSDesktop.MessageRouting.GUI.Util;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting.GUI
{
    public abstract class ScreenCaptureSinkBase<TRequest>: IScreenCatpureSink where TRequest:ScreenCaptureRequestMessage
    {
        private readonly IRoutedPublisher<ScreenCaptureResponseMessage> screenCaptureResponsePublisher;
        private readonly IRoutedPublisher<TRequest> screenCaptureRequestPublisher;
        protected readonly IRoutingCommunicator Communicator;

        protected ScreenCaptureSinkBase(IRoutingCommunicator communicator_)
        {
            Communicator = communicator_;
            this.screenCaptureResponsePublisher = communicator_.GetPublisher<ScreenCaptureResponseMessage>();
            this.screenCaptureRequestPublisher = communicator_.GetPublisher<TRequest>(); 
        } 

        public DeliveryInfo PublishScreenCaptureRequest(RouteModel target_)
        {
            return this.screenCaptureRequestPublisher.Publish(ConvertToRequest(target_));
        }

        public void SubscribeScreenCaptureRequest()
        {
            Communicator.GetSubscriber<TRequest>().GetObservable().Subscribe(HandleScreenCaptureRequest);
        }

        public void SubscribeScreenCaptureResponse(Action<ScreenCaptureModel> handler_)
        {
            this.Communicator.GetSubscriber<ScreenCaptureResponseMessage>().GetObservable().Subscribe(response_ => handler_(response_.Source == null ? null : new ScreenCaptureModel(response_)));
        }

        public void HandleScreenCaptureRequest(TRequest request_)
        {
            Action screenCaptureAction = () =>
            {
                var response = RepondScreenCaptureRequest(request_);
                response.Target = request_.Origin;

                screenCaptureResponsePublisher.Publish(response);
            };

            Application.Current.Dispatcher.BeginInvoke(screenCaptureAction);
        }

        protected abstract TRequest ConvertToRequest(RouteModel target_);


        protected abstract ScreenCaptureResponseMessage RepondScreenCaptureRequest(TRequest request_);
    }
}
