﻿using System;
using MSDesktop.MessageRouting.GUI.Model;
using MSDesktop.MessageRouting.GUI.Util;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting.GUI
{
    public interface IScreenCatpureSink
    {
        DeliveryInfo PublishScreenCaptureRequest(RouteModel target_);

        void SubscribeScreenCaptureRequest();

        void SubscribeScreenCaptureResponse(Action<ScreenCaptureModel> handler_);
    }
}
