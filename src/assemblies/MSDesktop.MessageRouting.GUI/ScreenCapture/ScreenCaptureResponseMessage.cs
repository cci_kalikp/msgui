﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.MessageRouting.Routing;
using MSDesktop.MessageRouting.Message;

namespace MSDesktop.MessageRouting.GUI.Message
{
    [Serializable]
    [InternalMessage]
    [DeliveryStrategy(DeliveryStrategy = DeliveryStrategy.FireAndForget, Fallback = false)]
    public class ScreenCaptureResponseMessage : IExplicitTarget, IProvideMessageInfo
    {
        public Endpoint Target { get; set; }

        public byte[] Source { get; set; }
        public bool IsSetout { get; set; }
        public double SetoutX { get; set; }
        public double SetoutY { get; set; }
        public double SetoutWidth { get; set; }
        public double SetoutHeight { get; set; }

        public string GetInfo()
        {
            return "Respond the screen shot of current application to " + Target + ": " + (Source == null || Source.Length == 0 ? 
                "No screen shot taken" : 
                ("Screen shot taken successfully" + "SetoutX: " + SetoutX + " SetoutY: " + SetoutY + " SetoutWidth: " + SetoutWidth + " SetoutHeight: " + SetoutHeight));
        }
    }
}
