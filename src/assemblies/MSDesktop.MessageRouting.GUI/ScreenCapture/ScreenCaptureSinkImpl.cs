﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.MessageRouting.GUI.Message;
using MSDesktop.MessageRouting.GUI.Model;
using MSDesktop.MessageRouting.GUI.Util;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting.GUI
{
    public class ScreenCaptureSinkImpl:ScreenCaptureSinkBase<ScreenCaptureRequestMessage>
    {
        public ScreenCaptureSinkImpl(IRoutingCommunicator communicator_) : base(communicator_)
        {
            
        }

        protected override ScreenCaptureRequestMessage ConvertToRequest(RouteModel target_)
        {
            return new ScreenCaptureRequestMessage
            {
                Target = new Endpoint(){Application = target_.Application, Hostname = target_.Hostname, InstanceName = target_.InstanceName},
                Origin = Endpoint.LocalEndpoint
            };
        }


        protected override ScreenCaptureResponseMessage RepondScreenCaptureRequest(ScreenCaptureRequestMessage request_)
        {
            return ScreenCapturer.CaptureWindow();
        }
        
    }
}
