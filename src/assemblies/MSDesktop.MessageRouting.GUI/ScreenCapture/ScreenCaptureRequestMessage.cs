﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.MessageRouting.Routing;
using MSDesktop.MessageRouting.Message;

namespace MSDesktop.MessageRouting.GUI.Message
{
    [Serializable]
    [InternalMessage]
    [DeliveryStrategyAttribute(Fallback = false)]
    public class ScreenCaptureRequestMessage : IExplicitTarget, IProvideMessageInfo
    {
        public Endpoint Origin { get; set; }
        public Endpoint Target { get; set; }

        public string GetInfo()
        {
            return "Query the screen shot of " + Target;
        }
    }
}
