﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Windows.Interop;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Runtime.InteropServices;
using System.IO;
using MSDesktop.MessageRouting.GUI.Message;
using MorganStanley.MSDotNet.MSGui.Core.Win32;

namespace MSDesktop.MessageRouting.GUI.Util
{
    public class ScreenCapturer
    {
        private const double RESIZE = 300;

        public static ScreenCaptureResponseMessage CaptureWindow()
        {
            try
            {
                return CaptureWindow(new WindowInteropHelper(System.Windows.Application.Current.MainWindow).Handle);
            }
            catch
            {
                return new ScreenCaptureResponseMessage();
            }
        }

        public static ScreenCaptureResponseMessage CaptureWindow(Process process_)
        {
            if (process_ == null) throw new ArgumentNullException("process_");
            try
            {
                if (process_.MainWindowHandle != IntPtr.Zero)
                {
                    return CaptureWindow(process_.MainWindowHandle);
                }


                return null;
            }
            catch  
            { 
                return new ScreenCaptureResponseMessage();
            }
        }

        public static ScreenCaptureResponseMessage CaptureWindow(IntPtr handle_)
        {
            try
            {
                var screen = Screen.FromHandle(handle_);
                var rect = new Win32.RECT();
                Win32.GetWindowRect(handle_, ref rect);
                //consider docked window
                int height = screen.WorkingArea.Height;
                if (rect.Bottom > screen.WorkingArea.Height && Math.Abs(rect.Top - screen.WorkingArea.Height) < 5)
                {
                    height = rect.Bottom;
                }
                var ratio = GetRatio(screen.WorkingArea.Width, height);
                  
                return new ScreenCaptureResponseMessage
                {
                    Source = Capture(screen.WorkingArea.Width, height, screen.WorkingArea.X, screen.WorkingArea.Y),
                    IsSetout = true,
                    SetoutHeight = rect.GetHeight() * ratio,
                    SetoutWidth = rect.GetLength() * ratio,
                    SetoutX = (rect.Left - screen.WorkingArea.Left) * ratio,
                    SetoutY = (rect.Top - screen.WorkingArea.Top) * ratio
                };
            }
            catch
            {
                return new ScreenCaptureResponseMessage();
            }
        }
        public static BitmapSource BytesToBitmapSource(byte[] bytes)
        {
            BitmapSource source = null;

            using (var stream = new MemoryStream(bytes))
            {
                var decoder = BitmapDecoder.Create(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
                source = decoder.Frames[0];
            }

            return source;
        }
        
        private static byte[] Capture(int width, int height, int fromX = 0, int fromY = 0)
        {
            using (var screen = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
            {
                using (var graphic = Graphics.FromImage(screen))
                {
                    graphic.CopyFromScreen(fromX, fromY, 0, 0, screen.Size);
                    
                    var ratio = GetRatio(width, height);
                    var hbitmap = screen.GetHbitmap();

                    Image img = Image.FromHbitmap(hbitmap).GetThumbnailImage((int)(width * ratio), (int)(height * ratio), null, IntPtr.Zero);
                    byte[] imageBytes;

                    using (var stream = new MemoryStream())
                    {
                        img.Save(stream, ImageFormat.Jpeg);
                        imageBytes = stream.GetBuffer();
                    }

                    DeleteObject(hbitmap);

                    return imageBytes;
                }
            }
        }

        private static double GetRatio(int width, int height)
        {
            return RESIZE / (double)(width > height ? width : height);
        }

        private static Screen GetScreen()
        {
            return Screen.FromHandle(new WindowInteropHelper(System.Windows.Application.Current.MainWindow).Handle);
        } 
        #region Native methods

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("gdi32", ExactSpelling = true, CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        internal static extern bool DeleteObject(IntPtr hObject);

        #endregion
    }
}
