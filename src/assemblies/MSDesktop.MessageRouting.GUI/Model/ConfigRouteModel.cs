﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using MSDesktop.MessageRouting.Routing;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel; 

namespace MSDesktop.MessageRouting.GUI.Model
{
    public class ConfigRouteModel : ViewModelBase
    { 
        public ConfigRouteModel(Route route_)
        {
            MessageTypeName = route_.MessageTypeIdentifier;
            OriginApplication = route_.Origin.Application;
            OriginHostname = route_.Origin.Hostname;
            TargetApplication = route_.Target.Application;
            TargetHostname = route_.Target.Hostname;
            TargetInstanceName = route_.Target.InstanceName;
            CreatedAt = route_.CreatedAt;
            IsDirty = false;
        }
        public bool IsDirty { get; private set; }
        public string MessageTypeName { get; set; }

        public bool DefaultRoute { get; set; } 
      
        private string targetHostname;

        public string TargetHostname
        {
            get { return targetHostname; }
            set
            {
                IsDirty = true;
                targetHostname = value;
                OnPropertyChanged("TargetHostname");
            }
        }

        public string TargetApplicationResolved
        {
            get
            {
                return string.IsNullOrEmpty(TargetInstanceName) ? TargetApplication :
                    TargetApplication + " (" + TargetInstanceName + ")";
            }
            set
            {
                IsDirty = true;
                if (string.IsNullOrEmpty(value))
                {
                    TargetApplication = value;
                    TargetInstanceName = null; 
                    return;
                }
                if (value.Contains("(") && value.Contains(")"))
                {
                    int index = value.IndexOf("(", System.StringComparison.Ordinal);
                    TargetApplication = value.Substring(0, index).Trim();
                    TargetInstanceName = value.Substring(index).Trim('(', ')', ' ');
                }
                else
                {
                    TargetApplication = value;
                    TargetInstanceName = null;
                }
                OnPropertyChanged("TargetApplicationResolved");
            }
        }
        public string TargetApplication { get; set; }
        public string TargetInstanceName { get; set; }
        public string OriginHostname { get; set; }
        public string OriginApplication { get; set; } 

        public DateTime? CreatedAt { get; set; }
         

        public override string ToString()
        {
            return string.Format("Route to {0} for {1}", TargetHostname, MessageTypeName);
        }
    }
}
