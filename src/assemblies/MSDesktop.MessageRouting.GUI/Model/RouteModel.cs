﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.MessageRouting.GUI.Model
{
    public class RouteModel : ViewModelBase
    {
        private string hostname;

        public string Hostname
        {
            get { return hostname; }
            set
            {
                hostname = value;
                OnPropertyChanged("Hostname");
            }
        }

        private string application;

        public string Application
        {
            get { return application; }
            set
            {
                application = value; 
                OnPropertyChanged("Application");
            }
        }

        private string displayName;

        public string DisplayName
        {
            get { return displayName; }
            set
            {
                displayName = value;
                OnPropertyChanged("DisplayName");
            }
        }

        private string instanceName;
        public string InstanceName
        {
            get { return instanceName; }
            set
            {
                instanceName = value; 
                OnPropertyChanged("InstanceName");
            }
        }
        private bool isOnline;

        public bool IsOnline
        {
            get { return isOnline; }
            set
            {
                isOnline = value;
                OnPropertyChanged("IsOnline");
            }
        }

        private string serverHostName;

        public string ServerHostName
        {
            get { return serverHostName; }
            set
            {
                serverHostName = value;
                OnPropertyChanged("ServerHostName");
            }
        }

        private bool serverIsKerberized;

        public bool ServerIsKerberized 
        {
            get { return serverIsKerberized; }
            set
            {
                serverIsKerberized = value;
                OnPropertyChanged("ServerIsKerberized");
            }
        }
 
    }
}
