﻿using System;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using MSDesktop.MessageRouting.GUI.Message;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.MessageRouting.GUI.Util
{
    public class ScreenCaptureModel : ViewModelBase
    {
        private static readonly BitmapImage defaultSource = new BitmapImage(
            new Uri("/MSDesktop.MessageRouting.GUI;component/Images/defaultPreview.png", UriKind.Relative));

        private BitmapSource _source;
        private bool _isPreviewLoading;
        private bool _isSetout;
        private double _setoutX;
        private double _setoutY;
        private double _setoutWidth;
        private double _setoutHeight;

        public BitmapSource Source
        {
            get { return _source ?? defaultSource; }
            set
            {
                _source = value;
                OnPropertyChanged("Source");
            }
        }

        public bool IsPreviewLoading
        {
            get { return _isPreviewLoading; }
            set
            {
                _isPreviewLoading = value;
                OnPropertyChanged("IsPreviewLoading");
            }
        }

        public bool IsSetout
        {
            get { return _isSetout; }
            set
            {
                _isSetout = value;
                OnPropertyChanged("IsSetout");
            }
        }


        public double SetoutX
        {
            get { return _setoutX - 2.0; }
            set
            {
                _setoutX = value;
                OnPropertyChanged("SetoutX");
            }
        }

        public double SetoutY
        {
            get { return _setoutY - 2.0; }
            set
            {
                _setoutY = value;
                OnPropertyChanged("SetoutY");
            }
        }

        public double SetoutWidth
        {
            get { return _setoutWidth; }
            set
            {
                _setoutWidth = value;
                OnPropertyChanged("SetoutWidth");
            }
        }

        public double SetoutHeight
        {
            get { return _setoutHeight; }
            set
            {
                _setoutHeight = value;
                OnPropertyChanged("SetoutHeight");
            }
        }

        public ScreenCaptureModel(){}
        public ScreenCaptureModel(ScreenCaptureResponseMessage response_)
        {
            _source = ScreenCapturer.BytesToBitmapSource(response_.Source);
            _isSetout = response_.IsSetout;
            _setoutX = response_.SetoutX;
            _setoutY = response_.SetoutY;
            _setoutHeight = response_.SetoutHeight;
            _setoutWidth = response_.SetoutWidth;
        }
 
    }
}
