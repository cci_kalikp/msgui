﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl;
using MSDesktop.MessageRouting.History.Modules;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    public static class FrameworkExtensions
    {
        public static void EnableMessageRoutingHistory(this Framework framework)
        {
            framework.AddModule<MessageRoutingHistoryModule>();

        }
    }
}
