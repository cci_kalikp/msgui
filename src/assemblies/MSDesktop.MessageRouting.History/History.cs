﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting.History
{
    public class History
    {
        public DateTime When { get; set; }
        public Endpoint Target { get; set; }
        public DeliveryStatus Status { get; set; }
        public int Id { get; set; }
        public string MessageInfo { get; set; }
        public bool IsInternal { get; set; }
    }
}
