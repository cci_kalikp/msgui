﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using MSDesktop.MessageRouting.Routing;
using MSDesktop.MessageRouting.Message;
using System.Threading;
using System.Collections;

namespace MSDesktop.MessageRouting.History
{
    public class HistoryManager : IHistoryManager
    {
        private const int HISTORYSIZE = 100;

        private readonly IRoutingCommunicator routingCommunicator;

        private readonly Queue queue;

        public IEnumerable<History> Histories
        {
            get
            {
                return queue.ToArray().Select(h => (History)h).Reverse();
            }
        }

        public HistoryManager(IRoutingCommunicator routingCommunicator)
        {
            this.routingCommunicator = routingCommunicator;
            this.queue = Queue.Synchronized(new Queue());

            var remover = new Thread(Remover)
            {
                Name = "MSDesktop message routing history cleanup",
				IsBackground = true
            };
            remover.Start();

            this.routingCommunicator.MessageDelivered += RoutingCommunicator_MessageDelivered;
            this.routingCommunicator.MessageFailed += RoutingCommunicator_MessageFailed;
        }

        private void RoutingCommunicator_MessageDelivered(object sender, MessageDeliveredEventArgs eventArgs)
        {
            Add(eventArgs.DeliveryInfo, eventArgs.Message);
        }

        private void RoutingCommunicator_MessageFailed(object sender, MessageFailedEventArgs eventArgs)
        {
            Add(eventArgs.DeliveryInfo, eventArgs.Message);
        }

        private void Add(DeliveryInfo deliveryInfo, object message)
        {
            var history = new History
            {
                Id = deliveryInfo.Id,
                Status = deliveryInfo.Status,
                Target = deliveryInfo.Target,
                When = DateTime.Now,
                IsInternal = message != null && 
                message.GetType().GetCustomAttributes(typeof(InternalMessageAttribute), true).Length > 0
            };

            string messageInfo = message.GetMessageInfo(); 
            if (!string.IsNullOrEmpty(messageInfo))
            {
                history.MessageInfo = messageInfo;
            }

            queue.Enqueue(history);
        }

        private void Remover()
        {
            while (true)
            {
                while (queue.Count > HISTORYSIZE)
                    queue.Dequeue();

                Thread.Sleep(100);
            }
        }
    }
}
