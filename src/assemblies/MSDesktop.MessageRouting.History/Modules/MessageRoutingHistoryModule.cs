﻿using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.MessageRouting.History.Modules
{
    public class MessageRoutingHistoryModule : IMSDesktopModule
    {
        private readonly IUnityContainer container;
        private readonly IRoutingCommunicator routingCommunicator;
        private IHistoryManager historyManager;

        public MessageRoutingHistoryModule(IUnityContainer container, IRoutingCommunicator routingCommunicator)
        {
            this.container = container;
            this.routingCommunicator = routingCommunicator;
        }

        public void Initialize()
        {
            this.historyManager = new HistoryManager(this.routingCommunicator);

            this.container.RegisterInstance(this.historyManager);
        }
    }
}
