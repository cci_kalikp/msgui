﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting.History
{
    public interface IHistoryManager
    {
        IEnumerable<History> Histories { get; }
    }
}
