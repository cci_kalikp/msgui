﻿using System;
using System.Diagnostics;
using System.Linq;

namespace MSDesktop.Isolation.X86
{
    internal class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            if (args != null &&
                args.Length > 0 &&
                args.Any(parameter => string.Equals(parameter, @"/debug", StringComparison.CurrentCultureIgnoreCase)))
            {
                Debugger.Launch();
            }

            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            ProcessMain.IsolatedProcessMain(args);
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            LaunchDebugger();
        }

        private static void LaunchDebugger()
        {
#if DEBUG
            Debugger.Launch();
#endif
        }
    }
}
