﻿using System;
using System.Windows.Input;
using System.Windows.Interop;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation.AddInInterface
{
	public interface IXProcAddInSite
	{
		int HostProcessId { get; }
		void SetAddIn(IXProcAddIn addin);
		bool TabOut(TraversalRequest request);
		bool TranslateAccelerator(MSG msg);
	};

	public interface IXProcAddIn
	{
		IntPtr AddInWindow { get; }
		void OnAddInAttached();
		bool TabInto(TraversalRequest request);
		void ShutDown();
	};
}
