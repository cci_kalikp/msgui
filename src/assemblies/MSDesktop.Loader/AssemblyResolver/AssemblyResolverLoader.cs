﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.AssemblyResolver
{
	internal static class AssemblyResolverLoader
	{
		internal static void StartViaAssemblyResolver(string[] args_, string configPath)
		{
			global::MorganStanley.MSDotNet.Runtime.AssemblyResolver.Load(configPath);
			global::MorganStanley.MSDotNet.Runtime.AssemblyResolver.RunMain(args_);
		}
	}
}