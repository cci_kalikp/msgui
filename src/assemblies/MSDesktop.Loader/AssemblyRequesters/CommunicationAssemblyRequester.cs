﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;

namespace MorganStanley.Desktop.Loader.AssemblyRequesters
{
    class CommunicationAssemblyRequester:AssemblyRequesterBase<Communication>
    {
        protected override IList<ExternalAssembly> GetAssemblesNeeded(Communication configuration_)
        {
            List<ExternalAssembly> assemblies = new List<ExternalAssembly>();
            if (configuration_.ImcCommunicationStatus != CommunicationChannelStatus.Disabled)
            {
                assemblies.Add(ExternalAssembly.GoogleProtocolBuffers);
            }
            if (configuration_.MessageRoutingCommunicationEnabled)
            {
                assemblies.AddRange(new[] { ExternalAssembly.MSDesktopMessageRouting, ExternalAssembly.MSDesktopMessageRoutingInterfaces});
            }
            if (configuration_.MessageRoutingDefaultGuiEnabled)
            {
                assemblies.Add(ExternalAssembly.MSDesktopMessageRoutingGUI);
            }
            if (configuration_.MessageRoutingHistoryEnabled)
            {
                assemblies.Add(ExternalAssembly.MSDesktopMessageRoutingHistory);
            }
            if (configuration_.MessageRoutingHistoryGuiEnabled) 
            {
                assemblies.Add(ExternalAssembly.MSDesktopMessageRoutingHistoryGUI);
            }
            if (configuration_.MessageRoutingMonitorEnabled)
            {
                assemblies.Add(ExternalAssembly.MSDesktopMessageRoutingMonitor);
            }
            return assemblies;
        }
    }
}
