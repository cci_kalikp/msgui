﻿using System;
using System.Collections.Generic;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;

namespace MorganStanley.Desktop.Loader.AssemblyRequesters
{
    class EnvironmentAssemblyRequester : AssemblyRequesterBase<Configuration.Environment>
    {
        protected override IList<ExternalAssembly> GetAssemblesNeeded(Configuration.Environment configuration_)
        {
            return new ExternalAssembly[] { ExternalAssembly.MSDesktopEnvironment }; 
        }
    }
}
