﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;

namespace MorganStanley.Desktop.Loader.AssemblyRequesters
{
    class ViewModelBasedButtonsAssemblyRequester : AssemblyRequesterBase<ViewModelBasedButtons>
    {
        protected override IList<ExternalAssembly> GetAssemblesNeeded(ViewModelBasedButtons configuration_)
        {
            if (!string.IsNullOrEmpty(configuration_.DefinitionFile) ||
                (configuration_.Buttons != null && configuration_.Buttons.Count > 0))
            {
                return new ExternalAssembly[] { ExternalAssembly.MSDesktopViewModelAPI };
            }
            return null;
        }
    }
}
