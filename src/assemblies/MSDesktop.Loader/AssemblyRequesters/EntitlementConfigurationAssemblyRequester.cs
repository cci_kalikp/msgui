﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Loader.Configuration; 
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;

namespace MorganStanley.Desktop.Loader.AssemblyRequesters
{
    class EntitlementConfigurationAssemblyRequester : AssemblyRequesterBase<EntitlementConfiguration>
    {
        protected override IList<ExternalAssembly> GetAssemblesNeeded(EntitlementConfiguration configuration_)
        {
            if (!configuration_.EntitlementEnabled)
            {
                return null;
            }
            return new ExternalAssembly[] { ExternalAssembly.MSDesktopEntitlement, ExternalAssembly.MSDesktopModuleEntitlements }; 
        }
    }
}
