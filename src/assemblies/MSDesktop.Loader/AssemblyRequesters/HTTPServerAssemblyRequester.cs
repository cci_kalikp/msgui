﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;

namespace MorganStanley.Desktop.Loader.AssemblyRequesters
{
    class HTTPServerAssemblyRequester : AssemblyRequesterBase<HTTPServer>
    {
        protected override IList<ExternalAssembly> GetAssemblesNeeded(HTTPServer configuration_)
        {
            if (!configuration_.Enabled)
            {
                return null;
            }
            return new ExternalAssembly[] { ExternalAssembly.MSDesktopHTTPServer }; 
        }
    }
}
