﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;

namespace MorganStanley.Desktop.Loader.AssemblyRequesters
{
    class MSDesktopConfigurationAssemblyRequester:AssemblyRequesterBase<MSDesktopConfiguration>
    { 
        protected override IList<ExternalAssembly> GetAssemblesNeeded(MSDesktopConfiguration configuration_)
        {

            var list = new List<ExternalAssembly>();
            if (configuration_.GUI.ShellMode != ShellMode.LauncherBarAndFloatingWindows)
            {
                list.Add(ExternalAssembly.Infragistics);
                list.Add(ExternalAssembly.InfragisticsEditors);
                list.Add(ExternalAssembly.InfragisticsDockManager);
                list.Add(ExternalAssembly.InfragisticsRibbon);
            }
            else if (!configuration_.GUI.UseHarmoniaDockManager)
            {
                list.Add(ExternalAssembly.Infragistics); 
                list.Add(ExternalAssembly.InfragisticsDockManager); 
            }
            list.AddRange(new ExternalAssembly[]
                {
                    ExternalAssembly.MSDotNetRuntime,
                    ExternalAssembly.Log4net, 
                    ExternalAssembly.MicrosoftComposite,
                    ExternalAssembly.MicrosoftCompositePresentation,
                    ExternalAssembly.MicrosoftCompositeUnityExtensions,
                    ExternalAssembly.MicrosoftServiceLocation,
                    ExternalAssembly.MicrosoftCompositeObjectBuilder2,
                    ExternalAssembly.MicrosoftUnity,
                    ExternalAssembly.ConcordLogger,
                    ExternalAssembly.ConcordApplication,
                    ExternalAssembly.ConcordRuntime,
                    ExternalAssembly.DnpartsMSLog,
                    ExternalAssembly.DnpartsMSTools,
                    ExternalAssembly.DnpartsMSXml,
                    ExternalAssembly.DnpartsMSNet,
                    ExternalAssembly.DnpartsMSNetNative,
                    ExternalAssembly.DnpartsChannels,
                    ExternalAssembly.DnpartsChannelsCps,
                    ExternalAssembly.DnpartsProtocolBuffers,
                    ExternalAssembly.MSDesktopServiceDiscovery,
                    ExternalAssembly.DnpartsCafDispatch,
                    ExternalAssembly.MSDesktopAuthenticationService
                });

            return list;
        }
    }
}
