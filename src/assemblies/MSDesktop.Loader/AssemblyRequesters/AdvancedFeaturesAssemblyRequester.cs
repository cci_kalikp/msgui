﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;

namespace MorganStanley.Desktop.Loader.AssemblyRequesters
{
    class AdvancedFeaturesAssemblyRequester : AssemblyRequesterBase<AdvancedFeatures>
    {
        protected override IList<ExternalAssembly> GetAssemblesNeeded(AdvancedFeatures configuration_)
        {
            List<ExternalAssembly> assemblies = new List<ExternalAssembly>();
            if (configuration_.DebuggingEnabled)
            {
                assemblies.Add(ExternalAssembly.MSDesktopDebugging);
            } 
            if (configuration_.MailServiceEnabled)
            {
                assemblies.Add(ExternalAssembly.MSDesktopMailService);
            }
            if (configuration_.WormholeEnabled)
            {
                assemblies.Add(ExternalAssembly.MSDesktopWormhole);
            } 
            if (configuration_.WebSupportEnabled)
            {
                assemblies.AddRange(new []
                    {
                        ExternalAssembly.MSDesktopWebSupport
                    });
                if (!System.Environment.Is64BitProcess) //currently cef is only supported for 32 bit app
                {
                    assemblies.AddRange(new []
                        {
                             ExternalAssembly.MSDesktopWebSupportCEF,
                             ExternalAssembly.CefSharp, ExternalAssembly.CefSharpWpf
                        });
                }
            }
            return assemblies;
        }
    }
}
