﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;

namespace MorganStanley.Desktop.Loader.AssemblyRequesters
{
    class IsolatedSubsystemsAssemblyRequester: AssemblyRequesterBase<IsolatedSubsystems>
    {
        protected override IList<ExternalAssembly> GetAssemblesNeeded(IsolatedSubsystems configuration_)
        {
            if (configuration_.Subsystems == null || configuration_.Subsystems.Count == 0) return null;
            return new ExternalAssembly[] { ExternalAssembly.MSDesktopIsolationInterfaces, ExternalAssembly.MSDesktopIsolation };
        }
    } 
}
