﻿using System;
using System.Collections.Generic;
using System.Linq;
using MSDesktop.Wormhole; 
using Microsoft.Practices.Unity; 
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Modules;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;

namespace MorganStanley.Desktop.Loader.Modules
{
    public class ExternalApplicationsConfigurationModule : IMSDesktopModule
    {
        private readonly IChromeManager manager;
        private readonly IChromeRegistry registry;
        private readonly IUnityContainer container;

        private readonly ExternalApplications externalApplications; 

        public ExternalApplicationsConfigurationModule(IChromeManager manager, IChromeRegistry registry, IUnityContainer container)
        {
            this.manager = manager;
            this.registry = registry;
            this.container = container;
            externalApplications = container.Resolve<ExternalApplications>();
        }

        public void Initialize()
        {
            var application = new WormholeBridgedApplication(registry, manager, container);
            application.WindowCaptured += new EventHandler<WindowEventArgs>(application_WindowCaptured); 
            foreach (var externalApplication in externalApplications.Applications)
            {
                if (string.IsNullOrEmpty(externalApplication.ExecutableName)) return;
                bool hasStaticInvokerId = !string.IsNullOrEmpty(externalApplication.InvokerId);

                if (hasStaticInvokerId && externalApplication.CheckEntitlement)
                {
                    registry.CheckEntitlement(externalApplication.InvokerId);
                }

                if (!hasStaticInvokerId)
                {
                    externalApplication.InvokerId = Guid.NewGuid().ToString("N");
                }

                var copy = externalApplication;
                
                var holder = application.RegisterWindowFactory(externalApplication.InvokerId) as WindowFactoryHolder;
  
                if (holder != null)
                {
                    holder.AddStep((container_, state_, chain_) =>
                    {
                        if (state_ == null && !string.IsNullOrEmpty(copy.ExecutableName)) //not restored
                        {
                            if (copy.WindowLocation == InitialWindowLocation.DockBottom ||
                                copy.WindowLocation == InitialWindowLocation.DockLeft ||
                                copy.WindowLocation == InitialWindowLocation.DockRight ||
                                copy.WindowLocation == InitialWindowLocation.DockTop)
                            {
                                var tab = manager.Tabwell.Tabs.FirstOrDefault(t_ => t_.Title == copy.WindowTabName);
                                if (tab != null) tab.Activate();
                                else
                                {
                                    manager.Tabwell.AddTab(copy.WindowTabName);
                                }
                            }
                        }
                        if (chain_.Count > 0)
                        {
                            var initHandler = chain_.Pop();
                            var initResult = initHandler(container_, state_, chain_);
                            if (!initResult)
                                return false;
                        }
                        return true;
                    });
                }

                InitialLocation location;
                switch (externalApplication.WindowLocation)
                {
                    case InitialWindowLocation.FloatingAnywhere:
                    case InitialWindowLocation.FloatingOnlyAnywhere:
                    case InitialWindowLocation.FloatingAtCursor:
                    case InitialWindowLocation.FloatingOnlyAtCursor:
                    case InitialWindowLocation.DockInNewTab:
                        location = (InitialLocation)externalApplication.WindowLocation;
                        break;
                    case InitialWindowLocation.DockLeft:
                    case InitialWindowLocation.DockTop:
                    case InitialWindowLocation.DockRight:
                    case InitialWindowLocation.DockBottom:
                        if (manager is LauncherBarChromeManager)
                        {
                            location = InitialLocation.Floating; ;
                        }
                        else
                        {
                            location = InitialLocation.DockInActiveTab | (InitialLocation)externalApplication.WindowLocation;
                        }
                        break;
                    default:
                        location = InitialLocation.Floating;
                        break;
                }
                var initParameter = new InitialShowWindowButtonParameters()
                    {
                        Text = copy.Title,
                        Image = copy.InvokerIcon == null ? null : copy.InvokerIcon.ToImageSource(),
                        WindowFactoryID = externalApplication.InvokerId,
                        InitialParameters = new InitialProcessBoundWindowParameters()
                            {
                                ProcessExe =PathUtilities.ReplaceEnvironmentVariables(externalApplication.ExecutableName),
                                ProcessArgs = PathUtilities.ReplaceEnvironmentVariables(externalApplication.Arguments),
                                WorkingDirectory = PathUtilities.ReplaceEnvironmentVariables(externalApplication.Arguments),
                                WindowTitleSubstring = externalApplication.WindowTitleSubstring,
                                InitialLocation = location,
                                KillOnClose = externalApplication.KillOnClose,
                                StartupTimeoutInSeconds = externalApplication.StartupTimeoutInSeconds
                            }
                    };
                if (!string.IsNullOrEmpty(externalApplication.InvokerPlacementLocation))
                {
                    manager.PlaceWidget(externalApplication.InvokerId, externalApplication.InvokerPlacementLocation, initParameter);
                }
                else
                {
                    manager.AddWidget(externalApplication.InvokerId, initParameter); //might be useful for centralized control placement
                }
                
            }
 
        }

        void application_WindowCaptured(object sender, WindowEventArgs e)
        {
            var externalApplication =
                externalApplications.Applications.FirstOrDefault(a_ => a_.InvokerId == e.ViewContainer.FactoryID);
            if (externalApplication != null)
            { 
                e.ViewContainer.Tooltip = externalApplication.ExecutableName +
                                        (string.IsNullOrEmpty(externalApplication.Arguments)
                                             ? string.Empty
                                             : " " + externalApplication.Arguments);  
            }
        } 
    } 

}

namespace MSDesktop.Wormhole
{
    public static class WormholeExtensions2
    {
        public static void LoadExternalApplications(this Framework framework_, ExternalApplications externalApplications_)
        {
            if (externalApplications_ == null || externalApplications_.Applications.Count == 0) return;
            framework_.EnableWormhole(); 
            framework_.Bootstrapper.ConfiguringContainer += (sender_, args_) => args_.Container.RegisterInstance(externalApplications_);
            framework_.AddModule<ExternalApplicationsConfigurationModule>();

        }
    }
}
