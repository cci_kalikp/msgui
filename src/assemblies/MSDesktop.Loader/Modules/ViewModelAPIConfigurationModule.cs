﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Media;
using System.Xml.Linq;
using System.Xml.Serialization; 
using MSDesktop.ViewModelAPI;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Modules;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl; 
using MorganStanley.MSDotNet.My;

namespace MorganStanley.Desktop.Loader.Modules
{
    public class ViewModelAPIConfigurationModule:ViewModelBasedModule, IMSDesktopModule
    {  
        private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger<ViewModelAPIConfigurationModule>();
        private readonly  ViewModelBasedButtons buttonDefinitions;
        private readonly IChromeRegistry chromeRegistry;
        public ViewModelAPIConfigurationModule(IUnityContainer container_) : base(container_)
        {  
            buttonDefinitions = container_.Resolve<ViewModelBasedButtons>();
            chromeRegistry = container_.Resolve<IChromeRegistry>();
        }

        public override void Initialize()
        {
            foreach (var button in buttonDefinitions.Buttons)
            {
                AddButton(button);
            }
        }

        private void AddButton(ViewModelBasedButton buttonDef_)
        {
            if (buttonDef_.Singleton || buttonDef_.CheckEntitlement)
            {
                if (string.IsNullOrEmpty(buttonDef_.ID))
                {
                    logger.Error("ViewModelBasedButton.ID must be specified when Singleton or CheckEntitlement is true, fall back to use auto generated id and disable Singleton and CheckEntitlement");
                    buttonDef_.Singleton = buttonDef_.CheckEntitlement = false;
                }
            }

            if (string.IsNullOrEmpty(buttonDef_.ViewModelType))
            {
                logger.Error("ViewModelBasedButton.ViewModelType is not defined");
                return;
            }
            if (string.IsNullOrEmpty(buttonDef_.ViewType))
            {
                logger.Error("ViewModelBasedButton.ViewType is not defined");
                return; 
            }
            Type viewModelType = GetType(buttonDef_, buttonDef_.ViewModelType);
            if (viewModelType == null)
            {
                logger.Error("ViewModelBasedButton.ViewModelType: " + buttonDef_.ViewModelType + " is not valid");
                return;
            }

            Type viewType = GetType(buttonDef_, buttonDef_.ViewType);
            if (viewType == null)
            {
                logger.Error("ViewModelBasedButton.ViewType: " + buttonDef_.ViewType + " is not valid");
                return;
            }
            Type parameterType = null;
            if (!string.IsNullOrEmpty(buttonDef_.ViewParametersType))
            {
                parameterType = GetType(buttonDef_, buttonDef_.ViewParametersType);
            }
            if (parameterType == null)
            {
                parameterType = buttonDef_.ViewStatePersistable ? typeof(DefaultPersistableFrameworkViewParameters) : 
                    typeof (DefaultFrameworkViewParameters);
            }

            if (buttonDef_.CheckEntitlement)
            {
                chromeRegistry.CheckEntitlement(buttonDef_.ID);
            }
            ImageSource image = buttonDef_.Image == null ? null : buttonDef_.Image.ToImageSource();

            AddButton(viewModelType, viewType, parameterType, string.IsNullOrEmpty(buttonDef_.ID) ? "Button." + Guid.NewGuid() : buttonDef_.ID, 
                image, buttonDef_); 
        }

        private void AddButton(Type viewModelType_, Type viewType_, Type parameterType_, string viewId_, ImageSource buttonImage_, ViewModelBasedButton button_)
        {
            BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            MethodInfo method = typeof(ViewModelBasedModule).GetMethod("AddButton",
                                             flags, null, new[] { typeof(ButtonDefinition) }, null);

            method = method.MakeGenericMethod(viewType_, viewModelType_, parameterType_);
            method.Invoke(this, flags, null,
                           new object[]
                                  {
                                      new ButtonDefinition()
                                          {
                                              Group = button_.Group,
                                              Id = viewId_,
                                              Image = buttonImage_, 
                                              Singleton = button_.Singleton,
                                              Tab = button_.Tab,
                                              Text = button_.Text,
                                              Size = button_.Size == Configuration.Helpers.ButtonSize.Default ? 
                                              new ButtonSize?() : 
                                              (button_.Size == Configuration.Helpers.ButtonSize.Large ? ButtonSize.Large : ButtonSize.Small)  ,
                                              InitialViewLocation = button_.ViewLocation,
                                              InitialViewTab = button_.ViewTabName
                                          }
                                  }, null);

           
        }

        private static Type GetType(ViewModelBasedButton button_, string typeName_)
        {
            return TypeResolver.GetCustomizationType(button_.AssemblyFile, button_.AssemblyName, typeName_);
             
        }
    }

    public class DefaultFrameworkViewParameters: IFrameworkViewParameters
    {
        public int Version { get; set; }

        public IFrameworkViewParameters GetDefault()
        {
            return new DefaultFrameworkViewParameters {};
        }

        public XDocument Upgrade(int version_, XDocument xDocument_)
        {
            return xDocument_;
        } 
    }

    public class DefaultPersistableFrameworkViewParameters : DefaultFrameworkViewParameters, IPersistableFrameworkViewParameters
    {
         
    }
}


namespace MSDesktop.ViewModelAPI
{
    public static class ViewModelAPIExtensions
    {
        public static void LoadViewModelBasedButtons(this Framework framework_, ViewModelBasedButtons buttons_)
        {
            if (buttons_ == null || buttons_.Buttons.Count == 0) return;
            framework_.Bootstrapper.ConfiguringContainer += (sender_, args_) => args_.Container.RegisterInstance(buttons_); 
            framework_.AddModule<ViewModelAPIConfigurationModule>();
        }

        public static void LoadViewModelBasedButtons(this Framework framework_, string buttonDefinitionFile_)
        {
             
            using (var reader = new StreamReader(buttonDefinitionFile_))
            {
                var buttons =
                (ViewModelBasedButtons)
                new XmlSerializer(typeof(ViewModelBasedButtons)).Deserialize(reader);
                if (buttons != null)
                {
                    LoadViewModelBasedButtons(framework_, buttons);
                }
            }
        }
    }
}
