﻿using System.IO;
using System.Linq;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.LauncherBar;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.Desktop.Loader.Modules
{
    public class ChromeConfigurationModule : IMSDesktopModule
    {
        internal static string PlacementInfo = null;
        internal static bool Relock = true;

        readonly IChromeManager chromeManager;
        public static Framework Framework;
        private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger<ChromeConfigurationModule>();

        public ChromeConfigurationModule(IChromeManager chromeManager)
        {
            this.chromeManager = chromeManager;
        }

        #region IModule Members

        public void Initialize()
        {
           
            if (!File.Exists(PlacementInfo))
            {
                logger.Error("Placement file:" + PlacementInfo + " doesn't exist.");
                return;
            }
            ChromeConfiguration config = null;
            using (var reader = new StreamReader(PlacementInfo))
            {
                config =
                    (ChromeConfiguration)
                    new XmlSerializer(typeof(ChromeConfiguration)).Deserialize(reader); 
            }
            if (config == null) return;

            ChromeManagerBase.LockerBlackHoleEnabled = false;
            PlacementInfo currentConfig =
                config.Placements.FirstOrDefault(p_ => p_.ShellMode == (Configuration.Helpers.ShellMode)Framework.InitialSettings.ShellMode); 

            if (currentConfig != null)
            {
                if (currentConfig.HasRibbon)
                {
                    LoadRibbon(currentConfig.Ribbon);
                }
                if (currentConfig.HasLauncherbar)
                {
                    LoadLauncherbar(currentConfig.Launcherbar);
                }
                if (currentConfig.HasTabwell)
                {
                    LoadTabwell(currentConfig.Tabwell);
                }
                if (currentConfig.HasStatusbar)
                {
                    LoadStatusbar(currentConfig.Statusbar);
                }
                if (currentConfig.HasSystemTray)
                {
                    LoadSystemTray(currentConfig.SystemTray);
                }
 
            }
            if (Relock)
                ChromeManagerBase.LockerBlackHoleEnabled = true;
        }

        private void LoadButton(IWidgetViewContainer parent_, Widget widget_)
        {
            if (string.IsNullOrEmpty(widget_.Id)) return;
            InitialWidgetParameters parameters = null;
            if (widget_.Size != Configuration.Helpers.ButtonSize.Default)
            {
                parameters = new InitialButtonParametersBase() { Size = (ButtonSize)widget_.Size};
            }
            parent_.AddWidget(widget_.Id, parameters);
        }

        private void LoadLauncherbar(Launcherbar launcherbar_)
        {
            var launcherbar = this.chromeManager.Launcherbar();
            if (launcherbar_.Widgets != null && launcherbar_.Widgets.Count > 0)
            {
                foreach (var widget in launcherbar_.Widgets)
                {
                    LoadButton(launcherbar, widget);
                }
            }
            if (launcherbar_.Groups != null && launcherbar_.Groups.Count > 0)
            {
                foreach (var @group in launcherbar_.Groups)
                {
                    if (string.IsNullOrEmpty(group.Id)) continue;
                    var groupContainer = launcherbar.AddWidget("group." + group.Id, new InitialLauncherGroupParameters() { Text = group.Id });
                    if (group.Widgets != null)
                    {
                        foreach (var widget in group.Widgets)
                        {
                            LoadButton(groupContainer, widget);
                        }
                    } 
                }
            } 
        }

        private void LoadSystemTray(SystemTray systemTray_)
        {
            if (systemTray_.Widgets != null && systemTray_.Widgets.Count > 0)
            {
                var systemTray = this.chromeManager.SystemTray();
                foreach (var widget in systemTray_.Widgets)
                { 
                    if (string.IsNullOrEmpty(widget.Id)) continue;
                    systemTray.AddWidget(widget.Id, null);
                }
            } 
        }

        private void LoadStatusbar(Statusbar statusbar_)
        {
            var statusBar = this.chromeManager.Statusbar(); 

            if (statusbar_.LeftSide != null)
            {
                foreach (var widget in statusbar_.LeftSide.Widgets)
                {
                    if (string.IsNullOrEmpty(widget.Id)) continue;
                    statusBar.LeftSide.AddWidget(widget.Id, null);
                }
            }

            if (statusbar_.RightSide != null)
            {
                foreach (var widget in statusbar_.RightSide.Widgets)
                {
                    if (string.IsNullOrEmpty(widget.Id)) continue;
                    statusBar.RightSide.AddWidget(widget.Id, null);
                }
            }
        }

        private void LoadTabwell(Tabwell tabwell_)
        {
            var tabwell = this.chromeManager.Tabs();
            if (tabwell_.LeftEdge != null)
            {
                foreach (var widget in tabwell_.LeftEdge.Widgets)
                {
                    if (string.IsNullOrEmpty(widget.Id)) continue;
                    tabwell.LeftEdge.AddWidget(widget.Id, null);
                }
            }

            if (tabwell_.RightEdge != null)
            {
                foreach (var widget in tabwell_.RightEdge.Widgets)
                {
                    if (string.IsNullOrEmpty(widget.Id)) continue;
                    tabwell.RightEdge.AddWidget(widget.Id, null);
                }
            } 
        }
	
        private void LoadRibbon(Ribbon ribbon_)
        {
            if (ribbon_.Tabs != null && ribbon_.Tabs.Count > 0)
            {
                var ribbon = this.chromeManager.Ribbon();
                foreach (var tab in ribbon_.Tabs)
                {
                    if (string.IsNullOrEmpty(tab.Id)) continue;
                    var tabContainer = ribbon.AddTab(tab.Id);
                    if (tab.Groups != null && tab.Groups.Count > 0)
                    {
                        foreach (var @group in tab.Groups)
                        {
                            if (string.IsNullOrEmpty(group.Id)) continue;
                            var groupContainer = tabContainer.AddGroup(group.Id);
                            if (group.Widgets != null && group.Widgets.Count > 0)
                            {
                                foreach (var widget in group.Widgets)
                                {
                                    LoadButton(groupContainer, widget);
                                }
                            }
                        }
                    }
                }
            } 
        }

        #endregion
    }
}

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    public static class FrameworkExtensions
    {
        public static void EnableCentralizedPlacement(this Framework f, string placementInfo, bool relock)
        {
            f.ForceLockerRegistrations(true);
            global::MorganStanley.Desktop.Loader.Modules.ChromeConfigurationModule.PlacementInfo = placementInfo;
            global::MorganStanley.Desktop.Loader.Modules.ChromeConfigurationModule.Relock = relock;
            global::MorganStanley.Desktop.Loader.Modules.ChromeConfigurationModule.Framework = f;
            f.AddModule<global::MorganStanley.Desktop.Loader.Modules.ChromeConfigurationModule>();
        }
    }
}

