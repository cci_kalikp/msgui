﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.ComponentEntitlements;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.Desktop.Loader.Modules
{
    public static class ComponentEntitlementExtension
    {
        public static void CheckEntitlement(this IChromeRegistry registry_, string invokerId_)
        {
            registry_.RegisterWidgetFactory(invokerId_).CheckEntitlement();
        }
    }
}
