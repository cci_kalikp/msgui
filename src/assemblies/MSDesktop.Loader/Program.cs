﻿using System;
using System.Collections.Generic;
using System.Linq; 
using MorganStanley.MSDotNet.Runtime; 
using MorganStanley.Desktop.Loader.Configuration;
using System.IO;
using System.Reflection;
using MorganStanley.Desktop.Loader.AssemblyResolver; 
using System.Configuration;
using System.Security;
using System.Security.Permissions;

namespace MorganStanley.Desktop.Loader
{
	class Program
	{
		#region Constants
		
		private const string COMMANDLINE_SWITCH_PREFIX_1 = "-MSDesktop:";
		private const string COMMANDLINE_SWITCH_PREFIX_2 = "/MSDesktop:";

		private const string DEBUG = "debug";
		private const string WRITE_DEFAULT_CONFIG = "writedefaultconfig"; 
		private const string APP_DOMAIN = "appdomain";
		private const string MSDE_CONFIG = "MsdeConfig";
		private static readonly string RESOLVETIMEEXCEPTIONBEHAVIOUR = "ResolveTimeExceptionBehaviour";
		private static readonly string NEVERTHROW = "neverthrow";

		#endregion

		internal static bool LaunchDebugger = false;



		[MSDEMain]
		static void MSDEMain(string[] args_)
		{
			StartUp.Start();
		}

		[STAThread]
		static void Main(string[] args_)
		{
			var args = ProcessCommandLine(args_);

			if (args.Keys.Contains(DEBUG))
			{
				LaunchDebugger = true;
			}

			if (args.Keys.Contains(WRITE_DEFAULT_CONFIG))
			{
				EmptyConfiguration.GenerateExampleConfig();
				return;
			}

			if (args.Keys.Contains(APP_DOMAIN) && AppDomain.CurrentDomain.SetupInformation.ApplicationName != args[APP_DOMAIN][0])
			{
				CreateAppDomain(args[APP_DOMAIN][0], args_);
			}
			else
			{
				Launch(args_, args);
			}
		}

		private static void CreateAppDomain(string domainName_, string[] args_)
		{
			AppDomainSetup setup = new AppDomainSetup
			{
				ApplicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase,
				ConfigurationFile = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile,
				ApplicationName = domainName_
			};


			var domain = AppDomain.CreateDomain(domainName_, null, setup, new PermissionSet(PermissionState.Unrestricted));
			domain.ExecuteAssembly(Assembly.GetExecutingAssembly().Location, args_);
		}

		private static void Launch(string[] args_, Dictionary<string, List<string>> argsProcessed_)
		{
			string msdeConfig = null;

			if (argsProcessed_.Keys.Contains(MSDE_CONFIG))
			{
				msdeConfig = argsProcessed_[MSDE_CONFIG][0];
			}
			else if (ConfigurationManager.AppSettings.AllKeys.Contains(MSDE_CONFIG))
			{
				msdeConfig = ConfigurationManager.AppSettings[MSDE_CONFIG];
			}
			
			if (msdeConfig != null)
			{
				if (!Path.IsPathRooted(msdeConfig))
				{
					msdeConfig = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, msdeConfig);
				}

				if (ConfigurationManager.AppSettings.AllKeys.Contains(RESOLVETIMEEXCEPTIONBEHAVIOUR))
				{
					MSDotNet.Runtime.AssemblyResolver.ResolveTimeExceptionBehaviour =
						ConfigurationManager.AppSettings[RESOLVETIMEEXCEPTIONBEHAVIOUR].ToLowerInvariant() == NEVERTHROW
							? ResolveTimeExceptionBehaviourEnum.NeverThrow
							: ResolveTimeExceptionBehaviourEnum.ThrowIfDefinedAndCantLoad;
				}
				AssemblyResolverLoader.StartViaAssemblyResolver(args_, msdeConfig);
			}
			else
			{
				MSDEMain(args_);
			}
		}

		private static Dictionary<string, List<string>> ProcessCommandLine(string[] args_)
		{
			var result = new Dictionary<string, List<string>>();
			if (args_.Length > 0)
			{
				for (int i = 0; i < args_.Length; i++)
				{
					var arg = args_[i].ToLower();
					if (arg.StartsWith(COMMANDLINE_SWITCH_PREFIX_1) || arg.StartsWith(COMMANDLINE_SWITCH_PREFIX_2))
					{
						var cmd = arg.Substring(11);
						switch (cmd)
						{
							// parameterless commands
							case DEBUG:
							case WRITE_DEFAULT_CONFIG:
                                result.Add(cmd, null);
								break;

							// commands with 1 parameter
							case APP_DOMAIN:
							case MSDE_CONFIG:
								result.Add(cmd, new List<string>() { args_[++i] });
								break;
						}
					}
				}
			}
			return result;
		}
	}
}