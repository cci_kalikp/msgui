﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Themes;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Xml.XPath;
using System.Xml.Serialization;
using System.Xml;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Configuration.Bridges;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using Microsoft.Practices.Composite.Modularity;
using System.IO;
using MorganStanley.Ecdev.EngineGui.Misc.SyncFusionLicense;

namespace MorganStanley.Desktop.Loader
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);
            StartUp.Start();
		}
	}

}