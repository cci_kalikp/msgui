﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq; 
using System.Text;
using System.Xml.Serialization; 
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.SmartApi;

namespace MorganStanley.Desktop.Loader
{
	public static class StartUp
	{
        public static void Start(Framework framework_, MSDesktopConfiguration config_)
        {
            framework_.EnableSmartApi();
            ((IApplyableConfiguration)config_).GetApplier().Apply(config_, framework_); 
            framework_.Start();
        }

        public static bool Start(Stream configReader_)
        {
            IApplyableConfiguration config = null;
            try
            {
                var ser = new XmlSerializer(typeof (MSDesktopConfiguration));
                config = (IApplyableConfiguration) ser.Deserialize(configReader_);

            }
            catch (Exception ex)
            {
                throw new Exception("Failed to load configuration. See inner exception for details.", ex);
            }
            finally
            {
                configReader_.Dispose();
            }
            if (config == null) return false;
            Extensions.EnableSmartApi();
            config.Resolve((MSDesktopConfiguration)config);
            config.GetAssemblyRequester().RequestAssemblies(config);
            var framework = new Framework();
            config.GetApplier().Apply(config, framework); 
            if (Program.LaunchDebugger)
            {
                Debugger.Launch();
            }
            framework.Start();
            return true;
        }

		public static bool Start()
		{
		    var stream = GetConfigStream();
            if (stream != null)
            {
                return Start(stream);
            }
		    return false;
		}

         
        public const string MSDESKTOP_CONFIG_ARG1 = "/" + MSDesktopConfiguration.MSDESKTOP_CONFIG + ":";
        public const string MSDESKTOP_CONFIG_ARG2 = "/" + MSDesktopConfiguration.MSDESKTOP_CONFIG + "=";
        public const string MSDESKTOP_CONFIG_ARG3 = "-" + MSDesktopConfiguration.MSDESKTOP_CONFIG + ":";
        public const string MSDESKTOP_CONFIG_ARG4 = "-" + MSDesktopConfiguration.MSDESKTOP_CONFIG + "=";
        private static Stream GetConfigStream()
        {
            string configFile = null;
            string args = System.Environment.GetCommandLineArgs()
                                      .FirstOrDefault(
                                          arg_ => arg_.StartsWith(MSDESKTOP_CONFIG_ARG1, StringComparison.CurrentCultureIgnoreCase) ||
                                                  arg_.StartsWith(MSDESKTOP_CONFIG_ARG2, StringComparison.CurrentCultureIgnoreCase) || 
                                                  arg_.StartsWith(MSDESKTOP_CONFIG_ARG3, StringComparison.CurrentCultureIgnoreCase) ||
                                                  arg_.StartsWith(MSDESKTOP_CONFIG_ARG4, StringComparison.CurrentCultureIgnoreCase));
            if (args != null)
            {
                configFile = args.Substring(MSDESKTOP_CONFIG_ARG1.Length);
            }
            if (string.IsNullOrEmpty(configFile) && ConfigurationManager.AppSettings.AllKeys.Contains(MSDesktopConfiguration.MSDESKTOP_CONFIG))
            {
                configFile = ConfigurationManager.AppSettings[MSDesktopConfiguration.MSDESKTOP_CONFIG];
            }

            if (!string.IsNullOrEmpty(configFile))
            {
                if (!configFile.EndsWith(MSDesktopConfiguration.MSDESKTOP_CONFIG_SUFFIX))
                {
                    configFile += MSDesktopConfiguration.MSDESKTOP_CONFIG_SUFFIX;
                }
                configFile = PathUtilities.GetAbsolutePath(configFile); 
                if (!File.Exists(configFile))
                {
                    return null;
                }
                return new FileStream(configFile, FileMode.Open, FileAccess.Read);
            }
             
            System.Configuration.Configuration exeConfiguration =
                ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            var config = exeConfiguration.GetSection("MSDesktop");
            if (config == null) return null;
            string configXml = config.SectionInformation.GetRawXml();
            if (string.IsNullOrEmpty(configXml)) return null;
            return new MemoryStream(Encoding.UTF8.GetBytes(configXml)); 
        }
 
	}
}


