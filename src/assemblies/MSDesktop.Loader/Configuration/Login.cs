﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration
{
    public class Login : ComplexConfigurationItem, IApplyableConfiguration, INullableConfigurationItem
    {
        public Login()
        {
            AllowImpersonation = true;
        }

        [DefaultValue(true)]
        [PropertyDescription(
Description = "Is impersonation allowed when login?")]
        public bool AllowImpersonation { get; set; } 

        [PropertyDescription(
Description = "If true, adds a default profile to user & profile login for debugging purpose")]
        public bool AddDefaultProfileToAdvancedLoginScenario { get; set; }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }

        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new LoginApplier();
        }

        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }

        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        { 
        }
        public void InitializeNullObject(object parentObject_)
        {
             
        }
    }
}
