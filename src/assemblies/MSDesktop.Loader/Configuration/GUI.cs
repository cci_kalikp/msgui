﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure;  
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using System.Windows.Input; 

namespace MorganStanley.Desktop.Loader.Configuration
{
    public class GUI : ComplexConfigurationItem, IApplyableConfiguration
	{
        public GUI()
        {
            EnforcedSizeRestrictions = true;
            ViewRenameKeyGesture = new KeyGestureHelper(Key.F2, ModifierKeys.Alt);
            LauncherBarExpandable = true;
            TabGroupTabStripPlacement = Dock.Bottom; 
            RenameTabEnabled = true;
            TaskbarPinEnabled = true;
            AcquireFocusAfterStartup = true;
            RibbonCleaningEnabled = true;
        }

        [PropertyDescription(
Description = "Reads the application theme from MSDesktopTheme.config, if not defined, uses the Theme defined statically")]
        [Order(0)]
        [BindableEnablement("ModernTheme.Enabled", typeof(InverseBoolConverter), "disable ModernTheme")]
        [BindableValue("ModernTheme.Enabled", typeof(AutoUncheckConverter))]
        public bool ConfigurationDependentTheme { get; set; }
         
        [PropertyDescription(
Description = "Sets the application theme statically",
SnapShots = new string[]{SnapShots.SimpleTheme, SnapShots.BlackTheme, SnapShots.BlueTheme, SnapShots.WhiteTheme})]
        [Order(1)]
        [BindableEnablement("ModernTheme.Enabled", typeof(InverseBoolConverter), "disable ModernTheme")]
        [BindableValue("ModernTheme.Enabled", typeof(AutoThemeValueWhenEnabledConverter))]
        public Theme Theme { get; set; }

        [PropertyDescription(Description = "Sets the application modern theme statically", SnapShots = new string[]{SnapShots.ModernThemeLight})]
        [Order(2)] 
        public ModernThemeHelper ModernTheme { get; set; }

        [PropertyDescription(
            Description = "Selects the shell mode",
            CodeWiki = "Shells/Settings_Howto_Set_Shell_Modes",
            SnapShots = new string[]{SnapShots.RibbonWindow, SnapShots.LauncherBarAndFloatingWindows,
            SnapShots.LauncherBarAndWindow, SnapShots.RibbonAndFloatingWindows})]
        [Order(3)]
        public ShellMode ShellMode { get; set; }

        [PropertyDescription(
    Description = "Uses homemade Harmonia instead of Infragistics as the dock manager",
    SnapShots=new string[]{"InfragisticsDock", "HarmoniaDock"})]
        [BindableEnablement("ShellMode", typeof(ShellModeToStrictLauncherBarEnablementConverter), "sets {0} to \"LauncherBarAndFloatingWindows\"")]
        [Order(4)]
        public bool UseHarmoniaDockManager { get; set; }
         
        [PropertyDescription(
            Description = "Sets the orb icon on ribbon window/launcher bar",
            CodeWiki = "Shells/Settings_Howto_Set_Ribbon_Icon", 
            SnapShots = new string[] { SnapShots.RibbonWindow, SnapShots.LauncherBarAndFloatingWindows },
            SnapShotPart = "RibbonIcon")]
        public ImageHelper RibbonIcon { get; set; }

        [DefaultValue(true)]
        [PropertyDescription(
      Description = "If true, windows can't be larger than a single screen",
      CodeWiki = "Chrome/Windows/Windows_parameters")]
        public bool EnforcedSizeRestrictions { get; set; } //supported in harmonia

        [PropertyDescription(
Description = "Place controls according to definition in placement file specified",
            SnapShots = new string[] { SnapShots.RibbonWindow, SnapShots.LauncherBarAndFloatingWindows },
            SnapShotPart = "ToolBar")]
        public CentralizedPlacementHelper CentralizedPlacement { get; set; }

        [PropertyDescription(
            Description = "If true, enables non-owned floating windows in the XamDockManager instances (non-owned windows can appear behind their parent window).")]
        public bool NonOwnedWindows { get; set; } //supported in harmonia
         
        [PropertyDescription(
            Description = "If true, restricts pane closing to the X button in the pane header. All other methods, for example Alt+F4, right-click menu on the task bar won't work.")]
        public bool RestrictWindowClosingToHeaderButton { get; set; }  //supported in harmonia

        [PropertyDescription(Description = "Sets the rename gesture for view headings.",
            SnapShots = new string[]{SnapShots.FloatingWindow},
            SnapShotPart = "Title")]
        [XmlElement(Type=typeof(string))]
        public KeyGestureHelper ViewRenameKeyGesture { get; set; }  //supported in harmonia

        [PropertyDescription(
            Description = "If false, the application will NOT try to acquire user input focus after startup is complete",
            CodeWiki = "Application/Settings_Howto_Acquire_Focus_After_Startup")]
        public bool AcquireFocusAfterStartup { get; set; }

        [PropertyDescription(
            Description = "Locks/hides some shell and window parts")]
        public HideOrLockParameters HideOrLockParameters { get; set; }  //supported in harmonia

        [PropertyDescription(Description = "If true, enables workspace-like behavior for floating views. Floating views will be tied to the active tab at their creation and be visible only when that tab is active.")]
        public bool TabsOwnFloatingWindowsEnabled { get; set; } 

        [PropertyDescription(Description = "If true, the topmost button is visible for floating views.",
            SnapShots = new string[] { SnapShots.FloatingWindow },
            SnapShotPart = "TopmostButton")]
        public bool TopmostFloatingPanesEnabled { get; set; }  //supported in harmonia


        [PropertyDescription(
            Description = "Panes in ribbon window can be docked together to form a group so that the user can switch between them with tabs. The position of these tabs is by default at the bottom of the pane group. However, this can be changed with this setting.",
            CodeWiki = "Application/Settings_Howto_Set_Position_Grouped_Panes_Tab_Strip",
            SnapShots = new string[]{SnapShots.RibbonWindow},
            SnapShotPart = "TabStrip")]
        [DefaultValue(Dock.Bottom)]
        public Dock TabGroupTabStripPlacement { get; set; } //todo: support in harmonia ???

        [PropertyDescription(
       Description = "If true, enables drag and drop behavior of tabs when using a shell with tabbed dock",
       CodeWiki = "Application/Settings_Howto_Enable_Tabs_Drag_And_Drop",
       SnapShots = new string[] { SnapShots.RibbonWindow4 },
       SnapShotPart = "DragAndDrop")]
        public bool TabDragAndDropEnabled { get; set; }
          
        [PropertyDescription(
            Description = "If not disabled, a new item in the application menu is available for configuring the widget chrome globally/specific to current layout",
            CodeWiki = "Chrome/Widgets/Configuration",
            SnapShots = new string[] { SnapShots.ApplicationMenu},
            SnapShotPart = "RibbonConfig"
            )]
        [DefaultValue(WidgetConfigEnablement.Disabled)]
        public WidgetConfigEnablement WidgetUserConfigEnabled { get; set; }

        [PropertyDescription(
       Description = "If true, reduces views' margins, paddings, borders and shadows to minimum in order to save real estate, also removes background gradients",
       CodeWiki = "Chrome/Widgets/Configuration",
       SnapShots = new string[] { SnapShots.FloatingWindow },
       SnapShotPart = "Border")]
        public bool FlatModeEnabled { get; set; } //supported in harmonia

        [PropertyDescription(
            Description = "By default, both document pane tab items and regular pane tab items have a uniform look. The switch allows you to restore the original Infragistics look for document tabs.",
       SnapShots = new string[] { SnapShots.RibbonWindow })]
        public bool NonuniformLookForDocumentTabsEnabled { get; set; }

        [PropertyDescription(
      Description = "If true, uses the classic look and feel for task dialog.",
      SnapShots = new string[]{SnapShots.TaskDialog, SnapShots.ClassicTaskDialog})]
        public bool UseClassicThemeForTaskDialog { get; set; }
         
        [PropertyDescription(
            Description = "If not None, hides main ribbon window or launcher, or moves it beyond screen, or never even never show it. Application should either have a provided default layout having floating windows or using ITrayIconViewContainer",
            SnapShots = new string[] { SnapShots.RibbonWindow, SnapShots.LauncherBarAndFloatingWindows },
            SnapShotPart = "MainWindow")]
        public InvisibleMainControllerModeEnum InvisibleMainControllerMode { get; set; }

        [PropertyDescription(
      Description = "If not None, enables automatically removing of empty tabs when loading layout or/and last child tab is closed",
          SnapShots = new string[] { SnapShots.RibbonWindow},
          SnapShotPart = "Tabwell")]
        [BindableEnablement("ShellMode", typeof(ShellModeToStrictRibbonWindowEnablementConverter), "sets {0} to \"RibbonWindow\"")]
        public RemoveEmptyTabsWhen RemoveEmptyTabs { get; set; }

        [PropertyDescription(
 Description = "If disabled, the rename menu item will be removed from the tab context menu",
          SnapShots = new string[] { SnapShots.RibbonWindow2 },
          SnapShotPart = "RenameMenuItem")] 
        [DefaultValue(true)]
        public bool RenameTabEnabled { get; set; }


        [PropertyDescription(
Description = "If enabled, hides header of views docked as tab item inside a tabwell to give a more compact layout",
     SnapShots = new string[] { SnapShots.RibbonWindow, SnapShots.RibbonWindow9 },
     SnapShotPart = "MainArea")]
        [DefaultValue(false)]
        public bool CompactTabGroupsEnabled { get; set; }
         
        [PropertyDescription(Description =
            "If enabled, tabbed panes created with InitialLocationTarget==null will be placed inside a DocumentContentHost which always resides in the center of the layout. Other docked panes will be located relative to the DocumentContentHost",
            SnapShots = new string[]{SnapShots.RibbonWindow3},
            SnapShotPart = "ContentHost")]
        public bool ContentHostModeEnabled { get; set; }

        [PropertyDescription(
            Description = "If disabled, the ability of a shortcut or window to be pinned to the task bar or the Start menu is disabled")]
        [DefaultValue(true)]
        public bool TaskbarPinEnabled { get; set; }

        [PropertyDescription(
        Description = "If true, the header items added to the view would appear before title",
        SnapShots = new string[]{SnapShots.FloatingWindow2},
        SnapShotPart = "HeaderItemStyle")]
        public bool HeaderItemPositionBeforeTitle { get; set; } //todo: support in harmonia ??

        [PropertyDescription(
Description = "If enabled, shows tooltip of view docked as tab item when hovering on the tab item",
     SnapShots = new string[] { SnapShots.RibbonWindow9},
     SnapShotPart = "TabItemTooltip")]
        [DefaultValue(false)]
        public bool SubTabHeaderTooltipsEnabled { get; set; } 

        [PropertyDescription(
            Description = "Override the default height for view header",
            Example = "22.0",
            SnapShots = new string[]{SnapShots.FloatingWindow},
            SnapShotPart = "Header")]
        [XmlElement(IsNullable = true)]
        public double? ViewHeaderHeight { get; set; } //supported in harmonia 
         
        [PropertyDescription(Description = "If false, the launcher bar is not expandable through double click or clicking the expander.",
            SnapShots = new string[] { SnapShots.LauncherBarAndFloatingWindows },
            SnapShotPart = "BarExpander")]
        [DefaultValue(true)]
        [BindableEnablement("ShellMode", typeof(ShellModeToLauncherBarEnablementConverter), "sets {0} to \"LauncherBarAndFloatingWindows\" or \"LauncherBarAndWindow\"")]
        public bool LauncherBarExpandable { get; set; }

        [PropertyDescription(
            Description = "If hidden, the user name won't appear in the launcher bar giving the layout name more space.",
            SnapShots = new string[] { SnapShots.LauncherBarWithUserName, SnapShots.LauncherBarNoUserName },
            SnapShotPart = "Info")]
        [DefaultValue(false)]
        [BindableEnablement("ShellMode", typeof(ShellModeToStrictLauncherBarEnablementConverter), "sets {0} to \"LauncherBarAndFloatingWindows\"")]
        public bool HideUserNameOnLauncherBar { get; set; }

        [PropertyDescription(
           Description = "If hidden, the layout name won't appear in the launcher bar, this is usually used for those application which doesn't allow user to change layout.",
           SnapShots = new string[] { SnapShots.LauncherBarWithUserName, SnapShots.LauncherBarNoLayoutName },
           SnapShotPart = "LayoutName")]
        [DefaultValue(false)]
        [BindableEnablement("ShellMode", typeof(ShellModeToStrictLauncherBarEnablementConverter), "sets {0} to \"LauncherBarAndFloatingWindows\"")]
        public bool HideLayoutNameOnLauncherBar { get; set; }

        [PropertyDescription(
           Description = "Override the default height for launcher bar when shrinked",
           Example = "50",
           SnapShots = new string[] { SnapShots.LauncherBarAndFloatingWindows },
           SnapShotPart = "MainWindow")]
        [XmlElement(IsNullable = true)]
        [BindableEnablement("ShellMode", typeof(ShellModeToLauncherBarEnablementConverter), "sets {0} to \"LauncherBarAndFloatingWindows\" or \"LauncherBarAndWindow\"")]
        public int? LauncherBarShrinkedHeight { get; set; }

        [PropertyDescription(
      Description = "Override the default height for launcher bar when expanded",
      Example = "150",
      SnapShots = new string[] { SnapShots.LauncherBarExpanded },
      SnapShotPart = "Expander")]
        [XmlElement(IsNullable = true)]
        [BindableEnablement("ShellMode", typeof(ShellModeToLauncherBarEnablementConverter), "sets {0} to \"LauncherBarAndFloatingWindows\" or \"LauncherBarAndWindow\"")]
        public int? LauncherBarExpandedHeight { get; set; }

        [PropertyDescription(
        Description = "Override the height of launcher bar button when launcher bar is expanded",
        Example = "44.0",
        SnapShots = new string[] { SnapShots.LauncherBarExpanded },
        SnapShotPart = "Button")]
        [XmlElement(IsNullable = true)]
        [BindableEnablement("ShellMode", typeof(ShellModeToLauncherBarEnablementConverter), "sets {0} to \"LauncherBarAndFloatingWindows\" or \"LauncherBarWindows\"")]
        public double? LauncherBarButtonMaxHeight { get; set; }

        [PropertyDescription(
    Description = "If true, creates a minimize button on the rightmost of the launcher bar", 
    SnapShots = new string[] { SnapShots.LauncherBar },
    SnapShotPart = "MinimizeButton")]
        [BindableEnablement("ShellMode", typeof(ShellModeToStrictLauncherBarEnablementConverter), "sets {0} to \"LauncherBarAndFloatingWindows\"")]
        public bool OneClickMinimizeLauncherBarEnabled { get; set; }

        [PropertyDescription(
Description = "If true, user could resize the launcher bar horizontally, take to make it narrower",
SnapShots = new string[] { SnapShots.LauncherBarNarrowed })]
        [BindableEnablement("ShellMode", typeof(ShellModeToStrictLauncherBarEnablementConverter), "sets {0} to \"LauncherBarAndFloatingWindows\"")]
        public bool LauncherBarHorizontalResizeEnabled { get; set; }

        [PropertyDescription(
Description = "If disabled, horizontal scrollbar won't appear in the launcher bar even if it's not wide enough to show all content",
SnapShots = new string[] { SnapShots.LauncherBarNarrowed },
SnapShotPart = "ScrollBar")]
        [BindableEnablement("ShellMode", typeof(ShellModeToLauncherBarEnablementConverter), "sets {0} to \"LauncherBarAndFloatingWindows\" or \"LauncherBarWindows\"")]
        public bool LauncherBarHorizontalAutoScrollDisabled { get; set; }

        [PropertyDescription(
        Description = "Override offset of the view shadow",
        Example = "6.0",
        SnapShots = new string[] { SnapShots.RibbonWindow3 },
        SnapShotPart = "Shadow")]
        [XmlElement(IsNullable = true)]
        public double? ViewShadowSize { get; set; } //supported in harmonia

        [PropertyDescription(
        Description = "Override margins between split views",
        Example = "4.0",
        SnapShots = new string[] { SnapShots.RibbonWindow3 },
        SnapShotPart = "Splitter")]
        [XmlElement(IsNullable = true)]
        public double? ViewGapSize { get; set; } //supported in harmonia

        [PropertyDescription(
        Description = "Override padding of the content in view",
        Example = "5.0",
        SnapShots = new string[] { SnapShots.RibbonWindow3 },
        SnapShotPart = "Padding")]
        [XmlElement(IsNullable = true)]
        public double? ViewContentPadding { get; set; } //supported in harmonia

        [PropertyDescription(
         Description = "Override height of status bar",
         Example = "25.8",
         SnapShots = new string[] { SnapShots.RibbonWindow },
         SnapShotPart = "StatusBar")]
        [XmlElement(IsNullable = true)]
        public double? StatusBarHeight { get; set; }

        [PropertyDescription(
        Description = "Override padding within status bar",
        Example = "3.0",
        SnapShots = new string[] { SnapShots.RibbonWindow },
        SnapShotPart = "StatusBarPadding")]
        [XmlElement(IsNullable = true)]
        public double? StatusBarPadding { get; set; }

        [PropertyDescription(
 Description = "It true, enables window deactivation notifications", 
 CodeWiki = "Application/Settings_Howto_Control_Window_Deactivation")]
        public bool WindowDeactivatingNotificationEnabled { get; set; } //todo: support in harmnia

        [PropertyDescription(
            Description = "If true, enables access to drop target events such as Drop, DragEnter, etc on shell elements",
            CodeWiki = "Application/Settings_Howto_Enable_Drop_Target_Events_On_Shell")]
        public bool DragDropTargetEventsOnShellEnabled { get; set; } //supported in harmonia

        [PropertyDescription(
Description = "If true, calls System.Windows.Forms.Application.SetCompatibleTextRenderingDefault with the specified value")]
        public bool CompatibleTextRendering { get; set; }

        [PropertyDescription(
     Description = "If true, all controls registered through IChromeRegistry will be placed even if no PlaceWidget or AddWidget calls have been made for some")]
        public bool LegacyControlPlacement { get; set; }

        [PropertyDescription(
            Description = "If true, call CloseQuietly instead of Close on existing windows when overriding the current layout")]
        public bool QuietCloseOfViews { get; set; } //supported in harmina, todo: needs to test events

        [PropertyDescription(
            Description = "If true, specifies the WPF render mode preference for the current process to software only")]
        public bool SoftwareRendering { get; set; }

        [PropertyDescription(
    Description = "If not None, all PlaceWidget calls will be altered and routed into ChromeArea.Locker. If set to MainAreaOnly, the altered placement to locker would only affect locker only for the ribbon, qat and launcher bar areas; If set to AllAreas, the the altered placement to locker would affect all chrome areas.")]
        public ForceLockerRegistrations ForceLockerRegistrations { get; set; }

        [PropertyDescription(
    Description = "If true, turns on the dock manager focus hack")]
        public bool FocusHackEnabled { get; set; }

        [PropertyDescription(
    Description = "It true, will pass the title of the owner tab of the current view as attribute \"Tab\" into the state passed to the InitializeWindowHandler.\r\nNote: this value might be overwritten and be different from the actual tab the view is docked into.")]
        public bool InjectContainingTabIntoLayoutEnabled { get; set; }

        [PropertyDescription(Description = "If disabled, the chrome manager will not remove empty ribbon groups")]
        [BindableEnablement("ShellMode", typeof(ShellModeToRibbonWindowEnablementConverter), "sets {0} to \"RibbonWindow\" or \"RibbonAndFloatingWindows\"")]
        public bool RibbonCleaningEnabled { get; set; }

        [PropertyDescription(Description = "By default, a floating window group's title is bound to the title of the active pane within this group.\r\nIf the separator is set here, it will be used to concatenate the list of contained panes to produce an aggregated window group's title.",
            Example = ",",
            SnapShots = new string[]{"FloatingWindow4", "FloatingWindow3"},
            SnapShotDescriptions = new string[]{"Not set", "Set to comma"},
            SnapShotPart = "Title")]
        public string AggregatedWindowGroupTitleSeparator { get; set; } //supported in harmonia

        [PropertyDescription(Description = "By default, header items of windows grouped into a floating window is hidden, only the header items of active pane is displayed on the floating window header.\r\nIf to true, header items would show on its own pane, and the floating window header wouldn't have any header item.",
            SnapShots = new string[] { "FloatingWindow4", "FloatingWindow3" },
            SnapShotPart = "HeaderButton",
            SnapShotPartForComparison = true)]
        public bool ShowSeparateHeaderItemsInFloatingWindow { get; set; }  //supported in harmonia

        [PropertyDescription(Description = "Sets the trimming strategy to apply when view's title doesn't fit into the header bar")]
        public bool ViewTitleTextTrimmingCharacterElipsisEnabled { get; set; }  //supported in harmonia

        [PropertyDescription(Description = "Sets the trimming strategy to apply when view's title doesn't fit into the header bar")]
        public bool ViewTitleTextTrimmingWordElipsisEnabled { get; set; }  //supported in harmonia

        public bool QATPlacementHackDisabled { get; set; }

        [PropertyDescription(Description = "If not \"None\", all windows created can stick to the boundary of each other or/and the boundary of the screen")]
        [DefaultValue(StickyWindowOptions.None)]
        public StickyWindowOptions StickinessForAllWindows { get; set; }  //supported in harmonia

        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new GUIApplier();
        }
        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }
        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
            
        }
        #endregion
         
    }
}
