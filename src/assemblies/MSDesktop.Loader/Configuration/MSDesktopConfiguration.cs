﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;  
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.AssemblyRequesters;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Entitlement;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;  
using MorganStanley.Desktop.Loader.Configuration.Helpers; 

namespace MorganStanley.Desktop.Loader.Configuration
{
	[XmlRoot("MSDesktop", Namespace="http://tempuri.org/XMLSchema.xsd")]
    public class MSDesktopConfiguration: IApplyableConfiguration
	{
        public const string MSDESKTOP_CONFIG = "msdesktopconfig";
        public const string MSDESKTOP_CONFIG_SUFFIX = "." + MSDESKTOP_CONFIG;

        [PropertyDescription(
            Description = "Sets Control.CheckForIllegalCrossThreadCalls to the specified value.")]
        public bool CheckForIllegalCrossThreadCalls { get; set; }

        [Category("Behavior")]
        [PropertyDescription(
            Description = "Configures the mailboxes separated by \";\" to receive unhandled exceptions",
            Example = "email1@ms.com;email2@ms.com",
            CodeWiki = "Application/Settings_Howto_Enable_Unhandled_Exception_Handler")]
        public string ExceptionHandler { get; set; }
          
        [Category("Module")]
        [PropertyDescription(
            Description = "It true, enables automatically loading of modules which implements \"IModule\" interface",
            CodeWiki = "Module/Reflection_Module_Load")]
        public bool ReflectionModuleLoad { get; set; }

        [Category("Module")]
        [PropertyDescription(
Description = "Makes current module to be loaded depends on all modules loaded before it during module loading")]
        public bool EnforceModuleLoadingDependency { get; set; }

        [Category("Module")]
        [PropertyDescription(
Description = "Prism instead of Composite assemblies is used to define modules")]
        public bool UsePrism { get; set; }
         
        [Category("Behavior")]
        [PropertyDescription(
Description = "Configures the layout persistence behavior")]
        public Login Login { get; set; }

        [Category("Appearance")]
        [PropertyDescription(
Description = "Specifies the application specific information",
CodeWiki = "Application")]
        public ApplicationInformation ApplicationInformation { get; set; }

        [Category("Layout")]
        [PropertyDescription(
Description = "Specifies the chrome appearance, layout and behavior",
CodeWiki = "Shells",
SnapShots = new string[]{SnapShots.RibbonWindow})]
        public GUI GUI { get; set; }

        [Category("Layout")]
        [PropertyDescription(
Description = "Configures the layout persistence behavior")]
        public LayoutAndPersistence LayoutAndPersistence { get; set; }

        [Category("Behavior")]
        [PropertyDescription(
Description = "Configures the behavior of quit dialog when user tries to close the application",
CodeWiki = "Application/Settings_Howto_Setup_Quit_Behavior",
SnapShots = new string[] { SnapShots.QuitDialog })]
        public QuitPromptDialogBehaviour QuitPromptDialogBehaviour { get; set; }

        [Category("Appearance")]
        [PropertyDescription(
Description = "Configures the appearance of splash screen",
SnapShots = new string[] { SnapShots.SplashScreen })]
        public Splash Splash { get; set; }

        [Category("Appearance")]
        [PropertyDescription(
Description = "Configures the appearance of application option pages",
CodeWiki = "Application/Creating_custom_settings_screen",
 SnapShots = new string[] { SnapShots.OptionsDialog })]
        public ApplicationOptions ApplicationOptions { get; set; }

        [Category("Behavior")]
        [PropertyDescription(
            Description ="Configures the behavior of exception dialog",
            CodeWiki = "Exceptions", 
            SnapShots = new string[] { SnapShots.ExceptionDialog })]
        public ExceptionDialogBehaviour ExceptionDialogBehaviour { get; set; }
         
        [PropertyDescription(
            Description= "Sets up log",
            CodeWiki = "Logs")]
        public LoggingParameters LoggingParameters { get; set; }

        [PropertyDescription(
     Description = "Configures the behavior of communication systems",
     CodeWiki = "Communications")]
        public Communication Communication { get; set; }

        [PropertyDescription(
Description = "Configures support for concord applications",
CodeWiki = "Bridges/Concord")]
        public global::MorganStanley.Desktop.Loader.Configuration.Bridges.Concord Concord { get; set; }

        [PropertyDescription(Description = "Configures the environment support",
            CodeWiki = "Environment/Introduction")]
        public Environment Environment { get; set; }

        [PropertyDescription(Description = "Configures module and component entitlement")]
        public EntitlementConfiguration Entitlement { get; set; }

        [Category("Module")]
        [PropertyDescription(Description = "Configures extra features to be enabled")]
        public AdvancedFeatures AdvancedFeatures { get; set; }

        [Category("Module")]
        [PropertyDescription(Description = "Configures MVVM based buttons to be added to chrome", CodeWiki = "ViewModelAPI/Introduction")]
        public ViewModelBasedButtons ViewModelBasedButtons { get; set; }

        [Category("Module")]
        [PropertyDescription(Description = "Specifies boot modules implementing \"IBootModule\" or \"IModule\" with constructor that accepts Framework as parameter, to further customize the behavior of MSDesktop application")]
        public BootModules BootModules { get; set; }

        [Category("Module")] 
        [PropertyDescription(Description = "Specifies modules implementing \"IModule\" to be loaded", CodeWiki = "Module/Add_Module_Via_App_Config")]
        public ModuleElements Modules { get; set; }

        [Category("Module")]
        [PropertyDescription(Description = "Specifies modules to be loaded in isolated processes", CodeWiki = "Module_Isolation/Introduction")]
        public IsolatedSubsystems IsolatedSubsystems { get; set; }

        [Category("Module")]
        [PropertyDescription(Description = "Specifies external applications to be loaded in into the MSDesktop application",
            SnapShots = new string[] { SnapShots.RibbonWindow11 })]
        public ExternalApplications ExternalApplications { get; set; }
         
        [Browsable(false)]
		[XmlAttribute("type")]
		public string TypeHack { get { return "MorganStanley.Desktop.Loader.Configuration.MSDesktopConfiguration, MorganStanley.Desktop.Loader"; } set { } }

		public MSDesktopConfiguration():this(false)
		{ 
        }

        public MSDesktopConfiguration(bool noDefaultSettings_)
        { 
            if (!noDefaultSettings_)
            {
                this.GUI = new GUI();
                this.LayoutAndPersistence = new LayoutAndPersistence();
                this.QuitPromptDialogBehaviour = new QuitPromptDialogBehaviour();
                this.ApplicationInformation = new ApplicationInformation();
                this.Splash = new Splash();
                this.ExceptionDialogBehaviour = new ExceptionDialogBehaviour();
                this.ApplicationOptions = new ApplicationOptions();
                this.Communication = new Communication();
                this.LoggingParameters = new LoggingParameters();
                this.Environment = new Environment();
                this.Entitlement = new EntitlementConfiguration();
                this.AdvancedFeatures = new AdvancedFeatures();
            }

        }


        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new MSDesktopConfigurationApplier();
        }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return new MSDesktopConfigurationAssemblyRequester();
        }

        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get
            {
                List<IApplyableConfiguration> configurations = new List<IApplyableConfiguration>();
                if (Login != null) configurations.Add(Login);
                if (ApplicationInformation != null) configurations.Add(ApplicationInformation);
                if (GUI != null) configurations.Add(GUI);
                if (LayoutAndPersistence != null) configurations.Add(LayoutAndPersistence);
                if (Splash != null) configurations.Add(Splash);
                if (QuitPromptDialogBehaviour != null) configurations.Add(QuitPromptDialogBehaviour);
                if (ExceptionDialogBehaviour != null) configurations.Add(ExceptionDialogBehaviour);
                if (Environment != null) configurations.Add(Environment);
                if (ApplicationOptions != null) configurations.Add(ApplicationOptions);
                if (Concord != null) configurations.Add(Concord);
                if (LoggingParameters != null) configurations.Add(LoggingParameters);
                if (Communication != null) configurations.Add(Communication);
                if (AdvancedFeatures != null) configurations.Add(AdvancedFeatures);
                if (Entitlement != null) configurations.Add(Entitlement);
                if (BootModules != null) configurations.Add(BootModules);
                if (ViewModelBasedButtons != null) configurations.Add(ViewModelBasedButtons);
                if (Modules != null) configurations.Add(Modules);
                if (IsolatedSubsystems != null) configurations.Add(IsolatedSubsystems);
                if (ExternalApplications != null) configurations.Add(ExternalApplications);
                return configurations;
            }
        }

        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
            foreach (var subConfiguration in ((IApplyableConfiguration)this).SubConfigurations)
            {
                subConfiguration.Resolve(this);
            }
        }
        #endregion 

        internal static MSDesktopConfiguration GenerateExampleConfiguration()
        {
           var config = new MSDesktopConfiguration();
            config.ExceptionHandler = "msguiadministrators@ms.com";
            config.ApplicationInformation.IconPath = new ImageHelper()
                {
                    Path = @"Resources\icon.ico",
                    Source = Source.Resource
                };
            config.GUI.RibbonIcon = config.ApplicationInformation.IconPath = new ImageHelper()
            {
                Path = @"Resources\icon.ico",
                Source = Source.Resource
            };
            config.GUI.CentralizedPlacement = new CentralizedPlacementHelper() {FileName = @"Configuration\Buttons.xml"};
            config.LayoutAndPersistence.Storage = new LayoutPersistenceStorage()
                {
                    MultipleFiles = false,
                    PresetDirectory = "Profiles"
                };
            config.Splash.NameFont = "Microsoft Sans Serif";
            config.Splash.StatusFont = "Microsoft Sans Serif";
            config.Splash.Picture = @"Resources\Splash.png";
            config.Concord = new global::MorganStanley.Desktop.Loader.Configuration.Bridges.Concord()
                {
                    LogFolderName =
                        Path.Combine(
                            System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData),
                            "Morgan Stanley",
                            "MSDesktop Application",
                            "Log"),
                    WindowsFormHostRestyleOptions = new WindowsFormHostRestyleOptions()
                };
            config.AdvancedFeatures = new AdvancedFeatures()
                {
                    WorkspaceSupport = new WorkspaceSupport()
                        {
                            DefaultCategory = "Global",
                            DefaultTitle = "New workspace",
                            Layout = LayoutEngine.Tile,
                            DefaultTitlesByCategory = new List<WorkspaceTitle>()
                                {
                                    new WorkspaceTitle {Category = "Global", Title = "New workspace"}
                                }
                        }
                };
            config.QuitPromptDialogBehaviour = new QuitPromptDialogBehaviour()
                {
                    WarningPrompt = "Are you sure to close the App?"
                };
            config.ExceptionDialogBehaviour = new ExceptionDialogBehaviour()
                {
                    ExceptionMessage = "Error occurred",
                    LargeFileStorageDirectory = @"U:\tmp"
                };
            config.ApplicationOptions.GroupPriorityOverrides = new List<PriorityOverride>()
			        {
			            new PriorityOverride() {Group = "RiskViewer", Priority = 100},
			            new PriorityOverride() {Group = "Custom", Title = "Test Page", Priority = 50}
			        };
            config.LoggingParameters = new LoggingParameters()
                { 
                    ApplicationName = "MSDesktop Application",
                    Directory = Path.Combine(
                            System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData),
                            "Morgan Stanley",
                            "MSDesktop Application",
                            "Log"),
                    Basename = "MSDesktopLog_%" + LoggingParameters.ProcessIdParameter + "%_%" + LoggingParameters.DateParameter +"%",
                    LoggingLevels =
                        new List<ThresholdPolicy>()
                            {
                                new ThresholdPolicy{Target = "/", Policy = "msdotnet/msgui/*:Notice,msdotnet/*/*:Critical,*/*/*:Emergency"}
                            }
                };
            config.Entitlement = new EntitlementConfiguration()
                {
                    ServiceType = EntitlementServiceType.LDAP,
                    ModuleEntitlement = new ModuleEntitlement(),
                    ComponentEntitlement = new ComponentEntitlement(),
                    E3Service =
                        new E3Service()
                            {
                                DomainName = "Domain_1",
                                AddressOrAlias = "HKT_MSDotNet_MSEntitlement", 
                                CacheEnabled = true,
                                ResilienceFile = @"%APPDATA%\Morgan Stanley\LoaderExample\EntitlementStore.xml"
                            },
                    LDAPService =
                        new LDAPService()
                            {
                                LDAPUserName = "cn=msdotnet.directory,  ou=ldapids, o=Morgan Stanley",
                                LDAPPassword = "msdotnet", 
                                CacheEnabled = true,
                                ResilienceFile = @"%APPDATA%\Morgan Stanley\LoaderExample\EntitlementStore.xml",
                                ModuleResourcesMap = new List<ModuleResourceMapEntry>()
                                {
                                    new ModuleResourceMapEntry(){Action= ModuleEntitlementAction.Load, Name = "LoaderExample.Modules.ExampleModule1", MailGroups = "msdesktop-core;msdesktop-users", CostCenters = "J168"}
                                },
                                ComponentResourcesMap = new List<ComponentResourceMapEntry>()
                                {
                                    new ComponentResourceMapEntry(){Action= ComponentEntitlementAction.Modify, ID = "Button1", MailGroups = "msdesktop-core;msdesktop-users", CostCenters = "J168"}
                                } 
                            },
                    OfflineService = new OfflineService()
                        {
                            ModuleResources = new List<ModuleResourceHelper>()
                                {
                                    new ModuleResourceHelper(){Action= ModuleEntitlementAction.Load, Name = "LoaderExample.Modules.ExampleModule1"}
                                },
                            ComponentResources = new List<ComponentResourceHelper>()
                                {
                                    new ComponentResourceHelper(){Action= ComponentEntitlementAction.Modify, ID = "Button1"}
                                }
                        }
                };
            config.ViewModelBasedButtons = new ViewModelBasedButtons()
                {
                    Buttons = new List<ViewModelBasedButton>()
                        {
                            new ViewModelBasedButton()
                                {
                                    CheckEntitlement = true,
                                    Group = "View Model Based",
                                    ID = "ViewModelBasedButton1",
                                    Image = new ImageHelper(){Path = "SampleIcon", Source = Source.Resource},
                                    ViewType = "SampleView",
                                    ViewModelType = "SampleViewModel",
                                    ViewParametersType = "SampleViewParameter",
                                    Text = "Button 1" 
                                }
                        }
                };
            config.BootModules = new BootModules()
                {
                    Modules = { new BootModule() { ModuleAssembly = "LoaderExample", ModuleFQCN = "LoaderExample.Class1" } }
                };
            
            return config;

        }
         
    }
}
