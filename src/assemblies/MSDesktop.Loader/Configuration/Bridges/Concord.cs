﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.Configuration.Bridges
{
	[XmlRoot("Concord")]
    public class Concord : EnablableComplexConfigurationItem, IApplyableConfiguration
	{
        public Concord()
        {
            WindowsFormHostRestyleOptions = new WindowsFormHostRestyleOptions();
        }

        [PropertyDescription(Description = "If true, suppresses the concord file log.")]
        public bool SuppressLogSetup { get; set; }
        [PropertyDescription(Description = "If specified, overrides the logging folder if logging is enabled, default is either application name or specified using LoggingParameters")]
        public string LogFolderName { get; set; }

        [PropertyDescription(Description = "If true, sets the size of the concord window to be exactly the same as the original concord form, only use it (and is recommended) if the original concord form is designed to be resizable using docking or anchor correctly, or else the content might be trimmed due to the concord window padding.",
            SnapShots = new string[]{SnapShots.ConcordWindowNoAutoSize, SnapShots.ConcordWindowAutoSize})]
        public bool AutosizeForms { get; set; }

        [PropertyDescription(Description = "If true, same concord window won't be opened twice when using ConcordWindowManager.CreateWindow")]
        public bool PreventDoubleWindowCreation { get; set; }

        [PropertyDescription(Description = "If true, the version sets to IConcordApplication won't needs to be reformatted")]
        public bool DoNotReparseVersionNumber { get; set; }

        [PropertyDescription(Description = "If true, enables default config environment variable extractor, which set application version infos read from msde file and key value pairs defined under section \"environmentVariablesToAdd\" in app.config into environment variables")]
        public bool ConfigEnvironmentVariableExtractorEnabled { get; set; }

        [PropertyDescription(Description = "If true, hides the concord user and profile status from the status bar",
            SnapShots = new string[]{SnapShots.RibbonWindow},
            SnapShotPart = "ConcordStatus")]
        public bool SuppressConcordStatusBarItems { get; set; }

        [PropertyDescription(Description = "Uses this option to override the style of the concord window",
            SnapShots = new string[]{SnapShots.ConcordWindowAutoSize})]
        public WindowsFormHostRestyleOptions WindowsFormHostRestyleOptions { get; set; }

        [PropertyDescription(Description = "Specifies a list of applet names, which must run in UI thread", 
            Example = "TestApplet;TestApplet2")]
        public string UIOnlyApplets { get; set; } 
          
        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new ConcordApplier();
        }
        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }
        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return WindowsFormHostRestyleOptions == null ? null : new List<IApplyableConfiguration>(){WindowsFormHostRestyleOptions}; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
        }
        #endregion
         
    }
}
