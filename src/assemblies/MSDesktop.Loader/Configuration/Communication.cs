﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using MorganStanley.Desktop.Loader.AssemblyRequesters;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core.Messaging; 

namespace MorganStanley.Desktop.Loader.Configuration
{
    public class Communication : ComplexConfigurationItem, IApplyableConfiguration
    {
        public Communication()
        {
            IpcCommunicationStatus = CommunicationChannelStatus.Optional;
            ImcCommunicationStatus = CommunicationChannelStatus.Optional;
            RoutingMessageDispatchExceptionAsFailed = true;
        }

        [PropertyDescription(
            Description =  "Indicates the inter process communication channel status",
        ValueDescriptions = new string[] 
            {
              "Indicates that the inter process communication channel is disabled and not used",
              "Indicates that the inter process communication channel is optional. MSDesktop platform will attempt to initialize it, however the application will start and run if the initialization of the channel fails.This is the default behavior for isolated subsystem hosting process",
              "Indicates that the inter process communication channel is required. MSDesktop platform will attempt to initialize it, and the application will fail start if the initialization of the channel fails. This is the default behavior for the host applications."
            }
            )]
        [DefaultValue(CommunicationChannelStatus.Optional)]
        public CommunicationChannelStatus IpcCommunicationStatus { get; set; }

      [PropertyDescription(
            Description = "Indicates the inter machine communication channel status",
        ValueDescriptions = new string[] 
            {
              "Indicates that the inter machine communication channel is disabled and not used",
              "Indicates that the inter machine communication channel is optional. MSDesktop platform will attempt to initialize it, however the application will start and run if the initialization of the channel fails.This is the default behavior for isolated subsystem hosting process",
              "Indicates that the inter machine communication channel is required. MSDesktop platform will attempt to initialize it, and the application will fail start if the initialization of the channel fails. This is the default behavior for the host applications."
            }
            )]
        [DefaultValue(CommunicationChannelStatus.Optional)]
        public CommunicationChannelStatus ImcCommunicationStatus { get; set; }

        [PropertyDescription(
     Description = @"If true, provides a GUI to allow users connecting/disconnecting the message v2v senders and subscribers at the runtime",
     CodeWiki = "Communications/View_to_View_Communication/ViewToViewConnection",
     SnapShots = new string[]{SnapShots.ApplicationMenu},
     SnapShotPart = "V2VCommunication")]
        public bool V2VConfigEnabled { get; set; }

        [PropertyDescription(
Description = @"If true, enables kraken communication")]
        public bool MessageRoutingCommunicationEnabled { get; set; }

        [PropertyDescription(
Description = @"If false, any communication exception happened when dispatching a kraken message would treated as exception instead of normal dispatching failure to raise warning level.")]
        [BindableEnablement("MessageRoutingCommunicationEnabled")]
        [DefaultValue(true)]
        public bool RoutingMessageDispatchExceptionAsFailed { get; set; }
         
        [PropertyDescription(
       Description = @"If true, pop ups the route finder when failed to decide the kraken dispatch route.",
     SnapShots = new string[] { SnapShots.RouteFinder, SnapShots.RoutePersistence },
     SnapShotDescriptions = new string[]{"Route Finder", "Routed Communication Configuration Options"})]
        [BindableEnablement("MessageRoutingCommunicationEnabled")]
        [BindableValue("MessageRoutingCommunicationEnabled", typeof(AutoDisableConverter))]
        public bool MessageRoutingDefaultGuiEnabled { get; set; }

        [PropertyDescription(
   Description = @"If true, provides the runtime kraken message monitor",
 SnapShots = new string[] { SnapShots.KrakenMonitor },
 SnapShotPart = "MonitorArea")]
        [BindableEnablement("MessageRoutingCommunicationEnabled")]
        [BindableValue("MessageRoutingCommunicationEnabled", typeof(AutoDisableConverter))]
        public bool MessageRoutingMonitorEnabled { get; set; }

        [PropertyDescription(
Description = @"If true, records the kraken message history")]
        [BindableEnablement("MessageRoutingCommunicationEnabled")]
        [BindableValue("MessageRoutingCommunicationEnabled", typeof(AutoDisableConverter))]
        public bool MessageRoutingHistoryEnabled { get; set; }

        [PropertyDescription(
Description = @"If true, provides the kraken message history viewer",
SnapShots = new string[] { SnapShots.KrakenHistoryViewer })]
        [BindableEnablement("MessageRoutingHistoryEnabled")]
        [BindableValue("MessageRoutingHistoryEnabled", typeof(AutoDisableConverter))]
        public bool MessageRoutingHistoryGuiEnabled { get; set; }


        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new CommunicationApplier();
        }
        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return new CommunicationAssemblyRequester();
        }
        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }

        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
            if (!MessageRoutingCommunicationEnabled)
            {
                MessageRoutingDefaultGuiEnabled = MessageRoutingMonitorEnabled = MessageRoutingHistoryEnabled = false;
            }
            if (!MessageRoutingHistoryEnabled)
            {
                MessageRoutingHistoryGuiEnabled = false;
            }
        }
        #endregion
         
    }
}
