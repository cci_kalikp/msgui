﻿using System;
using System.Collections.Generic;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.Configuration
{
    public class LayoutAndPersistence: ComplexConfigurationItem, IApplyableConfiguration
	{
         [PropertyDescription(
Description = "Specifies how the layout is persisted to file",
CodeWiki = "Application")]
        public LayoutPersistenceStorage Storage { get; set; }

         [PropertyDescription(
Description = "If true, the view title would not be affected by layout persisted", 
SnapShots = new string[]{SnapShots.FloatingWindow},
SnapShotPart = "Title")]
         public bool ViewTitleRestoreNoOverwrite { get; set; }

         [PropertyDescription(
Description = "If true, the view location would not be affected by layout persisted", 
SnapShots = new string[] { SnapShots.LauncherBarAndFloatingWindows },
SnapShotPart = "FloatingWindow")]
         public bool ViewLocationNoOverwrite { get; set; }

         [PropertyDescription(
Description = "Specifies whether user would be asked to save current layout when switches layout or exit application")]
         public LayoutLoadStrategy LayoutLoadStrategy { get; set; }

         [PropertyDescription(
Description = "If specified, the save layout confirmation would be suppressed if certain error occurs loading the application, to prevent user saving a bad layout in such error state.")]
         public SkipSaveProfileOnExitCondition SkipSaveProfileOnExitWhen { get; set; }

        [PropertyDescription(Description = "If true, enables to keep windows hidden until layout is fully loaded")]
         public bool HiddenWindowsOnLayoutLoadingEnabled { get; set; } //supported in harmonia
        
        [PropertyDescription(Description = "If true, would display a progress tracking dialog during layout loads",
            SnapShots = new string[] { "LayoutLoadingProgress" },
            SnapShotPart = "ProgressWindow")]
        public bool LayoutLoadProgressTrackingEnabled { get; set; }
         
        [PropertyDescription(Description = "If true, views would be loaded one by one during layout loads, this is can be used to avoid message queue overflow exception if there are too many views and each view would have some heavy logic at start up")]
        public bool SequencialViewLoadEnabled { get; set; }

        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new LayoutAndPersistenceApplier();
        }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }

        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        { 
        }
        #endregion


    }
}
