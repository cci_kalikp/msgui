﻿using System.Collections.Generic; 
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.Configuration
{
    public class ApplicationOptions : ComplexConfigurationItem, IApplyableConfiguration
    {
        [PropertyDescription(Description = "Specifies extra option pages to be added to the options window",
        CodeWiki = "Application/Creating_custom_settings_screen/Overview",
        SnapShots = new string[] { SnapShots.OptionsDialog },
        SnapShotPart = "CustomPages")]
        [XmlArrayItem("Page")]
        public List<OptionPage> CustomPages { get; set; }

        [PropertyDescription(Description = "If true, uses classic theme for the options window.",
            CodeWiki = "Application/Creating_custom_settings_screen/Overview",
            SnapShots = new string[] { SnapShots.OptionsDialog, SnapShots.ClassicOptionsDialog })]
        public bool UseClassicTheme { get; set; }

        [PropertyDescription(Description = "If true, enables type ahead drop down to show the searched result in the options window.",
      CodeWiki = "Application/Creating_custom_settings_screen/Overview",
      SnapShots = new string[] { SnapShots.OptionsDialogSearchTree, SnapShots.OptionsDialogSearchDropDown },
      SnapShotPart = "SearchResult",
      SnapShotPartForComparison = true)]
        public bool TypeaheadDropdownOptionsSearchEnabled { get; set; }

        [PropertyDescription(Description = "Overrides the position of options pages, default is to append option pages in the end of the navigation tree",
            CodeWiki = "Application/Creating_custom_settings_screen", 
            SnapShots = new string[] { SnapShots.OptionsDialog },
            SnapShotPart = "NavigationTree")] 
        public List<PriorityOverride> GroupPriorityOverrides { get; set; }

        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new ApplicationOptionsApplier();
        }
        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }
        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }

        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {

        }
        #endregion
    }
}
