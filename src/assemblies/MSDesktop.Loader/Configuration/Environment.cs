﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.AssemblyRequesters;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.Configuration
{
    public class Environment : ComplexConfigurationItem, IApplyableConfiguration
    {
        public static Color DefaultDevLabelColor = Colors.Green;
        public static readonly Color DefaultQaLabelColor = Colors.Yellow;
        public static readonly Color DefaultUatLabelColor = Colors.Orange;
        public static readonly Color DefaultProdLabelColor = Colors.Red;

        public Environment()
        {
            DevLabelColor = new ColorHelper(DefaultDevLabelColor);
            QaLabelColor = new ColorHelper(DefaultQaLabelColor);
            UatLabelColor = new ColorHelper(DefaultUatLabelColor);
            ProdLabelColor = new ColorHelper(DefaultProdLabelColor);
            EnvironProviderId = EnvironProviderId.ConcordProfile;
            RegionProviderId = RegionProviderId.ConcordProfile;
            UsernameProviderId = UsernameProviderId.ConcordProfile;
            SubenvironmentProviderId = SubenvironmentProviderId.None;
        }

        [PropertyDescription(Description = "Specifies the way to resolve environment name.\r\nIf enabled, the environment label would appear if not turned off",
            ValueDescriptions = new string[]
                {
                    "Disables environment name resolution",
                    "Resolves environment name from command line like \"-env:dev\", \"-env:qa\"",
                    "Resolves environment name from current concord user profile",
                    "Resolves environment name from node \"Environ\" under \"Environment\" concord configuration"
                },
                CodeWiki = "Environment/Introduction",
                SnapShots = new string[]{SnapShots.EnvironmentLabel},
                SnapShotPart = "EnvironmentLabel",
                FlashColor = "Gold"
            )]
        [DefaultValue(EnvironProviderId.ConcordProfile)]
        public EnvironProviderId EnvironProviderId { get; set; }

        [PropertyDescription(Description = "Specifies the way to resolve region name",
      ValueDescriptions = new string[]
                {
                    "Disables region name resolution", 
                    "Resolves region name from current concord user profile",
                    "Resolves region name from node \"Region\" under \"Environment\" concord configuration"
                },
                CodeWiki = "Environment/Introduction"
      )]
        [DefaultValue(RegionProviderId.ConcordProfile)]
        public RegionProviderId RegionProviderId { get; set; }

       [PropertyDescription(Description = "Specifies the way to resolve user name",
      ValueDescriptions = new string[]
                {
                    "Disables region name resolution", 
                    "Resolves region name from current concord user profile" 
                },
                CodeWiki = "Environment/Introduction"
      )]
       [DefaultValue(UsernameProviderId.ConcordProfile)]
        public UsernameProviderId UsernameProviderId { get; set; }

       [PropertyDescription(Description = "Specifies the way to resolve sub environment name.\r\nIf enabled, the environment label would appear if not using modern environment label and not turned off",
ValueDescriptions = new string[]
                {
                    "Disables sub environment name resolution", 
                    "Resolves sub environment name from command line \"-subenv:subenvironment\"" 
                },
                CodeWiki = "Environment/Labelling",
                SnapShots = new string[] { SnapShots.SubEnvironmentLabel },
                SnapShotPart = "SubEnvironmentLabel",
                FlashColor = "Gold"
      )]
       [DefaultValue(SubenvironmentProviderId.None)]
       public SubenvironmentProviderId SubenvironmentProviderId { get; set; }

        [PropertyDescription(Description = "If true, uses the modern triangle environment label instead of the square environment label.",
            SnapShots = new string[]{SnapShots.EnvironmentLabel, SnapShots.ModernEnvironmentLabel},
            SnapShotPart = "EnvironmentLabel",
            SnapShotPartForComparison = true,
            FlashColor = "Gold")]
        public bool ModernLabelsEnabled { get; set; }

        [XmlElement(Type = typeof(string))]
        [BindableEnablement("ModernLabelsEnabled", typeof(InverseBoolConverter), "sets {0} to false")]
        [PropertyDescription(Description = "Overrides the color of dev environment label, default is \"Yellow\", and is only valid when modern label is not turned on",
                SnapShots = new string[]{SnapShots.EnvironmentLabelDEV},
                SnapShotPart = "EnvironmentLabel")]
        public ColorHelper DevLabelColor { get; set; }

        [XmlIgnore]
        public Color DevLabelColorResolved
        {
            get { return DevLabelColor != null ? DevLabelColor.Color : DefaultDevLabelColor; }
        }

        [XmlElement(Type = typeof(string))]
        [BindableEnablement("ModernLabelsEnabled", typeof(InverseBoolConverter), "sets {0} to false")]
        [PropertyDescription(Description = "Overrides the color of qa environment label, default is \"Green\", and is only valid when modern label is not turned on",
SnapShots = new string[] { SnapShots.EnvironmentLabelQA },
SnapShotPart = "EnvironmentLabel")]
        public ColorHelper QaLabelColor { get; set; }


        [XmlIgnore]
        public Color QaLabelColorResolved
        {
            get { return QaLabelColor != null ? QaLabelColor.Color : DefaultQaLabelColor; }
        }

        [XmlElement(Type = typeof(string))]
        [BindableEnablement("ModernLabelsEnabled", typeof(InverseBoolConverter), "sets {0} to false")]
        [PropertyDescription(Description = "Overrides the color of uat environment label, default is \"Orange\", and is only valid when modern label is not turned on",
        SnapShots = new string[] { SnapShots.EnvironmentLabelUAT },
        SnapShotPart = "EnvironmentLabel")]
        public ColorHelper UatLabelColor { get; set; }

        [XmlIgnore]
        public Color UatLabelColorResolved
        {
            get
            {
                return UatLabelColor != null ? UatLabelColor.Color : DefaultUatLabelColor;
            }
        }

        [XmlElement(Type = typeof(string))]
        [BindableEnablement("ModernLabelsEnabled", typeof(InverseBoolConverter), "sets {0} to false")]
        [PropertyDescription(Description = "Overrides the color of prod environment label, default is \"Red\", and is only valid when modern label is not turned on",
             SnapShots = new string[] { SnapShots.EnvironmentLabel },
             SnapShotPart = "EnvironmentLabel", 
             FlashColor = "Gold")]
        public ColorHelper ProdLabelColor { get; set; }
        [XmlIgnore]
        public Color ProdLabelColorResolved
        {
            get { return ProdLabelColor != null ? ProdLabelColor.Color : DefaultProdLabelColor; }
        }


        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new EnvironmentApplier();
        }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return new EnvironmentAssemblyRequester();
        }

        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {

        }
        #endregion


    }
}
