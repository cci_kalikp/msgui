﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.AssemblyRequesters;
using MorganStanley.Desktop.Loader.Configuration.Designer; 
using MorganStanley.Desktop.Loader.Configuration.Modules;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.Configuration
{
    [XmlRoot("IsolatedSubsystems")]
    public class IsolatedSubsystems : ExternalComplexConfigurationItem, IApplyableConfiguration, INullableConfigurationItem
    {
        [PropertyDescription(Description = "Specifies modules to be loaded in isolated processes", CodeWiki = "Module_Isolation/Introduction")]
        [XmlElement("Subsystem")]
        public List<IsolatedSubsystem> Subsystems { get; set; }  
         
        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new IsolatedSubsystemsApplier();
        }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return new IsolatedSubsystemsAssemblyRequester();
        }

        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }



        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
            var configResolved = base.Resolve() as IsolatedSubsystems; 
            rootConfiguration_.IsolatedSubsystems = configResolved;
        }
        #endregion


        void INullableConfigurationItem.InitializeNullObject(object parentObject_)
        {

        }
    }
}
