﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace MorganStanley.Desktop.Loader.Configuration.Designer
{
    public class AutoEnableWhenNotNullConverter:IMultiValueConverter 
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values == null || values.Length != 2) return Binding.DoNothing; 
            if (!string.IsNullOrWhiteSpace(values[0] as string)) return true;
            return values[1];
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
