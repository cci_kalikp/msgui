﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.Desktop.Loader.Configuration.Designer
{
    public class AutoFloatingWhenEnabledConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values == null || values.Length != 2) return Binding.DoNothing; 
            if (System.Convert.ToBoolean(values[0])) return InitialWindowLocation.FloatingAnywhere;
            return values[1];
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
         
    }
}
