﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace MorganStanley.Desktop.Loader.Configuration.Designer
{
    public abstract class EntitlementServiceEnablementConverter:IValueConverter
    { 
        object IValueConverter.Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            EntitlementServiceType service = (EntitlementServiceType)value;
            return ResolveEnablement(service);
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        protected abstract bool ResolveEnablement(EntitlementServiceType serviceType_);
    }
}
