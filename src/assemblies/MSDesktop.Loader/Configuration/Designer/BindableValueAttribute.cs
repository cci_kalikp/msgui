﻿using System;

namespace MorganStanley.Desktop.Loader.Configuration.Designer
{
    [AttributeUsage(AttributeTargets.Property)]
    public class BindableValueAttribute:Attribute
    {
        public BindableValueAttribute(string bindToPropertyName_, Type converterType_)
        {
            BindToPropertyName = bindToPropertyName_;
            ConverterType = converterType_; 
        } 

        public string BindToPropertyName { get; set; }

        public Type ConverterType { get; set; } 
    }
}
