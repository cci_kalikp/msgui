﻿using System;

namespace MorganStanley.Desktop.Loader.Configuration.Designer
{
    [AttributeUsage(AttributeTargets.Property)]
    public class BindableEnablementAttribute:Attribute
    {
        public BindableEnablementAttribute(string bindToPropertyName_)
        {
            BindToPropertyName = bindToPropertyName_;
            DescriptionFormat = "sets {0} to true";
        }
         
        public BindableEnablementAttribute(string bindToPropertyName_, Type converterType_):this(bindToPropertyName_)
        { 
            ConverterType = converterType_;
        }

        public BindableEnablementAttribute(string bindToPropertyName_, Type converterType_, string descriptionFormat_)
            : this(bindToPropertyName_, converterType_)
        {
            DescriptionFormat = descriptionFormat_;
        }
        public string BindToPropertyName { get; set; }

        public Type ConverterType { get; set; }

        public string DescriptionFormat { get; set; }
    }
}
