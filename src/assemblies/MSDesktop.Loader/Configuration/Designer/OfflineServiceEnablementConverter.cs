﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Configuration.Designer
{
    public class OfflineServiceEnablementConverter:EntitlementServiceEnablementConverter
    {
        protected override bool ResolveEnablement(EntitlementServiceType serviceType_)
        {
            return serviceType_ == EntitlementServiceType.Offline;
        }
    }
}
