﻿using System; 
using System.Windows.Data; 
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.Desktop.Loader.Configuration.Designer
{
    public class InitialTabEnablementConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            InitialWindowLocation location = (InitialWindowLocation)value;
            return location == InitialWindowLocation.DockBottom ||
                   location == InitialWindowLocation.DockLeft ||
                   location == InitialWindowLocation.DockRight ||
                   location == InitialWindowLocation.DockTop;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

       
    }
}
