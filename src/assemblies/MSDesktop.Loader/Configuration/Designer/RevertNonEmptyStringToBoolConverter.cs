﻿using System;
using System.Windows.Data;

namespace MorganStanley.Desktop.Loader.Configuration.Designer
{
    public class RevertNonEmptyStringToBoolConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string str = value as string;
            if (!String.IsNullOrWhiteSpace(str))
            {
                return false;
            }
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
