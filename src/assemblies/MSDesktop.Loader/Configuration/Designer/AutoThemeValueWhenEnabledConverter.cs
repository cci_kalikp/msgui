﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using MorganStanley.Desktop.Loader.Configuration.Helpers;

namespace MorganStanley.Desktop.Loader.Configuration.Designer
{
    public class AutoThemeValueWhenEnabledConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values == null || values.Length != 2 || values[0] == DependencyProperty.UnsetValue) return Binding.DoNothing;
            if (System.Convert.ToBoolean(values[0])) return Theme.None; 
            if (values[1] is Theme && (Theme)values[1] == Theme.None)
            {
                return Theme.Simple;
            }
            return values[1];
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}
