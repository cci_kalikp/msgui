﻿using System;

namespace MorganStanley.Desktop.Loader.Configuration.Designer
{
    [AttributeUsage(AttributeTargets.Property)]
    public class PathAttribute:Attribute
    {
        public PathAttribute(PathType pathType_)
        {
            PathType = pathType_;
        }
        public PathType PathType { get; set; } 
    }

    public enum PathType
    {
        XmlFile,
        Image,
        ConfigFile,
        MsdeConfigFile,
        AssemblyFile,
        Executable,
        HtmlFile,
        FlexFile,
        Directory,
        JavaBatch,

    }


}
