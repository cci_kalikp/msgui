﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Configuration.Designer
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class)]  
    public class PropertyDescriptionAttribute: Attribute
    { 
        public string Description { get; set; }
        public string[] ValueDescriptions { get; set; }
        public string CodeWiki { get; set; }
        public CodeWikiType CodeWikiType { get; set; }
        public string Example { get; set; }
        public string[] SnapShots { get; set; }
        public string[] SnapShotDescriptions { get; set; }
        public string SnapShotPart { get; set; }
        public bool SnapShotPartForComparison { get; set; }
        public string FlashColor { get; set; }

    }

    public enum CodeWikiType
    {
        MSDesktop,
        DNParts,
        External
    }
}
