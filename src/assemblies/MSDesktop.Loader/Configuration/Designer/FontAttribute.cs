﻿using System;

namespace MorganStanley.Desktop.Loader.Configuration.Designer
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FontAttribute:Attribute
    {
    }
}
