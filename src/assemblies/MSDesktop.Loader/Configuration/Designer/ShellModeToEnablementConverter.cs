﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using MorganStanley.Desktop.Loader.Configuration.Helpers;

namespace MorganStanley.Desktop.Loader.Configuration.Designer
{
    public abstract class ShellModeToEnablementConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ShellMode mode = (ShellMode)value;
            return ConvertToBoolean(mode);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        protected abstract bool ConvertToBoolean(ShellMode mode_);
    }
}
