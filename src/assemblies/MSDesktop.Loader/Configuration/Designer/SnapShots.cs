﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Configuration.Designer
{
    public static class SnapShots
    {
        public const string SplashScreen = "SplashScreen"; 
        public const string LegacySplashScreen = "LegacySplashScreen"; 
        public const string LauncherBarAndFloatingWindows = "LauncherBarAndFloatingWindows";
        public const string RibbonWindow = "RibbonWindow";
        public const string RibbonWindow2 = "RibbonWindow2";
        public const string RibbonWindow3 = "RibbonWindow3";
        public const string RibbonWindow4 = "RibbonWindow4";
        public const string RibbonWindow5 = "RibbonWindow5";
        public const string RibbonWindow6 = "RibbonWindow6";
        public const string RibbonWindow7 = "RibbonWindow7";
        public const string RibbonWindow8 = "RibbonWindow8";
        public const string RibbonWindow9 = "RibbonWindow9";
        public const string RibbonWindow10 = "RibbonWindow10";
        public const string RibbonWindow11 = "RibbonWindow11";
        public const string LauncherBarAndWindow = "LauncherBarAndWindow";
        public const string LauncherBar = "LauncherBar";
        public const string LauncherBarExpanded = "LauncherBarExpanded";
        public const string LauncherBarNarrowed = "LauncherBarNarrowed";
        public const string RibbonAndFloatingWindows = "RibbonAndFloatingWindows";
        public const string LauncherBarNoUserName = "LauncherBarNoUserName";
        public const string LauncherBarWithUserName = "LauncherBarWithUserName";
        public const string LauncherBarNoLayoutName = "LauncherBarNoLayoutName";
        public const string FloatingWindow = "FloatingWindow";
        public const string FloatingWindow2 = "FloatingWindow2";
        public const string ExceptionDialog = "ExceptionDialog";
        public const string LegacyExceptionDialog = "LegacyExceptionDialog"; 
        public const string QuitDialog = "QuitDialog";
        public const string LegacyQuitDialog = "LegacyQuitDialog";
        public const string SimpleTheme = "SimpleTheme";
        public const string BlackTheme = "BlackTheme";
        public const string BlueTheme = "BlueTheme";
        public const string WhiteTheme = "WhiteTheme";
        public const string ApplicationMenu = "ApplicationMenu";
        public const string TaskDialog = "TaskDialog";
        public const string ClassicTaskDialog = "ClassicTaskDialog";
        public const string OptionsDialog = "OptionsDialog";
        public const string ClassicOptionsDialog = "ClassicOptionsDialog";
        public const string OptionsDialogSearchDropDown = "OptionsDialogSearchDropDown";
        public const string OptionsDialogSearchTree = "OptionsDialogSearchTree"; 
        public const string RouteFinder = "RouteFinder";
        public const string RoutePersistence = "RoutePersistence";
        public const string KrakenMonitor = "KrakenMonitor";
        public const string KrakenHistoryViewer = "KrakenHistoryViewer";
        public const string ConcordWindowNoAutoSize = "ConcordWindowNoAutoSize";
        public const string ConcordWindowAutoSize = "ConcordWindowAutoSize";
        public const string ConcordWindowReStyle = "ConcordWindowReStyle";
        public const string EnvironmentLabel = "EnvironmentLabel";
        public const string EnvironmentLabelQA = "EnvironmentLabelQA";
        public const string EnvironmentLabelDEV = "EnvironmentLabelDEV";
        public const string EnvironmentLabelUAT = "EnvironmentLabelUAT";
        public const string ModernEnvironmentLabel = "ModernEnvironmentLabel";
        public const string SubEnvironmentLabel = "SubEnvironmentLabel";
        public const string WorkspaceDockLayout = "WorkspaceDockLayout";
        public const string WorkspaceTileLayout = "WorkspaceTileLayout";
        public const string WorkspaceOptions = "WorkspaceOptions";

        public const string RibbonWindowMVVM = "RibbonWindowMVVM";
        public const string RibbonWindowMVVMLarge = "RibbonWindowMVVMLarge";
        public const string LauncherBarMVVM = "LauncherBarMVVM";

        public const string ModernThemeDark = "ModernThemeDark";
        public const string ModernThemeLight = "ModernThemeLight";
    }
}
