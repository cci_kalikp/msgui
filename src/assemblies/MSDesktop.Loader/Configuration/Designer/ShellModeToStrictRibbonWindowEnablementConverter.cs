﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using MorganStanley.Desktop.Loader.Configuration.Helpers;

namespace MorganStanley.Desktop.Loader.Configuration.Designer
{
    public class ShellModeToStrictRibbonWindowEnablementConverter : ShellModeToEnablementConverter
    {
        protected override bool ConvertToBoolean(ShellMode mode_)
        {
            return mode_ == ShellMode.RibbonWindow;
        }
    }
}
