﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Configuration.Designer
{
    [AttributeUsage(AttributeTargets.Class)]
    public class EditAsStringAttribute : Attribute
    {
    }
}
