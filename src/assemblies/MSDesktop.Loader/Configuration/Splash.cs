﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure; 
using MorganStanley.Desktop.Loader.Configuration.Helpers;

namespace MorganStanley.Desktop.Loader.Configuration
{
	[XmlRoot("Splash")] 
    public class Splash : DisablableComplexConfigurationItem, IApplyableConfiguration
	{ 
        public Splash()
        {
            ShowApplicationName = true;
            TransparentBitmap = TransparentBitmap.Default;
            NameColor = new ColorHelper(Color.FromArgb(0xff, 0x1f, 0x1f, 0x1f));
            StatusColor = new ColorHelper(Colors.Black);
            VersionColor = new ColorHelper(Colors.Black);
        }

         [PropertyDescription(
Description = "If true, uses the legacy splash screen",
SnapShots = new string[] { SnapShots.SplashScreen, SnapShots.LegacySplashScreen })]
         [BindableValue("Picture", typeof(AutoEnableWhenNotNullConverter))]
        public bool UseLegacySplash { get; set; }

        [PropertyDescription(
Description = "If false, hides the application name from the splash screen",
SnapShots = new string[] { SnapShots.SplashScreen, SnapShots.LegacySplashScreen },
SnapShotPart = "ApplicationName")]
        [DefaultValue(true)]
        public bool ShowApplicationName { get; set; }

        [PropertyDescription(
Description = "If true, double click the splash screen would hide it" )]
        public bool DoubleClickDismiss { get; set; }

        [PropertyDescription(
Description = "If true, minimizes instead of hides the splash screen if double clicked")]
        public bool MinimizeOnDismiss { get; set; }

        [PropertyDescription(
Description = "If true, show the close button on the upper right of the splash screen",
SnapShots = new string[] { SnapShots.SplashScreen, SnapShots.LegacySplashScreen },
SnapShotPart = "CloseButton")]
        public bool ShowCloseButton { get; set; }

        [PropertyDescription(
Description = "If specified, overrides the position of the close button",
SnapShots = new string[] { SnapShots.SplashScreen, SnapShots.LegacySplashScreen },
SnapShotPart = "CloseButton")]
        [XmlElement(Type = typeof(string))]
        public PointHelper CloseButtonPosition { get; set; }

        [PropertyDescription(
Description = "If specified, overrides the size of the close button",
SnapShots = new string[] { SnapShots.SplashScreen, SnapShots.LegacySplashScreen },
SnapShotPart = "CloseButton")]
        [XmlElement(Type = typeof(string))]
        public  SizeHelper CloseButtonSize { get; set; }

        [PropertyDescription(
Description = "If specified, provides a custom picture file path for splash screen, and would automatically switch to legacy splash",
Example = @"Images\Splash.png",
SnapShots = new string[] { SnapShots.LegacySplashScreen },
SnapShotPart = "Picture")]
        [Path(PathType.Image)]
        public string Picture { get; set; }

        [PropertyDescription(
Description = "If true, adjust the size of the splash screen according to the size of the splash picture, only valid for legacy splash",
SnapShots = new string[] { SnapShots.LegacySplashScreen },
SnapShotPart = "Picture")]
        [BindableEnablement("UseLegacySplash")]
        public bool Autosize { get; set; }

        [PropertyDescription(
Description = "If default, modern splash screen background is opaque and legacy splash screen background is transparent",
SnapShots = new string[] { SnapShots.SplashScreen, SnapShots.LegacySplashScreen },
SnapShotPart = "Picture")]
        [DefaultValue(TransparentBitmap.Default)]
        public TransparentBitmap TransparentBitmap { get; set; }

        [PropertyDescription(
Description = "If specified, overrides the fore color for application name text on the splash screen",
SnapShots = new string[] { SnapShots.SplashScreen, SnapShots.LegacySplashScreen },
SnapShotPart = "ApplicationName")]
        [XmlElement(Type = typeof(string))]
        public ColorHelper NameColor { get; set; }

        [PropertyDescription(
Description = "If specified, overrides the font for application name text on the splash screen. Default value is \"Tahoma\" for modern splash and \"Segoe UI Light\" for legacy splash",
Example = "Microsoft Sans Serif",
SnapShots = new string[] { SnapShots.SplashScreen, SnapShots.LegacySplashScreen },
SnapShotPart = "ApplicationName")]
        [Font]
        public string NameFont { get; set; }

        [PropertyDescription(
Description = "If specified, overrides the absolute position of the application name text on splash, default is centered. Needs to specify NamePosition, StatusPosition and VersionPosition all to make it work.",
SnapShots = new string[] { SnapShots.SplashScreen, SnapShots.LegacySplashScreen },
SnapShotPart = "ApplicationName")]
        [XmlElement(Type = typeof(string))]
        public PointHelper NamePosition { get; set; }

        [PropertyDescription(
Description = "If specified, overrides the fore color for loading status on the splash screen",
SnapShots = new string[] { SnapShots.SplashScreen, SnapShots.LegacySplashScreen },
SnapShotPart = "Status")]
        [XmlElement(Type = typeof(string))]
        public ColorHelper StatusColor { get; set; }

        [PropertyDescription(
Description = "If specified, overrides the font for loading status on the splash screen. Default value is \"Tahoma\".",
Example = "Microsoft Sans Serif",
SnapShots = new string[] { SnapShots.SplashScreen, SnapShots.LegacySplashScreen },
SnapShotPart = "Status")]
        [Font]
        public string StatusFont { get; set; }

        [PropertyDescription(
Description = "If specified, overrides the absolute position of the loading status on splash, default is centered. Needs to specify NamePosition, StatusPosition and VersionPosition all to make it work.",
SnapShots = new string[] { SnapShots.SplashScreen, SnapShots.LegacySplashScreen },
SnapShotPart = "Status")]
        [XmlElement(Type = typeof(string))] 
        public PointHelper StatusPosition { get; set; }

        [PropertyDescription(
Description = "If specified, overrides the fore color for version text on the splash screen",
SnapShots = new string[] { SnapShots.SplashScreen, SnapShots.LegacySplashScreen },
SnapShotPart = "Version")]
        [XmlElement(Type = typeof(string))]
        public ColorHelper VersionColor { get; set; }

        [PropertyDescription(
Description = "If specified, overrides the absolute position for version text on the splash screen, default is centered. Needs to specify NamePosition, StatusPosition and VersionPosition all to make it work.",
SnapShots = new string[] { SnapShots.SplashScreen, SnapShots.LegacySplashScreen },
SnapShotPart = "Version")]
        [XmlElement(Type = typeof(string))]
        public PointHelper VersionPosition { get; set; }

        [PropertyDescription(
Description = "If true, make the splash screen topmost, in this case, any other message box popped up would behind it.",
SnapShots = new string[] { SnapShots.SplashScreen })]
        public bool TopMost { get; set; }

        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new SplashApplier();
        }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }

        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        { 
        }
        #endregion

    }

}
