﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Helpers;

namespace MorganStanley.Desktop.Loader.Configuration
{
     
    public interface IPlacementElement
    {
        string Name { get; }
        ObservableCollection<IPlacementElement> Children { get; }
        IList<Type> ValidChildrenTypes { get; }
        bool AllowMultiple { get; }
        IPlacementElement Parent { get; } 
    }

    public interface IEditablePlacementElement:IPlacementElement, INotifyPropertyChanged
    {
        string Id { get; set; }
    }

    public enum PlacementArea
    {
        Ribbon,
        Launcherbar,
        Tabwell,
        Statusbar,
        SystemTray,
    }
     
    public class ChromeConfiguration
    {
        [XmlElement("PlacementInfo")]
        public List<PlacementInfo> Placements { get; set; } 

    }

    public class PlacementInfo:IPlacementElement
    {
        #region Editor
        [XmlIgnore]
        public string Name
        {
            get { return ShellMode.ToString(); } 
        }
         
        [XmlIgnore]
        public IList<Type> ValidChildrenTypes
        {
            get
            {
                switch (ShellMode)
                {
                    case ShellMode.RibbonWindow:
                        return new[] { typeof(Ribbon), typeof(Tabwell), typeof(Statusbar), typeof(SystemTray) };
                    case ShellMode.LauncherBarAndFloatingWindows:
                        return new[] { typeof(Launcherbar), typeof(Statusbar), typeof(SystemTray) };
                    case ShellMode.LauncherBarAndWindow:
                        return new[] { typeof(Launcherbar), typeof(Tabwell), typeof(Statusbar), typeof(SystemTray) };
                    case ShellMode.RibbonAndFloatingWindows:
                        return new[] { typeof(Ribbon), typeof(Statusbar), typeof(SystemTray) };
                    default:
                        return null;
                }
            }
        }
        [XmlIgnore]
        public IPlacementElement Parent { get; internal set; }

        [XmlIgnore]
        public bool AllowMultiple 
        {
            get { return false; }
        }

        [XmlIgnore]
        public bool HasRibbon
        {
            get { return Ribbon != null && ValidChildrenTypes.Contains(typeof (Ribbon)); }
        }

        [XmlIgnore]
        public bool HasLauncherbar
        {
            get { return Launcherbar != null && ValidChildrenTypes.Contains(typeof(Launcherbar)); }
        }

        [XmlIgnore]
        public bool HasTabwell
        {
            get { return Tabwell != null && ValidChildrenTypes.Contains(typeof (Tabwell)); }
        }

        [XmlIgnore]
        public bool HasStatusbar
        {
            get { return Statusbar != null && ValidChildrenTypes.Contains(typeof (Statusbar)); }
        }

        [XmlIgnore]
        public bool HasSystemTray
        {
            get { return SystemTray != null && ValidChildrenTypes.Contains(typeof (SystemTray)); }
        }

        private ObservableCollection<IPlacementElement> children;
        [XmlIgnore]
        public ObservableCollection<IPlacementElement> Children
        {
            get
            {
                if (children == null)
                {
                    children = new ObservableCollection<IPlacementElement>();
                    if (HasRibbon)
                    {
                        Ribbon.Parent = this;
                        children.Add(Ribbon);
                    }
                    if (HasLauncherbar)
                    {
                        Launcherbar.Parent = this;
                        children.Add(Launcherbar);
                    }
                    if (HasTabwell)
                    {
                        Tabwell.Parent = this;
                        children.Add(Tabwell);
                    }
                    if (HasStatusbar)
                    {
                        Statusbar.Parent = this;
                        children.Add(Statusbar);
                    }
                    if (HasSystemTray)
                    {
                        SystemTray.Parent = this;
                        children.Add(SystemTray);
                    }
                    children.CollectionChanged += children_CollectionChanged;
                }
                return children;
            }
        }

        private void children_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (var newItem in e.NewItems)
                {
                    if (!ValidChildrenTypes.Contains(newItem.GetType())) continue; 
                    Ribbon ribbon = newItem as Ribbon;
                    if (ribbon != null)
                    {
                        ribbon.Parent = this;
                        Ribbon = ribbon;
                        break;
                    }
                    Launcherbar launcherbar = newItem as Launcherbar;
                    if (launcherbar != null)
                    {
                        launcherbar.Parent = this;
                        Launcherbar = launcherbar;
                        break;
                    }
                    Tabwell tabwell = newItem as Tabwell;
                    if (tabwell != null)
                    {
                        tabwell.Parent = this;
                        Tabwell = tabwell;
                        break;
                    }
                    Statusbar statusbar = newItem as Statusbar;
                    if (statusbar != null)
                    {
                        statusbar.Parent = this;
                        Statusbar = statusbar;
                        break;
                    }
                    SystemTray systemTray = newItem as SystemTray;
                    if (systemTray != null)
                    {
                        systemTray.Parent = this;
                        SystemTray = systemTray;
                        break;
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (var oldItem in e.OldItems)
                {
                    if (!ValidChildrenTypes.Contains(oldItem.GetType())) continue;

                    Ribbon ribbon = oldItem as Ribbon;
                    if (ribbon != null)
                    {
                        Ribbon = null;
                        break;
                    }
                    Launcherbar launcherbar = oldItem as Launcherbar;
                    if (launcherbar != null)
                    {
                        Launcherbar = null;
                        break;
                    }
                    Tabwell tabwell = oldItem as Tabwell;
                    if (tabwell != null)
                    {
                        Tabwell = null;
                        break;
                    }
                    Statusbar statusbar = oldItem as Statusbar;
                    if (statusbar != null)
                    {
                        Statusbar = null;
                        break;
                    }
                    SystemTray systemTray = oldItem as SystemTray;
                    if (systemTray != null)
                    {
                        SystemTray = null;
                        break;
                    }
                }
            }
        }
          
        #endregion

        [XmlAttribute("shellMode")]
        public ShellMode ShellMode { get; set; }

        public Ribbon Ribbon { get; set; }

        public Launcherbar Launcherbar { get; set; }

        public Tabwell Tabwell { get; set; }

        public Statusbar Statusbar { get; set; }

        public SystemTray SystemTray { get; set; }
    }
  
    public class Ribbon:IPlacementElement
    {
        #region Editor
        [XmlIgnore]
        public string Name { get { return "Ribbon"; } } 

        [XmlIgnore]
         public IPlacementElement Parent { get; internal set; }

        private ObservableCollection<IPlacementElement> children;
        [XmlIgnore]
        public ObservableCollection<IPlacementElement> Children
        {
            get
            {
                if (children == null)
                {
                    if (Tabs == null) Tabs = new List<Tab>();
                    children = new ObservableCollection<IPlacementElement>();
                    foreach (var tab in Tabs)
                    {
                        tab.Parent = this;
                        children.Add(tab);
                    }
                    children.CollectionChanged += children_CollectionChanged;
                }
                return children;
            }
        }

        void children_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                for (int i = 0; i < e.NewItems.Count; i++)
                {
                    var newItem = (Tab)e.NewItems[i];
                    newItem.Parent = this;
                    Tabs.Insert(e.NewStartingIndex + i, newItem);
                } 
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (Tab oldItem in e.OldItems)
                {
                    Tabs.Remove(oldItem);
                }
            }
        }

        public IList<Type> ValidChildrenTypes
        {
            get { return new[] { typeof(Tab) }; }
        }

        [XmlIgnore]
        public bool AllowMultiple
        {
            get { return true; }
        } 
         
        #endregion

        [XmlElement("Tab")]
        public List<Tab> Tabs { get; set; } 
    }

    public class Tab:IEditablePlacementElement
    {
        #region Editor
        [XmlIgnore]
        public string Name { get { return "Tab"; } } 
        private string id;
        [XmlAttribute("name")]
        public string Id
        {
            get { return id; }
            set
            {
                if (id != value)
                {
                    id = value;
                    OnPropertyChanged("Id");
                }
            }
        }

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName_">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName_)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName_);
                handler(this, e);
            }
        }
        [XmlIgnore]
         public IPlacementElement Parent { get; internal set; }
        
        private ObservableCollection<IPlacementElement> children;
        [XmlIgnore]
        public ObservableCollection<IPlacementElement> Children
        {
            get
            {
                if (children == null)
                {
                    if (Groups == null) Groups = new List<Group>();
                    children = new ObservableCollection<IPlacementElement>();
                    foreach (var group in Groups)
                    {
                        group.Parent = this;
                        children.Add(group);
                    }
                    children.CollectionChanged += children_CollectionChanged;
                }
                return children;
            }
        }

        void children_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                for (int i = 0; i < e.NewItems.Count; i++)
                {
                    var newItem = (Group)e.NewItems[i];
                    newItem.Parent = this;
                    Groups.Insert(e.NewStartingIndex + i, newItem);
                }  
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (Group oldItem in e.OldItems)
                {
                    Groups.Remove(oldItem);
                }
            }
        }

        public IList<Type> ValidChildrenTypes
        {
            get { return new[]{typeof(Group)}; }
        }

        [XmlIgnore]
        public bool AllowMultiple
        {
            get { return true; }
        }
        #endregion

        [XmlElement("Group")]
        public List<Group> Groups { get; set; }
 
    }

    public class WidgetsContainer:IPlacementElement
    {
        #region Editor 
        [XmlIgnore]
        public string Name { get { return this.GetType().Name; } } 
         
        [XmlIgnore]
         public IPlacementElement Parent { get; internal set; }

        private ObservableCollection<IPlacementElement> children;
        [XmlIgnore]
        public ObservableCollection<IPlacementElement> Children
        {
            get
            {
                if (children == null)
                {
                    if (Widgets == null) Widgets = new List<Widget>();
                    children = new ObservableCollection<IPlacementElement>();
                    foreach (var widget in Widgets)
                    {
                        widget.Parent = this;
                        children.Add(widget);
                    }
                    children.CollectionChanged += children_CollectionChanged;
                }
                return children;
            }
        }

        void children_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                for (int i = 0; i < e.NewItems.Count; i++)
                {
                    var newItem = (Widget)e.NewItems[i];
                    newItem.Parent = this;
                    Widgets.Insert(e.NewStartingIndex + i, newItem);
                }    
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (Widget oldItem in e.OldItems)
                {
                    Widgets.Remove(oldItem);
                }
            }
        }
          
        public IList<Type> ValidChildrenTypes
        {
            get { return new[] { typeof(Widget) }; }
        }

        [XmlIgnore]
        public bool AllowMultiple
        {
            get { return true; }
        }
        #endregion

        [XmlElement("Widget")]
        public List<Widget> Widgets { get; set; }
    }

    public class Group:WidgetsContainer, IEditablePlacementElement
    {
        private string id;
        [XmlAttribute("name")]
        public string Id
        {
            get { return id; }
            set
            {
                if (id != value)
                {
                    id = value;
                    OnPropertyChanged("Id");
                }
            }
        }

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName_">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName_)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName_);
                handler(this, e);
            }
        }
 
    }

    public class Widget : IEditablePlacementElement
    {
        public Widget()
        {
            Size = Helpers.ButtonSize.Default;
        }
        #region Editor
        [XmlIgnore]
        public string Name { get { return "Widget"; } }

        private string id;
        [XmlText]
        public string Id
        {
            get { return id; }
            set
            {
                if (id != value)
                {
                    id = value;
                    OnPropertyChanged("Id");
                }
            }
        }

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName_">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName_)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName_);
                handler(this, e);
            }
        }
         
        [XmlIgnore]
         public IPlacementElement Parent { get; internal set; }

        [XmlIgnore]
        public ObservableCollection<IPlacementElement> Children
        {
            get { return null; }
        }
         
        public IList<Type> ValidChildrenTypes
        {
            get { return null; }
        }

        [XmlIgnore]
        public bool AllowMultiple
        {
            get { return false; }
        }
        #endregion

        private Helpers.ButtonSize size;
        [XmlAttribute("size")]
        public Helpers.ButtonSize Size
        {
            get { return size; }
            set
            {
                if (size != value)
                {
                    size = value;
                    OnPropertyChanged("Size");
                }
            }
        }
 
        public bool ShouldSerializeSize()
        {
            return size != Helpers.ButtonSize.Default;
        }
        [XmlIgnore]
        public bool AllowOverrideSize
        {
            get { return this.Parent is Group && this.Parent.Parent is Tab; }
        }
    }

    public class Tabwell:IPlacementElement
    {
        #region Editor
        [XmlIgnore]
        public string Name { get { return "Tabwell"; } }

        public IList<Type> ValidChildrenTypes
        {
            get { return new[] { typeof(LeftWidgets), typeof(RightWidgets) }; }
        }

        [XmlIgnore]
        public bool AllowMultiple
        {
            get { return false; }
        }

        [XmlIgnore]
         public IPlacementElement Parent { get; internal set; }

        private ObservableCollection<IPlacementElement> children;
        [XmlIgnore]
        public ObservableCollection<IPlacementElement> Children
        {
           get
           {
               if (children == null)
               {
                   children = new ObservableCollection<IPlacementElement>();
                   if (LeftEdge != null)
                   {
                       LeftEdge.Parent = this;
                       children.Add(LeftEdge);
                   }
                   if (RightEdge != null)
                   {
                       RightEdge.Parent = this;
                       children.Add(RightEdge);
                   }
                   children.CollectionChanged += children_CollectionChanged;
               }
               return children;
           }
        }

        void children_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (var newItem in e.NewItems)
                {
                    LeftWidgets widgets = newItem as LeftWidgets;
                    if (widgets != null)
                    {
                        widgets.Parent = this;
                        LeftEdge = widgets;
                        break;
                    }
                    RightWidgets widgets2 = newItem as RightWidgets;
                    if (widgets2 != null)
                    {
                        widgets2.Parent = this;
                        RightEdge = widgets2;
                        break;
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (var oldItem in e.OldItems)
                {
                    LeftWidgets widgets = oldItem as LeftWidgets;
                    if (widgets != null)
                    {
                        LeftEdge = null;
                        break;
                    }
                    RightWidgets widgets2 = oldItem as RightWidgets;
                    if (widgets2 != null)
                    {
                        RightEdge = null;
                        break;
                    }
                }
            }
        }
        #endregion

        [XmlElement("LeftEdge")]
        public LeftWidgets LeftEdge { get; set; }

        [XmlElement("RightEdge")]
        public RightWidgets RightEdge { get; set; } 
    }

    public class LeftWidgets:WidgetsContainer
    { 
    }

    public class RightWidgets : WidgetsContainer
    { 
    }
     
    public class Statusbar : IPlacementElement
    {
        #region Editor
        [XmlIgnore]
        public string Name { get { return "Statusbar"; }}

        public IList<Type> ValidChildrenTypes
        {
            get { return new[] { typeof(LeftWidgets), typeof(RightWidgets) }; }
        }

        [XmlIgnore]
        public bool AllowMultiple
        {
            get { return false; }
        }

        [XmlIgnore]
         public IPlacementElement Parent { get; internal set; }
        
        private ObservableCollection<IPlacementElement> children;
        [XmlIgnore]
        public ObservableCollection<IPlacementElement> Children
        {
            get
            {
                if (children == null)
                {
                    children = new ObservableCollection<IPlacementElement>();
                    if (LeftSide != null)
                    {
                        LeftSide.Parent = this;
                        children.Add(LeftSide);
                    }
                    if (RightSide != null)
                    {
                        RightSide.Parent = this;
                        children.Add(RightSide);
                    }
                    children.CollectionChanged += children_CollectionChanged;
                }
                return children;
            }
        }

        void children_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (var newItem in e.NewItems)
                {
                    LeftWidgets widgets = newItem as LeftWidgets;
                    if (widgets != null)
                    {
                        widgets.Parent = this;
                        LeftSide = widgets;
                        break;
                    }
                    RightWidgets widgets2 = newItem as RightWidgets;
                    if (widgets2 != null)
                    {
                        widgets2.Parent = this;
                        RightSide = widgets2;
                        break;
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (var oldItem in e.OldItems)
                {
                    LeftWidgets widgets = oldItem as LeftWidgets;
                    if (widgets != null)
                    {
                        LeftSide = null;
                        break;
                    }
                    RightWidgets widgets2 = oldItem as RightWidgets;
                    if (widgets2 != null)
                    {
                        RightSide = null;
                        break;
                    }
                }
            }
        }
        #endregion

        [XmlElement("Left")]
        public LeftWidgets LeftSide { get; set; }

        [XmlElement("Right")]
        public RightWidgets RightSide { get; set; } 

    }

    public class SystemTray:WidgetsContainer
    { 
    }

    public class Launcherbar:IPlacementElement
    {
        [XmlIgnore]
        public string Name { get { return "Launcherbar"; } }

        public IList<Type> ValidChildrenTypes
        {
            get { return new[] { typeof(Widget), typeof(Group) }; }
        }

        [XmlIgnore]
        public bool AllowMultiple
        {
            get { return true; }
        }

        [XmlIgnore]
         public IPlacementElement Parent { get; internal set; }
        private ObservableCollection<IPlacementElement> children;
        [XmlIgnore]
        public ObservableCollection<IPlacementElement> Children
        {
            get
            {
                if (children == null)
                {
                    children = new ObservableCollection<IPlacementElement>();
                    if (Widgets != null)
                    {
                        foreach (var widget in Widgets)
                        {
                            widget.Parent = this;
                            children.Add(widget);
                        }
                    }
                    else
                    {
                        Widgets = new List<Widget>();
                    }
                    if (Groups != null)
                    {
                        foreach (var @group in Groups)
                        {
                            group.Parent = this;
                            children.Add(group);
                        }
                    }
                    else
                    {
                        Groups = new List<Group>();
                    }
                    children.CollectionChanged += children_CollectionChanged;
                }
                return children;
            }
        }

        void children_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {

                for (int i = 0; i < e.NewItems.Count; i++)
                {
                    var newItem = e.NewItems[i];
                    int newIndex = e.NewStartingIndex + i;
                    
                    Widget widget = newItem as Widget;
                    if (widget != null)
                    {
                        int realNewIndex = 0;
                        for (int j = 0; j < newIndex; j++)
                        {
                            if (Children[j] is Widget)
                            {
                                realNewIndex++;
                            }
                        }
                        widget.Parent = this;
                        Widgets.Insert(realNewIndex, widget);
                        break;
                    }
                    Group group = newItem as Group;
                    if (group != null)
                    {
                        int realNewIndex = 0;
                        for (int j = 0; j < newIndex; j++)
                        {
                            if (Children[j] is Group)
                            {
                                realNewIndex++;
                            }
                        }
                        group.Parent = this;
                        Groups.Insert(realNewIndex, group);
                        break;
                    }
 
                } 
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (var oldItem in e.OldItems)
                {
                    Widget widget = oldItem as Widget;
                    if (widget != null)
                    {
                        Widgets.Remove(widget);
                        break;
                    }
                    Group group = oldItem as Group;
                    if (group != null)
                    {
                        Groups.Remove(group);
                        break;
                    }
                }
            }
        }
        [XmlElement("Widget")]
        public List<Widget> Widgets { get; set; }

        [XmlElement("Group")]
        public List<Group> Groups { get; set; }
    }
}
