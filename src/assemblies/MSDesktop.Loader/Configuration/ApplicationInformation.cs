﻿using System;
using System.Collections.Generic; 
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.Configuration
{
	[XmlRoot("Application")]
	public class ApplicationInformation: ComplexConfigurationItem, IApplyableConfiguration
	{
        [PropertyDescription(
     Description = "Sets the name of the application used to generate logs, shell window title, splash window title and login window title. If not set, name of executing assembly would be used.",
     Example = "MSDesktop Demo Application",
     CodeWiki = "Application/Settings_Howto_Setup_Application_Information",
     SnapShots = new string[] { SnapShots.RibbonWindow , SnapShots.LauncherBarAndFloatingWindows},
     SnapShotPart = "Title")]
        public string Name { get; set; }


        [PropertyDescription(
     Description = "Sets the version of the application used to set shell window title, splash window title and login window title. If not set, version of executing assembly would be used.",
     Example = "1.0.0.0",
     CodeWiki = "Application/Settings_Howto_Setup_Application_Information",
     SnapShots = new string[] { SnapShots.SplashScreen, SnapShots.LauncherBarAndFloatingWindows },
     SnapShotPart = "Version")]
        public string Version { get; set; }


        [PropertyDescription(
     Description = "Sets the title on the Launcher bar(for LauncherBarAndFloatingWindows shell mode only) to be different from ApplicationInformation.Name",
     Example = "Welcome to MSDesktop Demo Application",
     CodeWiki = "Shells/Settings_Howto_Set_Application_Bar_Title",
     SnapShots = new string[] { SnapShots.LauncherBarAndFloatingWindows }, 
     SnapShotPart = "Title")]
        public string AppBarTitle { get; set; }

        [PropertyDescription(
Description = "Sets the title of the ribbon window(for RibbonWindow and RibbonMDI shell modes only) to be different from \"[app name] - (v[app version])\"",
SnapShots = new string[] { SnapShots.RibbonWindow },
SnapShotPart = "Title")]
        public TitleHelper MainWindowTitle { get; set; }

        [PropertyDescription(
Description = "Sets the icon of the application displayed in the task bar",
CodeWiki = "Application/Settings_Howto_Set_Application_Icon")]
        public ImageHelper IconPath { get; set; }

        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new ApplicationInformationApplier();
        }
        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }
        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }

        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {

        }
        #endregion
         
    }
}
