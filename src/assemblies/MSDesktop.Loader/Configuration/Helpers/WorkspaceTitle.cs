﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class WorkspaceTitle
    {
        [XmlAttribute]
        public string Category { get; set; }
        [XmlAttribute]
        public string Title { get; set; }

    }
}
