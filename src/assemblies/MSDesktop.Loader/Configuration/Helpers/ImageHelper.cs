﻿using System;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;
using System.Windows.Media;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class ImageHelper : ComplexConfigurationItem, INullableConfigurationItem
	{
        [PropertyDescription(
         Description = "Specifies the absolute or relative path(to the executable) of the image.")]
        [Path(PathType.Image)]
        [XmlAttribute]
        public string Path { get; set; }

        [PropertyDescription(
     Description = "Specifies whether the path points an external file, or a wpf resource located in the executable and should use relative path in this case")]
        [XmlAttribute]
        public Source Source { get; set; }

        public ImageSource ToImageSource()
        {
            if (string.IsNullOrEmpty(Path)) return null;
            return Source == Source.File
                       ? new BitmapImage(new Uri(PathUtilities.GetAbsolutePath(Path)))
                       : new BitmapImage(new Uri(PathUtilities.ConvertToResourcePath(Path), UriKind.RelativeOrAbsolute));
		}


        void INullableConfigurationItem.InitializeNullObject(object parentObject_)
        {

        }
	}

    public enum Source
    {
        File,
        Resource
    }
}
