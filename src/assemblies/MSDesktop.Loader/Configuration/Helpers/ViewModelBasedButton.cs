﻿using System.ComponentModel;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class ViewModelBasedButton
    {
        public ViewModelBasedButton()
        {
            Size = ButtonSize.Default;
            ViewLocation = InitialWindowLocation.FloatingAnywhere;
        }

        [PropertyDescription(Description = "Defines the assembly file name of the entitlement customization assembly",
    Example = "LoaderExample.exe")]
        [Path(PathType.AssemblyFile)]
        [XmlAttribute]
        public string AssemblyFile { get; set; }

        [PropertyDescription(Description = "Defines the assembly name of the entitlement customization assembly",
            Example = "LoaderExample")]
        [XmlAttribute]
        public string AssemblyName { get; set; } 

        [PropertyDescription(Description = "The type name of the view model, it must inherit from IFrameworkViewModel<TParamter>, where TParameter is the view parameter type",
     CodeWiki = "ViewModelAPI/Introduction",
     Example = "LoaderExample.ViewModelBasedAPI.SampleViewModel")] 
        [XmlAttribute]
        public string ViewModelType { get; set; }

        [PropertyDescription(Description = "The type name of the view to display the view model",
            SnapShots = new string[] { SnapShots.RibbonWindowMVVM },
            SnapShotPart = "View",
            CodeWiki = "ViewModelAPI/Introduction",
            Example = "LoaderExample.ViewModelBasedAPI.SampleView")] 
        [XmlAttribute]
        public string ViewType { get; set; }

       [PropertyDescription(Description = "The type name of the view parameter for data persistence",
     CodeWiki = "ViewModelAPI/Introduction",
            Example = "LoaderExample.ViewModelBasedAPI.SampleViewParameters")] 
        [XmlAttribute]
        public string ViewParametersType { get; set; } 

        [PropertyDescription(Description = "If true, the same view can only be opened once at the same time")]
        [XmlAttribute]
        public bool Singleton { get; set; }

        [PropertyDescription(Description = "Initial location of the view created",
ValueDescriptions = new string[]
                {
                    "Start up as a floating window(can be docked in tab) in random location",
                    "Start up as a floating window(can be docked in tab) at the current cursor place", 
                    "Start up as a floating only window(cannot be docked in tab) in random location",
                    "Start up as a floating only window(cannot be docked in tab) at the current cursor place",
                    "Start up in the new tabwell, only valid for ribbon shell",
                    "Start up in the active tab or tab specified by \"WindowTabName\", only valid for ribbon shell",
                    "Start up in the left of the active tab or tab specified by \"WindowTabName\", only valid for ribbon shell",
                    "Start up in the top of the active tab or tab specified by \"WindowTabName\", only valid for ribbon shell",
                    "Start up in the right of the active tab or tab specified by \"WindowTabName\", only valid for ribbon shell",
                    "Start up in the bottom of the active tab or tab specified by \"WindowTabName\", only valid for ribbon shell",
                })]
        [XmlAttribute]
        public InitialWindowLocation ViewLocation { get; set; }

        [PropertyDescription(Description = "Initial location of the application window in the MSDesktop layout",
            Example = "Config Viewer",
            SnapShots = new string[] { SnapShots.RibbonWindow11 },
            SnapShotPart = "Tabwell")]
        [BindableEnablement("ViewLocation", typeof(InitialTabEnablementConverter),
            "sets {0} to \"DockInActiveTab\", \"DockBottom\", \"DockTop\", \"DockLeft\" or \"DockRight\"")]
        [XmlAttribute]
        public string ViewTabName { get; set; }

        [PropertyDescription(Description = "If true, check the entitlement of the component resource specified by the ID",
     CodeWiki = "Component_Entitlements/Introduction")]
        [XmlAttribute]
        public bool CheckEntitlement { get; set; }

        [PropertyDescription(Description = "If true, the ui state of the view would be persisted",
     CodeWiki = "Component_Persistence/Persist_Window")]
        [XmlAttribute]
        public bool ViewStatePersistable { get; set; }
         
        [PropertyDescription(Description = "Specifies the id of the button for invoking the view",
 CodeWiki = "ViewModelAPI/Introduction")]
        [XmlAttribute]
        public string ID { get; set; }

        [PropertyDescription(Description = "Specifies the ribbon tab for to host the button for invoking the view, not needed for launcher bar mode",
            SnapShots = new string[]{SnapShots.RibbonWindowMVVM},
            SnapShotPart = "RibbonTab",
CodeWiki = "ViewModelAPI/Introduction")]
        [XmlAttribute]
        public string Tab { get; set; }

        [PropertyDescription(Description = "Specifies the ribbon group/launcher bar group for to host the button for invoking the view",
            SnapShots = new string[] { SnapShots.RibbonWindowMVVM, SnapShots.LauncherBarMVVM },
            SnapShotPart = "Group",
CodeWiki = "ViewModelAPI/Introduction")]
        [XmlAttribute]
        public string Group { get; set; }

        [PropertyDescription(Description = "Specifies text for the button for invoking the view, and the title of the view",
           SnapShots = new string[] { SnapShots.RibbonWindowMVVM },
           SnapShotPart = "Text", 
          CodeWiki = "ViewModelAPI/Introduction")]
        [XmlAttribute]
        public string Text { get; set; }

        [PropertyDescription(Description = "Specifies icon for the button for invoking the view",
           SnapShots = new string[] { SnapShots.RibbonWindowMVVM },
           SnapShotPart = "Image", 
CodeWiki = "ViewModelAPI/Introduction")]
        public ImageHelper Image { get; set; }

        [PropertyDescription(Description = "Specifies size(large or small) for the button(only configurable for buttons on ribbon) for invoking the view",
           SnapShots = new string[] { SnapShots.RibbonWindowMVVMLarge, SnapShots.RibbonWindowMVVM },
           SnapShotPart = "Image", 
           SnapShotPartForComparison = true,
CodeWiki = "ViewModelAPI/Introduction")]
        [DefaultValue(ButtonSize.Default)]
        [XmlAttribute]
        public ButtonSize Size { get; set; }

    }

    public enum ButtonSize
    {
        Large = 0,
        Small = 1,
        Default = 2,
    }
}
