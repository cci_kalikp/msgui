﻿using System.ComponentModel; 
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class CentralizedPlacementHelper : ComplexConfigurationItem, INullableConfigurationItem
    {
        [PropertyDescription(Description = "Specifies the control placement file")]
        [Path(PathType.XmlFile)]
        [XmlAttribute]
        public string FileName { get; set; }

        [PropertyDescription(Description = "If true, resets to locking status after control placement is applied.")]
        [DefaultValue(false)]
        [XmlAttribute]
        public bool Relock { get; set; }

        public CentralizedPlacementHelper()
        {
            Relock = false;
        }

        void INullableConfigurationItem.InitializeNullObject(object parentObject_)
        {
            
        }
    }
}
