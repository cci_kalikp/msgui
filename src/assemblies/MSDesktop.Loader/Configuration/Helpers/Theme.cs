﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
	public enum Theme
	{
		Simple = 0,
		Black = 1,
		Blue = 2,
		White = 3,
        BackgroundModifiedBlue = 4, 
        None = 5,
	}
}
