﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    [Flags]
    public enum StickyWindowOptions
    {
        /// <summary>
        /// No sticky
        /// </summary>
        None = 0,
        /// <summary>
        /// stick to boundaries of other windows
        /// </summary>
        StickToOther = 1,
        /// <summary>
        /// stick to boundaries of screens
        /// </summary>
        StickToScreen = 2,
        /// <summary>
        /// stick to boundaries of other windows and screens
        /// </summary>
        StickToAll = StickToOther | StickToScreen
    } 
}
