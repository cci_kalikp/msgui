﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Input;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    [TypeConverter(typeof(KeyGestureHelperConverter))]
    [EditAsString]
	public class KeyGestureHelper:ComplexConfigurationItem
	{ 
        private static readonly KeyGestureHelperConverter converter = new KeyGestureHelperConverter();
		public Key Key { get; set; } 
        public ModifierKeys Modifiers { get; set; }

        public KeyGestureHelper()
        {
            
        }
        public KeyGestureHelper(Key key_, ModifierKeys modifier_)
        {
            Key = key_;
            Modifiers = modifier_;
        }

        public static implicit operator KeyGestureHelper(string keyGestureText_)
        {
            if (string.IsNullOrWhiteSpace(keyGestureText_)) return null;
            return (KeyGestureHelper)converter.ConvertFromString(keyGestureText_); 
        }

        public static implicit operator string(KeyGestureHelper keyGestureHelper_)
        {
            if (keyGestureHelper_ == null || (keyGestureHelper_.Key == Key.None && keyGestureHelper_.Modifiers == ModifierKeys.None)) return null;
            return converter.ConvertToString(keyGestureHelper_);
        }
        public override string ToString()
        {
            return this;
        }

        public override int GetHashCode()
        {
            return Key.GetHashCode() ^ Modifiers.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            KeyGestureHelper gesture = obj as KeyGestureHelper;
            if (gesture == null) return false;
            return this.Key.Equals(gesture.Key) && this.Modifiers.Equals(gesture.Modifiers);
        }
	}

   
    public class KeyGestureHelperConverter:TypeConverter
    { 
        private static readonly KeyConverter keyConverter = new KeyConverter();
        private static readonly ModifierKeysConverter modifierKeysConverter = new ModifierKeysConverter();
        private const char MODIFIERS_DELIMITER = '+';
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }
       
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string) && context != null && context.Instance != null)
            {
                KeyGestureHelper keyGesture = context.Instance as KeyGestureHelper;
                if (keyGesture != null)
                {
                    return ModifierKeysConverter.IsDefinedModifierKeys(keyGesture.Modifiers) && 
                        IsDefinedKey(keyGesture.Key);
                }
            }
            return false;
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object source)
        {
            if (source is string)
            {
                string text = ((string)source).Trim();
                if (text == string.Empty)
                {
                    return null;
                }
                int num = text.LastIndexOf(MODIFIERS_DELIMITER);
                string value;
                string value2;
                if (num >= 0)
                {
                    value = text.Substring(0, num);
                    value2 = text.Substring(num + 1);
                }
                else
                {
                    value = string.Empty;
                    value2 = text;
                }
                ModifierKeys modifiers = ModifierKeys.None;
                object obj = keyConverter.ConvertFrom(context, culture, value2);
                if (obj != null)
                {
                    object obj2 = modifierKeysConverter.ConvertFrom(context, culture, value);
                    if (obj2 != null)
                    {
                        modifiers = (ModifierKeys)obj2;
                    }
                    return new KeyGestureHelper((Key)obj, modifiers);
                }
            }
            throw base.GetConvertFromException(source);
        }
         
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == null)
            {
                throw new ArgumentNullException("destinationType");
            }
            if (destinationType == typeof(string))
            {
                if (value == null)
                {
                    return string.Empty;
                }
                KeyGestureHelper keyGesture = value as KeyGestureHelper;
                if (keyGesture != null)
                {
                    if (keyGesture.Key == Key.None)
                    {
                        return string.Empty;
                    }
                    string text = "";
                    string text2 = (string)keyConverter.ConvertTo(context, culture, keyGesture.Key, destinationType);
                    if (text2 != string.Empty)
                    {
                        text += (modifierKeysConverter.ConvertTo(context, culture, keyGesture.Modifiers, destinationType) as string);
                        if (text != string.Empty)
                        {
                            text += MODIFIERS_DELIMITER;
                        }
                        text += text2; 
                    }
                    return text;
                }
            }
            throw base.GetConvertToException(value, destinationType);
        }
        internal static bool IsDefinedKey(Key key)
        {
            return key >= Key.None && key <= Key.OemClear;
        }
    }
}
