﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{ 
    public class Property
    {
        [PropertyDescription(Description = "Name of the module property",
     Example = "for URLModule,CEFBasedURLModule,CEFBasedHTMLModule: url, title, location, image;\r\nfor SimpleFlexModule:url, showButtonText, showButtonLocation, buttonId")]
        [XmlAttribute]
        public string Name { get; set; }

        [PropertyDescription(Description = "value of the module property",
     Example = "file:///C:/MSDE/milosp/shadows/eai/msdotnet/msgui/build/common/ms/csc/Debug/assemblies/LiveDataModule/WebModule.html")]
        [XmlAttribute] 
        public string Value { get; set; }

        public override string ToString()
        {
            return Name + ":" + Value;
        }
    }
}
