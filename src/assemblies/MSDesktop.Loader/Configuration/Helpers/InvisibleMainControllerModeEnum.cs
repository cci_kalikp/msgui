﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    [Flags]
    public enum InvisibleMainControllerModeEnum
    {
        None = 0,
        HideWindow = 1 << 0,
        MoveWindow = 1 << 1,
        DontShowWindow = 1 << 2,
    }
}
