﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public enum ShellMode
    {
        RibbonWindow = 0,
        LauncherBarAndFloatingWindows = 1,
        LauncherBarAndWindow = 2,
        //RibbonMDI = 4,
        RibbonAndFloatingWindows = 5
    }
}
