﻿using System.Collections.Generic;
using System.ComponentModel;
using MorganStanley.Desktop.Loader.AssemblyRequesters;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class HTTPServer : EnablableComplexConfigurationItem, IApplyableConfiguration
    {
        public HTTPServer()
        {
            Port = 8080;
        }

        [PropertyDescription(Description = "Configures the port for this http server")]
        [DefaultValue(8080)]
        public int Port { get; set; }

        [PropertyDescription(Description = "If provided, enables the http request to be sent secured by using the hash seed specified")]
        public string HashSeed { get; set; }

        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new HTTPServertApplier();
        }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return new HTTPServerAssemblyRequester();
        }
        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
        }
        #endregion
    }
}
