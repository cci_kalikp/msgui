﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    /// <summary>
    /// Specifies the desired isolated process platform.
    /// </summary>
    public enum IsolatedProcessPlatform
    {
        /// <summary>
        /// Pick up the platform of the caller.
        /// </summary>
        CurrentCpu,

        /// <summary>
        /// Subsystem is to be run in 64 bit process.
        /// </summary>
        X64,

        /// <summary>
        /// Subsystem is to be run in 64 bit process.
        /// </summary>
        X86
    }
}
