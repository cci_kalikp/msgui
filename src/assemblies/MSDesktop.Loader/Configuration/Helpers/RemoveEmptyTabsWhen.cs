﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    [Flags]
    public enum RemoveEmptyTabsWhen
    {
        Never = 0,
        LayoutLoaded = 1,
        LastChildClosed = 2,
        LayoutLoadedAndLastChildClosed = LayoutLoaded | LayoutLoaded
    }
}
