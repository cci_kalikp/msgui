﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class SkipSaveProfileOnExitCondition : ComplexConfigurationItem, INullableConfigurationItem
    {
        [PropertyDescription(
Description = "If true, skip the layout saving confirmation if there is an exception loading module.")]
        [XmlAttribute]
        public bool ModuleLoadExceptioned { get; set; }

        [PropertyDescription(
Description = "If true, skip the layout saving confirmation if there is an exception resolving a module type.")]
        [XmlAttribute]
        public bool ModuleTypeResolutionFailed { get; set; }

        [PropertyDescription(
Description = "If true, skip the layout saving confirmation if certain module is not entitled.")]
        [XmlAttribute]
        public bool ModuleBlockedByEntitlement { get; set; }
         
        public void InitializeNullObject(object parentObject_)
        {
             
        }
    }
}
