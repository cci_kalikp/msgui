﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class ThresholdPolicy 
    {

        [PropertyDescription(
            Description = "Specifies the MPR target this the log message filter rule would apply to",
            Example = "/",
            CodeWiki = "Logs/Settings_Howto_Initialize_Log_Framework")]
        [XmlAttribute]
        public string Target { get; set; }

        [PropertyDescription(
    Description = "Specifies the log message filter rule for message logs from specific MPR, take, \"FixATDL/*/*:Info,IED/CFM.Infrastructure/*:Error\" means, for message sent from all produces under FixATDL meta, log it if it's >= Info level; for message sent from IED/CFM.Infrastructure, log it if it's >= Error level.",
    Example = "FixATDL/*/*:Info,IED/CFM.Infrastructure/*:Error",
    CodeWiki = "Logs/Settings_Howto_Initialize_Log_Framework")]
        [XmlAttribute]
        public string Policy { get; set; }
    }
}
