﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    [TypeConverter(typeof(SizeHelperTypeConverter))]
    [EditAsString]
    public class SizeHelper: ComplexConfigurationItem, INullableConfigurationItem
    {
        private static readonly SizeConverter converter = new SizeConverter();


        [XmlIgnore]
        public Size Size { get; set; }
         

        //legacy support
        [XmlElement(IsNullable = true)]
        public double Width
        {
            get { return Size.Width; }
            set {Size = new Size(value, Size.Height);}
        }
        [XmlElement(IsNullable = true)]
        public double Height
        {
            get { return Size.Height; }
            set { Size = new Size(Size.Width, value);}
        }

        public SizeHelper(){}
        public SizeHelper(Size size_)
        {
            Size = size_;
        }
        public static implicit operator SizeHelper(string sizeText_)
        {
            if (string.IsNullOrWhiteSpace(sizeText_)) return null;
            var convertFromString = converter.ConvertFromString(sizeText_);
            if (convertFromString != null)
            {
                return new SizeHelper((Size)convertFromString);
            }
            return null;
        }

        public static implicit operator string(SizeHelper sizeHelper_)
        { 
            if (sizeHelper_ == null || sizeHelper_.Size.IsEmpty) return null;
            return converter.ConvertToString(sizeHelper_.Size);
        }
        public override string ToString()
        {
            return this;
        }
        public override int GetHashCode()
        {
            return Size.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            SizeHelper size = obj as SizeHelper;
            if (size == null) return false;
            return this.Size.Equals(size.Size);
        }

        public void InitializeNullObject(object parentObject_)
        {
            
        }
    }

    public class SizeHelperTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return destinationType == typeof(string) || base.CanConvertTo(context, destinationType);
        }
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            string text = value as string;
            return string.IsNullOrEmpty(text) ? null : new SizeHelper(Size.Parse(text));
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            var size = value as SizeHelper;
            if (size != null && destinationType == typeof(string))
            {
                return size;
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

    }
}
