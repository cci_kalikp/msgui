﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public enum WidgetConfigEnablement
    {
        Disabled = 0,
        ProfileSpecific = 1,
        Global = 2
    }
}
