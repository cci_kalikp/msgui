﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class QuitPromptDialogBehaviour : ComplexConfigurationItem, IApplyableConfiguration
	{  
        public QuitPromptDialogBehaviour()
        {
            ShowDontAskAgainOption = ShowSaveAndQuitOption = true;
        }

        [PropertyDescription(
Description = "If true, uses legacy exception dialog and quit dialog.",
SnapShots = new string[] { SnapShots.QuitDialog, SnapShots.LegacyQuitDialog })]
        public bool UseLegacyDialog { get; set; }

        [PropertyDescription(Description = "If false, hides the \"Do not ask next time\" option on quit dialog.",
            SnapShots = new string[]{SnapShots.QuitDialog, SnapShots.LegacyQuitDialog},
            SnapShotPart = "ShowDontAskAgainOption")]
        [DefaultValue(true)]
        public bool ShowDontAskAgainOption { get; set; }

        [PropertyDescription(Description = "If false, hides the \"Save and Quit\" button on quit dialog.",
            SnapShots = new string[] { SnapShots.QuitDialog, SnapShots.LegacyQuitDialog },
            SnapShotPart = "ShowSaveAndQuitOption")]
        [DefaultValue(true)]
        public bool ShowSaveAndQuitOption { get; set; }

        [PropertyDescription(Description = "If specified, sets the time out of the quit dialog in seconds. When time out reaches, quit dialog would be closed automatically if not closed yet, and thus exit the application.",
            SnapShots = new string[] { SnapShots.QuitDialog, SnapShots.LegacyQuitDialog },
            SnapShotPart = "Timeout")]
        [XmlElement(IsNullable = true)]
        public int? Timeout { get; set; }

        [PropertyDescription(Description = "If specified, overrides the title of the quit dialog.",
         Example = "Are you sure to quit this application?",
            SnapShots = new string[] { SnapShots.QuitDialog, SnapShots.LegacyQuitDialog },
            SnapShotPart = "WarningPrompt")]
        public string WarningPrompt { get; set; }

        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new QuitPromptDialogBehaviourApplier();
        }
        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }
        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
        }
        #endregion
    }
}
