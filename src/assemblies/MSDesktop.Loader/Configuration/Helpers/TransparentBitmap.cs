﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public enum TransparentBitmap
    {
        Default = 0,
        False = 1,
        True = 2
    }
}
