﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class TitleHelper : ComplexConfigurationItem, INullableConfigurationItem
    {
        [PropertyDescription(
Description = "Sets the base part of the main window title",
SnapShots = new string[] { SnapShots.RibbonWindow5 },
SnapShotPart = "MainTitleBase")]
        [XmlAttribute]
        public string Title { get; set; }
        [PropertyDescription(
Description = "If true, the current profile name will be appended to the base title.",
SnapShots = new string[] { SnapShots.RibbonWindow5 },
SnapShotPart = "ProfileTitle")]
        [XmlAttribute]
        public bool AppendProfile { get; set; }

        void INullableConfigurationItem.InitializeNullObject(object parentObject_)
        {
             
        }
    }
}
