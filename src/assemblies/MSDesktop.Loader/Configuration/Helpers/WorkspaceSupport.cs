﻿using System.Collections.Generic;
using MorganStanley.Desktop.Loader.AssemblyRequesters;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class WorkspaceSupport : EnablableComplexConfigurationItem, IApplyableConfiguration
    {
        [PropertyDescription(Description = "Specifies the default layout(dock/tile) to be used when calling AddToWorkspace extension upon the window factory registry without the layout specified",
            CodeWiki = "Workspace_Support/General",
            SnapShots = new string[] { SnapShots.WorkspaceDockLayout, SnapShots.WorkspaceTileLayout })]
        public LayoutEngine Layout { get; set; }

        [PropertyDescription(Description = "Specifies the default workspace category when calling AddToWorkspace extension upon the window factory registry without the workspace category specified",
            CodeWiki = "Workspace_Support/General")]
        public string DefaultCategory { get; set; }

        [PropertyDescription(Description = "Specifies the default title for workspace to be created without workspace category specified",
            CodeWiki = "Workspace_Support/General",
            SnapShots = new string[]{SnapShots.WorkspaceTileLayout},
            SnapShotPart = "Title")]
        public string DefaultTitle { get; set; }

         [PropertyDescription(Description = "Specifies the default title according to workspace category for new workspace created",
            CodeWiki = "Workspace_Support/General",
            SnapShots = new string[] { SnapShots.WorkspaceTileLayout },
            SnapShotPart = "Title")]
        public List<WorkspaceTitle> DefaultTitlesByCategory { get; set; }

         [PropertyDescription(Description = "Specifies whether user would be asked to save current sub layout when closes the workspace or exit application",
    CodeWiki = "Workspace_Support/Save_On_Exit", 
    ValueDescriptions = new string[]
        {
            "If the workspace to be closed is changed, would be saved according to the options of Workspace Manager",
            "The workspace to be closed would always be saved according to the options of Workspace Manager",
            "Would always close without saving",
            "Would only be saved according to the options of Workspace Manager when the application exits and the workspace is changed"
        })]
         public LayoutLoadStrategy CloseStrategy { get; set; } 

         [PropertyDescription(Description = "If true, enables saving the sub layout of the open workspaces when saving the profile",
CodeWiki = "Workspace_Support/Save_along_with_Profile")]
         public bool SaveWorkspaceWhenSavingProfile { get; set; }

        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new WorkspaceSupportApplier();
        }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return new WorkspaceSupportAssemblyRequester();
        }
        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
        }
        #endregion
    }
}
