﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class PriorityOverride
    {
        [PropertyDescription(Description = "Specifies the group name in navigation tree to be adjusted",
      CodeWiki = "Application/Creating_custom_settings_screen",
      SnapShots = new string[] { SnapShots.OptionsDialog },
        SnapShotPart = "Group")] 
        [XmlAttribute]
        public string Group { get; set; }
        [PropertyDescription(Description = "Specifies the title under the group to be ajudsted",
    CodeWiki = "Application/Creating_custom_settings_screen",
    SnapShots = new string[] { SnapShots.OptionsDialog },
        SnapShotPart = "Title")] 
        [XmlAttribute]
        public string Title { get; set; }
        [PropertyDescription(Description = "Overrides the default priority, a positive priority would move the page above normal order, a negative one would move it below normal order",
 CodeWiki = "Application/Creating_custom_settings_screen",
 SnapShots = new string[] { SnapShots.OptionsDialog })]
        [XmlAttribute]
        public int Priority { get; set; } 
    }
}
