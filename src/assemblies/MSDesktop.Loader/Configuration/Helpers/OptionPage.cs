﻿using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class OptionPage
    {
        /// <summary>
        /// The full type name of ViewModel, must implement IOptionViewViewModel
        /// </summary>
        [PropertyDescription(Description = "Specifies the full type name of the view model, which must implements IOptionViewViewModel",
            CodeWiki = "Application/Creating_custom_settings_screen/Option_page_implemented_as_data_object_visualized_by_DataTemplate",
            Example = "LoaderExample.OptionPages.ExampleSetting, LoaderExample",
            SnapShots = new string[] { SnapShots.OptionsDialog },
            SnapShotPart = "OptionPage")]
        [XmlAttribute]
        public string ViewModelType { get; set; }

        [PropertyDescription(Description = "If provided, specifies the resource uri of the data template associated with the view model type, and the HasCustomTemplate property of the view model must return true to indicate that as well. If not specified, would use property grid to populate the option page.",
            CodeWiki = "Application/Creating_custom_settings_screen/Option_page_implemented_as_data_object_visualized_by_DataTemplate",
            Example = "/LoaderExample;component/Resources/ExampleSetting.xaml",
            SnapShots = new string[] { SnapShots.OptionsDialog },
            SnapShotPart = "OptionPage")]
        [XmlAttribute]
        public string DataTemplateResourceUri { get; set; }

        [PropertyDescription(Description = "Specifies the path in the options window navigation tree, \"/\" can be added in the path to specify multiple levels.",
        CodeWiki = "Application/Creating_custom_settings_screen/Overview",
        Example = "Loader Example/General Settings",
        SnapShots = new string[] { SnapShots.OptionsDialog},
        SnapShotPart = "Group")]
        [XmlAttribute]
        public string Path { get; set; }

        [PropertyDescription(Description = "Specifies title in the options window navigation tree",
        CodeWiki = "Application/Creating_custom_settings_screen/Overview",
        Example = "Test Setting",
        SnapShots = new string[] { SnapShots.OptionsDialog },
        SnapShotPart = "Title")] 
        [XmlAttribute]
        public string Title { get; set; }

        [PropertyDescription(Description = "If true, the option data would be persisted globally instead of layout specific.",
        CodeWiki = "Application/Creating_custom_settings_screen/Overview", 
        SnapShots = new string[] { SnapShots.OptionsDialog },
        SnapShotPart = "NavigationTree")]  
        [XmlAttribute]
        public bool Global { get; set; }
    }
}
