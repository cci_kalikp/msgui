﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Loader.AssemblyRequesters;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class CrossMachineDragAndDrop : EnablableComplexConfigurationItem, IApplyableConfiguration
    {
        [PropertyDescription(Description ="Configures the options for the ipc communication for transferring view information between MSDesktop application on different machines",
            CodeWiki = "Communications/IPC_Communication/IPC_Getting_Started")]
        public IPCOptionsHelper IPCOptions { get; set; }

        public CrossMachineDragAndDrop()
        {
            IPCOptions = new IPCOptionsHelper();
        }

        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new CrossMachineDragAndDropApplier();
        }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return new CrossMachineDragAndDropAssemblyRequester();
        }
        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
        }
        #endregion
    }
}
