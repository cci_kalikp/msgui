﻿using System;
using System.ComponentModel;
using System.Globalization; 
using System.Windows;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    [TypeConverter(typeof(PointHelperTypeConverter))]
    [EditAsString]
    public class PointHelper : ComplexConfigurationItem, INullableConfigurationItem
    {
        private static readonly PointConverter converter = new PointConverter();  
        [XmlIgnore]
        public Point Point { get; set; } 
         
        //legacy support
        [XmlElement(IsNullable = true)]
        public double X
        {
            get { return Point.X; }
            set { Point = new Point(value, Point.Y); }
        }
        [XmlElement(IsNullable = true)]
        public double Y
        {
            get { return Point.Y; }
            set { Point = new Point(Point.X,value); }
        } 

        public PointHelper()
        {
            
        }

        public PointHelper(Point point_)
        {
            Point = point_;
        }
        public static implicit operator PointHelper(string pointText_)
        {
            if (string.IsNullOrWhiteSpace(pointText_)) return null;
            var convertFromString = converter.ConvertFromString(pointText_);
            if (convertFromString != null)
            {
                return new PointHelper((Point)convertFromString);
            }
            return null;
        }

        public static implicit operator string(PointHelper pointHelper_)
        {
            if (pointHelper_ == null || pointHelper_.Point == default(Point)) return null;
            return converter.ConvertToString(pointHelper_.Point);
        }
        public override string ToString()
        {
            return this;
        }

        public override int GetHashCode()
        {
            return Point.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            PointHelper point = obj as PointHelper;
            if (point == null) return false;
            return this.Point.Equals(point.Point);
        }


        public void InitializeNullObject(object parentObject_)
        {
            
        }
    }

    public class PointHelperTypeConverter:TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return destinationType == typeof(string) || base.CanConvertTo(context, destinationType);
        }
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            string text = value as string;
            return string.IsNullOrEmpty(text) ? null : new PointHelper(Point.Parse(text));
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            var point = value as PointHelper;
            if (point != null && destinationType == typeof(string))
            {
                return point;
            }
            return base.ConvertTo(context, culture, value, destinationType);
        } 

    }
}
