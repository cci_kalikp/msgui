﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public enum EnvironProviderId
    {
        None, CommandLine, ConcordProfile,
        ConcordConfig
    }

    public enum RegionProviderId
    {
        None,
        ConcordConfig,
        ConcordProfile
    }

    public enum UsernameProviderId
    {
        None, ConcordProfile
    }

    public enum SubenvironmentProviderId
    {
        None, CommandLine
    }
}
