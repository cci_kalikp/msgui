﻿ 
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Data;
using System.Windows.Media;
using System.Xml.Serialization;
using System.Drawing; 
using Color = System.Windows.Media.Color;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    [TypeConverter(typeof(ColorHelperConverter))]
	public class ColorHelper 
    {
        private static readonly ColorHelperConverter converter = new ColorHelperConverter(); 

        public ColorHelper()
        { 
        }
		public ColorHelper(Color color_)
		{
            Color = color_;
		}

        [XmlIgnore]
        public Color Color { get; set; } 

        public static implicit operator ColorHelper(string colorText)
        {
            if (string.IsNullOrWhiteSpace(colorText)) return null;
            return converter.ConvertFromString(colorText) as ColorHelper; 
        }

        public static implicit operator string(ColorHelper colorHelper_)
        {
            if (colorHelper_ == null) return null;
            return converter.ConvertToString(colorHelper_);
        }

        public override string ToString()
        {
            return this;
        }

        public override int GetHashCode()
        {
            return Color.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            ColorHelper color = obj as ColorHelper;
            if (color == null) return false;
            return color.Color.Equals(this.Color);
        }
         
	}

    public class ColorHelperConverter:TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return destinationType == typeof(string) || base.CanConvertTo(context, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            string txt = value as string;
            if (string.IsNullOrEmpty(txt)) return null;
            var c = ColorTranslator.FromHtml(txt);
            return new ColorHelper(Color.FromArgb(c.A, c.R, c.G, c.B)); 
        }
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        { 
            ColorHelper c = value as ColorHelper;
            if (c == null) return null;
            return ColorTranslator.ToHtml(System.Drawing.Color.FromArgb(c.Color.A, c.Color.R, c.Color.G, c.Color.B));
        } 
    }
}
