﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class ExceptionDialogBehaviour : ComplexConfigurationItem, IApplyableConfiguration
	{ 
        public ExceptionDialogBehaviour()
        {
            CopyButtonVisible = true;
            EmailButtonCloses = true;
            ContinueButtonVisible = true;
            MaxAttachedLogEntryCount = 200;
        }

        [PropertyDescription(
      Description = "If true, uses legacy exception dialog and quit dialog.",
      SnapShots = new string[] { SnapShots.ExceptionDialog, SnapShots.LegacyExceptionDialog })]
        public bool UseLegacyDialog { get; set; }

        [PropertyDescription(
        Description = "If false, hides the \"Copy\" button in the exception dialog.",
        SnapShots = new string[] { SnapShots.ExceptionDialog, SnapShots.LegacyExceptionDialog },
        SnapShotPart = "CopyButton")]
        [DefaultValue(true)]
        public bool CopyButtonVisible { get; set; }

        [PropertyDescription(
Description = "If false, hides the \"Continue\" button in the exception dialog.",
SnapShots = new string[] { SnapShots.ExceptionDialog, SnapShots.LegacyExceptionDialog },
SnapShotPart = "ContinueButton")]
        [DefaultValue(true)]
        public bool ContinueButtonVisible { get; set; }
        
        [PropertyDescription(
        Description = "If false, pressing the \"Send report\" button will NOT automatically closes the exception dialog",
        SnapShots = new string[] { SnapShots.ExceptionDialog, SnapShots.LegacyExceptionDialog },
        SnapShotPart = "EmailButton")]
        [DefaultValue(true)]
        public bool EmailButtonCloses { get; set; }

        [PropertyDescription(
        Description = "If true, at the same time the exception handler address is configured and continue is enabled, skips the exception dialog and sends email directly if exception happens.")]
        public bool SuppressDialog { get; set; }

        [PropertyDescription(
 Description = "If true, and the exception handler address is configured, send email directly if exception happens and still show the exception dialog.")]
        public bool SendExceptionReportAutomatically { get; set; }

        [PropertyDescription(
   Description = "If specified, overrides the default error message on the exception dialog when exception happens.",
   Example = "We are sorry but the application encountered an unexpected error. Your unsaved data may have been damaged or lost.",
        SnapShots = new string[] { SnapShots.ExceptionDialog, SnapShots.LegacyExceptionDialog },
        SnapShotPart = "ExceptionMessage")]
        public string ExceptionMessage { get; set; }

        [PropertyDescription(
Description = "If true, will attach the current screen shot in the exception email sent")]
        public bool AttachScreenshot { get; set; }

        [PropertyDescription(
Description = "If true, will attach the latest exception log in the exception email sent")]
        public bool AttachLog { get; set; }

        [PropertyDescription(
Description = "Specifies the maximum entry count of exception log that would be attached when exception report is sent")]
        [BindableEnablement("AttachLog")]
        [DefaultValue(200)]
        public int MaxAttachedLogEntryCount { get; set; }


        [PropertyDescription(
Description = "If true, will attach not only the latest exception log, but also all other logs available, in the exception email sent")]
        public bool AttachFullLog { get; set; }


        [PropertyDescription(
Description = "If specified, overrides the maximum size of the file in bytes to be attached directly in exception email sent, default is 19MB",
Example = "10 * 1024 * 1024")]
        [XmlElement(IsNullable = true)]
        public int? MaxOutlookAttachFileSize { get; set; }

        [PropertyDescription(
Description = "If specified, overrides the maximum size of the file in bytes to be attached as attachlink in exception email sent, default is 70MB",
Example = "50 * 1024 * 1024")]
        [XmlElement(IsNullable = true)]
        public int? MaxAttachlinkFileSize { get; set; }

        [PropertyDescription(
Description = "If specified, provides shared directory to share the large file to be when size of exception email goes beyond the attachlink threshold; If not set, would be in a local temp directory",
Example = @"\\v\global\user\c\ca\caijin\logs")]
        public string LargeFileStorageDirectory { get; set; }


        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new ExceptionDialogBehaviourApplier();
        }
        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }
        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
        }
        #endregion
    }
}
