﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core; 

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class HideOrLockParameters : ComplexConfigurationItem, INullableConfigurationItem
    {
        [PropertyDescription(
            Description = "If true, hides the minimize button in views",
            SnapShots = new string[]{SnapShots.FloatingWindow},
            SnapShotPart = "MinimizeButton")]
        [XmlAttribute]
        public bool HideViewMinimizeButton { get; set; }
        [PropertyDescription(
        Description = "If true, hides the maximize button on view header",
        SnapShots = new string[] { SnapShots.FloatingWindow },
        SnapShotPart = "MaximizeButton")]
        [XmlAttribute]
        public bool HideViewMaximizeButton { get; set; }
        [PropertyDescription(
            Description = "If true, hides the tear off button on docked header",
            SnapShots = new string[] { SnapShots.RibbonWindow },
            SnapShotPart = "TearOffButton")]
        [XmlAttribute]
        public bool HideViewTearOffButton { get; set; }

        [PropertyDescription(
            Description = "If true, hides the pin/unpin button flyable view header",
            SnapShots = new string[] { SnapShots.RibbonWindow },
            SnapShotPart = "PinButton")]
        [XmlAttribute]
        public bool HideViewPinButton { get; set; }

        [PropertyDescription(
    Description = "If true, disables drag and drop views within docked tabs",
    SnapShots = new string[] { SnapShots.RibbonWindow4 },
    SnapShotPart = "DragAndDrop")]
        [XmlAttribute]
        public bool LockViewDragDrop { get; set; }

        [PropertyDescription(
            Description = "If true, hides the ribbon area",
            SnapShots = new string[] { SnapShots.RibbonWindow },
            SnapShotPart = "ToolBar")]
        [XmlAttribute]
        public bool HideRibbon { get; set; }

        [PropertyDescription(
      Description = "If true, hides the launcher bar for LauncherBarAndWindow shell",
      SnapShots = new string[] { SnapShots.LauncherBarAndWindow },
      SnapShotPart = "ToolBar")]
        [XmlAttribute]
        public bool HideLauncherBar { get; set; }

        [PropertyDescription(
      Description = "If true, hides the status bar for shell window",
      SnapShots = new string[] { SnapShots.RibbonWindow },
      SnapShotPart = "StatusBar")]
        [XmlAttribute]
        public bool HideStatusBar { get; set; }

        [PropertyDescription(
            Description = "If true, the current profile name will not be appended to the title",
      SnapShots = new string[] { SnapShots.RibbonWindow5 }, 
      SnapShotPart = "ProfileTitle")]
        [XmlAttribute]
        public bool DontAppendProfileToWindowTitle { get; set; }
        [PropertyDescription(
            Description = "If true, hides the \"Floating\" item in view header context menu, so doesn't allow making views non-dockable",
      SnapShots = new string[] { SnapShots.FloatingWindow },
      SnapShotPart = "FloatingMenuItem")]
        [XmlAttribute]
        public bool DisableViewFloatingOnly { get; set; }

        [PropertyDescription(
            Description = "If true, hides the new tabwell button",
      SnapShots = new string[] { SnapShots.RibbonWindow },
      SnapShotPart = "NewTabButton")]
        [XmlAttribute]
        public bool HideNewTabButton { get; set; }

        [PropertyDescription(
            Description = "If true, hides the context menu of the tabwell",
      SnapShots = new string[] { SnapShots.RibbonWindow2 },
      SnapShotPart = "TabwellContextMenu")]
        [XmlAttribute]
        public bool DisableTabwellContextMenu { get; set; }
        [PropertyDescription(
        Description = "If true, hides the close button on view header",
  SnapShots = new string[] { SnapShots.FloatingWindow },
  SnapShotPart = "CloseButton")]
        [XmlAttribute]
        public bool HideViewCloseButton { get; set; }
        [PropertyDescription(
        Description = "If true, hides the close related items in view header context menu",
  SnapShots = new string[] { SnapShots.FloatingWindow },
  SnapShotPart = "CloseMenuItem")]
        [XmlAttribute]
        public bool DisableViewHeaderContextMenuClose { get; set; }

        [PropertyDescription(
     Description = "If true, hides the move related items in view header context menu",
SnapShots = new string[] { SnapShots.RibbonWindow7 },
SnapShotPart = "MoveMenuItem")] 
        [XmlAttribute]
        public bool DisableSubtabContextMenuMove { get; set; }

        [PropertyDescription(
     Description = "If true, hides the \"Rename\" item in view header context menu",
SnapShots = new string[] { SnapShots.FloatingWindow },
SnapShotPart = "RenameMenuItem")]
        [XmlAttribute]
        public bool DisableViewHeaderContextMenuRename { get; set; }

        [PropertyDescription(
     Description = "If true, hides the tear off and maximize/restore button on docked header",
     SnapShots = new string[] { SnapShots.RibbonWindow },
     SnapShotPart = "TearOffAndMaximizeButton")]
        [XmlAttribute]
        public bool DisableViewTearOff { get; set; }
         
        [PropertyDescription(
Description = "If true, disable the \"Maximize\" and \"Restore\" button in docked view header",
SnapShots = new string[] { SnapShots.RibbonWindow },
SnapShotPart = "MaximizeButton")]
        [XmlAttribute]
        public bool DisableViewMaximize { get; set; }
        [PropertyDescription(
Description = "If true, disable the close related items in tabwell context menu",
SnapShots = new string[] { SnapShots.RibbonWindow2 },
SnapShotPart = "CloseMenuItem")]
        [XmlAttribute]
        public bool DisableTabClose { get; set; }
        [PropertyDescription(
Description = "If true, disable the move related items in tabwell context menu",
SnapShots = new string[] { SnapShots.RibbonWindow2 },
SnapShotPart = "MoveMenuItem")]
        [XmlAttribute]
        public bool DisableTabMove { get; set; }
        [PropertyDescription(
Description = "If true, disable the \"Rename\" item in tabwell context menu",
SnapShots = new string[] { SnapShots.RibbonWindow2 },
SnapShotPart = "RenameMenuItem")] 
        [XmlAttribute]
        public bool DisableTabRename { get; set; }
        [PropertyDescription(
Description = "If true, hides the main tabwell",
SnapShots = new string[] { SnapShots.RibbonWindow },
SnapShotPart = "Tabwell")]
        [XmlAttribute]
        public bool HideMainTabwell { get; set; }
         
        [PropertyDescription(
            Description = "If true, hides the \"Dockable\" item in view header context menu",
      SnapShots = new string[] { SnapShots.FloatingWindow },
      SnapShotPart = "DockableMenuItem")]
        [XmlAttribute]
        public bool DisableViewDocking { get; set; }
        [PropertyDescription(
            Description = "If true, hides the icon on view header",
      SnapShots = new string[] { SnapShots.FloatingWindow },
      SnapShotPart = "HeaderIcon")]
        [XmlAttribute]
        public bool HideHeaderIcon { get; set; }
        [PropertyDescription(
            Description = "If true, turns off the environment label on view header",
            CodeWiki = "Environment/Labelling",
            SnapShots = new string[]{SnapShots.FloatingWindow},
            SnapShotPart = "EnvironmentLabel",
            FlashColor = "Gold")]
        [XmlAttribute]
        public bool HideEnvironmentLabelling { get; set; }
        [PropertyDescription(
            Description = "If true, move the floating windows along with the main window", 
            SnapShots = new string[] { SnapShots.LauncherBarAndFloatingWindows },
            SnapShotPart = "FloatingWindow")]
        [XmlAttribute] 
        public bool LockRelativeWindowsPositions { get; set; }
  
        [PropertyDescription(
           Description = "If true, views can only be drag and drop onto the main area as split panes", 
           SnapShots = new string[] { SnapShots.RibbonWindow4, SnapShots.RibbonWindow6 },
           SnapShotPart = "DragAndDrop",
           SnapShotPartForComparison = true)]
        [XmlAttribute]
        public bool WorkspaceShellStyleDragDropLock { get; set; }


        [PropertyDescription(
   Description = "If true, views in split panes cannot be resized",
   SnapShots = new string[] { SnapShots.RibbonWindow3 },
   SnapShotPart = "Splitter")] 
        [XmlAttribute]
        public bool DisableResizingViews { get; set; }

        [PropertyDescription(
           Description = "If true, hides both the close button on view header and the close related items in view header context menu",
           SnapShots = new string[] { SnapShots.FloatingWindow },
           SnapShotPart = "CloseButtonAndCloseMenuItem")] 
        [XmlAttribute]
        public bool DisableViewClose { get; set; }
         
        [PropertyDescription(
   Description = "If true, hide the main status bar item on the left side of the main window",
   SnapShots = new string[] {SnapShots.RibbonWindow3 },
   SnapShotPart = "MainStatusBarItem")]
        [XmlAttribute]
        public bool HideStatusBarNotifications { get; set; }
         
        [PropertyDescription(
Description = "If true, won't close the main ribbon window when double click its orb",
SnapShots = new string[] { SnapShots.RibbonWindow },
SnapShotPart = "RibbonIcon")]
        [XmlAttribute]
        public bool DisableCloseWindowByOrbDoubleClick { get; set; }
        /// <summary>
        /// Reflects the obsolete SetLockedLayoutMode setting.
        /// </summary>
        [PropertyDescription(
           Description = "If true, hides the close, minimize, maximize, pin, tear off, floating button on view header; hides the ribbon, launcher bar, new tabwell button, tabwell context menu, profile information on ribbon window title",
           SnapShots = new string[] { SnapShots.RibbonWindow2 },
           SnapShotPart = "All")]     
        [XmlAttribute]
        public bool LegacyLockedLayoutMode { get; set; }
        /// <summary>
        /// Completely locked layout. A default layout file should be provided.
        /// </summary>
        [PropertyDescription(
           Description = "If true, hides and disables everything, except the main tabwell. A default layout file should be provided.",
           SnapShots = new string[] { SnapShots.RibbonWindow2 },
           SnapShotPart = "All2")]   
        [XmlAttribute]
        public bool LockedLayoutMode { get; set; } 
        [PropertyDescription(
           Description = "If true, hides and disables everything, except the main tabwell, ribbon, launcher bar, status bar, view closing.",
           SnapShots = new string[] { SnapShots.RibbonWindow2 },
           SnapShotPart = "All2")]   
        [XmlAttribute]
        public bool LockedLayoutSoftMode { get; set; }

        public static implicit operator HideLockUIGranularity(HideOrLockParameters parameters_)
        {
            HideLockUIGranularity granularity = HideLockUIGranularity.None;
            if (parameters_.HideViewMinimizeButton)
            {
                granularity |= HideLockUIGranularity.HideViewMinimizeButton;
            }
            if (parameters_.HideViewMaximizeButton)
            {
                granularity |= HideLockUIGranularity.HideViewMaximizeButton;
            }
            if (parameters_.HideViewTearOffButton)
            {
                granularity |= HideLockUIGranularity.HideViewTearOffButton;
            }
            if (parameters_.HideViewPinButton)
            {
                granularity |= HideLockUIGranularity.HideViewPinButton;
            }
            if (parameters_.LockViewDragDrop)
            {
                granularity |= HideLockUIGranularity.LockViewDragDrop;
            }
            if (parameters_.HideRibbon)
            {
                granularity |= HideLockUIGranularity.HideRibbon;
            }
            if (parameters_.HideLauncherBar)
            {
                granularity |= HideLockUIGranularity.HideLauncherBar;
            }
            if (parameters_.HideStatusBar)
            {
                granularity |= HideLockUIGranularity.HideStatusBar;
            }
            if (parameters_.DontAppendProfileToWindowTitle)
            {
                granularity |= HideLockUIGranularity.DontAppendProfileToWindowTitle;
            }
            if (parameters_.DisableViewFloatingOnly)
            {
                granularity |= HideLockUIGranularity.DisableViewFloatingOnly;
            }
            if (parameters_.HideNewTabButton)
            {
                granularity |= HideLockUIGranularity.HideNewTabButton;
            }
            if (parameters_.DisableTabwellContextMenu)
            {
                granularity |= HideLockUIGranularity.DisableTabwellContextMenu;
            }
            if (parameters_.HideViewCloseButton)
            {
                granularity |= HideLockUIGranularity.HideViewCloseButton;
            }
            if (parameters_.DisableViewHeaderContextMenuClose)
            {
                granularity |= HideLockUIGranularity.DisableViewHeaderContextMenuClose;
            }
            if (parameters_.DisableSubtabContextMenuMove)
            {
                granularity |= HideLockUIGranularity.DisableSubtabContextMenuMove;
            }
            if (parameters_.DisableViewHeaderContextMenuRename)
            {
                granularity |= HideLockUIGranularity.DisableViewHeaderContextMenuRename;
            }
            if (parameters_.DisableViewTearOff)
            {
                granularity |= HideLockUIGranularity.DisableViewTearOff;
            }
            if (parameters_.DisableViewMaximize)
            {
                granularity |= HideLockUIGranularity.DisableViewMaximize;
            }
            if (parameters_.DisableTabClose)
            {
                granularity |= HideLockUIGranularity.DisableTabClose;
            }
            if (parameters_.DisableTabMove)
            {
                granularity |= HideLockUIGranularity.DisableTabMove;
            }
            if (parameters_.DisableTabRename)
            {
                granularity |= HideLockUIGranularity.DisableTabRename;
            }
            if (parameters_.HideMainTabwell)
            {
                granularity |= HideLockUIGranularity.HideMainTabwell;
            }
            if (parameters_.DisableViewDocking)
            {
                granularity |= HideLockUIGranularity.DisableViewDocking;
            }
            if (parameters_.HideHeaderIcon)
            {
                granularity |= HideLockUIGranularity.HideHeaderIcon;
            }
            if (parameters_.HideEnvironmentLabelling)
            {
                granularity |= HideLockUIGranularity.HideEnvironmentLabelling;
            }
            if (parameters_.LockRelativeWindowsPositions)
            {
                granularity |= HideLockUIGranularity.LockRelativeWindowsPositions;
            } 
            if (parameters_.WorkspaceShellStyleDragDropLock)
            {
                granularity |= HideLockUIGranularity.WorkspaceShellStyleDragDropLock;
            }
            if (parameters_.DisableResizingViews)
            {
                granularity |= HideLockUIGranularity.DisableResizingViews;
            }
            if (parameters_.DisableViewClose)
            {
                granularity |= HideLockUIGranularity.DisableViewClose;
            }
            if (parameters_.HideStatusBarNotifications)
            {
                granularity |= HideLockUIGranularity.HideStatusBarNotifications;
            }
            if (parameters_.DisableCloseWindowByOrbDoubleClick)
            {
                granularity |= HideLockUIGranularity.DisableCloseWindowByOrbDoubleClick;
            }
            if (parameters_.LegacyLockedLayoutMode)
            {
                granularity |= HideLockUIGranularity.LegacyLockedLayoutMode;
            }
            if (parameters_.LockedLayoutMode)
            {
                granularity |= HideLockUIGranularity.LockedLayoutMode;
            }
            if (parameters_.LockedLayoutSoftMode)
            {
                granularity |= HideLockUIGranularity.LockedLayoutSoftMode;
            }
            return granularity;
        }


        void INullableConfigurationItem.InitializeNullObject(object parentObject_)
        {
             
        }
    }
}
