﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class LayoutPersistenceStorage : ComplexConfigurationItem, INullableConfigurationItem
    {
        [PropertyDescription(
Description = "Specifies the directory where the default layouts comes from",
Example = "Profile")]
        [XmlAttribute()]
        [Path(PathType.Directory)]
        public string PresetDirectory { get; set; }

        [PropertyDescription(
Description = @"Specifies the base directory where the user preferences layouts are persisted, if missing, use ""U:\Application Data\Morgan Stanley\[Application Name]""")]
        [XmlAttribute()]
        [Path(PathType.Directory)]
        public string BaseDirectory { get; set; }

        [PropertyDescription(
Description = @"Specifies the alternative directory where the user preferences layouts are persisted, if missing, use ""%Appdata%\Morgan Stanley\[Application Name]""")]
        [XmlAttribute()]
        [Path(PathType.Directory)]
        public string AlternativeDirectory { get; set; }

        [PropertyDescription(
Description = @"Persists each layout in single file or multiple files")]
        [XmlAttribute()]
        public bool MultipleFiles { get; set; }
         
        public void InitializeNullObject(object parentObject_)
        {
             
        }
    }

}
