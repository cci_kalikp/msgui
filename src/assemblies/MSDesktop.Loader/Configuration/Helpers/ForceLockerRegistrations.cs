﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public enum ForceLockerRegistrations
    {
        None = 0,
        MainAreaOnly = 1,
        AllAreas = 2
    }
}
