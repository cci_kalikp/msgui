﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
	public class BootModule
	{ 
        [PropertyDescription(Description = "Defines the assembly file name of the boot module",
    Example = "LoaderExample.exe")]
        [Path(PathType.AssemblyFile)]
        [XmlAttribute("AssemblyFile")]
        public string ModuleAssemblyFile { get; set; }

        [PropertyDescription(Description = "Defines the assembly name for the boot module",
    Example = "LoaderExample")] 
        [XmlAttribute("Assembly")]
        public string ModuleAssembly { get; set; }

        [PropertyDescription(Description = "Defines the type the boot module, it must implement \"IBootModule\", or implementation of \"IModule\" with constructor that accepts Framework as parameter",
    Example = "LoaderExample.BootModules.BootModule")]  
		[XmlAttribute("FQCN")]
		public string ModuleFQCN { get; set; }
	}
}
