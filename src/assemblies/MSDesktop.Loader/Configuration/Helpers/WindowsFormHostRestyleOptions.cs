﻿using System;
using System.Collections.Generic; 
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class WindowsFormHostRestyleOptions: ComplexConfigurationItem, IApplyableConfiguration
    {
        public WindowsFormHostRestyleOptions()
        {
            ForWin7Only = true;
        }
        [PropertyDescription(Description = "If true, makes sure that the margin of AppletFormHost is 0")]
        [XmlAttribute]
        public bool MarginEnabled { get; set; }

        [PropertyDescription(Description = "If true, makes sure that the font size of AppletFormHost is 11.33",
            SnapShots = new string[] { SnapShots.ConcordWindowAutoSize, SnapShots.ConcordWindowReStyle },
         SnapShotPart = "Text")]
        [XmlAttribute]
        public bool FontSizeEnabled { get; set; }

        [PropertyDescription(Description = "If true, makes sure that the font family of AppletFormHost is \"Microsoft Sans Serif\"",
         SnapShots = new string[] { SnapShots.ConcordWindowAutoSize, SnapShots.ConcordWindowReStyle },
         SnapShotPart = "Text")]
        [XmlAttribute]
        public bool FontFamilyEnabled { get; set; }

        [PropertyDescription(Description = "If false, apply the restyle even the application is NOT running on Win 7")]
        [XmlAttribute]
        public bool ForWin7Only { get; set; }

        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new WindowsFormHostRestyleOptionsApplier();
        }
        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }
        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
        }
        #endregion
    }
}
