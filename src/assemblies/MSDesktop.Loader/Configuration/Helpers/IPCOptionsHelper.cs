﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class IPCOptionsHelper:ComplexConfigurationItem
    {
        [PropertyDescription(Description = "Specifies the valid target applications for the message to be sent to",
            CodeWiki = "Communications/IPC_Communication/IPC_Getting_Started")]
        [XmlArrayItem("Target")]
        public List<IPCTargetHelper> GlobalTargeting { get; set; }
        [PropertyDescription(Description = "Specifies the CPS server name for sending messages cross machines",
            Example = "cps_host",
            CodeWiki = "Communications/IPC_Communication/IPC_Getting_Started")]
        public string Host { get; set; }
        [PropertyDescription(Description = "If true, uses kerberos for communicating with CPS server",
            CodeWiki = "Communications/IPC_Communication/IPC_Getting_Started")]
        public bool Kerberos { get; set; }
        [PropertyDescription(Description = "Specifies the CPS server port for sending messages cross machines",
            Example = "6666",
            CodeWiki = "Communications/IPC_Communication/IPC_Getting_Started")]
        public int Port { get; set; } 
    }

    public class IPCTargetHelper:ComplexConfigurationItem
    {
        [PropertyDescription(Description = "Specifies whether the target is a specific application, host or user",
        CodeWiki = "Communications/IPC_Communication/IPC_Getting_Started")]
        [XmlAttribute]
        public IPCTargetType Key { get; set; }
        [PropertyDescription(Description = "Specifies the name of application, host or user to be target",
 CodeWiki = "Communications/IPC_Communication/IPC_Getting_Started")]
        [XmlAttribute]
        public string Value { get; set; }
    }

    public enum IPCTargetType
    {
        Application,
        Host,
        User
    }
}
