﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    public class ModernThemeHelper : EnablableComplexConfigurationItem, IXmlSerializable 
    {
        public ModernThemeHelper()
        {
            AccentColor = new ColorHelper(Colors.LightSteelBlue);
        }

       [PropertyDescription(Description = "Specifies the base theme for the modern theme. The dark theme will color the body of the shell black and use white font. The light theme uses a white body color and black font",
        SnapShots = new string[] { SnapShots.ModernThemeDark, SnapShots.ModernThemeLight })]
        public BaseModernTheme BaseTheme { get; set; }
         
       [PropertyDescription(Description = "Specifies the AccentColor for the modern theme, which would be used to calculate those highlighted part",
               SnapShots = new string[] { SnapShots.ModernThemeLight },
               SnapShotPart = "AccentColor")]
       public ColorHelper AccentColor { get; set; }


       System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
       {
           return null;
       }

       void IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
       {
           if (reader.MoveToContent() == XmlNodeType.Element && reader.LocalName == "ModernTheme")
           {
               string enabledStr = reader["Enabled"] ?? "true";
               Enabled = enabledStr.ToLower() == "true";
               AccentColor = reader["AccentColor"];
               BaseTheme = (BaseModernTheme)Enum.Parse(typeof(BaseModernTheme), reader["BaseTheme"]);
               reader.Read();
           }

       }

       void IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
       {
           if (!this.Enabled)
           { 
               writer.WriteAttributeString("Enabled", this.Enabled.ToString().ToLower());
           }
           writer.WriteAttributeString("BaseTheme", BaseTheme.ToString());
           writer.WriteAttributeString("AccentColor", AccentColor); 
        
       }
    } 

    public enum BaseModernTheme
    {
        Dark,
        Light
    }
}
