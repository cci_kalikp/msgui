﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Configuration.Helpers
{
    /// <summary>
    /// Sets the load strategy for the layouts
    /// </summary>
    public enum LayoutLoadStrategy
    {
        /// <summary>
        /// Checks for changed profile
        /// </summary>
        Default = 0,

        /// <summary>
        /// Always ask before saving layout
        /// </summary>
        AlwaysAsk = 1,

        /// <summary>
        /// Never ask before saving layout
        /// </summary>
        NeverAsk = 2,
         
        /// <summary>
        /// Never ask before saving the layout, but do pop up exit window
        /// </summary>
        NeverAskButPromptForExit = 4
    }
}
