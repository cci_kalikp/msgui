﻿using System;
using System.Collections.Generic;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration
{
    public class LoggingParameters : DisablableComplexConfigurationItem, IApplyableConfiguration
    {

        [PropertyDescription(
     Description = "If specified, overrides the default directory name(%AppData%/Morgan Stanley/[App Name]/Log) that stores the logs",
     Example = "Long Application Name",
     CodeWiki = "Logs/Settings_Howto_Initialize_Log_Framework")]
        public string ApplicationName { get; set; }
         
        [PropertyDescription(
     Description = "If specified, overrides the full path of the directory log, environment variables is supported",
     Example = "%APPDATA%/Morgan Stanley/Long Application Name/Debug Log",
     CodeWiki = "Logs/Settings_Howto_Initialize_Log_Framework")]
        [Path(PathType.Directory)]
        public string Directory { get; set; }

        [PropertyDescription(
            Description = "Specifies the base name of the log file, environment variables, date format and process id format is supported",
            Example = "debug_%REGIONCODE%_%yyyyMMdd%_%processid%",
            CodeWiki = "Logs/Settings_Howto_Initialize_Log_Framework")]
        public string Basename { get; set; }

        [PropertyDescription(
            Description = "Overrides the logging rule, default loggingLevels is null. Null or empty means \"*/*/*:ALL\", it means all log calls would be recorded", 
            CodeWiki = "Logs/Settings_Howto_Initialize_Log_Framework")]
        public List<ThresholdPolicy> LoggingLevels { get; set; }

        [PropertyDescription(
            Description = "When using MSLog you have a possibility to provide a method name. MSDesktop allows you to automate this and can deduce the method name by analyzing the stack trace. Bear in mind this operation is costly so toggling auto resolve on may have impact on performance.",
            CodeWiki = "Logs/Settings_Howto_Enable_Logging_Method_Name_Autoresolve")]
        public bool AutoResolveMethodNameForLoggingEnabled { get; set; }

        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new LoggingParametersApplier();
        }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }

        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        { 
        }
        #endregion

        public const string ProcessIdParameter = "processid";
        public const string DateParameter = "yyyyMMdd"; 

    }
}
