﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;

namespace MorganStanley.Desktop.Loader.Configuration
{
	[XmlRoot("configuration")]
	public class EmptyConfiguration
	{
	    public ConfigSections configSections { get { return new ConfigSections(); } set { } }
        public MSDesktopConfiguration MSDesktop { get; set; }

        public static void GenerateExampleConfig()
        {
            var xmlSerializer = new XmlSerializer(typeof(EmptyConfiguration));
            var stringWriter = new StringWriter();
            var config = new EmptyConfiguration();
            config.MSDesktop = MSDesktopConfiguration.GenerateExampleConfiguration();

            xmlSerializer.Serialize(stringWriter, config);
            var configPath = Path.ChangeExtension(Assembly.GetExecutingAssembly().Location, "default.config");
            File.WriteAllText(configPath, stringWriter.ToString());
        }
	}

	[XmlRoot("configSections")]
	public class ConfigSections
	{
		public Section section { get { return new Section(); } set { } }
	}

	[XmlRoot("section")]
	public class Section
	{
		[XmlAttribute("name")]
		public string name { get { return "MSDesktop"; } set { } }
		[XmlAttribute("type")]
		public string type { get { return "MorganStanley.Desktop.Loader.XmlSerializerSectionHandler, MorganStanley.Desktop.Loader"; } set { } }
	}

}
