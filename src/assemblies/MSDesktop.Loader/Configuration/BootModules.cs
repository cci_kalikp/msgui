﻿using System.Collections.Generic;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.Configuration
{
    public class BootModules : ExternalComplexConfigurationItem, IApplyableConfiguration, INullableConfigurationItem
    {
        [PropertyDescription(Description = "Specifies boot modules implementing \"IBootModule\" or \"IModule\" with constructor that accepts Framework as parameter, to further customize the behavior of MSDesktop application",
            CodeWiki = "Loader/Loader")]
        [XmlElement("Module")]
        public List<BootModule> Modules { get; set; } 

        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new BootModulesApplier();
        }
        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }
        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
            var configResolved = base.Resolve() as BootModules; 
            rootConfiguration_.BootModules = configResolved;
        }
        #endregion

        void INullableConfigurationItem.InitializeNullObject(object parentObject_)
        {

        }
    }
}
