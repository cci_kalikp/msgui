﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Modules;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.Configuration
{
    [XmlRoot("Modules")]
    public class ModuleElements : ExternalComplexConfigurationItem, IApplyableConfiguration, INullableConfigurationItem
    {
        [PropertyDescription(Description = "Specifies modules implementing \"IModule\" to be loaded", CodeWiki = "Module/Add_Module_Via_App_Config")]
        [XmlElement("Module")]
        [XmlElement("HTMLModule", typeof(HTMLModule))]
        [XmlElement("URLModule", typeof(URLModule))]
        [XmlElement("ActivatedURLModule", typeof(ActivatedURLModule))]
        [XmlElement("CEFBasedHTMLModule", typeof(CEFBasedHTMLModule))]
        [XmlElement("CEFBasedURLModule", typeof(CEFBasedURLModule))]
        [XmlElement("CEFBasedActivatedURLModule", typeof(CEFBasedActivatedURLModule))]
        [XmlElement("JavaModule", typeof(JavaModule))]
        [XmlElement("JavaHandlerModule", typeof(JavaHandlerModule))]
        [XmlElement("FlexModule", typeof(FlexModule))] 
        public List<Module> Modules { get; set; } 
 
        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new ModuleElementsApplier();
        }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }

        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
            var configResolved = base.Resolve() as ModuleElements;
            if (configResolved != null && configResolved.Modules != null && configResolved.Modules.Count > 0)
            { 
                foreach (var module in configResolved.Modules)
                {
                    TypedModule typedModule = module as TypedModule;
                    if (typedModule != null)
                    {
                        typedModule.Resolve(rootConfiguration_);
                    } 
                } 
            }

            rootConfiguration_.Modules = configResolved;
        }
        #endregion

        void INullableConfigurationItem.InitializeNullObject(object parentObject_)
        {

        }
    }
}
