﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Modules;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.Configuration
{
    [XmlRoot("ExternalApplications")]
    public class ExternalApplications : ExternalComplexConfigurationItem, IApplyableConfiguration, INullableConfigurationItem
    {
  
        [XmlElement("Application")]
        public List<ExternalApplication> Applications { get; set; }  

        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new ExternalApplicationsApplier();
        }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }

        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }

        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
            var configResolved = base.Resolve() as ExternalApplications;
            if (configResolved != null && configResolved.Applications != null && configResolved.Applications.Count > 0)
            {
                if (rootConfiguration_.AdvancedFeatures == null)
                {
                    rootConfiguration_.AdvancedFeatures = new AdvancedFeatures();
                }
                if (!rootConfiguration_.AdvancedFeatures.WormholeEnabled)
                {
                    rootConfiguration_.AdvancedFeatures.WormholeEnabled = true;
                }
            }

            rootConfiguration_.ExternalApplications = configResolved;
        }
        #endregion


        void INullableConfigurationItem.InitializeNullObject(object parentObject_)
        {

        }
    }
}
