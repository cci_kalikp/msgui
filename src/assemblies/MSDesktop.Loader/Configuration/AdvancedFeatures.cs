﻿using System.Collections.Generic;
using MorganStanley.Desktop.Loader.AssemblyRequesters;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.Configuration
{
    public class AdvancedFeatures : ComplexConfigurationItem, IApplyableConfiguration
    {
        [PropertyDescription(Description = "If true, added print and print preview buttons to the application menu to print all open windows",
            SnapShots = new string[]{SnapShots.ApplicationMenu},
            SnapShotPart = "Print")]
        public bool PrintSupportEnabled { get; set; }
         
        [PropertyDescription(Description = "If true, added the button to invoke log viewer and assembly viewer to the ribbon/launcher bar",
    SnapShots = new string[] { SnapShots.RibbonWindow8 },
    SnapShotPart = "DebuggingModule",
    CodeWiki = "Debug_Module/Debug_Module")]
        public bool DebuggingEnabled { get; set; }

        [PropertyDescription(Description = "If configured, start an http server inside the application to handle view creation request and delegate communication message to modules/views",
CodeWiki = "HTTPServer/HTTPServer")]
        public HTTPServer HTTPServer { get; set; }

        [PropertyDescription(Description = "If true, provides configurable email sending client for exception email sending.\r\nThe smtp server, port, from address, to address, cc address can be configured in \"MailConfig\" concord configuration; if not configured, default settings would be used.")]
        public bool MailServiceEnabled { get; set; }

        [PropertyDescription(Description = "If true, enables Wormhole bridge for hosting Java, Adobe Air, and custom external applications within MSDesktop application",
            CodeWiki = "Bridges/Java",
            SnapShots = new string[]{"JavaWindow"},
            SnapShotPart = "JavaApplication")]
        public bool WormholeEnabled { get; set; }

        [PropertyDescription(Description = "If true, enables workspace support which can manage content in top level floating window as sub layout, dock and tile layout are supported.",
            SnapShots = new string[] { SnapShots.WorkspaceDockLayout, SnapShots.WorkspaceTileLayout },
            CodeWiki = "Workspace_Support/Overview")]
        public WorkspaceSupport WorkspaceSupport { get; set; }

        [PropertyDescription(Description = "If true, enables support for loading modules web related modules and FlexModule from configuration",
     CodeWiki =  "Bridges/JavaScript")]
        public bool WebSupportEnabled { get; set; }

        [PropertyDescription(Description = "If configured, enables the same view to be drag and drop between same MSDesktop application running on different machine.")]
        public CrossMachineDragAndDrop CrossMachineDragAndDrop { get; set; }

        [PropertyDescription(Description = "If true, logs the TierChange event of RenderCapability.")]
        public bool RenderCapabilityLoggerEnabled { get; set; }

        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new AdvancedFeaturesApplier();
        }
        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return new AdvancedFeaturesAssemblyRequester();
        }
         
        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get 
            { 
                var subConfigurations = new List<IApplyableConfiguration>();
                if (HTTPServer != null) subConfigurations.Add(HTTPServer);
                if (WorkspaceSupport != null) subConfigurations.Add(WorkspaceSupport);
                if (CrossMachineDragAndDrop != null) subConfigurations.Add(CrossMachineDragAndDrop);
                return subConfigurations;
            }
        }

        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        { 

        }
 
        #endregion 

    

    } 

}
