﻿using System;
using System.Collections.Generic; 
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.AssemblyRequesters;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration
{
    public class ViewModelBasedButtons : ExternalComplexConfigurationItem, IApplyableConfiguration, INullableConfigurationItem
    {
        [PropertyDescription(Description = "Configures MVVM based buttons to be added to chrome", CodeWiki = "ViewModelAPI/Introduction")]
        [XmlElement("Button")]
        public List<ViewModelBasedButton> Buttons { get; set; }
          
        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        { 
            return new ViewModelBasedButtonsApplier();
        }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return new ViewModelBasedButtonsAssemblyRequester();
        }

        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
            var configResolved = base.Resolve() as ViewModelBasedButtons; 
            rootConfiguration_.ViewModelBasedButtons = configResolved;
        }
        #endregion

        void INullableConfigurationItem.InitializeNullObject(object parentObject_)
        {
             
        }



    }
}
