﻿ 
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration.Entitlement
{
    public class EntitlementCustomization:ComplexConfigurationItem, INullableConfigurationItem
    {

        [PropertyDescription(Description = "Defines the assembly file name of the entitlement customization assembly",
            Example = "LoaderExample.exe")]
        [Path(PathType.AssemblyFile)]
        [XmlAttribute]
        public string CustomizationAssemblyFile { get; set; }

        [PropertyDescription(Description = "Defines the assembly name of the entitlement customization assembly",
            Example = "LoaderExample")]
        [XmlAttribute]
        public string CustomizationAssemblyName { get; set; }

        [PropertyDescription(Description = "Defines the type name of the class that provides the resource factory method",
            Example = "LoaderExample.EntitlementHelper")]
        [XmlAttribute]
        public string ResourceFactoryTypeName { get; set; }

        [PropertyDescription(Description = "Defines the method name of the resource factory method",
            Example = "GetResourceForModule")]
        [XmlAttribute]
        public string ResourceFactoryMethodName { get; set; } 

 
        void INullableConfigurationItem.InitializeNullObject(object parentObject_)
        {
             
        }
    }
}
