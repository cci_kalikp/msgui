﻿using System.ComponentModel; 
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration.Entitlement
{
    public class ModuleEntitlement : EnablableComplexConfigurationItem
    {
        public ModuleEntitlement()
        { 
            MustOptIn = true;
            LoadActionName = "Load";
            PingActionName = "Ping"; 
        }

        [PropertyDescription(
            Description = "If false, entitlement check would be done upon all modules added;\r\nIf true, entitlement check would only be done upon modules added either using \"AddModuleWithEntitlementCheck\" or through configuration and with \"CheckEntitlement\" set to true.",
            CodeWiki = "Module_entitlements/Introduction")]
        [DefaultValue(true)]
        [XmlAttribute]
        public bool MustOptIn { get; set; }

         [PropertyDescription(
          Description = "Overrides the action name used for entitling the module to be loaded",
          Example = "Execute",
            CodeWiki = "Module_entitlements/Introduction")]
        [DefaultValue("Load")]
        [XmlAttribute]
        public string LoadActionName { get; set; }

         [PropertyDescription(
          Description = "Overrides the action name used for check if the module is a entitlable resource, thus to discriminate between not entitled and no entitlement defined in the \"Customization.BlockingModuleCallback\"",
          Example = "Execute",
            CodeWiki = "Module_entitlements/Introduction")]
        [DefaultValue("Ping")]
        [XmlAttribute]
        public string PingActionName { get; set; }

        [PropertyDescription(
          Description = "Provides advanced handling for module entitlement check")]
        public ModuleEntitlementCustomization Customization { get; set; } 

    }
}
