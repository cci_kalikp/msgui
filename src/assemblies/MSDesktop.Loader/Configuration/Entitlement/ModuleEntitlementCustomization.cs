﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Configuration.Entitlement
{
    public class ModuleEntitlementCustomization:EntitlementCustomization
    {
        [PropertyDescription(Description = "Defines the type name of the class that provides the blocking module callback method",
            Example = "LoaderExample.EntitlementHelper")]
        [XmlAttribute]
        public string BlockingModuleCallbackTypeName { get; set; }

        [PropertyDescription(Description = "Defines the method name of the custom blocking handler",
            Example = "ModuleBlockedCallback",
            CodeWiki = "Module_entitlements/Custom_blocking_policies")] 
        [XmlAttribute]
        public string BlockingModuleCallbackMethodName { get; set; }

    }
}
