﻿using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Configuration.Entitlement
{

    [XmlRoot("ComponentResource")]
    public class ComponentResourceHelper
    {
        [PropertyDescription(Description = "Specifies the action allowed for this component resource",
      CodeWiki = "Entitlements",
      CodeWikiType = CodeWikiType.DNParts,
      Example = "Execute")]
        [XmlAttribute]
        public ComponentEntitlementAction Action { get; set; }

       [PropertyDescription(Description = "Specifies the name to identify this component resource, usually the module name for \"MSDesktop.Component\" resource type",
            CodeWiki = "Entitlements",
            CodeWikiType = CodeWikiType.DNParts,
            Example = "Button1")] 
        [XmlAttribute]
        public string ID { get; set; }
         
    }

    public enum ComponentEntitlementAction
    {
        Execute,
        View,
        Modify,
        Load,
        Ping
    }
}
