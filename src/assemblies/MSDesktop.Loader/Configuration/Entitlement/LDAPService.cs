﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration.Entitlement
{
    public class LDAPService : OnlineService, IApplyableConfiguration
    {
        [PropertyDescription(Description = "Specifies user name to access ldap, should be a LDAP registered ldapID",
            Example = "cn=msdotnet.directory,  ou=ldapids, o=Morgan Stanley",
     CodeWiki = "Entitlements/How_to_create_entitlement_services",
     CodeWikiType = CodeWikiType.DNParts)] 
        [XmlAttribute]
        public string LDAPUserName { get; set; }

       [PropertyDescription(Description = "Specifies password to access ldap",
            Example = "msdotnet",
     CodeWiki = "Entitlements/How_to_create_entitlement_services",
     CodeWikiType = CodeWikiType.DNParts)] 
        [XmlAttribute]
        public string LDAPPassword { get; set; }

       [PropertyDescription(Description = "Ldap lacks the concept of Action and Resource, it should be pre-setup by setting up the map from groups/cost centers to actions for each module resource",
     CodeWiki = "Entitlements/How_to_create_entitlement_services",
     CodeWikiType = CodeWikiType.DNParts)] 
        [XmlArrayItem("ModuleResource")]
        public List<ModuleResourceMapEntry> ModuleResourcesMap { get; set; }

        [PropertyDescription(Description = "Ldap lacks the concept of Action and Resource, it should be pre-setup by setting up the map from groups/cost centers to actions for each component resource",
     CodeWiki = "Entitlements/How_to_create_entitlement_services",
     CodeWikiType = CodeWikiType.DNParts)] 
        [XmlArrayItem("ComponentResource")]
        public List<ComponentResourceMapEntry> ComponentResourcesMap { get; set; }

        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new LDAPServiceApplier();
        }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }
        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
        }
        #endregion

        private EntitlementConfiguration entitlementConfiguration;
        protected override void SetParentObject(object parentObject_)
        {
            entitlementConfiguration = parentObject_ as EntitlementConfiguration;
            if (entitlementConfiguration != null)
            {
                entitlementConfiguration.ServiceTypeChanged += new EventHandler(entitlementConfiguration_ServiceTypeChanged);
            }
            base.SetParentObject(parentObject_);
        }

        void entitlementConfiguration_ServiceTypeChanged(object sender, EventArgs e)
        {
            if (entitlementConfiguration != null)
            {
                this.Enabled = entitlementConfiguration.ServiceType == EntitlementServiceType.LDAP;
            }
        }

        protected override void OnEnablementChanged()
        {
            base.OnEnablementChanged();
            if (entitlementConfiguration != null)
            {
                if (this.Enabled)
                {
                    entitlementConfiguration.ServiceType = EntitlementServiceType.LDAP;
                }
                else if (entitlementConfiguration.ServiceType == EntitlementServiceType.LDAP)
                {
                    entitlementConfiguration.ServiceType = EntitlementServiceType.None;
                }
            }
        }
    }
}
