﻿using System;
using System.Collections.Generic; 
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration.Entitlement
{
    public class OfflineService : InternalEnablableComplexConfigurationItem, IApplyableConfiguration, INullableConfigurationItem
    {
        [PropertyDescription(Description = "Offline service lacks the concept of Action and Resource, it should be pre-setup by setting up the actions for each module resource for current user",
CodeWiki = "Entitlements/How_to_create_entitlement_services",
CodeWikiType = CodeWikiType.DNParts)]
        [XmlArrayItem("ModuleResource")]
        public List<ModuleResourceHelper> ModuleResources { get; set; }

        [PropertyDescription(Description = "Offline service lacks the concept of Action and Resource, it should be pre-setup by setting up the actions for each component resource for current user",
CodeWiki = "Entitlements/How_to_create_entitlement_services",
CodeWikiType = CodeWikiType.DNParts)]
        [XmlArrayItem("ComponentResource")]
        public List<ComponentResourceHelper> ComponentResources { get; set; } 
 
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new OfflineServiceApplier();
        }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }
        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
        }
        private EntitlementConfiguration entitlementConfiguration;
        protected override void SetParentObject(object parentObject_)
        {
            entitlementConfiguration = parentObject_ as EntitlementConfiguration;
            if (entitlementConfiguration != null)
            {
                entitlementConfiguration.ServiceTypeChanged += new EventHandler(entitlementConfiguration_ServiceTypeChanged);
            }
            base.SetParentObject(parentObject_);
        }

        void entitlementConfiguration_ServiceTypeChanged(object sender, EventArgs e)
        {
            if (entitlementConfiguration != null)
            {
                this.Enabled = entitlementConfiguration.ServiceType == EntitlementServiceType.Offline;
            }
        }

        protected override void OnEnablementChanged()
        {
            base.OnEnablementChanged();
            if (entitlementConfiguration != null)
            {
                if (this.Enabled)
                {
                    entitlementConfiguration.ServiceType = EntitlementServiceType.Offline;
                }
                else if (entitlementConfiguration.ServiceType == EntitlementServiceType.Offline)
                {
                    entitlementConfiguration.ServiceType = EntitlementServiceType.None;
                }
            }
        }
    }
}
