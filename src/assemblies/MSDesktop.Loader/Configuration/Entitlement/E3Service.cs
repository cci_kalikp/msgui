﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration.Entitlement
{
    public class E3Service : OnlineService, IApplyableConfiguration
    {
        public E3Service()
        {
            Kerberos = true;
        }

        [PropertyDescription(Description = "Specifies the domain name for accessing eclipse3 entitlement serivce",
            Example = "Domain_1",
     CodeWiki = "Entitlements/How_to_create_entitlement_services",
     CodeWikiType = CodeWikiType.DNParts)]
        [XmlAttribute]
        public string DomainName { get; set; }

        [PropertyDescription(Description = "Specifies the address or address alias name(which can be used to locate the real address) for accessing eclipse3 entitlement serivce",
            Example = "e3farm-hkt:2233 or HKT_MSDotNet_MSEntitlement",
     CodeWiki = "Entitlements/How_to_create_entitlement_services",
     CodeWikiType = CodeWikiType.DNParts)]
        [XmlAttribute]
        public string AddressOrAlias { get; set; }

        [PropertyDescription(Description = "Uses Kerberos tcp or not", 
     CodeWiki = "Entitlements/How_to_create_entitlement_services",
     CodeWikiType = CodeWikiType.DNParts)] 
        [XmlAttribute]
        [DefaultValue(true)]
        public bool Kerberos { get; set; }


        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return new E3ServiceApplier();
        }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }
        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
        }
        #endregion

        private EntitlementConfiguration entitlementConfiguration;
        protected override void SetParentObject(object parentObject_)
        {
            entitlementConfiguration = parentObject_ as EntitlementConfiguration;
            if (entitlementConfiguration != null)
            {
                entitlementConfiguration.ServiceTypeChanged += new EventHandler(entitlementConfiguration_ServiceTypeChanged);
            }
            base.SetParentObject(parentObject_);
        }

        void entitlementConfiguration_ServiceTypeChanged(object sender, EventArgs e)
        {
            if (entitlementConfiguration != null)
            {
                this.Enabled = entitlementConfiguration.ServiceType == EntitlementServiceType.E3;
            }
        }

        protected override void OnEnablementChanged()
        {
            base.OnEnablementChanged();
            if (entitlementConfiguration != null)
            {
                if (this.Enabled)
                {
                    entitlementConfiguration.ServiceType = EntitlementServiceType.E3;
                }
                else if (entitlementConfiguration.ServiceType == EntitlementServiceType.E3)
                {
                    entitlementConfiguration.ServiceType = EntitlementServiceType.None;
                }
            }
        }
    }
}
