﻿
using System.ComponentModel;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.Configuration.Entitlement
{
    public abstract class OnlineService : InternalEnablableComplexConfigurationItem, IApplyableConfiguration
    {
        protected OnlineService()
        {
            CacheEnabled = true;
            RequestTimeout = 12000;
            CacheTimeout = -1;
        }

        [PropertyDescription(Description = "A resilience file can be specified to enable resilient cache, so when the remote entitlement service is down, if the query was executed before and stored in the cache.",
            CodeWiki = "Entitlements/How_to_use_resilient_cache",
            CodeWikiType = CodeWikiType.DNParts,
            Example = @"%APPDATA%\Morgan Stanley\LoaderExample\EntitlementStore.xml")]
        [Path(PathType.XmlFile)]
        [XmlAttribute]
        public string ResilienceFile { get; set; }

        [PropertyDescription(Description = "Overrides the default entitlement query request timeout in ms.",
            Example = "30000",
     CodeWiki = "Entitlements/How_to_create_entitlement_services",
     CodeWikiType = CodeWikiType.DNParts)] 
        [DefaultValue(12000)]
        [XmlAttribute]
        public int RequestTimeout { get; set; }

        [PropertyDescription(Description = "If true, enables the performance cache.", 
     CodeWiki = "Entitlements/How_to_create_entitlement_services",
     CodeWikiType = CodeWikiType.DNParts)] 
        [XmlAttribute]
        public bool CacheEnabled { get; set; }

         [PropertyDescription(Description = "Specifies the cache timeout in ms, by default the cache timeout is infinite.",
     CodeWiki = "Entitlements/How_to_create_entitlement_services",
     CodeWikiType = CodeWikiType.DNParts)]
         [BindableEnablement("CacheEnabled")]
        [XmlAttribute]
        [DefaultValue(-1)]
        public int CacheTimeout { get; set; }
         

        IConfigurationApplier IApplyableConfiguration.GetApplier()
        {
            return null;
        }

        System.Collections.Generic.IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get { return null; }
        }


        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return null;
        }
        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
        }
    }
}
