﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Configuration.Entitlement
{
    public class ComponentResourceMapEntry : ComponentResourceHelper
    {
        [PropertyDescription(Description = "Specifies the mail group names separated by \";\" to be entitled to the action for this component resource",
            Example = "msdesktop-core;msdesktop-users",
     CodeWiki = "Entitlements/How_to_create_entitlement_services",
     CodeWikiType = CodeWikiType.DNParts)]
        [XmlAttribute]
        public string MailGroups { get; set; }

        [PropertyDescription(Description = "Specifies the cost centers separated by \";\" to be entitled to the action for this component resource",
        Example = "J168",
     CodeWiki = "Entitlements/How_to_create_entitlement_services",
     CodeWikiType = CodeWikiType.DNParts)]
        [XmlAttribute]
        public string CostCenters { get; set; }
    }
}
