﻿using System.ComponentModel;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Configuration.Entitlement
{

    [XmlRoot("ModuleResource")]
    public class ModuleResourceHelper
    {
        public ModuleResourceHelper()
        {
            Action = ModuleEntitlementAction.Load;
        }

        [PropertyDescription(Description = "Specifies the action allowed for this module resource",
            CodeWiki = "Entitlements",
            CodeWikiType = CodeWikiType.DNParts,
            Example = "Load")]
        [XmlAttribute]
        [DefaultValue(ModuleEntitlementAction.Load)]
        public ModuleEntitlementAction Action { get; set; }

        [PropertyDescription(Description = "Specifies the name to identify this module resource, usually the module name for \"MSDesktop.Module\" resource type",
            CodeWiki = "Entitlements",
            CodeWikiType = CodeWikiType.DNParts,
            Example = "TestModule")]
        [XmlAttribute]
        public string Name { get; set; }

         
    }

    public enum ModuleEntitlementAction
    {
        Load,
        Ping
    }
}
