﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Configuration.Entitlement
{
    public class ComponentEntitlementCustomization:EntitlementCustomization
    {
        [PropertyDescription(
Description = "Provides custom component entitlement handler type names, separated by \";\"",
Example = "FieldSettingsEntitlementHandler;GridFieldEntitlementHandler",
CodeWiki = "Component_Entitlements/Custom_Entitlement_Handling")]
        [XmlAttribute]
        public string ComponentEntitlementHandlerTypeNames { get; set; } 

    }
}
