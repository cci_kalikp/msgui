﻿using System;
using System.Collections.Generic;
using System.ComponentModel; 
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration.Entitlement
{
    public class ComponentEntitlement : EnablableComplexConfigurationItem
    {
        public ComponentEntitlement()
        { 
            LoadActionName = "Load";
            ViewActionName = "View";
            ModifyActionName = "Modify";
            ExecuteActionName = "Execute"; 
        }

        [PropertyDescription(
   Description = "Overrides the action name used for entitling the widget/view to be loaded",
   Example = "Show")]
        [DefaultValue("Load")]
        [XmlAttribute]
        public string LoadActionName { get; set; }

         [PropertyDescription(
   Description = "Overrides the action name used for entitling the child control on view to be visible",
   Example = "Read")]
        [DefaultValue("View")]
        [XmlAttribute]
        public string ViewActionName { get; set; }

         [PropertyDescription(
Description = "Overrides the action name used for entitling the editable child control on view to be editable",
Example = "Edit")]
         [DefaultValue("Modify")]
        [XmlAttribute]
        public string ModifyActionName { get; set; }

         [PropertyDescription(
   Description = "Overrides the action name used for entitling the child button controls on view to be invokable",
   Example = "Operate")]
        [DefaultValue("Execute")]
        [XmlAttribute]
        public string ExecuteActionName { get; set; }

        [PropertyDescription(
    Description = "Provides advanced handling for component entitlement check")]
        public ComponentEntitlementCustomization Customization { get; set; } 
    }
}
