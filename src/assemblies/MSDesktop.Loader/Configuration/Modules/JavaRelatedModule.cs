﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Configuration.Modules
{
    public class JavaRelatedModule:TypedModule
    {
        public override void Resolve(MSDesktopConfiguration rootConfiguration_)
        {
            if (rootConfiguration_.AdvancedFeatures == null)
            {
                rootConfiguration_.AdvancedFeatures = new AdvancedFeatures();
            }
            if (!rootConfiguration_.AdvancedFeatures.WormholeEnabled)
            {
                rootConfiguration_.AdvancedFeatures.WormholeEnabled = true;
            }
        }

        private string path;
        [Path(PathType.JavaBatch)]
        [XmlAttribute]
        [PropertyDescription(Description = "Specifies a run script to be used to run the java application",
            CodeWiki = ModulePropertyDescriptions.JavaBridgeCodeWiki,
            Example = @"%ExampleRoot%\LoaderExample%ExeRelative%\bin\run-java2.cmd")]
        public string Path
        {
            get { return path; }
            set
            {
                path = value;
                SetProperty(ModuleProperties.Path, value);
            }
        } 
         

        private string processCommandLine;

        [PropertyDescription(Description = "If specified, the application will try to attach to a javaw.exe process with CommandLine containing the provided string",
       CodeWiki = ModulePropertyDescriptions.JavaBridgeCodeWiki,
       Example = @"-Djava.endorsed.dirs=%MSDE_IVY_ENDORSED_DIRS% -Djava.library.path=%MSDE_JNI_PATH% -cp %RUNTIME_CLASSPATH% %MAIN_CLASS% %MAIN_CLASS_ARGS%
%MSDE_LAUNCH_WRAPPER% java %JVM_ARGS% -Djava.endorsed.dirs=%MSDE_IVY_ENDORSED_DIRS% -Djava.library.path=%MSDE_JNI_PATH% -cp %RUNTIME_CLASSPATH% %MAIN_CLASS% %MAIN_CLASS_ARGS%")]
        [XmlAttribute]
        public string ProcessCommandLine
        {
            get { return processCommandLine; }
            set
            {
                processCommandLine = value;
                SetProperty(ModuleProperties.ProcessCommandLine, value);
            }
        }
        private int portNumber;

        [PropertyDescription(Description = "Specifies the tcp port number used to setup the communication between MSDesktop host application and the java application",
        CodeWiki = ModulePropertyDescriptions.JavaBridgeCodeWiki,
        Example = @"10092")]
        [XmlAttribute]
        public int PortNumber
        {
            get { return portNumber; }
            set
            {
                portNumber = value;
                if (value > 0)
                {
                    SetProperty(ModuleProperties.PortNumber, value.ToString());
                }
            }
        }
    }
}
