﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Loader.Configuration.Helpers;

namespace MorganStanley.Desktop.Loader.Configuration.Modules
{
    public static class ModuleProperties
    {
        internal const string Url = "Url";
        internal const string UrlProxyHtml = "UrlProxyHtml";
        internal const string Location = "Location";
        internal const string Title = "Title";
        internal const string Image = "Image";
        internal const string Name = "Name";
        internal const string Path = "Path";
        internal const string ProcessCommandLine = "ProcessCommandLine";
        internal const string PortNumber = "PortNumber";
        internal const string TimeoutInSeconds = "TimeoutInSeconds";
        internal const string EnableKrakenCommunication = "EnableKrakenCommunication";
        internal const string Id = "Id";

        internal static void SetProperty(this List<Property> extraProperties_, string propertyName_, string propertyValue_)
        {
            if (string.IsNullOrEmpty(propertyName_))
            {
                throw new ArgumentNullException("propertyName_");
            }
            if (string.IsNullOrEmpty(propertyValue_)) return;
            var oldProp = extraProperties_.FirstOrDefault(p_ => p_.Name == propertyName_);
            if (oldProp != null)
            {
                oldProp.Value = propertyValue_;
            }
            else
            {
                extraProperties_.Add(new Property() { Name = propertyName_, Value = propertyValue_ });
            }
        }
    }

    public static class ModulePropertyDescriptions
    {
        internal const string JavaScriptBridgeCodeWiki = "Bridges/JavaScript";
        internal const string JavaBridgeCodeWiki = "Bridges/Java";
        internal const string FlexBridgeCodeWiki = "Bridges/Flex";

        internal const string ActivatedURLModuleDescription =
            "Module that registers an open window button, which loads remote url into it until the window is activated";
        
        internal const string CEFBasedActivatedURLModuleDescription =
            "Module that registers an open window button, which loads remote url into it utilizing cef until the window is activated";

        internal const string URLModuleDescription =
            "Module that registers a button that opens a window with remote url loaded inside it";

        internal const string CEFBasedURLModuleDescription =
            "Module that registers an button that opens a window with remote url loaded inside it utilizing cef";

        internal const string FlexModuleDescription =
            "Module that loads a flex application using MSDesktop JS bridge";

        internal const string JavaHandlerModuleDescription =
            "Module that launches a java application which simply responds to commands issued by MSDesktop hosts. Only visual integration (wrapping Java windows with .NET containers) is supported in this approach";

        internal const string WebUrlDescription = "Specifies a url that can be opened when the button is clicked";
        internal const string WebUrlExample = "http://codewiki.webfarm.ms.com/msdotnet/#!/MSDesktop/Loader/Loader/";

        internal const string WebLocationDescription = "Specifies the ribbon/launcher bar path of the button to open the url defined";
        internal const string JavaLocationDescription = "Specifies the ribbon/launcher bar path of the button to open the java application defined";
        internal const string LocationExample = "Module/Special Modules";

        internal const string FlexUrlDescription =
            "Specifies a url pointing to a flex application which already utilized MSDesktop flex bridge to register some initial buttons on chrome";

        internal const string FlexUrlExample =
            @"%ExampleRoot%\IsolatedWebModulesExample%ExeRelative%\Lib\tradehistory\bin-debug\tradehistory.swf";

        internal const string WebImageDescription = "Specifies image path for the button that invokes the url";
        internal const string JavaImageDescription = "Specifies image path for the button that invokes java application";
        internal const string ImageExample = @"%ExampleRoot%\LoaderExample%ExeRelative%\Resources\app1.png";

        internal const string ActivatedURLProxyDescription =
            "If not specified, the either default url proxy html would be used, or the \"ActivatedURLProxy.html\" file found in the current directory would be used. If specified, the custom url proxy html would be used to wrap the remote url";
        internal const string URLProxyDescription =
        "If not specified, the either default url proxy html would be used, or the \"URLProxy.html\" file found in the current directory would be used. If specified, the custom url proxy html would be used to wrap the remote url";

        internal const string URLProxyExample = "MyURLProxy.html";

        internal const string ModuleNameDescription = "Specifies the unique name of module";
        internal const string ModuleNameExample = "MyModule";

        internal const string WebTitleDescription = "Specifies text of the button that invokes the url";
        internal const string JavaTitleDescription = "Specifies text of the button that invokes java application";
        internal const string TitleExample = "Open My App";

        internal const string TimeoutInSecondsDescription =
            "Overrides the default time out in seconds for starting the application";

        internal const string TimeoutInSecondsExample = "30";
    }
}
