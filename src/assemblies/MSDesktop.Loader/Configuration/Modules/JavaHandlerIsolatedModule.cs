﻿
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer; 

namespace MorganStanley.Desktop.Loader.Configuration.Modules
{
    [PropertyDescription(Description = ModulePropertyDescriptions.JavaHandlerModuleDescription, CodeWiki = ModulePropertyDescriptions.JavaBridgeCodeWiki)]
    public class JavaHandlerIsolatedModule:JavaRelatedIsolatedModule
    {    
        private string location;
        [XmlAttribute]
        [PropertyDescription(Description = ModulePropertyDescriptions.JavaLocationDescription,
            Example = ModulePropertyDescriptions.LocationExample)]
        public string Location
        {
            get { return location; }
            set
            {
                location = value;
                SetProperty(ModuleProperties.Location, value);
            }
        }

        private string title;
        [XmlAttribute]
        [PropertyDescription(Description = ModulePropertyDescriptions.JavaTitleDescription,
            Example = ModulePropertyDescriptions.TitleExample)]
        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                SetProperty(ModuleProperties.Title, value);
            }
        }

        private string image;
        [XmlAttribute]
        [Path(PathType.Image)]
        [PropertyDescription(Description = ModulePropertyDescriptions.JavaImageDescription,
            Example = ModulePropertyDescriptions.ImageExample)]
        public string Image
        {
            get { return image; }
            set
            {
                image = value;
                SetProperty(ModuleProperties.Image, value);
            }
        }
    }
}
