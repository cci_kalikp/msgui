﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Configuration.Modules
{
    public class WebRelatedModule:TypedModule
    {
        public override void Resolve(MSDesktopConfiguration rootConfiguration_)
        {
            if (rootConfiguration_.AdvancedFeatures == null)
            {
                rootConfiguration_.AdvancedFeatures = new AdvancedFeatures();
            }
            if (!rootConfiguration_.AdvancedFeatures.WebSupportEnabled)
            {
                rootConfiguration_.AdvancedFeatures.WebSupportEnabled = true;
            }
        }


        [PropertyDescription(Description = ModulePropertyDescriptions.ModuleNameDescription,
      Example = ModulePropertyDescriptions.ModuleNameExample)]
        [XmlAttribute]
        public override string ModuleName
        {
            get
            {
                return base.ModuleName;
            }
            set
            {
                base.ModuleName = value;
                SetProperty(ModuleProperties.Name, value);
            }
        }
         
    }
}
