﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Configuration.Modules
{
    [PropertyDescription(Description = ModulePropertyDescriptions.FlexModuleDescription, CodeWiki = ModulePropertyDescriptions.FlexBridgeCodeWiki)]
    public class FlexIsolatedModule : TypedIsolatedModule
    {
        public FlexIsolatedModule()
        {
            TimeoutInSeconds = 15;
        }
        private string url; 
        [Path(PathType.FlexFile)]
        [XmlAttribute]
        [PropertyDescription(Description = ModulePropertyDescriptions.FlexUrlDescription,
            CodeWiki = ModulePropertyDescriptions.FlexBridgeCodeWiki,
            Example = ModulePropertyDescriptions.FlexUrlExample)]
        public string Url
        {
            get { return url; }
            set
            {
                url = value;
                SetProperty(ModuleProperties.Url, value);
            }
        }
        private uint timeoutInSeconds;
        [PropertyDescription(Description = ModulePropertyDescriptions.TimeoutInSecondsDescription,
         Example = ModulePropertyDescriptions.TimeoutInSecondsExample)]
        [DefaultValue(15)]
        [XmlAttribute]
        public uint TimeoutInSeconds
        {
            get { return timeoutInSeconds; }
            set
            {
                timeoutInSeconds = value;
                if (value > 0)
                {
                    SetProperty(ModuleProperties.TimeoutInSeconds, value.ToString());
                }
            }
        }
    }
}
