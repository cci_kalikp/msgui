﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Configuration.Modules
{
    [PropertyDescription(Description = ModulePropertyDescriptions.URLModuleDescription)]
    public class URLIsolatedModule : TypedIsolatedModule
    { 
        private string url;
        //the web page pointed by the url must used the MSDesktop javascript bridge to create initial buttons on the chrome
        [Path(PathType.HtmlFile)]
        [XmlAttribute]
        [PropertyDescription(Description = ModulePropertyDescriptions.WebUrlDescription,
            Example = ModulePropertyDescriptions.WebUrlExample)]
        public string Url
        {
            get { return url; }
            set
            {
                url = value;
                SetProperty(ModuleProperties.Url, value); 
            }
        }
        private string urlProxyHtml;
        [Path(PathType.HtmlFile)]
        [XmlAttribute]
        [PropertyDescription(Description = ModulePropertyDescriptions.URLProxyDescription,
            Example = ModulePropertyDescriptions.URLProxyExample)]
        public string UrlProxyHtml
        {
            get { return urlProxyHtml; }
            set
            {
                urlProxyHtml = value;
                SetProperty(ModuleProperties.UrlProxyHtml, value); 
            }
        }

        private string location;
        [XmlAttribute]
        [PropertyDescription(Description = ModulePropertyDescriptions.WebLocationDescription,
            Example = ModulePropertyDescriptions.LocationExample)]
        public string Location
        {
            get { return location; }
            set
            {
                location = value;
                SetProperty(ModuleProperties.Location, value); 
            }
        }

        private string title;
        [XmlAttribute]
        [PropertyDescription(Description = ModulePropertyDescriptions.WebTitleDescription,
            Example = ModulePropertyDescriptions.TitleExample)]
        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                SetProperty(ModuleProperties.Title, value); 
            }
        }

        private string image;
        [XmlAttribute]
        [Path(PathType.Image)]
        [PropertyDescription(Description = ModulePropertyDescriptions.WebImageDescription,
            Example = ModulePropertyDescriptions.ImageExample)]
        public string Image
        {
            get { return image; }
            set
            {
                image = value;
                SetProperty(ModuleProperties.Image, value); 
            }
        }


        [PropertyDescription(Description = ModulePropertyDescriptions.ModuleNameDescription,
      Example = ModulePropertyDescriptions.ModuleNameExample)]
        [XmlAttribute]
        public override string ModuleName
        {
            get
            {
                return base.ModuleName;
            }
            set
            {
                base.ModuleName = value;
                SetProperty(ModuleProperties.Name, value); 
            }
        }
    }

    [PropertyDescription(Description = ModulePropertyDescriptions.CEFBasedURLModuleDescription)]
    public class CEFBasedURLIsolatedModule : URLIsolatedModule
    {
    }
}
