﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer; 

namespace MorganStanley.Desktop.Loader.Configuration.Modules
{
    [PropertyDescription(Description = "Module that launches a java application calls full-fledged java bridge to interface with hosting MSDesktop application.",
        CodeWiki = ModulePropertyDescriptions.JavaBridgeCodeWiki)]
    public class JavaIsolatedModule : JavaRelatedIsolatedModule
    {   
        public JavaIsolatedModule()
        {
            TimeoutInSeconds = 15;
        }
      
        private uint timeoutInSeconds;
        [PropertyDescription(Description = ModulePropertyDescriptions.TimeoutInSecondsDescription,
         CodeWiki = ModulePropertyDescriptions.JavaBridgeCodeWiki, 
         Example = ModulePropertyDescriptions.TimeoutInSecondsExample)]
        [DefaultValue(15)] 
        [XmlAttribute]
        public uint TimeoutInSeconds
        {
            get { return timeoutInSeconds; }
            set 
            { 
                timeoutInSeconds = value;
                if (value > 0)
                {
                    SetProperty(ModuleProperties.TimeoutInSeconds, value.ToString());
                }
            }
        }

        private bool enableKrakenCommunication;

        [PropertyDescription(Description = "If true, enables routed communication between the Java application and the MSDesktop host",
   CodeWiki = ModulePropertyDescriptions.JavaBridgeCodeWiki)] 
        [XmlAttribute]
        public bool EnableKrakenCommunication
        {
            get { return enableKrakenCommunication; }
            set 
            { 
                enableKrakenCommunication = value;
                SetProperty(ModuleProperties.EnableKrakenCommunication, value.ToString()); 
            }
        }
    }
}
