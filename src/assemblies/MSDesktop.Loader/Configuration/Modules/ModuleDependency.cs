﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Configuration.Modules
{
    public class ModuleDependency
    {
        [PropertyDescription(Description = "Specifies the unique name of module",
      Example = "InitModule")]
        [XmlAttribute]
        public string ModuleName { get; set; } 
    }
}
