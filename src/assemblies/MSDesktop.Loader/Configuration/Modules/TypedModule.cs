﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Helpers;

namespace MorganStanley.Desktop.Loader.Configuration.Modules
{
    public abstract class TypedModule:Module
    {
        protected TypedModule() 
        { 
            var asm = this.GetType().Assembly.GetName();
            assemblyName = asm.Name;
            assemblyVersion = asm.Version.ToString();
            var bytes = asm.GetPublicKeyToken();
            if (bytes != null && bytes.Length > 0)
            {
                publicKeyToken = bytes.Aggregate(publicKeyToken, (current_, b_) =>
current_ + string.Format("{0:x}", b_));
            }
            else
            {
                publicKeyToken = "null";
            }
            culture = asm.CultureInfo.ToString();
            if (string.IsNullOrEmpty(culture))
            {
                culture = "neutral";
            } 
        }
         
        [XmlIgnore]
        public override string AssemblyFile
        {
            get
            {
                return base.AssemblyFile;
            }
            set
            {
                base.AssemblyFile = value;
            }
        }

        private string assemblyName;
        [XmlIgnore]
        public override string AssemblyName
        {
            get
            {
                return assemblyName;
            }
            set
            {
                assemblyName = value;
            }
        }

        private string assemblyVersion;
        [XmlIgnore]
        public override string AssemblyVersion
        {
            get
            {
                return assemblyVersion;
            }
            set
            {
                assemblyVersion = value;
            }
        }

        private string culture;
        [XmlIgnore]
        public override string Culture
        {
            get
            {
                return culture;
            }
            set
            {
                culture = value;
            }
        }

        [XmlIgnore]
        public override List<ModuleDependency> Dependencies
        {
            get
            {
                return base.Dependencies;
            }
            set
            {
                base.Dependencies = value;
            }
        }

        private List<Property> extraProperties = new List<Property>();
        [XmlIgnore]
        public override List<Property> ExtraProperties
        {
            get
            {
                return extraProperties;
            }
            set
            {
                extraProperties = value;
            }
        }
         
        [XmlIgnore]
        public override string ModuleType
        {
            get
            {
                return
                    string.Format(
                        "MorganStanley.Desktop.Loader.ConfigurationAppliers.Modules.{0}Impl, MorganStanley.Desktop.Loader",
                        this.GetType().Name);
            }
            set
            {

            }
        }

        private string publicKeyToken;
        [XmlIgnore]
        public override string PublicKeyToken
        {
            get
            {
                return publicKeyToken;
            }
            set
            {
                publicKeyToken = value;
            }
        }

        [XmlIgnore]
        public override bool StartupLoaded
        {
            get
            {
                return base.StartupLoaded;
            }
            set
            {
                base.StartupLoaded = value;
            }
        }

        [XmlIgnore]
        public override bool LoadModuleBundle
        {
            get
            {
                return base.LoadModuleBundle;
            }
            set
            {
                base.LoadModuleBundle = value;
            }
        }
        public override bool ShouldSerializeAssemblyFile()
        {
            return false;
        }

        public override bool ShouldSerializeAssemblyVersion()
        {
            return false;
        }

        public override bool ShouldSerializeDependencies()
        {
            return false;
        }

        public override bool ShouldSerializeCulture()
        {
            return false;
        }

        public override bool ShouldSerializeExtraProperties()
        {
            return false;
        }

        public override bool ShouldSerializeAssemblyName()
        {
            return false;
        }

        public override bool ShouldSerializeLoadModuleBundle()
        {
            return false;
        }
        public override bool ShouldSerializePublicKeyToken()
        {
            return false;
        }

        public override bool ShouldSerializeModuleType()
        {
            return false;
        }

        public override bool ShouldSerializeStartupLoaded()
        {
            return false;
        }

        protected void SetProperty(string propertyName_, string propertyValue_)
        {
            ExtraProperties.SetProperty(propertyName_, propertyValue_);
        }

        public virtual void Resolve(MSDesktopConfiguration rootConfiguration_)
        {
            
        }
    }
}
