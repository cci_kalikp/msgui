﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Configuration.Modules
{
    [PropertyDescription(Description = "Module that loads a webpage using MSDesktop JS bridge.", CodeWiki = ModulePropertyDescriptions.JavaScriptBridgeCodeWiki)]
    public class HTMLModule : WebRelatedModule
    {   
        private string url;
        //the web page pointed by the url must used the MSDesktop javascript bridge to create initial buttons on the chrome
        [Path(PathType.HtmlFile)]
        [XmlAttribute]
        [PropertyDescription(Description = "Specifies a url pointing to a web page which already utilized MSDesktop javascript bridge to register some initial buttons on chrome",
            CodeWiki = ModulePropertyDescriptions.JavaScriptBridgeCodeWiki,
            Example = @"\\msad\root\EU\LN\lib\MSDotNet\Codewiki\MSDesktop\PROD\examples\LoaderExample\TestWebPage.html")]
        public string Url
        {
            get { return url; }
            set
            {
                url = value;
                SetProperty(ModuleProperties.Url, value); 
            }
        }
 
         
    }

    [PropertyDescription(Description = "Module that loads a webpage using MSDesktop JS bridge utilizing cef.", CodeWiki = ModulePropertyDescriptions.JavaScriptBridgeCodeWiki)]
    public class CEFBasedHTMLModule : HTMLModule
    {
    }
}
