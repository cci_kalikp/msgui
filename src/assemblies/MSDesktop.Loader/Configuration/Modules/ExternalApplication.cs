﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.Desktop.Loader.Configuration.Modules
{
    public class ExternalApplication:ComplexConfigurationItem
    {
        public ExternalApplication()
        {
            StartupTimeoutInSeconds = 60;
            WindowLocation = InitialWindowLocation.FloatingAnywhere;
        }

        [PropertyDescription(Description = "Specifies unique id of the invoker button, can be used for entitlement check and centralized control placement")]
        [XmlAttribute]
        [Order(0)]
        public string InvokerId { get; set; }
         
        [PropertyDescription(Description = "Specifies program name for the external application, also shown in window tooltip", 
            Example = @"\\san01b\DevAppsGML\dist\msdotnet\PROJ\msgui\2014.04.16\bin\MSDesktop.LogViewer\MSDesktop.LogViewer.exe",
            SnapShots = new string[]{SnapShots.RibbonWindow11},
            SnapShotPart = "ExternalApplicationTooltip")]
        [Path(PathType.Executable)]
        [XmlAttribute]
        [Order(2)]
        public string ExecutableName { get; set; }

        [PropertyDescription(Description = "Specifies command line arguments for executable, also shown in window tooltip",
            Example = @"%AppData%\Morgan Stanley\Launcher\Log\Launcher.log",
            SnapShots = new string[] { SnapShots.RibbonWindow11 },
            SnapShotPart = "ExternalApplicationTooltip")]
        [XmlAttribute]
        [Order(3)]
        public string Arguments { get; set; }

        [PropertyDescription(Description = "Sets the current directory for the executable",
Example = @"\\san01b\DevAppsGML\dist\msdotnet\PROJ\msgui\2014.04.16\bin\MSDesktop.LogViewer\")]
        [Path(PathType.Directory)]
        [XmlAttribute]
        public string WorkingDirectory { get; set; }

        [PropertyDescription(Description = "Sets the text of the invoker button",
Example = @"Open config in editor",
SnapShots = new string[] { SnapShots.RibbonWindow11 },
SnapShotPart = "ExternalApplicationTitle")]
        [XmlAttribute]
        public string Title { get; set; }

        [PropertyDescription(Description = "Specifies a string used to match part of the main window title in the external application to be started, this is usually used when the external application is started using some batch command",
Example = @"Untitled - Notepad",
SnapShots = new string[] { "ExternalApplication" },
SnapShotPart = "Title")]
        [XmlAttribute]
        public string WindowTitleSubstring { get; set; }

        [PropertyDescription(Description = "If true, kills this application when the hosting application exists")]
        [XmlAttribute]
        public bool KillOnClose { get; set; }

        [PropertyDescription(Description = "Sets the placement path of the invoker button on the ribbon/launcher bar, must use centralized control placement to define the placement for the \"InvokerId\"",
Example = @"Tools/External Applications",
SnapShots = new string[] { SnapShots.RibbonWindow11 },
SnapShotPart = "ExternalApplicationInvokerLocation")]
        [BindableEnablement("StandaloneApplication", typeof(InverseBoolConverter), "sets {0} to false")]
        [XmlAttribute]
        public string InvokerPlacementLocation { get; set; }

        [PropertyDescription(Description = "Sets the icon for the invoker button",
Example = @"Tools/External Applications",
SnapShots = new string[] { SnapShots.RibbonWindow11 },
SnapShotPart = "ExternalApplicationInvokerButton")]
        public ImageHelper InvokerIcon { get; set; }

        [PropertyDescription(Description = "Sets initial location of the application window in the MSDesktop layout",
       ValueDescriptions = new string[]
                {
                    "Start up as a floating window(can be docked in tab) in random location",
                    "Start up as a floating window(can be docked in tab) at the current cursor place", 
                    "Start up in the new tabwell, only valid for ribbon shell",
                    "Start up as a floating only window(cannot be docked in tab) in random location",
                    "Start up as a floating only window(cannot be docked in tab) at the current cursor place",
                    "Start up in the left of the active tab or tab specified by \"WindowTabName\", only valid for ribbon shell",
                    "Start up in the top of the active tab or tab specified by \"WindowTabName\", only valid for ribbon shell",
                    "Start up in the right of the active tab or tab specified by \"WindowTabName\", only valid for ribbon shell",
                    "Start up in the bottom of the active tab or tab specified by \"WindowTabName\", only valid for ribbon shell",
                    "Start up in the active tab or tab specified by \"WindowTabName\", only valid for ribbon shell",
                })] 
        [XmlAttribute]
        public InitialWindowLocation WindowLocation { get; set; }

        [PropertyDescription(Description = "Sets initial location of the application window in the MSDesktop layout",
            Example = "Config Viewer",
            SnapShots = new string[]{SnapShots.RibbonWindow11},
            SnapShotPart = "Tabwell")]
        [BindableEnablement("WindowLocation", typeof(InitialTabEnablementConverter),
            "sets {0} to \"DockInActiveTab\", \"DockBottom\", \"DockTop\", \"DockLeft\" or \"DockRight\"")]
        [XmlAttribute]
        public string WindowTabName { get; set; }

        [PropertyDescription(Description = "Overrides the time out for starting the application in seconds",
Example = @"300")]
        [XmlAttribute]
        public int StartupTimeoutInSeconds { get; set; }

        [PropertyDescription(Description = "If true, checks the entitlement of the component resource specified by the \"InvokerId\"",
     CodeWiki = "Component_Entitlements/Introduction")]
        [XmlAttribute]
        public bool CheckEntitlement { get; set; }
    }
}
