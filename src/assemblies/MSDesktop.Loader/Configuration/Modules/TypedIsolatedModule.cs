﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Helpers;

namespace MorganStanley.Desktop.Loader.Configuration.Modules
{
    public abstract class TypedIsolatedModule:IsolatedModule
    {
        protected TypedIsolatedModule()
        {
            var asm = this.GetType().Assembly.GetName();
            assembly = asm.Name; 
            this.ExtraProperties = new List<Property>();
            this.SetProperty("AssemblyLocation", GetAssemblyPath());
        }
        
        private static string GetAssemblyPath()
        {
            var path = typeof(TypedIsolatedModule).Assembly.CodeBase;
            var uri = new Uri(path);
            var file = new FileInfo(uri.LocalPath);
            return file.DirectoryName;
        }

        private string assembly;
        [XmlIgnore]
        public override string Assembly
        {
            get { return assembly; }
            set { assembly = value; }
        }

        public override bool ShouldSerializeAssembly()
        {
            return false;
        }
         
        [XmlIgnore]
        public override string ModuleType
        {
            get
            {
                return
                    string.Format(
                        "MorganStanley.Desktop.Loader.ConfigurationAppliers.Modules.{0}Impl, MorganStanley.Desktop.Loader",
                        this.GetType().Name);
            }
            set
            {

            }
        }

        public override bool ShouldSerializeModuleType()
        {
            return false;
        }

        protected void SetProperty(string propertyName_, string propertyValue_)
        {
            ExtraProperties.SetProperty(propertyName_, propertyValue_);
        }
         
    }
}
