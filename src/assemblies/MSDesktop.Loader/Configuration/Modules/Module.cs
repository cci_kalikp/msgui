﻿using System;
using System.Collections.Generic; 
using System.ComponentModel;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Helpers;

namespace MorganStanley.Desktop.Loader.Configuration.Modules
{
    //this is not needed for serialization due to the setting of XmlElement on the list defined, but is needed for config editor
    [XmlInclude(typeof(HTMLModule))]
    [XmlInclude(typeof(URLModule))]
    [XmlInclude(typeof(ActivatedURLModule))]
    [XmlInclude(typeof(CEFBasedHTMLModule))]
    [XmlInclude(typeof(CEFBasedURLModule))]
    [XmlInclude(typeof(CEFBasedActivatedURLModule))]
    [XmlInclude(typeof(JavaModule))]
    [XmlInclude(typeof(JavaHandlerModule))]
    [XmlInclude(typeof(FlexModule))]
    public class Module
    {

        [PropertyDescription(Description = ModulePropertyDescriptions.ModuleNameDescription,
      Example = ModulePropertyDescriptions.ModuleNameExample)]
        [XmlAttribute]
        public virtual string ModuleName { get; set; }
        
        public virtual bool ShouldSerializeModuleName()
        {
            return !string.IsNullOrEmpty(ModuleName);
        }

        [PropertyDescription(Description = "Specifies the type of module which implements \"IModule\"",
            Example = "LoaderExample.Modules.ExampleModule2")]
        [XmlAttribute]
        public virtual string ModuleType { get; set; }
        public virtual bool ShouldSerializeModuleType()
        {
            return !string.IsNullOrEmpty(ModuleType);
        }
        [PropertyDescription(Description = "Specifies the assembly of module type which implements \"IModule\"",
            Example = "LoaderExample")]
        [XmlAttribute]
        public virtual string AssemblyName { get; set; }
        public virtual bool ShouldSerializeAssemblyName()
        {
            return !string.IsNullOrEmpty(AssemblyName);
        }
        [PropertyDescription(Description = "Specifies the version of the assembly of module type which implements \"IModule\"",
            Example = "1.0.0.0")]
        [XmlAttribute]
        public virtual string AssemblyVersion { get; set; }
        public virtual bool ShouldSerializeAssemblyVersion()
        {
            return !string.IsNullOrEmpty(AssemblyVersion);
        }
         [PropertyDescription(Description = "Specifies the culture of the assembly of module type which implements \"IModule\"",
            Example = "neutral")]
        [XmlAttribute]
        public virtual string Culture { get; set; }
         public virtual bool ShouldSerializeCulture()
         {
             return !string.IsNullOrEmpty(Culture);
         }
         [PropertyDescription(Description = "Specifies the public token of the assembly of module type which implements \"IModule\"",
            Example = "b77a5c561934e089")]
        [XmlAttribute]
        public virtual string PublicKeyToken { get; set; }
         public virtual bool ShouldSerializePublicKeyToken()
         {
             return !string.IsNullOrEmpty(PublicKeyToken);
         }
        [PropertyDescription(Description = "Specifies the assembly file for assembly of module type which implements \"IModule\"",
            Example = "LoaderExample")]
        [Path(PathType.AssemblyFile)]
        [XmlAttribute]
        public virtual string AssemblyFile { get; set; }
        public virtual bool ShouldSerializeAssemblyFile()
        {
            return !string.IsNullOrEmpty(AssemblyFile);
        }

        private bool loadModuleBundle = true;
        [PropertyDescription(Description = "If false, will not load the msde.config file associated with the assembly",
            CodeWiki = "GetStarted/GetStarted_AR/Define_AR_Startup_Object")]
        [Path(PathType.AssemblyFile)]
        [XmlAttribute]
        [DefaultValue(true)]
        public virtual bool LoadModuleBundle {
            get { return loadModuleBundle; }
            set { loadModuleBundle = value; }
        }
        
        public virtual bool ShouldSerializeLoadModuleBundle()
        {
            return !loadModuleBundle;
        }

        private bool startupLoaded = true;
        [PropertyDescription(Description = "If false, will load the module until requested instead of at startup")]
        [XmlAttribute]
        [DefaultValue(true)]
        public virtual bool StartupLoaded
        {
            get { return startupLoaded; }
            set { startupLoaded = value; }
        }
        public virtual bool ShouldSerializeStartupLoaded()
        {
            return !startupLoaded;
        }
        [PropertyDescription(Description = "If true, this module needs to pass entitlement check before loading",
            CodeWiki = "Module_entitlements/Introduction")]
        [XmlAttribute]
        public bool CheckEntitlement { get; set; } 

        [PropertyDescription(Description = "Provides extra properties for the module, which can be used by flex modules and web modules, etc.")]
        public virtual List<Property> ExtraProperties { get; set; }
        public virtual bool ShouldSerializeExtraProperties()
        {
            return ExtraProperties != null && ExtraProperties.Count > 0;
        }
        [PropertyDescription(Description = "Specifies the modules this module depends on, to ensure that they are loaded before this module")]
        [XmlArrayItem("Dependency")]  
        public virtual List<ModuleDependency> Dependencies { get; set; }
        public virtual bool ShouldSerializeDependencies()
        {
            return Dependencies != null && Dependencies.Count > 0;
        }
        
    }
}
