﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;

namespace MorganStanley.Desktop.Loader.Configuration.Modules
{
    //this is not needed for serialization due to the setting of XmlElement on the list defined, but is needed for config editor
    [XmlInclude(typeof(HTMLIsolatedModule))]
    [XmlInclude(typeof(URLIsolatedModule))]
    [XmlInclude(typeof(ActivatedURLIsolatedModule))]
    [XmlInclude(typeof(CEFBasedHTMLIsolatedModule))]
    [XmlInclude(typeof(CEFBasedURLIsolatedModule))]
    [XmlInclude(typeof(CEFBasedActivatedURLIsolatedModule))]
    [XmlInclude(typeof(JavaIsolatedModule))]
    [XmlInclude(typeof(JavaHandlerIsolatedModule))]
    [XmlInclude(typeof(FlexIsolatedModule))]
    public class IsolatedModule : ComplexConfigurationItem
    { 
        [PropertyDescription(Description = "Specifies default assembly name of the module to be loaded in isolated process, if missing using the \"Assembly\" defined in the parent IsolatedSubsystem",
 Example = "ModuleIsolationExample")]
        [XmlAttribute]
        public virtual string Assembly { get; set; }
 
        public virtual bool ShouldSerializeAssembly()
        {
            return !string.IsNullOrEmpty(Assembly);
        }

        [PropertyDescription(Description = "Specifies type the module to be loaded in isolated process, if only one module is to be loaded in one isolated processes",
 Example = "ModuleIsolationExample.FlashPlayerBasedFlexModule")]
        [XmlAttribute]
        public virtual string ModuleType { get; set; }

        public virtual bool ShouldSerializeModuleType()
        {
            return !string.IsNullOrEmpty(Assembly);
        }

        [PropertyDescription(Description = ModulePropertyDescriptions.ModuleNameDescription,
      Example = ModulePropertyDescriptions.ModuleNameExample)]
        [XmlAttribute]
        public virtual string ModuleName { get; set; }
        public virtual bool ShouldSerializeModuleName()
        {
            return !string.IsNullOrEmpty(ModuleName);
        }
        [PropertyDescription(Description = "If true, this module needs to pass entitlement check before loading",
CodeWiki = "Module_entitlements/Introduction")]
        [XmlAttribute]
        public bool CheckEntitlement { get; set; }

        [XmlIgnore]
        public List<Property> ExtraProperties { get; set; }
    }
}
