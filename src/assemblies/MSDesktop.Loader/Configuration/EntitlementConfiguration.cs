﻿using System;
using System.Collections.Generic; 
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.AssemblyRequesters;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Entitlement;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.Desktop.Loader.Infrastructure; 
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;

namespace MorganStanley.Desktop.Loader.Configuration
{
    [XmlRoot("Entitlement")]
    public class EntitlementConfiguration : ExternalComplexConfigurationItem, IApplyableConfiguration
    { 
        [PropertyDescription(
            Description = "Controls which modules would be loaded based on queries to the entitlement service configured",
            CodeWiki = "Module_entitlements/Introduction")]
        public ModuleEntitlement ModuleEntitlement { get; set; } 

        [PropertyDescription(
     Description = "Controls if a specific window, widget or control can be loaded/enabled based on queries to the entitlement service configured",
     CodeWiki = "Component_Entitlements/Introduction")]
        public ComponentEntitlement ComponentEntitlement { get; set; }
        private EntitlementServiceType serviceType = EntitlementServiceType.None;
        public event EventHandler ServiceTypeChanged;
        protected virtual void OnServiceTypeChanged()
        {
            var copy = ServiceTypeChanged;
            if (copy != null)
            {
                copy(this, EventArgs.Empty);
            }
        }

      [PropertyDescription(
     Description = "Specifies which entitlement service to use",
     ValueDescriptions = new string[]
         {
             "No entitlement service specified",
             "Uses eclipse3 entitlement service, configure E3Service for service details",
             "Uses ldap entitlement service, configure LDAPService for service details",
             "Uses offline entitlement service, configure OfflineService for service details"
         },
     CodeWiki = "Entitlements/QuickStart",
     CodeWikiType = CodeWikiType.DNParts)]
        [OrderAttribute(0)]
        [XmlAttribute]
        public EntitlementServiceType ServiceType
        {
            get { return serviceType; }
            set
            {
                if (serviceType != value)
                {
                    serviceType = value;

                    if (E3Service != null) E3Service.Enabled = serviceType == EntitlementServiceType.E3; 
                    if (LDAPService != null) LDAPService.Enabled = serviceType == EntitlementServiceType.LDAP;
                    if (OfflineService != null) OfflineService.Enabled = serviceType == EntitlementServiceType.Offline; 
                    OnServiceTypeChanged();
                }

            } 
        }

        [PropertyDescription(
     Description = "Configures how to connect to the Eclipse3 entitlement service used for entitlement check, and how to improve the performance using cache.",
     CodeWiki = "http://wiki-eu.ms.com/MSDotnet/EntitlementsAPI?skin=msstdbare#EclipseEntitlementService",
     CodeWikiType = CodeWikiType.External)]
        [OrderAttribute(1)]
        [BindableEnablement("ServiceType", typeof(E3ServiceEnablementConverter), "sets {0} to \"E3\"")]
        public E3Service E3Service { get; set; }

         [PropertyDescription(
     Description = "Configures how to connect to the Ldap entitlement service used for entitlement check, how the ldap group entitlements are mapped to module/component resources, and how to improve the performance using cache",
     CodeWiki = "http://wiki-eu.ms.com/MSDotnet/EntitlementsAPI?skin=msstdbare#LdapEntitlementService",
     CodeWikiType = CodeWikiType.External)]
        [OrderAttribute(2)]
        [BindableEnablement("ServiceType", typeof(LDAPServiceEnablementConverter), "sets {0} to \"LDAP\"")]
        public LDAPService LDAPService { get; set; }

         [PropertyDescription(
   Description = "Configures resource authorization explictly for the offline entitlement service, usually for debugging purpose",
   CodeWiki = "http://wiki-eu.ms.com/MSDotnet/EntitlementsAPI?skin=msstdbare#OfflineEntitlementService",
   CodeWikiType = CodeWikiType.External)]
         [OrderAttribute(3)]
         [BindableEnablement("ServiceType", typeof(OfflineServiceEnablementConverter), "sets {0} to \"Offline\"")]
        public OfflineService OfflineService { get; set; }
 
        [XmlIgnore] 
        public bool EntitlementEnabled
        {
            get
            {
                return (ModuleEntitlement != null && ModuleEntitlement.Enabled) ||
                        (ComponentEntitlement != null && ComponentEntitlement.Enabled);

            }
        }

        #region IApplyableConfiguration
        IConfigurationApplier IApplyableConfiguration.GetApplier()
        { 
            return new EntitlementConfigurationApplier();
        }

        IAssemblyRequester IApplyableConfiguration.GetAssemblyRequester()
        {
            return new EntitlementConfigurationAssemblyRequester();
        }

        IList<IApplyableConfiguration> IApplyableConfiguration.SubConfigurations
        {
            get
            {
                List<IApplyableConfiguration> configurations = new List<IApplyableConfiguration>();
                switch (ServiceType)
                { 
                    case EntitlementServiceType.E3:
                        if (E3Service != null && E3Service.Enabled) configurations.Add(E3Service);
                        break;
                    case EntitlementServiceType.LDAP:
                        if (LDAPService != null && LDAPService.Enabled) configurations.Add(LDAPService);
                        break;
                    case EntitlementServiceType.Offline:
                        if (OfflineService != null && OfflineService.Enabled) configurations.Add(OfflineService);
                        break;
                    default:
                        if (E3Service != null && E3Service.Enabled) configurations.Add(E3Service);
                        else if (LDAPService != null && LDAPService.Enabled) configurations.Add(LDAPService);
                        else if (OfflineService != null && OfflineService.Enabled) configurations.Add(OfflineService);
                        break;
                }
                return configurations;
            }
        }


        void IApplyableConfiguration.Resolve(MSDesktopConfiguration rootConfiguration_)
        {
            var configResolved = base.Resolve() as EntitlementConfiguration;
            if (configResolved != null && configResolved.ServiceType != EntitlementServiceType.None)
            {
                if (rootConfiguration_.Environment == null)
                {
                    rootConfiguration_.Entitlement = new EntitlementConfiguration();
                }
            }

            rootConfiguration_.Entitlement = configResolved;
        }
        #endregion



    }


    public enum EntitlementServiceType
    {
        None,
        E3,
        LDAP,
        Offline
    }
}
