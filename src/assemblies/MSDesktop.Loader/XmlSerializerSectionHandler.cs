﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace MorganStanley.Desktop.Loader
{
    public class XmlSerializerSectionHandler : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            XPathNavigator nav = section.CreateNavigator();
            var typename = (string)nav.Evaluate("string(@type)");
            Type t = Type.GetType(typename);
            var ser = new XmlSerializer(t);
            var nr = new XmlNodeReader(section);
            var rs = new XmlReaderSettings();
            //var vr = new XmlValidatingReader(section.OuterXml, XmlNodeType.Element, null);

            rs.Schemas.Add(null, @"file://" + Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "MSDesktopConfigSchema.xsd"));
            rs.ValidationType = ValidationType.Schema;
            rs.ValidationEventHandler += rs_ValidationEventHandler;

            XmlReader vr = XmlReader.Create(nr, rs); //section.OuterXml, XmlNodeType.Element, null);
            return ser.Deserialize(vr);
        }

        private void rs_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            Debug.WriteLine(e.Message);
        }
    }
}