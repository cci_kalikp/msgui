﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;

namespace MorganStanley.Desktop.Loader.Configuration.Modules
{
    class ConfigModuleInfoInterceptor: IModuleInfoInterceptor
    {
        private readonly List<Module> modules;
        private readonly IModuleLoadInfos moduleLoadInfos;
        public ConfigModuleInfoInterceptor(List<Module> modules_, IUnityContainer container_)
        {
            modules = modules_;
            moduleLoadInfos = container_.Resolve<IModuleLoadInfos>();
        }
        public void Intercept(IList<ModuleInfo> modules_, Stack<IModuleInfoInterceptor> interceptors_)
        { 
            foreach (var moduleElement in modules)
            {
                if (modules_.FirstOrDefault(m_ => m_.ModuleName == moduleElement.ModuleName) != null) continue; 
                var def = ModuleHelper.ToModule(moduleElement, moduleLoadInfos);
                if (def == null) continue;
                ResolverModuleInfo info = new ResolverModuleInfo(def.ModuleName, def.ModuleType, def.Dependencies)
                    {
                        ExtraValues = def.ExtraValues,
                        Ref = def.Ref,
                        InitializationMode =
                            def.StartupLoaded ? InitializationMode.WhenAvailable : InitializationMode.OnDemand
                    };
                modules_.Add(info);
            }
            interceptors_.Pop().Intercept(modules_, interceptors_);
        }
    }
}
