﻿using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Infrastructure; 
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls; 

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class LoginApplier:ConfigurationApplierBase<Login>
    { 
        protected override bool ApplyCurrent(Login login_, Framework framework_)
        {
            UserNameProfileWindow.IsImpersonationEnabled = login_.AllowImpersonation; 
            if (!login_.AddDefaultProfileToAdvancedLoginScenario)
            {
                framework_.RemoveDefaultProfileElementFromAdvancedLoginScenario();
            }
            return true;
        }
    }
}
