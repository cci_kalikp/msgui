﻿ 
using MSDesktop.Wormhole; 
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Infrastructure; 

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class ExternalApplicationsApplier : ConfigurationApplierBase<ExternalApplications>
    {
        protected override bool ApplyCurrent(ExternalApplications externalApplications_, MSDotNet.MSGui.Impl.Framework framework_)
        {
            if (externalApplications_.Applications == null || externalApplications_.Applications.Count == 0) return false;
            
            framework_.LoadExternalApplications(externalApplications_);

            return true;
        }
    }
}
