﻿using System;
using System.Windows;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class ApplicationOptionsApplier:ConfigurationApplierBase<ApplicationOptions>
    {
 
        protected override bool ApplyCurrent(ApplicationOptions applicationOptions_, Framework framework_)
        {
            if (applicationOptions_.UseClassicTheme)
            {
                framework_.UseClassicOptionsWindow();
            }
            if (applicationOptions_.TypeaheadDropdownOptionsSearchEnabled)
            {
                framework_.EnableTypeaheadDropdownOptionsSearch();
            }
            if (applicationOptions_.CustomPages != null && applicationOptions_.CustomPages.Count > 0)
            {
                framework_.Bootstrapper.BeforeInitializeModules += (s_, e_) =>
                {
                    var options = e_.Container.Resolve<IApplicationOptions>();
                    var method = typeof(IApplicationOptions).GetMethod("AddViewModelBasedOptionPage");
                    foreach (var optionPage in applicationOptions_.CustomPages)
                    {
                        if (string.IsNullOrEmpty(optionPage.ViewModelType)) continue;
                        Type viewModelType = Type.GetType(optionPage.ViewModelType, false);
                        if (viewModelType == null || !typeof(IOptionViewViewModel).IsAssignableFrom(viewModelType)) continue;

                        var method2 = method.MakeGenericMethod(viewModelType);
                        method2.Invoke(options, new object[] { optionPage.Path, optionPage.Title, optionPage.Global });
                        if (!string.IsNullOrEmpty(optionPage.DataTemplateResourceUri))
                        {
                            Application.Current.Resources.MergedDictionaries.Add(
                            new ResourceDictionary
                            {
                                Source = new Uri(optionPage.DataTemplateResourceUri, UriKind.RelativeOrAbsolute)
                            });
                        }
                    }
                };

            }
            if (applicationOptions_.GroupPriorityOverrides != null)
            {
                foreach (var optionPagePriority in applicationOptions_.GroupPriorityOverrides)
                {
                    if (optionPagePriority != null && optionPagePriority.Priority != 0)
                    {
                        framework_.SetOptionPagePriority(optionPagePriority.Group, optionPagePriority.Title, optionPagePriority.Priority);
                    }
                }
            }
            return true;
        }
    }
}
