﻿using System;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class ConcordApplier:ConfigurationApplierBase<global::MorganStanley.Desktop.Loader.Configuration.Bridges.Concord>
    {
 
        protected override bool ApplyCurrent(global::MorganStanley.Desktop.Loader.Configuration.Bridges.Concord concord_, Framework framework_)
        {
            if (!concord_.Enabled) return false;
            framework_.EnableConcord();
            if (!string.IsNullOrEmpty(concord_.LogFolderName))
            {
                framework_.SetConcordLogFolderName(concord_.LogFolderName);
            }
            if (concord_.AutosizeForms)
            {
                framework_.EnableAutosizeConcordForms();
            }
            if (concord_.PreventDoubleWindowCreation)
            {
                framework_.PreventDoubleWindowCreation();
            }
            if (concord_.DoNotReparseVersionNumber)
            {
                framework_.DoNotReparseVersionNumber();
            }
            if (concord_.ConfigEnvironmentVariableExtractorEnabled)
            {
                framework_.EnableConfigEnvironmentVariableExtractor();
            }
            if (concord_.SuppressLogSetup)
            {
                framework_.EnableSuppressLogSetupForConcord();
            }
            if (concord_.SuppressConcordStatusBarItems)
            {
                framework_.SuppressConcordStatusBarItems();
            } 
            if (!string.IsNullOrEmpty(concord_.UIOnlyApplets))
            {
                string[] applets = concord_.UIOnlyApplets.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var applet in applets)
                {
                    framework_.AddToUIOnlyAppletList(applet);
                }
            } 
            return true;
        }
    }
}
