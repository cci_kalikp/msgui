﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class QuitPromptDialogBehaviourApplier:ConfigurationApplierBase<QuitPromptDialogBehaviour>
    {
        protected override bool ApplyCurrent(QuitPromptDialogBehaviour quitPromptDialogBehaviour_, Framework framework_)
        {
            if (quitPromptDialogBehaviour_.UseLegacyDialog)
            {
                framework_.EnableLegacyDialog = true;
            }
            if (!quitPromptDialogBehaviour_.ShowDontAskAgainOption || !quitPromptDialogBehaviour_.ShowSaveAndQuitOption ||
                            quitPromptDialogBehaviour_.Timeout != null || !string.IsNullOrEmpty(quitPromptDialogBehaviour_.WarningPrompt))
            {
                framework_.SetQuitPromptDialogBehaviour(quitPromptDialogBehaviour_.ShowDontAskAgainOption, quitPromptDialogBehaviour_.ShowSaveAndQuitOption,
                    quitPromptDialogBehaviour_.Timeout, quitPromptDialogBehaviour_.WarningPrompt);
                return true;
            }
            return true;
        }
    }
}
