﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Infrastructure; 
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions; 

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class MSDesktopConfigurationApplier:ConfigurationApplierBase<MSDesktopConfiguration>
    {
        internal static MSDesktopConfiguration RootConfiguration;
        protected override bool ApplyCurrent(MSDesktopConfiguration msDesktopConfiguration_, Framework framework_)
        {
            RootConfiguration = msDesktopConfiguration_;
            if (msDesktopConfiguration_.CheckForIllegalCrossThreadCalls)
            {
                framework_.EnableCheckForIllegalCrossThreadCalls(msDesktopConfiguration_.CheckForIllegalCrossThreadCalls);
            }
            if (!string.IsNullOrEmpty(msDesktopConfiguration_.ExceptionHandler))
            {
                framework_.EnableExceptionHandler(msDesktopConfiguration_.ExceptionHandler.Split(';'));
            }
            if (msDesktopConfiguration_.ReflectionModuleLoad)
            {
                framework_.EnableReflectionModuleLoad();
            }
            if (msDesktopConfiguration_.EnforceModuleLoadingDependency)
            {
                framework_.EnforceModuleLoadingDependency();
            }
            if (msDesktopConfiguration_.UsePrism)
            {
                framework_.UsePrism();
            } 
            return true;
        }
    }
}
