﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    public class TypeResolver
    {
        private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger<TypeResolver>();
        public static Type GetCustomizationType(string assemblyFile_, string assemblyName_, string typeName_)
        {
            try
            {
                Assembly assembly = null;
                if (!string.IsNullOrEmpty(assemblyFile_))
                {
                    try
                    {
                        assembly = Assembly.LoadFile(PathUtilities.GetAbsolutePath(assemblyFile_));
                        if (assembly == null)
                        {
                            logger.Error("Failed to load assembly file: " + assemblyFile_);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Failed to load assembly file: " + assemblyFile_, ex); 
                    }
                    if (assembly == null) return null;
                    return assembly.GetType(typeName_);
                }
                if (!string.IsNullOrEmpty(assemblyName_))
                {
                    try
                    {
                        assembly = Assembly.Load(assemblyName_);
                        if (assembly == null)
                        {
                            logger.Error("Failed to load assembly: " + assemblyName_);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Failed to load assembly: " + assemblyName_, ex);
                    }
                    if (assembly == null) return null;
                    return assembly.GetType(typeName_);
                }
                return Type.GetType(typeName_);

            }
            catch (Exception ex)
            {
                logger.Error("Failed to load type: " + typeName_, ex);
                return null;
            }

        }
    }
}
