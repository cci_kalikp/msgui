﻿using MSDesktop.Environment;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Impl;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class EnvironmentApplier: ConfigurationApplierBase<Environment>
    { 

        protected override bool ApplyCurrent(Environment environment_, Framework framework_)
        {
            if (environment_.EnvironProviderId != EnvironProviderId.None ||
               environment_.RegionProviderId != RegionProviderId.None ||
               environment_.UsernameProviderId != UsernameProviderId.None ||
               environment_.SubenvironmentProviderId != SubenvironmentProviderId.None)
            {
                framework_.EnableEnvironment((EnvironmentExtensions.EnvironProviderId)environment_.EnvironProviderId,
                    (EnvironmentExtensions.RegionProviderId)environment_.RegionProviderId,
                    (EnvironmentExtensions.UsernameProviderId)environment_.UsernameProviderId,
                    (EnvironmentExtensions.SubenvironmentProviderId)environment_.SubenvironmentProviderId,
                    environment_.ModernLabelsEnabled);
                if (environment_.EnvironProviderId != EnvironProviderId.None)
                {

                    framework_.SetEnvironmentLabelColorMapping(environment_.DevLabelColorResolved, environment_.QaLabelColorResolved,
                        environment_.UatLabelColorResolved, environment_.ProdLabelColorResolved);
                }
                return true;
            }
            return false;
        } 
    }
}
