﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MSDesktop.Isolation.Interfaces;
using MSDesktop.Isolation.Proxies;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.Configuration.Modules;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using FlexModule = MSDesktop.WebSupport.Flex.FlexModule;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers.Modules
{
    public class FlexIsolatedModuleImpl: FlexModule, IModuleWithExtraInformation
    {
        public FlexIsolatedModuleImpl(
            IUnityContainer container_,
            IChromeManager chromeManager_,
            IChromeRegistry chromeRegistry_,
            IApplication application_,
            IEventRegistrator eventRegistrator_)
            : base(container_, chromeManager_, chromeRegistry_, application_, eventRegistrator_)
        {
            
        }

        public void SetExtraInformation(int moduleIndex, IsolatedSubsystemSettings settings)
        {
           if (string.IsNullOrEmpty(settings.ExtraInformation)) return;
           XDocument doc = XDocument.Parse(settings.ExtraInformation);
           int index = 0;
           foreach (var moduleElement in doc.Descendants("Module"))
           {
               if (moduleIndex == index)
               {

                   var urlAttr = moduleElement.Attribute(ModuleProperties.Url);
                   if (urlAttr != null)
                   {
                       string urlLocal = PathUtilities.ReplaceEnvironmentVariables(urlAttr.Value);
                       if (!string.IsNullOrEmpty(urlLocal))
                       {
                           this.url = new Uri(urlLocal);
                       }
                   }
                   var timeoutInSecondsAttr = moduleElement.Attribute(ModuleProperties.TimeoutInSeconds);
                   if (timeoutInSecondsAttr != null && !string.IsNullOrEmpty(timeoutInSecondsAttr.Value))
                   { 
                       Int32.TryParse(timeoutInSecondsAttr.Value, out timeoutInSeconds);
                   }
                   break;
               }
               index++;
           } 
        }
         

        private Uri url;
        protected override Uri Url
        {
            get { return url; }
        }

        private int timeoutInSeconds;
        protected override int TimeoutInSeconds
        {
            get
            {
                return timeoutInSeconds;
            }
        }
    }
}
