﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MSDesktop.WebSupport;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.Configuration.Modules;
using MorganStanley.Desktop.Loader.Properties;
using CEFBasedActivatedURLModule = MSDesktop.WebSupport.CEF.CEFBasedActivatedURLModule;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers.Modules
{
    public class CEFBasedActivatedURLModuleImpl: CEFBasedActivatedURLModule
    {
        public CEFBasedActivatedURLModuleImpl(IUnityContainer container_)
            : base(container_)
        {
            
        }

        protected override string Name
        {
            get { return WebSupportExtensions.Registry[this].ExtraInfo[ModuleProperties.Name]; }
        }


        protected override string GetURLProxyHtml()
        {
            string urlProxyHtml = WebSupportExtensions.Registry[this].ExtraInfo.ContainsKey(ModuleProperties.UrlProxyHtml)
                                      ? WebSupportExtensions.Registry[this].ExtraInfo[ModuleProperties.UrlProxyHtml]
                                      : "ActivatedURLProxy.html";
            if (File.Exists(urlProxyHtml))
            {
                return File.ReadAllText(urlProxyHtml);
            }
            return Resources.URLActivatedProxy; 
        }
    }
}
