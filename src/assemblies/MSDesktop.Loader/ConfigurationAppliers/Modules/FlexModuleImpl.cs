﻿using System;
using MSDesktop.WebSupport;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.Configuration.Modules;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using FlexModule = MSDesktop.WebSupport.Flex.FlexModule;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers.Modules
{
    public class FlexModuleImpl: FlexModule
    {
        public FlexModuleImpl(
            IUnityContainer container_,
            IChromeManager chromeManager_,
            IChromeRegistry chromeRegistry_,
            IApplication application_,
            IEventRegistrator eventRegistrator_)
            : base(container_, chromeManager_, chromeRegistry_, application_, eventRegistrator_)
        {
            
        }

        protected override int TimeoutInSeconds
        {
            get 
            { 
                if (WebSupportExtensions.Registry[this].ExtraInfo.ContainsKey(ModuleProperties.TimeoutInSeconds))
                {
                    int timeoutInSeconds;
                    if (Int32.TryParse(
                        WebSupportExtensions.Registry[this].ExtraInfo[ModuleProperties.TimeoutInSeconds],
                        out timeoutInSeconds))
                    {
                        return timeoutInSeconds;
                    }
                }
                return -1;
            }

        }

        protected override Uri Url
        {
            get { return WebSupportExtensions.Registry[this].Url; }
        }
    }
}
