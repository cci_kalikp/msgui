﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.WebSupport;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.Configuration.Modules;
using HTMLModule = MSDesktop.WebSupport.HTMLModule;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers.Modules
{
    public class HTMLModuleImpl: HTMLModule
    {
        public HTMLModuleImpl(IUnityContainer container_):base(container_)
        {
            
        }

        protected override string Name
        {
            get { return WebSupportExtensions.Registry[this].ExtraInfo[ModuleProperties.Name]; }
        } 
    }
}
