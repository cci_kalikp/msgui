﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Configuration.Modules;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.Prism;
using MorganStanley.MSDotNet.My;
using AssemblyResolverLoader = MorganStanley.MSDotNet.MSGui.Impl.Modularity.AssemblyResolverLoader;
using Module = MorganStanley.Desktop.Loader.Configuration.Modules.Module;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class ModuleHelper
    {
        private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger<ModuleHelper>();

        public static ModuleDefinition ToModule(Module element_, IModuleLoadInfos moduleLoadInfos_)
        {

            Assembly assembly;
            string assemblyFileName = string.Empty;

            // If the AssemblyFile is not specified
            // Manually attempt to load it
            if (string.IsNullOrEmpty(element_.AssemblyFile))
            {
                // Assembly definition for this module
                var sb = new StringBuilder();
                sb.Append(element_.AssemblyName);
                sb.Append(", Version=");
                sb.Append(String.IsNullOrEmpty(element_.AssemblyVersion) ? "0.0.0.0" : element_.AssemblyVersion);
                sb.Append(", Culture=");
                sb.Append(String.IsNullOrEmpty(element_.Culture) ? "neutral" : element_.Culture);
                sb.Append(", PublicKeyToken=");
                sb.Append(String.IsNullOrEmpty(element_.PublicKeyToken) ? "null" : element_.PublicKeyToken);
                string assemblyToLoad = sb.ToString();

                // Get the type of the module to load
                // This causes the module to be loaded into the current AppDomain
                // This will be resolved by the assembly resolver
                // if the module has not been loaded yet.
                // Note: AssemblyVersion = 0.0.0.0 will cause the most precedent
                // version of this assembly to load (specified by the MSDE config)
                // type version independence is a _well_ known behaviour of the
                // assembly resolver.

                // can throw IOException!
                try
                {
                    assembly = Assembly.Load(assemblyToLoad);
                }
                catch (Exception exc)
                {
                    logger.Error(
                         string.Format("Assembly {0} couldn't be loaded for module {1}", assemblyToLoad, element_.ModuleType), exc);
                    IModuleLoadInfo info = moduleLoadInfos_.AddModuleLoadInfo(element_.ModuleType + ", " + assemblyToLoad, ModuleStatus.TypeResolutionFailed);
                    info.Error = exc;
                    return null;
                }
                assemblyFileName = assembly.Location;
            }
            else
            {
                try
                {
                    assemblyFileName = PathUtilities.GetAbsolutePath(element_.AssemblyFile);
                    assembly = Assembly.LoadFile(assemblyFileName);
                }
                catch (Exception exc)
                {
                    logger.Error(
                        string.Format("Assembly {0} couldn't be loaded for module {1}", assemblyFileName, element_.ModuleType), exc);
                    IModuleLoadInfo info = moduleLoadInfos_.AddModuleLoadInfo(element_.ModuleType + ", " + assemblyFileName, ModuleStatus.TypeResolutionFailed);
                    info.Error = exc;
                    return null;
                }

            }

            if (element_.LoadModuleBundle)
            {
                //Attempt to load the MSDE config file from the
                //AssemblyFile Location
                string msdeConfigFile = assemblyFileName + ".msde.config";
                if (File.Exists(msdeConfigFile))
                {
                    AssemblyResolverLoader.Load(msdeConfigFile);
                }
            }

            string moduleType = element_.ModuleType;

            // correct the module name to an assembly qualified name
            if (!moduleType.Contains(","))
            {
                moduleType += ", " + assembly.FullName;
            }
            var extraValues = new StringDictionary();
            extraValues["CheckEntitlement"] = element_.CheckEntitlement.ToString();
            if (element_.ExtraProperties != null)
            {
                foreach (var extraValue in element_.ExtraProperties)
                {
                    extraValues[extraValue.Name] = extraValue.Value;
                }
            }

            return new ModuleDefinition()
            {
                Dependencies = element_.Dependencies == null ? new string[0] : element_.Dependencies.Select(e_ => e_.ModuleName).ToArray(),
                ModuleName = element_.ModuleName,
                ModuleType = moduleType,
                Ref = GetFileAbsoluteUri(assemblyFileName),
                StartupLoaded = element_.StartupLoaded,
                ExtraValues = extraValues
            };
        }

        public static void AddInterceptorToCatalog(List<Module> elements_, IUnityContainer container_)
        {
            var catalog = container_.Resolve<IInterceptableModuleCatalog>();
            var interceptor = new ConfigModuleInfoInterceptor(elements_, container_);
            catalog.AddInterceptor(interceptor);
             
        }

        public static void AddPrismInterceptorToCatalog(List<Module> elements_, IUnityContainer container_)
        {
            var catalog = container_.Resolve<IInterceptablePrismModuleCatalog>();
            var interceptor = new ConfigPrismModuleInfoInterceptor(elements_, container_);
            catalog.AddInterceptor(interceptor);
        }

        private static string GetFileAbsoluteUri(string filePath_)
        {
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Host = String.Empty;
            uriBuilder.Scheme = Uri.UriSchemeFile;
            uriBuilder.Path = Path.GetFullPath(filePath_);
            Uri fileUri = uriBuilder.Uri;

            return fileUri.ToString();
        }


    }
}
