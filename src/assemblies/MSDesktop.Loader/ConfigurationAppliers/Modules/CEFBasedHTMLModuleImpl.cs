﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.WebSupport;
using MSDesktop.WebSupport.CEF;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.Configuration.Modules;
using CEFBasedHTMLModule = MSDesktop.WebSupport.CEF.CEFBasedHTMLModule;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers.Modules
{
    public class CEFBasedHTMLModuleImpl : CEFBasedHTMLModule
    {
        public CEFBasedHTMLModuleImpl(IUnityContainer container_)
            : base(container_)
        {
            
        }

        protected override string Name
        {
            get { return WebSupportExtensions.Registry[this].ExtraInfo[ModuleProperties.Name]; }
        } 
    }
}
