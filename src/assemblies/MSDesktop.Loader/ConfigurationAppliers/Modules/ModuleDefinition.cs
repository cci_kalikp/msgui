﻿using System.Collections.Specialized;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    public class ModuleDefinition
    {
        public string ModuleName { get; set; }
        public string ModuleType { get; set; }
        public string[] Dependencies { get; set; }
        public string Ref { get; set; }
        public bool StartupLoaded { get; set; }
        public StringDictionary ExtraValues { get; set; }  
    }
}
