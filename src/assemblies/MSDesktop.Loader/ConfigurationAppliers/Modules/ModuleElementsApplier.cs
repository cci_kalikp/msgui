﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.WebSupport;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Configuration.Modules;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.Prism; 

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class ModuleElementsApplier : ConfigurationApplierBase<ModuleElements>
    {
        private List<Module> moduleElements;
        protected override bool ApplyCurrent(ModuleElements moduleElements_, MSDotNet.MSGui.Impl.Framework framework_)
        {
            moduleElements = moduleElements_.Modules;
            if (moduleElements == null || moduleElements.Count == 0) return false;
            framework_.Bootstrapper.AfterPersistenceStorageInitialized += Bootstrapper_AfterPersistenceStorageInitialized;
            return true;
        }

 
        void Bootstrapper_AfterPersistenceStorageInitialized(object sender, ContainerEventArgs e)
        {
            var container = e.Container;
            if (container.IsRegistered<IInterceptableModuleCatalog>())
            {
                ModuleHelper.AddInterceptorToCatalog(moduleElements, container);
            }
            else if (container.IsRegistered<IInterceptablePrismModuleCatalog>())
            {
                ModuleHelper.AddPrismInterceptorToCatalog(moduleElements, container);
            }
        } 
    }
}
