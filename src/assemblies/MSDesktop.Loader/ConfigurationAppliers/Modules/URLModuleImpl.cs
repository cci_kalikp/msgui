﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MSDesktop.WebSupport;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.Configuration.Modules;
using MorganStanley.Desktop.Loader.Properties;
using URLModule = MSDesktop.WebSupport.URLModule;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers.Modules
{
    public class URLModuleImpl: URLModule
    {
        public URLModuleImpl(IUnityContainer container_)
            : base(container_)
        {
            
        }

        protected override string Name
        {
            get { return WebSupportExtensions.Registry[this].ExtraInfo[ModuleProperties.Name]; }
        }


        protected override string GetURLProxyHtml()
        {
            string urlProxyHtml = WebSupportExtensions.Registry[this].ExtraInfo.ContainsKey(ModuleProperties.UrlProxyHtml)
                                      ? WebSupportExtensions.Registry[this].ExtraInfo[ModuleProperties.UrlProxyHtml]
                                      : "URLProxy.html";
            if (File.Exists(urlProxyHtml))
            {
                return File.ReadAllText(urlProxyHtml);
            }
            return Resources.URLProxy; 
        }
    }
}
