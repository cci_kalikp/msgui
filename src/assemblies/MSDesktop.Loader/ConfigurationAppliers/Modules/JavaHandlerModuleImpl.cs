﻿using System;
using System.Diagnostics;
using System.Threading;
using MSDesktop.Wormhole;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.Configuration.Modules;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers.Modules
{
    public class JavaHandlerModuleImpl: JavaHandlerModuleBase
    {
        public JavaHandlerModuleImpl(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, IApplication application_, IUnityContainer container_)
            : base(chromeRegistry_, chromeManager_, application_, container_)
        {
        }

         
        protected override string Path
        {
            get { return WormholeExtensions.Registry[this].Path; }

        }

        protected override int PortNumber
        {
            get { return WormholeExtensions.Registry[this].PortNumber; }
        }

        protected override string ProcessCommandLine
        {
            get { return WormholeExtensions.Registry[this].ProcessCommandLine; } 
        }

        protected override void RegisterJavaApplication(WormholeBridgedApplication hostApplication_)
        {
            string javaWindowButtonId = WormholeExtensions.Registry[this].ExtraInfo.ContainsKey(ModuleProperties.Id) ?
                WormholeExtensions.Registry[this].ExtraInfo[ModuleProperties.Id] :
                Guid.NewGuid().ToString(); 
            string javaWindowFactoryId =  Guid.NewGuid().ToString(); 
            hostApplication_.RegisterWindowFactory(javaWindowFactoryId);
            var initialParameters = new InitialButtonParameters
                {
                    Text = WormholeExtensions.Registry[this].ExtraInfo.ContainsKey(ModuleProperties.Title)
                               ? WormholeExtensions.Registry[this].ExtraInfo[ModuleProperties.Title]
                               : null,
                    ImagePath = WormholeExtensions.Registry[this].ExtraInfo.ContainsKey(ModuleProperties.Image)
                                    ? PathUtilities.GetAbsolutePath(WormholeExtensions.Registry[this].ExtraInfo[ModuleProperties.Image])
                                    : null,
                    Click = (sender1_, args1_) => hostApplication_.CreateWindow(javaWindowFactoryId, null)
                };

            if (!WormholeExtensions.Registry[this].ExtraInfo.ContainsKey(ModuleProperties.Location))
            {
                chromeManager.AddWidget(javaWindowButtonId, initialParameters);
            }
            else
            {
                chromeManager.PlaceWidget(javaWindowButtonId, 
                    WormholeExtensions.Registry[this].ExtraInfo[ModuleProperties.Location], initialParameters);
            }
            

        }

        protected override Process GetProcess()
        {
            if (Path != null)
            {
                string path = PathUtilities.GetAbsolutePath(Path);
                ProcessStartInfo psi = new ProcessStartInfo();
                if (path.ToLower().EndsWith(".cmd") || path.ToLower().EndsWith(".bat"))
                {
                    string commandExe = Environment.GetEnvironmentVariable("ComSpec");
                    if (!string.IsNullOrEmpty(commandExe))
                    {
                        psi.FileName = commandExe;
                        //psi.CreateNoWindow = true;
                        psi.Arguments = string.Format("/C \"{0}\"", path);
                    }
                }
                else
                {
                    psi.FileName = path;
                }
                psi.EnvironmentVariables["MSDesktopPortNumber"] = PortNumber.ToString();
                psi.UseShellExecute = false;
                return Process.Start(psi);
            }
            return base.GetProcess();
        }
    }
}
