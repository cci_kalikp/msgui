﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Xml.Linq;
using MSDesktop.Isolation.Interfaces;
using MSDesktop.Wormhole;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.Configuration.Modules;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers.Modules
{
    public class JavaHandlerIsolatedModuleImpl : JavaHandlerModuleBase, IModuleWithExtraInformation
    {
        public JavaHandlerIsolatedModuleImpl(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, IApplication application_, IUnityContainer container_)
            : base(chromeRegistry_, chromeManager_, application_, container_)
        {
        }

         
        protected override string Path
        {
            get { return WormholeExtensions.Registry[this].Path; }

        }

        protected override int PortNumber
        {
            get { return WormholeExtensions.Registry[this].PortNumber; }
        }

        protected override string ProcessCommandLine
        {
            get { return WormholeExtensions.Registry[this].ProcessCommandLine; } 
        }

        protected override void RegisterJavaApplication(WormholeBridgedApplication hostApplication_)
        {
            string javaWindowFactoryId = Guid.NewGuid().ToString(); 
            hostApplication_.RegisterWindowFactory(javaWindowFactoryId);
            var initialParameters = new InitialButtonParameters
                {
                    Text = WormholeExtensions.Registry[this].ExtraInfo.ContainsKey(ModuleProperties.Title)
                               ? WormholeExtensions.Registry[this].ExtraInfo[ModuleProperties.Title]
                               : null,
                    ImagePath = WormholeExtensions.Registry[this].ExtraInfo.ContainsKey(ModuleProperties.Image)
                                    ? PathUtilities.GetAbsolutePath(WormholeExtensions.Registry[this].ExtraInfo[ModuleProperties.Image])
                                    : null,
                    Click = (sender1_, args1_) => hostApplication_.CreateWindow(javaWindowFactoryId, null)
                };

            if (!WormholeExtensions.Registry[this].ExtraInfo.ContainsKey(ModuleProperties.Location))
            {
                chromeManager.AddWidget(Guid.NewGuid().ToString(), initialParameters);
            }
            else
            {
                chromeManager.PlaceWidget(Guid.NewGuid().ToString(), 
                    WormholeExtensions.Registry[this].ExtraInfo[ModuleProperties.Location], initialParameters);
            }
            

        }

        protected override Process GetProcess()
        {
            if (Path != null)
            {
                string path = PathUtilities.GetAbsolutePath(Path);
                ProcessStartInfo psi = new ProcessStartInfo();
                if (path.ToLower().EndsWith(".cmd") || path.ToLower().EndsWith(".bat"))
                {
                    string commandExe = Environment.GetEnvironmentVariable("ComSpec");
                    if (!string.IsNullOrEmpty(commandExe))
                    {
                        psi.FileName = commandExe;
                        //psi.CreateNoWindow = true;
                        psi.Arguments = string.Format("/C \"{0}\"", path);
                    }
                }
                else
                {
                    psi.FileName = path;
                }
                psi.EnvironmentVariables["MSDesktopPortNumber"] = PortNumber.ToString();
                psi.UseShellExecute = false;
                return Process.Start(psi);
            }
            return base.GetProcess();
        }

        public void SetExtraInformation(int moduleIndex, global::MSDesktop.Isolation.Proxies.IsolatedSubsystemSettings settings)
        {
            if (string.IsNullOrEmpty(settings.ExtraInformation)) return;
            JavaModuleInfo info = new JavaModuleInfo();
            XDocument doc = XDocument.Parse(settings.ExtraInformation);
            int index = 0;
            foreach (var moduleElement in doc.Descendants("Module"))
            {
                if (moduleIndex == index)
                {
                    var pathAttr = moduleElement.Attribute(ModuleProperties.Path);
                    if (pathAttr != null)
                    {
                        info.Path = PathUtilities.ReplaceEnvironmentVariables(pathAttr.Value);
                        pathAttr.Remove();
                    }
                    var processCommandLineAttr = moduleElement.Attribute(ModuleProperties.ProcessCommandLine);
                    if (processCommandLineAttr != null)
                    {
                        info.ProcessCommandLine = processCommandLineAttr.Value;
                        processCommandLineAttr.Remove();
                    }
                    var portNumberAttr = moduleElement.Attribute(ModuleProperties.PortNumber);
                    if (portNumberAttr != null )
                    {
                        portNumberAttr.Remove();
                        if (!string.IsNullOrEmpty(portNumberAttr.Value))
                        {

                            int portNumber;
                            if (Int32.TryParse(portNumberAttr.Value, out portNumber))
                            {
                                info.PortNumber = portNumber;
                            } 
                        }
                    }

                    foreach (var attribute in moduleElement.Attributes())
                    {
                        if (!string.IsNullOrEmpty(attribute.Value))
                        { 
                            info.ExtraInfo.Add(attribute.Name.LocalName, attribute.Value);
                        }
                    }

                    WormholeExtensions.Registry.Add(this, info);
                    break;
                }
                index++;
            }
        }
    }
}
