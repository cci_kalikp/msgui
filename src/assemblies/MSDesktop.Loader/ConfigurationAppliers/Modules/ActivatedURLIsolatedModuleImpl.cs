﻿using System; 
using System.IO; 
using System.Xml.Linq;
using MSDesktop.Isolation.Interfaces;
using MSDesktop.Isolation.Proxies;
using MSDesktop.WebSupport;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.Configuration.Modules;
using MorganStanley.Desktop.Loader.Properties;
using MorganStanley.MSDotNet.MSGui.Core;
using ActivateURLModule = MSDesktop.WebSupport.ActivatedURLModule;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers.Modules
{
    public class ActivatedURLIsolatedModuleImpl : ActivateURLModule, IModuleWithExtraInformation
    {
        public ActivatedURLIsolatedModuleImpl(IUnityContainer container_)
            : base(container_)
        { 
        }

        private string urlProxyHtml = "ActivatedURLProxy.html";

        public void SetExtraInformation(int moduleIndex, IsolatedSubsystemSettings settings)
        {
            if (string.IsNullOrEmpty(settings.ExtraInformation)) return;
            HTMLModuleInfo info = new HTMLModuleInfo();
            XDocument doc = XDocument.Parse(settings.ExtraInformation);
            int index = 0;
            foreach (var moduleElement in doc.Descendants("Module"))
            {
                if (moduleIndex == index)
                {
                    var nameAttr = moduleElement.Attribute(ModuleProperties.Name);
                    if (nameAttr != null)
                    {
                        name = nameAttr.Value;
                    }
                    var urlAttr = moduleElement.Attribute(ModuleProperties.Url);
                    if (urlAttr != null)
                    {
                        string urlLocal = PathUtilities.ReplaceEnvironmentVariables(urlAttr.Value);
                        if (!string.IsNullOrEmpty(urlLocal))
                        {
                            info.Url = new Uri(urlLocal);
                        }
                    }
                    var locationAttr = moduleElement.Attribute(ModuleProperties.Location);
                    if (locationAttr != null)
                    {
                        info.Location = locationAttr.Value;
                    }
                    var imageAttr = moduleElement.Attribute(ModuleProperties.Image);
                    if (imageAttr != null)
                    {
                        info.Image = PathUtilities.ReplaceEnvironmentVariables(imageAttr.Value);
                    }

                    var titleAttr = moduleElement.Attribute(ModuleProperties.Title);
                    if (titleAttr != null)
                    {
                        info.Title = titleAttr.Value;
                    }
                    var urlProxyHtmlAttr = moduleElement.Attribute(ModuleProperties.UrlProxyHtml);
                    if (urlProxyHtmlAttr != null && !string.IsNullOrEmpty(urlProxyHtmlAttr.Value))
                    {
                        urlProxyHtml = urlProxyHtmlAttr.Value;
                    }
                    WebSupportExtensions.Registry.Add(this, info);
                    break;
                }
                index++;
            } 
        }

        private string name;
        protected override string Name
        {
            get { return name; }
        }
         
        protected override string GetURLProxyHtml()
        {
            if (File.Exists(urlProxyHtml))
            {
                return File.ReadAllText(urlProxyHtml);
            }
            return Resources.URLActivatedProxy; 
        }
    }
}
