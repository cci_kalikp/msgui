﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MSDesktop.Isolation.Interfaces;
using MSDesktop.Isolation.Proxies;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.Configuration.Modules;
using MorganStanley.MSDotNet.MSGui.Core;
using HTMLModule = MSDesktop.WebSupport.HTMLModule;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers.Modules
{
    public class HTMLIsolatedModuleImpl: HTMLModule, IModuleWithExtraInformation
    {
        public HTMLIsolatedModuleImpl(IUnityContainer container_)
            : base(container_)
        {
            
        }


        public void SetExtraInformation(int moduleIndex, IsolatedSubsystemSettings settings)
        {
           if (string.IsNullOrEmpty(settings.ExtraInformation)) return;
           XDocument doc = XDocument.Parse(settings.ExtraInformation);
           int index = 0;
           foreach (var moduleElement in doc.Descendants("Module"))
           {
               if (moduleIndex == index)
               {
                   var nameAttr = moduleElement.Attribute(ModuleProperties.Name);
                   if (nameAttr != null)
                   {
                       name = nameAttr.Value;
                   }
                   var urlAttr = moduleElement.Attribute(ModuleProperties.Url);
                   if (urlAttr != null)
                   {
                       string urlLocal = PathUtilities.ReplaceEnvironmentVariables(urlAttr.Value);
                       if (!string.IsNullOrEmpty(urlLocal))
                       {
                           this.url = new Uri(urlLocal);
                       }
                   }
                   break;
               }
               index++;
           } 
        }

        private string name;
        protected override string Name
        {
            get { return name; }
        }

        private Uri url;
        protected override Uri Url
        {
            get { return url; }
        }
    }
}
