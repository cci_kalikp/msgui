﻿using System;
using System.Collections.Generic;
using System.IO; 
using System.Xml.Linq; 
using MSDesktop.Isolation.Proxies;
using MSDesktop.ModuleEntitlements;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.Configuration; 
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation; 

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    internal sealed class IsolatedSubsystemsApplier : ConfigurationApplierBase<IsolatedSubsystems>
    {
        public const string ExtraInformationArgs = "/extraInfo:";
 
        protected override bool ApplyCurrent(IsolatedSubsystems isolatedSubsystems_,
                                             MSDotNet.MSGui.Impl.Framework framework_)
        { 
            if (isolatedSubsystems_.Subsystems == null || isolatedSubsystems_.Subsystems.Count == 0) return false;
            if (MSDesktopConfigurationApplier.RootConfiguration.Communication != null &&
                MSDesktopConfigurationApplier.RootConfiguration.Communication.MessageRoutingCommunicationEnabled)
            {
                framework_.EnableProcessIsolation();
            }
            framework_.Bootstrapper.InitializationFinished += (sender_, args_) =>
                {
                    IIsolatedModuleInterceptor interceptor =
                        framework_.Container.IsRegistered<IIsolatedModuleInterceptor>()
                            ? framework_.Container.Resolve<IIsolatedModuleInterceptor>()
                            : null;

                    bool hasExtraProperty = false;
                    foreach (var subsystem in isolatedSubsystems_.Subsystems)
                    {
                        if (subsystem.Modules == null || subsystem.Modules.Count == 0)
                        {
                            continue;
                        }

                        
                        XElement docExtraInfo = new XElement("Modules");   
                        var moduleTypes = new List<string>(); 
                        foreach (var module in subsystem.Modules)
                        {
                            string moduleAssembly = !string.IsNullOrEmpty(module.Assembly)
                                                        ? module.Assembly
                                                        : subsystem.Assembly;
                            string moduleType = module.ModuleType;
                            if (!string.IsNullOrEmpty(moduleAssembly) && !moduleType.Contains(","))
                            {
                                moduleType += ", " + moduleAssembly;
                            }
                            
                            if (interceptor == null ||
                                interceptor.Accept(moduleType, module.ModuleName, module.CheckEntitlement))
                            {
                                moduleTypes.Add(moduleType);

                                var childNode = new XElement("Module");
                                childNode.SetAttributeValue("Type", moduleType); 
                                if (module.ExtraProperties != null && module.ExtraProperties.Count > 0)
                                {
                                    hasExtraProperty = true;
                                    foreach (var property in module.ExtraProperties)
                                    {
                                       childNode.SetAttributeValue(property.Name, property.Value);
                                    }
                                }
                                docExtraInfo.Add(childNode);
                            }
                        }

                        if (moduleTypes.Count == 0) continue;
                        string isolatedExecuterLocation =
                            PathUtilities.ReplaceEnvironmentVariables(subsystem.IsolatedExecuterLocation);
                        string isolatedAssemblyLocation =
                            PathUtilities.ReplaceEnvironmentVariables(subsystem.AssemblyLocation);

                        if (!PathUtilities.PathEquals(isolatedAssemblyLocation, isolatedExecuterLocation))
                        {
                            var hostExecutable = subsystem.Platform == Configuration.Helpers.IsolatedProcessPlatform.X86
                                                     ? @"MSDesktop.Isolation.HostProcess.X86.exe"
                                                     : @"MSDesktop.Isolation.HostProcess.exe";
                            if (File.Exists(Path.Combine(isolatedAssemblyLocation ?? string.Empty, hostExecutable)))
                            {
                                isolatedExecuterLocation = subsystem.AssemblyLocation;
                            }
                        }

                        if (moduleTypes.Count == 1)
                        {
                            //single module
                            framework_.SetUpProcessIsolatedWrapper(moduleTypes[0],
                                                                   (IsolatedProcessPlatform) subsystem.Platform,
                                                                   isolatedAssemblyLocation, isolatedExecuterLocation,
                                                                   null,
                                                                   subsystem.AdditionalCommandLine, subsystem.NeedsAR,
                                                                   PathUtilities.ReplaceEnvironmentVariables(subsystem.ExtraAR), subsystem.ConfigFile,
                                                                   subsystem.RibbonPath, null, subsystem.UsePrism, hasExtraProperty ? docExtraInfo.ToString() : null);
                            continue;
                        }
                         
                         
                        //multiple module
                        framework_.SetUpProcessIsolatedWrapper(
                            typeof(SubsystemModuleCollectionFromSettings).AssemblyQualifiedName,
                            (IsolatedProcessPlatform) subsystem.Platform, isolatedAssemblyLocation,
                            isolatedExecuterLocation, null, subsystem.AdditionalCommandLine, subsystem.NeedsAR,
                            PathUtilities.ReplaceEnvironmentVariables(subsystem.ExtraAR), subsystem.ConfigFile, subsystem.RibbonPath, null, subsystem.UsePrism, docExtraInfo.ToString());

                    }
                };

            return true;
        }

 

    }
}
