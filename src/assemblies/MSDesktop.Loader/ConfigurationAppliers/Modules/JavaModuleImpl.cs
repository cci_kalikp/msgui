﻿using System;
using System.Diagnostics;
using System.Threading;
using MSDesktop.Wormhole;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers.Modules
{
    public class JavaModuleImpl: JavaModuleBase
    {
        public JavaModuleImpl(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, IApplication application_, IUnityContainer container_)
            : base(chromeRegistry_, chromeManager_, application_, container_)
        {
        }

        protected override bool EnableKrakenCommunication
        {
            get { return WormholeExtensions.Registry[this].EnableKrakenCommunication; }
        }

        protected override string Path
        {
            get { return WormholeExtensions.Registry[this].Path; }

        }

        protected override int PortNumber
        {
            get { return WormholeExtensions.Registry[this].PortNumber; }
        }

        protected override string ProcessCommandLine
        {
            get { return WormholeExtensions.Registry[this].ProcessCommandLine; } 
        }

        protected override int TimeoutInSeconds
        {
            get { return WormholeExtensions.Registry[this].TimeoutInSeconds; } 

        }

        protected override Process GetProcess()
        {
            if (Path != null)
            {
                string path = PathUtilities.GetAbsolutePath(Path);
                ProcessStartInfo psi = new ProcessStartInfo();
                if (path.ToLower().EndsWith(".cmd") || path.ToLower().EndsWith(".bat"))
                {
                    string commandExe = Environment.GetEnvironmentVariable("ComSpec");
                    if (!string.IsNullOrEmpty(commandExe))
                    {
                        psi.FileName = commandExe;
                        //psi.CreateNoWindow = true;
                        psi.Arguments = string.Format("/C \"{0}\"", path);
                    }
                }
                else
                {
                    psi.FileName = path; 
                }
                psi.EnvironmentVariables["MSDesktopPortNumber"] = PortNumber.ToString(); 
                psi.UseShellExecute = false;
                return Process.Start(psi);
            }
            return base.GetProcess();
        }
    }
}
