﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using MSDesktop.ComponentEntitlements;
using MSDesktop.Environment;
using MSDesktop.ModuleEntitlements;
using MorganStanley.Desktop.Entitlement;
using MorganStanley.Desktop.Loader.Configuration.Entitlement;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;
using Microsoft.Practices.Unity;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    abstract class OnlineServiceApplier<TService> : ConfigurationApplierBase<TService> where TService:OnlineService
    {

        private TService onlineService;
        protected override bool ApplyCurrent(TService onlineService_, Framework framework_)
        {
            onlineService = onlineService_;
            var service = CreateService(framework_, null);
             ApplyService(service);
            return true;
        }

        protected abstract OnlineEntitlementService CreateServiceCore(TService onlineService_, string userName_);

        private Framework framework;
        protected IEntitlementService CreateService(Framework framework_, string userName_)
        {
            this.framework = framework_;
            OnlineEntitlementService service = CreateServiceCore(onlineService, userName_);
            if (service == null)
            {
                EnvironmentExtensions.EnvironmentRegistered += new EventHandler(EnvironmentExtensions_EnvironmentRegistered);
                return null;
            }
            service.EnableCache = onlineService.CacheEnabled;
            if (!string.IsNullOrEmpty(onlineService.ResilienceFile))
            {
                service.Resilience = true;
                string path = PathUtilities.GetAbsolutePath(onlineService.ResilienceFile);
                service.OnResilienceWrite += (object sender, ResilienceWriteEventArgs args) =>
                {
                    XElement element = args.Element;
                    using (FileStream fs = new FileStream(path, FileMode.Create))
                    {
                        using (XmlWriter writer = XmlWriter.Create(fs))
                        {
                            element.WriteTo(writer);
                        }
                    }
                };
                service.ResilienceRead = new Func<XElement>(() =>
                    { 
                        if (!File.Exists(path)) return null;
                        using (FileStream fs = new FileStream(path, FileMode.Open))
                        {
                            using (XmlReader reader = XmlReader.Create(fs))
                            {
                                return XElement.Load(reader);
                            }
                        }
                    }); 
            } 
            if (onlineService.CacheTimeout > 0)
            {
                service.CacheTimeout = onlineService.CacheTimeout;
            }
            if (onlineService.RequestTimeout > 0)
            {
                service.RequestTimeout = onlineService.RequestTimeout;
            }
            return service;
        }

        void EnvironmentExtensions_EnvironmentRegistered(object sender, EventArgs e)
        {
            var env = framework.Container.Resolve<IMSDesktopEnvironment>();
            var service = CreateService(framework, env.User);
            EnvironmentExtensions.EnvironmentRegistered -= new EventHandler(EnvironmentExtensions_EnvironmentRegistered);
            ApplyService(service);
        } 
         
        private void ApplyService(IEntitlementService service_)
        {
            if (service_ != null)
            { 
                framework.SetModuleEntitlementService(service_);
                framework.SetComponentEntitlementService(service_); 
            }
        }
    }
}
