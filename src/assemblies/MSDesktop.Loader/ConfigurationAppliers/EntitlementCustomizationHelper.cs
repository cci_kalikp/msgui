﻿using System;
using System.Reflection;
using MSDesktop.ModuleEntitlements;
using MorganStanley.Desktop.Loader.Configuration.Entitlement;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class EntitlementCustomizationHelper
    { 
        private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger<EntitlementCustomizationHelper>(); 
        public static Func<string, object> GetResourceFactory(EntitlementCustomization customization_)
        {
            return (Func<string, object>)GetCustomizationDelegate(customization_, typeof(Func<string, object>), customization_.ResourceFactoryTypeName,
                                     customization_.ResourceFactoryMethodName);
        }

        public static Delegate GetCustomizationDelegate(EntitlementCustomization customization_, Type delegateType_,
            string methodType_, string methodName_)
        {
            if (!string.IsNullOrEmpty(methodType_) &&
               !string.IsNullOrEmpty(methodName_))
            {

                Type type = GetCustomizationType(customization_, methodType_);
                if (type != null)
                {
                    var delegateMethod = type.GetMethod(methodName_,
                                                           BindingFlags.Static | BindingFlags.Public);
                    if (delegateMethod != null)
                    {
                        return Delegate.CreateDelegate(delegateType_, delegateMethod); 
                    }
                    logger.Error("Failed to find method " + methodName_ + " in " + type.FullName);
                }
            }
            return null;
        }
          
        public static Type GetCustomizationType(EntitlementCustomization customization_, string typeName_)
        {
            return TypeResolver.GetCustomizationType(customization_.CustomizationAssemblyFile,
                                                     customization_.CustomizationAssemblyName,
                                                     typeName_);

        }
    }
}
