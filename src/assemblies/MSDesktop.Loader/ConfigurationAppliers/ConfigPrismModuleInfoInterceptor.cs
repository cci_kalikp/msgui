﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.ConfigurationAppliers;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.Prism;

namespace MorganStanley.Desktop.Loader.Configuration.Modules
{
    class ConfigPrismModuleInfoInterceptor: IPrismModuleInfoInterceptor
    {
        private readonly List<Module> modules;
        private readonly IModuleLoadInfos moduleLoadInfos;

        public ConfigPrismModuleInfoInterceptor(List<Module> modules_, IUnityContainer container_)
        {
            modules = modules_;
            moduleLoadInfos = container_.Resolve<IModuleLoadInfos>();
        }
        public void Intercept(IList<ModuleInfo> modules_, Stack<IPrismModuleInfoInterceptor> interceptors_)
        {
            foreach (var moduleElement in modules)
            {
                if (modules_.FirstOrDefault(m_ => m_.ModuleName == moduleElement.ModuleName) != null) continue;
                var def = ModuleHelper.ToModule(moduleElement, moduleLoadInfos);
                ResolverPrismModuleInfo info = new ResolverPrismModuleInfo(def.ModuleName, def.ModuleType, def.Dependencies)
                {
                    ExtraValues = def.ExtraValues,
                    Ref = def.Ref,
                    InitializationMode =
                        def.StartupLoaded ? InitializationMode.WhenAvailable : InitializationMode.OnDemand
                };
                modules_.Add(info);
            }
            interceptors_.Pop().Intercept(modules_, interceptors_);
        }
    }
}
