﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
using MSDesktop.ComponentEntitlements;
using MSDesktop.ModuleEntitlements;
using MorganStanley.Desktop.Entitlement;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Impl;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class EntitlementConfigurationApplier : ConfigurationApplierBase<EntitlementConfiguration>
    { 
        protected override bool ApplyCurrent(EntitlementConfiguration entitlementConfiguration_, Framework framework_)
        {
 
            if (entitlementConfiguration_.EntitlementEnabled)
            {
                if (entitlementConfiguration_.ModuleEntitlement != null && entitlementConfiguration_.ModuleEntitlement.Enabled)
                {
                    var customization = entitlementConfiguration_.ModuleEntitlement.Customization;
                    Func<string, object> resourceFactory = customization == null ? null :
                        EntitlementCustomizationHelper.GetResourceFactory(customization);
                    EventHandler<BlockingModuleEventArgs> blockingModuleCallback =
                        customization == null
                            ? null
                            : (EventHandler<BlockingModuleEventArgs>)EntitlementCustomizationHelper.GetCustomizationDelegate(
                                customization,
                                typeof (EventHandler<BlockingModuleEventArgs>),
                                customization.BlockingModuleCallbackTypeName,
                                customization.BlockingModuleCallbackMethodName);

                    framework_.EnableModuleEntitlements((IEntitlementService)null, resourceFactory,
                        entitlementConfiguration_.ModuleEntitlement.LoadActionName, entitlementConfiguration_.ModuleEntitlement.PingActionName,
                        entitlementConfiguration_.ModuleEntitlement.MustOptIn, blockingModuleCallback);
                }
                if (entitlementConfiguration_.ComponentEntitlement != null && entitlementConfiguration_.ComponentEntitlement.Enabled)
                {
                    var customization = entitlementConfiguration_.ComponentEntitlement.Customization;
                    Func<string, object> resourceFactory = customization == null ? null :
                        EntitlementCustomizationHelper.GetResourceFactory(customization);
                    List<IComponentEntitlementHandler> handlers = new List<IComponentEntitlementHandler>();
                    if (customization != null && !string.IsNullOrEmpty(customization.ComponentEntitlementHandlerTypeNames))
                    {
                        foreach (var componentEntitlementHandlerTypeName in customization.ComponentEntitlementHandlerTypeNames.Split(';', ','))
                        {
                            var handlerType = EntitlementCustomizationHelper.GetCustomizationType(
                                customization, componentEntitlementHandlerTypeName);
                            if (handlerType != null)
                            {
                                IComponentEntitlementHandler handler = Activator.CreateInstance(handlerType) as IComponentEntitlementHandler;
                                if (handler != null)
                                {
                                    handlers.Add(handler);
                                }
                            }
                           
                        }
                    }
                    framework_.EnableComponentEntitlements((IEntitlementService)null, resourceFactory,
                        entitlementConfiguration_.ComponentEntitlement.LoadActionName, entitlementConfiguration_.ComponentEntitlement.ViewActionName,
                        entitlementConfiguration_.ComponentEntitlement.ModifyActionName, entitlementConfiguration_.ComponentEntitlement.ExecuteActionName,
                        handlers.ToArray());
                }
                return true;
            }
            return false;
        }
         
    }
}
