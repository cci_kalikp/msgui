﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MSDesktop.ViewModelAPI;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Impl;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class ViewModelBasedButtonsApplier : ConfigurationApplierBase<ViewModelBasedButtons>
    {
  
        protected override bool ApplyCurrent(ViewModelBasedButtons viewModelBasedButtons_, Framework framework_)
        {
            if (viewModelBasedButtons_.Buttons == null || viewModelBasedButtons_.Buttons.Count == 0) return false;
            framework_.LoadViewModelBasedButtons(viewModelBasedButtons_);
            return true;
        }
         
    }
}
