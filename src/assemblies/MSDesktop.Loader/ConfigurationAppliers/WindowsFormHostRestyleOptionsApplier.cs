﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class WindowsFormHostRestyleOptionsApplier:ConfigurationApplierBase<WindowsFormHostRestyleOptions>
    { 
        protected override bool ApplyCurrent(WindowsFormHostRestyleOptions options_, MSDotNet.MSGui.Impl.Framework framework_)
        {
            if (options_.MarginEnabled || options_.FontSizeEnabled || options_.FontFamilyEnabled)
            {
                framework_.EnableWindowsFormHostRestyleOptions(options_.MarginEnabled, options_.FontSizeEnabled,
                    options_.FontFamilyEnabled, options_.ForWin7Only);
                return true;
            }
            return false;
        }
    }
}
