﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.HTTPServer; 
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Impl; 

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class HTTPServertApplier : ConfigurationApplierBase<HTTPServer>
    {
        protected override bool ApplyCurrent(HTTPServer server_, Framework framework_)
        {
            if (!server_.Enabled) return false;
            framework_.EnableHTTPServer(server_.Port, !string.IsNullOrEmpty(server_.HashSeed), server_.HashSeed);
            
            return true;
        }
    }
}
