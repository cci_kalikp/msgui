﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDesktop.IPC;
using MorganStanley.MSDotNet.MSGui.Impl;
using MSDesktop.CrossMachineApplication.Extensions;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class CrossMachineDragAndDropApplier : ConfigurationApplierBase<CrossMachineDragAndDrop>
    {
        protected override bool ApplyCurrent(CrossMachineDragAndDrop crossMachineDragAndDrop_, Framework framework_)
        { 
            if (!crossMachineDragAndDrop_.Enabled) return false;
            IPCOptions options = new IPCOptions();
            if (crossMachineDragAndDrop_.IPCOptions != null)
            {
                options.Port = crossMachineDragAndDrop_.IPCOptions.Port.ToString();
                options.Host = crossMachineDragAndDrop_.IPCOptions.Host;
                options.Kerberos = crossMachineDragAndDrop_.IPCOptions.Kerberos;
                if (crossMachineDragAndDrop_.IPCOptions.GlobalTargeting != null &&
                    crossMachineDragAndDrop_.IPCOptions.GlobalTargeting.Count > 0)
                {
                    var targets = new List<IIPCTarget>();
                    foreach (var ipcTargetHelper in crossMachineDragAndDrop_.IPCOptions.GlobalTargeting)
                    {
                        string targetName = null;

                        if (!string.IsNullOrEmpty(ipcTargetHelper.Value))
                        {
                            switch (ipcTargetHelper.Key)
                            {
                                case IPCTargetType.Application:
                                    targetName = "application";
                                    break;
                                case IPCTargetType.Host:
                                    targetName = "host";
                                    break;
                                case IPCTargetType.User:
                                    targetName = "username";
                                    break; 
                            }
                        }
                        if (!string.IsNullOrEmpty(targetName))
                        {
                            targets.Add(new IPCTarget() {Key = targetName, Value = ipcTargetHelper.Value});
                        }
                    }
                    if (targets.Count > 0)
                    {
                        options.GlobalTargeting = new IPCTargeting(targets.ToArray());
                    }
                }
            }
            framework_.EnableCrossMachineDragAndDrop(options);
            
            return true;
        }
    }
}
