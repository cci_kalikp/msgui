﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using LayoutLoadStrategy = MorganStanley.MSDotNet.MSGui.Impl.Extensions.LayoutLoadStrategy;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class LayoutAndPersistenceApplier:ConfigurationApplierBase<Configuration.LayoutAndPersistence>
    {
        protected override bool ApplyCurrent(Configuration.LayoutAndPersistence layoutAndPersistence_, Framework framework_)
        {
            if (layoutAndPersistence_.Storage != null && !string.IsNullOrEmpty(layoutAndPersistence_.Storage.PresetDirectory))
            {
                framework_.SetupPersistenceStorage(ToStorage(layoutAndPersistence_.Storage, framework_));
            }
            if (layoutAndPersistence_.ViewTitleRestoreNoOverwrite)
            {
                framework_.SetViewTitleRestoreNoOverwrite(true);
            }
            if (layoutAndPersistence_.ViewLocationNoOverwrite)
            {
                framework_.SetViewLocationNoOverwrite();
            }
            if (layoutAndPersistence_.LayoutLoadStrategy != Configuration.Helpers.LayoutLoadStrategy.Default)
            {
                framework_.SetLayoutLoadStrategy((LayoutLoadStrategy)layoutAndPersistence_.LayoutLoadStrategy);
            }
            if (layoutAndPersistence_.SkipSaveProfileOnExitWhen != null)
            {
                framework_.SkipSaveProfileOnExitWhen(ToCondition(layoutAndPersistence_.SkipSaveProfileOnExitWhen));
            }
            if (layoutAndPersistence_.HiddenWindowsOnLayoutLoadingEnabled)
            {
                framework_.EnableHiddenWindowsOnLayoutLoading();
            }
            if (layoutAndPersistence_.LayoutLoadProgressTrackingEnabled)
            {
                framework_.EnableLayoutLoadProgressTracking();
            }
            if (layoutAndPersistence_.SequencialViewLoadEnabled)
            {
                framework_.EnableSequencialViewLoad();
            }
            return true;
        }
         

        private static IPersistenceStorage ToStorage(LayoutPersistenceStorage storageConfig_, Framework framework_)
        {
            BaseFilesystemPersistenceStorage storage;
            if (storageConfig_.MultipleFiles)
            {
                storage = new MultipleFilePersistenceStorage(PathUtilities.GetAbsolutePath(storageConfig_.PresetDirectory));
            }
            else
            {
                storage =  new FilePersistenceStorage(framework_.Bootstrapper.Application.Name, PathUtilities.GetAbsolutePath(storageConfig_.PresetDirectory));
            }
            if (!string.IsNullOrEmpty(storageConfig_.BaseDirectory))
            {
                storage.BaseFolder = PathUtilities.GetAbsolutePath(storageConfig_.BaseDirectory);
            }
            if (!string.IsNullOrEmpty(storageConfig_.AlternativeDirectory))
            {
                storage.AlternativeFolder = PathUtilities.GetAbsolutePath(storageConfig_.AlternativeDirectory);
            }
            return storage as IPersistenceStorage;
        }

        private static MSDotNet.MSGui.Impl.Application.SkipSaveProfileOnExitCondition ToCondition(SkipSaveProfileOnExitCondition helper_)
        {
            var condition = MSDotNet.MSGui.Impl.Application.SkipSaveProfileOnExitCondition.Never;
            if (helper_.ModuleLoadExceptioned)
            {
                condition |= MSDotNet.MSGui.Impl.Application.SkipSaveProfileOnExitCondition.ModuleLoadExceptioned;
            }
            if (helper_.ModuleTypeResolutionFailed)
            {
                condition |= MSDotNet.MSGui.Impl.Application.SkipSaveProfileOnExitCondition.ModuleTypeResolutionFailed;
            }
            if (helper_.ModuleBlockedByEntitlement)
            {
                condition |= MSDotNet.MSGui.Impl.Application.SkipSaveProfileOnExitCondition.ModuleBlockedByEntitlement;
            }
            return condition;
        }
    }
}
