﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.Workspaces;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Impl;
using LayoutLoadStrategy = MorganStanley.MSDotNet.MSGui.Impl.Extensions.LayoutLoadStrategy;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class WorkspaceSupportApplier:ConfigurationApplierBase<WorkspaceSupport>
    { 
        protected override bool ApplyCurrent(WorkspaceSupport workspaceSupport_, Framework framework_)
        {
            if (!workspaceSupport_.Enabled) return false;

            if (workspaceSupport_.DefaultTitlesByCategory != null && workspaceSupport_.DefaultTitlesByCategory.Count > 0)
            {
                Dictionary<string, string> defaultTitles = new Dictionary<string, string>();
                foreach (var category in workspaceSupport_.DefaultTitlesByCategory)
                {
                    defaultTitles.Add(category.Category, category.Title);
                }
                framework_.EnableWorkspaceSupport(defaultTitles, workspaceSupport_.Layout, workspaceSupport_.DefaultCategory);
            }
            else
            {
                framework_.EnableWorkspaceSupport(workspaceSupport_.Layout, workspaceSupport_.DefaultCategory, workspaceSupport_.DefaultTitle);
            }
            if (workspaceSupport_.CloseStrategy != Configuration.Helpers.LayoutLoadStrategy.Default)
            {
                framework_.SetWorkspaceCloseStrategy((LayoutLoadStrategy)workspaceSupport_.CloseStrategy);
            }
            if (workspaceSupport_.SaveWorkspaceWhenSavingProfile)
            {
                framework_.SaveWorkspaceWhenSavingProfile();
            }
            return true;
        }
    }
}
