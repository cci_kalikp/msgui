﻿using MSDesktop.ComponentEntitlements;
using MSDesktop.ModuleEntitlements;
using MorganStanley.Desktop.Entitlement;
using MorganStanley.Desktop.Loader.Configuration.Entitlement;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Impl;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class OfflineServiceApplier:ConfigurationApplierBase<OfflineService>
    { 
        protected override bool ApplyCurrent(OfflineService offlineService_, Framework framework_)
        {
            var service = new OfflineEntitlementService();
            if (offlineService_.ModuleResources != null && offlineService_.ModuleResources.Count > 0)
            {
                foreach (var entitlementResource in offlineService_.ModuleResources)
                {
                    service.AddAuthorized(new ModuleResource() { Name = entitlementResource.Name }, entitlementResource.Action.ToString());
                }
            }
            if (offlineService_.ComponentResources != null && offlineService_.ComponentResources.Count > 0)
            {
                foreach (var entitlementResource in offlineService_.ComponentResources)
                {
                    service.AddAuthorized(new ComponentResource() { ID = entitlementResource.ID }, entitlementResource.Action.ToString());
                }
            }
            framework_.SetModuleEntitlementService(service);
            return true;
        }
         
    }
}
