﻿using System;
using MSDesktop.ComponentEntitlements;
using MSDesktop.ModuleEntitlements;
using MorganStanley.Desktop.Entitlement;
using MorganStanley.Desktop.Loader.Configuration.Entitlement;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class LDAPServiceApplier:OnlineServiceApplier<LDAPService>
    {
 
        protected override OnlineEntitlementService CreateServiceCore(LDAPService service_, string userName_)
        {
            if (string.IsNullOrEmpty(userName_)) return null;
            var serviceLocal = new LdapEntitlementService(service_.LDAPUserName, service_.LDAPPassword, userName_);
            if (service_.ModuleResourcesMap != null && service_.ModuleResourcesMap.Count > 0)
            {
                foreach (var entitlementResource in service_.ModuleResourcesMap)
                {
                    if (!string.IsNullOrEmpty(entitlementResource.MailGroups))
                    {
                        string[] mailgroups = entitlementResource.MailGroups.Split(new string[] { ";", "," },
                                                               StringSplitOptions.RemoveEmptyEntries);
                        foreach (var mailgroup in mailgroups)
                        {
                            serviceLocal.AddLdapEntitlement(new ModuleResource() { Name = entitlementResource.Name }, mailgroup, entitlementResource.Action.ToString());
                        }
                    }
                    if (!string.IsNullOrEmpty(entitlementResource.CostCenters))
                    {
                        string[] costCenters = entitlementResource.CostCenters.Split(new string[] { ";", "," },
                                       StringSplitOptions.RemoveEmptyEntries);
                        foreach (var costCenter in costCenters)
                        {
                            serviceLocal.AddLdapEntitlementByCostCenter(new ModuleResource() { Name = entitlementResource.Name }, costCenter, entitlementResource.Action.ToString());
                        }
                    } 
                }
            }
            if (service_.ComponentResourcesMap != null && service_.ComponentResourcesMap.Count > 0)
            {
                foreach (var entitlementResource in service_.ComponentResourcesMap)
                {
                    if (!string.IsNullOrEmpty(entitlementResource.MailGroups))
                    {
                        string[] mailgroups = entitlementResource.MailGroups.Split(new string[] { ";", "," },
                                                               StringSplitOptions.RemoveEmptyEntries);
                        foreach (var mailgroup in mailgroups)
                        {
                            serviceLocal.AddLdapEntitlement(new ComponentResource() { ID = entitlementResource.ID }, mailgroup, entitlementResource.Action.ToString());
                        }
                    }
                    if (!string.IsNullOrEmpty(entitlementResource.CostCenters))
                    {
                        string[] costCenters = entitlementResource.CostCenters.Split(new string[] { ";", "," },
                                       StringSplitOptions.RemoveEmptyEntries);
                        foreach (var costCenter in costCenters)
                        {
                            serviceLocal.AddLdapEntitlementByCostCenter(new ComponentResource() { ID = entitlementResource.ID }, costCenter, entitlementResource.Action.ToString());
                        }
                    } 
                }
            }
            return serviceLocal;
        } 
    }
}
