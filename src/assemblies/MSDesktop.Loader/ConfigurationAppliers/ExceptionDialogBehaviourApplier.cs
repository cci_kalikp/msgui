﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class ExceptionDialogBehaviourApplier:ConfigurationApplierBase<ExceptionDialogBehaviour>
    {
 
        protected override bool ApplyCurrent(ExceptionDialogBehaviour exceptionDialogBehaviour_, Framework framework_)
        {
            if (exceptionDialogBehaviour_.UseLegacyDialog)
            {
                framework_.EnableLegacyDialog = true;          
            }
            if (!exceptionDialogBehaviour_.CopyButtonVisible || !exceptionDialogBehaviour_.EmailButtonCloses ||
    exceptionDialogBehaviour_.SuppressDialog || !string.IsNullOrEmpty(exceptionDialogBehaviour_.ExceptionMessage) ||
   exceptionDialogBehaviour_.AttachScreenshot || exceptionDialogBehaviour_.AttachLog || exceptionDialogBehaviour_.AttachFullLog)
            {
                framework_.SetExceptionDialogBehaviour(exceptionDialogBehaviour_.CopyButtonVisible, exceptionDialogBehaviour_.EmailButtonCloses,
                    exceptionDialogBehaviour_.SuppressDialog, exceptionDialogBehaviour_.ExceptionMessage,
                    exceptionDialogBehaviour_.AttachScreenshot, exceptionDialogBehaviour_.AttachLog, exceptionDialogBehaviour_.AttachFullLog);
            }
            if (exceptionDialogBehaviour_.MaxOutlookAttachFileSize >= 0 || exceptionDialogBehaviour_.MaxAttachlinkFileSize >= 0 ||
!string.IsNullOrEmpty(exceptionDialogBehaviour_.LargeFileStorageDirectory))
            {
                framework_.ConfigMailAttachmentService(exceptionDialogBehaviour_.LargeFileStorageDirectory, exceptionDialogBehaviour_.MaxOutlookAttachFileSize, exceptionDialogBehaviour_.MaxAttachlinkFileSize);
            }
            if (!exceptionDialogBehaviour_.ContinueButtonVisible)
            {
                framework_.DisallowContinueInExceptionDialog();
            }
            if (exceptionDialogBehaviour_.SendExceptionReportAutomatically)
            {
                framework_.SendExceptionReportAutomatically();
            }
            if (exceptionDialogBehaviour_.MaxAttachedLogEntryCount > 0)
            {
                framework_.SetMaxAttachedLogEntryCount(exceptionDialogBehaviour_.MaxAttachedLogEntryCount);            
            }
            return true;
        }
    }
}
