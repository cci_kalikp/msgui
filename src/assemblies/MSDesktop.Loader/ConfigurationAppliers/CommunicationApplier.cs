﻿using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core.Messaging; 
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class CommunicationApplier:ConfigurationApplierBase<Communication>
    {
 
        protected override bool ApplyCurrent(Communication communication_, Framework framework_)
        {
            if (communication_.IpcCommunicationStatus != CommunicationChannelStatus.Required)
            {
                framework_.SetIpcCommunicationStatus(communication_.IpcCommunicationStatus);
            }
            if (communication_.ImcCommunicationStatus != CommunicationChannelStatus.Required)
            {
                framework_.SetImcCommunicationStatus(communication_.ImcCommunicationStatus);
            }
 
            if (communication_.V2VConfigEnabled)
            {
                framework_.AddModule<V2VConfigModule>();
            }
            if (communication_.MessageRoutingCommunicationEnabled)
            {
                framework_.EnableMessageRoutingCommunication(communication_.RoutingMessageDispatchExceptionAsFailed);
            }
            if (communication_.MessageRoutingDefaultGuiEnabled)
            {
                framework_.EnableMessageRoutingDefaultGui();
            }
            if (communication_.MessageRoutingHistoryEnabled)
            {
                framework_.EnableMessageRoutingHistory();
            }
            if (communication_.MessageRoutingHistoryGuiEnabled)
            {
                framework_.EnableMessageRoutingHistoryGui();
            } 
            if (communication_.MessageRoutingMonitorEnabled)
            {
                framework_.EnableMessageRoutingMonitor();
            }
            return true;
        }
    }
}
