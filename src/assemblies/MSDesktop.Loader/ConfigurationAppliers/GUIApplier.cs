﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input; 
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;
using MorganStanley.MSDotNet.MSGui.Themes; 
using ShellMode = MorganStanley.MSDotNet.MSGui.Impl.Extensions.ShellMode;
using StickyWindowOptions = MorganStanley.Desktop.Loader.Configuration.Helpers.StickyWindowOptions;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class GUIApplier:ConfigurationApplierBase<global::MorganStanley.Desktop.Loader.Configuration.GUI>
    {
  
        protected override bool ApplyCurrent(global::MorganStanley.Desktop.Loader.Configuration.GUI gui_, Framework framework_)
        {
            framework_.SetShellMode((ShellMode)gui_.ShellMode, gui_.UseHarmoniaDockManager);
            if (gui_.ModernTheme != null && gui_.ModernTheme.Enabled && gui_.ModernTheme.AccentColor != null)
            {
                framework_.AddModernTheme(gui_.ModernTheme.AccentColor.Color, gui_.ModernTheme.BaseTheme == BaseModernTheme.Dark ? ModernThemeBase.Dark : ModernThemeBase.Light);
            } 
            else if (gui_.ConfigurationDependentTheme)
            {
                framework_.AddConfigurationDependentTheme(gui_.Theme.ToString());
            }
            else
            {
                switch (gui_.Theme)
                {
                    case Theme.Black:
                        framework_.AddBlackTheme();
                        break;
                    case Theme.Blue:
                        framework_.AddBlueTheme();
                        break;
                    case Theme.Simple:
                        framework_.AddSimpleTheme();
                        break;
                    case Theme.White:
                        framework_.AddWhiteTheme();
                        break;
                    case Theme.BackgroundModifiedBlue:
                        framework_.AddBackgroundModifiedBlueTheme();
                        break;
                }
            }
 
            if (gui_.RibbonIcon != null && !string.IsNullOrEmpty(gui_.RibbonIcon.Path))
            {
                framework_.SetRibbonIcon(gui_.RibbonIcon.ToImageSource());
            }
            if (!gui_.EnforcedSizeRestrictions)
            {
                framework_.DisableEnforcedSizeRestrictions();
                InitialWindowParameters.Defaults.EnforceSizeRestrictions = false;
            }
            if (gui_.CompatibleTextRendering)
            {
                framework_.EnableCompatibleTextRendering(gui_.CompatibleTextRendering);
            }
            if (gui_.LegacyControlPlacement)
            {
                framework_.EnableLegacyControlPlacement();
            }
            if (gui_.NonOwnedWindows)
            {
                framework_.EnableNonOwnedWindows();
            }
            if (!gui_.QuietCloseOfViews)
            {
                framework_.EnableNonQuietCloseOfViews();
            }
            if (gui_.SoftwareRendering)
            {
                framework_.EnableSoftwareRendering();
            }
            if (gui_.RestrictWindowClosingToHeaderButton)
            {
                framework_.RestrictWindowClosingToHeaderButton();
            }
            if (gui_.FocusHackEnabled)
            {
                framework_.SetFocusHackEnabled();
            }
            if (gui_.ViewRenameKeyGesture != null)
            {
                framework_.SetViewRenameKeyGesture(new KeyGesture(gui_.ViewRenameKeyGesture.Key, gui_.ViewRenameKeyGesture.Modifiers));
            }
            if (gui_.AcquireFocusAfterStartup)
            {
                framework_.AcquireFocusAfterStartup();
            }
            if (gui_.ForceLockerRegistrations != ForceLockerRegistrations.None)
            {
                framework_.ForceLockerRegistrations(true, gui_.ForceLockerRegistrations == ForceLockerRegistrations.MainAreaOnly);
            }
            if (gui_.HideOrLockParameters != null)
            {
                framework_.HideOrLockShellElements(gui_.HideOrLockParameters);
            } 
            if (gui_.TabsOwnFloatingWindowsEnabled)
            {
                framework_.EnableTabsOwnFloatingWindows();
            }
            if (gui_.TopmostFloatingPanesEnabled)
            {
                framework_.EnableTopmostFloatingPanes();
            }
            if (!gui_.LauncherBarExpandable)
            {
                framework_.DisableExpandedLauncherBar();
            }
            if (gui_.HideUserNameOnLauncherBar)
            {
                framework_.DisableUserNameOnLauncherBar();
            }
            if (gui_.HideLayoutNameOnLauncherBar)
            {
                framework_.HideLayoutNameOnLauncherBar();
            }

            if (gui_.TabGroupTabStripPlacement != Dock.Bottom)
            {
                framework_.SetTabGroupTabStripPlacement(gui_.TabGroupTabStripPlacement);
            }
            if (gui_.TabDragAndDropEnabled)
            {
                framework_.EnableTabDragAndDrop();
            }
            if (gui_.WindowDeactivatingNotificationEnabled)
            {
                framework_.SetWindowDeactivatingNotificationEnabled();
            }
            if (gui_.DragDropTargetEventsOnShellEnabled)
            {
                framework_.EnableDragDropTargetEventsOnShell();
            }
            if (gui_.InjectContainingTabIntoLayoutEnabled)
            {
                framework_.EnableInjectContainingTabIntoLayout();
            } 
            if (gui_.WidgetUserConfigEnabled != WidgetConfigEnablement.Disabled)
            {
                framework_.EnableWidgetUserConfig(gui_.WidgetUserConfigEnabled == WidgetConfigEnablement.Global);
            }
            if (gui_.FlatModeEnabled)
            {
                framework_.EnableFlatMode();
            }
            if (gui_.NonuniformLookForDocumentTabsEnabled)
            {
                framework_.EnableNonuniformLookForDocumentTabs();
            }
            if (gui_.UseClassicThemeForTaskDialog)
            {
                framework_.UseClassicTheme(PopupDialogType.TaskDialog);
            }
            if (gui_.InvisibleMainControllerMode != InvisibleMainControllerModeEnum.None)
            {
                framework_.SetInvisibleMainControllerMode((ShellModeExtension.InvisibleMainControllerModeEnum)gui_.InvisibleMainControllerMode);
            }
            if (gui_.RemoveEmptyTabs != RemoveEmptyTabsWhen.Never)
            {
                framework_.EnableRemoveEmptyTabs(gui_.RemoveEmptyTabs.HasFlag(RemoveEmptyTabsWhen.LayoutLoaded), gui_.RemoveEmptyTabs.HasFlag(RemoveEmptyTabsWhen.LastChildClosed));
            }
            if (!gui_.RenameTabEnabled)
            {
                framework_.DisableRenameTab(true);
            }
            if (gui_.CompactTabGroupsEnabled)
            {
                framework_.EnableCompactTabGroups(true);
            }
            if (gui_.ContentHostModeEnabled)
            {
                framework_.EnableContentHostMode();
            }
            if (!gui_.TaskbarPinEnabled)
            {
                framework_.EnableNoTaskbarPin();
            }
            if (gui_.HeaderItemPositionBeforeTitle)
            {
                framework_.SetHeaderItemPositionBeforeTitle();
            }
            if (gui_.SubTabHeaderTooltipsEnabled)
            {
                framework_.EnableSubTabHeaderTooltips();
            }
            if (gui_.ViewHeaderHeight != null && gui_.ViewHeaderHeight.Value > 0)
            {
                framework_.SetViewHeaderHeight(gui_.ViewHeaderHeight.Value);
            }
            if (gui_.LauncherBarShrinkedHeight != null && gui_.LauncherBarShrinkedHeight.Value > 0)
            {
                framework_.SetLauncherBarHeight(shrinked: gui_.LauncherBarShrinkedHeight);
            }
            if (gui_.LauncherBarExpandedHeight != null && gui_.LauncherBarShrinkedHeight.Value > 0)
            {
                framework_.SetLauncherBarHeight(expanded: gui_.LauncherBarExpandedHeight);
            }
            if (gui_.LauncherBarButtonMaxHeight != null && gui_.LauncherBarButtonMaxHeight.Value > 0)
            {
                framework_.SetLauncherBarButtonMaxHeight(gui_.LauncherBarButtonMaxHeight.Value);
            }
            if (gui_.OneClickMinimizeLauncherBarEnabled)
            {
                framework_.EnableOneClickMinimizeLauncherBar();
            }
            if (gui_.ViewShadowSize != null && gui_.ViewShadowSize.Value >= 0)
            {
                framework_.SetViewsSpacingAndShadowSize(shadowSize: gui_.ViewShadowSize);
            }
            if (gui_.ViewGapSize != null && gui_.ViewGapSize.Value >= 0)
            {
                framework_.SetViewsSpacingAndShadowSize(gapSize: gui_.ViewGapSize);
            }
            if (gui_.ViewContentPadding != null && gui_.ViewContentPadding.Value >= 0)
            {
                framework_.SetDockedWindowPadding(gui_.ViewContentPadding.Value);
            }
            if (gui_.StatusBarHeight != null && gui_.StatusBarHeight.Value >= 0)
            {
                framework_.SetStatusBarHeight(gui_.StatusBarHeight.Value);
            }
            if (gui_.StatusBarPadding != null && gui_.StatusBarPadding.Value >= 0)
            {
                framework_.SetStatusBarPadding(gui_.StatusBarPadding.Value);
            }

            if (gui_.CentralizedPlacement != null && !string.IsNullOrEmpty(gui_.CentralizedPlacement.FileName))
            {
                framework_.Bootstrapper.AfterPersistenceStorageInitialized += (sender_, args_) => framework_.EnableCentralizedPlacement(
                    PathUtilities.GetAbsolutePath(gui_.CentralizedPlacement.FileName),
                    gui_.CentralizedPlacement.Relock);
            }
            if (!gui_.RibbonCleaningEnabled)
            {
                framework_.EnableRibbonCleaning(false);
            }
            if (gui_.AggregatedWindowGroupTitleSeparator != null)
            {
                framework_.EnableAggregatedWindowGroupTitle(gui_.AggregatedWindowGroupTitleSeparator);
            }
            if (gui_.ShowSeparateHeaderItemsInFloatingWindow)
            {
                framework_.ShowSeparateHeaderItemsInFloatingWindow();
            }
            framework_.SetViewTitleTextTrimming(gui_.ViewTitleTextTrimmingCharacterElipsisEnabled, gui_.ViewTitleTextTrimmingWordElipsisEnabled);
            if (gui_.QATPlacementHackDisabled)
            {
                framework_.DisableQATPlacementHack();
            }
            if (gui_.StickinessForAllWindows != StickyWindowOptions.None)
            {
                framework_.EnableStickinessForAllWindows((MSDotNet.MSGui.Impl.Extensions.StickyWindowOptions)gui_.StickinessForAllWindows);
            }
            if (gui_.LauncherBarHorizontalResizeEnabled)
            {
                framework_.EnableHorizontalResizeLauncherBar(!gui_.LauncherBarHorizontalAutoScrollDisabled);
            }
            else if (gui_.LauncherBarHorizontalAutoScrollDisabled)
            {
                framework_.DisableLauncherBarHorizontalAutoScroll();
            }
            
            return true;
        }
    }
}
