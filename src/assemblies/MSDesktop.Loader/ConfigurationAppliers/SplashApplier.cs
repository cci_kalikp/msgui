﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class SplashApplier:ConfigurationApplierBase<Splash>
    {
        protected override bool ApplyCurrent(Splash splash_, Framework framework_)
        {
            if (splash_.Disabled)
            {
                framework_.DisableSplashScreen();
                return true;
            } 
            if (splash_.UseLegacySplash)
            {
                framework_.UseLegacySplash();
            }
            if (!splash_.ShowApplicationName)
            {
                framework_.SetSplashScreenApplicationNameToBeShown(splash_.ShowApplicationName);
            }
            if (splash_.DoubleClickDismiss)
            {
                framework_.SetSplashScreenDoubleClickDismiss(splash_.DoubleClickDismiss);
            }
            if (splash_.MinimizeOnDismiss)
            {
                framework_.SetSplashScreenMinimizeOnDismiss(splash_.MinimizeOnDismiss);
            }
            if (splash_.ShowCloseButton)
            {
                framework_.SetSplashScreenCloseButtonVisible(splash_.ShowCloseButton);
            }
            if (splash_.CloseButtonPosition != null && splash_.CloseButtonPosition.Point != default(Point))
            {
                framework_.SetSplashScreenCloseButtonPosition(splash_.CloseButtonPosition.Point);
            }
            if (splash_.CloseButtonSize != null && !splash_.CloseButtonSize.Size.IsEmpty)
            {
                framework_.SetSplashScreenCloseButtonSize(splash_.CloseButtonSize.Size);
            }
            if (!string.IsNullOrEmpty(splash_.Picture))
            {
                framework_.SetSplashScreenPicture(PathUtilities.GetAbsolutePath(splash_.Picture));
            }
            if (splash_.Autosize)
            {
                framework_.SetSplashScreenAutosize(splash_.Autosize);
            }
            if (splash_.TransparentBitmap != TransparentBitmap.Default)
            {
                framework_.SetSplashScreenBitmapTransparent(splash_.TransparentBitmap == TransparentBitmap.True);
            }
            if (splash_.NameColor != null )
            {
                framework_.SetSplashScreenNameColor(splash_.NameColor.Color);
            }
            if (!string.IsNullOrEmpty(splash_.NameFont))
            { 
                framework_.SetSplashScreenNameFontFamily(new FontFamily(splash_.NameFont));
            }
            if (splash_.NamePosition != null && splash_.NamePosition.Point != default(Point))
            {
                framework_.SetSplashScreenNamePosition(splash_.NamePosition.Point);
            }
            if (splash_.StatusColor != null )
            {
                framework_.SetSplashScreenStatusColor(splash_.StatusColor.Color);
            }
            if (!string.IsNullOrEmpty(splash_.StatusFont))
            {
                framework_.SetSplashScreenStatusFontFamily(new FontFamily(splash_.StatusFont));
            }
            if (splash_.StatusPosition != null && splash_.StatusPosition.Point != default(Point))
            {
                framework_.SetSplashScreenStatusPosition(splash_.StatusPosition.Point);
            }
            if (splash_.VersionColor != null)
            {
                framework_.SetSplashScreenVersionColor(splash_.VersionColor.Color);
            }
            if (splash_.VersionPosition != null && splash_.VersionPosition.Point != default(Point))
            {
                framework_.SetSplashScreenVersionPosition(splash_.VersionPosition.Point);
            }
            if (splash_.TopMost)
            {
                framework_.SetSplashScreenTopMost(splash_.TopMost);
            }
            return true;
        }
    }
}
