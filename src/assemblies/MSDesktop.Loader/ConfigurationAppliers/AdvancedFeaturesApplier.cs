﻿using MSDesktop.Debugging; 
using MSDesktop.WebSupport;
using MSDesktop.Wormhole;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Infrastructure; 
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class AdvancedFeaturesApplier: ConfigurationApplierBase<AdvancedFeatures>
    { 
        protected override bool ApplyCurrent(AdvancedFeatures advancedFeatures_, Framework framework_)
        {
            if (advancedFeatures_.PrintSupportEnabled)
            {
                framework_.EnablePrintSupport();
            }
            if (advancedFeatures_.DebuggingEnabled)
            {
                framework_.EnableDebugging();
            }
            if (advancedFeatures_.WebSupportEnabled)
            {
                framework_.EnableWebSupport();
            }
            if (advancedFeatures_.MailServiceEnabled)
            {
                framework_.EnableMailService();
            }
            if (advancedFeatures_.WormholeEnabled)
            {
                framework_.EnableWormhole();
            } 
            if (advancedFeatures_.RenderCapabilityLoggerEnabled)
            {
                framework_.AddModule<global::MorganStanley.MSDotNet.MSGui.Impl.Diagnostics.RenderCapabilityLogger>();
            }
            return true;
        } 
    }
}
