﻿using System;
using System.Collections.Generic;
using System.Reflection;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using Microsoft.Practices.Unity;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class BootModulesApplier:ConfigurationApplierBase<BootModules>
    { 
        protected override bool ApplyCurrent(BootModules bootModules_, Framework framework_)
        {
            var exceptions = new List<Exception>();
            var moduleLoadInfos = framework_.Container.Resolve<IModuleLoadInfos>();
            foreach (BootModule module in bootModules_.Modules)
            {
                IModuleLoadInfo info = moduleLoadInfos.AddModuleLoadInfo(module.ModuleFQCN + ", " + module.ModuleAssembly);
                info.Type = ModuleType.BootModule;
                Type moduleType = null;
                try
                {

                    Assembly assembly = string.IsNullOrEmpty(module.ModuleAssemblyFile) ? 
                        Assembly.Load(module.ModuleAssembly):
                        Assembly.LoadFile(module.ModuleAssemblyFile);
                    if (assembly == null)
                    {
                        info.Status = ModuleStatus.TypeResolutionFailed;
                        info.TypeNotFoundReason = "Failed to load Assembly " + module.ModuleAssembly;
                        continue;
                    }
                    moduleType = assembly.GetType(module.ModuleFQCN);
                    info.FullTypeName = moduleType.AssemblyQualifiedName;
                }
                catch (Exception ex)
                {
                    info.Status = ModuleStatus.TypeResolutionFailed;
                    info.Error = ex;
                    continue;
                }
                try
                {
                    var realModuleType = ModuleInfoHelper.GetModuleType(moduleType);
                    if (realModuleType != moduleType)
                    {
                        info.FullTypeName = realModuleType.AssemblyQualifiedName;
                    }
                    moduleLoadInfos.StartLoading(info);
                    if (typeof(IBootModule).IsAssignableFrom(moduleType))
                    {
                        var bootModule = (IBootModule)Activator.CreateInstance(moduleType);
                        bootModule.Initialize(framework_);
                    }
                    else
                    {
                        var bootModule = Activator.CreateInstance(moduleType, framework_);
                        if (moduleType.GetInterface("IModule") != null)
                        {
                            ReflHelper.MethodInvoke(bootModule, "Initialize", null);
                        }
                    }

                    moduleLoadInfos.EndLoading(info);
                }
                catch (Exception ex)
                {
                    info.Status = ModuleStatus.Exceptioned;
                    info.Error = ex;
                    exceptions.Add(ex);
                }
            }

            if (exceptions.Count == 1)
            {
                throw exceptions[0];
            }
            if (exceptions.Count > 1)
            {
                throw new AggregateException(exceptions);
            }
            return true;
        }
    }
}
