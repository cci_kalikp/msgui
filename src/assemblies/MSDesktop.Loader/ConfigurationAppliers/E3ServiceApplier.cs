﻿using System;
using MorganStanley.Desktop.Entitlement;
using MorganStanley.Desktop.Loader.Configuration.Entitlement;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class E3ServiceApplier:OnlineServiceApplier<E3Service>
    {
        protected override OnlineEntitlementService CreateServiceCore(E3Service service_, string userName_)
        {
            if (string.IsNullOrEmpty(service_.AddressOrAlias))
            {
                throw new ArgumentNullException("AddressOrAlias");
            }
            if (userName_ == null) return null;
            return new EclipseEntitlementService(service_.DomainName, userName_, service_.AddressOrAlias,
                                                 service_.Kerberos ? EclipseTransport.KerberosTcp : EclipseTransport.Tcp);
        }
    }
}
