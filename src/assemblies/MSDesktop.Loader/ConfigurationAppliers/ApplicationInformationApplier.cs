﻿using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class ApplicationInformationApplier:ConfigurationApplierBase<ApplicationInformation>
    {
 
        protected override bool ApplyCurrent(ApplicationInformation applicationInformation_, Framework framework_)
        {
            if (!string.IsNullOrEmpty(applicationInformation_.Name))
            {
                framework_.SetApplicationName(applicationInformation_.Name);
            }
            if (!string.IsNullOrEmpty(applicationInformation_.AppBarTitle))
            {
                framework_.SetAppBarTitle(applicationInformation_.AppBarTitle);
            }
            if (applicationInformation_.MainWindowTitle != null)
            {
                framework_.Bootstrapper.BeforeInitializeModules += (s_, e_) =>
                {
                    var chromeManager = e_.Container.Resolve<IChromeManager>();
                    if (applicationInformation_.MainWindowTitle != null)
                    {
                        chromeManager.SetMainWindowTitle(applicationInformation_.MainWindowTitle.Title, applicationInformation_.MainWindowTitle.AppendProfile);
                    }
                };
            }
            if (applicationInformation_.IconPath != null && !string.IsNullOrEmpty(applicationInformation_.IconPath.Path))
            {
                framework_.SetApplicationIcon(applicationInformation_.IconPath.ToImageSource());
            }
            if (!string.IsNullOrEmpty(applicationInformation_.Version))
            {
                framework_.SetApplicationVersion(applicationInformation_.Version);
            }
            return true;
        } 
    }
}
