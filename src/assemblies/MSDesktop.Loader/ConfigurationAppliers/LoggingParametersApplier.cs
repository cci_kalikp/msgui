﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using MorganStanley.Desktop.Loader.Configuration; 
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.Desktop.Loader.ConfigurationAppliers
{
    class LoggingParametersApplier:ConfigurationApplierBase<LoggingParameters>
    {
        private const string EnvVariablePattern = @"%(.+?)%";
        protected override bool ApplyCurrent(LoggingParameters loggingParameters_, MSDotNet.MSGui.Impl.Framework framework_)
        {
            if (loggingParameters_.Disabled) return false;
            var loggingLevels = new Dictionary<string, string>();
            if (loggingParameters_.LoggingLevels != null && loggingParameters_.LoggingLevels.Count > 0)
            {
                foreach (var target in loggingParameters_.LoggingLevels)
                {
                    loggingLevels.Add(target.Target, target.Policy);
                }
            }
            string baseName = loggingParameters_.Basename;
            if (!string.IsNullOrEmpty(baseName))
            {

                baseName = Regex.Replace(baseName, EnvVariablePattern, match_ =>
                {
                    string envName = match_.Groups[1].ToString();
                    if (envName.ToLower() == LoggingParameters.ProcessIdParameter)
                    {
                        return Process.GetCurrentProcess().Id.ToString();
                    }
                    if (envName.ToLower() == LoggingParameters.DateParameter.ToLower())
                    {
                        return DateTime.Today.ToString(LoggingParameters.DateParameter);
                    }
                    return System.Environment.GetEnvironmentVariable(envName);
                }, RegexOptions.Compiled);
            }
 
            framework_.EnableLogging(loggingParameters_.ApplicationName, PathUtilities.GetAbsolutePath(loggingParameters_.Directory), baseName, loggingLevels);

            if (loggingParameters_.AutoResolveMethodNameForLoggingEnabled)
            {
                framework_.EnableAutoResolveMethodNameForLogging();
            }
            return true;
        }


    }
}
