﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;

namespace MorganStanley.Desktop.Loader.Infrastructure
{
    abstract class AssemblyRequesterBase<T>:IAssemblyRequester where T:IApplyableConfiguration
    {

        public void RequestAssemblies(IApplyableConfiguration configuration_)
        {
            var assemblies = GetAssemblesNeeded((T)configuration_);
            if (assemblies != null)
            {
                foreach (var assembly in assemblies)
                {
                    ExtensionsUtils.RequireAssembly(assembly);
                }
            }
        }

        protected abstract IList<ExternalAssembly> GetAssemblesNeeded(T configuration_);
    }
}
