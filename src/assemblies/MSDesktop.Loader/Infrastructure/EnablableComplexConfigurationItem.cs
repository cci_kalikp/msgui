﻿using System.ComponentModel;
using System.Xml.Serialization;

namespace MorganStanley.Desktop.Loader.Infrastructure
{
    public abstract class EnablableComplexConfigurationItem : TogglableComplexConfigurationItem, INullableConfigurationItem
    {
        protected EnablableComplexConfigurationItem()
        {
            Enabled = true;
            EnablementPropertyName = "Enabled";
        }

        [DefaultValue(true)]
        [XmlAttribute]
        public bool Enabled { get; set; }

        public override System.Windows.Data.IValueConverter GetEnablementSourceConverter()
        {
            return null;
        }
         
        void INullableConfigurationItem.InitializeNullObject(object parentObject_)
        {
            this.Enabled = false; 
        }
         
        [XmlIgnore]
        public override string EnablementDescriptionFormat
        {
            get { return "enables parent property"; }
        }
         
    }
}
