﻿using System.Xml.Serialization; 
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MorganStanley.Desktop.Loader.Infrastructure
{
    public abstract class DisablableComplexConfigurationItem: TogglableComplexConfigurationItem
    {
  
        protected DisablableComplexConfigurationItem()
        {
            EnablementPropertyName = "Disabled"; 
        }
        [XmlAttribute]
        public bool Disabled { get; set; }

        public override System.Windows.Data.IValueConverter GetEnablementSourceConverter()
        {
            return new InverseBoolConverter();
        }

        [XmlIgnore]
        public override string EnablementDescriptionFormat
        {
            get { return "enables parent property"; }
        }
         
    }
}
