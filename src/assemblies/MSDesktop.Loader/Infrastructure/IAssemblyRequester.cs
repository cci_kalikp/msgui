﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Infrastructure
{
    internal interface IAssemblyRequester
    {
        void RequestAssemblies(IApplyableConfiguration configuration_);
    }
}
