﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.Desktop.Loader.Infrastructure
{
    public abstract class ExternalComplexConfigurationItem:TogglableComplexConfigurationItem
    {
        [XmlAttribute]
        public string DefinitionFile { get; set; }

        protected ExternalComplexConfigurationItem()
        {
            EnablementPropertyName = "DefinitionFile";
        }
         
        public override System.Windows.Data.IValueConverter GetEnablementSourceConverter()
        {
            return new RevertNonEmptyStringToBoolConverter();
        }


        [XmlIgnore]
        public override string EnablementDescriptionFormat
        {
            get { return "clears the external configuration file defined on parent property"; }
        }

        protected object Resolve()
        {
            if (string.IsNullOrWhiteSpace(this.DefinitionFile)) return this;
            string fileName = PathUtilities.GetAbsolutePath(this.DefinitionFile);
            if (!File.Exists(fileName)) return null;
            using (var reader = new StreamReader(fileName))
            {
                return new XmlSerializer(this.GetType()).Deserialize(reader);
            }
        }
    }
}
