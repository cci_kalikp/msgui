﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.Loader.Infrastructure
{
    public interface INullableConfigurationItem
    {
        void InitializeNullObject(object parentObject_);
    }
}
