﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Xml.Serialization;

namespace MorganStanley.Desktop.Loader.Infrastructure
{
    public abstract class TogglableComplexConfigurationItem:ComplexConfigurationItem
    {
        [XmlIgnore]
        public string EnablementPropertyName { get; protected set; }

        public abstract IValueConverter GetEnablementSourceConverter();

        public abstract string EnablementDescriptionFormat { get; }
    }
}
