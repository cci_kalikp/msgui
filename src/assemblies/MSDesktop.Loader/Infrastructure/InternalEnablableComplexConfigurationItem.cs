﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MorganStanley.Desktop.Loader.Infrastructure
{
    public abstract class InternalEnablableComplexConfigurationItem: TogglableComplexConfigurationItem, INullableConfigurationItem
    {
        protected InternalEnablableComplexConfigurationItem()
        {
            Enabled = true;
            EnablementPropertyName = "Enabled";
        }

        private bool enabled = false;
        [XmlIgnore]
        public bool Enabled
        {
            get { return enabled; }
            set
            {
                if (value != enabled)
                {
                    enabled = value;
                    OnEnablementChanged();
                }
            }
        }
         
        public event EventHandler EnablementChanged;

        protected virtual void OnEnablementChanged()
        {
            var copy = EnablementChanged;
            if (copy != null)
            {
                copy(this, EventArgs.Empty);
            }
        }
        public override System.Windows.Data.IValueConverter GetEnablementSourceConverter()
        {
            return null;
        }

         
        void INullableConfigurationItem.InitializeNullObject(object parentObject_)
        {
            this.Enabled = false;
            SetParentObject(parentObject_);
             
        }

        protected virtual void SetParentObject(object parentObject_){}
         
        [XmlIgnore]
        public override string EnablementDescriptionFormat
        {
            get { return string.Empty; }
        }
         
    }
}
