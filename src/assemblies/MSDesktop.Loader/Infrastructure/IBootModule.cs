﻿using MorganStanley.MSDotNet.MSGui.Impl;

namespace MorganStanley.Desktop.Loader.Infrastructure
{
    public interface IBootModule
    { 
        void Initialize(Framework framework_);
    }
}
