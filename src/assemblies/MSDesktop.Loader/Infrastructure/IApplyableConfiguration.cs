﻿
using System.Collections.Generic;
using MorganStanley.Desktop.Loader.Configuration;

namespace MorganStanley.Desktop.Loader.Infrastructure
{
    internal interface IApplyableConfiguration
    {
        IAssemblyRequester GetAssemblyRequester();
        IConfigurationApplier GetApplier();
        IList<IApplyableConfiguration> SubConfigurations { get; }
        void Resolve(MSDesktopConfiguration rootConfiguration_);
    }
}
