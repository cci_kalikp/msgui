﻿using MorganStanley.MSDotNet.MSGui.Impl;

namespace MorganStanley.Desktop.Loader.Infrastructure
{
    internal interface IConfigurationApplier
    {
        void Apply(IApplyableConfiguration configuration_, Framework framework_);
    }
}