﻿using MorganStanley.MSDotNet.MSGui.Impl;

namespace MorganStanley.Desktop.Loader.Infrastructure
{
    abstract class ConfigurationApplierBase<T>:IConfigurationApplier where T:IApplyableConfiguration
    {
 
        public virtual void Apply(IApplyableConfiguration configuration_, Framework framework_)
        {  
            if (!ApplyCurrent((T)configuration_, framework_)) return;
            if (configuration_.SubConfigurations != null)
            {
                foreach (var subConfiguration in configuration_.SubConfigurations)
                {
                    if (subConfiguration != null)
                    {
                        var subRequester = subConfiguration.GetAssemblyRequester();
                        if (subRequester != null) subRequester.RequestAssemblies(subConfiguration);

                        subConfiguration.GetApplier().Apply(subConfiguration, framework_);
                    }
                }
            }
        }

        protected abstract bool ApplyCurrent(T quitPromptDialogBehaviour_, Framework framework_); 
    }
}
