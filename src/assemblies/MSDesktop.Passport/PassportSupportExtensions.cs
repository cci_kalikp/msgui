﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Interop;
using Microsoft.Practices.Unity;
using MorganStanley.IED.Concord.Runtime;
using MorganStanley.IED.Passport.API;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Passport;

namespace MSDesktop.Passport
{
    public static class PassportSupportExtensions
    {
        private static IUnityContainer container;
        public static void EnablePassportSupport(this IFramework framework)
        {
            PassportExtensionPoints.BeforeStart += PassportExtensionPoints_BeforeStart;
            PassportExtensionPoints.CreatingPassportApplication += PassportExtensionPoints_CreatingPassportApplication;
        }

        static void PassportExtensionPoints_BeforeStart(object sender, EventArgs e)
        {
            HwndSource hwndSource = new HwndSource(0, 0, 0, 0, 0, string.Empty, IntPtr.Zero);

            System.Windows.Forms.Application.EnableVisualStyles();

            var section = (Hashtable)System.Configuration.ConfigurationManager.GetSection("AddInProbingPaths");
            if (section!=null)
            foreach (string path in section.Values)
            {
                AppDomain.CurrentDomain.AppendPrivatePath(path);
            }

            ConcordRuntime.Current.Context["ReleaseLink"] = "dev";
            ConcordRuntime.Current.Context["Version"] = Assembly.GetExecutingAssembly().GetName().Version.ToString();

        }

        static void PassportExtensionPoints_CreatingPassportApplication(object sender, EventArgs e)
        {
            container = sender as IUnityContainer;
            var api = new PassportApplication("Passport",
                                              ConcordRuntime.Current.Context["ServiceApplicationName"] as string,
                                              container);
        }
    }
}
