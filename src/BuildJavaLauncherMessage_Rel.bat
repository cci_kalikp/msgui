
pushd %cd%\examples\HttpServerExample\JavaLauncherMessage\src 
call build.cmd
call ant generate-train-metadata
popd  
call xcopy /S /C /H /R /Y %cd%\examples\HttpServerExample\JavaLauncherMessage\install\common\lib\* %cd%\..\install\common\lib\* 
call xcopy /S /C /H /R /Y %cd%\examples\HttpServerExample\JavaLauncherMessage\install\common\docs\* %cd%\..\install\common\docs\*  
call xcopy /R /Y %cd%\..\install\common\lib\launcherMessageAPI.jar %cd%\..\install\examples_release\HttpServerExample\lib\
call xcopy /R /Y %cd%\..\install\common\lib\launcherMessageExample.jar %cd%\..\install\examples_release\HttpServerExample\lib\
call xcopy /R /Y %cd%\..\install\common\lib\launcherMessageAPI.jar %cd%\..\install\examples_release\HttpServerExample\.debug\lib\
call xcopy /R /Y %cd%\..\install\common\lib\launcherMessageExample.jar %cd%\..\install\examples_release\HttpServerExample\.debug\lib\


