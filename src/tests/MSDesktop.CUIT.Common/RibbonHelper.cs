﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;

namespace MSDesktop.CUIT.Common
{
    public static class RibbonHelper
    {
        public static void ClickRibbonTitleBar(this UIAppMainWindow mainWindow)
        {
            var ribbon = new UIRibbonControl(mainWindow);
            Mouse.Click(ribbon);
        }
        public static void ClickTab(this WpfWindow mainWindow, string name)
        {
            var ribbon = new UIRibbonControl(mainWindow);
            var tab = new UITabPage(ribbon, name);
            Mouse.Click(tab);
        }

        public static void ClickRibbonButton(this UIAppMainWindow mainWindow, string tabName, string groupName, string buttonName)
        {
            var ribbon = new UIRibbonControl(mainWindow);
            var tab = new UITabPage(ribbon, tabName);
            Mouse.Click(tab);
            var group = new UIRibbonGroup(tab, groupName);

            group.WaitForControlReady();
            
            var button = new UIRibbonButton(group, buttonName);
            
            Mouse.Click(button);
        }

        public static void ClickRibbonButton(this UIAppMainWindow mainWindow, string tabName, string buttonName)
        {
            var ribbon = new UIRibbonControl(mainWindow);
            var tab = new UITabPage(ribbon, tabName);
            var button = new UIRibbonButton(tab, buttonName);
            Mouse.Click(button);
        }
    }

    #region  CUIT UI controls
    
    internal class UIRibbonControl : WpfTabList
    {

        public UIRibbonControl(UITestControl searchLimitContainer) :
            base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WpfTabList.PropertyNames.AutomationId] = "MSDesktop_Main_RibbonXamTabControl";
            #endregion
        }

    }


    internal class UITabPage : WpfTabPage
    {

        public UITabPage(UITestControl searchLimitContainer, string tabName) :
            base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WpfTabPage.PropertyNames.Name] = tabName;
            #endregion
        }
    }

    internal class UIRibbonGroup : WpfToolBar
    {
        public UIRibbonGroup(UITestControl searchLimitContainer, string groupName) :
            base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WpfToolBar.PropertyNames.Name] = groupName;
            #endregion
        }
    }

    internal class UIRibbonButton: WpfButton
    {
        public UIRibbonButton(UITestControl searchLimitContainer, string name):base(searchLimitContainer)
        {
            this.SearchProperties[WpfButton.PropertyNames.Name] = name;
        }
    }
   #endregion
}
