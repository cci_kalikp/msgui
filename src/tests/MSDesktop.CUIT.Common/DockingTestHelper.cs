using System.Diagnostics;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
using MSDesktop.CUIT.Extension;

namespace MSDesktop.CUIT.Common
{
    public static class DockingTestHelper
    {
        #region CUIT UITestControls   DockIndicators

        public class UIDockIndicatorWindow : WpfControl
        {
            public UIDockIndicatorWindow(UITestControl searchLimitContainer)
                : base(searchLimitContainer)
            {
                #region Search Criteria
                //this.SearchProperties[CustomToolWindowControl.PropertyNames.ControlType] = "UserControl";
                this.SearchProperties[WpfControl.PropertyNames.AutomationId] = "DockingIndicator";
                #endregion
            }

            public UIDockIndicator UIDockIndicator
            {
                get
                {
                    if (mIndicator == null)
                    {
                        mIndicator = new UIDockIndicator(this);
                    }
                    return mIndicator;
                }
            }
            private UIDockIndicator mIndicator;

            public UIDockIndicatorOut UITabbedDockIndicator
            {
                get
                {
                    if(mUITabbedDockIndicator == null)
                        mUITabbedDockIndicator = new UIDockIndicatorOut(this);
                    return mUITabbedDockIndicator;
                }
               
            }

            private UIDockIndicatorOut mUITabbedDockIndicator;
        }
        
        public class UIDockIndicator : WpfControl
        {
            public UIDockIndicator(UITestControl searchLimitContainer)
                : base(searchLimitContainer)
            {
                #region Search Criteria
                this.SearchProperties[WpfCustom.PropertyNames.ControlType] = "Custom";
                this.SearchProperties.Add(WpfControl.PropertyNames.AutomationId, "DockingIndicatorCentre", PropertyExpressionOperator.EqualTo);
                #endregion
            }

            public UIIndicator Left
            {
                get
                {
                    if (mLeft == null)
                    {
                        mLeft = new UIIndicator(this, DockLocation.Left);
                    }
                    return mLeft;
                }
            }

            public UIIndicator Top
            {
                get
                {
                    if (mTop == null)
                        mTop = new UIIndicator(this, DockLocation.Top);
                    return mTop;
                }
            }

            public UIIndicator Right
            {
                get
                {
                    if (mRight == null)
                        mRight = new UIIndicator(this, DockLocation.Right);
                    return mRight;
                }

            }

            public UIIndicator Bottom
            {
                get
                {
                    if (mBottom == null)
                        mBottom = new UIIndicator(this, DockLocation.Bottom);
                    return mBottom;
                }
            }

            public UIIndicator Center
            {
                get
                {
                    if (mCenter == null)
                        mCenter = new UIIndicator(this, DockLocation.Center);
                    return mCenter;
                }
            }

            private UIIndicator mLeft;
            private UIIndicator mTop;
            private UIIndicator mRight;
            private UIIndicator mBottom;
            private UIIndicator mCenter;

        }
        
        public class UIDockIndicatorOut : WpfControl
        {
            public UIDockIndicatorOut(UITestControl searchLimitContainer)
                : base(searchLimitContainer)
            {
                #region Search Criteria
                this.SearchProperties[WpfCustom.PropertyNames.ControlType] = "Custom";
                this.SearchProperties.Add(WpfControl.PropertyNames.AutomationId, "DockingIndicatorOut", PropertyExpressionOperator.EqualTo);

                #endregion
            }

            public UIIndicator Left
            {
                get
                {
                    if (mLeft == null)
                    {
                        mLeft = new UIIndicator(this, DockLocation.Left);
                    }
                    return mLeft;
                }
            }

            public UIIndicator Top
            {
                get
                {
                    if (mTop == null)
                        mTop = new UIIndicator(this, DockLocation.Top);
                    return mTop;
                }
            }

            public UIIndicator Right
            {
                get
                {
                    if (mRight == null)
                        mRight = new UIIndicator(this, DockLocation.Right);
                    return mRight;
                }

            }

            public UIIndicator Bottom
            {
                get
                {
                    if (mBottom == null)
                        mBottom = new UIIndicator(this, DockLocation.Bottom);
                    return mBottom;
                }
            }

            public UIIndicator Center
            {
                get
                {
                    if (mCenter == null)
                        mCenter = new UIIndicator(this, DockLocation.Center);
                    return mCenter;
                }
            }

            private UIIndicator mLeft;
            private UIIndicator mTop;
            private UIIndicator mRight;
            private UIIndicator mBottom;
            private UIIndicator mCenter;

        }

        public enum DockLocation
        {
            Left, Right, Top, Bottom, Center
        }
        
        public class UIIndicator : WpfCustom
        {
            public UIIndicator(UITestControl searchLimitContainer, DockLocation dockLocation)
                : base(searchLimitContainer)
            {
                #region Search Criteria
                this.SearchProperties[WpfCustom.PropertyNames.ControlType] = "Custom";
                
                this.SearchProperties.Add(WpfCustom.PropertyNames.AutomationId, dockLocation.ToString(), PropertyExpressionOperator.Contains);

                #endregion
            }

        }

        #endregion

        /// <summary>
        /// Drag the dragControl into to this contentpane. 
        /// </summary>
        /// <param name="target">the traget Content Pane</param>
        /// <param name="dragControl">any dragable contorl. e.g. The header of a content pane</param>
        /// <param name="location"></param>
        public static void Dock(this CustomContentPaneControl target, UITestControl dragControl, DockLocation location)
        {
            dragControl.EnsureClickable();
            Mouse.StartDragging(dragControl, new Point(100, dragControl.BoundingRectangle.Height-5));
            
            var point = new Point(target.BoundingRectangle.Left + target.BoundingRectangle.Width / 2, target.BoundingRectangle.Top + target.BoundingRectangle.Height / 2);
            Mouse.Hover(point, 1000);
            

           
            var window = new UIDockIndicatorWindow(null);

            var indicator = window.UIDockIndicator;
            if (indicator.WaitForControlExist(500))
            {
                UIIndicator position = null;
                switch (location)
                {
                    case DockLocation.Left:
                        position = indicator.Left;
                        break;

                    case DockLocation.Right:
                        position = indicator.Right;
                        break;

                    case DockLocation.Top:
                        position = indicator.Top;
                        break;
                    case DockLocation.Bottom:
                        position = indicator.Bottom;
                        break;
                    case DockLocation.Center:
                        position = indicator.Center;
                        break;
                }

                if (position.WaitForControlExist(300))
                {
                    Mouse.StopDragging(position);
                    return;
                }
            }

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.Fail("Docking failed");

        }

    }
}