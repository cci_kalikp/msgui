﻿using System.Diagnostics;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSDesktop.CUIT.Extension;

namespace MSDesktop.CUIT.Common
{
    public class ShellWindowHelper
    {

        public static void LoadLayout(UIAppMainWindow appMainWindow, string layout)
        {
            var appMenu = new UIInfragisticsXamRibboToolBar(appMainWindow).UIAppmenuMenu;

            // Click 'appmenu' popup menu
            Mouse.Click(appMenu);

            var loadLayoutItem = new UILoadLayoutMenuItem(appMainWindow);
            var layoutItem = loadLayoutItem.GetLayoutItem(layout);

            Mouse.Hover(loadLayoutItem);

            // Click 'Load Layout' -> 'FloatingDockTest' menu item);
            if (layoutItem.WaitForControlExist(300))
            {
                Mouse.Click(layoutItem, new Point(78, 23));
            }
            else
            {
                Assert.Fail("Cannot find {0} menu", layout);
            }

            var uIYesButton = new UILoadWindow().UIYesButton;

            // Click 'Yes' button
            if (uIYesButton.WaitForControlExist(300))
            {
                Mouse.Click(uIYesButton, new Point(40, 9));
            }
            else
            {
                Assert.Fail("Cannot find confirm button");
            }
            
        }

        public static void RecordLoadLayout(string appName, string layoutName)
        {
            var window = new UIAppWindow(appName);
            Trace.WriteLine("The main window " + window.Name );
            var appMenu = new UIInfragisticsXamRibboToolBar(window).UIAppmenuMenu;

            // Click 'appmenu' popup menu
            Mouse.Click(appMenu);
        
            var loadLayoutItem = new UILoadLayoutMenuItem(window);
            var layoutItem = loadLayoutItem.GetLayoutItem(layoutName);

            Mouse.Hover(loadLayoutItem);

            // Click 'Load Layout' -> 'FloatingDockTest' menu item);
            if (layoutItem.WaitForControlExist(300))
            {
                Mouse.Click(layoutItem, new Point(78, 23));
            }
            else
            {
                Assert.Fail("Cannot find " + layoutName + " menu");
            }

            var uIYesButton = new UILoadWindow().UIYesButton;

            // Click 'Yes' button
            if (uIYesButton.WaitForControlExist(300))
            {
                Mouse.Click(uIYesButton, new Point(40, 9));
            }
            else
            {
                Assert.Fail("Cannot find confirm button");
            }
            
        }


        public static void ClickMenuItem(UIAppMainWindow mainWindow, string menuName)
        {
            var appMenu = new UIInfragisticsXamRibboToolBar(mainWindow).UIAppmenuMenu;
            Mouse.Click(appMenu);

            var menuItem = new AppMenuItem(mainWindow, menuName);
            if(menuItem.WaitForControlReady(1000))
            {
                Mouse.Click(menuItem);
            }else
            {
                Assert.Fail("Cannot find {0}  menu", menuItem);
            }
        }

        public static void ClickMenuItem (UIAppMainWindow mainWindow, string parent, string item)
        {
             var appMenu = new UIInfragisticsXamRibboToolBar(mainWindow).UIAppmenuMenu;
            Mouse.Click(appMenu);

            var parentMenu = new AppMenuItem(appMenu, parent);
            if(!parentMenu.WaitForControlReady(300))
            {
                 Assert.Fail("Cannot find {0}  menu", parent);
            }
            
            Mouse.Hover(parentMenu);
            var menuItem = new AppMenuItem(parentMenu, item);

            if(! menuItem.WaitForControlExist(100))
            {
                 Assert.Fail("Cannot find {0}  menu", menuItem);
            }
            
            Mouse.Click(menuItem);
        }
    }



    #region CUIT 


    public class UIAppWindow : WpfWindow
    {

        public UIAppWindow(string appName)
        {
            #region Search Criteria
            this.SearchProperties.Add(WpfWindow.PropertyNames.Name, appName, PropertyExpressionOperator.Contains);
            this.SearchProperties.Add(WpfWindow.PropertyNames.ClassName, "HwndWrapper", PropertyExpressionOperator.Contains);
            #endregion
        }
    }


    class AppMenuItem:  WpfMenuItem
    {
        public AppMenuItem(WpfControl parent, string itemName):base(parent)
        {
            SearchProperties[WpfMenuItem.PropertyNames.Name] = itemName;
            SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
        }
    }

    public class UIInfragisticsXamRibboToolBar : WpfToolBar
    {

        public UIInfragisticsXamRibboToolBar(UITestControl window) :
            base(window)
        {
            #region Search Criteria
            this.SearchProperties[WpfToolBar.PropertyNames.AutomationId] = "ribbon";
            WindowTitles.Add(window.Name);
            foreach (var windowTitle in window.WindowTitles)
            {
                this.WindowTitles.Add(windowTitle);
            }
            #endregion
        }

        #region Properties
        public WpfMenu UIAppmenuMenu
        {
            get
            {
                if ((this.mUIAppmenuMenu == null))
                {
                    this.mUIAppmenuMenu = new WpfMenu(this);
                    #region Search Criteria
                    this.mUIAppmenuMenu.SearchProperties[WpfMenu.PropertyNames.AutomationId] = "appmenu";
                    
                    foreach (var windowTitle in WindowTitles)
                    {
                        this.mUIAppmenuMenu.WindowTitles.Add(windowTitle);
                    }
                    #endregion
                }
                return this.mUIAppmenuMenu;
            }
        }
        #endregion

        #region Fields
        private WpfMenu mUIAppmenuMenu;
        #endregion
    }

  
    public class UILoadLayoutMenuItem : WpfMenuItem
    {

        public UILoadLayoutMenuItem(UITestControl searchLimitContainer) :
            base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WpfMenuItem.PropertyNames.Name] = "Load Layout";
            foreach (var windowTitle in WindowTitles)
            {
                this.WindowTitles.Add(windowTitle);
            }
            
            #endregion
        }


        public WpfMenuItem GetLayoutItem(string layoutName)
        {
            if ((this.mUILayoutMenuItem == null))
            {
                this.mUILayoutMenuItem = new WpfMenuItem(this);
                #region Search Criteria
                this.mUILayoutMenuItem.SearchProperties[WpfMenuItem.PropertyNames.Name] = layoutName;
                this.mUILayoutMenuItem.SearchConfigurations.Add(SearchConfiguration.ExpandWhileSearching);
                foreach (var windowTitle in WindowTitles)
                {
                    this.mUILayoutMenuItem.WindowTitles.Add(windowTitle);
                }

                #endregion
            }
            return this.mUILayoutMenuItem;
        }
        #region Properties
      
        private WpfMenuItem mUILayoutMenuItem;
        #endregion
    }



    public class UILoadWindow : WpfWindow
    {

        public UILoadWindow()
        {
            #region Search Criteria
            this.SearchProperties[WpfWindow.PropertyNames.Name] = "Load";
            this.SearchProperties.Add(new PropertyExpression(WpfWindow.PropertyNames.ClassName, "HwndWrapper", PropertyExpressionOperator.Contains));
            this.WindowTitles.Add("Load");
            #endregion
        }

        #region Properties
        public WpfButton UIYesButton
        {
            get
            {
                if ((this.mUIYesButton == null))
                {
                    this.mUIYesButton = new WpfButton(this);
                    #region Search Criteria
                    this.mUIYesButton.SearchProperties[WpfButton.PropertyNames.Name] = "Yes";
                    this.mUIYesButton.WindowTitles.Add("Load");
                    #endregion
                }
                return this.mUIYesButton;
            }
        }
        #endregion

        #region Fields
        private WpfButton mUIYesButton;
        #endregion
    }
   
    #endregion
}
