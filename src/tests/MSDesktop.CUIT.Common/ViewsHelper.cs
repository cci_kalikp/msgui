﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Automation;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
using MSDesktop.CUIT.Extension;

namespace MSDesktop.CUIT.Common
{
    public class ViewsHelper
    {
        public static CustomContentPaneControl GetCustomContentPaneControl(WpfWindow shellWindow, string viewId)
        {
            var cp = new CustomContentPaneControl(shellWindow);
            cp.SearchProperties.Add(CustomContentPaneControl.PropertyNames.AutomationId, viewId);
            return cp;
        }
        
        public static CustomToolWindowControl GetCustomToolWindowControl(WpfWindow shellWindow, string viewId)
        {
            /* var cp = new CustomToolWindowControl(shellWindow);
             cp.SearchProperties.Add(CustomToolWindowControl.PropertyNames.AutomationId, viewId, PropertyExpressionOperator.Contains);
             return cp;*/
            return new FloatingWindowControl(shellWindow, viewId);
        }

        public static WpfWindow GetTaskDialog(string title = null)
        {
            return new UITaskDialog(title);
        }

        internal class FloatingWindowControl : CustomToolWindowControl
        {
            public FloatingWindowControl(WpfWindow searchLimitContainer, string viewId) :
                base(searchLimitContainer)
            {
                #region Search Criteria
                this.SearchProperties[CustomToolWindowControl.PropertyNames.ControlType] = "Custom";
                this.SearchProperties.Add(CustomToolWindowControl.PropertyNames.AutomationId, viewId,
                                          PropertyExpressionOperator.Contains);
                #endregion
            }
            
            public override WpfControl PaneHeader
            {
                get
                {
                    if ((this.mUITxtCaptionCustom == null))
                    {
                        this.mUITxtCaptionCustom = new WpfCustom(this);
                        #region Search Criteria
                        this.mUITxtCaptionCustom.SearchProperties[UITestControl.PropertyNames.ClassName] = "Uia.PaneHeaderControl";
                        this.mUITxtCaptionCustom.SearchProperties["AutomationId"] = "txtCaption";
                        #endregion
                    }
                    return this.mUITxtCaptionCustom;
                }
            }
            
            private WpfCustom mUITxtCaptionCustom;
        }


        

       
    }


    public class UIAppMainWindow : WpfWindow
    {
        
        public UIAppMainWindow(string appName)
        {
            #region Search Criteria

            this.SearchProperties.Add(WpfWindow.PropertyNames.Name, appName,
                                      PropertyExpressionOperator.Contains);
            this.SearchProperties.Add(new PropertyExpression(WpfWindow.PropertyNames.ClassName, "HwndWrapper", PropertyExpressionOperator.Contains));
            #endregion
        }


        private AutomationElement _automationElement;

        public  string GetLastViewId()
        {
            return this.Invoke(MainWindowSupportedCommands.GetLastCreatedView.ToString());
        }


        public  IEnumerable<string> GetAllViewIds()
        {
            return this.Invoke(MainWindowSupportedCommands.GetAllViews.ToString())
                .Split(';').Where((id) => !string.IsNullOrEmpty(id));
        }

        public  IEnumerable<string> GetFloatingViewIds()
        {
            return this.Invoke(MainWindowSupportedCommands.GetFloatingViews.ToString())
                .Split(';').Where((id) => !string.IsNullOrEmpty(id));
        }

        public  IEnumerable<string> GetDockedViewIds()
        {
            return this.Invoke(MainWindowSupportedCommands.GetDockedViews.ToString())
                .Split(';').Where((id) => !string.IsNullOrEmpty(id));
        }

        public  void MoveTo(double left, double top)
        {
            this.Invoke(MainWindowSupportedCommands.MoveTo.ToString(), left.ToString(), top.ToString());
        }

        private Process _process;
        public Process CurrentProcess
        {
            get
            {
                if (_process == null)
                {
                    try
                    {
                        var procId = this.Invoke(MainWindowSupportedCommands.GetProcessId.ToString());
                        _process = Process.GetProcessById(int.Parse(procId));
                    }
                    catch
                    {
                       
                    }
                }
                return _process;
            }
        }
    }
    
    public class UITaskDialog : WpfWindow
    {
        public UITaskDialog(string title = null, WpfWindow owner = null)
            : base(owner)
        {
            #region Search Criteria

            this.SearchProperties.Add(WpfWindow.PropertyNames.AutomationId, "MSDesktop_TaskDialog",
                                      PropertyExpressionOperator.EqualTo);
            this.SearchProperties.Add(new PropertyExpression(WpfWindow.PropertyNames.ClassName, "HwndWrapper", PropertyExpressionOperator.Contains));

            if (title != null)
            {
                //this.SearchProperties.Add(WpfWindow.PropertyNames.Name, title, PropertyExpressionOperator.Contains);
                this.WindowTitles.Add(title);
            }
           
            #endregion
        }

        public WpfButton GetButton(string name)
        {
            var btn = new WpfButton(this);
            btn.SearchProperties[WpfButton.PropertyNames.Name] = name;
            return btn;
        }

        public void ClickButton(string name)
        {
            var btn = GetButton(name);
            Mouse.Click(btn);
        }
    }

    class UICloseButton: WpfButton
    {
         public UICloseButton(WpfControl parent)
            : base(parent)
        {
            #region Search Criteria
            this.SearchProperties.Add(WpfWindow.PropertyNames.AutomationId, "XamRibbonCaptionButton_Right",
                                      PropertyExpressionOperator.EqualTo);
            #endregion
        }
    }


    class UISplashWindowControl : WpfWindow
    {
        public UISplashWindowControl()
        {
            this.SearchProperties.Add(WpfWindow.PropertyNames.AutomationId, "MSDesktop_Splash_Window", PropertyExpressionOperator.EqualTo);
            this.SearchProperties.Add(new PropertyExpression(WpfWindow.PropertyNames.ClassName, "HwndWrapper", PropertyExpressionOperator.Contains));
        }
    }
   
}
