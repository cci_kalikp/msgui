﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Windows.Automation;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSDesktop.CUIT.Common
{
    public static class VisualTestHelper
    {

        private static bool IsValid(Color expected, Color actual, int colorTolerance, int alphaTolerance)
        {
            if (Math.Abs(expected.R - actual.R) + Math.Abs(expected.G - actual.G) + Math.Abs(expected.B - actual.B) > colorTolerance)
                return false;
            if (Math.Abs(expected.A - actual.A) > alphaTolerance)
                return false;

            return true;

        }


        public static string GetRecordedImagePath(this WpfControl control)
        {
            AutomationElement nativeElement;

            if (control.ControlType == Microsoft.VisualStudio.TestTools.UITesting.ControlType.Window)
            {
                nativeElement = AutomationElement.RootElement.FindFirst(TreeScope.Children | TreeScope.Element,
                      new PropertyCondition(AutomationElement.NameProperty, control.Name, PropertyConditionFlags.IgnoreCase));

            }
            else
            {
                nativeElement = control.NativeElement as AutomationElement;
            }

            if (nativeElement == null)
            {
               Assert.Fail("cannot get peer for " + control.Name + " : " + control.ClassName);
            }
            var valuePattern = nativeElement.GetCurrentPattern(ValuePattern.Pattern) as ValuePattern;
            if (valuePattern == null)
            {
                Assert.Fail("The control does not support image recording ");
            }

            valuePattern.SetValue("GetImagePath");
            return valuePattern.Current.Value;
        }

        public static string GetPlaybackImagePath(this WpfControl control)
        {
            var path = control.GetRecordedImagePath();
            var fileName = System.IO.Path.GetFileNameWithoutExtension(path);
            var root = Path.GetDirectoryName(path);

            return Path.Combine(root, fileName + "_playback.bmp");
        }

        /// <summary>
        /// Capture image when main window got the focus
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private static Image CaptureImageWithFocusHack(this WpfControl control)
        {
            Image img = null;

            var b = control.BoundingRectangle;
            var leftTop = new Point(b.Left, b.Top);

            control.TopParent.SetFocus();

            Thread.Sleep(300);

            var bitmap = new Bitmap(b.Width, b.Height);
            {
                using (var g = Graphics.FromImage(bitmap))
                {
                    g.CopyFromScreen(leftTop, System.Drawing.Point.Empty, b.Size);
                }
                //bitmap.Save(path);
                img = (Image)bitmap;
            }
            return img;
        }

        

        public static void AssertImageEqual(this WpfControl control, double tolerantRatio, string message = "")
        {
            
            var path = control.GetRecordedImagePath();


            if (!System.IO.File.Exists(path))
            {
                Assert.Fail(message + "  Cannot find recorded image for " + path);
            }

            var savedImg = Image.FromFile(path);


            var img = control.CaptureImageWithFocusHack();

            Assert.IsTrue(img != null, "Cannot capture image when playback for {0}", path);
            Assert.AreEqual(savedImg.Width, img.Width, " different width for {0}", path);
            Assert.AreEqual(savedImg.Height, img.Height, " different height for {0}", path);
            int count = 0;
            var actual = new Bitmap(img);
            var expected = new Bitmap(savedImg);

            var diff = new Bitmap(img.Width, img.Height);

            for (int i = 0; i < actual.Width; ++i)
            {
                for (int j = 0; j < actual.Height; ++j)
                {
                    var ap = actual.GetPixel(i, j);
                    var ep = expected.GetPixel(i, j);

                    if (!IsValid(ep, ap, 0, 0))
                    {
                        ++count;
                        var dc = Color.FromArgb(Math.Abs(ep.R - ap.R), Math.Abs(ep.G - ap.G), Math.Abs(ep.B - ap.B));
                        diff.SetPixel(i, j, dc);  
                    }


                    else
                    {
                        diff.SetPixel(i, j, Color.Transparent);
                    }
                }
            }


            var fileName = System.IO.Path.GetFileNameWithoutExtension(path);
            var root = Path.GetDirectoryName(path);

            var p2 = Path.Combine(root, fileName + "_playback.bmp");
            if (File.Exists(p2))
                File.Delete(p2);

            using (var f = File.Open(p2, FileMode.CreateNew, FileAccess.Write))
            {
                using (var stream = new MemoryStream())
                {
                    img.Save(stream, ImageFormat.Bmp);
                    var buf = stream.GetBuffer();
                    f.Write(buf, 0, buf.Length);
                }
            }

            if (count > 0)
            {
                
                int total = (img.Width*img.Height);
                double ratio = ((double) count)/total;
                Assert.IsTrue(ratio <= tolerantRatio, "{0}  {1,5:##0.##%} pixels are different for {2}", message, ratio, path);
                
            }
           

        }
    }
}
