﻿using System;
using System.Drawing;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Automation;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSDesktop.CUIT.Extension;

namespace MSDesktop.CUIT.Common
{
    
    public static class WindowLocationHelper
    {
    
        private static Tuple<double, double> CalculateLocation(Rectangle src, Rectangle target)
        {
            double top = src.Top, left = src.Left;

            if (src.Height <= target.Top)
            {
                top = 0;
                return new Tuple<double, double>(left, top);
            }
            if (src.Width <= target.Left)
            {
                left = 0;
                return new Tuple<double, double>(left, top); ;
            }

            var botMargin = System.Windows.SystemParameters.WorkArea.Bottom - target.Height - target.Top;
            var rightMargin = System.Windows.SystemParameters.WorkArea.Bottom - target.Width - target.Left;


            if (src.Height <= botMargin)
            {
                top = target.Bottom;
                return new Tuple<double, double>(left, top); ;
            }
            if (src.Width <= rightMargin)
            {
                left = target.Right;
                return new Tuple<double, double>(left, top); ;
            }

            top = target.Bottom;
            return new Tuple<double, double>(left, top);
        }

        #region Commands
        /// <summary>
        /// Move toolWindow outside the target bounds; to make sure this tool window don't hind/cover any part of the target
        /// </summary>
        /// <param name="toolWindowControl"></param>
        /// <param name="target"></param>
        static public void MoveToOutsideOf(this CustomToolWindowControl toolWindowControl, WpfControl target)
        {
            var loc = CalculateLocation(toolWindowControl.BoundingRectangle, target.BoundingRectangle);

            var ae = toolWindowControl.NativeElement as AutomationElement;
            ae.Invoke("MoveTo", loc.Item1.ToString(), loc.Item2.ToString());
        }
        #endregion
       
    }
}
