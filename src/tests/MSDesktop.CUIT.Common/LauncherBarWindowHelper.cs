﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;

namespace MSDesktop.CUIT.Common
{
    public static class LauncherBarWindowHelper
    {

        public static void ClickMenuItem(UIAppMainWindow launcherBar, string name)
        {
            var optionsMenu = new OptionsMenuControl(launcherBar);
            optionsMenu.ClickMenuItme(name);
        }
    }




    #region  CUIT UI Controls

    public class OptionsMenuControl : WpfCustom
    {
        public OptionsMenuControl(UIAppMainWindow searchLimitContainer) :
            base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[UITestControl.PropertyNames.ClassName] = "Uia.OptionsButton";
            this.SearchProperties["AutomationId"] = "MSDesktop_LauncherBar_OptionsButton";
            #endregion

            _mainButton = new WpfButton(this);
            _mainButton.SearchProperties[WpfButton.PropertyNames.AutomationId] = "MSDesktop_OptionsButton_MainButton";

            _contextMenu = new WpfMenu(this);
            _contextMenu.SearchProperties[WpfMenu.PropertyNames.AutomationId] = "MSDesktop_OptionsButton_ContextMenu";
        }

        public WpfButton MainButton { get { return _mainButton; } }
        public WpfMenu ContextMenu { get { return _contextMenu; } }


        private WpfMenuItem GetMenuItem(string name)
        {
            var menuItem = new WpfMenuItem(_contextMenu);
            menuItem.SearchProperties[WpfMenuItem.PropertyNames.Name] = name;
            return menuItem;
        }

        public void ClickMenuItme(string name)
        {
            MainButton.WaitForControlReady(1000);
            Mouse.Click(MainButton);
            ContextMenu.WaitForControlExist(1000);
            var item = GetMenuItem(name);
            Mouse.Click(item);
        }

        private readonly WpfButton _mainButton;
        private readonly WpfMenu _contextMenu;
    }
    #endregion
    


}
