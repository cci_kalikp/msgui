﻿using System;
using System.Text;
using System.Linq;
using System.Windows.Automation;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSDesktop.CUIT.Common
{
    enum MainWindowSupportedCommands
    {
        GetLastCreatedView, GetAllViews, GetFloatingViews, GetDockedViews, GetAppliationTitle,
        MoveTo, GetProcessId,
        SetToPlaybackMode,
    }


    internal static class CommandsHelper
    {
        public static string Invoke(this AutomationElement automationElement, string command, params string[] paramreters)
        {   
            var vp = automationElement.GetCurrentPattern(ValuePattern.Pattern) as ValuePattern;
            if (vp == null)
                throw new NotSupportedException(command);
            vp.SetValue(paramreters.Aggregate(command, (ac, para) => ac + " " + para));
            return vp.Current.Value;
        }

        public static string Invoke(this WpfControl control, string command,  params string[] paramreters)
        {
            AutomationElement nativeElement;
            if (control.ControlType == Microsoft.VisualStudio.TestTools.UITesting.ControlType.Window)
            {
                nativeElement = AutomationElement.RootElement.FindFirst(TreeScope.Children | TreeScope.Element,
                      new PropertyCondition(AutomationElement.NameProperty, control.Name, PropertyConditionFlags.IgnoreCase));

            }
            else
            {
                nativeElement = control.NativeElement as AutomationElement;
            }

            if (nativeElement == null)
            {
               Assert.Fail("cannot get peer for " + control.Name + " : " + control.ClassName);
            }

            return nativeElement.Invoke(command, paramreters);

        }
    }

    public  enum ShellMode
    {
        Unknown, Launcher, Ribbon
    }


  
   
}
