﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Automation;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSDesktop.CUIT.Common
{
    public class ApplicationHelper
    {
        public static IEnumerable<UIApplication> GetApplications()
        {
            var shells =  AutomationElement.RootElement.FindAll(TreeScope.Children | TreeScope.Element,
                                                                new PropertyCondition(
                                                                    AutomationElement.AutomationIdProperty,
                                                                    "MSDesktop_Main_Window_ShellWindow",
                                                                    PropertyConditionFlags.IgnoreCase));

            for (int i = 0; i < shells.Count; ++i)
            {
                yield return new UIApplication(shells[i]);
            }

            var launchers = AutomationElement.RootElement.FindAll(TreeScope.Children | TreeScope.Element,
                                                                  new PropertyCondition(
                                                                      AutomationElement.AutomationIdProperty,
                                                                      "MSDesktop_Main_Window_LauncherBarWindow",
                                                                      PropertyConditionFlags.IgnoreCase));

            for (int i = 0; i < launchers.Count; ++i)
            {
                yield return new UIApplication(launchers[i]);
            }

        }

        public static UIApplication GetApplication(string name)
        {
            var mainWindow = new UIAppMainWindow(name);
            AutomationElement nativeElement;
            if (mainWindow.ControlType == Microsoft.VisualStudio.TestTools.UITesting.ControlType.Window)
            {
                nativeElement = AutomationElement.RootElement.FindFirst(TreeScope.Children | TreeScope.Element,
                                                                        new PropertyCondition(AutomationElement.NameProperty, mainWindow.Name, PropertyConditionFlags.IgnoreCase));

            }
            else
            {
                nativeElement = mainWindow.NativeElement as AutomationElement;
            }

            if (nativeElement == null)
            {
                Assert.Fail("cannot get peer for " + mainWindow.Name + " : " + mainWindow.ClassName);
            }

            return new UIApplication(nativeElement);
        }

        public static void LoginWithProfile(string profile)
        {
            var automationElement = AutomationElement.RootElement.FindFirst(TreeScope.Children | TreeScope.Element,
                                                  new PropertyCondition(
                                                      AutomationElement.AutomationIdProperty,
                                                      "MSDesktop_LoginWindow"));
            var title = automationElement.Invoke(MainWindowSupportedCommands.GetAppliationTitle.ToString());

            var loginWindow = new WpfWindow();
            loginWindow.SearchProperties.Add(new PropertyExpression(WpfWindow.PropertyNames.ClassName, "HwndWrapper", PropertyExpressionOperator.Contains));
            loginWindow.WindowTitles.Add(title);

            var comboBox = new WpfComboBox(loginWindow);
            comboBox.SearchProperties[WpfComboBox.PropertyNames.AutomationId] = "LoginWindow_Profile_ComboBox";
            comboBox.WindowTitles.Add(title);

            var item = (from i in comboBox.Items
                        where i.Name.Contains(profile)
                        select i).FirstOrDefault();

            if (item == null)
            {
                throw new ApplicationException("Cannot choose profile " + profile);
            }

            comboBox.SelectedItem = item.Name;

            var btn = new WpfButton(loginWindow);
            btn.SearchProperties[WpfButton.PropertyNames.Name] = "OK";
            btn.WindowTitles.Add(title);
            btn.WaitForControlEnabled(1000);
            Mouse.Click(btn);

        }

        public static bool WaitforSplahwindowDone(int timeout)
        {
            try
            {
                var automationElement =  AutomationElement.RootElement.FindFirst(
                       TreeScope.Children | TreeScope.Element,
                       new PropertyCondition(AutomationElement.AutomationIdProperty,
                                             "MSDesktop_Splash_Window",
                                             PropertyConditionFlags.IgnoreCase));

                var appName = automationElement.Invoke("GetAppliationTitle");
                var splash = new WpfWindow();
                splash.SearchProperties.Add(WpfWindow.PropertyNames.Name, appName,
                                         PropertyExpressionOperator.Contains);
                splash.SearchProperties.Add(new PropertyExpression(WpfWindow.PropertyNames.ClassName, "HwndWrapper", PropertyExpressionOperator.Contains));

                
                if (splash.WaitForControlExist(1000))
                {
                    return splash.WaitForControlNotExist(timeout);
                }
                return true;
            }
            catch
            {
                return true;  //when failed, treated as the splash window disapeared before can obtain it.
            }


        }
    }

    public class UIApplication
    {
        public AutomationElement AutomationElement { get; private set; }
        public UIApplication(AutomationElement automationElement)
        {
            AutomationElement = automationElement;
            var id =
               (string)AutomationElement.GetCurrentPropertyValue(
                   AutomationElement.AutomationIdProperty);

            ShellMode = ShellMode.Unknown;
            if (id == "MSDesktop_Main_Window_ShellWindow")
            {
                ShellMode = ShellMode.Ribbon;
            }
            if (id == "MSDesktop_Main_Window_LauncherBarWindow")
            {
                ShellMode = ShellMode.Launcher;
            }
        }

        public ShellMode ShellMode { get; private set; }

        private UIAppMainWindow _mainWindow;
        public UIAppMainWindow MainWindow
        {
            get
            {
                if (_mainWindow == null)
                {
                    var name = AutomationElement.Invoke(MainWindowSupportedCommands.GetAppliationTitle.ToString());
                    _mainWindow = new UIAppMainWindow(name);
                }
                return _mainWindow;
            }
        }


        public string ApplicationName
        {
            get
            {
                return AutomationElement.Invoke(MainWindowSupportedCommands.GetAppliationTitle.ToString());
            }
        }


        public void SetToPlaybackMode()
        {
            AutomationElement.Invoke(MainWindowSupportedCommands.SetToPlaybackMode.ToString());
        }

        public UITaskDialog GetTaskDialog(string title = null)
        {
            return new UITaskDialog(title, null);
        }

    }

}