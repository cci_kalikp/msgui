﻿using MorganStanley.MSDotNet.Runtime;
using NUnit.Framework;

namespace MorganStanley.Desktop.EntityServices.Common.Testing
{
    [SetUpFixture]
    public class SetUpFixture
    {
        static SetUpFixture()
        {
            AssemblyResolver.Load("CommonTests.msde.config");
        }
    }
}
