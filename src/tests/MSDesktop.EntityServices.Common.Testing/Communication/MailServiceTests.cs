﻿using System.Net.Mail;
using MorganStanley.Desktop.EntityServices.Common.Communication;
using MorganStanley.Desktop.EntityServices.TestingCommon;
using NUnit.Framework;

namespace MorganStanley.Desktop.EntityServices.Common.Testing.Communication
{
    public class MailServiceTests
    {

        public class When_sending_a_mail : BasicContext
        {
            private MailService _mailService;
            private MailMessage _mailMessage;

            protected override void Arrange()
            {
                _mailService = new MailService();
            }

            protected override void Act()
            {
                _mailMessage = _mailService.CreateMail("jasd", new[] { "jasd" }, new[] { "jasd" }, "Test", "Test");
            }

            [Test]
            public void It_should_set_the_from_address_correctly()
            {
                _mailMessage.From.Address.ShouldEqual("jasd@ms.com");
            }

            [Test]
            public void It_should_set_the_to_address_correctly()
            {
                _mailMessage.To[0].Address.ShouldEqual("jasd@ms.com");
            }

            [Test]
            public void It_should_set_the_cc_address_correctly()
            {
                _mailMessage.To[0].Address.ShouldEqual("jasd@ms.com");
            }

            [Test]
            public void It_should_default_html_body_to_false()
            {
                _mailMessage.IsBodyHtml.ShouldBeFalse();
            }
        }

    }
}
