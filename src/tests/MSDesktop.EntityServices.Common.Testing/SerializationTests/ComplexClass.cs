﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace MorganStanley.Desktop.EntityServices.Common.Testing.SerializationTests
{
    [XmlRoot("MyComplexTestClass")]
    public class ComplexClass
    {
        [XmlAttribute("MyProperty")]
        public string Property1 { get; set; }

        [XmlArray("MyItems")]
        [XmlArrayItem("AnItem")]
        public List<string> Items { get; set; }
    }
}
