﻿namespace MorganStanley.Desktop.EntityServices.Common.Testing.SerializationTests
{
    public class SimpleClass
    {
        public string Property1 { get; set; }
        public string Property2 { get; set; }
    }
}
