﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace MorganStanley.Desktop.EntityServices.Common.Testing.SerializationTests
{
    [XmlRoot("Root")]
    public class NestedXmlRootClass1
    {
        public NestedXmlRootClass1()
        {
            Nested = new List<NestedXmlRootClass2>();
        }
        [XmlArray("Contexts")]
        [XmlArrayItem("context")]
        public List<NestedXmlRootClass2> Nested { get; set; }
        public string Name { get; set; }
    }

    [XmlRoot("Nested")]
    public class NestedXmlRootClass2
    {
        public string Name { get; set; }
    }
}
