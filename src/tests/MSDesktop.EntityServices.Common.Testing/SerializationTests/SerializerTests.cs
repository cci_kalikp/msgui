﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using MorganStanley.Desktop.EntityServices.Common.Serialization;
using NUnit.Framework;

namespace MorganStanley.Desktop.EntityServices.Common.Testing.SerializationTests
{
    [TestFixture]
    public class SerializerTests
    {
        private SimpleClass _simple;
        private SimpleClass _simpleDeserialized;

        private ComplexClass _complex;
        private ComplexClass _complexDeserialized;

        [SetUp]
        public void SetUp()
        {
            _simple = new SimpleClass { Property1 = "Dan", Property2 = "Huth" };
            _complex = new ComplexClass
                           {
                               Property1 = "Dan",
                               Items = new List<string> { "Property1", "Property2" }
                           };
        }

        public void AssertSimple()
        {
            Assert.IsNotNull(_simpleDeserialized);
            Assert.AreEqual(_simple.Property1, _simpleDeserialized.Property1);
            Assert.AreEqual(_simple.Property2, _simpleDeserialized.Property2);
        }

        public void AssertComplex()
        {
            Assert.IsNotNull(_complexDeserialized);
            Assert.AreEqual(_complex.Property1, _complexDeserialized.Property1);
            Assert.IsNotNull(_complexDeserialized.Items);
            Assert.AreEqual(2, _complexDeserialized.Items.Count);
            Assert.AreEqual(_complex.Items[0], _complexDeserialized.Items[0]);
            Assert.AreEqual(_complex.Items[1], _complexDeserialized.Items[1]);
        }

        [Test]
        public void SerializeShouldSupportBasicSerializationToString()
        {
            string xml = _simple.Serialize();
            _simpleDeserialized = xml.DeserializeAs<SimpleClass>();
        }

        [Test]
        public void SerializeShouldSupportBasicSerializationToNode()
        {
            XmlNode xml = _simple.SerializeToNode();
            _simpleDeserialized = xml.DeserializeAs<SimpleClass>();
        }

        [Test]
        public void SerializeShouldSupportBasicSerializationToStream()
        {
            Stream xml = _simple.SerializeToStream();
            _simpleDeserialized = xml.DeserializeAs<SimpleClass>();
        }

        [Test]
        public void SerializeShouldSupportComplexSerializationToString()
        {
            string xml = _complex.Serialize();
            _complexDeserialized = xml.DeserializeAs<ComplexClass>();
        }

        [Test]
        public void SerializeShouldSupportComplexSerializationToNode()
        {
            XmlNode xml = _complex.SerializeToNode();
            _complexDeserialized = xml.DeserializeAs<ComplexClass>();
        }

        [Test]
        public void SerializeShouldSupportComplexSerializationToStream()
        {
            Stream xml = _complex.SerializeToStream();
            _complexDeserialized = xml.DeserializeAs<ComplexClass>();
        }

        [Test]
        public void SerializeShouldSupportNestedXmlRootElements()
        {
            var nested = new NestedXmlRootClass1();
            var s = nested.Serialize();
        }
    }
}
