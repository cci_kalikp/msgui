﻿using System;
using MorganStanley.Desktop.EntityServices.Common.Cache;
using MorganStanley.Desktop.EntityServices.TestingCommon;
using Rhino.Mocks;

namespace MorganStanley.Desktop.EntityServices.Common.Testing.CacheTests
{
    public abstract class CachingServiceTestBase : TestBase
    {
        protected ICacheService CachingService { get; private set; }
        protected ICache<TKey, T> SetUpCachingMocks<TKey, T>(TimeSpan timeSpan)
        {
            CachingService = MockRepository.GenerateMock<ICacheService>();
            var cache = MockRepository.GenerateMock<ICache<TKey, T>>();
            CachingService
                .Stub(x => x.CreateCache<TKey, T>(timeSpan))
                .Return(cache);
            cache.Stub(x => x.Subscribe());
            return cache;
        }
    }
}
