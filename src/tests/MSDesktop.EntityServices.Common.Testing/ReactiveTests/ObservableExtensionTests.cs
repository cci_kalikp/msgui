﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using MorganStanley.Desktop.EntityServices.TestingCommon;
using NUnit.Framework;

namespace MorganStanley.Desktop.EntityServices.Common.Testing.ReactiveTests
{
    [TestFixture]
    public class ObservableExtensionTests
    {
        private ISubject<object> _subject;
        private IController _controller;

        [SetUp]
        public void SetUp()
        {
            _subject = new Subject<object>();
            _controller = new BooleanController();
        }

        [Test]
        public void ShouldThrowExceptionWhenSourceIsNull()
        {
            // Arrange
            _subject = null;
            var exceptionTester = new ExceptionTester();

            // Act
            exceptionTester.Act(() => _subject.ControlWith(_controller));

            // Assert
            exceptionTester.ShouldThrow<ArgumentNullException>();
        }

        [Test]
        public void ShouldThrowExceptionWhenControllerIsNull()
        {
            // Arrange
            var exceptionTester = new ExceptionTester();

            // Act
            exceptionTester.Act(() => _subject.ControlWith(null));

            // Assert
            exceptionTester.ShouldThrow<ArgumentNullException>();
        }

        [Test]
        public void ShouldAllowNormalFlowWhenNotBlocked()
        {
            // Arrange
            bool updateRecieved = false;
            _subject.ControlWith(_controller).Subscribe(x => updateRecieved = true);
            
            // Act
            _subject.OnNext(new object());

            // Assert
            Assert.IsTrue(updateRecieved);
        }

        [Test]
        public void ShouldGateNormalFlowWhenBlocked()
        {
            // Arrange
            bool updateRecieved = false;
            _controller.Block();
            _subject.ControlWith(_controller).Subscribe(x => updateRecieved = true);

            // Act
            _subject.OnNext(new object());

            // Assert
            Assert.IsFalse(updateRecieved);
        }

        [Test]
        public void ShouldSendGatedUpdateWhenGateIsUnblocked()
        {
            // Arrange
            int updateRecievedCount = 0;
            _controller.Block();
            _subject.ControlWith(_controller).Subscribe(x => updateRecievedCount++);
            _subject.OnNext(new object());
            _subject.OnNext(new object());

            // Act
            _controller.Unblock();

            // Assert
            Assert.AreEqual(2, updateRecievedCount);
        }

        [Test]
        public void ShouldSendUpdatesButKeepGatingOnWhenAcceptCalled()
        {
            // Arrange
            int updateRecievedCount = 0;
            _controller.Block();
            _subject.ControlWith(_controller).Subscribe(x => updateRecievedCount++);
            _subject.OnNext(new object());
            _subject.OnNext(new object());

            // Act
            _controller.Accept();

            // Assert
            Assert.AreEqual(2, updateRecievedCount);
            Assert.IsTrue(_controller.IsBlocked);
        }

        [Test]
        public void ShouldForwardExceptionsAndDisposeOnError()
        {
            // Arrange
            bool exceptionWasCalled = false;
            _subject.ControlWith(_controller).Subscribe(x => { }, ex => exceptionWasCalled = true);

            // Act
            _subject.OnError(new Exception());

            // Assert
            Assert.IsTrue(exceptionWasCalled);
        }

        [Test]
        public void ShouldNotSendBlockedUpdatesWhenOnCompleteCalled()
        {
            // Arrange
            bool onNextWasCalled = false;
            bool onCompletedWasCalled = false;
            _subject.ControlWith(_controller).Subscribe(x => onNextWasCalled = true, ex => { }, () => onCompletedWasCalled = true);
            _controller.Block();
            _subject.OnNext(new object());
            _subject.OnNext(new object());

            // Act
            _subject.OnCompleted();

            // Assert
            Assert.IsFalse(onNextWasCalled);
            Assert.IsTrue(onCompletedWasCalled);
        }

        [Test]
        public void ShouldSendBlockedUpdatesWhenOnCompleteCalled()
        {
            // Arrange
            bool onNextWasCalled = false;
            bool onCompletedWasCalled = false;
            _subject.ControlWith(_controller).Subscribe(x => onNextWasCalled = true, ex => { }, () => onCompletedWasCalled = true);
            _controller.Block();
            _subject.OnNext(new object());
            _subject.OnNext(new object());

            // Act
            _subject.OnCompleted();

            // Assert
            Assert.IsFalse(onNextWasCalled);
            Assert.IsTrue(onCompletedWasCalled);
        }
    }
}
