﻿using System;
using System.Reactive.Linq;
using MorganStanley.Desktop.EntityServices.TestingCommon;
using NUnit.Framework;

namespace MorganStanley.Desktop.EntityServices.Common.Testing.ReactiveTests
{
    [TestFixture]
    public class AbstractObserverTests
    {
        [Test]
        public void OnCompletedShouldSetIsStoppedFlag()
        {
            // Arrange
            var testable = new TestableObserver();

            // Act
            testable.OnCompleted();

            // Assert
            Assert.IsTrue(testable.IsCompleted);
        }

        [Test]
        public void OnErrorShouldRaiseExceptionIfErrorIsNull()
        {
            // Arrange
            var testable = new TestableObserver();
            var exceptionTester = new ExceptionTester();

            // Act
            exceptionTester.Act(() => testable.OnError(null));

            // Assert
            exceptionTester.ShouldThrow<ArgumentNullException>();
        }

        [Test]
        public void OnErrorShouldForwardCallToError()
        {
            // Arrange
            var testable = new TestableObserver();

            // Act
            testable.OnError(new Exception());

            // Assert
            Assert.IsNotNull(testable.Exception);
        }

        [Test]
        public void OnNextShouldForwardCallToNext()
        {
            // Arrange
            var testable = new TestableObserver();

            // Act
            testable.OnNext(new object());

            // Assert
            Assert.IsNotNull(testable.Value);
        }

        [Test]
        public void OnNextShouldNotForwardCallsToNextAfterOnComplete()
        {
            // Arrange
            var testable = new TestableObserver();
            testable.OnCompleted();

            // Act
            testable.OnNext(new object());

            // Assert
            Assert.IsNull(testable.Value);
        }

        [Test]
        public void OnNextShouldNotForwardCallsToNextIfDisposed()
        {
            // Arrange
            var testable = new TestableObserver();
            testable.Dispose();

            // Act
            testable.OnNext(new object());

            // Assert
            Assert.IsNull(testable.Value);
        }

        class TestableObserver : AbstractObserver<object>
        {
            public bool IsCompleted { get; set; }
            public Exception Exception { get; set; }
            public object Value { get; set; }

            protected override void Completed()
            {
                IsCompleted = true;
            }

            protected override void Error(Exception error)
            {
                Exception = error;
            }

            protected override void Next(object value)
            {
                Value = value;
            }
        }
    }
}
