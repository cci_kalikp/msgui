﻿using System;
using System.Reactive.Linq;
using NUnit.Framework;

namespace MorganStanley.Desktop.EntityServices.Common.Testing.ReactiveTests
{
    [TestFixture]
    public class BooleanControllerTests
    {
        [Test]
        public void ControllerShouldRaiseOnNextWhenBlockIsCalledFirstTime()
        {
            // Arrange
            bool onNextWasCalled = false;
            var controller = new BooleanController();
            controller.Subscribe(x => onNextWasCalled = true);

            // Act
            controller.Block();

            // Assert
            Assert.IsTrue(onNextWasCalled);
        }

        [Test]
        public void ControllerNotShouldRaiseOnNextWhenBlockIsCalledAndAlreadyBlocked()
        {
            // Arrange
            var controller = new BooleanController(true);
            bool onNextWasCalled = false;
            controller.Subscribe(x => onNextWasCalled = true);

            // Act
            controller.Block();

            // Assert
            Assert.IsFalse(onNextWasCalled);
        }

        [Test]
        public void ControllerShouldReportIsBlockedAsTrueWhenBlockCalled()
        {
            // Arrange
            var controller = new BooleanController();

            // Act
            controller.Block();

            // Assert
            Assert.IsTrue(controller.IsBlocked);
        }

        [Test]
        public void ControllerShouldRaiseOnNextWhenUnblockIsCalledFirstTime()
        {
            // Arrange
            var controller = new BooleanController(true);
            bool onNextWasCalled = false;
            controller.Subscribe(x => onNextWasCalled = true);

            // Act
            controller.Unblock();

            // Assert
            Assert.IsTrue(onNextWasCalled);
        }

        [Test]
        public void ControllerShouldNotRaiseOnNextWhenUnblockIsCalledAndAlreadyUnblocked()
        {
            // Arrange
            var controller = new BooleanController(false);
            bool onNextWasCalled = false;
            controller.Subscribe(x => onNextWasCalled = true);

            // Act
            controller.Unblock();

            // Assert
            Assert.IsFalse(onNextWasCalled);
        }

        [Test]
        public void ControllerShouldReportIsBlockedAsFalseWhenUnblockCalled()
        {
            // Arrange
            var controller = new BooleanController(true);

            // Act
            controller.Unblock();

            // Assert
            Assert.IsFalse(controller.IsBlocked);
        }

        [Test]
        public void AcceptShouldReportIsBlockedAsFalseWhenQueriedInContextOfAcceptCall()
        {
            // Arrange
            var controller = new BooleanController(true);
            bool isBlocked = false;
            controller.Subscribe(x => isBlocked = x.IsBlocked);

            // Act
            controller.Accept();

            // Assert
            Assert.IsFalse(isBlocked);
        }
    }
}
