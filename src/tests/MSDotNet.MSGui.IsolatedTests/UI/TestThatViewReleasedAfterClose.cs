﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.IsolatedTests/UI/TestThatViewReleasedAfterClose.cs#1 $
// $Change: 839595 $
// $DateTime: 2013/07/29 12:06:16 $
// $Author: milosp $

using System;
using System.Reflection;
using System.Threading;
using System.Windows.Controls;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers;
using MorganStanley.MSDotNet.MSGui.Themes;
using MorganStanley.MSDotNet.MSLog;
using Rhino.Mocks;

namespace MSDotNet.MSGui.IsolatedTests.UI
{
  
  [TestClass]
  public class TestThatViewReleasedAfterClose
  {
      [TestMethod]
      public void TestThatViewReleasedAfterCloseInTabbedDock()
      {
          ExampleModule1.WindowCreated = new AutoResetEvent(false);
          ExampleModule1.ReadyToClose = new AutoResetEvent(false);
          ExampleModule1.WindowClosed = new AutoResetEvent(false);

          var thread = new Thread(state_ => StartFramework());
          thread.SetApartmentState(ApartmentState.STA);
          thread.Start();

          ExampleModule1.WindowCreated.WaitOne();
          Assert.IsNotNull(ExampleModule1.ViewReference.Target);
          GC.Collect();
          Assert.IsTrue(ExampleModule1.ViewReference.IsAlive);

          ExampleModule1.ReadyToClose.Set();
          ExampleModule1.WindowClosed.WaitOne();
          GC.Collect();
          Assert.IsFalse(ExampleModule1.ViewReference.IsAlive);

          thread.Abort();
      }

      private static void StartFramework()
      {
          try
          {
              MSLog.RemoveDestinationFromTarget(new MSLogTarget("/default/errorlogging"), "ExceptionLogger");
          }
          catch (Exception)
          {
              //ignore
          }
          var stubbedAssemblyProvider = MockRepository.GenerateStub<IEntryAssemblyProvider>();
          stubbedAssemblyProvider.Stub(x => x.GetEntryAssembly()).Return(Assembly.GetExecutingAssembly());
          EntryAssemblyHelper.EntryAssemblyProvider = stubbedAssemblyProvider;
          var framework = new Framework();
          framework.AddModule(typeof(ExampleModule1));
          framework.AddBlackTheme();
          framework.Start();
      }

      class ExampleModule1 : IModule
      {
          private readonly IChromeManager m_manager;
          private readonly IChromeRegistry m_registry;
          private readonly IApplication m_application;

          internal static WeakReference ViewReference;
          internal static AutoResetEvent WindowCreated;
          internal static AutoResetEvent ReadyToClose;
          internal static AutoResetEvent WindowClosed;

          public ExampleModule1(IChromeManager manager_, IChromeRegistry registry_, IApplication application_)
          {
              m_manager = manager_;
              m_registry = registry_;
              m_application = application_;
          }

          public void Initialize()
          {
              m_registry.RegisterWindowFactory("window", (container_, state_) =>
                                                             {
                                                                 container_.Content = new UserControl();
                                                                 return true;
                                                             });
              m_application.ApplicationLoaded += OnApplicationLoaded;
          }

          private void OnApplicationLoaded(object sender_, EventArgs eventArgs_)
          {
              var view = m_manager.CreateWindow("window", new InitialWindowParameters());
              ViewReference = new WeakReference(view);
              WindowCreated.Set();
              ReadyToClose.WaitOne();
              view.Close();
              WindowClosed.Set();
          }
      }  
  }
}
