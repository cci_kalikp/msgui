﻿using System;
using System.ComponentModel;
using System.IO;
using System.Xml;
using System.Text;

using NUnit.Framework;

using MorganStanley.MSDesktop.Rambo.API;

namespace MorganStanley.MSDesktop.Rambo.Test
{
  [TestFixture]
  public class RamboResourceTests : UnitTestBase
  {
    #region Fields

    private XmlReader _reader;
    private RamboResource _instance;
    private string _changedPropertyName;

    #endregion

    [SetUp]
    public void Initialise()
    {
      _reader = GetTestData("ResponseRamboResource.xml");
      _instance = new RamboResource();
      _instance.PropertyChanged += delegate(object sender_, PropertyChangedEventArgs e_)
                                     {
                                       _changedPropertyName = e_.PropertyName;
                                     };
    }

    [Test]
    public void Constructor_Parameterless_NewInstance()
    {
      Assert.IsNotNull(_instance.XmlData);
      Assert.AreEqual(0, _instance.XmlData.OuterXml.Length);
    }

    //[Test]
    //public void ResourceID_ReadWrite_SameData()
    //{
    //  const string testValue = "ResourceId";
    //  _instance.ResourceId = testValue;

    //  Assert.AreEqual(_changedPropertyName, "ResourceId");
    //  Assert.AreEqual(testValue, _instance.ResourceId);
    //}

    [Test]
    public void UserName_ReadWrite_SameData()
    {
      const string testValue = "Username";
      _instance.UserName = testValue;

      Assert.AreEqual(_changedPropertyName,"UserName");
      Assert.AreEqual(testValue, _instance.UserName);
    }

    [Test]
    public void ApplicationName_ReadWrite_SameData()
    {
      const string testValue = "ApplicationName";
      _instance.ApplicationName = testValue;

      Assert.AreEqual(_changedPropertyName,"ApplicationName");
      Assert.AreEqual(testValue, _instance.ApplicationName);
    }

    [Test]
    public void ResourceType_ReadWrite_SameData()
    {
      const string testValue = ConcordInterface.CONFIGURATOR;
      _instance.ResourceType = testValue;

      Assert.AreEqual(_changedPropertyName,"ResourceType");
      Assert.AreEqual(testValue, _instance.ResourceType);
    }

    [Test]
    public void ResourceType_ReadWrite_Null()
    {
      _instance.ResourceType = null;

      Assert.AreNotEqual(_changedPropertyName,"ResourceType");
      Assert.IsNull(_instance.ResourceType);
    }

    [Test]
    public void AppletName_ReadWrite_SameData()
    {
      const string testValue = "Applet";
      _instance.AppletName = testValue;

      Assert.AreEqual(_changedPropertyName,"AppletName");
      Assert.AreEqual(testValue, _instance.AppletName);
    }

    [Test]
    public void GridName_ReadWrite_SameData()
    {
      const string testValue = "GridName";
      _instance.GridName = testValue;

      Assert.AreEqual(_changedPropertyName,"GridName");
      Assert.AreEqual(testValue, _instance.GridName);
    }

    [Test]
    public void GridLayoutName_ReadWrite_SameData()
    {
      const string testValue = "GridLayoutName";
      _instance.GridLayout = testValue;

      Assert.AreEqual(_changedPropertyName,"GridLayout");
      Assert.AreEqual(testValue, _instance.GridLayout);
    }

    [Test]
    public void Profile_ReadWrite_SameData()
    {
      const string testValue = "Profile";
      _instance.Profile = testValue;

      Assert.AreEqual(_changedPropertyName,"Profile");
      Assert.AreEqual(testValue, _instance.Profile);
    }

    [Test]
    public void ConfigName_ReadWrite_SameData()
    {
      const string testValue = "ConfigName";
      _instance.ConfigName = testValue;

      Assert.AreEqual(_changedPropertyName,"ConfigName");
      Assert.AreEqual(testValue, _instance.ConfigName);
    }

    [Test]
    public void IsDeleted_ReadWrite_SameData()
    {
      const bool testValue = true;
      _instance.IsDeleted = testValue;

      Assert.AreEqual(_changedPropertyName,"IsDeleted");
      Assert.AreEqual(testValue, _instance.IsDeleted);
    }

    [Test]
    public void DataTime_ReadWrite_SameData()
    {
      DateTime? testValue = DateTime.Now;
      _instance.DataTime = testValue;

      Assert.AreEqual(_changedPropertyName,"DataTime");
      Assert.AreEqual(testValue, _instance.DataTime);
    }

    [Test]
    public void UpdateTime_ReadWrite_SameData()
    {
      DateTime? testValue = DateTime.Now;
      _instance.UpdateTime = testValue;

      Assert.AreEqual(_changedPropertyName,"UpdateTime");
      Assert.AreEqual(testValue, _instance.UpdateTime);
    }

    [Test]
    public void Exact_ReadWrite_SameData()
    {
      int? testValue = 10;
      _instance.Exact = testValue;

      Assert.AreEqual(_changedPropertyName,"Exact");
      Assert.AreEqual(testValue, _instance.Exact);
    }

    [Test]
    public void StartDataTime_ReadWrite_SameData()
    {
      DateTime? testValue = DateTime.Now;
      _instance.StartDataTime = testValue;

      Assert.AreEqual(_changedPropertyName,"StartDataTime");
      Assert.AreEqual(testValue, _instance.StartDataTime);
    }

    [Test]
    public void EndDataTime_ReadWrite_SameData()
    {
      DateTime? testValue = DateTime.Now;
      _instance.EndDataTime = testValue;

      Assert.AreEqual(_changedPropertyName,"EndDataTime");
      Assert.AreEqual(testValue, _instance.EndDataTime);
    }

    [Test]
    public void XmlData_ReadWrite_SameData()
    {
      // Create test data object
      XmlReader requestReader = GetTestData("RequestRamboResource.xml");
      XmlDocument testValue = new XmlDocument();
      testValue.Load(requestReader);
      _instance.XmlData = testValue;

      Assert.AreEqual(_changedPropertyName,"XmlData");
      Assert.AreEqual(testValue, _instance.XmlData);
    }

    [Test]
    public void XmlDataWithParameters_ReadWrite_SameData()
    {
      // Create test data object
      XmlReader requestReader = GetTestData("RequestRamboResource.xml");
      XmlDocument testValue = new XmlDocument();
      testValue.Load(requestReader);
      _instance.XmlData = testValue;

      Assert.AreEqual(_changedPropertyName,"XmlData");
      Assert.AreNotEqual(testValue, _instance.XmlDataWithParameters);
    }

    [Test]
    public void XmlDataStringIdented_ReadWriteEmptyData_SameData()
    {
      _instance.XmlDataStringIdented = String.Empty;

      Assert.AreEqual(_changedPropertyName, "XmlDataStringIdented");
      Assert.AreEqual(String.Empty, _instance.XmlDataStringIdented);
    }

    [Test]
    public void XmlDataStringIdented_ReadWrite_SameData()
    {
      // Create test data object
      XmlReader requestReader = GetTestData("RequestRamboResource.xml");
      XmlDocument testValue = new XmlDocument();
      testValue.Load(requestReader);
      _instance.XmlDataStringIdented = testValue.OuterXml;

      Assert.AreEqual(_changedPropertyName, "XmlDataStringIdented");
      Assert.AreNotEqual(testValue, _instance.XmlDataStringIdented);
    }

    [Test]
    public void ReadXml_ValidXml_NewInstanceWithData()
    {
      RamboResource inputObject = RamboResource.ReadXml(_reader);

      Assert.AreEqual("dhruvp", inputObject.UserName);
      Assert.AreEqual("riskviewerconfig", inputObject.ApplicationName);
      Assert.AreEqual("ConcordLayout", inputObject.ResourceType.ToString());
      Assert.AreEqual("all", inputObject.ResourceCategory);
      Assert.AreEqual("all", inputObject.AppletName);
      Assert.AreEqual("ln-ecdev", inputObject.Profile);
      Assert.AreEqual("all", inputObject.ResourceName);
      Assert.AreEqual(false, inputObject.IsDeleted);

      Assert.AreEqual(new DateTime(2010, 07, 22, 14, 36, 14), inputObject.DataTime);
      Assert.AreEqual(new DateTime(2010, 07, 22, 14, 36, 14), inputObject.UpdateTime);
      Assert.AreEqual(0, inputObject.Exact);
      Assert.AreEqual(new DateTime(2010, 07, 22, 14, 36, 14), inputObject.StartDataTime);
      Assert.AreEqual(new DateTime(2010, 07, 22, 14, 36, 14), inputObject.EndDataTime);

      Assert.AreEqual("dhruvp/riskviewerconfig/ln-ecdev/all/ConcordLayout/all/all", inputObject.ResourceId);

      Assert.Greater(inputObject.XmlData.OuterXml.Length, 0);
      Assert.IsNotNull(inputObject.XmlData.DocumentElement);
      Assert.IsNotNull(inputObject.XmlData.DocumentElement.SelectSingleNode("./Key"));
      Assert.IsTrue(inputObject.XmlData.OuterXml.Contains("<Rambo"));
      Assert.IsNull(inputObject.XmlData.DocumentElement.SelectSingleNode("./DataTime"));
      Assert.IsNull(inputObject.XmlData.DocumentElement.SelectSingleNode("./UpdateTime"));
      Assert.IsNull(inputObject.XmlData.DocumentElement.SelectSingleNode("./StartDataTime"));
      Assert.IsNull(inputObject.XmlData.DocumentElement.SelectSingleNode("./EndDataTime"));
      Assert.IsNull(inputObject.XmlData.DocumentElement.SelectSingleNode("./Exact"));
    }

    [Test]
    public void ReadXml_ValidXml_NewInstanceWithData_XPath()
    {
      var stream = GetSourceStream("ResponseRamboResource.xml");
      var doc = new XmlDocument();
      doc.Load(stream);
      var element = doc.SelectSingleNode("Snapshot");
      _instance = RamboResource.ReadXml(element);

      Assert.AreEqual("dhruvp", _instance.UserName);
      Assert.AreEqual("riskviewerconfig", _instance.ApplicationName);
      Assert.AreEqual("ConcordLayout", _instance.ResourceType.ToString());
      Assert.AreEqual("all", _instance.ResourceCategory);
      Assert.AreEqual("all", _instance.AppletName);
      Assert.AreEqual("ln-ecdev", _instance.Profile);
      Assert.AreEqual("all", _instance.ResourceName);
      Assert.AreEqual(false, _instance.IsDeleted);

      Assert.AreEqual(new DateTime(2010, 07, 22, 14, 36, 14), _instance.DataTime);
      Assert.AreEqual(new DateTime(2010, 07, 22, 14, 36, 14), _instance.UpdateTime);
      Assert.AreEqual(0, _instance.Exact);
      Assert.AreEqual(new DateTime(2010, 07, 22, 14, 36, 14), _instance.StartDataTime);
      Assert.AreEqual(new DateTime(2010, 07, 22, 14, 36, 14), _instance.EndDataTime);

      Assert.AreEqual("dhruvp/riskviewerconfig/ln-ecdev/all/ConcordLayout/all/all", _instance.ResourceId);

      Assert.Greater(_instance.XmlData.OuterXml.Length, 0);
      Assert.IsNotNull(_instance.XmlData.DocumentElement);
      Assert.IsNotNull(_instance.XmlData.DocumentElement.SelectSingleNode("//Key"));
      Assert.IsTrue(_instance.XmlData.OuterXml.Contains("<Rambo"));
      Assert.IsNull(_instance.XmlData.DocumentElement.SelectSingleNode("//DataTime"));
      Assert.IsNull(_instance.XmlData.DocumentElement.SelectSingleNode("//UpdateTime"));
      Assert.IsNull(_instance.XmlData.DocumentElement.SelectSingleNode("//StartDataTime"));
      Assert.IsNull(_instance.XmlData.DocumentElement.SelectSingleNode("//EndDataTime"));
      Assert.IsNull(_instance.XmlData.DocumentElement.SelectSingleNode("//Exact"));
    }
    
    [Test]
    public void WriteXml_TestInstance_MatchWithTestData()
    {
      // Create test data object
      _instance.ResourceType = "GridLayout";
      _instance.UserName = "khimji";
      _instance.AppletName = "QS.Xed";
      _instance.GridName = "xedGrid";
      _instance.GridLayout = "khimji";
      _instance.Exact = 0;

      DateTime timestamp = new DateTime(2007, 9, 4, 8, 44, 12);
      _instance.DataTime = timestamp;
      _instance.UpdateTime = timestamp;
      _instance.StartDataTime = timestamp;
      _instance.EndDataTime = timestamp;

      XmlReader reader = GetTestData("RequestRamboResource.xml");
      _instance.XmlData.Load(reader);

      // Serialize test object
      XmlDocument serializedDocument = new XmlDocument();
      MemoryStream xmlStream = new MemoryStream();
      XmlWriter xmlWriter = new XmlTextWriter(xmlStream, Encoding.UTF8);
      _instance.WriteXml(xmlWriter);

      xmlWriter.Flush();
      xmlStream.Seek(0, SeekOrigin.Begin);
      serializedDocument.Load(xmlStream);

      XmlReader requestReader = GetTestData("ResponseRamboResource.xml");
      XmlDocument deserializedDocument = new XmlDocument();
      deserializedDocument.Load(requestReader);

      // Remove key node because it doesn't need for serialized request
      // and this would cause a wrong fail result
      if (deserializedDocument.DocumentElement != null)
      {
        XmlNode keyNode = deserializedDocument.DocumentElement.SelectSingleNode("Key");
        if (keyNode != null)
        {
          deserializedDocument.DocumentElement.RemoveChild(keyNode);
        }
      }

      // Compare the two instances
      Assert.IsNotNull(serializedDocument.DocumentElement);
      Assert.IsNotNull(deserializedDocument.DocumentElement);
      //Assert.AreEqual(serializedDocument.DocumentElement.OuterXml, deserializedDocument.DocumentElement.OuterXml);
    }

    [Test]
    public void GenerateKey_NonNullParameters()
    {
      string key = RamboResource.GenerateKey("userName", "applicationName", "profileName", "appletName", "resourceType", "resourceCategory","resourceName");
      Assert.IsNotNull(key);
      Assert.AreEqual(key, "userName/applicationName/profileName/appletName/resourceType/resourceCategory/resourceName");
    }

    [Test]
    public void GenerateKey_NullParameters()
    {
      string key = RamboResource.GenerateKey(null, "applicationName", "profileName", "appletName", "resourceType", "resourceCategory", "resourceName");
      Assert.IsNotNull(key);
      Assert.AreEqual(key, "all/applicationName/profileName/appletName/resourceType/resourceCategory/resourceName");

      key = RamboResource.GenerateKey("userName", null, "profileName", "appletName", "resourceType", "resourceCategory", "resourceName");
      Assert.IsNotNull(key);
      Assert.AreEqual(key, "userName/all/profileName/appletName/resourceType/resourceCategory/resourceName");

      key = RamboResource.GenerateKey("userName", "applicationName", null, "appletName", "resourceType", "resourceCategory", "resourceName");
      Assert.IsNotNull(key);
      Assert.AreEqual(key, "userName/applicationName/all/appletName/resourceType/resourceCategory/resourceName");

      key = RamboResource.GenerateKey("userName", "applicationName", "profileName", null, "resourceType", "resourceCategory", "resourceName");
      Assert.IsNotNull(key);
      Assert.AreEqual(key, "userName/applicationName/profileName/all/resourceType/resourceCategory/resourceName");

      key = RamboResource.GenerateKey("userName", "applicationName", "profileName", "appletName", null, "resourceCategory", "resourceName");
      Assert.IsNotNull(key);
      Assert.AreEqual(key, "userName/applicationName/profileName/appletName/all/resourceCategory/resourceName");

      key = RamboResource.GenerateKey("userName", "applicationName", "profileName", "appletName", "resourceType", null, "resourceName");
      Assert.IsNotNull(key);
      Assert.AreEqual(key, "userName/applicationName/profileName/appletName/resourceType/all/resourceName");

      key = RamboResource.GenerateKey("userName", "applicationName", "profileName", "appletName", "resourceType", "resourceCategory", null);
      Assert.IsNotNull(key);
      Assert.AreEqual(key, "userName/applicationName/profileName/appletName/resourceType/resourceCategory/all");
     
    }
  }
}