﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using MorganStanley.MSDesktop.Rambo.API;
using NUnit.Framework;

namespace MorganStanley.MSDesktop.Rambo.Test
{
  [TestFixture]
  public class ResourceXmlValidatorTests : UnitTestBase
  {
    [Test]
    [TestCase("GridLayout")]
    [TestCase("GridGrouping")]
    [TestCase("CustomGrouping")]
    [TestCase("AppScreen")]
    [TestCase("BasketTradingConfig")]
    public void WhenComplyWithSchema_ShouldPass(string resourceType_)
    {
      var document = new XmlDocument();
      var stream = GetSourceStream(string.Format(@"ValidResourceXml.{0}.xml", resourceType_));
      document.Load(stream);
      Assert.DoesNotThrow(() => RamboResourceXmlValidator.ValidateResourceXml(resourceType_, document));
    }

    [Test]
    [TestCase("GridLayout")]
    [TestCase("GridGrouping")]
    [TestCase("CustomGrouping")]
    [TestCase("AppScreen")]
    [TestCase("BasketTradingConfig")]
    public void WhenNotComplyWithSchema_ShouldThrow(string resourceType_)
    {
      var document = new XmlDocument();
      var stream = GetSourceStream(string.Format(@"InvalidResourceXml.{0}.xml", resourceType_));
      document.Load(stream);
      Assert.Throws<ApplicationException>(() => RamboResourceXmlValidator.ValidateResourceXml(resourceType_, document));
    }
  }
}
