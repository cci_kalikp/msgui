﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Threading;
using System.Xml;
//-- boqwang: JetBrain not installed
//using JetBrains.Annotations;
using NUnit.Framework;

namespace MorganStanley.MSDesktop.Rambo.Test
{
  public class UnitTestBase
  {
    public delegate void InitializeDSManagerDelegate();

    [TestFixtureTearDown]
    public void UnitTestBaseTearDown()
    {
      Console.WriteLine();
    }

    internal static Stream GetSourceStream(string name_)
    {
      Stream sourceStream = typeof(UnitTestBase).Assembly.GetManifestResourceStream("MorganStanley.MSDesktop.Rambo.Test.TestData." + name_);
      return sourceStream;
    }

    internal static XmlReader GetTestData(string name_)
    {
      Stream testStream = GetSourceStream(name_);
      XmlReader sourceDoc = new XmlTextReader(testStream);

      XmlReader withoutHeader = RemoveHeader(sourceDoc);

      if (withoutHeader == null)
      {
        testStream.Seek(0, SeekOrigin.Begin);
        return new XmlTextReader(testStream);
      }
      return withoutHeader;
    }
    internal static XmlReader RemoveHeader(XmlReader source_)
    {
      XmlDocument sourceDoc = new XmlDocument();
      sourceDoc.Load(source_);

      XmlNode DSBodyNode = sourceDoc.SelectSingleNode("Snapshot");

      if (DSBodyNode != null)
      {
        XmlWriter writer = CreateWriter();
        writer.WriteStartDocument();

        DSBodyNode.FirstChild.WriteTo(writer);
        writer.Flush();
        return GetReaderForWriter(writer);
      }
      return null;
    }
    internal static XmlWriter CreateWriter()
    {
      Stream writerStream = new MemoryStream();
      XmlWriter writer = new XmlTextWriter(writerStream, Encoding.UTF8);

      return writer;
    }
    internal static XmlReader GetReaderForWriter(XmlWriter source_)
    {
      Stream sourceStream = ((XmlTextWriter)source_).BaseStream;
      sourceStream.Seek(0, SeekOrigin.Begin);

      return new XmlTextReader(sourceStream);
    }
    
    internal static void CheckXmlRoot(XmlDocument document_, string rootValue_)
    {
      Assert.IsNotNull(document_);
      Assert.IsNotNull(document_.DocumentElement);
      Assert.AreEqual(rootValue_, document_.DocumentElement.Name);
    }
    internal static XmlNode CheckXmlNodeExists(XmlDocument document_, XmlNamespaceManager namespaceManager_, string xPath_)
    {
      XmlNode node = namespaceManager_ == null ? 
        document_.SelectSingleNode(xPath_) : 
        document_.SelectSingleNode(xPath_, namespaceManager_);
      Assert.IsNotNull(node);
      return node;
    }
    internal static void CheckXmlValue(XmlDocument document_, XmlNamespaceManager namespaceManager_, string xPath_, string value_)
    {
      XmlNode node = CheckXmlNodeExists(document_, namespaceManager_, xPath_);
      Assert.AreEqual(value_, node.InnerText);
    }
    //-- boqwang: JetBrain not installed
    //protected static void Wait([NotNull] Func<bool> callback_)
    protected static void Wait(Func<bool> callback_)
    {
      for (int i = 1; i <= 20; ++i)
      {
        Thread.Sleep(1000);
        DoEvents();
        if (callback_()) return;
      }
      Assert.Fail("timeout");
    }

    protected static void DoEvents()
    {
      var frame = new DispatcherFrame();
      Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<DispatcherFrame>(ExitFrame), frame);
      Dispatcher.PushFrame(frame);
    }

    private static void ExitFrame(DispatcherFrame frame_)
    {
      frame_.Continue = false;
    }
  }
}
