﻿using System;
using System.Xml;

using NUnit.Framework;

using MorganStanley.IED.Concord.Runtime;
using MorganStanley.MSDesktop.Rambo.API;
using MorganStanley.IED.Concord.Configuration;
using System.Threading;

namespace MorganStanley.MSDesktop.Rambo.Test
{
  [TestFixture]
  public class ConcordInterfaceTests : UnitTestBase
  {
    #region Fields

    private string _settings;

    #endregion

    [SetUp]
    public void SetUpTestRun()
    {
      _settings = String.Format("app={0};region=ln-qa;env=qa;etscfg=QA", UnitTestConstants.UnitTestApplicationName);

      ConcordRuntime.Current.SetUserInfo(new ConcordUserInfo(null, UnitTestConstants.UnitTestUserName, null, null));
    }

    [Test]
    [Category("Initialize")]
    public void Initialize_StringSettings_SuccessfullyInitialized()
    {
      ConcordInterface concordInterface = new ConcordInterface();
      concordInterface.Initialize(_settings);

      Assert.AreEqual(concordInterface._excludeList.Count, 0);
    }

    [Test]
    [Category("Initialize")]
    public void Initialize_StringAndXmlNodeSettings_SuccessfullyInitialized()
    {
      XmlReader reader = GetTestData("ConcordConfig.xml");
      XmlDocument document = new XmlDocument();
      document.Load(reader);
      XmlNode configNode = document.DocumentElement;

      ConcordInterface concordInterface = new ConcordInterface();
      concordInterface.Initialize(_settings, configNode);

      Assert.IsNotNull(FileConfigurationStoreEx.NamespacesForXInclude);
      Assert.Greater(FileConfigurationStoreEx.NamespacesForXInclude.Count, 0);
      Assert.IsNotNull(FileConfigurationStoreEx.NamespacesForXInclude["grid"]);
      Assert.AreEqual(FileConfigurationStoreEx.NamespacesForXInclude["grid"], "http://xml.ms.com/ied/concord/viewmanagement/grid/0.4");

      Assert.Greater(concordInterface._excludeList.Count, 0);

      FileConfigurationStoreEx.NamespacesForXInclude.Clear();
    }

    [Test]
    [Category("Read configurations")]
    [Ignore("Causing failures")]
    public void ReadConfigurations_WrongSettings_LoadingFailed()
    {
      ConcordRuntime.Current.SetUserInfo(null);

      ConfigurationIdentityCollection collection = new ConfigurationIdentityCollection
                                                   {
                                                     new ConfigurationIdentity(null, String.Empty),
                                                   };


      ConcordInterface concordInterface = new ConcordInterface();
      concordInterface.Initialize(_settings);
      ConfigurationSection[] sections = concordInterface.ReadConfigurations(collection);

      Assert.IsNotNull(sections);
      Assert.Greater(sections.Length, 0);
      Assert.IsNull(sections[0].ReadOnlyNodes);
      Assert.IsNull(sections[0].ReadWriteNode);
    }

    //[Test]
    //[Category("Read configurations")]
    //public void ReadConfigurations_ConcordLayout_SuccessfullyLoadedFromFile()
    //{
    //  bool deleted = false;

    //  // Delete Rambo resource before try to load it
    //  RamboAPI.DeleteConcordLayoutResource(
    //    UnitTestConstants.UnitTestUserName,
    //    UnitTestConstants.UnitTestApplicationName,
    //    UnitTestConstants.UnitTestConcordInterfaceProfileName,
    //    delegate
    //    {
    //      deleted = true;
    //    });

    //  while (!deleted)
    //  {
    //    Thread.Sleep(1000);
    //  }

    //  ConfigurationIdentityCollection collection = new ConfigurationIdentityCollection
    //                                               {
    //                                                 new ConfigurationIdentity(UnitTestConstants.ConcordConfigurationLayouts, String.Empty),
    //                                               };


    //  ConcordInterface concordInterface = new ConcordInterface();
    //  concordInterface.Initialize(_settings);
    //  ConfigurationSection[] sections = concordInterface.ReadConfigurations(collection);

    //  Assert.IsNotNull(sections);
    //  Assert.Greater(sections.Length, 0);
    //}

    [Test]
    [Category("Read configurations")]
    [Ignore("Causing failures")]
    public void ReadConfigurations_ConcordLayout_SuccessfullyLoadedFromDS()
    {
      // Save resource before try to load it
      RamboResource resource = new RamboResource
                                 {
                                   UserName = UnitTestConstants.UnitTestUserName,
                                   ApplicationName = UnitTestConstants.UnitTestApplicationName,
                                   ResourceType=ConcordInterface.CONFIGURATOR,
                                   Profile = UnitTestConstants.UnitTestConcordInterfaceProfileName,
                                 };
      resource.XmlData.LoadXml(UnitTestConstants.UnitTestXmlString);
      bool saved = false;
      RamboAPI.SaveResource(resource, delegate { saved = true; });

      while (!saved)
      {
        Thread.Sleep(1000);
      }

      ConfigurationIdentityCollection collection = new ConfigurationIdentityCollection
                                                   {
                                                     new ConfigurationIdentity(UnitTestConstants.ConcordConfigurationLayouts, String.Empty),
                                                   };


      ConcordInterface concordInterface = new ConcordInterface();
      concordInterface.Initialize(_settings);
      ConfigurationSection[] sections = concordInterface.ReadConfigurations(collection);

      Assert.IsNotNull(sections);
      Assert.Greater(sections.Length, 0);
    }

    //[Test]
    //[Category("Read configurations")]
    //public void ReadConfigurations_NonConcordLayout_SuccessfullyLoadedFromFile()
    //{
    //  bool deleted = false;

    //  // Delete Rambo resource before try to load it
    //  RamboAPI.DeleteConfiguratorResource(
    //    UnitTestConstants.UnitTestUserName,
    //    UnitTestConstants.UnitTestApplicationName,
    //    UnitTestConstants.UnitTestConcordInterfaceProfileName,
    //    UnitTestConstants.OtherLayouts,
    //    delegate
    //    {
    //      deleted = true;
    //    });

    //  while (!deleted)
    //  {
    //    Thread.Sleep(1000);
    //  }

    //  ConfigurationIdentityCollection collection = new ConfigurationIdentityCollection
    //                                               {
    //                                                 new ConfigurationIdentity(UnitTestConstants.OtherLayouts, String.Empty)
    //                                               };


    //  ConcordInterface concordInterface = new ConcordInterface();
    //  concordInterface.Initialize(_settings);
    //  ConfigurationSection[] sections = concordInterface.ReadConfigurations(collection);

    //  Assert.IsNotNull(sections);
    //  Assert.Greater(sections.Length, 0);
    //}

    [Test]
    [Category("Read configurations")]
    [Ignore("Causing failures")]
    public void ReadConfigurations_NonConcordLayout_SuccessfullyLoadedFromDS()
    {
      // Save resource before try to load it
      RamboResource resource = new RamboResource
      {
        UserName = UnitTestConstants.UnitTestUserName,
        ApplicationName = UnitTestConstants.UnitTestApplicationName,
        ResourceType = ConcordInterface.CONFIGURATOR,
        Profile = UnitTestConstants.UnitTestConcordInterfaceProfileName,
        ConfigName = UnitTestConstants.OtherLayouts,
      };
      resource.XmlData.LoadXml(UnitTestConstants.UnitTestXmlString);
      bool saved = false;
      RamboAPI.SaveResource(resource, delegate { saved = true; });

      while (!saved)
      {
        Thread.Sleep(1000);
      }

      ConfigurationIdentityCollection collection = new ConfigurationIdentityCollection
                                                   {
                                                     new ConfigurationIdentity(UnitTestConstants.OtherLayouts, String.Empty)
                                                   };


      ConcordInterface concordInterface = new ConcordInterface();
      concordInterface.Initialize(_settings);
      ConfigurationSection[] sections = concordInterface.ReadConfigurations(collection);

      Assert.IsNotNull(sections);
      Assert.Greater(sections.Length, 0);
    }

    [Test]
    [Category("Write configuration")]
    [Ignore("Causing failures")]
    public void WriteConfiguration_ConcordLayout_Successfully()
    {
      ConcordInterface concordInterface = new ConcordInterface();
      concordInterface.Initialize(_settings);

      XmlDocument writeDocument = new XmlDocument();
      writeDocument.LoadXml(UnitTestConstants.UnitTestXmlString);
      concordInterface.WriteConfiguration(UnitTestConstants.ConcordConfigurationLayouts, String.Empty, writeDocument.DocumentElement);
    }

    [Test]
    [Category("Write configuration")]
    public void WriteConfiguration_NonConcordLayout_Successfully()
    {
      ConcordInterface concordInterface = new ConcordInterface();
      concordInterface.Initialize(_settings);

      XmlDocument writeDocument = new XmlDocument();
      writeDocument.LoadXml(UnitTestConstants.UnitTestXmlString);
      concordInterface.WriteConfiguration(UnitTestConstants.OtherLayouts, String.Empty, writeDocument.DocumentElement);
    }

    [Test]
    [Category("Write configuration")]
    public void WriteConfiguration_WrongSettings_Failed()
    {
      ConcordInterface concordInterface = new ConcordInterface();
      concordInterface.Initialize(_settings);

      XmlDocument writeDocument = new XmlDocument();
      writeDocument.LoadXml("<RamboResource></RamboResource>");
      concordInterface.WriteConfiguration(UnitTestConstants.OtherLayouts, String.Empty, writeDocument);
    }
  }
}