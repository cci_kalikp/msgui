﻿using System.Xml;

using MorganStanley.MSDesktop.Rambo.API;

using NUnit.Framework;

namespace MorganStanley.MSDesktop.Rambo.Test
{
  [TestFixture]
  public class CommonBaseTests : UnitTestBase
  {
    #region Fields

    private XmlReader _reader;

    #endregion

    [SetUp]
    public void Initialise()
    {
      _reader = GetTestData("ResponseRamboFieldValueList.xml");
    }

    [Test]
    public void Deserialize_NullReader_ReturnNull()
    {
      object testObject = CommonBase<object>.Deserialize(null);
      Assert.IsNull(testObject);
    }

    [Test]
    public void Deserialize_ValidReader_NewInstance()
    {
      RamboFieldValueList testInstance = CommonBase<RamboFieldValueList>.Deserialize(_reader);

      Assert.IsNotNull(testInstance);
    }

    [Test]
    public void EmptyClass_Constructor_NewInstanceWithMessage()
    {
      const string testMessage = "Test message";
      EmptyClass testInstance = new EmptyClass(testMessage);
      Assert.IsNotNull(testInstance);
      Assert.AreEqual(testMessage, testInstance.Message);
    }
  }
}