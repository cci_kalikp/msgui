﻿using System;
using System.Collections.Generic;
using System.Xml;

using MorganStanley.MSDesktop.Rambo.API;

using NUnit.Framework;

namespace MorganStanley.MSDesktop.Rambo.Test
{
  [TestFixture]
  public class RamboFieldValueListTests : UnitTestBase
  {
    #region Fields

    private XmlReader _reader;
    private RamboFieldValueList _instance;

    #endregion

    [SetUp]
    public void Initialise()
    {
      _reader = GetTestData("ResponseRamboFieldValueList.xml");
      _instance = new RamboFieldValueList();
    }

    private static void CheckDeserializedInstance(RamboFieldValueList newInstance_)
    {
        Assert.AreEqual(ColumnName.RamboUserid, newInstance_.ColumnName);

      Assert.IsNotNull(newInstance_.ValueList);
      Assert.AreEqual(893, newInstance_.ValueList.Count);

      string valueItem = newInstance_.ValueList[1];
      Assert.IsNotNull(valueItem);
      Assert.AreEqual("abderrah", valueItem);
    }

    [Test]
    public void Deserialize_ValidXml_NewInstanceWithData()
    {
      RamboFieldValueList newInstance = CommonBase<RamboFieldValueList>.Deserialize(_reader);

      CheckDeserializedInstance(newInstance);
    }

    [Test]
    public void Constructor_Parameterless_NewInstance()
    {
      Assert.IsNotNull(_instance.ValueList);
      Assert.AreEqual(0, _instance.ValueList.Count);
    }

    [Test]
    public void ColumnName_ReadWrite_SameData()
    {
        const ColumnName testValue = ColumnName.RamboUserid;

      _instance.ColumnName = testValue;
      Assert.AreEqual(testValue, _instance.ColumnName);
    }

    [Test]
    public void ValueList_ReadWrite_SameData()
    {
      List<string> testValue = new List<string> { "string" };

      _instance.ValueList = testValue;
      Assert.AreEqual(testValue, _instance.ValueList);
    }

    [Test]
    public void GetSchema_CallFunction_ThrowException()
    {
      Assert.Throws<NotImplementedException>(() => _instance.GetSchema());   
    }

    [Test]
    public void Serialize_CallFunction_MatchWithTestData()
    {
      XmlDocument deserlializedDocument = new XmlDocument();
      deserlializedDocument.Load(_reader);
      // Reinitialize the _reader variable
      Initialise();

      RamboFieldValueList newInstance = CommonBase<RamboFieldValueList>.Deserialize(_reader);

      XmlDocument serlializedDocument = newInstance.Serialize();

      Assert.IsNotNull(serlializedDocument.DocumentElement);
      Assert.IsNotNull(deserlializedDocument.DocumentElement);
      Assert.AreEqual(serlializedDocument.DocumentElement.OuterXml, deserlializedDocument.DocumentElement.OuterXml);
    }
  }
}