﻿using System.Xml;
using MorganStanley.MSDesktop.Rambo.DataServices;

namespace MorganStanley.MSDesktop.Rambo.Test.MockDataService
{
  public class RamboSharingDSMockUp : DataServiceBase 
  {
    public override DataEvent  GetSnapshot(IDataKey key_)
    {
 	     XmlDocument doc = new XmlDocument();
       doc.Load("./TestData/ResponseRamboSharing.xml");

       var dataObject = new XMLDataObject(key_, doc.DocumentElement);
       return new DataEvent(dataObject);
    }
  }
}
