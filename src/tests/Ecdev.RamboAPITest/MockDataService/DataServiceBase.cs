﻿using System;
using System.Collections;
using System.Collections.Generic;
using MorganStanley.MSDesktop.Rambo.DataServices;
using MorganStanley.MSDesktop.Rambo.DataServices.SoapCps;

namespace MorganStanley.MSDesktop.Rambo.Test.MockDataService
{
  public class DataServiceBase : IDataService
  {
    public virtual void refreshServerList()
    {
      throw new NotSupportedException();
    }

    public virtual ISubscription CreateSubscription(IDataKey key_)
    {
      throw new NotSupportedException();
    }

    public virtual ISubscription CreateSubscription(IDataKey key_, string userId_)
    {
      throw new NotSupportedException();
    }

    public virtual ISubscription CreateSubscription(IList keys_)
    {
      throw new NotSupportedException();
    }

    public ISubscription CreateBatchSubscription(IEnumerable<IDataKey> keys_)
    {
      throw new NotSupportedException();
    }

    public virtual ISubscription CreateSubscription(IList keys_, string userId_)
    {
      throw new NotSupportedException();
    }

    public virtual ISubscription CreateSubscription(IDictionary dict_)
    {
      throw new NotSupportedException();
    }

    public ISubscription CreateSubscription(IDictionary dict_, bool strictAttributeNames)
    {
      throw new NotSupportedException();
    }

    public virtual ISubscription CreateSubscription(IDictionary dict_, string userId_)
    {
      throw new NotSupportedException();
    }

    public ISubscription CreateSubscription(IDictionary dict_, string userId_, bool strictAttributeNames)
    {
      throw new NotSupportedException();
    }

    public virtual ISubscription GlobalSubscription()
    {
      throw new NotSupportedException();
    }

    public virtual ISubscription GlobalSubscription(string userId_)
    {
      throw new NotSupportedException();
    }

    public virtual IDataKey CreateDataKey(object key_)
    {
      throw new NotSupportedException();
    }

    public virtual void Publish(IDataObject obj_)
    {
      throw new NotSupportedException();
    }

    public virtual IDataKey[] Publish(IList dataObjects_)
    {
      throw new NotSupportedException();
    }

    public IEnumerable<IDataKey> PublishBatch(IEnumerable<IDataObject> dataObjects_)
    {
      throw new NotSupportedException();
    }

    public virtual IAsyncResult BeginPublish(IDataObject obj_, AsyncCallback acb_, object state_)
    {
      throw new NotSupportedException();
    }

    public virtual IAsyncResult BeginPublish(IList dataObjects_, AsyncCallback acb_, object state_)
    {
      throw new NotSupportedException();
    }

    public virtual IDataKey[] EndPublish(IAsyncResult result_)
    {
      throw new NotSupportedException();
    }

    public virtual DataEvent GetSnapshot(IDataKey key_)
    {
      throw new NotSupportedException();
    }

    public virtual IDictionary GetSnapshot(ISubscription subscription_)
    {
      throw new NotSupportedException();
    }

    public IDictionary<IDataKey, DataEvent> GetSnapshots(ISubscription subscription_)
    {
      throw new NotSupportedException();
    }

    public virtual IAsyncResult BeginSnapshot(ISubscription subscription_, AsyncCallback callback_, object state_)
    {
      throw new NotSupportedException();
    }

    public virtual IDictionary EndSnapshot(IAsyncResult result)
    {
      throw new NotSupportedException();
    }

    public virtual DataEvent GetSnapshot(IDataKey key_, DateTime version_)
    {
      throw new NotSupportedException();
    }

    public virtual DateTime[] GetVersions(IDataKey key_)
    {
      throw new NotSupportedException();
    }

    public virtual IDataKey[] GetDataKeys()
    {
      throw new NotSupportedException();
    }

    public virtual IDataKey[] GetDataKeys(DataServiceDate startDataTime_, DataServiceDate endDataTime_)
    {
      throw new NotSupportedException();
    }

    public virtual IDataKey[] GetDataKeys(IDictionary attributes_)
    {
      throw new NotSupportedException();
    }

    public virtual IDataKey[] GetDataKeys(IDictionary attributes_, DataServiceDate startDataTime_, DataServiceDate endDataTime_)
    {
      throw new NotSupportedException();
    }

    public virtual IDataKey[] GetDataKeys(IDataKey key_)
    {
      throw new NotSupportedException();
    }

    public virtual IDataKey[] GetDataKeys(IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_)
    {
      throw new NotSupportedException();
    }

    public virtual IAsyncResult BeginGetDataKeys(AsyncCallback callback_, object state_)
    {
      throw new NotSupportedException();
    }

    public virtual IAsyncResult BeginGetDataKeys(DataServiceDate startDataTime_, DataServiceDate endDataTime_, AsyncCallback callback_, object state_)
    {
      throw new NotSupportedException();
    }

    public virtual IAsyncResult BeginGetDataKeys(IDictionary attributes_, AsyncCallback callback_, object state_)
    {
      throw new NotSupportedException();
    }

    public virtual IAsyncResult BeginGetDataKeys(IDictionary attributes_, DataServiceDate startDataTime_, DataServiceDate endDataTime_, AsyncCallback callback_, object state_)
    {
      throw new NotSupportedException();
    }

    public virtual IAsyncResult BeginGetDataKeys(IDataKey key_, AsyncCallback callback_, object state_)
    {
      throw new NotSupportedException();
    }

    public virtual IAsyncResult BeginGetDataKeys(IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_, AsyncCallback callback_, object state_)
    {
      throw new NotSupportedException();
    }

    public virtual IDataKey[] EndGetDataKeys(IAsyncResult result)
    {
      throw new NotSupportedException();
    }

    public virtual VersionDataKeyCollection GetVersionsList(IDataKey key_, bool sort_)
    {
      throw new NotSupportedException();
    }

    public virtual VersionDataKeyCollection GetVersionsList(IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_)
    {
      throw new NotSupportedException();
    }

    public virtual IAsyncResult BeginGetVersionsList(IDataKey key_, bool sort_, AsyncCallback callback_, object state_)
    {
      throw new NotSupportedException();
    }

    public virtual IAsyncResult BeginGetVersionsList(IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_, AsyncCallback callback_, object state_)
    {
      throw new NotSupportedException();
    }

    public virtual VersionDataKeyCollection EndGetVersionsList(IAsyncResult result)
    {
      throw new NotSupportedException();
    }

    public virtual DataEvent GetVersion(IVersionDataKey key_)
    {
      throw new NotSupportedException();
    }

    public virtual IAsyncResult BeginGetVersion(IVersionDataKey key_, AsyncCallback callback_, object state_)
    {
      throw new NotSupportedException();
    }

    public virtual DataEvent EndGetVersion(IAsyncResult result)
    {
      throw new NotSupportedException();
    }

    public virtual IVersionDataKey GetVersionDataKey(IAliasDataKey aliasDataKey_)
    {
      throw new NotSupportedException();
    }

    public virtual void TurnoverAlias(IAliasDataKey aliasDataKey_, IVersionDataKey newVersionKey_)
    {
      throw new NotSupportedException();
    }

    public virtual IMetaDataObject GetMetaData()
    {
      throw new NotSupportedException();
    }

    public virtual IAsyncResult BeginMetaData(AsyncCallback callback_, object state_)
    {
      throw new NotSupportedException();
    }

    public virtual IMetaDataObject EndMetaData(IAsyncResult result)
    {
      throw new NotSupportedException();
    }

    public virtual string Id
    {
      get { throw new NotSupportedException(); }
    }

    public virtual bool ServerSubFiltering
    {
      get { throw new NotSupportedException(); }
      set { throw new NotSupportedException(); }
    }

    public virtual ServerList Servers
    {
      get { throw new NotSupportedException(); }
    }
  }
}
