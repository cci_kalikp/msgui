﻿using System.Xml;
using MorganStanley.MSDesktop.Rambo.DataServices;

namespace MorganStanley.MSDesktop.Rambo.Test.MockDataService
{
  public class RamboResourceDSMockUp : DataServiceBase
  {
    public static bool IsPublished { get; set; }
    public static IDataObject CurrentObject { get; set; }
    public string LoadResourcePath { private get; set; }

    public override DataEvent GetSnapshot(IDataKey key_)
    {
      XmlDocument doc = new XmlDocument();
      doc.Load(string.IsNullOrEmpty(LoadResourcePath) ? "./TestData/ResponseRamboResource.xml" : LoadResourcePath);

      var dataObject = new XMLDataObject(key_, doc.DocumentElement);
      return new DataEvent(dataObject);
    }

    public override void Publish(IDataObject obj_)
    {
      CurrentObject = obj_;
      IsPublished = true;
    }
  }
}