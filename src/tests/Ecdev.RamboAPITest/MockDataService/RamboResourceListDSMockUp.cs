﻿using System.Xml;
using MorganStanley.MSDesktop.Rambo.DataServices;

namespace MorganStanley.MSDesktop.Rambo.Test.MockDataService
{
  class RamboResourceListDSMockUp : DataServiceBase
  {
    public override DataEvent GetSnapshot(IDataKey key_)
    {
      XmlDocument doc = new XmlDocument();
      doc.Load("./TestData/ResponseRamboResourceList.xml");

      var dataObject = new XMLDataObject(key_, doc.DocumentElement);
      return new DataEvent(dataObject);
    }

    public override void Publish(IDataObject obj_)
    {
      base.Publish(obj_);
    }
  }
}
