﻿using System;
using System.IO;
using MorganStanley.MSDesktop.Rambo.API;
using MorganStanley.MSDesktop.Rambo.DataServices;

namespace MorganStanley.MSDesktop.Rambo.Test.MockDataService
{
  public class RamboResourceManagerMockUp : RamboResourceManager
  {
    protected IDataService _dataService;
    protected IDataService _listDataService;

    public RamboResourceManagerMockUp(IDataService dataService_, IDataService listDataService_)
    {
      _dataService = dataService_;
      _listDataService = listDataService_;
    }

    protected override IDataService DataService
    {
      get { return _dataService ?? new RamboResourceDSMockUp(); }
    }

    protected override IDataService ListDataService
    {
      get { return _listDataService ?? new RamboResourceListDSMockUp(); }
    }
    
    protected override object DeserializeObject(RamboResource resource_)
    {
      return resource_;
    }

    protected override string GetMapKey(object object_)
    {
      var ramboResource = object_ as RamboResource;
      var key = (ramboResource == null)
                  ? null
                  : String.Format("{0},{1}",
                                  ramboResource.UserName,
                                  ramboResource.ResourceName);

      return key;
    }

    protected override object GetObject(RamboResource resource_)
    {
      throw new NotImplementedException();
    }

    protected override void SerializeObject(StringWriter reader_, object o)
    {
      throw new NotImplementedException();
    }
     
    
  }
}
