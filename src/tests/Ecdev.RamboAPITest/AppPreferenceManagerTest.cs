﻿using System;
using System.Reflection;
using System.Xml;
using MorganStanley.MSDesktop.Rambo.API.AppPreference;
using MorganStanley.MSDesktop.Rambo.Test.MockDataService;
using NUnit.Framework;

namespace MorganStanley.MSDesktop.Rambo.Test
{
  public class AppPreferenceManagerTest : UnitTestBase
  {
    private const string APP_SCREEN_FILE_PATH = "./TestData/ResponseRamboAppPrefResource.xml";
    private IAppPreferenceManager _appPreferenceManager;
    private RamboResourceDSMockUp _mockDS;
    private const string APP_NAME = "Ecdev.RiskViewer";

    [SetUp]
    public void TestSetUp()
    {
      _appPreferenceManager = new AppPreferenceManager();

      // TODO: not good here, we need to user UnityContainer register/resolve to re-direct data service to another
      FieldInfo field = typeof(AppPreferenceManager).GetField("_prefsDS", BindingFlags.NonPublic | BindingFlags.Instance);
      _mockDS = new RamboResourceDSMockUp {LoadResourcePath = APP_SCREEN_FILE_PATH};
      if (field != null) field.SetValue(_appPreferenceManager, _mockDS);
    }

    [Test]
    public void TestGetAppPreference()
    {
      XmlElement xml = _appPreferenceManager.GetPreference(Environment.UserName, APP_NAME);
      XmlNodeList screenNameNode = xml.SelectNodes("Screen/ScreenName");
      Assert.IsNotNull(screenNameNode);
      Assert.AreEqual(1, screenNameNode.Count);
      Assert.AreEqual("XinTest", screenNameNode[0].InnerText);
    }

    [Test]
    public void TestSaveAppPreference()
    {
      XmlElement xml = _appPreferenceManager.GetPreference(Environment.UserName, APP_NAME);
      Assert.IsNull(xml.SelectSingleNode("RamboUserid"));
      Assert.IsNull(xml.SelectSingleNode("RamboApplication"));
      Assert.IsNull(xml.SelectSingleNode("RamboProfile"));
      Assert.IsNull(xml.SelectSingleNode("RamboResType"));
      Assert.IsNull(xml.SelectSingleNode("RamboResCategory"));
      Assert.IsNull(xml.SelectSingleNode("RamboResName"));
      Assert.IsNull(xml.SelectSingleNode("RamboIsDeleted"));

      _appPreferenceManager.SavePreference(Environment.UserName, APP_NAME, xml);
      XmlNode currentObject = RamboResourceDSMockUp.CurrentObject.Value as XmlNode;
      Assert.NotNull(currentObject);
      // Assert all rambo related node is added
      Assert.NotNull(currentObject.SelectSingleNode("RamboUserid"));
      Assert.NotNull(currentObject.SelectSingleNode("RamboApplication"));
      Assert.NotNull(currentObject.SelectSingleNode("RamboProfile"));
      Assert.NotNull(currentObject.SelectSingleNode("RamboResType"));
      Assert.NotNull(currentObject.SelectSingleNode("RamboResCategory"));
      Assert.NotNull(currentObject.SelectSingleNode("RamboResName"));
      Assert.NotNull(currentObject.SelectSingleNode("RamboIsDeleted"));
    }
  }
}
