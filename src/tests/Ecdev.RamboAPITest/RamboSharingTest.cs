﻿using System.Diagnostics;
using System.Xml;
using System.Xml.Serialization;
using MorganStanley.MSDesktop.Rambo.API.DataObjects;
using NUnit.Framework;

namespace MorganStanley.MSDesktop.Rambo.Test
{
  [TestFixture]
  public class RamboSharingTest : UnitTestBase
  {
    private RamboSharing _ramboSharing; 

    [SetUp]
    public void TestSetUp()
    {
      
      _ramboSharing = new RamboSharing
                        {
                          GroupName = "rvguidev",
                          GroupType = "group",
                          RamboKey =
                            "yuxin/Ecdev.RiskViewer/all/Ecdev.RiskViewer/GridGrouping/RVGridGrouping_positionLite/yuxin"
                        };
    }

    [Test]
    public void WriteXmlTest()
    {
     XmlDocument doc = new XmlDocument();
      XmlElement ramboGroups = doc.CreateElement("RamboGroupgs");
      _ramboSharing.WriteXml(ramboGroups);

      Assert.AreEqual(1, ramboGroups.ChildNodes.Count);

      XmlNodeList ramboGroupInNode = ramboGroups.ChildNodes[0].ChildNodes;
      Assert.AreEqual(3, ramboGroupInNode.Count);
      Assert.AreEqual("RamboKey", ramboGroupInNode[0].Name);
      Assert.AreEqual(_ramboSharing.RamboKey, ramboGroupInNode[0].InnerText);
      Assert.AreEqual("GroupName", ramboGroupInNode[1].Name);
      Assert.AreEqual(_ramboSharing.GroupName, ramboGroupInNode[1].InnerText);
      Assert.AreEqual("GroupType", ramboGroupInNode[2].Name);
      Assert.AreEqual(_ramboSharing.GroupType, ramboGroupInNode[2].InnerText);
    }

    [Test]
    public void DeserializeRamboSharing()
    {
      XmlSerializer serializer = new XmlSerializer(typeof(RamboSharing));
      XmlDocument document = new XmlDocument();
      document.Load("./TestData/ResponseRamboSharing.xml");
      XmlNodeList nodes = document.SelectNodes("//RVRamboSharing/RamboGroups/RamboGroup");

      Assert.NotNull(nodes);
      Assert.AreEqual(1, nodes.Count);
      XmlNode groupNode = nodes[0];

      XmlNodeReader groupNodeReader = new XmlNodeReader(groupNode);
      RamboSharing ramboSharing = serializer.Deserialize(groupNodeReader) as RamboSharing;
      Assert.NotNull(ramboSharing);
      Assert.AreEqual("yuxin", ramboSharing.GroupName);
      Assert.AreEqual("user", ramboSharing.GroupType);
      Assert.AreEqual("yuxin/Ecdev.RiskViewer/all/Ecdev.RiskViewer/GridGrouping/RVGridGrouping_positionLite/yuxin", ramboSharing.RamboKey);
    }
  }
}
