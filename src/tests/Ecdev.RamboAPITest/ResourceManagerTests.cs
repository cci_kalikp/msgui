﻿using System.Xml;
using MorganStanley.MSDesktop.Rambo.Test.MockDataService;
using NUnit.Framework;
using MorganStanley.MSDesktop.Rambo.API;

namespace MorganStanley.MSDesktop.Rambo.Test
{
  public class ResourceManagerTests : UnitTestBase
  {
    
    [SetUp]
    public void TestSetUp()
    {
      RamboResourceDSMockUp.IsPublished = false;
      RamboResourceDSMockUp.CurrentObject = null;
    }

    [Test]
    public void DeleteResourceTest()
    {
      //Arrange
       var ramboResourceManagerMockUp = CreateSUT(string.Empty);
      
      //Act
      ramboResourceManagerMockUp.DeleteResource(null, false, "khimji", "CBMarketMonitor", "ln", "all", "GridLayout", "CBTradeGrRamboId", "all", "all");
      Wait(() => RamboResourceDSMockUp.IsPublished); 

      //Assert
      var xmlDataObject = RamboResourceDSMockUp.CurrentObject.Value as XmlNode;
      Assert.NotNull(xmlDataObject);

      XmlNode  isDeletedNode =  xmlDataObject.SelectSingleNode("//RamboIsDeleted");
      Assert.NotNull(isDeletedNode);
      Assert.AreEqual("1", isDeletedNode.InnerText);
    }

    [Test]
    public void LoadResourceReturnsDeletedResource_WhenLoadDeletedIsSet()
    {
       //Arrange
       var resource = new RamboResource();
       const string loadResourcePath = "./TestData/ResponseRamboResource/ResponseDeletedRamboResource.xml";
       var ramboResourceManagerMockup = CreateSUT(loadResourcePath);
       
       //Act
      resource = ramboResourceManagerMockup.LoadResource(resource, false, "kanchb", "Ecdev.RiskViewer", "all", "all", "BasketTradingConfig",
                                               "KanTest1", "all", "all", true) as RamboResource;
       
      //Assert
      Assert.NotNull(resource);
      Assert.AreEqual(resource.IsDeleted, true); 
    }

    [Test]
    public void LoadResourceReturnsNull_WhenLoadDeletedIsSetFalse()
    {
      //Arrange
      var resource = new RamboResource();
      const string loadResourcePath = "./TestData/ResponseRamboResource/ResponseDeletedRamboResource.xml";
      var ramboResourceManagerMockup = CreateSUT(loadResourcePath); 
      
      //Act
      resource = ramboResourceManagerMockup.LoadResource(resource, false, "kanchb", "Ecdev.RiskViewer", "all", "all", "BasketTradingConfig",
                                               "KanTest1", "all", "all", false) as RamboResource;

      //Assert
      Assert.IsNull(resource);
    }

    [Test]
    public void LoadResourceReturnsNotDeletedResource_WhenLoadDeletedIsSetFalse()
    {
      //Arrange
      var resource = new RamboResource();
      const string loadResourcePath = "./TestData/ResponseRamboResource/ResponseBasketTradingRamboResource.xml";
      var ramboResourceManagerMockup = CreateSUT(loadResourcePath);

      //Act
      resource = ramboResourceManagerMockup.LoadResource(resource, false, "kanchb", "Ecdev.RiskViewer", "all", "all", "BasketTradingConfig",
                                               "KanTest1", "all", "all", false) as RamboResource;

      //Assert
      Assert.NotNull(resource);
      Assert.AreEqual(resource.IsDeleted, false); 
    }

    private RamboResourceManagerMockUp CreateSUT(string loadResourcePath_)
    {
      var dataService = new RamboResourceDSMockUp {LoadResourcePath = loadResourcePath_};
      var listDataService = new RamboResourceListDSMockUp();

      return new RamboResourceManagerMockUp(dataService, listDataService);
    }
  }
}
