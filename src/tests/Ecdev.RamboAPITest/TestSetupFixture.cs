﻿using System;
using MorganStanley.MSDesktop.Rambo.DataServices;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.Runtime;
using NUnit.Framework;

namespace MorganStanley.MSDesktop.Rambo.Test
{
  [SetUpFixture]
  public class TestSetupFixture
  {
    [SetUp]
    public void SuperSetUp()
    {
      AssemblyResolver.Load();
      AssemblyResolver.Run(new Action(InitializeDSManager));
    }

    public void InitializeDSManager()
    {
      if (!DataServiceManager.IsInitialised())
      {
        MSNetLoopPoolImpl loop = MSNetLoopPoolImpl.Instance();
        DataServiceManager.Init(loop, "LN", "QA");
      }
    }
  }
}
