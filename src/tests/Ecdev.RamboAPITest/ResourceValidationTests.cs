﻿using System;
using MorganStanley.MSDesktop.Rambo.API;
using NUnit.Framework;

namespace MorganStanley.MSDesktop.Rambo.Test
{
  [TestFixture]
  public class ResourceValidationTests : UnitTestBase
  {

    private RamboResource GetValidResouce()
    {
      return new RamboResource() { ResourceType = "something-not-null-or-all" ,
                                   ApplicationName = "test-application", 
                                   ResourceCategory = "test-category" };
    }

    [Test]
    public void ValidateResource_NullParameter()
    {
      Assert.Throws<ApplicationException>(() => RamboAPI.ValidateResource(null));  
    }

    [Test]
    public void ValidateResource_NullResourceType()
    {
      RamboResource resource = GetValidResouce();
      resource.ResourceType = null;
      Assert.Throws<ApplicationException>(() => RamboAPI.ValidateResource(resource));  
    }

    [Test]
    public void ValidateResource_DefaultValueResourceType()
    {
      RamboResource resource = GetValidResouce();
      resource.ResourceType = Constants.EmptyKeyString;
      Assert.Throws<ApplicationException>(() => RamboAPI.ValidateResource(resource));  
    }

    [Test]
    public void ValidateResource_ValidResource()
    {
      RamboResource resource = GetValidResouce();
      Assert.DoesNotThrow(() => RamboAPI.ValidateResource(resource));  
    }

    [Test]
    public void ValidateResource_InvalidAppNameInvalidResCat()
    {
      RamboResource resource = GetValidResouce();
      resource.ApplicationName = null;
      resource.ResourceCategory = null;
      Assert.Throws<ApplicationException>(() => RamboAPI.ValidateResource(resource));  
    }

    [Test]
    public void ValidateResource_InvalidAppNameValidResCat()
    {
      RamboResource resource = GetValidResouce();
      resource.ApplicationName = null;
      //If resource category is not null and application name is null then resource is valid.
      Assert.DoesNotThrow(() => RamboAPI.ValidateResource(resource));   
    }

    [Test]
    public void ValidateResource_ValidAppNameInvalidResCat()
    {
      RamboResource resource = GetValidResouce();
      resource.ResourceCategory = null;
      //If resource category is category is null and application name is not null then resource is valid.
      Assert.DoesNotThrow(() => RamboAPI.ValidateResource(resource));   
    }
  }
}
