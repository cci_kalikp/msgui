﻿using System.Collections.Generic;
using System.Reflection;
using MorganStanley.MSDesktop.Rambo.API;
using MorganStanley.MSDesktop.Rambo.Test.MockDataService;
using NUnit.Framework;
using MorganStanley.MSDesktop.Rambo.DataServices;

namespace MorganStanley.MSDesktop.Rambo.Test
{
  public class RamboEntitlmentTest : UnitTestBase
  {
    private RamboResourceEntitlement _ramboResourceEntitlement;

    [SetUp]
    public void TestSetUp()
    {
      _ramboResourceEntitlement = new RamboResourceEntitlement();

      // TODO: not good here, we need to user UnityContainer register/resolve to re-direct data service to another
      FieldInfo field = typeof (RamboResourceEntitlement).GetField("_ds", BindingFlags.NonPublic | BindingFlags.Instance);
      field.SetValue(_ramboResourceEntitlement, new RamboSharingDSMockUp());
    }

    [Test]
    public void TestIfUserInGroup()
    {
      IDictionary<string, string> groups = new Dictionary<string, string> {{"rvguidev", "group"}};
      bool isInGroup = RamboResourceEntitlement.IfUserInGroup(groups, "yuxin");
      Assert.IsTrue(isInGroup);

      IDictionary<string, string> groups1 = new Dictionary<string, string> {{"pasdev", "group"}};
      bool isInGroup1 = RamboResourceEntitlement.IfUserInGroup(groups1, "yuxin");
      Assert.IsFalse(isInGroup1);

      IDictionary<string, string> groups2 = new Dictionary<string, string> {{"pasdev", "group"}, {"yuxin", "user"}};
      bool isInGroup2 = RamboResourceEntitlement.IfUserInGroup(groups2, "yuxin");
      Assert.IsTrue(isInGroup2);
    }

    [Test]
    public void TestGetPermission()
    {
      IDictionary<string, string> dicGroup = _ramboResourceEntitlement.GetPermission(
        "yuxin/Ecdev.RiskViewer/all/Ecdev.RiskViewer/GridGrouping/RVGridGrouping_positionLite/yuxin");

      Assert.AreEqual(1, dicGroup.Count);
      Assert.IsTrue(dicGroup.ContainsKey("yuxin"));
      Assert.AreEqual("user", dicGroup["yuxin"]);
    }

    [Test]
    public void TestHasPermission()
    {
      bool hasPermission = _ramboResourceEntitlement.HasPermission(
        "yuxin/Ecdev.RiskViewer/all/Ecdev.RiskViewer/GridGrouping/RVGridGrouping_positionLite/yuxin", "yuxin");

      Assert.IsTrue(hasPermission);
    }

    [Test]
    [TestCase("gautama", DSEnvironment.DSEnv.Prod)]
    [TestCase("rvguidev", DSEnvironment.DSEnv.QA)]
    public void WhenUserIsASGMemberOrInQA_CanUpdateResourcesReturnsTrue(string asgMember_, DSEnvironment.DSEnv currentEnv_)
    {
      //Act
      bool result = RamboResourceEntitlement.CanUpdateResource(asgMember_,currentEnv_);

      //Assert
      Assert.IsTrue(result);
    }

    [Test]
    [TestCase("rvguidev", DSEnvironment.DSEnv.Prod)]
    [TestCase("barrymay", DSEnvironment.DSEnv.Prod)]

    public void WhenUserIsNonASGMember_CanUpdateResourcesReturnsFalse(string nonMember_, DSEnvironment.DSEnv currentEnv_)
    { 
      //Act
      bool result = RamboResourceEntitlement.CanUpdateResource(nonMember_, currentEnv_);

      //Assert
      Assert.IsFalse(result);
    }
  }
}