﻿using MorganStanley.MSDesktop.Rambo.API;
using NUnit.Framework;

namespace MorganStanley.MSDesktop.Rambo.Test
{
  [TestFixture]
  public class DirectoryHandlerTests : UnitTestBase
  {
     
    [Test]
    [TestCase("gautama")]
    public void WhenUserIsASGMember_IsInASGMailGroupShouldReturnTrue(string asgMember_)
    {
      //Act
      bool result = DirectoryHandler.IsInASGMailGroup(asgMember_);

      //Assert
      Assert.IsTrue(result);
    }

    [Test] 
    [TestCase("rvguidev")]
    [TestCase("barrymay")]
    public void WhenUserIsNotASGMember_IsInASGMailGroupShouldReturnFalse(string nonMember_)
    { 
      //Act
      bool result = DirectoryHandler.IsInASGMailGroup(nonMember_);

      //Assert
      Assert.IsFalse(result);
    }
   
  }
}