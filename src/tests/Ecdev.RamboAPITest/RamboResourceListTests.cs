﻿using System;
using System.Collections.ObjectModel;
using System.Xml;

using MorganStanley.MSDesktop.Rambo.API;

using NUnit.Framework;

namespace MorganStanley.MSDesktop.Rambo.Test
{
  [TestFixture]
  public class RamboResourceListTests : UnitTestBase
  {
    #region Fields

    private XmlReader _reader;
    private RamboResourceList _instance;

    #endregion

    [SetUp]
    public void Initialise()
    {
      _reader = GetTestData("ResponseRamboResourceList.xml");
      _instance = new RamboResourceList();
    }

    [Test]
    public void Deserialize_ValidXml_NewInstanceWithData_XPath()
    {
      var stream = GetSourceStream("ResponseRamboResourceList.xml");
      var doc = new XmlDocument();
      doc.Load(stream);
      var element = (XmlElement) doc.SelectSingleNode("Snapshot/RVRamboResourceList");
      _instance = new RamboResourceList();
      _instance.ReadXml(element);

      Assert.AreEqual(3, _instance.Items.Count);

      RamboResource resourceItem = _instance.Items[0];
      Assert.IsNotNull(resourceItem);

      Assert.AreEqual("khimji/all/all/CBMarketMonitor/GridLayout/CBTradeGrRamboId/all", resourceItem.ResourceId);
      Assert.AreEqual("khimji", resourceItem.UserName);
      Assert.AreEqual(Constants.EmptyKeyString, resourceItem.ApplicationName);
      Assert.AreEqual("GridLayout", resourceItem.ResourceType);
      Assert.AreEqual("CBMarketMonitor", resourceItem.AppletName);
      Assert.AreEqual(Constants.EmptyKeyString, resourceItem.Profile);
      Assert.AreEqual(Constants.EmptyKeyString, resourceItem.ResourceName);
      Assert.AreEqual(false, resourceItem.IsDeleted);

      Assert.IsNotNull(resourceItem.DataTime);
      Assert.IsNotNull(resourceItem.UpdateTime);
      Assert.IsNotNull(resourceItem.Exact);
      Assert.IsNotNull(resourceItem.StartDataTime);
      Assert.IsNotNull(resourceItem.EndDataTime);

      Assert.IsNotNull(resourceItem.XmlData);
    }

    [Test]
    public void Deserialize_ValidXml_NewInstanceWithData()
    {
      RamboResourceList inputObject = CommonBase<RamboResourceList>.Deserialize(_reader);

      Assert.AreEqual(3, inputObject.Items.Count);

      RamboResource resourceItem = inputObject.Items[0];
      Assert.IsNotNull(resourceItem);

      Assert.AreEqual("khimji/all/all/CBMarketMonitor/GridLayout/CBTradeGrRamboId/all", resourceItem.ResourceId);
      Assert.AreEqual("khimji", resourceItem.UserName);
      Assert.AreEqual(Constants.EmptyKeyString, resourceItem.ApplicationName);
      Assert.AreEqual("GridLayout", resourceItem.ResourceType);
      Assert.AreEqual("CBMarketMonitor", resourceItem.AppletName);
      Assert.AreEqual(Constants.EmptyKeyString, resourceItem.Profile);
      Assert.AreEqual(Constants.EmptyKeyString, resourceItem.ResourceName);
      Assert.AreEqual(false, resourceItem.IsDeleted);

      Assert.IsNotNull(resourceItem.DataTime);
      Assert.IsNotNull(resourceItem.UpdateTime);
      Assert.IsNotNull(resourceItem.Exact);
      Assert.IsNotNull(resourceItem.StartDataTime);
      Assert.IsNotNull(resourceItem.EndDataTime);

      Assert.IsNotNull(resourceItem.XmlData);
    }

    [Test]
    public void Constructor_Parameterless_NewInstance()
    {
      Assert.IsNotNull(_instance.Items);
      Assert.AreEqual(0, _instance.Items.Count);
    }

    [Test]
    public void ValueList_ReadWrite_SameData()
    {
      ObservableCollection<RamboResource> testValue = new ObservableCollection<RamboResource>
                                                        {
                                                          new RamboResource()
                                                        };

      _instance.Items = testValue;
      Assert.AreEqual(testValue, _instance.Items);
    }

    [Test]
    public void GetSchema_CallFunction_ThrowException()
    {
      Assert.Throws<NotImplementedException>(() => _instance.GetSchema());
    }

    [Test]
    public void Serialize_CallFunction_MatchWithTestData()
    {
      XmlDocument deserializedDocument = new XmlDocument();
      deserializedDocument.Load(_reader);
      // Reinitialize the _reader variable
      Initialise();

      RamboResourceList newInstance = CommonBase<RamboResourceList>.Deserialize(_reader);

      XmlDocument serializedDocument = newInstance.Serialize();

      Assert.IsNotNull(serializedDocument.DocumentElement);
      Assert.IsNotNull(deserializedDocument.DocumentElement);
      //Assert.AreEqual(serializedDocument.DocumentElement.OuterXml, deserializedDocument.DocumentElement.OuterXml);
    }
  }
}