﻿using System.Collections.Generic;
using System.Threading;

using MorganStanley.MSDesktop.Rambo.API;

using NUnit.Framework;

namespace MorganStanley.MSDesktop.Rambo.Test
{
  [TestFixture]
  public class RamboAPITests : UnitTestBase
  {
    #region Get field values tests

    [Test]
    [Ignore]
    public void GetFieldValues_RamboUserName_NewInstanceWithData()
    {
      bool loaded = false;
      DSResponse response = null;
      RamboAPI.GetFieldValues(
        ColumnName.RamboUserid,
        delegate(DSResponse response_)
        {
          loaded = true;
          response = response_;
        });

      while (!loaded)
      {
        Thread.Sleep(1000);
      }

      Assert.IsNotNull(response);
      Assert.IsNotNull(response.Response);
      Assert.IsTrue(response.IsSuccessful);

      RamboFieldValueList fieldValueList = response.Response as RamboFieldValueList;
      Assert.IsNotNull(fieldValueList);
      Assert.Greater(fieldValueList.ValueList.Count, 0);
    }

    #endregion
    
    #region Get resource tests
    
    [Test]
    [Category("Get resource")]
    [Ignore]
    public void GetResource_TestData_NewInstanceWithData()
    {
      bool loaded = false;
      DSResponse response = null;
      RamboAPI.GetResource(
      UnitTestConstants.UnitTestResourceID,
      delegate(DSResponse response_)
      {
        loaded = true;
        response = response_;
      });

      while (!loaded)
      {
        Thread.Sleep(1000);
      }

      Assert.IsNotNull(response);
      Assert.IsNotNull(response.Response);
      Assert.IsTrue(response.IsSuccessful);

      RamboResource resource = response.Response as RamboResource;
      Assert.IsNotNull(resource);
    }

    [Test]
    [Category("Get resource")]
    [Ignore]
    public void GetResource_InvalidParameters_Null()
    {
      bool loaded = false;
      DSResponse response = null;
      RamboAPI.GetResource(
      UnitTestConstants.UnitTestWrongResourceID,
      delegate(DSResponse response_)
      {
        loaded = true;
        response = response_;
      });

      while (!loaded)
      {
        Thread.Sleep(1000);
      }

      Assert.IsNotNull(response);
      Assert.IsNull(response.Response);
      Assert.IsFalse(response.IsSuccessful);
    }

    #endregion

    #region Save resource tests

    [Test]
    [Category("Save resource")]
    [Ignore]
    public void SaveResource_ConcordLayoutResourceType_SavedSuccessfully()
    {
      bool loaded = false;
      DSResponse response = null;
      RamboResource resource = new RamboResource
                                 {
                                   ResourceType = ConcordInterface.CONFIGURATOR,
                                   UserName = UnitTestConstants.UnitTestUserName,
                                   ApplicationName = UnitTestConstants.UnitTestApplicationName,
                                   Profile = UnitTestConstants.UnitTestProfileName,
                                 };
      resource.XmlData.LoadXml(UnitTestConstants.UnitTestXmlString);
      RamboAPI.SaveResource(resource, delegate(DSResponse response_)
      {
        loaded = true;
        response = response_;
      });

      while (!loaded)
      {
        Thread.Sleep(1000);
      }

      Assert.IsNotNull(response);
      Assert.IsNotNull(response.Response);
      Assert.IsTrue(response.IsSuccessful);
    }

    [Test]
    [Category("Save resource")]
    [Ignore]
    public void SaveResource_ConfiguratorResourceType_SavedSuccessfully()
    {
      bool loaded = false;
      DSResponse response = null;
      RamboResource resource = new RamboResource
                                 {
                                   ResourceType = ConcordInterface.CONFIGURATOR,
                                   UserName = UnitTestConstants.UnitTestUserName,
                                   ApplicationName = UnitTestConstants.UnitTestApplicationName,
                                   Profile = UnitTestConstants.UnitTestProfileName,
                                   ConfigName = UnitTestConstants.UnitTestConfigName,
                                 };
      resource.XmlData.LoadXml(UnitTestConstants.UnitTestXmlString);
      RamboAPI.SaveResource(resource, delegate(DSResponse response_)
      {
        loaded = true;
        response = response_;
      });

      while (!loaded)
      {
        Thread.Sleep(1000);
      }

      Assert.IsNotNull(response);
      Assert.IsNotNull(response.Response);
      Assert.IsTrue(response.IsSuccessful);
    }

    [Test]
    [Category("Save resource")]
    [Ignore]
    public void SaveResource_GridLayoutResourceType_SavedSuccessfully()
    {
      bool loaded = false;
      DSResponse response = null;
      RamboResource resource = new RamboResource
                                 {
                                   ResourceType = "GridLayout",
                                   AppletName = UnitTestConstants.UnitTestAppletName,
                                   GridLayout = UnitTestConstants.UnitTestGridLayoutName,
                                   GridName = UnitTestConstants.UnitTestGridName,
                                 };
      resource.XmlData.LoadXml(UnitTestConstants.UnitTestXmlString);
      RamboAPI.SaveResource(resource, delegate(DSResponse response_)
      {
        loaded = true;
        response = response_;
      });

      while (!loaded)
      {
        Thread.Sleep(1000);
      }

      Assert.IsNotNull(response);
      Assert.IsNotNull(response.Response);
      Assert.IsTrue(response.IsSuccessful);
    }

    [Test]
    [Category("Save resource")]
    [Ignore]
    public void SaveResource_MiscellaneousResourceType_SavedSuccessfully()
    {
      bool loaded = false;
      DSResponse response = null;
      RamboResource resource = new RamboResource
                                 {
                                   ResourceType = "Miscellaneous",
                                   UserName = UnitTestConstants.UnitTestUserName,
                                   ApplicationName = UnitTestConstants.UnitTestApplicationName,
                                 };
      resource.XmlData.LoadXml(UnitTestConstants.UnitTestXmlString);
      RamboAPI.SaveResource(resource, delegate(DSResponse response_)
      {
        loaded = true;
        response = response_;
      });

      while (!loaded)
      {
        Thread.Sleep(1000);
      }

      Assert.IsNotNull(response);
      Assert.IsNotNull(response.Response);
      Assert.IsTrue(response.IsSuccessful);
    }

    #endregion
  }
}