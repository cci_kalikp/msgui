﻿using System;
using MorganStanley.MSDesktop.Rambo.API;

namespace MorganStanley.MSDesktop.Rambo.Test
{
  internal class UnitTestConstants
  {
    internal const string ConcordConfigurationLayouts = "Layouts";
    internal const string OtherLayouts = "DataServices";

    internal const string UnitTestXmlString = "<Grid>TestData</Grid>";

    internal const string UnitTestUserName = "RamboApiUnitTestUserName";
    internal const string UnitTestAppletName = "RamboApiUnitTestAppletName";
    internal const string UnitTestGridName = "RamboApiUnitTestGridName";
    internal const string UnitTestGridLayoutName = "RamboApiUnitTestLayoutName";
    internal const string UnitTestApplicationName = "RamboApiUnitTestApplicationName";
    internal const string UnitTestProfileName = "RamboApiUnitTestProfileName";
    internal const string UnitTestConfigName = "RamboApiUnitTestConfigName";

    internal const string UnitTestCloneUserName = "RamboApiUnitTestCloneUserName";
    internal const string UnitTestCloneAppletName = "RamboApiUnitTestCloneAppletName";
    internal const string UnitTestCloneGridName = "RamboApiUnitTestCloneGridName";
    internal const string UnitTestCloneGridLayoutName = "RamboApiUnitTestCloneGridLayoutName";
    internal const string UnitTestCloneApplicationName = "RamboApiUnitTestCloneApplicationName";
    internal const string UnitTestCloneProfileName = "RamboApiUnitTestCloneProfileName";
    internal const string UnitTestCloneConfigName = "RamboApiUnitTestCloneConfigName";

    internal const string UnitTestConcordInterfaceProfileName = "ln-qa-qa";

    internal const int UnitTestExact = 0;

    internal static readonly DateTime UnitTestDataTime = DateTime.Now.ToUniversalTime();
    internal static readonly DateTime UnitTestUpdateTime = DateTime.Now.ToUniversalTime();
    internal static readonly DateTime UnitTestStartDataTime = DateTime.Now.ToUniversalTime();
    internal static readonly DateTime UnitTestEndDataTime = DateTime.Now.ToUniversalTime();

    internal static readonly string UnitTestWrongResourceID = "0/1/2/3/4/5/6/7";
    internal static readonly string UnitTestResourceID = String.Format(
      "{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}",
      UnitTestUserName,
      UnitTestApplicationName,
      ConcordInterface.CONFIGURATOR,
      Constants.EmptyKeyString,
      Constants.EmptyKeyString,
      Constants.EmptyKeyString,
      UnitTestProfileName,
      Constants.EmptyKeyString);
  }
}