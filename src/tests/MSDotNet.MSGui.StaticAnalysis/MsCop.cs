﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.StaticAnalysis/MsCop.cs#7 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.XPath;

namespace MSDotNet.MSGui.StaticAnalysis
{
    /// <summary>
    /// Wrapper program for FxCop that resolves assembly dependencies and passes them to FxCop
    /// </summary>
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Valid prefix")]
    public class MsCop
    {
        private const string ASSEMBLIES = "/ns:Project/ns:ItemGroup/ns:Reference/ns:HintPath|//ns:ProjectReference/@Include";

        private HashSet<string> m_referencePaths = new HashSet<string>();
        private IList<string> m_fxCopArgs = new List<string>();
        private string m_targetFile;
        private string m_workingDir = Directory.GetCurrentDirectory();

        /// <summary>
        /// Gets or sets the working dir.
        /// </summary>
        /// <value>
        /// The working dir.
        /// </value>
        protected string WorkingDir
        {
            get
            {
                return m_workingDir;
            }

            set 
            {
                m_referencePaths.Remove(m_workingDir);
                m_workingDir = value;
                Directory.SetCurrentDirectory(m_workingDir);
                m_referencePaths.Add(m_workingDir);
            }
        }

        /// <summary>
        /// Program entry point, this method instantiates main object and runs it.
        /// </summary>
        /// <param name="args_">The arguments.</param>
        public static void Main(string[] args_)
        {
            if (args_.Length <= 0)
            {
                Console.WriteLine("Usage: Program <Project file|Solution file>");
            }
            else
            {
                MsCop prog = new MsCop();
                prog.Run(args_);
            }
        }

        /// <summary>
        /// Runs the program.
        /// </summary>
        /// <param name="args_">The arguments.</param>
        private void Run(string[] args_)
        {
            ParseArgs(args_);
            MakeRefPathsAbsolute();
            CopyLocalIfNecessary();
            RunProgram(@"\\ms\dist\microsoft\PROJ\fxcop\1.36\bin\FxCopcmd.exe", BuildFxCopArgs());
        }

        /// <summary>
        /// Makes the reference paths absolute.
        /// </summary>
        private void MakeRefPathsAbsolute()
        {
            if (m_targetFile != null)
            {
                string dir = Path.GetDirectoryName(m_targetFile);
                foreach (var item in m_referencePaths)
                {
                    if (!Path.IsPathRooted(dir))
                    {
                        m_referencePaths.Remove(item);
                        m_referencePaths.Add(Path.Combine(Path.GetDirectoryName(m_targetFile), dir));
                    }
                }
            }
         }

        /// <summary>
        /// Parses the arguments.
        /// </summary>
        /// <param name="args_">The arguments.</param>
        private void ParseArgs(string[] args_)
        {
            foreach (var arg in args_)
            {
                if (arg.StartsWith("/mscopworkingdir:"))
                {
                    string dir = Path.GetFullPath(arg.Substring("/mscopworkingdir:".Length));
                    if (dir != null && Directory.Exists(dir))
                    {
                        WorkingDir = dir;
                    }
                    else
                    {
                        Console.WriteLine("Error: Directory not found: \"" + dir + "\".");
                    }
                }
                else if (arg.StartsWith("/mscopproj:"))
                {
                    string prjFile = arg.Substring("/mscopproj:".Length);
                    if (prjFile != null && File.Exists(prjFile))
                    {
                        OpenProj(prjFile);
                    }
                    else
                    {
                        Console.WriteLine("Error: File not found: \"" + prjFile + "\".");
                        return;
                    }
                }
                else
                {
                    if (arg.StartsWith("/f:"))
                    {
                        m_targetFile = arg.Substring("/f:".Length);
                    }
                    else if (arg.StartsWith("/file:"))
                    {
                        m_targetFile = arg.Substring("/file:".Length);
                    }

                    m_fxCopArgs.Add(arg);
                }
            }
        }

        /// <summary>
        /// Opens a project/solution file.
        /// </summary>
        /// <param name="projFile_">The proj file.</param>
        private void OpenProj(string projFile_)
        {
            string extension = Path.GetExtension(projFile_);
            if (extension == ".csproj")
            {
                ParseProj(projFile_);
            }
            else if (extension == ".sln")
            {
                Regex regex = new Regex(@"""(?<path>[^""]*\.csproj)""");
                string text = null;
                using (StreamReader slnReader = new StreamReader(projFile_))
                {
                    text = slnReader.ReadToEnd();
                }

                if (text != null)
                {
                    foreach (Match item in regex.Matches(text))
                    {
                        string projPath = item.Groups["path"].Value;
                        ParseProj(Path.IsPathRooted(projPath) ? projPath : Path.Combine(Path.GetDirectoryName(projFile_), projPath));
                    }
                }
            }
        }

        /// <summary>
        /// Parses the proj file.
        /// </summary>
        /// <param name="projFile_">The proj file.</param>
        private void ParseProj(string projFile_)
        {
            XPathNavigator nav = new XPathDocument(projFile_).CreateNavigator();
            XmlNamespaceManager namespaceManager = new XmlNamespaceManager(nav.NameTable);
            namespaceManager.AddNamespace("ns", "http://schemas.microsoft.com/developer/msbuild/2003");
            XPathExpression expr = nav.Compile(ASSEMBLIES);
            expr.SetContext(namespaceManager);
            foreach (XPathNavigator item in nav.Select(expr))
            {
                if (item.LocalName == "HintPath" || item.LocalName == "Include")
                {
                    m_referencePaths.Add(Path.GetDirectoryName(item.Value));
                }
            }
        }

        /// <summary>
        /// Copies assemblies local if necessary using ARtoLocal.
        /// </summary>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation",
            Justification = "The program actually is called ARToLocal", Scope = "Type")]
        private void CopyLocalIfNecessary()
        {
            if (m_targetFile != null && File.Exists(m_targetFile + ".msde.config"))
            {
                foreach (var item in Directory.GetFiles(m_workingDir))
                {
                    FileAttributes attr = File.GetAttributes(item);
                    if ((attr & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    {
                        File.SetAttributes(item, attr & ~FileAttributes.ReadOnly);
                    }
                }

                string arToLocalArgs = " -all \"-dll:" + m_targetFile + "\" \"-msde:" + m_targetFile
                                     + ".msde.config\" -verbosity:Verbose \"-to:"
                                     + WorkingDir + "\"";
                RunProgram(
                    @"\\ms\dev\msdotnet\corona\0.4.4\install\common\bin\artolocal.exe",
                    arToLocalArgs);
            }
        }

        /// <summary>
        /// Builds the FxCop args.
        /// </summary>
        /// <returns>string of command line arguments</returns>
        private string BuildFxCopArgs()
        {
            StringBuilder argsBuilder = new StringBuilder("/c /gac");
            foreach (var reference in m_referencePaths)
            {
                argsBuilder.Append(" \"/d:").Append(reference).Append('"');
            }

            foreach (var arg in m_fxCopArgs)
            {
                argsBuilder.Append(" \"").Append(arg).Append('"');
            }

            return argsBuilder.ToString();
        }

        /// <summary>
        /// Runs the program forwarding its standard output and waiting for it to finish.
        /// </summary>
        /// <param name="fileName_">Name of the file.</param>
        /// <param name="arguments_">The arguments.</param>
        private void RunProgram(string fileName_, string arguments_)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = fileName_;
            startInfo.Arguments = arguments_;
            startInfo.WorkingDirectory = WorkingDir;
            startInfo.RedirectStandardOutput = true;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.UseShellExecute = false;
            Console.WriteLine(string.Format("Running {0} {1} in {2}", startInfo.FileName, startInfo.Arguments, startInfo.WorkingDirectory));
            using (Process process = Process.Start(startInfo))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    while (!reader.EndOfStream)
                    {
                        Console.WriteLine(reader.ReadLine());
                    }
                }
            }
        }
    }
}
