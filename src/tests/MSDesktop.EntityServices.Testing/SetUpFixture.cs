﻿using NUnit.Framework;
using MorganStanley.MSDotNet.Runtime;

namespace MorganStanley.Desktop.EntityServices.Testing
{
    [SetUpFixture]
    public class SetUpFixture
    {
        static SetUpFixture()
        {
            AssemblyResolver.Load("EntityServicesTests.msde.config");
        }
    }
}
