﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using MorganStanley.Desktop.EntityServices.Data;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.TestingCommon.Entities;
using NUnit.Framework;

namespace MorganStanley.Desktop.EntityServices.Testing.DataTests
{
    [TestFixture]
    public class XmlDataConverterTests
    {
        private XmlDataConverter<TestEntity> _xmlDataConverter;

        [SetUp]
        public void SetUp()
        {
            _xmlDataConverter = new XmlDataConverter<TestEntity>();
        }

        [Test]
        public void Convert_InputNull_ReturnNull()
        {
            // Arrange

            // Act
            var entity = _xmlDataConverter.Convert(null);

            // Assert
            Assert.IsNull(entity);
        }

        [Test]
        public void Convert_InputIsNotXmlElement_ReturnNull()
        {
            // Arrange

            // Act
            var entity = _xmlDataConverter.Convert("Not an XmlElement");

            // Assert
            Assert.IsNull(entity);
        }

        [Test]
        public void Convert_InputIsXmlElement_ReturnsDeserialisedObject()
        {
            // Arrange
            const string myValue = "testValue";
            var mySerialisableObject = new MySerialisableObject { MyValue = myValue };
            var serializedObject = GetSerializedObject(mySerialisableObject);
            var xmlDataConverter = new XmlDataConverter<MySerialisableObject>();

            // Act
            var returnedEntity = xmlDataConverter.Convert(serializedObject);

            // Assert
            Assert.AreEqual(myValue, returnedEntity.MyValue);
        }

        public class MySerialisableObject : IEntity
        {
            public string MyValue { get; set; }

            public IKey Key
            {
                get { throw new NotImplementedException(); }
            }
        }

        public XmlElement GetSerializedObject<T>(T objectToSerialise)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            string xml;
            using (var stringWriter = new StringWriter())
            {
                xmlSerializer.Serialize(stringWriter, objectToSerialise);
                xml = stringWriter.ToString();
            }
            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xml);
            return xmlDocument.DocumentElement;
        }
    }
}
