﻿using System;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;

namespace EntityServicesTests.Entities
{
    [Serializable]
    public class TestEntityKey : BasicKey<TestEntity>, IWithOptionalCaching
    {
        private readonly CachingOptions _options;
        private readonly WhenCaching _whenCaching;
        private readonly TimeSpan? _timeout;
        private readonly int? _resultsStoredTogetherThreshold;

        public TestEntityKey(string key, CachingOptions options = CachingOptions.Default, WhenCaching whenCaching = WhenCaching.DoDefault, TimeSpan? timeout = null, int? resultsStoredTogetherThreshold = null)
            : base(key)
        {
            _options = options;
            _whenCaching = whenCaching;
            _timeout = timeout;
            _resultsStoredTogetherThreshold = resultsStoredTogetherThreshold;
        }

        public TimeSpan? Timeout{get { return _timeout; }}
        public int? ResultsStoredTogetherThreshold { get { return _resultsStoredTogetherThreshold; } }
        public CachingOptions CachingOptions{get { return _options; }}
        public WhenCaching WhenCaching{get { return _whenCaching; }}
    }
}