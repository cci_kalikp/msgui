﻿using System;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;

namespace EntityServicesTests.Entities
{
    public class UnserializableTestEntity : IEntity<UnserializableTestEntity, BasicKey<UnserializableTestEntity>>, IPublishable, IEquatable<UnserializableTestEntity>, ISubscribable, IFetchable, ITimestamped, IDeletable
    {
        private readonly UnserializableTestEntityKey _key;

        public UnserializableTestEntity(UnserializableTestEntityKey key)
        {
            _key = key;
        }

        public UnserializableTestEntity() : this(new UnserializableTestEntityKey("testkey"))
        {
        }

        IKey IEntity.Key
        {
            get { return ((IEntity<UnserializableTestEntity, BasicKey<UnserializableTestEntity>>)this).Key; }
        }

        BasicKey<UnserializableTestEntity> IEntity<UnserializableTestEntity, BasicKey<UnserializableTestEntity>>.Key
        {
            get { return _key; }
        }

        public bool Equals(UnserializableTestEntity other)
        {
            throw new NotImplementedException();
        }

        public DateTime Timestamp { get; set; }

        public DateTime? ReferenceDate
        {
            get { return null; }
        }

        DateTime ITimestamped.Timestamp
        {
            get { throw new NotImplementedException(); }
        }
    }
}