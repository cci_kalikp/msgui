﻿using System;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;

namespace EntityServicesTests.Entities
{
    [Serializable]
    public class TestEntity : IEntity<TestEntity, BasicKey<TestEntity>>, IPublishable, IEquatable<TestEntity>, ISubscribable, IFetchable, ITimestamped, IDeletable
    {
        public string TestTypeDifferentiator { get; set; }

        private readonly BasicKey<TestEntity> _key;

        public TestEntity()
            : this("Test")
        {
        }

        public TestEntity(string key)
        {
            _key = new BasicKey<TestEntity>(key);
        }

        public TestEntity(BasicKey<TestEntity> key)
        {
            _key = key;
        }

        IKey IEntity.Key { get { return _key; } }
        BasicKey<TestEntity> IEntity<TestEntity, BasicKey<TestEntity>>.Key { get { return _key; } }

        public override bool Equals(object obj)
        {
            return Equals(obj as TestEntity);
        }

        public bool Equals(TestEntity other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other._key, _key) && Equals(other.TestTypeDifferentiator, TestTypeDifferentiator);
        }

        public override int GetHashCode()
        {
            return (_key != null ? _key.GetHashCode() : 0);
        }

        public static bool operator ==(TestEntity left, TestEntity right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(TestEntity left, TestEntity right)
        {
            return !Equals(left, right);
        }

        public DateTime Timestamp { get; set; }

        public DateTime? ReferenceDate
        {
            get { return null; }
        }

        public override string ToString()
        {
            return string.Format("Key: {0}, Differentiator: {1}", _key, TestTypeDifferentiator);
        }
    }
}