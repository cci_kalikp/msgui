﻿using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using NUnit.Framework;

namespace EntityServicesTests
{
    [TestFixture]
    public class LocalAndNonLocalKeyTests
    {
        [Test]
        public void MakeLocal_BasicKey_AddsDefaultLocalKeySuffixToKey()
        {
            // Arrange
            var testKey = new TestKey("Test");

            // Act
            testKey = testKey.ToLocal();

            // Assert
            Assert.AreEqual("Test_<local>", testKey.ToString());
        }

        [Test]
        public void MakeLocal_AlreadyLocalKey_DoesNotAddDefaultLocalKeySuffixToKey()
        {
            // Arrange
            var testKey = new TestKey("Test_<local>");

            // Act
            testKey = testKey.ToLocal();

            // Assert
            Assert.AreEqual("Test_<local>", testKey.ToString());
        }

        [Test]
        public void MakeLocal_GetLocalKeyStringOverloaded_AppendsOverloadedValueRatherThanDefaultSuffix()
        {
            // Arrange
            var testKey = new NewLocalStringTestKey("Test");

            // Act
            testKey = testKey.ToLocal();

            // Assert
            Assert.AreEqual("TestNewLocalString", testKey.ToString());
        }

        [Test]
        public void MakeGlobal_BasicLocalKey_RemovesLocalKeyStringFromKey()
        {
            // Arrange
            var testKey = new TestKey("Test_<local>");

            // Act
            testKey = testKey.ToNonLocal();

            // Assert
            Assert.AreEqual("Test", testKey.ToString());
        }

        [Test]
        public void MakeGlobal_NonLocalKey_LeavesTheKeyUnchanged()
        {
            // Arrange
            var testKey = new TestKey("Test");

            // Act
            testKey = testKey.ToNonLocal();

            // Assert
            Assert.AreEqual("Test", testKey.ToString());
        }

        [Test]
        public void MakeGlobal_GetLocalKeyStringOverloaded_RemovesOverloadedValueRatherThanDefaultSuffix()
        {
            // Arrange
            var testKey = new NewLocalStringTestKey("TestNewLocalString");

            // Act
            testKey = testKey.ToNonLocal();

            // Assert
            Assert.AreEqual("Test", testKey.ToString());
        }
    }

    public class NewLocalStringTestKey : StringBasedKey
    {
        public NewLocalStringTestKey(string field)
            : base("NewLocalString", new KeySeperator(QuerySeperator.Create("/")), new[] {field})
        {
        }

        public NewLocalStringTestKey()
            : base("NewLocalString", new KeySeperator(QuerySeperator.Create("/")), 1)
        {
            
        }

        public NewLocalStringTestKey ToLocal()
        {
            return CreateLocalKey<NewLocalStringTestKey>();
        }

        public NewLocalStringTestKey ToNonLocal()
        {
            return CreateNonLocalKey<NewLocalStringTestKey>();
        }
    }

    public class TestKey : StringBasedKey
    {
        public TestKey(string field)
            : base(new KeySeperator(QuerySeperator.Create("/")), new[] { field })
        {
        }

        public TestKey()
            : base(new KeySeperator(QuerySeperator.Create("/")), 1)
        {
            
        }

        public TestKey ToLocal()
        {
            return CreateLocalKey<TestKey>();
        }

        public TestKey ToNonLocal()
        {
            return CreateNonLocalKey<TestKey>();
        }
    }
}
