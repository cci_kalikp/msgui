﻿using System;
using NUnit.Framework;

namespace EntityServicesTests
{
    public class ExceptionTester
    {
        private Exception _exc;

        public void Act(Action method)
        {
            try
            {
                method();
            }
            catch (Exception exc)
            {
                _exc = exc;
            }
        }

        public void ShouldMatchExceptionMessage(string message)
        {
            Assert.That(_exc, Is.Not.Null);
            Assert.That(message, Is.EqualTo(_exc.Message));
        }

        public void ShouldThrow<T>()
            where T : Exception
        {
            Assert.That(_exc, Is.Not.Null);
            Assert.IsTrue(_exc is T, String.Format("The expected type was {0} and the actual type was {1}", typeof(T).FullName, _exc.GetType().FullName));
        }

        public void ShouldThrow<T>(string message)
            where T : Exception
        {
            Assert.That(_exc, Is.Not.Null);
            Assert.IsTrue(_exc is T, String.Format("The expected type was {0} and the actual type was {1}", typeof(T).FullName, _exc.GetType().FullName));
            ShouldMatchExceptionMessage(message);
        }

        public void ShouldContainInnerException(Exception expectedInnerException)
        {
            Assert.That(expectedInnerException, Is.EqualTo(_exc.InnerException));
        }

        public void ShouldContainInnerException<T>()
            where T : Exception
        {
            Assert.That(_exc, Is.Not.Null);
            Assert.That(_exc.InnerException, Is.Not.Null);
            Assert.IsTrue(_exc.InnerException is T, String.Format("The expected type was {0} and the actual type was {1}", typeof(T).FullName, _exc.GetType().FullName));
        }
        public void ShouldContainInnerException<T>(string message)
            where T : Exception
        {
            Assert.That(_exc, Is.Not.Null);
            Assert.That(_exc.InnerException, Is.Not.Null);
            Assert.IsTrue(_exc.InnerException is T, String.Format("The expected type was {0} and the actual type was {1}", typeof(T).FullName, _exc.GetType().FullName));
            Assert.That(message, Is.EqualTo(_exc.InnerException.Message));
        }

    }
}
