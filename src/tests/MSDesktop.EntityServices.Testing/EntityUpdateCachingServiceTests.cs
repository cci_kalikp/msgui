﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using EntityServicesTests.Entities;
using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Caching;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;
using NUnit.Framework;
using Rhino.Mocks;

namespace EntityServicesTests
{
    [TestFixture]
    public class EntityUpdateCachingServiceTests
    {
        private IEntityService<TestEntity> _entityServiceMock;
        private IEntityServiceExceptionService _exceptionServiceMock;
        private EntityCachingService<TestEntity> _entityUpdateCachingService;

        [SetUp]
        public void SetUp()
        {
            _entityServiceMock = MockRepository.GenerateMock<IEntityService<TestEntity>>();
            _exceptionServiceMock = MockRepository.GenerateMock<IEntityServiceExceptionService>();
            _entityUpdateCachingService = new EntityCachingService<TestEntity>(
                _entityServiceMock,
                _exceptionServiceMock,
                new CacheService(),
                new EntityServiceCachingAttribute { TimeoutMinutes = 10 });
        }

        [Test]
        public void Get_BasicCall_CallsGetObservableToGetUpdateStream()
        {
            // Arrange
            var key = new TestEntityKey("My Key");
            var initialTestEntity = new TestEntity(key) { TestTypeDifferentiator = "Item1" };

            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => Equals(queries.Single(), key))))
                .Return(new[] { initialTestEntity });

            _entityServiceMock
                .Expect(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => Equals(y.First(), key))))
                .Return(subject);

            // Act
            _entityUpdateCachingService.Get(new[] { key });

            // Assert
            _entityServiceMock.VerifyAllExpectations();
        }

        [Test]
        public void Get_CacheValueExistsThatHasBeenUpdated_ReturnsUpdatedCacheValue()
        {
            // Arrange
            var key = new TestEntityKey("My Key");
            var initialTestEntity = new TestEntity(key) { TestTypeDifferentiator = "Item1" };
            var updatedTestEntity = new TestEntity(key) { TestTypeDifferentiator = "Item2" };

            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => Equals(queries.Single(), key))))
                .Return(new[] { initialTestEntity });

            _entityServiceMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => Equals(queries.Single(), key))))
                .Return(subject);

            // Gets the entity in to the cache...
            _entityUpdateCachingService.Get(new[] { key });

            subject.OnNext(new ObservableResource<TestEntity>(new[] { updatedTestEntity }));

            // Act
            var returnedTestEntity = _entityUpdateCachingService.Get(new[] { key }).First();

            // Assert
            Assert.AreEqual(updatedTestEntity.TestTypeDifferentiator, returnedTestEntity.TestTypeDifferentiator);
        }

        [Test]
        public void GetObservable_BasicCall_CallsGetObservableOnUnderlyingService()
        {
            // Arrange
            var query = new TestEntityQuery("MyQuery");

            _entityServiceMock
                .Expect(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
                .Return(new Subject<ObservableResource<TestEntity>>());

            // Act
            _entityUpdateCachingService.GetObservable(new[] { query }).Subscribe(x => { });

            // Assert
            _entityServiceMock.VerifyAllExpectations();
        }

        [Test]
        public void GetObservable_MultipleCalls_OnlyCallGetObservableOnUnderlyingServiceOnce()
        {
            // Arrange
            var query = new TestEntityQuery("MyQuery");

            _entityServiceMock
                .Expect(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
                .Return(new Subject<ObservableResource<TestEntity>>())
                .Repeat.Once();


            // Act
            _entityUpdateCachingService.GetObservable(new[] { query }).Subscribe(x => { });
            _entityUpdateCachingService.GetObservable(new[] { query }).Subscribe(x => { });

            // Assert
            _entityServiceMock.VerifyAllExpectations();
        }

        [Test]
        public void GetObservable_UpdateRaisedOnUnderlyingService_RaisesUpdate()
        {
            // Arrange
            var query = new TestEntityQuery("MyQuery");
            var key = new TestEntityKey("MyKey");
            var entity = new TestEntity(key);

            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
                .Return(subject);

            TestEntity entityRaised = null;
            _entityUpdateCachingService.GetObservable(new[] { query }).Select(resource => resource.Entities).Subscribe(x => { entityRaised = x.FirstOrDefault(); });

            // Act
            subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));

            // Assert
            Assert.IsNotNull(entityRaised);
            Assert.AreEqual(entity, entityRaised);
        }

        [Test]
        public void GetObservable_ExceptionRaisedOnUnderlyingService_RaisesException()
        {
            // Arrange
            var query = new TestEntityQuery("MyQuery");

            var exception = new Exception();
            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
                .Return(subject);

            Exception exceptionRaised = null;
            _entityUpdateCachingService.GetObservable(new[] { query }).Subscribe(x => { }, y => { exceptionRaised = y; });

            // Act
            subject.OnError(exception);

            // Assert
            Assert.IsNotNull(exceptionRaised);
            Assert.AreEqual(exception, exceptionRaised);
        }

        [Test]
        public void Get_UpdateOccursButTimeoutLapses_NextGetShouldCallUnderlyingService()
        {
            // RG: This test is proof that the updatable cache will timeout even if it has a stream of updates feeding it...

            // Arrange
            var cacheMock = MockRepository.GenerateMock<ICache<IOneToOneQuery, bool>>();
            var cacheMock2 = MockRepository.GenerateMock<ICache<IQuery, IEnumerable<IKey>>>();
            var cacheServiceMock = MockRepository.GenerateMock<ICacheService>();

            cacheMock.Stub(x => x.TryAdd(Arg<IKey>.Is.Anything, Arg<bool>.Is.Anything))
                     .Return(true);
            cacheServiceMock.Stub(x => x.CreateCache(Arg<TimeSpan>.Is.Anything, Arg<bool>.Is.Anything, Arg<bool>.Is.Anything, Arg<TryFuncWithTimeout<IQuery, IEnumerable<IKey>>>.Is.Anything))
                            .Return(cacheMock2);


            cacheServiceMock.Stub(x => x.CreateCache(Arg<TimeSpan>.Is.Anything, Arg<bool>.Is.Anything, Arg<bool>.Is.Anything, Arg<TryFuncWithTimeout<IOneToOneQuery, bool>>.Is.Anything))
                            .Return(cacheMock);


            var entityUpdateCachingService = new EntityCachingService<TestEntity>(
                _entityServiceMock,
                _exceptionServiceMock,
                cacheServiceMock,
                new EntityServiceCachingAttribute { TimeoutMinutes = 3 });

            var key = new TestEntityKey("My Key");
            var initialTestEntity = new TestEntity(key) { TestTypeDifferentiator = "Item1" };

            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Is.Anything))
                .Return(new[] { initialTestEntity });

            _entityServiceMock
                .Expect(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Is.Anything))
                .Return(subject);

            // Load the cache
            entityUpdateCachingService.Get(new[] { key });

            // Raise the update which should not touch the expiring cache
            var newTestEntity = new TestEntity(key);
            subject.OnNext(new ObservableResource<TestEntity>(new[] { newTestEntity }));

            //Assert
            cacheMock.AssertWasCalled(x => x.TryAdd(Arg<IKey>.Is.Anything, Arg<bool>.Is.Anything), options => options.Repeat.Once());
            Assert.IsTrue(ReferenceEquals(entityUpdateCachingService.Get(new[] { key }).Single(), newTestEntity));
        }


        [Test]
        public void Get_GetObservableUpdateOnKeyWithoutAPreviousGet_DoesNotCallUnderlyingEntityService()
        {
            // Arrange
            var key = new TestEntityKey("testkey");
            var entity = new TestEntity(key);

            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
                .Return(subject);

            _entityUpdateCachingService.GetObservable(new[] { key }).Subscribe(x => { });
            subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));

            // Act
            _entityUpdateCachingService.Get(new[] { key });

            // Assert
            _entityServiceMock.AssertWasNotCalled(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))));
        }

        [Test]
        public void Get_GetObservableUpdateOnKeyWithoutAPreviousGet_ReturnsValueFromCache()
        {
            // Arrange
            var key = new TestEntityKey("testkey");
            var entity = new TestEntity(key);

            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
                .Return(subject);

            _entityUpdateCachingService.GetObservable(new[] { key }).Subscribe(x => { });
            subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));

            // Act
            var entities = _entityUpdateCachingService.Get(new[] { key });

            // Assert
            Assert.AreEqual(1, entities.Count());
            Assert.AreEqual(entity, entities.Single());
        }

        [Test]
        public void Get_GetObservableUpdateOnNonKeyQueryWithoutAPreviousGet_CallsUnderlyingEntityService()
        {
            // Arrange
            var query = new TestEntityQuery("test query");
            var key = new TestEntityKey("testKey");
            var entityFromUpdate = new TestEntity(key);
            var entityFromGet = new TestEntity(key);

            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
                .Return(subject);

            _entityUpdateCachingService.GetObservable(new[] { query }).Subscribe(x => { });
            subject.OnNext(new ObservableResource<TestEntity>(new[] { entityFromUpdate }));

            _entityServiceMock
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
                .Return(new[] { entityFromGet });

            // Act
            _entityUpdateCachingService.Get(new[] { query });

            // Assert
            _entityServiceMock.AssertWasCalled(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))));
        }

        [Test]
        public void Get_GetObservableUpdateOnNonKeyQueryWithoutAPreviousGet_ReturnsValueFromUnderlyingService()
        {
            // Arrange
            var query = new TestEntityQuery("test query");
            var key = new TestEntityKey("testKey");
            var entityFromUpdate = new TestEntity(key);
            var entityFromGet = new TestEntity(key);

            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
                .Return(subject);

            _entityUpdateCachingService.GetObservable(new[] { query }).Subscribe(x => { });
            subject.OnNext(new ObservableResource<TestEntity>(new[] { entityFromUpdate }));

            _entityServiceMock
                .Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
                .Return(new[] { entityFromGet });

            // Act
            var entities = _entityUpdateCachingService.Get(new[] { query });

            // Assert
            Assert.AreEqual(1, entities.Count());
            Assert.AreEqual(entityFromGet, entities.Single());
        }

        [Test]
        public void Get_GetPreviouslyCalledOnQueryThatReturnsAnObjectWithTheSameKeyAsTheCurrentGet_DoesntCallUnderlyingServiceASecondTime()
        {
            // Arrange
            var key1 = new TestEntityKey("Key1");
            var key2 = new TestEntityKey("Key2");
            var query = new TestEntityQuery("Query");
            var entity1 = new TestEntity(key1);
            var entity2 = new TestEntity(key2);

            _entityServiceMock
                .Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
                .Return(new[] { entity1, entity2 });

            _entityServiceMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
                .Return(new Subject<ObservableResource<TestEntity>>());

            _entityUpdateCachingService.Get(new[] { query });

            // Act
            _entityUpdateCachingService.Get(new[] { key1 });

            // Assert
            _entityServiceMock.AssertWasNotCalled(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key1))));
        }

        [Test]
        public void Get_GetPreviouslyCalledOnQueryThatReturnsAnObjectWithTheSameKeyAsTheCurrentGet_ReturnsEntityFromCache()
        {
            // Arrange
            var key1 = new TestEntityKey("Key1");
            var key2 = new TestEntityKey("Key2");
            var query = new TestEntityQuery("Query");
            var entity1 = new TestEntity(key1);
            var entity2 = new TestEntity(key2);

            _entityServiceMock
                .Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
                .Return(new[] { entity1, entity2 });

            _entityServiceMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
                .Return(new Subject<ObservableResource<TestEntity>>());

            _entityUpdateCachingService.Get(new[] { query });

            // Act
            var returnedEntities = _entityUpdateCachingService.Get(new[] { key1 });

            // Assert
            Assert.AreEqual(entity1, returnedEntities.First());
        }
    }
}
