﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using EntityServicesTests.Entities;
using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;
using NUnit.Framework;
using Rhino.Mocks;

namespace EntityServicesTests
{
    [TestFixture]
    public class ServiceTest : TestBase
    {
        private EntityService<TestEntity> _service;
        private IEntityServiceConnector _dataConnectorMock;
        private IEntityServiceConverter<TestEntity> _dataConverterMock;
        private IEntityServiceExceptionService _exceptionService;

        [SetUp]
        public void SetUp()
        {
            ResetService();
        }

        private void ResetService()
        {
            _dataConnectorMock = MockRepository.GenerateMock<IEntityServiceConnector>();
            _dataConverterMock = MockRepository.GenerateMock<IEntityServiceConverter<TestEntity>>();
            _exceptionService = MockRepository.GenerateMock<IEntityServiceExceptionService>();
            _service = new EntityService<TestEntity>(_dataConnectorMock, _dataConverterMock, _exceptionService);
        }

        [Test]
        public void ShouldCallDataConnectorGetWithQueries()
        {
            // Arrange
            var testEntityKey = new TestEntityKey("MyKey");
            var queries = new[] { testEntityKey };
            var myTestEntities = new List<TestEntity>();
            _dataConnectorMock
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single().Equals(testEntityKey))))
                .Return(myTestEntities);

            _dataConverterMock
                 .Stub(x => x.ConvertFromQueriesToNativeFormat(queries, QueryConversionType.Fetching))
                 .Return(queries);

            _dataConverterMock
                .Stub(x => x.ConvertFromNativeFormatToEntities(myTestEntities))
                .Return(new List<TestEntity>());


            // Act
            _service.Get(queries);

            //Assert
            _dataConnectorMock.VerifyAllExpectations();
        }

        [Test]
        public void ShouldCallDataConverterWithResponseFromDataConnector()
        {
            // Arrange
            var testEntityKey = new TestEntityKey("MyKey");
            var queries = new[] { testEntityKey };
            var myTestEntities = new List<TestEntity>();
            var testEntities = new[] { new TestEntity() };

            _dataConnectorMock
                .Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single().Equals(testEntityKey))))
                .Return(myTestEntities);

            _dataConverterMock
                 .Expect(x => x.ConvertFromQueriesToNativeFormat(queries, QueryConversionType.Fetching))
                 .Return(queries);

            _dataConverterMock
                .Expect(x => x.ConvertFromNativeFormatToEntities(myTestEntities))
                .Return(testEntities);

            // Act
            _service.Get(queries);

            //Assert
            _dataConverterMock.VerifyAllExpectations();
        }

        [Test]
        public void ShouldReturnCollectionReturnedFromDataConverter()
        {
            // Arrange
            var testEntityKey = new TestEntityKey("MyKey");
            var queries = new[] { testEntityKey };
            var myTestEntities = new List<TestEntity>();
            var testEntities = new[] { new TestEntity() };

            _dataConnectorMock
                .Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single().Equals(testEntityKey))))
                .Return(myTestEntities);

            _dataConverterMock
                 .Expect(x => x.ConvertFromQueriesToNativeFormat(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single().Equals(testEntityKey)), Arg<QueryConversionType>.Is.Anything))
                 .Return(queries);

            _dataConverterMock
                .Expect(x => x.ConvertFromNativeFormatToEntities(myTestEntities))
                .Return(testEntities);

            // Act
            var output = _service.Get(queries);

            //Assert
            Assert.AreSame(testEntities.Single(), output.Single());
        }

        [Test]
        public void Publish_BasicCall_ShouldCallConvertBackOnDataConverter()
        {
            // Arrange
            var entity = new TestEntity("My Entity");

            _dataConverterMock
                .Expect(x => x.ConvertFromEntitiesToNativeFormat(new[] { entity }))
                .Return(new[] { new object() });

            // Act
            _service.Publish(new[] { entity });

            // Assert
            _dataConverterMock.VerifyAllExpectations();
        }

        [Test]
        public void PublishEntityShouldCallPublishOnDataConnector()
        {
            // Arrange
            var entity = new TestEntity();
            var convertedEntity = new TestEntity("ConvertedEntity");

            _dataConverterMock
                .Expect(x => x.ConvertFromEntitiesToNativeFormat(new[] { entity }))
                .Return(new[] { convertedEntity });

            _dataConnectorMock.Expect(x => x.Publish(new[] { convertedEntity }));

            // Act
            _service.Publish(new[] { entity });

            // Assert
            _dataConnectorMock.VerifyAllExpectations();
        }

        [Test]
        public void ShouldBeAbleToPublishADifferentEntityThatHasTheSameKey()
        {
            // Arrange
            var instance = new MyOtherTestEntity();
            _dataConverterMock
                .Expect(x => x.ConvertFromEntitiesToNativeFormat(new[] { instance }))
                .Return(new[] { instance });
            _dataConnectorMock.Expect(x => x.Publish(new[] { instance }));

            // Act
            _service.Publish(new[] { instance });

            // Assert
            _dataConverterMock.VerifyAllExpectations();
            _dataConnectorMock.VerifyAllExpectations();
        }

        [Test]
        public void Publish_LocalKeyIsLocalSetToFalse_ConvertsAndCallsPublishOnConnector()
        {
            // Arrange
            var key = new LocalTestEntityKey();
            var entity = new TestEntity(key);

            _dataConverterMock
                .Stub(x => x.ConvertFromEntitiesToNativeFormat(new[] { entity }))
                .Return(new[] { entity });
            _dataConnectorMock.Expect(x => x.Publish(new[] { entity }));

            // Act
            _service.Publish(new[] { entity });

            // Assert
            _dataConnectorMock.VerifyAllExpectations();
        }

        [Test]
        public void Publish_LocalKeyIsLocalSetToTrue_DoesNotCallConverterOnConverter()
        {
            // Arrange
            var key = new LocalTestEntityKey(isLocal: true);
            var entity = new TestEntity(key);

            _dataConnectorMock = MockRepository.GenerateStrictMock<IEntityServiceConnector>();
            var dataConverterMock = MockRepository.GenerateStrictMock<IEntityServiceConverter<TestEntity>>();
            var exceptionService = MockRepository.GenerateMock<IEntityServiceExceptionService>();
            var service = GetEntityService(_dataConnectorMock, dataConverterMock, exceptionService);

            // Act
            try
            {
                service.Publish(new[] { entity });
            }
            catch (ArgumentException)
            {
            }

            // Assert
            // Assert that no calls are made on the data provider...
            dataConverterMock.VerifyAllExpectations();
        }

        [Test]
        public void Publish_LocalKeyIsLocalSetToTrue_DoesNotPublish()
        {
            // Arrange
            var key = new LocalTestEntityKey(isLocal: true);
            var entity = new TestEntity(key);

            var dataConverterStub = MockRepository.GenerateStub<IEntityServiceConverter<TestEntity>>();
            var exceptionService = MockRepository.GenerateMock<IEntityServiceExceptionService>();
            var service = GetEntityService(_dataConnectorMock, dataConverterStub, exceptionService);

            var exceptionTester = new ExceptionTester();

            // Act
            exceptionTester.Act(() => service.Publish(new[] { entity }));

            // Assert
            dataConverterStub.AssertWasNotCalled(x => x.ConvertFromEntitiesToNativeFormat(Arg<IEnumerable<TestEntity>>.Is.Anything));
            _dataConnectorMock.AssertWasNotCalled(x => x.Publish(Arg<IEnumerable<object>>.Is.Anything));
        }

        [Test]
        public void GetObservable_BasicCall_CallsDataConnectorGetUpdates()
        {
            // Arrange
            var query1 = new TestEntityQuery("MyQuery1");
            var query2 = new TestEntityQuery("MyQuery2");
            var queries = new[] { query1, query2 };

            var subject = new Subject<IEnumerable<object>>();

            _dataConverterMock
                .Expect(x => x.ConvertFromQueriesToNativeFormat(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Contains(query1) && y.Contains(query2)), Arg<QueryConversionType>.Is.Anything))
                .Return(queries);

            _dataConnectorMock
                .Expect(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Contains(query1) && y.Contains(query2))))
                .Return(subject);

            // Act
            _service.GetObservable(queries).Subscribe(x => { });

            // Assert
            _dataConnectorMock.VerifyAllExpectations();
        }

        [Test]
        public void GetObservable_ExceptionThrownOnGetUpdates_BubblesException()
        {
            // Arrange
            var query1 = new TestEntityQuery("MyQuery1");
            var query2 = new TestEntityQuery("MyQuery2");
            var queries = new[] { query1, query2 };

            const string exceptionMessage = "TestException";
            var exception = new EntityServiceException(exceptionMessage);

            _dataConverterMock
                .Stub(x => x.ConvertFromQueriesToNativeFormat(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 2 && y.Contains(query1) && y.Contains(query2)), Arg<QueryConversionType>.Is.Anything))
                .Return(queries);

            _dataConnectorMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Contains(query1) && y.Contains(query2))))
                .Throw(exception);

            var exceptionTester = new ExceptionTester();

            // Act
            exceptionTester.Act(() => _service.GetObservable(queries).Subscribe(x => { }));

            // Assert
            exceptionTester.ShouldThrow<EntityServiceException>(exceptionMessage);
        }

        [Test]
        public void GetObservable_UpdateRaisedOnUnderlyingObservable_CallsDataConverterConvert()
        {
            // Arrange
            var query1 = new TestEntityQuery("MyQuery1");
            var query2 = new TestEntityQuery("MyQuery2");
            var queries = new[] { query1, query2 };

            var itemBeingRaised = new object();

            var subject = new Subject<IEnumerable<object>>();

            _dataConverterMock
                .Stub(x => x.ConvertFromQueriesToNativeFormat(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 2 && y.Contains(query1) && y.Contains(query2)), Arg<QueryConversionType>.Is.Anything))
                .Return(queries);

            _dataConnectorMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Contains(query1) && y.Contains(query2))))
                .Return(subject);

            _dataConverterMock
                .Expect(x => x.ConvertFromNativeFormatToEntities(new[] { itemBeingRaised }))
                .Return(new[] { new TestEntity() });

            _service.GetObservable(queries).Subscribe(x => { });

            // Act
            subject.OnNext(new[] { itemBeingRaised });

            // Assert
            _dataConverterMock.VerifyAllExpectations();
        }

        [Test]
        public void GetObservable_UpdateRaisedOnUnderlyingObservable_RaisesConvertedValue()
        {
            // Arrange
            var query1 = new TestEntityQuery("MyQuery1");
            var query2 = new TestEntityQuery("MyQuery2");
            var queries = new[] { query1, query2 };

            var itemBeingRaised = new object();
            var convertedValue = new TestEntity();

            var subject = new Subject<IEnumerable<object>>();

            _dataConverterMock
                .Stub(x => x.ConvertFromQueriesToNativeFormat(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 2 && y.Contains(query1) && y.Contains(query2)), Arg<QueryConversionType>.Is.Anything))
                .Return(queries);

            _dataConnectorMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Contains(query1) && y.Contains(query2))))
                .Return(subject);

            _dataConverterMock
                .Stub(x => x.ConvertFromNativeFormatToEntities(new[] { itemBeingRaised }))
                .Return(new[] { convertedValue });

            TestEntity raisedEntity = null;
            _service.GetObservable(queries).Select(resource => resource.Entities).Subscribe(x => { raisedEntity = x.FirstOrDefault(); });

            // Act
            subject.OnNext(new[] { itemBeingRaised });

            // Assert
            Assert.AreEqual(convertedValue, raisedEntity);
        }

        [Test]
        public void GetObservable_ErrorOnConvert_DoesntRaiseErrorOnError()
        {
            // Arrange
            var query1 = new TestEntityQuery("MyQuery1");
            var query2 = new TestEntityQuery("MyQuery2");
            var queries = new[] { query1, query2 };

            var itemBeingRaised = new object();

            var subject = new Subject<IEnumerable<object>>();

            _dataConnectorMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Contains(query1) && y.Contains(query2))))
                .Return(subject);

            _dataConverterMock
                .Stub(x => x.ConvertFromNativeFormatToEntities(new[] { itemBeingRaised }))
                .Throw(new Exception("This error is raised on error and it should be raised within an EntityResource on next"));

            Exception raisedException = null;
            _service.GetObservable(queries).Subscribe(x => { }, y => { raisedException = y; });

            // Act
            subject.OnNext(new[] { itemBeingRaised });

            // Assert
            Assert.IsNull(raisedException);
        }

        [Test]
        public void GetObservable_ErrorOnConvert_RaisesErrorOnExceptionService()
        {
            // Arrange
            var query = new TestEntityQuery("MyQuery1");
            var thrownException = new Exception("This error is raised on the Convert operation");

            var itemBeingRaised = new object();

            var subject = new Subject<IEnumerable<object>>();

            _dataConnectorMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Contains(query))))
                .Return(subject);

            _dataConverterMock
                .Stub(x => x.ConvertFromNativeFormatToEntities(new[] { itemBeingRaised }))
                .Throw(thrownException);

            _dataConverterMock
                .Stub(x => x.ConvertFromQueriesToNativeFormat(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(query)), Arg<QueryConversionType>.Is.Anything))
                .Return(new[] { query });

            _exceptionService.Expect(x => x.LogException(Arg<string>.Is.Anything, Arg<Exception>.Is.Anything));

            _service.GetObservable(query).Subscribe(x => { });

            // Act
            Run(() => subject.OnNext(new[] { itemBeingRaised }));

            // Assert
            _exceptionService.VerifyAllExpectations();
        }

        [Test]
        public void GetObservable_ErrorOnConvert_DoesNotRaiseAnythingOnNext()
        {
            // Arrange
            var query1 = new TestEntityQuery("MyQuery1");
            var query2 = new TestEntityQuery("MyQuery2");
            var queries = new[] { query1, query2 };
            var thrownException = new Exception("This error is raised on the Convert operation");

            var itemBeingRaised = new object();

            var subject = new Subject<IEnumerable<object>>();

            _dataConnectorMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Contains(query1) && y.Contains(query2))))
                .Return(subject);

            _dataConverterMock
                .Stub(x => x.ConvertFromNativeFormatToEntities(new[] { itemBeingRaised }))
                .Throw(thrownException);

            TestEntity raisedEntityResource = null;
            _service.GetObservable(queries).Select(resource => resource.Entities).Subscribe(x => { raisedEntityResource = x.FirstOrDefault(); });

            // Act
            subject.OnNext(new[] { itemBeingRaised });

            // Assert
            Assert.IsNull(raisedEntityResource);
        }

        [Test]
        public void GetObservable_ErrorRaisedOnUnderlyingObservable_PublishExceptionOnError()
        {
            // Arrange
            var query1 = new TestEntityQuery("MyQuery1");
            var query2 = new TestEntityQuery("MyQuery2");
            var queries = new[] { query1, query2 };

            var exceptionBeingRaised = new Exception();

            var subject = new Subject<IEnumerable<object>>();

            _dataConverterMock
                .Stub(x => x.ConvertFromQueriesToNativeFormat(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 2 && y.Contains(query1) && y.Contains(query2)), Arg<QueryConversionType>.Is.Anything))
                .Return(queries);

            _dataConnectorMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Contains(query1) && y.Contains(query2))))
                .Return(subject);

            Exception exceptionRaisedOnStream = null;
            _service.GetObservable(queries).Subscribe(x => { }, y => { exceptionRaisedOnStream = y; });

            // Act
            subject.OnError(exceptionBeingRaised);

            // Assert
            Assert.IsNotNull(exceptionRaisedOnStream);
            Assert.AreSame(exceptionBeingRaised, exceptionRaisedOnStream);
        }

        private static EntityService<TestEntity> GetEntityService(
            IEntityServiceConnector dataConnectorMock,
            IEntityServiceConverter<TestEntity> dataConverterMock,
            IEntityServiceExceptionService exceptionService)
        {
            return new EntityService<TestEntity>(dataConnectorMock, dataConverterMock, exceptionService);
        }
    }

    //public class LocalTestEntityKey : TestEntityKey, ILocalKey, IEquatable<LocalTestEntityKey>
    //{
    //    public LocalTestEntityKey() : base("MyLocalKey")
    //    {
    //    }

    //    public LocalTestEntityKey(string keyText) : base(keyText)
    //    {
    //    }

    //    public bool IsLocal { get; set; }

    //    public bool Equals(LocalTestEntityKey other)
    //    {
    //        if (other == null)
    //        {
    //            return false;
    //        }
    //        if (ReferenceEquals(this, other))
    //        {
    //            return true;
    //        }
    //        return IsLocal == other.IsLocal && base.Equals(other);
    //    }

    //    public override bool Equals(object obj)
    //    {
    //        return Equals(obj as LocalTestEntityKey);
    //    }

    //    public override int GetHashCode()
    //    {
    //        return IsLocal.GetHashCode() ^ 397 * base.GetHashCode();
    //    }

    //    public static bool operator ==(LocalTestEntityKey left, LocalTestEntityKey right)
    //    {
    //        return Equals(left, right);
    //    }

    //    public static bool operator !=(LocalTestEntityKey left, LocalTestEntityKey right)
    //    {
    //        return !Equals(left, right);
    //    }
    //}

    public class MyTestEntity : TestEntity
    {
        public MyTestEntity()
            : this("Test")
        {
        }

        public MyTestEntity(string key)
            : base(key)
        {
        }
    }

    public class MyOtherTestEntity : TestEntity
    {
        public MyOtherTestEntity()
            : this("Test")
        {
        }

        public MyOtherTestEntity(string key)
            : base(key)
        {
        }
    }
}
