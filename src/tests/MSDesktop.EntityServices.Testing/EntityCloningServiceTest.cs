﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using EntityServicesTests.Entities;
using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Cloning;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;
using NUnit.Framework;
using Rhino.Mocks;

namespace EntityServicesTests
{
    [TestFixture]
    public class EntityCloningServiceTest
    {
        private IEntityService<TestEntity> _entityServiceMock;
        private IEntityServiceExceptionService _exceptionServiceMock;
        private EntityCloningService<TestEntity> _entityCloningService;

        [SetUp]
        public void SetUp()
        {
            _entityServiceMock = MockRepository.GenerateMock<IEntityService<TestEntity>>();
            _exceptionServiceMock = MockRepository.GenerateMock<IEntityServiceExceptionService>();
            _entityCloningService = new EntityCloningService<TestEntity>(_entityServiceMock, new EntityCloning(), _exceptionServiceMock);
        }

        [Test]
        public void Get_BasicCall_CallsGetOnUnderlyingService()
        {
            // Arrange
            var key = new TestEntityKey("testKey");

            _entityServiceMock
                .Expect(x => x.Get(new[] {key}))
                .Return(Enumerable.Empty<TestEntity>());

            // Act
            _entityCloningService.Get(new[] {key});

            // Assert
            _entityServiceMock.AssertWasCalled(x => x.Get(new[] {key}));
        }

        [Test]
        public void Get_ValueReturnedFromUnderlyingService_ReturnsMatchingValue()
        {
            // Arrange
            var key = new TestEntityKey("testKey");
            var entity = new TestEntity(key);

            _entityServiceMock
                .Stub(x => x.Get(new[] {key}))
                .Return(new[] { entity });

            // Act
            var returnedEntities = _entityCloningService.Get(new[] {key});

            // Assert
            Assert.AreEqual(entity, returnedEntities.First());
        }

        [Test]
        public void Get_ValueReturnedFromUnderlyingService_ValueIsNotTheSameInstance()
        {
            // Arrange
            var key = new TestEntityKey("testKey");
            var entity = new TestEntity(key);

            _entityServiceMock
                .Stub(x => x.Get(new[] {key}))
                .Return(new[] { entity });

            // Act
            var returnedEntities = _entityCloningService.Get(new[] {key});

            // Assert
            Assert.AreNotSame(entity, returnedEntities.First());
        }

        [Test]
        public void Get_CloningFails_ThrowsException()
        {
            // Arrange
            var entityServiceMock = MockRepository.GenerateMock<IEntityService<UnserializableTestEntity>>();
            var exceptionServiceMock = MockRepository.GenerateMock<IEntityServiceExceptionService>();
            var entityCloningService = new EntityCloningService<UnserializableTestEntity>(entityServiceMock, new EntityCloning(), exceptionServiceMock);

            var key = new UnserializableTestEntityKey("testKey");
            var entity = new UnserializableTestEntity();

            entityServiceMock
                .Stub(x => x.Get(new[] {key}))
                .Return(new[] { entity });
            exceptionServiceMock.Expect(x => x.LogException(Arg<string>.Is.Anything, Arg<Exception>.Is.Anything));

            ExceptionTester tester = new ExceptionTester();

            // Act
            tester.Act(() => entityCloningService.Get(new[] {key}).ToList());

            // Assert
            tester.ShouldThrow<EntityServiceException>(string.Format("Error while performing a clone of entity '{0}'", typeof(UnserializableTestEntity).FullName));
        }


        [Test]
        public void GetObservable_BasicCall_CallsGetObservableonUnderlyingService()
        {
            // Arrange
            var key = new TestEntityKey("TestKey");

            _entityServiceMock
                .Expect(x => x.GetObservable(new[] { key }))
                .Return(new Subject<ObservableResource<TestEntity>>());

            // Act
            _entityCloningService.GetObservable(new[] { key });

            // Assert
            _entityServiceMock.AssertWasCalled(x => x.GetObservable(new[] { key }));
        }

        [Test]
        public void GetObservable_GetObservableReturnsStreamAndObjectRaised_MatchingObjectRaisedOnReturnedStream()
        {
            // Arrange
            var key = new TestEntityKey("TestKey");
            var entity = new TestEntity(key);

            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.GetObservable(new[] {key}))
                .Return(subject);

            TestEntity raisedEntity = null;
            _entityCloningService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x => { raisedEntity = x.FirstOrDefault(); });

            // Act
            subject.OnNext(new ObservableResource<TestEntity>(new[] {entity}));

            // Assert
            Assert.AreEqual(entity, raisedEntity);
        }

        [Test]
        public void GetObservable_GetObservableReturnsStreamAndObjectRaised_ObjectRaisedIsNotSameInstanceAsOneRaised()
        {
            // Arrange
            var key = new TestEntityKey("TestKey");
            var entity = new TestEntity(key);

            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.GetObservable(new[] {key}))
                .Return(subject);

            TestEntity raisedEntity = null;
            _entityCloningService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x => { raisedEntity = x.FirstOrDefault(); });

            // Act
            subject.OnNext(new ObservableResource<TestEntity>(new[] {entity}));

            // Assert
            Assert.AreNotSame(entity, raisedEntity);
        }

        [Test]
        public void GetObservable_CloningFails_CallsExceptionService()
        {
            // Arrange
            var entityServiceMock = MockRepository.GenerateMock<IEntityService<UnserializableTestEntity>>();
            var exceptionServiceMock = MockRepository.GenerateMock<IEntityServiceExceptionService>();
            var entityCloningService = new EntityCloningService<UnserializableTestEntity>(entityServiceMock, new EntityCloning(), exceptionServiceMock);

            var key = new UnserializableTestEntityKey("TestKey");
            var entity = new UnserializableTestEntity(key);

            var subject = new Subject<ObservableResource<UnserializableTestEntity>>();

            entityServiceMock
                .Stub(x => x.GetObservable( new[] {key}))
                .Return(subject);
            exceptionServiceMock.Expect(x => x.LogException(Arg<string>.Is.Anything, Arg<Exception>.Is.Anything));

            entityCloningService.GetObservable(key).Subscribe(x => { });

            // Act
            subject.OnNext(new ObservableResource<UnserializableTestEntity>(new[] { entity }));

            // Assert
            exceptionServiceMock.AssertWasCalled(x => x.LogException(Arg<String>.Is.Anything, Arg<Exception>.Is.Anything));
        }

        [Test]
        public void GetObservable_CloningFails_DoesntReturnAnything()
        {
            // Arrange
            var entityServiceMock = MockRepository.GenerateMock<IEntityService<UnserializableTestEntity>>();
            var exceptionServiceMock = MockRepository.GenerateMock<IEntityServiceExceptionService>();
            var entityCloningService = new EntityCloningService<UnserializableTestEntity>(entityServiceMock, new EntityCloning(), exceptionServiceMock);

            var key = new UnserializableTestEntityKey("TestKey");
            var entity = new UnserializableTestEntity(key);

            var subject = new Subject<ObservableResource<UnserializableTestEntity>>();

            entityServiceMock
                .Stub(x => x.GetObservable(new[] {key}))
                .Return(subject);

            bool isEntityRaised = false;
            entityCloningService.GetObservable(key).Subscribe(x => { isEntityRaised = true; });

            // Act
            subject.OnNext(new ObservableResource<UnserializableTestEntity>(new[] {entity}));

            // Assert
            Assert.IsFalse(isEntityRaised);
        }

        [Test]
        public void Publish_BasicCall_CallsUnderlyingService()
        {
            // Arrange
            var key = new TestEntityKey("TestKey");
            var entity = new TestEntity(key);

            _entityServiceMock.Expect(x => x.Publish(new[] {entity}));

            // Act
            _entityCloningService.Publish(new[] {entity});

            // Assert
            _entityServiceMock.AssertWasCalled(x => x.Publish(new[] {entity}));
        }

        [Test]
        public void Delete_BasicCall_CallsUnderlyingService()
        {
            // Arrange
            var key = new TestEntityKey("testkey");

            _entityServiceMock.Expect(x => x.Delete(new[] {key }));

            // Act
            _entityCloningService.Delete(new[] { key });

            // Assert
            _entityServiceMock.AssertWasCalled(x => x.Delete(new[] { key }));
        }
    }
}
