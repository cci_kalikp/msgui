﻿using System.Collections.Generic;
using System.Linq;
using MorganStanley.Desktop.EntityServices.Common.Cache;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.TestingCommon.Entities;
using NUnit.Framework;
using Rhino.Mocks;

namespace MorganStanley.Desktop.EntityServices.Testing
{
    [TestFixture]
    public class EntityStaticCachingServiceTest
    {
        private IEntityService<TestEntity> _entityService;
        private IEntityStaticCachingService<TestEntity> _cachingEntityService;

        [SetUp]
        public void SetUp()
        {
            _entityService = MockRepository.GenerateMock<IEntityService<TestEntity>>();
            ResetService();
        }

        private void ResetService()
        {
            _cachingEntityService = new EntityStaticCachingService<TestEntity>(_entityService);
        }

        [Test]
        public void ShouldGetEntityFromCache()
        {
            // Arrange 
            var key = new TestEntityKey("test");
            var testEntity = new TestEntity(key);

            // Get the entity in to the cache...
            _entityService
                .Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.First() == key)))
                .Return(new[] { testEntity }).Repeat.Once();
            _cachingEntityService.Get(key);

            // Act
            var result = _cachingEntityService.Get(key);

            // Assert
            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(testEntity, result.First());
        }

        [Test]
        public void WhenGettingEntityFromCacheShouldNotCallService()
        {
            // Arrange 
            _entityService = MockRepository.GenerateStrictMock<IEntityService<TestEntity>>();
            ResetService();

            var key = new TestEntityKey("test");
            var testEntity = new TestEntity(key);

            // Get the entity in to the cache...
            _entityService
                .Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.First() == key)))
                .Return(new[] { testEntity }).Repeat.Once(); // we only expect one call to this as it is required to get the item in to the cache but not to get the item on the second call
            _cachingEntityService.Get(key);

            // Act
            _cachingEntityService.Get(key);

            // Assert
            _entityService.VerifyAllExpectations();
        }

        [Test]
        public void ShouldCallBaseServiceWhenEntityNotInCache()
        {
            // Arrange
            var key = new TestEntityKey("test");
            var testEntity = new TestEntity(key);

            _entityService
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single() == key)))
                .Return(new[] { testEntity });

            // Act
            _cachingEntityService.Get(key);

            // Assert
            _entityService.VerifyAllExpectations();
        }

        [Test]
        public void ShouldReturnValueFromServiceWhenEntityNotInCache()
        {
            // Arrange
            var key = new TestEntityKey("test");
            var testEntity = new TestEntity(key);

            _entityService
                .Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single() == key)))
                .Return(new[] { testEntity });

            // Act
            var result = _cachingEntityService.Get(key);

            // Assert
            Assert.AreEqual(1, result.Count());
            Assert.AreSame(testEntity, result.Single());
        }

        [Test]
        public void ShouldCallBaseServiceWhenEntityInCacheAndForceFetchEnabled()
        {
            // Arrange
            var key = new TestEntityKey("test") { ForceFetch = true };
            var testEntity = new TestEntity(key);
            var cacheObject = new CacheObject<TestEntity>();
            cacheObject.SetValue(() => testEntity);

            _entityService
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single() == key)))
                .Return(new[] { testEntity });

            // Act
            _cachingEntityService.Get(key);

            // Assert
            _entityService.VerifyAllExpectations();
        }

        [Test]
        public void MultiQueryGet_MultipleResultsReturns_CallsGetOnSingleQuery()
        {
            // The code does this as it needs to keep the cache updated correctly but it cannot connect the 
            // entities returned with a single query (as there are multiple queries and multiple entities) 

            // Arrange
            var testEntityQuery1 = new TestEntityQuery("query1");
            var testEntityQuery2 = new TestEntityQuery("query2");
            var testEntityQueries = new[] { testEntityQuery1, testEntityQuery2 };

            var testEntity1 = new TestEntity("entity1");
            var testEntity2 = new TestEntity("entity1");
            var testEntities = new[] { testEntity1, testEntity2 };

            _entityService
                .Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => CollectionsMatch(y, testEntityQueries))))
                .Return(testEntities);

            _entityService
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => CollectionsMatch(y, new[] { testEntityQuery1 }))))
                .Return(new[] { testEntity1 });
            _entityService
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => CollectionsMatch(y, new[] { testEntityQuery2 }))))
                .Return(new[] { testEntity2 });

            // Act
            _cachingEntityService.Get(testEntityQueries);

            // Assert
            _entityService.VerifyAllExpectations();
        }

        [Test]
        public void MultiKeyGet_NoResultsReturns_ReturnsEmptyList()
        {
            // We are mainly checking here that an exception doesnt get thrown when no results are returned and hence 
            // there is nothing to match the keys up to in the cache

            // Arrange
            var testEntityQuery1 = new TestEntityKey("query1");
            var testEntityQuery2 = new TestEntityKey("query2");
            var testEntityQueries = new[] { testEntityQuery1, testEntityQuery2 };

            var testEntities = new TestEntity[0];

            _entityService
                .Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => CollectionsMatch(y, testEntityQueries))))
                .Return(testEntities);

            // Act
            var results = _cachingEntityService.Get(testEntityQueries);

            // Assert
            Assert.AreEqual(0, results.Count());
        }

        [Test]
        public void Get_NoItemInCache_SubsequentCallsShouldReceiveSameInstances()
        {
            // We need this test as we are currently implementing "copy on read"

            // Arrange
            var key = new TestEntityKey("My Key");
            var testEntity = new TestEntity(key);

            _entityService
                .Stub(x => x.Get(key))
                .Return(new[] { testEntity });

            // This gets it in to the cache...
            var initiallyRetrievedObject = _cachingEntityService.Get(key).First();

            // Act
            var cachedObject = _cachingEntityService.Get(key).First();

            // Assert
            Assert.AreSame(initiallyRetrievedObject, cachedObject);
        }

        [Test]
        public void Get_ItemInCache_SubsequentCallsShouldReceiveSameInstances()
        {
            // We need this test as we are currently implementing "copy on read"

            // Arrange
            var key = new TestEntityKey("My Key");
            var testEntity = new TestEntity(key);

            _entityService
                .Stub(x => x.Get(key))
                .Return(new[] { testEntity });

            // This gets it in to the cache...
            _cachingEntityService.Get(key);

            var firstCacheRetrievedObject = _cachingEntityService.Get(key).First();

            // Act
            var secondCacheRetrievedObject = _cachingEntityService.Get(key).First();

            // Assert
            Assert.AreSame(firstCacheRetrievedObject, secondCacheRetrievedObject);
        }

        [Test]
        public void Get_SingleQueryAndNoItemInCache_AllResultsShouldBeConnectedToTheSingleQuery()
        {
            // Arrange
            _entityService = MockRepository.GenerateStrictMock<IEntityService<TestEntity>>();
            ResetService();

            var testEntityQuery = new TestEntityQuery("My Query");
            var testEntity1 = new TestEntity("testEntity1");
            var testEntity2 = new TestEntity("testEntity2");
            var entitiesFromService = new List<TestEntity> { testEntity1, testEntity2 };

            _entityService
                .Stub(x => x.Get(testEntityQuery))
                .Return(entitiesFromService).Repeat.Once();

            // This should get the items in to the cache...
            _cachingEntityService.Get(testEntityQuery);

            // Act
            // This call should not call the underlying entity service as the results should already be in the cache...
            _cachingEntityService.Get(testEntityQuery);

            // Assert
            _entityService.VerifyAllExpectations();
        }

        private static bool CollectionsMatch<TQuery>(IEnumerable<TQuery> collection1, IEnumerable<TQuery> collection2)
        {
            if (collection1.Count() != collection2.Count())
            {
                return false;
            }
            if (collection1.Any(item => !collection2.Contains(item)))
            {
                return false;
            }
            if (collection2.Any(item => !collection1.Contains(item)))
            {
                return false;
            }
            return true;
        }
    }
}
