﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Reflection;
using EntityServicesTests.Entities;
using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;
using NUnit.Framework;
using Rhino.Mocks;

namespace EntityServicesTests
{
    [TestFixture]
    public class EntityLocalServiceTests
    {
        private EntityLocalService<TestEntity> _entityLocalService;
        private IEntityService<TestEntity> _entityServiceMock;
        private IEntityServiceExceptionService _exceptionServiceMock;

        [SetUp]
        public void SetUp()
        {
            _entityServiceMock = MockRepository.GenerateMock<IEntityService<TestEntity>>();
            ResetService();
        }

        private void ResetService()
        {
            _exceptionServiceMock = MockRepository.GenerateMock<IEntityServiceExceptionService>();
            _entityLocalService = new EntityLocalService<TestEntity>(
                _entityServiceMock, 
                _exceptionServiceMock);
        }

        [Test]
        public void Get_NoElementInCache_CallsEntityService()
        {
            // Arrange
            var query = new NonLocalTestEntityQuery();
            _entityServiceMock
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                    y => y.Count() == 1 && y.First().Equals(query))))
                .Return(new List<TestEntity>());

            // Act
            _entityLocalService.Get(new[] {query});

            // Assert
            _entityServiceMock.VerifyAllExpectations();
        }

        [Test]
        public void Get_NoElementInCache_ReturnsResultsFromEntityService()
        {
            // Arrange
            var query = new NonLocalTestEntityQuery();
            var entityFromService = new TestEntity(new LocalTestEntityKey("Test1"));
            var resultsFromService = new List<TestEntity> { entityFromService };

            _entityServiceMock
                .Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                    y => y.Count() == 1 && y.First().Equals(query))))
                .Return(resultsFromService);

            // Act
            var results = _entityLocalService.Get(new[] {query});

            // Assert
            Assert.AreEqual(1, results.Count());
            Assert.AreEqual(entityFromService, results.First());
        }

        [Test]
        public void Get_MatchingElementInCache_DoesntCallEntityService()
        {
            // Arrange

            // Set the entityService mock to strict settings so that we can pick up unexpected calls
            _entityServiceMock = MockRepository.GenerateStrictMock<IEntityService<TestEntity>>();
            ResetService();

            const string keyString = "Test1";
            var publishKey = new LocalTestEntityKey(keyString, true);
            var queryKey = new LocalTestEntityKey(keyString, true);
            var entity = new TestEntity(publishKey);

            // Get it in to the cache
            _entityLocalService.Publish(new[] {entity});

            // Act
            _entityLocalService.Get(new[] {queryKey});

            // Assert
            _entityServiceMock.VerifyAllExpectations();
        }

        [Test]
        public void Get_MatchingElementInCache_ReturnsElementFromCache()
        {
            // Arrange
            var key = new LocalTestEntityKey("Test1", true);
            var entity = new TestEntity(key);

            // Get it in to the cache
            _entityLocalService.Publish(new[] {entity});

            // Act
            var results = _entityLocalService.Get(new[] {key});

            // Assert
            Assert.AreEqual(1, results.Count());
            Assert.AreEqual(entity, results.First());
        }

        [Test]
        public void Get_MatchingElementInCacheButForceFetchKeyUsed_CallsEntityService()
        {
            // Arrange
            const string keyString = "Test1";
            var publishKey = new LocalTestEntityKey(keyString, true);
            var forceFetchKey = new ForceFetchTestEntityKey(key: keyString, options: CachingOptions.RetrieveFromSource);
            var entity = new TestEntity(publishKey);

            // Get it in to the cache
            _entityLocalService.Publish(new[] {entity});

            _entityServiceMock
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                    y => y.Count() == 1 && y.First().Equals(forceFetchKey))))
                .Return(new List<TestEntity>());

            // Act
            _entityLocalService.Get(new[] {forceFetchKey});

            // Assert
            _entityServiceMock.VerifyAllExpectations();
        }

        [Test]
        public void Get_MatchingElementInCacheButForceFetchKeyUsed_ReturnsValueFromEntityService()
        {
            // Arrange
            const string keyString = "Test1";
            var publishKey = new LocalTestEntityKey(keyString, true);
            var forceFetchKey = new ForceFetchTestEntityKey(key: keyString, options: CachingOptions.RetrieveFromSource);
            var entity = new TestEntity(publishKey);
            var entityReturnedFromService = new TestEntity(publishKey);
            var entitiesReturnedFromService = new List<TestEntity> { entityReturnedFromService };

            // Get it in to the cache
            _entityLocalService.Publish(new[] {entity});

            _entityServiceMock
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                    y => y.Count() == 1 && y.First().Equals(forceFetchKey))))
                .Return(entitiesReturnedFromService);

            // Act
            var results = _entityLocalService.Get(new[] {forceFetchKey});

            // Assert
            Assert.AreEqual(1, results.Count());
            Assert.AreSame(entityReturnedFromService, results.First());
        }

        [Test]
        public void Publish_PublishLocal_DoesNotCallEntityService()
        {
            // Arrange
            var key = new LocalTestEntityKey(isLocal: true);
            var entity = new TestEntity(key);

            _entityServiceMock = MockRepository.GenerateStrictMock<IEntityService<TestEntity>>();
            ResetService();

            // Act
            _entityLocalService.Publish(new[] {entity});

            // Assert
            // Verify that no calls are made on the service
            _entityServiceMock.VerifyAllExpectations();
        }

        [Test]
        public void Publish_PublishGlobal_CallsEntityServicePublish()
        {
            // Arrange
            var key = new LocalTestEntityKey();
            var entity = new TestEntity(key);

            _entityServiceMock.Expect(x => x.Publish(new[] {entity}));

            // Act
            _entityLocalService.Publish(new[] {entity});

            // Assert
            _entityServiceMock.VerifyAllExpectations();
        }

        [Test]
        public void Publish_PublishLocal_UpdatesLocalCache()
        {
            // Arrange
            var key = new LocalTestEntityKey(isLocal: true);
            var localValue = new TestEntity(key);

            // Act
            _entityLocalService.Publish(new[] {localValue});

            // Assert
            var returnedEntity = _entityLocalService.Get(new[] {key}).Single();
            Assert.AreEqual(localValue, returnedEntity);
        }

        [Test]
        public void Publish_PublishObjectIsDifferentFromEntityObject_ThrowException()
        {
            // This is one of the main assumptions we needed to make to write the entity local service.  
            // The published entity MUST be the same type as the retrieved entity.

            // Arrange
            var key = new LocalTestEntityKey(isLocal: true);
            var publishEntity = new DifferentPublishEntityType(key);

            var exceptionTester = new ExceptionTester();

            // Act
            exceptionTester.Act(() => _entityLocalService.Publish(new[] {publishEntity}));

            // Assert
            exceptionTester.ShouldThrow<InvalidOperationException>(string.Format("Published entity must be of type {0} and cannot be null", typeof(TestEntity).FullName));
        }

        class DifferentPublishEntityType : IEntity<TestEntity, BasicKey<TestEntity>>, IPublishable
        {
            private readonly BasicKey<TestEntity> _key;

            public DifferentPublishEntityType(BasicKey<TestEntity> key)
            {
                _key = key;
            }

            IKey IEntity.Key
            {
                get { return _key; }
            }

            BasicKey<TestEntity> IEntity<TestEntity, BasicKey<TestEntity>>.Key
            {
                get { return _key; }
            }
        }

        [Test]
        public void GetObservable_BasicCall_CallsGetObservableOnUnderlyingService()
        {
            // Arrange
            var key = new LocalTestEntityKey("MyKey");

            _entityServiceMock
                .Expect(
                    x =>
                    x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.First().Equals(key))))
                .Return(new Subject<ObservableResource<TestEntity>>());

            // Act
            _entityLocalService.GetObservable(new[] {key}).Subscribe(x => { });

            // Assert
            _entityServiceMock.VerifyAllExpectations();
        }

        [Test]
        public void GetObservable_UpdateOnUnderlyingService_RaisesUpdate()
        {
            // Arrange
            var key = new LocalTestEntityKey("MyKey");
            var entity = new TestEntity(key);

            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.First().Equals(key))))
                .Return(subject);

            TestEntity entityRaised = null;
            _entityLocalService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x => { entityRaised = x.FirstOrDefault(); });

            // Act
            subject.OnNext(new ObservableResource<TestEntity>(new[] {entity}));

            // Assert
            Assert.AreEqual(entity, entityRaised);
        }

        [Test]
        public void GetObservable_ExceptionOnUnderlyingService_RaisesException()
        {
            // Arrange
            var key = new LocalTestEntityKey("MyKey");
            var exception = new Exception();

            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.First().Equals(key))))
                .Return(subject);

            Exception exceptionRaised = null;
            _entityLocalService.GetObservable(new[] {key}).Subscribe(
                    x => { },
                    y => { exceptionRaised = y; });

            // Act
            subject.OnError(exception);

            // Assert
            Assert.IsNotNull(exceptionRaised);
            Assert.AreEqual(exception, exceptionRaised);
        }

        [Test]
        public void GetObservable_PublishLocal_RaisesUpdate()
        {
            // Arrange
            var key = new LocalTestEntityKey("MyKey", true);
            var publishedEntity = new TestEntity(key);

            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.First().Equals(key))))
                .Return(subject);

            TestEntity entityRaised = null;
            _entityLocalService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x => { entityRaised = x.FirstOrDefault(); });

            // Act
            _entityLocalService.Publish(new[] {publishedEntity});

            // Assert
            Assert.AreEqual(publishedEntity, entityRaised);
        }

        [Test]
        public void GetObservable_PublishGlobal_DoesNotRaiseUpdate()
        {
            // Arrange
            var key = new LocalTestEntityKey("MyKey");
            var publishedEntity = new TestEntity(key);

            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.GetObservable(new[] {key}))
                .Return(subject);

            TestEntity entityRaised = null;
            _entityLocalService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x => { entityRaised = x.FirstOrDefault(); });

            // Act
            _entityLocalService.Publish(new[] {publishedEntity});

            // Assert
            Assert.IsNull(entityRaised);
        }

        [Test]
        public void GetObservable_NullCacheService_ShouldNotThrowExceptionOnNext()
        {
            // Arrange
            var key = new LocalTestEntityKey("MyKey");
            var subject = new Subject<ObservableResource<TestEntity>>();

            var exceptionServiceStub = MockRepository.GenerateStub<IEntityServiceExceptionService>();
            _entityLocalService = GetEntityLocalService(_entityServiceMock, exceptionServiceStub);

            _entityServiceMock
                .Stub(x => x.GetObservable(new[] {key}))
                .Return(subject);

            _entityLocalService.GetObservable(new[] {key}).Subscribe(x => { });

            // Act
            subject.OnNext(new ObservableResource<TestEntity>(new[] {new TestEntity()}));

            // Assert
            Assert.Pass("No Exception was thrown on next");
        }

        [Test]
        public void GetObservable_NullCacheService_ShouldNotRaiseErrorInOnError()
        {
            // Arrange
            var key = new LocalTestEntityKey("MyKey");
            var subject = new Subject<ObservableResource<TestEntity>>();

            var exceptionServiceStub = MockRepository.GenerateStub<IEntityServiceExceptionService>();
            _entityLocalService = GetEntityLocalService(_entityServiceMock, exceptionServiceStub);

            _entityServiceMock
                .Stub(x => x.GetObservable(new[] {key}))
                .Return(subject);

            Exception error = null;
            _entityLocalService.GetObservable(new[] {key}).Subscribe(x => { }, y => { error = y; });

            // Act
            subject.OnNext(new ObservableResource<TestEntity>(new[] {new TestEntity()}));

            // Assert
            Assert.IsNull(error);
        }

        [Test]
        public void GetObservable_ErrorOnSubject_ShouldNotRaiseErrorInOnError()
        {
            // Arrange
            var key = new LocalTestEntityKey("MyKey", true);

            Exception raisedError = null;
            _entityLocalService.GetObservable(new[] {key}).Subscribe(x => { }, e => { raisedError = e; });

            var subjectFromService =
                _entityLocalService
                    .GetType()
                    .GetField("_subject", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(_entityLocalService)
                        as ISubject<IEnumerable<TestEntity>>;

            // Act
            subjectFromService.OnNext(null);

            // Assert
            Assert.IsNull(raisedError);
        }

        [Test]
        public void GetObservable_ErrorOnSubject_ShouldRaiseErrorOnExceptionService()
        {
            // Arrange
            var key = new LocalTestEntityKey("MyKey", true);

            _exceptionServiceMock
                .Expect(x => x.LogException(
                    Arg<string>.Is.Anything,
                    Arg<NullReferenceException>.Is.Anything));

            var subjectFromService =
                _entityLocalService
                    .GetType()
                    .GetField("_subject", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(_entityLocalService)
                        as ISubject<IEnumerable<TestEntity>>;

            _entityLocalService.GetObservable(key).Subscribe(x => { });

            // Act
            subjectFromService.OnNext(null);

            // Assert
            _exceptionServiceMock.VerifyAllExpectations();
        }

        [Test]
        public void GetObservable_ErrorOnSubject_ShouldNotRaiseAnythingOnNext()
        {
            // Arrange
            var key = new LocalTestEntityKey("MyKey");
            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                    y => y.Count() == 1 && y.First().Equals(key))))
                .Return(subject);

            TestEntity raisedEntity = null;
            _entityLocalService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x => { raisedEntity = x.FirstOrDefault(); });

            var subjectFromService =
                _entityLocalService
                    .GetType()
                    .GetField("_subject", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(_entityLocalService)
                        as ISubject<IEnumerable<TestEntity>>;

            // Act
            subjectFromService.OnNext(null);

            // Assert
            Assert.IsNull(raisedEntity);
        }

        private static EntityLocalService<TestEntity> GetEntityLocalService(
            IEntityService<TestEntity> entityService,
            IEntityServiceExceptionService exceptionServiceMock)
        {
            return new EntityLocalService<TestEntity>(
                entityService,
                exceptionServiceMock);
        }
    }

    public class ForceFetchTestEntityKey : IQuery<TestEntity>, IEquatable<ForceFetchTestEntityKey>, IWithOptionalCaching
    {
        private readonly CachingOptions _options;
        private readonly WhenCaching _whenCaching;
        private readonly TimeSpan? _timeout;
        private readonly int? _resultsStoredTogetherThreshold;
        private readonly string _key;


        public ForceFetchTestEntityKey(string key = "IdForceFetch1", CachingOptions options = CachingOptions.Default, WhenCaching whenCaching = WhenCaching.DoDefault, TimeSpan? timeout = null, int? resultsStoredTogetherThreshold = null)
        {
            _options = options;
            _whenCaching = whenCaching;
            _timeout = timeout;
            _resultsStoredTogetherThreshold = resultsStoredTogetherThreshold;
            _key = key;
        }

        public bool Equals(IQuery<TestEntity> other)
        {
            return Equals(other as ForceFetchTestEntityKey);
        }

        public bool Equals(IQuery other)
        {
            return Equals(other as ForceFetchTestEntityKey);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as ForceFetchTestEntityKey);
        }

        public bool Equals(ForceFetchTestEntityKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Key, Key);
        }

        public override int GetHashCode()
        {
            return (Key != null ? Key.GetHashCode() : 0);
        }

        public static bool operator ==(ForceFetchTestEntityKey left, ForceFetchTestEntityKey right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ForceFetchTestEntityKey left, ForceFetchTestEntityKey right)
        {
            return !Equals(left, right);
        }

        public string Key { get { return _key; } }
        public TimeSpan? Timeout { get { return _timeout; } }
        public int? ResultsStoredTogetherThreshold { get { return _resultsStoredTogetherThreshold; } }
        public CachingOptions CachingOptions { get { return _options; } }
        public WhenCaching WhenCaching { get { return _whenCaching; } }
    }

    public class NonLocalTestEntityQuery : IQuery<TestEntity>, IEquatable<NonLocalTestEntityQuery>
    {
        private readonly string _key;

        public NonLocalTestEntityQuery(string key = "IdNonLocal1")
        {
            _key = key;
        }

        public string Key { get { return _key; } }


        public bool Equals(IQuery<TestEntity> other)
        {
            return Equals(other as NonLocalTestEntityQuery);
        }

        public bool Equals(IQuery other)
        {
            return Equals(other as NonLocalTestEntityQuery);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as NonLocalTestEntityQuery);
        }

        public bool Equals(NonLocalTestEntityQuery other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Key, Key);
        }

        public override int GetHashCode()
        {
            return (Key != null ? Key.GetHashCode() : 0);
        }

        public static bool operator ==(NonLocalTestEntityQuery left, NonLocalTestEntityQuery right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(NonLocalTestEntityQuery left, NonLocalTestEntityQuery right)
        {
            return !Equals(left, right);
        }
    }

    [Serializable]
    public class LocalTestEntityKey :
        BasicKey<TestEntity>,
        IEquatable<LocalTestEntityKey>,
        ILocalable
    {
        private readonly bool _isLocal;

        public LocalTestEntityKey(string key = "DefaultKey", bool isLocal = false)
            : base(key)
        {
            _isLocal = isLocal;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as LocalTestEntityKey);
        }


        public bool IsLocal { get { return _isLocal; } }

        public bool Equals(LocalTestEntityKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.ToString(), ToString());
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public static bool operator ==(LocalTestEntityKey left, LocalTestEntityKey right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(LocalTestEntityKey left, LocalTestEntityKey right)
        {
            return !Equals(left, right);
        }
    }
}
