﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using EntityServicesTests.Entities;
using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;
using NUnit.Framework;
using Rhino.Mocks;

namespace EntityServicesTests
{
    [TestFixture]
    public class EntityReplayServiceTests
    {
        private EntityReplayService<TestEntity> _entityReplayService;
        private IEntityService<TestEntity> _entityServiceMock;
        private IEntityServiceExceptionService _exceptionService;

        [SetUp]
        public void SetUp()
        {
            _entityServiceMock = MockRepository.GenerateStrictMock<IEntityService<TestEntity>>();
            _exceptionService = MockRepository.GenerateStrictMock<IEntityServiceExceptionService>();
            _entityReplayService = new EntityReplayService<TestEntity>(_entityServiceMock, _exceptionService);
        }

        [Test]
        public void Get_BasicCall_CallsGetOnUnderlyingService()
        {
            // Arrange
            var key = new TestEntityKey("Test Key");
            var entity = new TestEntity(key);

            _entityServiceMock
                .Expect(x => x.Get(new[] {key}))
                .Return(new[] { entity });

            // Act
            _entityReplayService.Get(new[] {key});

            // Assert
            _entityServiceMock.AssertWasCalled(x => x.Get(new[] {key}));
        }

        [Test]
        public void Get_BasicCall_ReturnsValueFromUnderlyingService()
        {
            // Arrange
            var key = new TestEntityKey("Test Key");
            var entity = new TestEntity(key);
            var entities = new[] { entity };

            _entityServiceMock
                .Stub(x => x.Get(new[] {key}))
                .Return(entities);

            // Act
            var testEntities = _entityReplayService.Get(new[] {key});

            // Assert
            Assert.AreSame(entities, testEntities);
        }

        [Test]
        public void GetObservable_BasicCall_CallsGetObservableOnUnderlyingService()
        {
            // Arrange
            var key = new TestEntityKey("Test Key");

            _entityServiceMock
                .Expect(x => x.GetObservable(
                    Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                        queryList => queryList.Count() == 1 && queryList.First().Equals(key))))
                .Return(new Subject<ObservableResource<TestEntity>>());

            // Act
            _entityReplayService.GetObservable(new[] {key}).Subscribe(x => {});

            // Assert
            _entityServiceMock
                .AssertWasCalled(x => x.GetObservable(
                    Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                        queryList => queryList.Count() == 1 && queryList.First().Equals(key))));
        }

        [Test]
        public void GetObservable_CallMadeOnEntityServiceObservable_MakesCallOnReturnedObservable()
        {
            // Arrange
            var key = new TestEntityKey("Test Key");
            var entity = new TestEntity(key);

            var stream = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.GetObservable(
                    Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                        queryList => queryList.Count() == 1 && queryList.First().Equals(key))))
                .Return(stream);

            TestEntity entityRaised = null;
            _entityReplayService
                .GetObservable(new[] {key})
                .Select(resource => resource.Entities)
                .Subscribe(x => { entityRaised = x.FirstOrDefault(); });

            // Act
            stream.OnNext(new ObservableResource<TestEntity>(new[] { entity }));

            // Assert
            Assert.AreSame(entity, entityRaised);
        }

        [Test]
        public void Publish_BasicCall_CallsPublishOnUnderlyingService()
        {
            // Arrange
            var key = new TestEntityKey("Test Key");
            var entity = new TestEntity(key);

            _entityServiceMock.Expect(x => x.Publish(new[] {entity}));

            // Act
            _entityReplayService.Publish(new[] {entity});

            // Assert
            _entityServiceMock.AssertWasCalled(x => x.Publish(new[] {entity}));
        }

        [Test]
        public void Replay_GetPreviouslyCalledOnce_CallsUnderlyingServiceWithOriginalGetQuery()
        {
            // Arrange
            var query = new TestEntityKey("Test Key");
            var entity = new TestEntity(query);

            _entityServiceMock
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                    queryList => queryList.Count() == 1 && queryList.First() == query)))
                .Return(new[] { entity })
                .Repeat.Twice();

            _entityServiceMock
                .Stub(x => x.GetObservable(
                    Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                        queryList => queryList.Count() == 1 && queryList.First().Equals(query))))
                .Return(new Subject<ObservableResource<TestEntity>>());

            _entityReplayService.Get(new[] {query});
            _entityReplayService.GetObservable(new[] {query}).Subscribe(x => { });

            // Act
            _entityReplayService.Replay();

            // Assert
            _entityServiceMock
                .AssertWasCalled(
                    x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                        queryList => queryList.Count() == 1 && queryList.First() == query)),
                    options => options.Repeat.Twice());
        }

        [Test]
        public void Replay_GetPreviouslyCalledTwice_CallsUnderlyingServiceWithBothOriginalGetQueries()
        {
            // Arrange
            var query1 = new TestEntityKey("Test Key 1");
            var query2 = new TestEntityKey("Test Key 2");
            var entity1 = new TestEntity(query1);
            var entity2 = new TestEntity(query2);

            _entityServiceMock
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                    queryList => queryList.Count() == 1 && queryList.First() == query1)))
                .Return(new[] { entity1 })
                .Repeat.Twice();
            _entityServiceMock
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                    queryList => queryList.Count() == 1 && queryList.First() == query2)))
                .Return(new[] { entity2 })
                .Repeat.Twice();

            _entityServiceMock
                .Stub(x => x.GetObservable(
                    Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                        queryList => queryList.Count() == 1 && queryList.First().Equals(query1))))
                .Return(new Subject<ObservableResource<TestEntity>>());
            _entityServiceMock
                .Stub(x => x.GetObservable(
                    Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                        queryList => queryList.Count() == 1 && queryList.First().Equals(query2))))
                .Return(new Subject<ObservableResource<TestEntity>>());

            _entityReplayService.Get(new[] {query1});
            _entityReplayService.Get(new[] {query2});
            _entityReplayService.GetObservable(new[] {query1}).Subscribe(x => { });
            _entityReplayService.GetObservable(new[] {query2}).Subscribe(x => { });

            // Act
            _entityReplayService.Replay();

            // Assert
            _entityServiceMock
                .AssertWasCalled(
                    x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                        queryList => queryList.Count() == 1 && queryList.First() == query1)),
                    options => options.Repeat.Twice());
            _entityServiceMock
                .AssertWasCalled(
                    x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                        queryList => queryList.Count() == 1 && queryList.First() == query2)),
                    options => options.Repeat.Twice());
        }

        [Test]
        public void Replay_GetPreviouslyCalledTwiceWithSameKey_CallsUnderlyingServiceOnce()
        {
            // Arrange
            var query1 = new TestEntityKey("Test Key 1");
            var entity1 = new TestEntity(query1);

            _entityServiceMock
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                    queryList => queryList.Count() == 1 && queryList.First() == query1)))
                .Return(new[] { entity1 })
                .Repeat.Times(3); // Once for each get and ONLY ONCE for the replay

            _entityServiceMock
                .Stub(x => x.GetObservable(
                    Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                        queryList => queryList.Count() == 1 && queryList.First().Equals(query1))))
                .Return(new Subject<ObservableResource<TestEntity>>());

            _entityReplayService.Get(new[] {query1});
            _entityReplayService.Get(new[] {query1});
            _entityReplayService.GetObservable(new[] {query1}).Subscribe(x => { });

            // Act
            _entityReplayService.Replay();

            // Assert
            _entityServiceMock
                .AssertWasCalled(
                    x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                        queryList => queryList.Count() == 1 && queryList.First() == query1)),
                    options => options.Repeat.Times(3)); // Once for each get and ONLY ONCE for the replay
        }

        [Test]
        public void Replay_GetPreviouslyCalledNotObservedOn_DoesNotCallUnderlyingServiceGetASecondTime()
        {
            // Arrange
            var query1 = new TestEntityKey("Test Key 1");
            var query2 = new TestEntityKey("Test Key 2");
            var entity1 = new TestEntity(query1);
            var entity2 = new TestEntity(query2);

            _entityServiceMock
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                    queryList => queryList.Count() == 1 && queryList.First() == query1)))
                .Return(new[] { entity1 })
                .Repeat.Twice();
            _entityServiceMock
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                    queryList => queryList.Count() == 1 && queryList.First() == query2)))
                .Return(new[] { entity2 })
                .Repeat.Once();

            _entityServiceMock
                .Stub(x => x.GetObservable(
                    Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                        queryList => queryList.Count() == 1 && queryList.First().Equals(query1))))
                .Return(new Subject<ObservableResource<TestEntity>>());

            _entityReplayService.Get(new[] {query1});
            _entityReplayService.Get(new[] {query2});
            _entityReplayService.GetObservable(new[] {query1}).Subscribe(x => { });

            // Act
            _entityReplayService.Replay();

            // Assert
            _entityServiceMock
                .AssertWasCalled(
                    x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                        queryList => queryList.Count() == 1 && queryList.First() == query1)),
                    options => options.Repeat.Twice());
            _entityServiceMock
                .AssertWasCalled(
                    x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                        queryList => queryList.Count() == 1 && queryList.First() == query2)),
                    options => options.Repeat.Once());
        }

        [Test]
        public void Replay_GetObservablePreviouslyCalledOnce_CallsUnderlyingServiceWithKeysFromlastObservation()
        {
            // Arrange
            var query = new TestEntityKey("Test Key");
            var entity = new TestEntity(query);
            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                    queryList => queryList.Single() == query)))
                .Return(new[] { entity })
                .Repeat.Once();

            _entityServiceMock
                .Stub(x => x.GetObservable(
                    Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                        queryList => queryList.Count() == 1 && queryList.First().Equals(query))))
                .Return(subject);

            _entityReplayService.GetObservable(new[] {query}).Subscribe(x => { });

            subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));

            // Act

            _entityReplayService.Replay();

            // Assert
            _entityServiceMock
                .AssertWasCalled(
                    x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
                        queryList => queryList.Count() == 1 && queryList.First() == query)),
                    options => options.Repeat.Once());
        }
    }
}
