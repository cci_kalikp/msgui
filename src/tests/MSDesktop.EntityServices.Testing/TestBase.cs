﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EntityServicesTests
{
    public abstract class TestBase
    {
        protected static void Run(Action callback)
        {
            Task.Factory.StartNew(callback, CancellationToken.None, TaskCreationOptions.None, new CurrentThreadTaskScheduler());
        }

        protected static void Wait(WaitHandle handle, int timeout = 10000)
        {
            if (!handle.WaitOne(timeout))
            {
                throw new InvalidOperationException("Timeout exceeded waiting for WaitHandle");
            }
        }

        protected static void Wait(Task handle, int timeout = 10000)
        {
            if (!handle.Wait(timeout))
            {
                throw new InvalidOperationException("Timeout exceeded waiting for Task");
            }
        }

        private sealed class CurrentThreadTaskScheduler : TaskScheduler
        {
            /// <summary>Runs the provided Task synchronously on the current thread.</summary>
            /// <param name="task">The task to be executed.</param>
            protected override void QueueTask(Task task)
            {
                TryExecuteTask(task);
            }

            /// <summary>Runs the provided Task synchronously on the current thread.</summary>
            /// <param name="task">The task to be executed.</param>
            /// <param name="taskWasPreviouslyQueued">Whether the Task was previously queued to the scheduler.</param>
            /// <returns>True if the Task was successfully executed; otherwise, false.</returns>
            protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
            {
                return TryExecuteTask(task);
            }

            /// <summary>Gets the Tasks currently scheduled to this scheduler.</summary>
            /// <returns>An empty enumerable, as Tasks are never queued, only executed.</returns>
            protected override IEnumerable<Task> GetScheduledTasks()
            {
                return Enumerable.Empty<Task>();
            }

            /// <summary>Gets the maximum degree of parallelism for this scheduler.</summary>
            public override int MaximumConcurrencyLevel
            {
                get { return 1; }
            }
        }
    }
}