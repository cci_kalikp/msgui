﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using EntityServicesTests.Entities;
using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Dates;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;
using NUnit.Framework;
using Rhino.Mocks;

namespace EntityServicesTests
{
	[TestFixture]
	public class EntityGatingServiceTest : TestBase
	{
		private IEntityService<TestEntity> _entityService;
		private EntityGatingService<TestEntity> _gatedEntityService;
		private IDateTimeService _dateTimeService;
		private IEntityServiceExceptionService _exceptionServiceMock;

		[SetUp]
		public void SetUp()
		{
			_entityService = MockRepository.GenerateStrictMock<IEntityService<TestEntity>>();
			_dateTimeService = new BclDateTimeService();
			_exceptionServiceMock = MockRepository.GenerateMock<IEntityServiceExceptionService>();
			ResetService();
		}

		private void ResetService()
		{
			_gatedEntityService = new EntityGatingService<TestEntity>(
				_entityService,
				_dateTimeService,
				_exceptionServiceMock);
		}

		[Test]
		public void Get_KeyQueryUngated_CallsUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var instance = new[] { new TestEntity(key) };

			_entityService
				.Expect(x => x.Get(new[] { key }))
				.Return(instance);

			// Act
			_gatedEntityService.Get(new[] { key });

			//Asset
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void Get_KeyQueryUngated_ReturnsResponseFromUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var instance = new[] { new TestEntity(key) };

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(instance)
				.Repeat.Once();

			// Act
			var result = _gatedEntityService.Get(new[] { key });

			//Asset
			Assert.AreEqual(result, instance);
		}

		[Test]
		public void Get_KeyQueryGatedByKeyAndPreviouslyGot_DoesNotCallUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var instance = new[] { new TestEntity(key) };

			// This is for the get before gating and the get on gating
			_entityService
				.Expect(x => x.Get(new[] { key }))
				.Return(instance)
				.Repeat.Twice();

			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(key, true);

			// Act
			_gatedEntityService.Get(new[] { key });

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void Get_KeyQueryGatedByKeyAndPreviouslyGot_ReturnsResponseFromGatedArea()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var instanceOnInitialGet = new TestEntity(key) { TestTypeDifferentiator = "Entity1" };
			var instanceOnSetGating = new TestEntity(key) { TestTypeDifferentiator = "Entity2" };

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { instanceOnInitialGet })
				.Repeat.Once();

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { instanceOnSetGating })
				.Repeat.Once();

			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(key, true);

			// Act
			var returnedEntity = _gatedEntityService.Get(new[] { key }).Single();

			// Assert
			Assert.AreEqual(instanceOnSetGating, returnedEntity);
		}

		[Test]
		public void Get_KeyQueryGatedByKeyBeforeBeingPreviouslyGot_DoesNotCallUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var instance = new[] { new TestEntity(key) };

			// This is for the get on gating and the first get after gating
			_entityService
				.Expect(x => x.Get(new[] { key }))
				.Return(instance)
				.Repeat.Once();

			_gatedEntityService.SetGating(key, true); // This calls the underlying service
			_gatedEntityService.Get(new[] { key }); // This calls the underlying service

			// Act
			_gatedEntityService.Get(new[] { key }); // This DOES NOT call the underlying service

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void Get_KeyQueryGatedByKeyBeforeBeingPreviouslyGot_ReturnsResponseFromGatedArea()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Entity1" };

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity })
				.Repeat.Once();

			_gatedEntityService.SetGating(key, true);
			_gatedEntityService.Get(new[] { key });

			// Act
			var returnedEntity = _gatedEntityService.Get(new[] { key }).Single();

			// Assert
			Assert.AreEqual(entity, returnedEntity);
		}

		[Test]
		public void Get_KeyQueryGatedByKeyAndNotPreviouslyGot_CallsUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var instance = new[] { new TestEntity(key) };

			// This is for the get before gating and the get on gating
			_entityService
				.Expect(x => x.Get(new[] { key }))
				.Return(instance)
				.Repeat.Once();

			_gatedEntityService.SetGating(key, true);

			// Act
			_gatedEntityService.Get(new[] { key });

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void Get_KeyQueryGatedByKeyAndNotPreviouslyGot_ReturnsResponseFromService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var instance = new TestEntity(key) { TestTypeDifferentiator = "Entity1" };

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { instance })
				.Repeat.Once();

			_gatedEntityService.SetGating(key, true);

			// Act
			var returnedEntity = _gatedEntityService.Get(new[] { key }).Single();

			// Assert
			Assert.AreEqual(instance, returnedEntity);
		}

		[Test]
		public void Get_KeyQueryGatedByAllAndPreviouslyGot_DoesNotCallUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var instance = new[] { new TestEntity(key) };

			// This is for the get before gating and the get on gating
			_entityService
				.Expect(x => x.Get(new[] { key }))
				.Return(instance)
				.Repeat.Once();

			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IKey<TestEntity>>>.Matches(y => y.Single() == key)))
				.Return(instance)
				.Repeat.Once();

			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(true);

			// Act
			_gatedEntityService.Get(new[] { key });

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void Get_KeyQueryGatedByAllAndPreviouslyGot_ReturnsResponseFromGatedArea()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var instanceOnInitialGet = new TestEntity(key) { TestTypeDifferentiator = "Entity1" };
			var instanceOnSetGating = new TestEntity(key) { TestTypeDifferentiator = "Entity2" };

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { instanceOnInitialGet })
				.Repeat.Once();

			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IKey<TestEntity>>>.Matches(y => y.Single() == key)))
				.Return(new[] { instanceOnSetGating })
				.Repeat.Once();

			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(true);

			// Act
			var returnedEntity = _gatedEntityService.Get(new[] { key }).Single();

			// Assert
			Assert.AreEqual(instanceOnSetGating, returnedEntity);
		}

		[Test]
		public void Get_KeyQueryGatedByAllBeforeBeingPreviouslyGot_DoesNotCallUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var instance = new[] { new TestEntity(key) };

			// This is for the get on the first get after gating
			// Note: the SetGating doesn't call Get on the underlying service as we have not previously got that key
			_entityService
				.Expect(x => x.Get(new[] { key }))
				.Return(instance)
				.Repeat.Once();

			_gatedEntityService.SetGating(true); // this should call the underlying service
			_gatedEntityService.Get(new[] { key }); // this should call the underlying service

			// Act
			_gatedEntityService.Get(new[] { key }); // this should NOT call the underlying service

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void Get_KeyQueryGatedByAllBeforeBeingPreviouslyGot_ReturnsResponseFromGatedArea()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Entity1" };

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity })
				.Repeat.Once();

			_gatedEntityService.SetGating(true);
			_gatedEntityService.Get(new[] { key });

			// Act
			var returnedEntity = _gatedEntityService.Get(new[] { key }).Single();

			// Assert
			Assert.AreEqual(entity, returnedEntity);
		}

		[Test]
		public void Get_KeyQueryGatedByAllAndNotPreviouslyGot_CallsUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var instance = new[] { new TestEntity(key) };

			_entityService
				.Expect(x => x.Get(new[] { key }))
				.Return(instance)
				.Repeat.Once();

			_gatedEntityService.SetGating(true);

			// Act
			_gatedEntityService.Get(new[] { key });

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void Get_KeyQueryGatedByAllAndNotPreviouslyGot_ReturnsResponseFromService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var instance = new TestEntity(key) { TestTypeDifferentiator = "Entity1" };

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { instance })
				.Repeat.Once();

			_gatedEntityService.SetGating(true);

			// Act
			var returnedEntity = _gatedEntityService.Get(new[] { key }).Single();

			// Assert
			Assert.AreEqual(instance, returnedEntity);
		}

		[Test]
		public void Get_UngatedNonKeyQuery_CallsUnderlyingService()
		{
			// Arrange
			var query = new TestEntityQuery("My Query");
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			_entityService
				.Expect(x => x.Get(new[] { query }))
				.Return(new[] { entity });

			// Act
			_gatedEntityService.Get(new[] { query });

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void Get_UngatedNonKeyQuery_ReturnsResultsFromUnderlyingService()
		{
			// Arrange
			var query = new TestEntityQuery("My Query");
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			_entityService
				.Stub(x => x.Get(new[] { query }))
				.Return(new[] { entity });

			// Act
			var returnedEntities = _gatedEntityService.Get(new[] { query });

			// Assert
			Assert.AreEqual(entity, returnedEntities.Single());
		}

		[Test]
		public void Get_NonKeyQueryWithMultipleEntityResponseWhereSomeEntitiesAreGated_CallsUnderlyingService()
		{
			// Arrange
			var query = new TestEntityQuery("My Query");
			var key1 = new TestEntityKey("My Key1");
			var key2 = new TestEntityKey("My Key2");
			var entity1 = new TestEntity(key1);
			var entity2 = new TestEntity(key2);

			_entityService
				.Stub(x => x.Get(new[] { key1 }))
				.Return(new[] { entity1 });

			_entityService
				.Expect(x => x.Get(new[] { query }))
				.Return(new[] { entity1, entity2 });

			_gatedEntityService.SetGating(key1, true);

			// Act
			_gatedEntityService.Get(new[] { query });

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void Get_NonKeyQueryWithMultipleEntityResponseWhereSomeEntitiesAreGated_ReturnsUngatedEntities()
		{
			// Arrange
			var query = new TestEntityQuery("My Query");
			var key1 = new TestEntityKey("My Key1");
			var key2 = new TestEntityKey("My Key2");
			var entity1 = new TestEntity(key1);
			var entity1A = new TestEntity(key1) { TestTypeDifferentiator = "Diff" };
			var entity2 = new TestEntity(key2);

			// This is required for the SetGating call...
			_entityService
				.Stub(x => x.Get(new[] { key1 }))
				.Return(new[] { entity1 });

			_entityService
				.Stub(x => x.Get(new[] { query }))
				.Return(new[] { entity1A, entity2 });

			_gatedEntityService.SetGating(key1, true);

			// Act
			var returnedEntities = _gatedEntityService.Get(new[] { query });

			// Assert
			Assert.AreEqual(2, returnedEntities.Count());
			Assert.IsTrue(returnedEntities.Contains(entity2));
		}

		[Test]
		public void Get_NonKeyQueryWithMultipleEntityResponseWhereSomeEntitiesAreGated_SwitchesOutGatedEntitiesInResponse()
		{
			// Arrange
			var query = new TestEntityQuery("My Query");
			var key1 = new TestEntityKey("My Key1");
			var key2 = new TestEntityKey("My Key2");
			var entity1 = new TestEntity(key1);
			var entity1A = new TestEntity(key1) { TestTypeDifferentiator = "Diff" };
			var entity2 = new TestEntity(key2);

			// This is required for the SetGating call...
			_entityService
				.Stub(x => x.Get(new[] { key1 }))
				.Return(new[] { entity1 });

			_entityService
				.Stub(x => x.Get(new[] { query }))
				.Return(new[] { entity1A, entity2 });

			_gatedEntityService.SetGating(key1, true);
			_gatedEntityService.Get(new[] { key1 });

			// Act
			var returnedEntities = _gatedEntityService.Get(new[] { query });

			// Assert
			Assert.AreEqual(2, returnedEntities.Count());
			Assert.IsTrue(returnedEntities.Contains(entity1));
		}

		[Test]
		public void Get_NonKeyQueryWithMultipleEntityResponseWhereAllEntitiesAreGated_CallsUnderlyingService()
		{
			// It has to call the underlying service because the entity gating service has no way of knowing what entities will be returned from the query

			// Arrange
			var query = new TestEntityQuery("My Query");
			var key1 = new TestEntityKey("My Key1");
			var key2 = new TestEntityKey("My Key2");
			var entity1 = new TestEntity(key1);
			var entity1A = new TestEntity(key1) { TestTypeDifferentiator = "Diff" };
			var entity2 = new TestEntity(key2);
			var entity2A = new TestEntity(key2) { TestTypeDifferentiator = "Diff" };

			// This is required for the SetGating call...
			_entityService
				.Stub(x => x.Get(new[] { key1 }))
				.Return(new[] { entity1 });
			_entityService
				.Stub(x => x.Get(new[] { key2 }))
				.Return(new[] { entity2 });

			_entityService
				.Expect(x => x.Get(new[] { query }))
				.Return(new[] { entity1A, entity2A });

			_gatedEntityService.SetGating(key1, true);
			_gatedEntityService.SetGating(key2, true);

			// Act
			_gatedEntityService.Get(new[] { query });

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void Get_NonKeyQueryWithMultipleEntityResponseWhereAllEntitiesAreGated_ReturnsGatedEntities()
		{
			// Arrange
			var query = new TestEntityQuery("My Query");
			var key1 = new TestEntityKey("My Key1");
			var key2 = new TestEntityKey("My Key2");
			var entity1 = new TestEntity(key1);
			var entity1A = new TestEntity(key1) { TestTypeDifferentiator = "Diff" };
			var entity2 = new TestEntity(key2);
			var entity2A = new TestEntity(key2) { TestTypeDifferentiator = "Diff" };

			// This is required for the SetGating call...
			_entityService
				.Stub(x => x.Get(new[] { key1 }))
				.Return(new[] { entity1 });
			_entityService
				.Stub(x => x.Get(new[] { key2 }))
				.Return(new[] { entity2 });

			_entityService
				.Expect(x => x.Get(new[] { query }))
				.Return(new[] { entity1A, entity2A });

			_gatedEntityService.SetGating(key1, true);
			_gatedEntityService.Get(new[] { key1 });
			_gatedEntityService.SetGating(key2, true);
			_gatedEntityService.Get(new[] { key2 });

			// Act
			var returnedEntities = _gatedEntityService.Get(new[] { query });

			// Assert
			Assert.AreEqual(2, returnedEntities.Count());
			Assert.IsTrue(returnedEntities.Contains(entity1));
			Assert.IsTrue(returnedEntities.Contains(entity2));
		}

		[Test]
		public void Get_KeyQueryAndGatingBeforeGet_GetCallsUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var instance = new[] { new TestEntity(key) };

			_entityService
				.Expect(x => x.Get(new[] { key }))
				.Return(instance)
				.Repeat.Once();

			_gatedEntityService.SetGating(key, true);

			// Act
			_gatedEntityService.Get(new[] { key });

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void Get_KeyQueryAndGatingBeforeGet_ReturnsResultFromUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var instance = new TestEntity(key);

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { instance })
				.Repeat.Once();

			_gatedEntityService.SetGating(key, true);

			// Act
			var returnedEntities = _gatedEntityService.Get(new[] { key });

			// Assert
			Assert.AreEqual(1, returnedEntities.Count());
			Assert.AreEqual(instance, returnedEntities.Single());
		}

		[Test]
		public void Get_NonKeyQueryAndGatingBeforeGet_ReturnsResultsFromUnderlyingService()
		{
			// Arrange
			var key1 = new TestEntityKey("My Key1");
			var key2 = new TestEntityKey("My Key2");
			var instance1 = new TestEntity(key1);
			var instance2 = new TestEntity(key2);
			var query = new TestEntityQuery("My Query");

			_entityService
				.Stub(x => x.Get(new[] { query }))
				.Return(new[] { instance1, instance2 })
				.Repeat.Once();

			_gatedEntityService.SetGating(key1, true);
			_gatedEntityService.SetGating(key2, true);

			// Act
			var returnedEntities = _gatedEntityService.Get(new[] { query });

			// Assert
			Assert.AreEqual(2, returnedEntities.Count());
			Assert.IsTrue(returnedEntities.Contains(instance1));
			Assert.IsTrue(returnedEntities.Contains(instance2));
		}

		[Test]
		public void GetObservable_ByKeyAndUngated_CallsGetObservableOnUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Expect(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			// Act
			_gatedEntityService.GetObservable(new[] { key }).Subscribe(x => { });

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void GetObservable_SecondCallByKeyAndUngated_DoesNotCallGetObservableOnUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Expect(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single() == key)))
				.Return(subject)
				.Repeat.Once();

			_gatedEntityService.GetObservable(new[] { key }).Subscribe(x => { });

			// Act
			_gatedEntityService.GetObservable(new[] { key }).Subscribe(x => { });

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void GetObservable_ByKeyAndUngatedUpdateOnUnderlyingService_RaisesUpdate()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			bool updateRaised = false;
			TestEntity raisedEntity = null;

			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x =>
			{
				updateRaised = true;
				raisedEntity = x.FirstOrDefault();
			});

			// Act
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));

			// Assert
			Assert.IsTrue(updateRaised);
			Assert.AreEqual(entity, raisedEntity);
		}

		[Test]
		public void GetObservable_ByQueryAndAllUngated_CallsGetObservableOnUnderlyingService()
		{
			// Arrange
			var query = new TestEntityQuery("My Query");

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Expect(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
				.Return(subject);

			// Act
			_gatedEntityService.GetObservable(new[] { query }).Subscribe(x => { });

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void GetObservable_ByQueryAndAllUngatedUpdateOnUnderlyingService_RaisesUpdate()
		{
			// Arrange
			var query = new TestEntityQuery("My Query");
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
				.Return(subject);

			bool updateRaised = false;
			TestEntity entityRaised = null;
			_gatedEntityService.GetObservable(new[] { query })
							   .Select(resource => resource.Entities)
							   .Subscribe(x =>
							   {
								   updateRaised = true;
								   entityRaised = x.FirstOrDefault();
							   });

			// Act
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));

			// Assert
			Assert.IsTrue(updateRaised);
			Assert.AreEqual(entity, entityRaised);
		}

		[Test]
		public void GetObservable_ByKeyAndAlreadyGatedOnKeyUpdateOnUnderlyingService_DoesNotRaiseUpdate()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			_gatedEntityService.SetGating(key, true);

			bool updateRaised = false;
			_gatedEntityService.GetObservable(new[] { key }).Subscribe(x => { updateRaised = true; });

			// Act
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));

			// Assert
			Assert.IsFalse(updateRaised);
		}

		[Test]
		public void GetObservable_ByKeyAndAlreadyGatedOnAllUpdateOnUnderlyingService_DoesNotRaiseUpdate()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			var subject = new Subject<ObservableResource<TestEntity>>();

			bool updateRaised = false;

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			_gatedEntityService.SetGating(true);
			_gatedEntityService.GetObservable(new[] { key }).Subscribe(x => { updateRaised = true; });

			// Act
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));

			// Assert
			Assert.IsFalse(updateRaised);
		}

		[Test]
		public void GetObservable_ByQueryAndAlreadyGatedUpdateOnUnderlyingService_DoesNotRaiseUpdate()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var query = new TestEntityQuery("MyQuery");
			var entity = new TestEntity(key);

			var subject = new Subject<ObservableResource<TestEntity>>();

			bool updateRaised = false;

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
				.Return(subject);

			_gatedEntityService.SetGating(key, true);
			_gatedEntityService.GetObservable(new[] { query }).Subscribe(x => { updateRaised = true; });

			// Act
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));

			// Assert
			Assert.IsFalse(updateRaised);
		}

		[Test]
		public void GetObservable_ByKeySubscribedThenGateByKeyThenUpdateOnUnderlyingService_DoesNotRaiseUpdate()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			bool updateRaised = false;
			_gatedEntityService.GetObservable(new[] { key }).Subscribe(x => { updateRaised = true; });
			_gatedEntityService.SetGating(key, true);

			// Act
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));

			// Assert
			Assert.IsFalse(updateRaised);
		}

		[Test]
		public void GetObservable_ByKeySubscribedThenGateOnAllThenUpdateOnUnderlyingService_DoesNotRaiseUpdate()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			var subject = new Subject<ObservableResource<TestEntity>>();

			bool updateRaised = false;

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			_gatedEntityService.GetObservable(new[] { key }).Subscribe(x => { updateRaised = true; });
			_gatedEntityService.SetGating(true);

			// Act
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));

			// Assert
			Assert.IsFalse(updateRaised);
		}

		[Test]
		public void GetObservable_ByKeyGatedThenSubscribedThenUngatedThenUpdateOnUnderlyingService_RaisesUpdate()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject)
				.Repeat.Once();

			_gatedEntityService.SetGating(key, true);

			bool updateRaised = false;
			TestEntity entityRaised = null;
			_gatedEntityService.GetObservable(new[] { key })
							   .Select(resource => resource.Entities)
							   .Subscribe(x =>
							   {
								   updateRaised = true;
								   entityRaised = x.First();
							   });
			_gatedEntityService.SetGating(key, false);

			// Act
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));

			// Assert
			Assert.IsTrue(updateRaised);
			Assert.AreEqual(entity, entityRaised);
		}

		[Test]
		public void AcceptByKey_BasicCall_CallGetOnUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Original Entity" };
			var newAcceptedEntity = new TestEntity(key) { TestTypeDifferentiator = "New Entity" };

			// Called when get is called to get the value in to the gated area...
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(key))))
				.Return(new[] { entity })
				.Repeat.Once();

			// Gets the value in to the gated area...
			_gatedEntityService.SetGating(key, true);
			_gatedEntityService.Get(new[] { key });

			_entityService
				.Expect(x => x.Get(new[] { key }))
				.Return(new[] { newAcceptedEntity })
				.Repeat.Once();

			// Act
			_gatedEntityService.Accept(key);

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void AcceptByKey_ExistingGatedValueAndNewValuesDiffer_UpdatesGatedInstance()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Original Entity" };
			var newAcceptedEntity = new TestEntity(key) { TestTypeDifferentiator = "New Entity" };

			// Called when get is called to get the value in to the gated area...
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(key))))
				.Return(new[] { entity })
				.Repeat.Once();

			// Gets the value in to the gated area...
			_gatedEntityService.SetGating(key, true);
			_gatedEntityService.Get(new[] { key });

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { newAcceptedEntity })
				.Repeat.Once();

			// Act
			_gatedEntityService.Accept(key);

			// Assert
			var gatedEntity = _gatedEntityService.Get(new[] { key }).Single();
			Assert.AreEqual(newAcceptedEntity, gatedEntity);
		}

		[Test]
		public void AcceptByKey_ExistingGatedValueAndNewValuesDiffer_UpdatesMatchingKeyBasedObservables()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Original Entity" };
			var newAcceptedEntity = new TestEntity(key) { TestTypeDifferentiator = "New Entity" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { newAcceptedEntity })
				.Repeat.Once();

			int raiseCount = 0;
			var raisedEntities = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key })
				.Select(resource => resource.Entities)
				.Subscribe(x =>
				{
					raiseCount++;
					raisedEntities.AddRange(x);
				});

			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			_gatedEntityService.SetGating(key, true);
			subject.OnNext(new ObservableResource<TestEntity>(new[] { newAcceptedEntity }));

			// Act
			_gatedEntityService.Accept(key);

			// Assert
			Assert.AreEqual(2, raiseCount);
			Assert.AreEqual(entity, raisedEntities[0]);
			Assert.AreEqual(newAcceptedEntity, raisedEntities[1]);
		}

		[Test]
		public void AcceptByKey_ExistingGatedValueAndNewValuesDiffer_UpdatesQueryBasedObservablesThatHaveHadAnUpdateWithMatchingKeyFromTheServiceAlready()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var query = new TestEntityQuery("My Query");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Original Entity" };
			var newAcceptedEntity = new TestEntity(key) { TestTypeDifferentiator = "New Entity" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
				.Return(subject);

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { newAcceptedEntity })
				.Repeat.Once();

			int raiseCount = 0;
			var raisedEntities = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { query }).Select(resource => resource.Entities).Subscribe(x =>
			{
				raiseCount++;
				raisedEntities.AddRange(x);
			});

			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			_gatedEntityService.SetGating(key, true);
			subject.OnNext(new ObservableResource<TestEntity>(new[] { newAcceptedEntity }));

			// Act
			_gatedEntityService.Accept(key);

			// Assert
			Assert.AreEqual(2, raiseCount);
			Assert.AreEqual(entity, raisedEntities[0]);
			Assert.AreEqual(newAcceptedEntity, raisedEntities[1]);
		}

		[Test]
		public void AcceptByKey_ExistingGatedValueAndNewValuesAreSame_DoesNotUpdateMatchingKeyBasedObservables()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Original Entity" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(key))))
				.Return(new[] { entity })
				.Repeat.Once();

			int raiseCount = 0;
			var raisedEntities = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x =>
			{
				raiseCount++;
				raisedEntities.AddRange(x);
			});

			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			_gatedEntityService.SetGating(key, true);

			// Act
			_gatedEntityService.Accept(key);

			// Assert
			Assert.AreEqual(1, raiseCount);
			Assert.AreEqual(entity, raisedEntities[0]);
		}

		[Test]
		public void AcceptByKey_ExistingGatedValueAndNewValuesDiffer_DoesNotUpdateQueryBasedObservablesThatHaveHadAnUpdateWithMatchingKeyFromTheServiceAlready()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var query = new TestEntityQuery("My Query");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Original Entity" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
				.Return(subject);

			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(key))))
				.Return(new[] { entity })
				.Repeat.Once();

			int raiseCount = 0;
			var raisedEntities = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { query }).Select(resource => resource.Entities).Subscribe(x =>
			{
				raiseCount++;
				raisedEntities.AddRange(x);
			});

			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			_gatedEntityService.SetGating(key, true);

			// Act
			_gatedEntityService.Accept(key);

			// Assert
			Assert.AreEqual(1, raiseCount);
			Assert.AreEqual(entity, raisedEntities[0]);
		}

		[Test]
		public void AcceptAll_PreviousGotKeys_CallsUnderlyingServiceForKeys()
		{
			// Arrange
			var key1 = new TestEntityKey("MyKey1");
			var key2 = new TestEntityKey("MyKey2");
			var entity1 = new TestEntity(key1);
			var entity2 = new TestEntity(key2);

			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(key1))))
				.Return(new[] { entity1 });
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(key2))))
				.Return(new[] { entity2 });

			_gatedEntityService.SetGating(key1, true);
			_gatedEntityService.SetGating(key2, true);
			_gatedEntityService.Get(new[] { key1 });
			_gatedEntityService.Get(new[] { key2 });

			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
					y => y.Count() == 2 && y.Contains(key1) && y.Contains(key2))))
				.Return(new[] { entity1, entity2 });

			// Act
			_gatedEntityService.Accept();

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void AcceptAll_UngatedPreviousGotKeys_DoesNotCallUnderlyingServiceForKeys()
		{
			// Arrange
			var key1 = new TestEntityKey("MyKey1");
			var key2 = new TestEntityKey("MyKey2");
			var entity1 = new TestEntity(key1);
			var entity2 = new TestEntity(key2);

			_entityService
				.Stub(x => x.Get(new[] { key1 }))
				.Return(new[] { entity1 });
			_entityService
				.Stub(x => x.Get(new[] { key2 }))
				.Return(new[] { entity2 });

			_gatedEntityService.Get(new[] { key1 });
			_gatedEntityService.Get(new[] { key2 });

			// Act
			_gatedEntityService.Accept();

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void AcceptAll_GatedValuesExistedBeforeAccepting_GetAfterAcceptGetsNewValues()
		{
			// Arrange
			var key1 = new TestEntityKey("MyKey1");
			var key2 = new TestEntityKey("MyKey2");
			var entity1 = new TestEntity(key1) { TestTypeDifferentiator = "Old1" };
			var entity2 = new TestEntity(key2) { TestTypeDifferentiator = "Old2" };
			var newEntity1 = new TestEntity(key1) { TestTypeDifferentiator = "New1" };
			var newEntity2 = new TestEntity(key2) { TestTypeDifferentiator = "New2" };

			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(key1))))
				.Return(new[] { entity1 })
				.Repeat.Twice();
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(key2))))
				.Return(new[] { entity2 })
				.Repeat.Twice();

			_gatedEntityService.SetGating(key1, true);
			_gatedEntityService.SetGating(key2, true);
			_gatedEntityService.Get(new[] { key1 });
			_gatedEntityService.Get(new[] { key2 });

			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
					y => y.Count() == 2 && y.Contains(key1) && y.Contains(key2))))
				.Return(new[] { newEntity1, newEntity2 });

			// Act
			_gatedEntityService.Accept();

			// Assert
			var returnedEntity1 = _gatedEntityService.Get(new[] { key1 }).Single();
			var returnedEntity2 = _gatedEntityService.Get(new[] { key2 }).Single();
			Assert.AreEqual(newEntity1, returnedEntity1);
			Assert.AreEqual(newEntity2, returnedEntity2);
		}

		[Test]
		public void AcceptAll_GatedSubscriberHasBeenPreviouslyUpdated_GetsValueFromUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "New" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			// create the gated subscriber...
			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject)
				.Repeat.Once();
			var keyObservableRaisedEntities = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(keyObservableRaisedEntities.AddRange);
			_gatedEntityService.SetGating(key, true);

			// update the gated subscriber...
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			subject.OnNext(new ObservableResource<TestEntity>(new[] { newEntity }));

			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single() == key)))
				.Return(new[] { newEntity });

			// Act
			_gatedEntityService.Accept();

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void AcceptAll_GatedSubscriberHasBeenPreviouslyUpdated_UpdatesAllStreamsWithNewAcceptedValue()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "New" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			// create the gated subscriber...
			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject)
				.Repeat.Once();
			int keyObservableCallCount = 0;
			var keyObservableRaisedEntities = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x =>
			{
				keyObservableCallCount++;
				keyObservableRaisedEntities.AddRange(x);
			});
			_gatedEntityService.SetGating(key, true);

			// update the gated subscriber...
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			subject.OnNext(new ObservableResource<TestEntity>(new[] { newEntity }));

			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single() == key)))
				.Return(new[] { newEntity });

			// Act
			_gatedEntityService.Accept();

			// Assert
			Assert.AreEqual(1, keyObservableCallCount);
			Assert.AreEqual(newEntity, keyObservableRaisedEntities.First());
		}

		[Test]
		public void AcceptAll_GatedButNotSubscribedHasBeenPreviouslyUpdated_DoesNotGetValueFromUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "New" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			// gate...
			_gatedEntityService.SetGating(key, true);

			// ...then update the stream...
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			subject.OnNext(new ObservableResource<TestEntity>(new[] { newEntity }));

			// ...and then create the subscriber...
			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject)
				.Repeat.Once();
			var keyObservableRaisedEntities = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(keyObservableRaisedEntities.AddRange);

			// Act
			_gatedEntityService.Accept();

			// Assert
			// Verify that Get() is not called...
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void AcceptAll_GatedButNotSubscribedHasBeenPreviouslyUpdated_DoesNotUpdateStreamWithAnyAcceptedValue()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "New" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			// gate...
			_gatedEntityService.SetGating(key, true);

			// ...then update the stream...
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			subject.OnNext(new ObservableResource<TestEntity>(new[] { newEntity }));

			// ...and then create the subscriber...
			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject)
				.Repeat.Once();
			int keyObservableCallCount = 0;
			var keyObservableRaisedEntities = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x =>
			{
				keyObservableCallCount++;
				keyObservableRaisedEntities.AddRange(x);
			});

			// Act
			_gatedEntityService.Accept();

			// Assert
			Assert.AreEqual(0, keyObservableCallCount);
		}

		[Test]
		public void AcceptAll_GatedSubscriberHasNotBeenPreviouslyUpdated_DoesNotGetValueFromUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");

			var subject = new Subject<ObservableResource<TestEntity>>();


			// create the gated subscriber...
			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject)
				.Repeat.Once();
			var keyObservableRaisedEntities = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(keyObservableRaisedEntities.AddRange);
			_gatedEntityService.SetGating(key, true);

			// But do not update the stream before calling Accept()

			// Act
			_gatedEntityService.Accept();

			// Assert
			// Verify that Get() does not get called...
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void AcceptAll_GatedSubscriberHasNotBeenPreviouslyUpdated_DoesNotUpdateStreamWithAnyAcceptedValue()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");

			var subject = new Subject<ObservableResource<TestEntity>>();

			// create the gated subscriber...
			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject)
				.Repeat.Once();
			int keyObservableCallCount = 0;
			var keyObservableRaisedEntities = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x =>
			{
				keyObservableCallCount++;
				keyObservableRaisedEntities.AddRange(x);
			});
			_gatedEntityService.SetGating(key, true);

			// But do not update the stream before calling Accept()

			// Act
			_gatedEntityService.Accept();

			// Assert
			Assert.AreEqual(0, keyObservableCallCount);
		}

		[Test]
		public void SetGatingByKey_KeyNotPreviouslyGot_DoesNotCallGetOnUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");

			// Act
			_gatedEntityService.SetGating(key, true);

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void SetGatingByKey_KeyPreviouslyGot_CallsGetOnUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			// Once for the initial Get and another for the get on SetGating...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity })
				.Repeat.Twice();

			_gatedEntityService.Get(new[] { key });

			// Act
			_gatedEntityService.SetGating(key, true);

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void SetGatingByKey_KeyPreviouslyGotAndGatingIsBeingTurnedOff_DoesNotCallGetOnUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			// Once for the initial Get and another for the get on SetGating...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity })
				.Repeat.Twice();
			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(key, true);

			// Act
			// ...but this call should not call Get()
			_gatedEntityService.SetGating(key, false);

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void SetGatingByKey_KeyPreviouslyGotAndGatedAndWeSetGatingOnAgain_DoesNotCallGetOnUnderlyingServiceForSecondSetGating()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			// Once for the initial Get and another for the get on SetGating...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity })
				.Repeat.Twice();
			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(key, true);

			// Act
			_gatedEntityService.SetGating(key, true);

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void SetGatingByKey_UpdatesReceivedWhilstGatingIsOnAndWeSetGatingOff_GetsLatestValueFromUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity1 = new TestEntity(key);
			var entity2 = new TestEntity(key);

			var subject = new Subject<ObservableResource<TestEntity>>();
			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);
			var publishedEntities = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(publishedEntities.AddRange);

			// Once for the initial Get and another for the get on SetGating on and the final on set gating off...
			_entityService
				.Expect(x => x.Get(new[] { key }))
				.Return(new[] { entity1 })
				.Repeat.Times(3);
			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(key, true);
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity2 }));

			// Act
			_gatedEntityService.SetGating(key, false);

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void SetGatingByKey_UpdatesReceivedWhilstGatingIsOnAndWeSetGatingOff_PublishesLatestUpdate()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity1 = new TestEntity(key);
			var entity2 = new TestEntity(key);

			var subject = new Subject<ObservableResource<TestEntity>>();
			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);
			var publishedEntities = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(publishedEntities.AddRange);

			// Once for the initial Get and another for the get on SetGating on and the final on set gating off...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity1 })
				.Repeat.Times(3);
			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(key, true);
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity2 }));

			// Act
			_gatedEntityService.SetGating(key, false);

			// Assert
			Assert.AreEqual(1, publishedEntities.Count());
			Assert.AreEqual(entity2, publishedEntities.First());
		}

		[Test]
		public void SetGatingOnAll_KeyPreviouslyGot_CallsGetOnUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			// Once for the initial Get and another for the get on SetGating...
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.FirstOrDefault() == key)))
				.Return(new[] { entity })
				.Repeat.Twice();
			//_entityService
			//    .Stub(x => x.Get(new[] {key}))
			//    .Return(new[] {entity})
			//    .Repeat.Once();

			_gatedEntityService.Get(new[] { key });

			// Act
			_gatedEntityService.SetGating(true);

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void SetGatingOnAll_KeyNotPreviouslyGot_DoesNotCallGetOnUnderlyingService()
		{
			// Arrange

			// Act
			_gatedEntityService.SetGating(true);

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void SetGatingOnAll_KeyPreviouslyGot_FutureGetsDoNotCallUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			// Once for the initial Get and another for the get on SetGating...
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.FirstOrDefault() == key)))
				.Return(new[] { entity })
				.Repeat.Twice();

			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(true);

			// Act
			_gatedEntityService.Get(new[] { key });

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void SetGatingOnAll_KeyPreviouslyGot_FutureGetsReturnValueGotOnGating()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "new" };

			// Once for the initial Get and another for the get on SetGating...
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.FirstOrDefault() == key)))
				.Return(new[] { entity })
				.Repeat.Once();

			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.FirstOrDefault() == key)))
				.Return(new[] { newEntity })
				.Repeat.Once();

			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(true);

			// Act
			var futureGet = _gatedEntityService.Get(new[] { key }).Single();

			// Assert
			Assert.AreEqual(newEntity, futureGet);
		}

		[Test]
		public void SetGatingOnAll_AcceptedInstanceExistsAndWeTurnGatingOff_UnderlyingServiceIsNotCalled()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "new" };

			// Once for the initial Get and another two forb the calls onn SetGating...
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.FirstOrDefault() == key)))
				.Return(new[] { entity })
				.Repeat.Times(3);
			_gatedEntityService.Get(new[] { key });

			_gatedEntityService.SetGating(true);

			// Act
			_gatedEntityService.SetGating(false);

			// Assert
			// Verify that entityService.Get() is not called when we ungate...
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void SetGatingOnAll_AcceptedInstanceExistsAndWeTurnGatingOff_IsGatedIsSetToFalse()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "new" };

			// Once for the initial Get and another two forb the calls onn SetGating...
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.FirstOrDefault() == key)))
				.Return(new[] { entity })
				.Repeat.Times(3);
			_gatedEntityService.Get(new[] { key });

			_gatedEntityService.SetGating(true);

			// Act
			_gatedEntityService.SetGating(false);

			// Assert
			Assert.IsFalse(_gatedEntityService.IsGated(key));
		}

		[Test]
		public void SetGatingOnAll_GatingTurnedOnAndWeTryTurningItOnAgain_DoesntCallEntityServiceASecondTime()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "new" };

			// Once for the initial Get and another for the get on SetGating...
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.FirstOrDefault() == key)))
				.Return(new[] { entity })
				.Repeat.Once();
			_gatedEntityService.Get(new[] { key });

			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.FirstOrDefault() == key)))
				.Return(new[] { newEntity })
				.Repeat.Once();
			_gatedEntityService.SetGating(true);

			// Act
			_gatedEntityService.SetGating(true);

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void SetGatingOnAll_GatedInstanceExistsAndWeTurnGatingOff_CallsGetOnUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "new" };
			var lastEntity = new TestEntity(key) { TestTypeDifferentiator = "last" };

			var subject = new Subject<ObservableResource<TestEntity>>();
			_entityService
				.Expect(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);
			var publishedEntities = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(publishedEntities.AddRange);

			// Once for the initial Get and another for the get on SetGating on and another for the SetGating off...
			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.FirstOrDefault() == key)))
				.Return(new[] { entity })
				.Repeat.Once();
			_gatedEntityService.Get(new[] { key });

			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.FirstOrDefault() == key)))
				.Return(new[] { newEntity })
				.Repeat.Once();
			_gatedEntityService.SetGating(true);

			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.FirstOrDefault() == key)))
				.Return(new[] { lastEntity })
				.Repeat.Once();

			subject.OnNext(new ObservableResource<TestEntity>(new[] { lastEntity }));

			// Act
			_gatedEntityService.SetGating(false);

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void SetGatingOnAll_GatedInstanceExistsAndWeTurnGatingOff_PublishesLatestEntity()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "new" };
			var lastEntity = new TestEntity(key) { TestTypeDifferentiator = "last" };

			var subject = new Subject<ObservableResource<TestEntity>>();
			_entityService
				.Expect(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);
			var publishedEntities = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(publishedEntities.AddRange);

			// Once for the initial Get and another for the get on SetGating on and another for the SetGating off...
			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.FirstOrDefault() == key)))
				.Return(new[] { entity })
				.Repeat.Once();
			_gatedEntityService.Get(new[] { key });

			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.FirstOrDefault() == key)))
				.Return(new[] { newEntity })
				.Repeat.Once();
			_gatedEntityService.SetGating(true);

			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.FirstOrDefault() == key)))
				.Return(new[] { lastEntity })
				.Repeat.Once();

			subject.OnNext(new ObservableResource<TestEntity>(new[] { lastEntity }));

			// Act
			_gatedEntityService.SetGating(false);

			// Assert
			Assert.AreEqual(1, publishedEntities.Count());
			Assert.AreEqual(lastEntity, publishedEntities.First());
		}

		[Test]
		public void AcceptAllOnPredicate_PreviousGotKeys_CallsUnderlyingServiceForKeys()
		{
			// Arrange
			var key1 = new TestEntityKey("MyKey1");
			var key2 = new TestEntityKey("MyKey2");
			var entity1 = new TestEntity(key1);
			var entity2 = new TestEntity(key2);

			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(key1))))
				.Return(new[] { entity1 });
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(key2))))
				.Return(new[] { entity2 });

			_gatedEntityService.SetGating(key1, true);
			_gatedEntityService.SetGating(key2, true);
			_gatedEntityService.Get(new[] { key1 });
			_gatedEntityService.Get(new[] { key2 });

			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
					y => y.Count() == 2 && y.Contains(key1) && y.Contains(key2))))
				.Return(new[] { entity1, entity2 });

			// Act
			_gatedEntityService.Accept(x => ((IEntity)x).Key.ToString() == "MyKey1");

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void AcceptAllOnPredicate_UngatedPreviousGotKeys_DoesNotCallUnderlyingServiceForKeys()
		{
			// Arrange
			var key1 = new TestEntityKey("MyKey1");
			var key2 = new TestEntityKey("MyKey2");
			var entity1 = new TestEntity(key1);
			var entity2 = new TestEntity(key2);

			_entityService
				.Stub(x => x.Get(new[] { key1 }))
				.Return(new[] { entity1 });
			_entityService
				.Stub(x => x.Get(new[] { key2 }))
				.Return(new[] { entity2 });

			_gatedEntityService.Get(new[] { key1 });
			_gatedEntityService.Get(new[] { key2 });

			// Act
			_gatedEntityService.Accept(x => ((IEntity)x).Key.ToString() == "MyKey1");

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void AcceptAllOnPredicate_GatedValuesThatAllMatchPredicateExistedBeforeAccepting_GetAfterAcceptGetsNewValues()
		{
			// Arrange
			var key1 = new TestEntityKey("MyKey1");
			var key2 = new TestEntityKey("MyKey2");
			var entity1 = new TestEntity(key1) { TestTypeDifferentiator = "Old1" };
			var entity2 = new TestEntity(key2) { TestTypeDifferentiator = "Old2" };
			var newEntity1 = new TestEntity(key1) { TestTypeDifferentiator = "New1" };
			var newEntity2 = new TestEntity(key2) { TestTypeDifferentiator = "New2" };

			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(key1))))
				.Return(new[] { entity1 })
				.Repeat.Once();
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(key2))))
				.Return(new[] { entity2 })
				.Repeat.Once();

			_gatedEntityService.SetGating(key1, true);
			_gatedEntityService.SetGating(key2, true);
			_gatedEntityService.Get(new[] { key1 });
			_gatedEntityService.Get(new[] { key2 });

			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
					y => y.Count() == 2 && y.Contains(key1) && y.Contains(key2))))
				.Return(new[] { newEntity1, newEntity2 });

			// Act
			_gatedEntityService.Accept(x => true);

			// Assert
			var returnedEntity1 = _gatedEntityService.Get(new[] { key1 }).Single();
			var returnedEntity2 = _gatedEntityService.Get(new[] { key2 }).Single();
			Assert.AreEqual(newEntity1, returnedEntity1);
			Assert.AreEqual(newEntity2, returnedEntity2);
		}

		[Test]
		public void AcceptAllOnPredicate_GatedValuesThatSomeMatchPredicateExistedBeforeAccepting_GetAfterAcceptGetsNewValueForMatching()
		{
			// Arrange
			var key1 = new TestEntityKey("MyKey1");
			var key2 = new TestEntityKey("MyKey2");
			var entity1 = new TestEntity(key1) { TestTypeDifferentiator = "Old1" };
			var entity2 = new TestEntity(key2) { TestTypeDifferentiator = "Old2" };
			var newEntity1 = new TestEntity(key1) { TestTypeDifferentiator = "New1" };
			var newEntity2 = new TestEntity(key2) { TestTypeDifferentiator = "New2" };

			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(key1))))
				.Return(new[] { entity1 })
				.Repeat.Once();
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(key2))))
				.Return(new[] { entity2 })
				.Repeat.Once();

			_gatedEntityService.SetGating(key1, true);
			_gatedEntityService.SetGating(key2, true);
			_gatedEntityService.Get(new[] { key1 });
			_gatedEntityService.Get(new[] { key2 });

			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
					y => y.Count() == 2 && y.Contains(key1) && y.Contains(key2))))
				.Return(new[] { newEntity1, newEntity2 });

			// Act
			_gatedEntityService.Accept(x => ((IEntity)x).Key.ToString() == "MyKey1");

			// Assert
			var returnedEntity1 = _gatedEntityService.Get(new[] { key1 }).Single();
			var returnedEntity2 = _gatedEntityService.Get(new[] { key2 }).Single();
			Assert.AreEqual(newEntity1, returnedEntity1);
			Assert.AreEqual(entity2, returnedEntity2);
		}

		[Test]
		public void AcceptAllOnPredicate_GatedValuesThatNoneThatMatchPredicateExistedBeforeAccepting_GetAfterAcceptDoesNotGetNewValueForMatching()
		{
			// Arrange
			var key1 = new TestEntityKey("MyKey1");
			var key2 = new TestEntityKey("MyKey2");
			var entity1 = new TestEntity(key1) { TestTypeDifferentiator = "Old1" };
			var entity2 = new TestEntity(key2) { TestTypeDifferentiator = "Old2" };
			var newEntity1 = new TestEntity(key1) { TestTypeDifferentiator = "New1" };
			var newEntity2 = new TestEntity(key2) { TestTypeDifferentiator = "New2" };

			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(key1))))
				.Return(new[] { entity1 })
				.Repeat.Once();
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Count() == 1 && y.Contains(key2))))
				.Return(new[] { entity2 })
				.Repeat.Once();

			_gatedEntityService.SetGating(key1, true);
			_gatedEntityService.SetGating(key2, true);
			_gatedEntityService.Get(new[] { key1 });
			_gatedEntityService.Get(new[] { key2 });

			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
					y => y.Count() == 2 && y.Contains(key1) && y.Contains(key2))))
				.Return(new[] { newEntity1, newEntity2 });

			// Act
			_gatedEntityService.Accept(x => ((IEntity)x).Key.ToString() == "MyKey3");

			// Assert
			var returnedEntity1 = _gatedEntityService.Get(new[] { key1 }).Single();
			var returnedEntity2 = _gatedEntityService.Get(new[] { key2 }).Single();
			Assert.AreEqual(entity1, returnedEntity1);
			Assert.AreEqual(entity2, returnedEntity2);
		}

		[Test]
		public void AcceptAllOnPredicate_GatedSubscriberHasBeenPreviouslyUpdated_GetsValueFromUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "New" };

			var subject = new Subject<ObservableResource<TestEntity>>();


			// create the gated subscriber...
			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject)
				.Repeat.Once();
			var keyObservableRaisedEntities = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(keyObservableRaisedEntities.AddRange);
			_gatedEntityService.SetGating(key, true);

			// update the gated subscriber...
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			subject.OnNext(new ObservableResource<TestEntity>(new[] { newEntity }));

			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single() == key)))
				.Return(new[] { newEntity });

			// Act
			_gatedEntityService.Accept(x => ((IEntity)x).Key.ToString() == "MyKey1");

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void AcceptAllOnPredicate_GatedSubscriberHasBeenPreviouslyUpdated_UpdatesAllStreamsWithNewAcceptedValueIfValueMatchesPredicate()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "New" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			// create the gated subscriber...
			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject)
				.Repeat.Once();
			int keyObservableCallCount = 0;
			var keyObservableRaisedEntityResources = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x =>
			{
				keyObservableCallCount++;
				keyObservableRaisedEntityResources.AddRange(x);
			});
			_gatedEntityService.SetGating(key, true);

			// update the gated subscriber...
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			subject.OnNext(new ObservableResource<TestEntity>(new[] { newEntity }));

			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single() == key)))
				.Return(new[] { newEntity });

			// Act
			_gatedEntityService.Accept(x => ((IEntity)x).Key.ToString() == "MyKey");

			// Assert
			Assert.AreEqual(1, keyObservableCallCount);
			Assert.AreEqual(newEntity, keyObservableRaisedEntityResources.First());
		}

		[Test]
		public void AcceptAllOnPredicate_GatedSubscriberHasBeenPreviouslyUpdated_DoesntUpdateAllStreamsWithNewAcceptedValueIfValueDoesntMatchPredicate()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "New" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			// create the gated subscriber...
			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject)
				.Repeat.Once();
			int keyObservableCallCount = 0;
			var keyObservableRaisedEntityResources = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x =>
			{
				keyObservableCallCount++;
				keyObservableRaisedEntityResources.AddRange(x);
			});
			_gatedEntityService.SetGating(key, true);

			// update the gated subscriber...
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			subject.OnNext(new ObservableResource<TestEntity>(new[] { newEntity }));

			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single() == key)))
				.Return(new[] { newEntity });

			// Act
			_gatedEntityService.Accept(x => ((IEntity)x).Key.ToString() == "MyDifferentKey");

			// Assert
			Assert.AreEqual(0, keyObservableCallCount);
		}

		[Test]
		public void AcceptAllOnPredicate_GatedButNotSubscribedHasBeenPreviouslyUpdated_DoesNotGetValueFromUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "New" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			// gate...
			_gatedEntityService.SetGating(key, true);

			// ...then update the stream...
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			subject.OnNext(new ObservableResource<TestEntity>(new[] { newEntity }));

			// ...and then create the subscriber...
			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject)
				.Repeat.Once();
			var keyObservableRaisedEntityResources = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(keyObservableRaisedEntityResources.AddRange);

			// Act
			_gatedEntityService.Accept(x => ((IEntity)x).Key.ToString() == "MyKey");

			// Assert
			// Verify that Get() is not called...
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void AcceptAllOnPredicate_GatedButNotSubscribedHasBeenPreviouslyUpdated_DoesNotUpdateStreamWithAnyAcceptedValue()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "New" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			// gate...
			_gatedEntityService.SetGating(key, true);

			// ...then update the stream...
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			subject.OnNext(new ObservableResource<TestEntity>(new[] { newEntity }));

			// ...and then create the subscriber...
			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject)
				.Repeat.Once();
			int keyObservableCallCount = 0;
			var keyObservableRaisedEntities = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x =>
			{
				keyObservableCallCount++;
				keyObservableRaisedEntities.AddRange(x);
			});

			// Act
			_gatedEntityService.Accept(x => ((IEntity)x).Key.ToString() == "MyKey");

			// Assert
			Assert.AreEqual(0, keyObservableCallCount);
		}

		[Test]
		public void AcceptAllOnPredicate_GatedSubscriberHasNotBeenPreviouslyUpdated_DoesNotGetValueFromUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");

			var subject = new Subject<ObservableResource<TestEntity>>();

			// create the gated subscriber...
			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject)
				.Repeat.Once();
			var keyObservableRaisedEntityResources = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(keyObservableRaisedEntityResources.AddRange);
			_gatedEntityService.SetGating(key, true);

			// But do not update the stream before calling Accept()

			// Act
			_gatedEntityService.Accept(x => ((IEntity)x).Key.ToString() == "MyKey");

			// Assert
			// Verify that Get() does not get called...
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void AcceptAllOnPredicate_GatedSubscriberHasNotBeenPreviouslyUpdated_DoesNotUpdateStreamWithAnyAcceptedValue()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");

			var subject = new Subject<ObservableResource<TestEntity>>();

			// create the gated subscriber...
			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject)
				.Repeat.Once();
			int keyObservableCallCount = 0;
			var keyObservableRaisedEntityResources = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x =>
			{
				keyObservableCallCount++;
				keyObservableRaisedEntityResources.AddRange(x);
			});
			_gatedEntityService.SetGating(key, true);

			// But do not update the stream before calling Accept()

			// Act
			_gatedEntityService.Accept(x => ((IEntity)x).Key.ToString() == "MyKey");

			// Assert
			Assert.AreEqual(0, keyObservableCallCount);
		}

		[Test]
		public void AcceptByKeyOnPredicate_BasicCall_CallGetOnUnderlyingService()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Original Entity" };
			var newAcceptedEntity = new TestEntity(key) { TestTypeDifferentiator = "New Entity" };

			// Called when get is called to get the value in to the gated area...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity })
				.Repeat.Once();

			// Gets the value in to the gated area...
			_gatedEntityService.SetGating(key, true);
			_gatedEntityService.Get(new[] { key });

			_entityService
				.Expect(x => x.Get(new[] { key }))
				.Return(new[] { newAcceptedEntity })
				.Repeat.Once();

			// Act
			_gatedEntityService.Accept(key, x => ((IEntity)x).Key.ToString() == "MyKey");

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void AcceptByKeyOnPredicate_ExistingGatedValueAndNewValuesDifferAndNewValueMatchesPredicate_UpdatesGatedInstance()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Original Entity" };
			var newAcceptedEntity = new TestEntity(key) { TestTypeDifferentiator = "New Entity" };

			// Called when get is called to get the value in to the gated area...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity })
				.Repeat.Once();

			// Gets the value in to the gated area...
			_gatedEntityService.SetGating(key, true);
			_gatedEntityService.Get(new[] { key });

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { newAcceptedEntity })
				.Repeat.Once();

			// Act
			_gatedEntityService.Accept(key, x => ((IEntity)x).Key.ToString() == "MyKey");

			// Assert
			var gatedEntity = _gatedEntityService.Get(new[] { key }).Single();
			Assert.AreEqual(newAcceptedEntity, gatedEntity);
		}

		[Test]
		public void AcceptByKeyOnPredicate_ExistingGatedValueAndNewValuesDifferAndNewValueDoesntMatchPredicate_DoesntUpdateGatedInstance()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Original Entity" };
			var newAcceptedEntity = new TestEntity(key) { TestTypeDifferentiator = "New Entity" };

			// Called when get is called to get the value in to the gated area...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity })
				.Repeat.Once();

			// Gets the value in to the gated area...
			_gatedEntityService.SetGating(key, true);
			_gatedEntityService.Get(new[] { key });

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { newAcceptedEntity })
				.Repeat.Once();

			// Act
			_gatedEntityService.Accept(key, x => ((IEntity)x).Key.ToString() == "MyOtherKey");

			// Assert
			var gatedEntity = _gatedEntityService.Get(new[] { key }).Single();
			Assert.AreEqual(entity, gatedEntity);
		}

		[Test]
		public void AcceptByKeyOnPredicate_ExistingGatedValueAndNewValuesDiffer_UpdatesMatchingKeyBasedObservables()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Original Entity" };
			var newAcceptedEntity = new TestEntity(key) { TestTypeDifferentiator = "New Entity" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { newAcceptedEntity })
				.Repeat.Once();

			int raiseCount = 0;
			var raisedEntityResources = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x =>
			{
				raiseCount++;
				raisedEntityResources.AddRange(x);
			});

			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			_gatedEntityService.SetGating(key, true);
			subject.OnNext(new ObservableResource<TestEntity>(new[] { newAcceptedEntity }));

			// Act
			_gatedEntityService.Accept(key, x => ((IEntity)x).Key.ToString() == "MyKey");

			// Assert
			Assert.AreEqual(2, raiseCount);
			Assert.AreEqual(entity, raisedEntityResources[0]);
			Assert.AreEqual(newAcceptedEntity, raisedEntityResources[1]);
		}

		[Test]
		public void AcceptByKeyOnPredicate_ExistingGatedValueAndNewValuesDiffer_DoesntUpdateNonMatchingKeyBasedObservables()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Original Entity" };
			var newAcceptedEntity = new TestEntity(key) { TestTypeDifferentiator = "New Entity" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { newAcceptedEntity })
				.Repeat.Once();

			int raiseCount = 0;
			var raisedEntityResources = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x =>
			{
				raiseCount++;
				raisedEntityResources.AddRange(x);
			});

			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			_gatedEntityService.SetGating(key, true);
			subject.OnNext(new ObservableResource<TestEntity>(new[] { newAcceptedEntity }));

			// Act
			_gatedEntityService.Accept(key, x => ((IEntity)x).Key.ToString() == "MyDifferentKey");

			// Assert
			Assert.AreEqual(1, raiseCount);
			Assert.AreEqual(entity, raisedEntityResources[0]);
		}

		[Test]
		public void AcceptByKeyOnPredicate_ExistingGatedValueAndNewValuesDifferAndNewValueMatchesPredicate_UpdatesQueryBasedObservablesThatHaveHadAnUpdateWithMatchingKeyFromTheServiceAlready()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var query = new TestEntityQuery("My Query");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Original Entity" };
			var newAcceptedEntity = new TestEntity(key) { TestTypeDifferentiator = "New Entity" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
				.Return(subject);

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { newAcceptedEntity })
				.Repeat.Once();

			int raiseCount = 0;
			var raisedEntityResources = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { query }).Select(resource => resource.Entities).Subscribe(x =>
			{
				raiseCount++;
				raisedEntityResources.AddRange(x);
			});

			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			_gatedEntityService.SetGating(key, true);
			subject.OnNext(new ObservableResource<TestEntity>(new[] { newAcceptedEntity }));

			// Act
			_gatedEntityService.Accept(key, x => ((IEntity)x).Key.ToString() == "MyKey");

			// Assert
			Assert.AreEqual(2, raiseCount);
			Assert.AreEqual(entity, raisedEntityResources[0]);
			Assert.AreEqual(newAcceptedEntity, raisedEntityResources[1]);
		}

		[Test]
		public void AcceptByKeyOnPredicate_ExistingGatedValueAndNewValuesDifferAndNewValueDoesntMatchPredicate_UpdatesQueryBasedObservablesThatHaveHadAnUpdateWithMatchingKeyFromTheServiceAlready()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var query = new TestEntityQuery("My Query");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Original Entity" };
			var newAcceptedEntity = new TestEntity(key) { TestTypeDifferentiator = "New Entity" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
				.Return(subject);

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { newAcceptedEntity })
				.Repeat.Once();

			int raiseCount = 0;
			var raisedEntityResources = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { query }).Select(resource => resource.Entities).Subscribe(x =>
			{
				raiseCount++;
				raisedEntityResources.AddRange(x);
			});

			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			_gatedEntityService.SetGating(key, true);
			subject.OnNext(new ObservableResource<TestEntity>(new[] { newAcceptedEntity }));

			// Act
			_gatedEntityService.Accept(key, x => ((IEntity)x).Key.ToString() == "MyDifferentKey");

			// Assert
			Assert.AreEqual(1, raiseCount);
			Assert.AreEqual(entity, raisedEntityResources[0]);
		}

		[Test]
		public void AcceptByKeyOnPredicate_ExistingGatedValueAndNewValuesAreSame_DoesNotUpdateMatchingKeyBasedObservables()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Original Entity" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity })
				.Repeat.Once();

			int raiseCount = 0;
			var raisedEntityResources = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x =>
			{
				raiseCount++;
				raisedEntityResources.AddRange(x);
			});

			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			_gatedEntityService.SetGating(key, true);

			// Act
			_gatedEntityService.Accept(key, x => ((IEntity)x).Key.ToString() == "MyKey");

			// Assert
			Assert.AreEqual(1, raiseCount);
			Assert.AreEqual(entity, raisedEntityResources[0]);
		}

		[Test]
		public void AcceptByKeyOnPredicate_ExistingGatedValueAndNewValuesDiffer_DoesNotUpdateQueryBasedObservablesThatHaveHadAnUpdateWithMatchingKeyFromTheServiceAlready()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var query = new TestEntityQuery("My Query");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Original Entity" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(query))))
				.Return(subject);

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity })
				.Repeat.Once();

			int raiseCount = 0;
			var raisedEntityResources = new List<TestEntity>();
			_gatedEntityService.GetObservable(new[] { query }).Select(resource => resource.Entities).Subscribe(x =>
			{
				raiseCount++;
				raisedEntityResources.AddRange(x);
			});

			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));
			_gatedEntityService.SetGating(key, true);

			// Act
			_gatedEntityService.Accept(key, x => ((IEntity)x).Key.ToString() == "MyKey");

			// Assert
			Assert.AreEqual(1, raiseCount);
			Assert.AreEqual(entity, raisedEntityResources[0]);
		}

		[Test]
		public void AcceptByMultipleKeys_BasicCall_OnlyMakesOneCallToTheEntityService()
		{
			// Arrange
			var key1 = new TestEntityKey("Test Key1");
			var key2 = new TestEntityKey("Test Key2");
			var testEntity1 = new TestEntity(key1);
			var testEntity2 = new TestEntity(key2);

			_entityService
				.Stub(x => x.Get(new[] { key1 }))
				.Return(new[] { testEntity1 });
			_gatedEntityService.SetGating(key1, true);
			_gatedEntityService.Get(new[] { key1 });

			_entityService
				.Stub(x => x.Get(new[] { key2 }))
				.Return(new[] { testEntity2 });
			_gatedEntityService.SetGating(key2, true);
			_gatedEntityService.Get(new[] { key2 });

			_entityService
				.Expect(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(
					y => y.Count() == 2 && y.Contains(key1) && y.Contains(key2))))
				.Return(new[] { testEntity1, testEntity2 });

			// Act
			_gatedEntityService.Accept(new[] { key1, key2 });

			// Assert
			_entityService.VerifyAllExpectations();
		}

		[Test]
		public void GetObservable_ByKeyAndUngatedExceptionOnUnderlyingService_RaisesExceptionOnStream()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var exception = new Exception();

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			Exception raisedException = null;
			_gatedEntityService.GetObservable(new[] { key }).Subscribe(x => { }, y => { raisedException = y; });

			// Act
			subject.OnError(exception);

			// Assert
			Assert.IsNotNull(raisedException);
			Assert.AreEqual(exception, raisedException);
		}

		[Test]
		public void SubscribeToGatingServiceObjects_UserGetsFromTheUngatedService_GotValueRaisedOnObservable()
		{
			// Arrange
			const string keyString = "Key";
			var key = new TestEntityKey(keyString);
			var testEntity = new TestEntity(key);

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { testEntity });

			IGatingServiceObject raisedItem = null;
			_gatedEntityService.Subscribe<IGatingServiceObject>(x => { raisedItem = x; });

			// Act
			_gatedEntityService.Get(new[] { key });

			// Assert
			Assert.IsNotNull(raisedItem);
			Assert.AreEqual(keyString, raisedItem.Key);
		}

		[Test]
		public void SubscribeToGatingServiceObjects_GatedValueGetsNewAcceptedInstanceOnGetOnKey_RaisesGatingServiceObject()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity });

			IGatingServiceObject raisedItem = null;
			_gatedEntityService.Subscribe<IGatingServiceObject>(x => { raisedItem = x; });

			_gatedEntityService.SetGating(key, true);

			// Act
			_gatedEntityService.Get(new[] { key });

			// Assert
			Assert.IsNotNull(raisedItem);
			Assert.AreEqual(key.ToString(), raisedItem.Key);
		}

		[Test]
		public void SubscribeToGatingServiceObjects_GatedValueGetsNewAcceptedInstanceOnGetOnQuery_RaisesGatingServiceObject()
		{
			// Arrange
			var query = new TestEntityQuery("My Query");
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			_entityService
				.Stub(x => x.Get(new[] { query }))
				.Return(new[] { entity });

			IGatingServiceObject raisedItem = null;
			_gatedEntityService.Subscribe<IGatingServiceObject>(x => { raisedItem = x; });

			_gatedEntityService.SetGating(key, true);

			// Act
			_gatedEntityService.Get(new[] { query });

			// Assert
			Assert.IsNotNull(raisedItem);
			Assert.AreEqual(key.ToString(), raisedItem.Key);
		}

		[Test]
		public void SubscribeToGatingServiceObjects_GatedValueGetsNewAcceptedInstanceOnSetGatingByKey_RaisesGatingServiceObject()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity });

			_gatedEntityService.Get(new[] { key });

			IGatingServiceObject raisedItem = null;
			_gatedEntityService.Subscribe<IGatingServiceObject>(x => { raisedItem = x; });

			// Act
			_gatedEntityService.SetGating(key, true);

			// Assert
			Assert.IsNotNull(raisedItem);
			Assert.AreEqual(key.ToString(), raisedItem.Key);
		}

		[Test]
		public void SubscribeToGatingServiceObjects_GatedValueGetsNewAcceptedInstanceOnSetGatingOnAll_RaisesGatingServiceObject()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key);

			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.FirstOrDefault() == key)))
				.Return(new[] { entity });

			_gatedEntityService.Get(new[] { key });

			IGatingServiceObject raisedItem = null;
			_gatedEntityService.Subscribe<IGatingServiceObject>(x => { raisedItem = x; });

			// Act
			_gatedEntityService.SetGating(true);

			// Assert
			Assert.IsNotNull(raisedItem);
			Assert.AreEqual(key.ToString(), raisedItem.Key);
		}

		[Test]
		public void SubscribeToGatingServiceObjects_GatedValueGetsNewAcceptedInstanceOnAcceptOnKey_RaisesGatingServiceObject()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "New" };

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity })
				.Repeat.Once();
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { newEntity })
				.Repeat.Once();

			_gatedEntityService.SetGating(key, true);
			_gatedEntityService.Get(new[] { key });

			IGatingServiceObject raisedItem = null;
			_gatedEntityService.Subscribe<IGatingServiceObject>(x => { raisedItem = x; });

			// Act
			_gatedEntityService.Accept(key);

			// Assert
			Assert.IsNotNull(raisedItem);
			Assert.AreEqual(key.ToString(), raisedItem.Key);
		}

		[Test]
		public void SubscribeToGatingServiceObjects_GatedValueGetsNewAcceptedInstanceOnAcceptOnAll_RaisesGatingServiceObject()
		{
			// Arrange
			var key = new TestEntityKey("My Key");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "Old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "New" };

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity })
				.Repeat.Once();
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single() == key)))
				.Return(new[] { newEntity })
				.Repeat.Once();

			_gatedEntityService.SetGating(key, true);
			_gatedEntityService.Get(new[] { key });

			IGatingServiceObject raisedItem = null;
			_gatedEntityService.Subscribe<IGatingServiceObject>(x => { raisedItem = x; });

			// Act
			_gatedEntityService.Accept();

			// Assert
			Assert.IsNotNull(raisedItem);
			Assert.AreEqual(key.ToString(), raisedItem.Key);
		}

		[Test]
		public void SubscribeToGatingServiceObjects_GatedValueGetsUpdateOnGetByQueryAndEntityIsNewValue_RaisesGatingServiceObject()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var query = new TestEntityQuery("MyQuery");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "new" };

			_entityService
				.Stub(x => x.Get(new[] { query }))
				.Return(new[] { entity })
				.Repeat.Once();

			_entityService
				.Stub(x => x.Get(new[] { query }))
				.Return(new[] { newEntity })
				.Repeat.Once();

			_gatedEntityService.SetGating(key, true);
			_gatedEntityService.Get(new[] { query });

			IGatingServiceObject raisedItem = null;
			_gatedEntityService.Subscribe<IGatingServiceObject>(x => { raisedItem = x; });

			// Act
			_gatedEntityService.Get(new[] { query }); // this gets the newEntity value from the underlying service

			// Assert
			Assert.IsNotNull(raisedItem);
			Assert.AreEqual(key.ToString(), raisedItem.Key);
		}

		[Test]
		public void SubscribeToGatingServiceObjects_GatedValueGetsUpdateOnSubscribe_RaisesGatingServiceObject()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key);

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			_gatedEntityService.SetGating(key, true);
			_gatedEntityService.GetObservable(new[] { key }).Subscribe(x => { });

			IGatingServiceObject raisedItem = null;
			_gatedEntityService.Subscribe<IGatingServiceObject>(x => { raisedItem = x; });

			// Act
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));

			// Assert
			Assert.IsNotNull(raisedItem);
			Assert.AreEqual(key.ToString(), raisedItem.Key);
		}

		[Test]
		public void SubscribeToGatingServiceObjects_GatedValueGetsUpdateOnSubscribeWhenValueHasPreviouslyBeenGot_RaisesGatingServiceObject()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key);
			var newEntity = new TestEntity(key);

			var subject = new Subject<ObservableResource<TestEntity>>();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity });

			_gatedEntityService.SetGating(key, true);
			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.GetObservable(new[] { key }).Subscribe(x => { });

			IGatingServiceObject raisedItem = null;
			_gatedEntityService.Subscribe<IGatingServiceObject>(x => { raisedItem = x; });

			// Act
			subject.OnNext(new ObservableResource<TestEntity>(new[] { newEntity }));

			// Assert
			Assert.IsNotNull(raisedItem);
			Assert.AreEqual(key.ToString(), raisedItem.Key);
		}

		[Test]
		public void SubscribeToGatingServiceObjects_PredicateAcceptOnValueThatDoesntMatchPredicate_RaisesGatingServiceObject()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "new" };

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity })
				.Repeat.Once();
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { newEntity })
				.Repeat.Once();

			_gatedEntityService.SetGating(key, true);
			_gatedEntityService.Get(new[] { key });

			IGatingServiceObject raisedItem = null;
			_gatedEntityService.Subscribe<IGatingServiceObject>(x => { raisedItem = x; });

			// Act
			_gatedEntityService.Accept(key, x => ((IEntity<TestEntity, BasicKey<TestEntity>>)x).Key.ToString() == "My Other Key");

			// Assert
			Assert.IsNotNull(raisedItem);
			Assert.AreEqual(key.ToString(), raisedItem.Key);
		}

		[Test]
		public void SubscribeToGatingServiceObjects_PredicateAcceptOnValueThatDoesntMatchPredicateAndHasntBeenGot_RaisesGatingServiceObject()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "old" };

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity })
				.Repeat.Once();

			_gatedEntityService.SetGating(key, true);

			IGatingServiceObject raisedItem = null;
			_gatedEntityService.Subscribe<IGatingServiceObject>(x => { raisedItem = x; });

			// Act
			_gatedEntityService.Accept(key, x => ((IEntity<TestEntity, BasicKey<TestEntity>>)x).Key.ToString() == "My Other Key");

			// Assert
			Assert.IsNotNull(raisedItem);
			Assert.AreEqual(key.ToString(), raisedItem.Key);
		}

		[Test]
		public void SubscribeToGatingServiceObjects_AcceptOnAllWithPreviouslyRetrievedKeysThatDontMatchPredicate_RaisesGatingServiceObject()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "new" };

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity })
				.Repeat.Once();
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single() == key)))
				.Return(new[] { newEntity })
				.Repeat.Once();

			_gatedEntityService.SetGating(key, true);
			_gatedEntityService.Get(new[] { key });

			IGatingServiceObject raisedItem = null;
			_gatedEntityService.Subscribe<IGatingServiceObject>(x => { raisedItem = x; });

			// Act
			_gatedEntityService.Accept(x => ((IEntity<TestEntity, BasicKey<TestEntity>>)x).Key.ToString() == "My Other Key");

			// Assert
			Assert.IsNotNull(raisedItem);
			Assert.AreEqual(key.ToString(), raisedItem.Key);
		}

		[Test]
		public void SubscribeToGatingServiceObjects_GatedValueGetsUpdateOnAcceptAllAndMatchingPredicate_RaisesGatingServiceObject()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "new" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			// this gets called on the call to Accept...
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single() == key)))
				.Return(new[] { newEntity })
				.Repeat.Once();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			_gatedEntityService.SetGating(key, true);
			_gatedEntityService.GetObservable(new[] { key }).Subscribe(x => { });

			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));

			IGatingServiceObject raisedItem = null;
			_gatedEntityService.Subscribe<IGatingServiceObject>(x => { raisedItem = x; });

			// Act
			_gatedEntityService.Accept(x => true);

			// Assert
			Assert.IsNotNull(raisedItem);
			Assert.AreEqual(key.ToString(), raisedItem.Key);
		}

		[Test]
		public void SubscribeToGatingServiceObjects_GatedValueGetsUpdateOnAcceptAllAndDoesntMatchPredicate_RaisesGatingServiceObject()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");
			var entity = new TestEntity(key) { TestTypeDifferentiator = "old" };
			var newEntity = new TestEntity(key) { TestTypeDifferentiator = "new" };

			var subject = new Subject<ObservableResource<TestEntity>>();

			// this gets called on the call to Accept...
			_entityService
				.Stub(x => x.Get(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(y => y.Single() == key)))
				.Return(new[] { newEntity })
				.Repeat.Once();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			_gatedEntityService.SetGating(key, true);
			_gatedEntityService.GetObservable(new[] { key }).Subscribe(x => { });

			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity }));

			IGatingServiceObject raisedItem = null;
			_gatedEntityService.Subscribe<IGatingServiceObject>(x => { raisedItem = x; });

			// Act
			_gatedEntityService.Accept(x => ((IEntity<TestEntity, BasicKey<TestEntity>>)x).Key.ToString() == "My Other Key");

			// Assert
			Assert.IsNotNull(raisedItem);
			Assert.AreEqual(key.ToString(), raisedItem.Key);
		}

		[Test]
		public void IsGated_NoKeysAreGated_ReturnsNotGated()
		{
			// Arrange

			// Act
			var gatingState = _gatedEntityService.IsGated();

			// Assert
			Assert.AreEqual(GatingState.NotGated, gatingState);
		}

		[Test]
		public void IsGated_AllKeysAreGatedAndDefaultGatingIsOn_ReturnsFullyGated()
		{
			// Arrange
			_gatedEntityService.SetGating(true);

			// Act
			var gatingState = _gatedEntityService.IsGated();

			// Assert
			Assert.AreEqual(GatingState.FullyGated, gatingState);
		}

		[Test]
		public void IsGated_DefaultGatingIsOnButSomeKeysAreUngated_ReturnsPartiallyGated()
		{
			// Arrange
			var testEntityKey = new TestEntityKey("Test Key");

			_entityService
				.Stub(x => x.Get(new[] { testEntityKey }))
				.Return(new TestEntity[] { });

			_gatedEntityService.Get(new[] { testEntityKey });
			_gatedEntityService.SetGating(true);
			_gatedEntityService.SetGating(testEntityKey, false);

			// Act
			var gatingState = _gatedEntityService.IsGated();

			// Assert
			Assert.AreEqual(GatingState.PartiallyGated, gatingState);
		}

		[Test]
		public void IsGated_DefaultGatingIsOffButSomeKeysAreGated_ReturnsPartiallyGated()
		{
			// Arrange
			var testEntityKey = new TestEntityKey("Test Key");

			_entityService
				.Stub(x => x.Get(new[] { testEntityKey }))
				.Return(new TestEntity[] { });

			_gatedEntityService.Get(new[] { testEntityKey });
			_gatedEntityService.SetGating(testEntityKey, true);

			// Act
			var gatingState = _gatedEntityService.IsGated();

			// Assert
			Assert.AreEqual(GatingState.PartiallyGated, gatingState);
		}

		[Test]
		public void SubscribeToGatingState_GatingStateChangedFromOffToFullyOn_RaisesUpdate()
		{
			// Arrange
			GatingState gatingStateRaised = GatingState.NotGated;

			_gatedEntityService.Subscribe<GatingState>(x => { gatingStateRaised = x; });

			// Act
			_gatedEntityService.SetGating(true);

			// Assert
			Assert.AreEqual(GatingState.FullyGated, gatingStateRaised);
		}

		[Test]
		public void SubscribeToGatingState_GatingStateChangedFromFullyOnToOff_RaisesUpdate()
		{
			// Arrange
			GatingState gatingStateRaised = GatingState.FullyGated;
			_gatedEntityService.SetGating(true);

			_gatedEntityService.Subscribe<GatingState>(x => { gatingStateRaised = x; });

			// Act
			_gatedEntityService.SetGating(false);

			// Assert
			Assert.AreEqual(GatingState.NotGated, gatingStateRaised);
		}

		[Test]
		public void SubscribeToGatingState_GatingStateChangedFromOffToPartiallyOn_RaisesUpdate()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");

			GatingState gatingStateRaised = GatingState.NotGated;

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new TestEntity[] { });
			_gatedEntityService.Get(new[] { key });

			_gatedEntityService.Subscribe<GatingState>(x => { gatingStateRaised = x; });

			// Act
			_gatedEntityService.SetGating(key, true);

			// Assert
			Assert.AreEqual(GatingState.PartiallyGated, gatingStateRaised);
		}

		[Test]
		public void SubscribeToGatingState_GatingStateChangedFromOnToPartiallyOn_RaisesUpdate()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");

			GatingState gatingStateRaised = GatingState.FullyGated;

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new TestEntity[] { });
			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(true);

			_gatedEntityService.Subscribe<GatingState>(x => { gatingStateRaised = x; });

			// Act
			_gatedEntityService.SetGating(key, false);

			// Assert
			Assert.AreEqual(GatingState.PartiallyGated, gatingStateRaised);
		}

		[Test]
		public void SubscribeToGatingState_GatingStateChangedFromPartiallyOnToOff_RaisesUpdate()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");

			GatingState gatingStateRaised = GatingState.PartiallyGated;

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new TestEntity[] { });
			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(key, true);

			_gatedEntityService.Subscribe<GatingState>(x => { gatingStateRaised = x; });

			// Act
			_gatedEntityService.SetGating(key, false);

			// Assert
			Assert.AreEqual(GatingState.NotGated, gatingStateRaised);
		}

		[Test]
		public void SubscribeToGatingState_GatingStateChangedFromPartiallyOnToOn_RaisesUpdate()
		{
			// Arrange
			var key = new TestEntityKey("MyKey");

			GatingState gatingStateRaised = GatingState.PartiallyGated;

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new TestEntity[] { });
			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(true);
			_gatedEntityService.SetGating(key, false);

			_gatedEntityService.Subscribe<GatingState>(x => { gatingStateRaised = x; });

			// Act
			_gatedEntityService.SetGating(key, true);

			// Assert
			Assert.AreEqual(GatingState.FullyGated, gatingStateRaised);
		}

		[Test]
		public void GetLatestObjects_NothingPreviouslyReturnedThroughTheService_ReturnsEmptyList()
		{
			// Arrange
			// Act
			var gatingServiceObjects = _gatedEntityService.GetLatestObjects();

			// Assert
			Assert.AreEqual(0, gatingServiceObjects.Count());
		}

		[Test]
		public void GetLatestObjects_AnObjectPreviouslyGotThroughTheService_ReturnsListWithOneItem()
		{
			// Arrange
			const string keyString = "TestKey";
			var key = new TestEntityKey(keyString);
			var entity = new TestEntity(key);

			_entityService
				.Expect(x => x.Get(new[] { key }))
				.Return(new[] { entity });

			_gatedEntityService.Get(new[] { key });

			// Act
			var gatingServiceObjects = _gatedEntityService.GetLatestObjects();

			// Assert
			Assert.AreEqual(1, gatingServiceObjects.Count());
			Assert.AreEqual(keyString, gatingServiceObjects.Single().Key);
		}

		[Test]
		public void GetLatestObjects_TwoDifferentObjectsPreviouslyGotThroughTheService_ReturnsListWithTwoItems()
		{
			// Arrange
			const string keyString1 = "TestKey1";
			const string keyString2 = "TestKey2";
			var key1 = new TestEntityKey(keyString1);
			var key2 = new TestEntityKey(keyString2);
			var entity1 = new TestEntity(key1);
			var entity2 = new TestEntity(key2);

			_entityService
				.Expect(x => x.Get(new[] { key1 }))
				.Return(new[] { entity1 });
			_entityService
				.Expect(x => x.Get(new[] { key2 }))
				.Return(new[] { entity2 });

			_gatedEntityService.Get(new[] { key1 });
			_gatedEntityService.Get(new[] { key2 });

			// Act
			var gatingServiceObjects = _gatedEntityService.GetLatestObjects();

			// Assert
			Assert.AreEqual(2, gatingServiceObjects.Count());
			var gatingServiceObjectKeys = gatingServiceObjects.Select(x => x.Key);
			Assert.IsTrue(gatingServiceObjectKeys.Contains(keyString1));
			Assert.IsTrue(gatingServiceObjectKeys.Contains(keyString2));
		}

		[Test]
		public void GetLatestObjects_TwoObjectsWithTheSameKeyPreviouslyGotThroughTheService_ReturnsListWithOneItem()
		{
			// Arrange
			const string keyString = "TestKey1";
			var key = new TestEntityKey(keyString);
			var entity1 = new TestEntity(key) { TestTypeDifferentiator = "1" };
			var entity2 = new TestEntity(key) { TestTypeDifferentiator = "2" };

			_entityService
				.Expect(x => x.Get(new[] { key }))
				.Return(new[] { entity1 })
				.Repeat.Once();
			_entityService
				.Expect(x => x.Get(new[] { key }))
				.Return(new[] { entity2 })
				.Repeat.Once();

			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.Get(new[] { key });

			// Act
			var gatingServiceObjects = _gatedEntityService.GetLatestObjects();

			// Assert
			Assert.AreEqual(1, gatingServiceObjects.Count());
			Assert.AreEqual(keyString, gatingServiceObjects.First().Key);
		}

		[Test]
		public void GetLatestObjects_AfterGet_LatestDateIsEntityTimestamp()
		{
			// Arrange
			var key = new TestEntityKey("TestKey");
			var timestamp = new DateTime(2011, 12, 25, 12, 23, 34);
			var entity = new TestEntity(key) { Timestamp = timestamp };

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity });

			_gatedEntityService.Get(new[] { key });

			// Act
			var gatingServiceObjects = _gatedEntityService.GetLatestObjects();

			// Assert
			foreach (var gatingServiceObject in gatingServiceObjects)
			{
				Assert.AreEqual(timestamp, gatingServiceObject.LatestTime);
			}
		}

		[Test]
		public void GetLatestObjects_AfterGet_AcceptedDateIsEntityTimestamp()
		{
			// Arrange
			var key = new TestEntityKey("TestKey");
			var timestamp = new DateTime(2011, 12, 25, 12, 23, 34);
			var entity = new TestEntity(key) { Timestamp = timestamp };

			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity });

			_gatedEntityService.Get(new[] { key });

			// Act
			var gatingServiceObjects = _gatedEntityService.GetLatestObjects();

			// Assert
			foreach (var gatingServiceObject in gatingServiceObjects)
			{
				Assert.AreEqual(timestamp, gatingServiceObject.AcceptedTime);
			}
		}

		[Test]
		public void GetLatestObjects_SetGatingOnAll_LatestDateShouldBeTimestampFromLatestEntity()
		{
			// Arrange
			var key = new TestEntityKey("TestKey");
			var timestamp1 = new DateTime(2011, 12, 25, 12, 23, 34);
			var timestamp2 = new DateTime(2012, 1, 2, 3, 4, 5);
			var entity1 = new TestEntity(key) { Timestamp = timestamp1 };
			var entity2 = new TestEntity(key) { Timestamp = timestamp2 };

			// For the Get() call...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity1 })
				.Repeat.Once();

			// For the SetGating() call...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity2 })
				.Repeat.Once();

			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(true);

			// Act
			var gatingServiceObjects = _gatedEntityService.GetLatestObjects();

			// Assert
			foreach (var gatingServiceObject in gatingServiceObjects)
			{
				Assert.AreEqual(timestamp2, gatingServiceObject.LatestTime);
			}
		}

		[Test]
		public void GetLatestObjects_SetGatingOnAll_AcceptedDateShouldBeTimestampFromLatestEntity()
		{
			// Arrange
			var key = new TestEntityKey("TestKey");
			var timestamp1 = new DateTime(2011, 12, 25, 12, 23, 34);
			var timestamp2 = new DateTime(2012, 1, 2, 3, 4, 5);
			var entity1 = new TestEntity(key) { Timestamp = timestamp1 };
			var entity2 = new TestEntity(key) { Timestamp = timestamp2 };

			// For the Get() call...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity1 })
				.Repeat.Once();

			// For the SetGating() call...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity2 })
				.Repeat.Once();

			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(true);

			// Act
			var gatingServiceObjects = _gatedEntityService.GetLatestObjects();

			// Assert
			foreach (var gatingServiceObject in gatingServiceObjects)
			{
				Assert.AreEqual(timestamp2, gatingServiceObject.AcceptedTime);
			}
		}

		[Test]
		public void GetLatestObjects_GatedAndObjectRaised_LatestTimeShouldBeRaisedEntityTimestamp()
		{
			// Arrange
			var key = new TestEntityKey("TestKey");
			var timestamp1 = new DateTime(2011, 12, 25, 12, 23, 34);
			var timestamp2 = new DateTime(2012, 1, 2, 3, 4, 5);
			var timestamp3 = new DateTime(2012, 2, 3, 4, 5, 6);
			var entity1 = new TestEntity(key) { Timestamp = timestamp1 };
			var entity2 = new TestEntity(key) { Timestamp = timestamp2 };
			var entity3 = new TestEntity(key) { Timestamp = timestamp3 };

			var subject = new Subject<ObservableResource<TestEntity>>();

			// For the Get() call...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity1 })
				.Repeat.Once();

			// For the SetGating() call...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity2 })
				.Repeat.Once();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(true);
			_gatedEntityService.GetObservable(new[] { key }).Subscribe(x => { });

			// Act
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity3 }));

			// Assert
			var gatingServiceObjects = _gatedEntityService.GetLatestObjects();
			foreach (var gatingServiceObject in gatingServiceObjects)
			{
				Assert.AreEqual(timestamp3, gatingServiceObject.LatestTime);
			}
		}

		[Test]
		public void GetLatestObjects_GatedAndObjectRaised_AcceptedTimeShouldBeEntityRetrievedAtPointOfGatingsTimestamp()
		{
			// Arrange
			var key = new TestEntityKey("TestKey");
			var timestamp1 = new DateTime(2011, 12, 25, 12, 23, 34);
			var timestamp2 = new DateTime(2012, 1, 2, 3, 4, 5);
			var timestamp3 = new DateTime(2012, 2, 3, 4, 5, 6);
			var entity1 = new TestEntity(key) { Timestamp = timestamp1 };
			var entity2 = new TestEntity(key) { Timestamp = timestamp2 };
			var entity3 = new TestEntity(key) { Timestamp = timestamp3 };

			var subject = new Subject<ObservableResource<TestEntity>>();

			// For the Get() call...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity1 })
				.Repeat.Once();

			// For the SetGating() call...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity2 })
				.Repeat.Once();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(true);
			_gatedEntityService.GetObservable(new[] { key }).Subscribe(x => { });

			// Act
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity3 }));

			// Assert
			var gatingServiceObjects = _gatedEntityService.GetLatestObjects();
			foreach (var gatingServiceObject in gatingServiceObjects)
			{
				Assert.AreEqual(timestamp2, gatingServiceObject.AcceptedTime);
			}
		}

		[Test]
		public void GetLatestObjects_GatedAndObjectRaisedThenSetGatingOff_LatestTimeShouldBeGotEntityTimestamp()
		{
			// Arrange
			var key = new TestEntityKey("TestKey");
			var timestamp1 = new DateTime(2011, 12, 25, 12, 23, 34);
			var timestamp2 = new DateTime(2012, 1, 2, 3, 4, 5);
			var timestamp3 = new DateTime(2012, 2, 3, 4, 5, 6);
			var timestamp4 = new DateTime(2012, 3, 4, 5, 6, 7);
			var entity1 = new TestEntity(key) { Timestamp = timestamp1 };
			var entity2 = new TestEntity(key) { Timestamp = timestamp2 };
			var entity3 = new TestEntity(key) { Timestamp = timestamp3 };
			var entity4 = new TestEntity(key) { Timestamp = timestamp4 };

			var subject = new Subject<ObservableResource<TestEntity>>();

			// For the Get() call...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity1 })
				.Repeat.Once();

			// For the SetGating(true) call...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity2 })
				.Repeat.Once();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			// For the SetGating(false) call...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity4 })
				.Repeat.Once();

			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(true);
			_gatedEntityService.GetObservable(new[] { key }).Subscribe(x => { });
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity3 }));

			// Act
			_gatedEntityService.SetGating(false);

			// Assert
			var gatingServiceObjects = _gatedEntityService.GetLatestObjects();
			foreach (var gatingServiceObject in gatingServiceObjects)
			{
				Assert.AreEqual(timestamp4, gatingServiceObject.LatestTime);
			}
		}

		[Test]
		public void GetLatestObjects_GatedAndObjectRaisedThenSetGatingOff_AcceptedTimeShouldBeGotEntityTimestamp()
		{
			// Arrange
			var key = new TestEntityKey("TestKey");
			var timestamp1 = new DateTime(2011, 12, 25, 12, 23, 34);
			var timestamp2 = new DateTime(2012, 1, 2, 3, 4, 5);
			var timestamp3 = new DateTime(2012, 2, 3, 4, 5, 6);
			var timestamp4 = new DateTime(2012, 3, 4, 5, 6, 7);
			var entity1 = new TestEntity(key) { Timestamp = timestamp1 };
			var entity2 = new TestEntity(key) { Timestamp = timestamp2 };
			var entity3 = new TestEntity(key) { Timestamp = timestamp3 };
			var entity4 = new TestEntity(key) { Timestamp = timestamp4 };

			var subject = new Subject<ObservableResource<TestEntity>>();

			// For the Get() call...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity1 })
				.Repeat.Once();

			// For the SetGating(true) call...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity2 })
				.Repeat.Once();

			_entityService
				.Stub(x => x.GetObservable(Arg<IEnumerable<IQuery<TestEntity>>>.Matches(queries => queries.Single().Equals(key))))
				.Return(subject);

			// For the SetGating(false) call...
			_entityService
				.Stub(x => x.Get(new[] { key }))
				.Return(new[] { entity4 })
				.Repeat.Once();

			_gatedEntityService.Get(new[] { key });
			_gatedEntityService.SetGating(true);
			_gatedEntityService.GetObservable(new[] { key }).Subscribe(x => { });
			subject.OnNext(new ObservableResource<TestEntity>(new[] { entity3 }));

			// Act
			_gatedEntityService.SetGating(false);

			// Assert
			var gatingServiceObjects = _gatedEntityService.GetLatestObjects();
			foreach (var gatingServiceObject in gatingServiceObjects)
			{
				Assert.AreEqual(timestamp4, gatingServiceObject.AcceptedTime);
			}
		}
	}
}