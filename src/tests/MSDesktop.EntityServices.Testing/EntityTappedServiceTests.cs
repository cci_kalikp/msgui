﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using EntityServicesTests.Entities;
using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.EntityServices.Services.Cloning;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;
using NUnit.Framework;
using Rhino.Mocks;

namespace EntityServicesTests
{
    [TestFixture]
    public class EntityTappedServiceTests : TestBase
    {
        private EntityTappedService<TestEntity> _entityTappedService;
        private IEntityService<TestEntity> _entityServiceMock;

        [SetUp]
        public void SetUp()
        {
            _entityServiceMock = MockRepository.GenerateMock<IEntityService<TestEntity>>();
            _entityTappedService = new EntityTappedService<TestEntity>(_entityServiceMock, MockRepository.GenerateMock<IEntityServiceExceptionService>(),new EntityCloning());
        }

        [Test]
        public void Get_BasicCall_CallsGetOnUnderlyingEntityService()
        {
            // Arrange
            var key = new TestEntityKey("TestKey");

            _entityServiceMock
                .Expect(x => x.Get(new[] {key}))
                .Return(new List<TestEntity>());

            // Act
            _entityTappedService.Get(new[] {key});

            // Assert
            _entityServiceMock.AssertWasCalled(x => x.Get(new[] {key}));
        }

        [Test]
        public void Get_ValueReturnedFromUnderlyingService_ReturnsValueOnTap()
        {
            // Arrange
            var key = new TestEntityKey("TestKey");
            var entity = new TestEntity(key);

            TestEntity tappedEntity = null;
            _entityTappedService.Select(resource => resource.Entities).Subscribe(x => tappedEntity = x.FirstOrDefault());

            _entityServiceMock
                .Stub(x => x.Get(new[] {key}))
                .Return(new[] { entity });

            // Act
            Run(() => _entityTappedService.Get(new[] {key}));

            // Assert
            Assert.AreEqual(entity, tappedEntity);
        }

        [Test]
        public void Get_ValueReturnedFromUnderlyingService_ReturnsValue()
        {
            // Arrange
            var key = new TestEntityKey("TestKey");
            var entity = new TestEntity(key);
            var entities = new List<TestEntity>{ entity };

            _entityServiceMock
                .Stub(x => x.Get(new[] {key}))
                .Return(entities);

            // Act
            var returnedEntities = _entityTappedService.Get(new[] {key});

            // Assert
            Assert.AreEqual(entities.Count(), returnedEntities.Count());
            Assert.AreEqual(entities.First(), returnedEntities.First());
        }
        
        [Test]
        public void GetObservable_BasicCall_CallsGetObservableOnUnderlyingService()
        {
            // Arrange
            var key = new TestEntityKey("TestKey");

            _entityServiceMock
                .Expect(x => x.GetObservable(new[] {key}))
                .Return(new Subject<ObservableResource<TestEntity>>());

            // Act
            _entityTappedService.GetObservable(new[] {key}).Subscribe(x => { });

            // Assert
            _entityServiceMock.AssertWasCalled(x => x.GetObservable(new[] {key}));
        }

        [Test]
        public void GetObservable_OnNext_ReturnsOnTap()
        {
            // Arrange
            var key = new TestEntityKey("TestKey");
            var entity = new TestEntity(key);

            TestEntity tappedEntity = null;
            _entityTappedService.Select(resource => resource.Entities).Subscribe(x => tappedEntity = x.FirstOrDefault());

            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.GetObservable(new[] {key}))
                .Return(subject);

            _entityTappedService.GetObservable(new[] {key}).Subscribe(x => { });

            // Act
            Run(() => subject.OnNext(new ObservableResource<TestEntity>(new[] {entity})));

            // Assert
            Assert.AreEqual(entity, tappedEntity);
        }

        [Test]
        public void GetObservable_OnNext_ReturnsValueOnReturnedStream()
        {
            // Arrange
            var key = new TestEntityKey("TestKey");
            var entity = new TestEntity(key);

            var subject = new Subject<ObservableResource<TestEntity>>();

            _entityServiceMock
                .Stub(x => x.GetObservable(new[] {key}))
                .Return(subject);

            TestEntity raisedEntity = null;
            _entityTappedService.GetObservable(new[] { key }).Select(resource => resource.Entities).Subscribe(x => raisedEntity = x.FirstOrDefault());

            // Act
            subject.OnNext(new ObservableResource<TestEntity>(new[] {entity}));

            // Assert
            Assert.AreEqual(entity, raisedEntity);
        }

        [Test]
        public void Publish_BasicCall_CallsUnderlyingService()
        {
            // Arrange
            var key = new TestEntityKey("TestKey");
            var entity = new TestEntity(key);

            _entityServiceMock.Expect(x => x.Publish(new[] {entity}));

            // Act
            _entityTappedService.Publish(new[] {entity});

            // Assert
            _entityServiceMock.AssertWasCalled(x => x.Publish(new[] {entity}));
        }


        [Test]
        public void Delete_BasicCall_CallsDeleteOnUnderlyingService()
        {
            // Arrange
            var key = new TestEntityKey("TestKey");

            _entityServiceMock.Expect(x => x.Delete(new[] {key}));

            // Act
            _entityTappedService.Delete(new[] {key});

            // Assert
            _entityServiceMock.AssertWasCalled(x => x.Delete(new[] {key}));
        }
    }
}
