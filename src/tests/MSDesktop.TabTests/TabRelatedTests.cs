﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace MSDesktop.TabTests
{
    /// <summary>
    /// Summary description for TabRelatedTests
    /// </summary>
    [CodedUITest]
    public class TabRelatedTests
    {
        public TabRelatedTests()
        {
            System.Diagnostics.Process.Start(@"..\..\..\..\build\common\ms\csc\Debug\examples\CommunicationExamples\CommunicationExamples.exe");
        }


        [TestMethod]
        public void CodedUITestMethod1()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
            this.UIMap.RecordedM2MTypeMessage();
            this.UIMap.RecordedQuitApp();
        }

        [TestMethod]
        public void ClickToAddNewTab()
        {
            this.UIMap.RecordedCloseAllButThisTab();
            this.UIMap.AssertIsTheFirstTab();
            this.UIMap.RecordedClickOnNewTab();
            this.UIMap.AssertIsTheSecondTab();
            this.UIMap.RecordedQuitApp();
        }

        [TestMethod]
        public void ClickToCloseAllButThisTab()
        {
            this.UIMap.RecordedCloseAllButThisTab();
            this.UIMap.AssertIsTheFirstTab();
            this.UIMap.RecordedQuitApp();
        }

        [TestMethod]
        public void ClickToCloseThisTab()
        {
            this.UIMap.RecordedClickOnNewTab();
            this.UIMap.RecordedClickSecondTab();
            this.UIMap.RecordedCloseSecondTab();
            this.UIMap.AssertIsTheFirstTab();
            this.UIMap.RecordedQuitApp();
        }


        

        [TestMethod]
        public void MoveFirstTabToRight()
        {
            this.UIMap.RecordedClickOnNewTab();
            this.UIMap.RecordedClickFirstTab();
            this.UIMap.AssertIsTheFirstTab();
            this.UIMap.RecordedMoveFirstTabToRight();
            this.UIMap.AssertIsTheSecondTab();
            this.UIMap.RecordedQuitApp();
        }

        [TestMethod]
        public void MoveSecondTabToLeft()
        {
            this.UIMap.RecordedClickOnNewTab();
            this.UIMap.RecordedClickSecondTab();
            this.UIMap.AssertIsTheSecondTab();
            this.UIMap.RecordedMoveSecondTabToLeft();
            this.UIMap.AssertIsTheFirstTab();
            this.UIMap.RecordedQuitApp();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //    // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //    // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        public UIMap UIMap
        {
            get
            {
                if ((this.m_map == null))
                {
                    this.m_map = new UIMap();
                }

                return this.m_map;
            }
        }

        private UIMap m_map;
    }
}
