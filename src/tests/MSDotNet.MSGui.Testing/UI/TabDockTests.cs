﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/UI/TabDockTests.cs#6 $
// $Change: 839595 $
// $DateTime: 2013/07/29 12:06:16 $
// $Author: milosp $

using System.Collections.ObjectModel;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.HidingLocking;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using Rhino.Mocks;

namespace MSDotNet.MSGui.Testing.UI
{
  [TestClass]
  public class TabDockTests
  {
    [TestMethod]
    public void ConstructorTest()
    {
        GetTabbedDockViewModel();
    }

    [TestMethod]
    public void MainTabInitialized()
    {
        var td = GetTabbedDockViewModel();
        td.AddTab(TabbedDockViewModel.MAIN_TAB_NAME);
        ReadOnlyCollection<ShellTabViewModel> tabs = td.GetTabs();
        Assert.AreEqual(1, tabs.Count);
        Assert.AreEqual("Main", tabs[0].Title);
        Assert.AreEqual("Main", ((ShellTabViewModel)td.Tabs.CurrentItem).Title);
    }

    [TestMethod]
    public void NoWorkspacesInitially()
    {
        var td = GetTabbedDockViewModel();
        Assert.AreEqual(0, td.Workspaces.Count);
    }

    [TestMethod]
    public void AddOneNewTab()
    {
        var td = GetTabbedDockViewModel();
        td.AddTab(TabbedDockViewModel.MAIN_TAB_NAME);
        td.AddTab("NewTab");
        Assert.AreEqual(2, td.GetTabs().Count);
        ShellTabViewModel newtab = td.GetTabs()[1];
        Assert.AreEqual("NewTab", newtab.Title);
        //current tab is set to the new tab
        Assert.AreEqual("NewTab", ((ShellTabViewModel)td.Tabs.CurrentItem).Title);      
    }

    private static TabbedDockViewModel GetTabbedDockViewModel()
    {
        var cmForTabCreation = MockRepository.GenerateMock<IChromeManager>();
        cmForTabCreation.Stub(x_ => x_.AddWidget("", new InitialWidgetParameters()))
            .IgnoreArguments().Return(new WidgetViewContainer(""));
        WidgetViewContainerExtensions.chromeManager = cmForTabCreation;

        var container = new UnityContainer();
        var cmForTabbedDock = MockRepository.GenerateMock<IChromeManager>();
        cmForTabbedDock
            .Stub(x_ => x_.AddWidget(null, null, null))
            .IgnoreArguments()
            .Repeat.Any()
            .Do((System.Func<string, InitialWidgetParameters, IWidgetViewContainer, IWidgetViewContainer>)
                ((factoryId_, parameters_, parent_) => new ShellTabViewModel(factoryId_, new InitialWidgetParameters())
                                                            {Title = parameters_.Text}));
        container.RegisterInstance(cmForTabbedDock);
        return new TabbedDockViewModel(new ChromeRegistry(), container, new HidingLockingManager());
    }
  }

}
