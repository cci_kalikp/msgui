﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/UI/FrameworkTests.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Threading;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Themes;

namespace MSDotNet.MSGui.Testing.UI
{
  [TestClass]
  public class FrameworkTests
  {
    [TestMethod]
    public void NoExceptionsInFrameworkConstructor()
    {
      Framework framework = new Framework();
    }

    [TestMethod]    
    public void NoExceptionsWhenStartFramework()
    {
      Framework framework = null;
      Exception exception = null;
      bool threadStarted = false;
      Thread thread = new Thread(state_ =>
                                   {
                                     threadStarted = true;
                                     try
                                     {
                                       framework = new Framework();
                                       framework.AddModule(typeof (ExampleModule1));
                                     	framework.AddBlackTheme();
                                       framework.Start();
                                     }
                                     catch (Exception exc)
                                     {
                                       exception = exc;
                                     }
                                   });
      thread.SetApartmentState(ApartmentState.STA);
      thread.Start();
      Thread.Sleep(10000);
      Assert.IsTrue(threadStarted);

      string message = "Exception happened :\n";
      if (exception != null)
      {
        Exception exc = exception;
        while (exc != null)
        {
          message += string.Format("{0}, {1},\n StackTrace: {2}\n\n ",
                       exception.ToString(), exception.Message, exception.StackTrace);
          exc = exc.InnerException;
        }
        
      }
      Assert.IsNull(exception, message);
      Assert.IsNotNull(framework, "Framework must be not null");
      thread.Abort();
    }

    class ExampleModule1 : IModule
    {
      private readonly IViewManager m_viewManager;
      private readonly IApplication m_application;

      public ExampleModule1(IViewManager viewManager_, IApplication application_)
      {
        m_viewManager = viewManager_;
        m_application = application_;
      }

      public void Initialize()
      {
        m_viewManager.AddViewCreator("aaaa", OnStateToViewHandler);
        m_application.ApplicationLoaded +=
          delegate
          {
            IViewContainer view = m_viewManager.CreateView("aaaa");
            //Assert.AreEqual(143, view.Width);
            //Assert.AreEqual(119, view.Height);
          };
      }

      private void OnStateToViewHandler(IViewContainer emptyViewContainer_)
      {
        //emptyViewContainer_.DefaultSize = new Size(150, 150);
      }
    }
  }
}
