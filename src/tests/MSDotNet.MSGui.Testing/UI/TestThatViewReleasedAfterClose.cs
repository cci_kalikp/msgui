﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/UI/TestThatViewReleasedAfterClose.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.View;

namespace MSDotNet.MSGui.Testing.UI
{
  
  [TestClass]
  public class TestThatViewReleasedAfterClose
  {
    //[TestMethod]
    //public void TestThatViewReleasedAfterCloseInTabbedDock()
    //{          
    //  IWindowManager windowManager = new TabbedDock();
    //  ViewContainer vc = new ViewContainer();
    //  WeakReference viewWeakReference = new WeakReference(vc);
    //  windowManager.AddNewView(vc, null);
    //  vc = null;
    //  GC.Collect();
    //  Assert.IsTrue(viewWeakReference.IsAlive);
    //  ViewContainer vc2 = (ViewContainer) viewWeakReference.Target;
    //  vc2.Close();
    //  vc2 = null;
    //  GC.Collect();          
    //  Assert.IsFalse(viewWeakReference.IsAlive);      
    //}

    //[TestMethod]
    //public void TestThatViewReleasedAfterCloseInAppBarWithFloatingWindowsMode()
    //{
    //  IWindowManager windowManager = new TabbedDock();
    //  ViewContainer vc = new ViewContainer();
    //  WeakReference viewWeakReference = new WeakReference(vc);
    //  windowManager.AddNewView(vc, null);
    //  vc = null;
    //  GC.Collect();
    //  Assert.IsTrue(viewWeakReference.IsAlive);
    //  ViewContainer vc2 = (ViewContainer) viewWeakReference.Target;
    //  vc2.Close();
    //  vc2 = null;
    //  GC.Collect();          
    //  Assert.IsFalse(viewWeakReference.IsAlive);      
    //}

    //not really "unit" tests - too many additional actions
    //[TestMethod]
    //public void TestThatViewReleasedAfterCloseInTabbedDock()
    //{
    //  Framework framework = null;
    //  Exception exception = null;
    //  Thread thread = new Thread(state_ =>
    //  {
    //    try
    //    {
    //      framework = new Framework();
    //      framework.AddModule(typeof(ExampleModule2));
    //      framework.Start();
    //    }
    //    catch (Exception exc)
    //    {
    //      exception = exc;
    //    }
    //  });
    //  thread.SetApartmentState(ApartmentState.STA);
    //  thread.Start();
    //  Thread.Sleep(10000);
    //  Assert.IsNull(exception,
    //    string.Format("Exception happened : {0}, StackTrace: {1} ", exception == null ? "exception==null" : exception.Message, exception == null ? "exception==null" : exception.StackTrace));
    //  Assert.IsNotNull(framework, "Framework must be not null");
    //}

    //[TestMethod]
    //public void TestThatViewReleasedAfterCloseWithAppBar()
    //{
    //  Framework framework = null;
    //  Exception exception = null;
    //  Thread thread = new Thread(state_ =>
    //  {
    //    try
    //    {
    //      framework = new Framework();
    //      framework.SetShellMode(ShellMode.LauncherBarAndFloatingWindows);
    //      framework.AddModule(typeof(ExampleModule2));
    //      framework.Start();
    //    }
    //    catch (Exception exc)
    //    {
    //      exception = exc;
    //    }        
    //  });
    //  thread.SetApartmentState(ApartmentState.STA);
    //  thread.Start();
    //  Thread.Sleep(10000);      
    //  Assert.IsNull(exception,
    //    string.Format("Exception happened : {0}, StackTrace: {1} ", exception == null ? "exception==null" : exception.Message, exception == null ? "exception==null" : exception.StackTrace));
    //  Assert.IsNotNull(framework, "Framework must be not null");      
    //}

    //[TestCleanup]
    //public void TestCleanUp()
    //{
    //  System.Windows.Application.Current.Dispatcher.Invoke((Action)(() => System.Windows.Application.Current.Shutdown()));
    //}

    //class ExampleModule2 : IModule
    //{
    //  private readonly IViewManager m_viewManager;
    //  private readonly IApplication m_application;
    //  private List<WeakReference> m_viewWeakReferences = new List<WeakReference>();

    //  public ExampleModule2(IViewManager viewManager_, IApplication application_)
    //  {
    //    m_viewManager = viewManager_;
    //    m_application = application_;
    //  }

    //  public void Initialize()
    //  {
    //    m_viewManager.AddViewCreator("aaaa", OnStateToViewHandler);
    //    m_application.ApplicationLoaded +=
    //      delegate
    //      {
    //        IViewContainer view = m_viewManager.CreateView("aaaa");
    //        Assert.AreEqual(1, CountAliveViews());
    //        view.Close();
    //        view = null;
    //        Assert.AreEqual(0, CountAliveViews());
    //      };
    //  }

    //  private void OnStateToViewHandler(IViewContainer emptyViewContainer_)
    //  {
    //    m_viewWeakReferences.Add(new WeakReference(emptyViewContainer_));
    //  }

    //  private int CountAliveViews()
    //  {
    //    GC.Collect();
    //    Thread.Sleep(2);
    //    int counter = 0;
    //    foreach (WeakReference weakReference in m_viewWeakReferences)
    //    {
    //      if (weakReference.IsAlive)
    //      {
    //        counter++;
    //      }
    //    }
    //    return counter;
    //  }
    //}
  }
}
