﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/SetUpFixtureForTest.cs#9 $
// $Change: 839595 $
// $DateTime: 2013/07/29 12:06:16 $
// $Author: milosp $

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MorganStanley.MSDotNet.MSGui.Application
{
 [TestClass,Ignore] 
  public class SetUpAssemblyResolver
  {
   [AssemblyInitialize]
   public static void AnyName(TestContext context)
   {     
     //if (System.Windows.Application.Current == null)
     //{
     //  new FakeApplication();
     //}
   }

    /// <summary>
    /// Only here to con prism into thinking we are running in a real appliction.
    /// Without this prism region stuff thinks we are running in a design mode.
    /// </summary>
    internal class FakeApplication : System.Windows.Application
    {
    }
  }
}
