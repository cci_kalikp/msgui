﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/LayoutConversion/LayoutConversionTests.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Text.RegularExpressions;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace MSDotNet.MSGui.Testing.LayoutConversion
{
  [TestClass]
  public class LayoutConversionTests
  {
    [TestMethod]
    [DeploymentItem(@"LayoutConversion\TestData\xdmExample1.xml")]
    public void ParseXamDockManager()
    {
      XDocument doc = XDocument.Load(@"xdmExample1.xml");
      LayoutConverter.Xdm repr = LayoutConverter.Xdm.FromXml(doc.Root);
      Assert.IsNotNull(repr);
      Assert.AreEqual(repr.ContentPanes.Count, 5);
      Assert.AreEqual(repr.Panes.Count, 8);
    }

    [TestMethod]
    [DeploymentItem(@"LayoutConversion\TestData\layoutExample1.xml")]
    public void ParseConcordLayout()
    {
      XDocument doc = XDocument.Load(@"layoutExample1.xml");
      LayoutConverter.Layout layout = LayoutConverter.Layout.FromXml(doc.Root);
      Assert.IsNotNull(layout);
      Assert.AreEqual("ddd", layout.Name);
      Assert.AreEqual(1, layout.Splitter.Tabs.Count);      
      Assert.AreEqual("Main", layout.Splitter.SelectedTab);
      Assert.IsTrue(layout.Splitter.Tabs[0].Child is LayoutConverter.SyncSplitPane);
      Assert.IsTrue(((LayoutConverter.SyncSplitPane)layout.Splitter.Tabs[0].Child).Child1 is LayoutConverter.SyncSplitPane);
      Assert.IsTrue(((LayoutConverter.SyncSplitPane)layout.Splitter.Tabs[0].Child).Child2 is LayoutConverter.Pane);
      Assert.IsTrue(((LayoutConverter.SyncSplitPane)((LayoutConverter.SyncSplitPane)layout.Splitter.Tabs[0].Child).Child1).Child1 is LayoutConverter.Pane);
      Assert.IsTrue(((LayoutConverter.SyncSplitPane)((LayoutConverter.SyncSplitPane)layout.Splitter.Tabs[0].Child).Child1).Child2 is LayoutConverter.SyncSplitPane);
      Assert.IsTrue(((LayoutConverter.SyncSplitPane)((LayoutConverter.SyncSplitPane)((LayoutConverter.SyncSplitPane)layout.Splitter.Tabs[0].Child).Child1).Child2).Child1 is LayoutConverter.SyncSplitPane);
      Assert.IsTrue(((LayoutConverter.SyncSplitPane)((LayoutConverter.SyncSplitPane)((LayoutConverter.SyncSplitPane)layout.Splitter.Tabs[0].Child).Child1).Child2).Child2 is LayoutConverter.Pane);
      Assert.IsTrue(((LayoutConverter.SyncSplitPane) ((LayoutConverter.SyncSplitPane)((LayoutConverter.SyncSplitPane)((LayoutConverter.SyncSplitPane)layout.Splitter.Tabs[0].Child).Child1).Child2).Child1).Child1 is LayoutConverter.Pane);
      Assert.IsTrue(((LayoutConverter.SyncSplitPane) ((LayoutConverter.SyncSplitPane)((LayoutConverter.SyncSplitPane)((LayoutConverter.SyncSplitPane)layout.Splitter.Tabs[0].Child).Child1).Child2).Child1).Child2 is LayoutConverter.Pane);
      //Assert.AreEqual(repr.ContentPanes.Count, 5);
      //Assert.AreEqual(repr.Panes.Count, 8);
    }

    [TestMethod]
    [DeploymentItem(@"LayoutConversion\TestData\TabbedDockExample.xml")]
    public void ParseNewLayout()
    {
      XDocument doc =
        XDocument.Load(
          @"TabbedDockExample.xml");
      LayoutConverter.TabbedDockRepr td = LayoutConverter.TabbedDockRepr.FromXml(doc.Root);
      Assert.IsNotNull(td);      
    }

    [TestMethod]
    [DeploymentItem(@"LayoutConversion\TestData\layoutExample3.xml")]
    public void ParseAndConvertLayout()
    {
      XDocument doc =
        XDocument.Load(
          @"layoutExample3.xml");
      LayoutConverter.Layout layout = LayoutConverter.Layout.FromXml(doc.Root);
      Assert.IsNotNull(layout);      
      LayoutConverter.TabbedDockRepr tdr = LayoutConverter.TabbedDockRepr.FromLayout(layout);
      Assert.IsNotNull(tdr);      
      XElement result = LayoutConverter.TabbedDockRepr.ToXml(tdr);
      Assert.IsNotNull(result);      
    }

    [TestMethod]
    [DeploymentItem(@"LayoutConversion\TestData\layoutExample4.xml")]
    public void ParseAndConvertLayout2()
    {
      XDocument doc =
        XDocument.Load(
          @"layoutExample4.xml");
      LayoutConverter.Layout layout = LayoutConverter.Layout.FromXml(doc.Root);
      Assert.IsNotNull(layout);      
      LayoutConverter.TabbedDockRepr tdr = LayoutConverter.TabbedDockRepr.FromLayout(layout);
      Assert.IsNotNull(tdr);      
      XElement result = LayoutConverter.TabbedDockRepr.ToXml(tdr);
      Assert.IsNotNull(result);      
    }
    
    [TestMethod]
    [DeploymentItem(@"LayoutConversion\TestData\TabbedDockExample.xml")]
    public void ConvertNewToOldLayout()
    {
      XDocument doc =
        XDocument.Load(
          @"TabbedDockExample.xml");
      var xml = LayoutConverter.TabbedDockToPaneWindowManager(doc.Root);

      Assert.IsNotNull(xml);
    }

    [TestMethod]
    [DeploymentItem(@"LayoutConversion\TestData\OneFloatingConcordWindowXdm.xml")]
    [DeploymentItem(@"LayoutConversion\ExpectedResults\OneFloatingConcordWindowXdmExpectedResult.xml")]
    public void ConvertXdmLayoutWithOneFloatingWindow()
    {
      XDocument doc =
        XDocument.Load(
          @"OneFloatingConcordWindowXdm.xml");
      XElement xml = LayoutConverter.TabbedDockToPaneWindowManager(doc.Root);
      XDocument expectedDoc =
        XDocument.Load(
          @"OneFloatingConcordWindowXdmExpectedResult.xml");
      Assert.IsTrue(XNode.DeepEquals(xml, expectedDoc.Root));
    }

    [TestMethod]
    [DeploymentItem(@"LayoutConversion\TestData\OneFloatingConcordWindowConcord.xml")]
    [DeploymentItem(@"LayoutConversion\ExpectedResults\OneFloatingConcordWindowConcordExpectedResult.xml")]
    public void ConvertPaneWindowManageLayoutWithOneFloatingWindow()
    {
      XDocument doc =
        XDocument.Load(
          @"OneFloatingConcordWindowConcord.xml");
      XElement xml = LayoutConverter.PaneWindowManagerToTabbedDock(doc.Root);
      XDocument expectedDoc =
        XDocument.Load(
          @"OneFloatingConcordWindowConcordExpectedResult.xml");
      //guids are different, we can't simply use XNode.DeepEquals
      string result = RemoveGuids(xml.ToString());
      string expected = RemoveGuids(expectedDoc.ToString());
      Assert.AreEqual(result, expected);
    }


    [TestMethod]
    [DeploymentItem(@"LayoutConversion\TestData\OneDockedConcordWindowXdm.xml")]
    [DeploymentItem(@"LayoutConversion\ExpectedResults\OneDockedConcordWindowXdmExpectedResult.xml")]
    public void ConvertXdmLayoutWithOneDockedWindow()
    {
      XDocument doc =
        XDocument.Load(
          @"OneDockedConcordWindowXdm.xml");
      XElement xml = LayoutConverter.TabbedDockToPaneWindowManager(doc.Root);
      XDocument expectedDoc =
        XDocument.Load(
          @"OneDockedConcordWindowXdmExpectedResult.xml");
      Assert.IsTrue(XNode.DeepEquals(xml, expectedDoc.Root));
    }

    [TestMethod]
    [DeploymentItem(@"LayoutConversion\TestData\OneDockedConcordWindowConcord.xml")]
    [DeploymentItem(@"LayoutConversion\ExpectedResults\OneDockedConcordWindowConcordExpectedResult.xml")]
    public void ConvertPaneWindowManageLayoutWithOneDockedWindow()
    {
      XDocument doc =
        XDocument.Load(
          @"OneDockedConcordWindowConcord.xml");
      XElement xml = LayoutConverter.PaneWindowManagerToTabbedDock(doc.Root);
      XDocument expectedDoc =
        XDocument.Load(
          @"OneDockedConcordWindowConcordExpectedResult.xml");
      //guids are different, we can't simply use XNode.DeepEquals
      string result = RemoveGuids(xml.ToString());
      string expected = RemoveGuids(expectedDoc.ToString());
      Assert.AreEqual(result, expected);
    }


    [TestMethod]
    [DeploymentItem(@"LayoutConversion\TestData\TwoDockedConcordWindowXdm.xml")]
    [DeploymentItem(@"LayoutConversion\ExpectedResults\TwoDockedConcordWindowXdmExpectedResult.xml")]
    public void ConvertXdmLayoutWithTwoDockedWindow()
    {
      XDocument doc =
        XDocument.Load(
          @"TwoDockedConcordWindowXdm.xml");
      XElement xml = LayoutConverter.TabbedDockToPaneWindowManager(doc.Root);
      XDocument expectedDoc =
        XDocument.Load(
          @"TwoDockedConcordWindowXdmExpectedResult.xml");
      Assert.IsTrue(XNode.DeepEquals(xml, expectedDoc.Root));
    }

    [TestMethod]
    [DeploymentItem(@"LayoutConversion\TestData\TwoDockedConcordWindowConcord.xml")]
    [DeploymentItem(@"LayoutConversion\ExpectedResults\TwoDockedConcordWindowConcordExpectedResult.xml")]
    public void ConvertPaneWindowManageLayoutWithTwoDockedWindow()
    {
      XDocument doc =
        XDocument.Load(
          @"TwoDockedConcordWindowConcord.xml");
      XElement xml = LayoutConverter.PaneWindowManagerToTabbedDock(doc.Root);
      XDocument expectedDoc =
        XDocument.Load(
          @"TwoDockedConcordWindowConcordExpectedResult.xml");
      //guids are different, we can't simply use XNode.DeepEquals
      string result = RemoveGuids(xml.ToString());
      string expected = RemoveGuids(expectedDoc.ToString());
      Assert.AreEqual(result, expected);
    }
    
    [TestMethod]
    [DeploymentItem(@"LayoutConversion\TestData\FloatingDockedXdmWindows.xml")]
    [DeploymentItem(@"LayoutConversion\ExpectedResults\FloatingDockedXdmWindowsExpectedResult.xml")]
    public void ConvertFloatingDockedXdmWindows()
    {
      XDocument doc =
        XDocument.Load(
          @"FloatingDockedXdmWindows.xml");
      XElement xml = LayoutConverter.TabbedDockToPaneWindowManager(doc.Root);
      XDocument expectedDoc =
        XDocument.Load(
          @"FloatingDockedXdmWindowsExpectedResult.xml");
      Assert.IsTrue(XNode.DeepEquals(xml, expectedDoc.Root));
    }

    //[TestMethod]
    //[DeploymentItem(@"LayoutConversion\OneFloating1.xml")]
    //[DeploymentItem(@"LayoutConversion\FloatingDockedXdmWindowsExpectedResult.xml")]
    //public void ConvertFloatingDockedXdmWindows1()
    //{
    //  XDocument doc =
    //    XDocument.Load(
    //      @"OneFloating1.xml");
    //  XElement xml = LayoutConverter.StateToPaneWindowManagerLayout(doc.Root);
    //  XDocument expectedDoc =
    //    XDocument.Load(
    //      @"FloatingDockedXdmWindowsExpectedResult.xml");
    //  Assert.IsTrue(XNode.DeepEquals(xml, expectedDoc.Root));
    //}


    [TestMethod]
    [DeploymentItem(@"LayoutConversion\TestData\TwoDockedHor.xml")]
    [DeploymentItem(@"LayoutConversion\ExpectedResults\TwoDockedHorExpectedResult.xml")]
    public void TwoDockedHorizontallyToTabSplitter()
    {
      XDocument doc =
        XDocument.Load(
          @"TwoDockedHor.xml");
      XElement xml = LayoutConverter.StateToPaneWindowManagerLayout(doc.Root);
      XDocument expectedDoc =
        XDocument.Load(
          @"TwoDockedHorExpectedResult.xml");
      Assert.IsTrue(XNode.DeepEquals(xml, expectedDoc.Root));
    }

    [TestMethod]
    [DeploymentItem(@"LayoutConversion\TestData\ThreeDockedVert.xml")]
    [DeploymentItem(@"LayoutConversion\ExpectedResults\ThreeDockedVertExpectedResult.xml")]
    public void ThreeDockedVerticallyToTabSplitter()
    {
      XDocument doc =
        XDocument.Load(
          @"ThreeDockedVert.xml");
      XElement xml = LayoutConverter.StateToPaneWindowManagerLayout(doc.Root);
      XDocument expectedDoc =
        XDocument.Load(
          @"ThreeDockedVertExpectedResult.xml");
      Assert.IsTrue(XNode.DeepEquals(xml, expectedDoc.Root));
    }

    [TestMethod]
    [DeploymentItem(@"LayoutConversion\TestData\DockedTabGroup.xml")]
    [DeploymentItem(@"LayoutConversion\ExpectedResults\DockedTabGroupExpectedResult.xml")]
    public void DockedTabGroupToTabSplitter()
    {
      XDocument doc =
        XDocument.Load(
          @"DockedTabGroup.xml");
      XElement xml = LayoutConverter.StateToPaneWindowManagerLayout(doc.Root);
      XDocument expectedDoc =
        XDocument.Load(
          @"DockedTabGroupExpectedResult.xml");
      Assert.IsTrue(XNode.DeepEquals(xml, expectedDoc.Root));
    }
    

    private static string RemoveGuids(string xml_)
    {
      //quick and rough method to remove guids from TabbedDock representation
      //we do not compare that remove guids were equal
      string result = Regex.Replace(xml_, "contentPane name=\"[^\"]*", "");
      result = Regex.Replace(result, "splitPane name=\"[^\"]*", "");
      result = Regex.Replace(result, "IView Id=\"[^\"]*", "");
      return result;
    }
  }
}
