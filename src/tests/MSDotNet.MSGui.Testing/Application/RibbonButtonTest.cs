﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Application/RibbonButtonTest.cs#3 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using MorganStanley.MSDotNet.MSGui.Core.Ribbon;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSDotNet.MSGui.Testing.Application
{
  [TestClass]
  public class RibbonButtonTest
  {
    [TestMethod]
    public void TestConstructor()
    {
      RibbonButton rb = new RibbonButton("test", null);
      Assert.IsTrue(rb != null, "The RibbonButton constructor failed, it returned null.");
    }
  }
}
