﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Concord/ConcordApplicationTest.cs#7 $
// $Change: 839595 $
// $DateTime: 2013/07/29 12:06:16 $
// $Author: milosp $

using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.IED.Concord.Application;

namespace MSDotNet.MSGui.Testing.Concord
{
	/// <summary>
	/// Summary description for ConcordApplicationTest
	/// </summary>
	[TestClass]
	public abstract class ConcordApplicationTest
	{
		[TestInitialize]
		public void SetUp()
		{
			typeof (System.Windows.Application).GetField("_appInstance",
			                                             BindingFlags.NonPublic | BindingFlags.FlattenHierarchy |
			                                             BindingFlags.Static).SetValue
				(null, null);
			typeof (System.Windows.Application).GetField("_appCreatedInThisAppDomain",
			                                             BindingFlags.NonPublic | BindingFlags.FlattenHierarchy |
			                                             BindingFlags.Static).SetValue(null, false);
			var application = new System.Windows.Application {MainWindow = null};
		}

		public abstract IConcordApplication CreateInstance();
		public abstract IConcordApplication CreateInitializedInstance();

		[TestMethod]
		public void ConcordApplicationConstructorTest()
		{
			CreateInstance();
		}

        [TestMethod,Ignore]
		public void ConcordApplicationInitializeTest()
		{
			CreateInitializedInstance();
		}

		[TestMethod,Ignore]
		public void ConcordApplicationDefaultLayoutTest()
		{
			IConcordApplication application = CreateInitializedInstance();
			Assert.AreEqual(null, application.ActiveLayout);
		}

		[TestMethod]
		public void ConcordApplicationChangeNameTest()
		{
			IConcordApplication application = CreateInstance();
			application.Name = "newName";
			Assert.AreEqual("newName", application.Name);
		}
	}
}