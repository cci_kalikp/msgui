﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Concord/ConcordApplicationTestMain.cs#14 $
// $Change: 882731 $
// $DateTime: 2014/05/29 02:54:26 $
// $Author: caijin $

using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence.LayoutLoadingScreen;
using MorganStanley.MSDotNet.MSNet;

namespace MSDotNet.MSGui.Testing.Concord
{
  /// <summary>
  /// Summary description for ConcordApplicationTest
  /// </summary>
  [TestClass]
  public class ConcordApplicationTestMain : ConcordApplicationTest
  {
    private static IUnityContainer CreateUnityContainer()
    {
      UnityContainer container = new UnityContainer();

      container.RegisterInstance<IStatusBar>(new StatusBarTestImpl());
      container.RegisterInstance<IApplicationOptions>(new ApplicationOptionsTestImpl());
      container.RegisterInstance<IApplication>(new MorganStanley.MSDotNet.MSGui.Impl.Application.Application());
      container.RegisterInstance<IViewManager>(new RibbonChromeManager(new ChromeRegistry(), new ShellTestImpl(), null, null, container, null, new InitialSettings()));
      container.RegisterInstance<IPersistenceService>(new PersistenceServiceTestImpl());
      container.RegisterInstance<IMSNetLoop>(new MSNetLoopTestImpl());
      return container;
    }

    public override IConcordApplication CreateInstance()
    {
        var container = CreateUnityContainer();
      return new ConcordApplication(container,
        new PersistenceProfileService(new PersistenceStorageTestImpl(), new ItemsDictionaryServiceTestImpl(), new StatusBarTestImpl(), new LayoutLoadingScreenRetriever(container), new LayoutLoadingScreenLauncher(), container)
        );
    }

    public override IConcordApplication CreateInitializedInstance()
    {
      IUnityContainer container = CreateUnityContainer();
      ConcordApplication app = (ConcordApplication)new ConcordApplication(container, new PersistenceProfileService(new PersistenceStorageTestImpl(), new ItemsDictionaryServiceTestImpl(), new StatusBarTestImpl(),
          new LayoutLoadingScreenRetriever(container), new LayoutLoadingScreenLauncher(), container));
      app.Init(new ConcordWindowManager(app, container), null, null); //TODO
      app.Initialize();
      return app;
    }
  }
}
