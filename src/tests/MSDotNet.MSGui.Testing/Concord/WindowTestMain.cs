﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Concord/WindowTestMain.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl;

namespace MSDotNet.MSGui.Testing.Concord
{
	[TestClass]
	public class WindowTestMain : WindowTest
	{
		public override IWindow CreateInstance()
		{
			Form form = new Form1();
			WindowAdapter windowAdapter = new WindowAdapter("testCaption", form, Guid.NewGuid(), true, "");
			return windowAdapter;
		}

		protected override IWindow CreateLinkableInstance()
		{
			Form form = new LinkableForm();
			WindowAdapter windowAdapter = new WindowAdapter("testCaption", form, Guid.NewGuid(), true, "");
			return windowAdapter;
		}

		[TestMethod]
		public void TestCaptionSet()
		{
			Form1 form = new Form1();
			WindowAdapter windowAdapter = new WindowAdapter("testCaption", form, Guid.NewGuid(), true, "");
			Assert.AreEqual("testCaption", windowAdapter.Caption);
		}

		[TestMethod]
		public void TestCaptionViewContainer()
		{
			Form1 form = new Form1();
			WindowAdapter windowAdapter = new WindowAdapter("testCaption", form, Guid.NewGuid(), true, "");
			Assert.AreEqual(windowAdapter.ViewContainer.Title, windowAdapter.Caption);
		}

		[TestMethod]
		public void TestCaptionWindowAdapterChanged()
		{
			Form1 form = new Form1();

			WindowAdapter windowAdapter = new WindowAdapter("testCaption", form, Guid.NewGuid(), true, "");
			const string newtext = "NewText";
			windowAdapter.Caption = newtext;
			Assert.AreEqual(newtext, windowAdapter.ViewContainer.Title);
			Assert.AreEqual(newtext, form.Text);
		}

		[TestMethod]
		public void TestCaptionViewcontainerChangedAffectsAll()
		{
			Form1 form = new Form1();
			WindowAdapter windowAdapter = new WindowAdapter("testCaption", form, Guid.NewGuid(), true, "");
			const string newtext = "NewText";
			windowAdapter.ViewContainer.Title = newtext;
			Assert.AreEqual(newtext, form.Text);
			Assert.AreEqual(newtext, windowAdapter.Caption);
		}

		[TestMethod]
		public void WindowAdapterClosesViewContainer()
		{
			WindowAdapter windowAdapter = new WindowAdapter("testCaption", new Form1(), Guid.NewGuid(), true, "");
			bool closed = false;
			bool closing = false;
			windowAdapter.ViewContainer.Closing += (sender_, args_) => closing = true;
			windowAdapter.ViewContainer.Closed += (sender_, args_) => closed = true;
			windowAdapter.Close();
			Assert.IsTrue(closing);
			Assert.IsTrue(closed);
		}

		[TestMethod]
		public void ViewContainerClosesWindowAdaptersForm()
		{
			WindowAdapter window = new WindowAdapter("testCaption", new Form1(), Guid.NewGuid(), true, "");
			bool closingCalled = false;
			bool closedCalled = false;
			bool formClosingCalled = false;
			bool formClosedCalled = false;
			window.Form.Closing += (sender_, args_) => closingCalled = true;
			window.Form.Closed += (sender_, args_) => closedCalled = true;
			window.Form.FormClosing += (sender_, args_) => formClosingCalled = true;
			window.Form.FormClosed += (sender_, args_) => formClosedCalled = true;
			//closing window's viewcontainer
			window.ViewContainer.Close();
			System.Threading.Thread.Sleep(1000);
			Assert.IsTrue(closingCalled, "Closing was not called");
			Assert.IsTrue(closedCalled, "Closed was not called");
			Assert.IsTrue(formClosingCalled, "FormClosing was not called");
			Assert.IsTrue(formClosedCalled, "FormClosed was not called");
		}

		[TestMethod]
		public void WindowAdapterActivatesViewContainer()
		{
			WindowAdapter window = new WindowAdapter("testCaption", new Form1(), Guid.NewGuid(), true, "");
			bool activated = false;
			window.ViewContainer.ActivationRequested += (sender_, args_) => activated = true;
			window.SetFocus();
			System.Threading.Thread.Sleep(1000);
			Assert.IsFalse(activated, "Active was called, and chrome was not ready yet");
			ChromeManagerBase.StartupCompleted = true;
			window.SetFocus();
			System.Threading.Thread.Sleep(1000);
			Assert.IsTrue(activated, "Active was not called, and chrome was ready");
			ChromeManagerBase.StartupCompleted = false;

		}
	}
}
