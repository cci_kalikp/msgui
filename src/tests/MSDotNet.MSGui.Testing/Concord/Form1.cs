﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MSDotNet.MSGui.Testing.Concord
{

  internal class Form1 : Form
  {
    public Form1()
    {
      TopLevel = false;
      TextBox myText = new TextBox(){ Text = "Hello!" };
      myText.Location = new Point(25, 25);
      this.Controls.Add(myText);

    }
  }
}
