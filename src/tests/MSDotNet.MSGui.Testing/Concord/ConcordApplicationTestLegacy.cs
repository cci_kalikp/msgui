﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Concord/ConcordApplicationTestLegacy.cs#3 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.IED.Concord.Application;
using MorganStanley.IED.Concord.Host;

namespace MSDotNet.MSGui.Testing.Concord
{
  /// <summary>
  /// Summary description for ConcordApplicationTest
  /// </summary>
  [TestClass]
  public class ConcordApplicationTestLegacy : ConcordApplicationTest
  {
    public override IConcordApplication CreateInstance()
    {
      return new ConcordApplication();
    }

    public override IConcordApplication CreateInitializedInstance()
    {
      ConcordApplication app = new ConcordApplication();
      app.Initialize();
      return app;
    }

    [TestMethod]
    public void ConcordApplicationConstructorWithNameTest()
    {
      ConcordApplication application = new ConcordApplication("Sample Name");
      Assert.AreEqual("Sample Name", application.Name);
      Assert.AreEqual("Sample Name", application.DisplayName);
    }

    [TestMethod]
    public void ConcordApplicationConstructorWithIconTest()
    {
      var icon = SystemIcons.Application;
      ConcordApplication application = new ConcordApplication("Sample Name", icon);      
      Assert.AreEqual(icon, application.Icon);
    }
  }
}
