﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using MorganStanley.IED.Concord.Application;

namespace MSDotNet.MSGui.Testing.Concord
{
  internal class LinkableForm : Form, ILinkableWindow
  {
    public LinkableForm() 
    {
      TopLevel = false;
      TextBox myText = new TextBox(){ Text = "Hello!" };
      myText.Location = new Point(25, 25);
      this.Controls.Add(myText);
    }

    private readonly IColleague m_col = new Col();
    public IColleague Colleague
    {
      get
      {
        return m_col;
      }
    }
  }

  internal class Col : IColleague
  {
    public void SaveState(XmlNode node_, ref bool cancel_)
    {
      
    }

    public void LoadState(XmlNode node_, ref bool cancel_)
    {

    }

    private readonly Guid m_guid = Guid.NewGuid();
    public Guid ColleagueId
    {
      get 
      {
        return m_guid;
      }
    }
  }
}
