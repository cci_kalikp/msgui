﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Concord/WindowTestLegacy.cs#3 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.IED.Concord.Application;
using MorganStanley.IED.Concord.WindowManager.Pane;
using MorganStanley.IED.Concord.WindowManager.Pane.Controls;

namespace MSDotNet.MSGui.Testing.Concord
{
  [TestClass]
  public class WindowTestLegacy : WindowTest
  {
    private FloatingForm m_parentForm;
    public override IWindow CreateInstance()
    {      
      return CreateInstance(new Form1());
    }

    protected override IWindow CreateLinkableInstance()
    {
      return CreateInstance(new LinkableForm());
    }

    private IWindow CreateInstance(Form form_)
    {
      form_.BackColor = Color.Red;
      //I can't pass the right parent control directly here - this will require syncfusion license
      PaneWindow pw = new PaneWindow("testCaption", form_, null);
      form_.Show();
      if (form_.Parent == null)
      {
        m_parentForm = new FloatingForm();
        m_parentForm.LoadForm(form_);
      };
      if (form_.Parent != null) form_.Parent.Show();
      
      return pw;
    }
  }
}
