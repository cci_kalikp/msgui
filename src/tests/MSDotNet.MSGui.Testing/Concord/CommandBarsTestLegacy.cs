﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Concord/CommandBarsTestLegacy.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Globalization;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.IED.Concord.Application;

namespace MSDotNet.MSGui.Testing.Concord
{
  /// <summary>
  /// CommandBarsTest for Concord.Host implementation
  /// 
  /// All tests with CommandBarType.Popup will fail because this type of bar was never supported in Concord.Host
  /// </summary>
  [TestClass,Ignore]
  public class CommandBarsTestLegacy : CommandBarsTest
  {
    public override ICommandBars CreateInstance()
    {
      var application = new MorganStanley.IED.Concord.Host.ConcordApplication();
      var bars = Activator.CreateInstance(typeof(MorganStanley.IED.Concord.Host.Windows.Forms.CommandBars),
                                          BindingFlags.NonPublic | BindingFlags.Instance,
                                          default(Binder),
                                          new object[] { application, new Syncfusion.Windows.Forms.Tools.XPMenus.MainFrameBarManager(new Form()) },
                                          default(CultureInfo));
      return (ICommandBars)bars;
    }
  }
}
