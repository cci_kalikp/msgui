﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;
using MorganStanley.MSDotNet.MSGui.Impl.View;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MSDotNet.MSGui.Testing.Concord
{
    internal class StatusBarTestImpl : IStatusBar
    {
        public ICollection<string> Keys
        {
            get { throw new NotImplementedException(); }
        }

        public IStatusBarItem this[string statusBarItemKey]
        {
            get { throw new NotImplementedException(); }
        }

        private readonly StatusBarItemTestImpl m_statusBarItemTestImpl = new StatusBarItemTestImpl();

        public IStatusBarItem MainItem
        {
            get { return m_statusBarItemTestImpl; }
        }

        public void AddItem(string key, IStatusBarItem item, StatusBarItemAlignment alignment)
        {
            throw new NotImplementedException();
        }

        public string AddItem(IStatusBarItem item, StatusBarItemAlignment alignment)
        {
            throw new NotImplementedException();
        }

        public void AddTextStatusItem(string key, StatusBarItemAlignment alignment)
        {
            throw new NotImplementedException();
        }

        public string AddTextStatusItem(StatusBarItemAlignment alignment)
        {
            throw new NotImplementedException();
        }

        public void AddProgressBarStatusItem(string key, double @from, double to, StatusBarItemAlignment alignment)
        {
            throw new NotImplementedException();
        }

        public string AddProgressBarStatusItem(double @from, double to, StatusBarItemAlignment alignment)
        {
            throw new NotImplementedException();
        }

        public void AddSeparator(string key, StatusBarItemAlignment alignment)
        {
            throw new NotImplementedException();
        }

        public string AddSeparator(StatusBarItemAlignment alignment)
        {
            throw new NotImplementedException();
        }

        public void RemoveItem(string key)
        {
            throw new NotImplementedException();
        }

        public bool ContainsItem(string key)
        {
            throw new NotImplementedException();
        }
    }

    internal class StatusBarItemTestImpl : IStatusBarItem
    {
        public void UpdateStatus(StatusLevel level_, string statusText_)
        {
            throw new NotImplementedException();
        }

        public void ClearStatus()
        {
            throw new NotImplementedException();
        }

        public object Control
        {
            get { throw new NotImplementedException(); }
        }

        public event StatusUpdatedEventHandler StatusUpdated;
    }

    internal class ApplicationOptionsTestImpl : IApplicationOptions
    {
        public void AddOptionPage(string path, string title, IOptionView view)
        {
            throw new NotImplementedException();
        }

        public void AddViewModelBasedOptionPage<TViewModel>(string path, string title, bool global)
            where TViewModel : IOptionViewViewModel
        {
            throw new NotImplementedException();
        }

        public void RemoveOptionPage(string path, string title)
        {
            throw new NotImplementedException();
        }


        public void AddOptionPage(string path, string title, IOptionView view, int priority)
        {
            throw new NotImplementedException();
        }

        public void AddViewModelBasedOptionPage<TViewModel>(string path, string title, bool global, int priority) where TViewModel : IOptionViewViewModel
        {
            throw new NotImplementedException();
        }
    }

    internal class ShellTestImpl : IShell
    {
        public void AddTheme(string name_, ImageSource icon_, Theme theme_)
        {
            throw new NotImplementedException();
        }

        public void SetTheme(string name_)
        {
            throw new NotImplementedException();
        }

        public void SetTheme(Theme theme_)
        {
            throw new NotImplementedException();

        }

        public void DisableThemeChanger()
        {
            throw new NotImplementedException();
        }

        private readonly WindowManagerTestImpl m_windowManagerTestImpl = new WindowManagerTestImpl();

        public IWindowManager WindowManager
        {
            get { return m_windowManagerTestImpl; }
        }
        public ITileItemManager TileItemManager
        {
            get { throw new NotImplementedException(); }
        }

        public Window MainWindow
        {
            get { throw new NotImplementedException(); }
        }

        public StatusBar StatusBar
        {
            get { return new StatusBar(); }
        }

        public UIElement TitleBar
        {
            get { throw new NotImplementedException(); }
        }

        public Func<bool> TryCloseConcordMainWindow
        {
            set { throw new NotImplementedException(); }
        }

        public void Show()
        {
            throw new NotImplementedException();
        }

        public void Close()
        {
            throw new NotImplementedException();
        }

        public void LoadProfile(string profile_, bool calledWhenLoading_ = false)
        {
            throw new NotImplementedException();
        }

        public void SaveCurrentProfile()
        {
        }

        public void DeleteCurrentProfile()
        {
        }

        public void SaveProfileAs()
        {
        }

        public void SetLayoutLoadStrategy(LayoutLoadStrategy layoutLoadStrategy_,
                                          LayoutLoadStrategyCallback layoutLoadStrategyCallback_)
        {
            throw new NotImplementedException();
        }

        public void SetLayoutLoadStrategy(LayoutLoadStrategy layoutLoadStrategy_)
        {
            throw new NotImplementedException();
        }

        public Func<bool> OnClosing
        {
            set { }
        }
        public Action OnClosed
        {
            set { }
        }
        public void Initialize()
        {
            throw new NotImplementedException();
        }

        public ItemsControl MainMenu
        {
            get { throw new NotImplementedException(); }
        }

        public void LoadState(XDocument state_)
        {
            throw new NotImplementedException();
        }

        public XDocument SaveState()
        {
            throw new NotImplementedException();
        }

        public string PersistorId
        {
            get { throw new NotImplementedException(); }
        }

        public System.Windows.Forms.Form ConcordMSNetLoopForm
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }
         
        public Infragistics.Windows.DockManager.XamDockManager DockManager
        {
            get { throw new NotImplementedException(); }
        }


        public bool DisableClose
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        public void PostChromeCreationInitialize()
        {
            throw new NotImplementedException();
        }
    }

    internal class WindowManagerTestImpl : IWindowManager
    {
        public void AddNewView(WindowViewContainer viewContainer_, object additionalData_)
        {
            throw new NotImplementedException();
        }

        public void Clean()
        {
            throw new NotImplementedException();
        }

        public XDocument SaveState(Func<WindowViewContainer, XElement> buildIViewXml_)
        {
            throw new NotImplementedException();
        }

        public void BeforeSaveState()
        {
            throw new NotImplementedException();
        }

        public void AfterSaveState()
        {
            throw new NotImplementedException();
        }

        public void LoadState(XDocument layout_, PersistenceProfileService persistenceProfileService_, Action<XDocument, object> applyNodeAction_)
        {
            throw new NotImplementedException();
        }

        public void BeforeLoadState()
        {
            throw new NotImplementedException();
        }

        public void AfterLoadState()
        {
            throw new NotImplementedException();
        }

        public event EventHandler<WindowManagerActiveChangedEventArgs> ActivePaneChanged;
        public event EventHandler<GenericDragOrDropOnWindowEventArgs> DragOrDropOnWindow;

        public IWindowViewContainer CreateDialog()
        {
            throw new NotImplementedException();
        }

        public event EventHandler<PaneDroppedAtScreenEdgeEventArgs> PaneDroppedAtScreenEdge;
        public event EventHandler<WindowGroupDroppedOnScreenEdgeEventArgs> WindowGroupDroppedAtScreenEdge;
        public event EventHandler<CrossProcessDragDropEventArgs> CrossProcessDragDropAction;

        public void AddNewView(WindowViewContainer viewContainer_, object additionalData_,
                               InitialLocation initialLocation_)
        {
            throw new NotImplementedException();
        }


        public event EventHandler<WindowManagerActiveChangingEventArgs> ActivePaneChanging;


        public void CancelLoad(bool cancelLayoutRestore_)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<IWindowViewContainerHostRoot> ReadSubState(string subLayoutName_, XElement subLayout_, RehydratePaneDelegate callback_, 
            bool reuseExisting_ = false, SubLayoutLocation locationOverride_ = SubLayoutLocation.SavedLocation)
        {
            throw new NotImplementedException();
        }

        public XElement WriteSubState(IEnumerable<IWindowViewContainerHostRoot> roots_, IEnumerable<IWindowViewContainer> containers_= null, bool skipLocationInfo_ = false)
        {
            throw new NotImplementedException();
        }


        public void RegisterDockManagerHookers(System.Reflection.Assembly hookerAssembly_)
        {
            throw new NotImplementedException();
        }
    }

    internal class PersistenceServiceTestImpl : IPersistenceService
    {

        public void AddPersistor(string regeneratorId, RestoreStateHandler restoreStateHandler, SaveStateHandler saveStateHandler)
        {
            
        }
         
        public void AddGlobalPersistor(string persistorId, RestoreStateHandler restoreStateHandler, SaveStateHandler saveStateHandler)
        {
            
        }
         

        public void LoadProfile(string name)
        {
            
        }

        public void SaveProfileAs()
        {
             
        }

        public void SaveProfileAs(string name)
        {
           
        }

        public void SaveCurrentProfile()
        {
           
        }

        public ObservableCollection<string> GetProfileNames()
        {
            return null;
        }

        public void DeleteCurrentProfile()
        {
            
        }

        public string GetCurrentProfileName()
        {
            return null;
        }

        public void MakeProfileDefault(string name)
        {
            
        }

        public void PersistGlobalItems(IEnumerable<string> keys = null)
        {
            
        } 

        public void RemovePersistor(string persistorId)
        { 
        }

        public void RemoveGlobalPersistor(string persistorId)
        {
             
        }

        public void PersistCurrentProfileItems(IEnumerable<string> keys)
        {
            
        }

        public void PersistProfileItems(string profileName, IEnumerable<string> keys)
        {
             
        }

        public void LoadGlobalItems(IEnumerable<string> keys = null)
        {
             
        }

        public void LoadCurrentProfileItems(IEnumerable<string> keys)
        {
            
        }

        public void LoadProfileItems(string profileName, IEnumerable<string> keys)
        {
            
        }


        public event EventHandler<ProfileSavedEventArgs> ProfileSaved;
    }

    internal class PersistenceStorageTestImpl : IPersistenceStorage
    {
        public void SaveState(XDocument state, string name)
        {

        }

        public XDocument LoadState(string name)
        {
            return new XDocument();
        }

        public void DeleteState(string name)
        {

        }

        public void RenameState(string oldName, string newName)
        {

        }

        private readonly ReadOnlyProfilesCollection m_availableProfiles =
            new ReadOnlyProfilesCollection(new ProfilesCollection());

        public ReadOnlyProfilesCollection AvailableProfiles
        {
            get { return m_availableProfiles; }
        }


        public void InitializeStorage()
        {
        }

        public void Apply()
        {
        }


        public string DefaultName { get; set; }
    }

    internal class ItemsDictionaryServiceTestImpl : IItemsDictionaryService
    {
        public Dictionary<string, XElement> SaveItemsToDict(ItemScope scope=ItemScope.All)
        {
            return new Dictionary<string, XElement>();
        }

        public Dictionary<string, XElement> SaveItemsToDict(IEnumerable<string> keys, ItemScope scope = ItemScope.All)
        {
            return new Dictionary<string, XElement>();
        }

        public void RestoreItemsFromDict(IEnumerable<KeyValuePair<string, XElement>> itemsDict, ItemScope scope = ItemScope.All, IEnumerable<string> keys = null)
        {
             
        }
    }

    internal class MSNetLoopTestImpl : IMSNetLoop
    {
        public void Loop()
        {
            throw new NotImplementedException();
        }

        public object Invoke(Delegate method_, params object[] args_)
        {
            throw new NotImplementedException();
        }

        public void Invoke(MSNetVoidVoidDelegate cb_)
        {
            throw new NotImplementedException();
        }

        public void InvokeWithState(MSNetVoidObjectDelegate cb_, object state_)
        {
            throw new NotImplementedException();
        }

        public void DispatchCallback(Delegate method_, params object[] args_)
        {
            throw new NotImplementedException();
        }

        public void DispatchCallback(AsyncCallback cb_, IAsyncResult res_)
        {
            throw new NotImplementedException();
        }

        public void DispatchCallback(MSNetVoidVoidDelegate cb_)
        {
            throw new NotImplementedException();
        }

        public void DispatchCallbackWithState(MSNetVoidObjectDelegate cb_, object state_)
        {
            throw new NotImplementedException();
        }

        public void InvokeLater(Delegate method_, params object[] args_)
        {
            throw new NotImplementedException();
        }

        public void InvokeLater(MSNetVoidVoidDelegate cb_)
        {
            throw new NotImplementedException();
        }

        public void InvokeLaterWithState(MSNetVoidObjectDelegate cb_, object state_)
        {
            throw new NotImplementedException();
        }

        public void CompleteAsyncOperationSynchronously(IAsyncResult res_)
        {
            throw new NotImplementedException();
        }

        public void CompleteAsyncOperationSynchronously(IAsyncResult res_, int timeoutMillis_)
        {
            throw new NotImplementedException();
        }

        public void Quit()
        {
            throw new NotImplementedException();
        }

        public object CallbackAt(DateTime time_, MSNetTimerDelegate cb_)
        {
            throw new NotImplementedException();
        }

        public object CallbackAt(DateTime time_, MSNetTimerDelegate cb_, string tag_)
        {
            throw new NotImplementedException();
        }

        public object CallbackAfterDelay(long delayInMillis_, MSNetTimerDelegate cb_)
        {
            throw new NotImplementedException();
        }

        public object CallbackAfterDelay(long delayInMillis_, MSNetTimerDelegate cb_, string tag_)
        {
            throw new NotImplementedException();
        }

        public object CallbackRepeatedly(long delayInMillis_, MSNetTimerDelegate cb_)
        {
            throw new NotImplementedException();
        }

        public object CallbackRepeatedly(long delayInMillis_, MSNetTimerDelegate cb_, string tag_)
        {
            throw new NotImplementedException();
        }

        public void SetCallbackTag(object id_, string tag_)
        {
            throw new NotImplementedException();
        }

        public void CancelCallback(object id_)
        {
            throw new NotImplementedException();
        }

        public bool InvokeRequired
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsRunning
        {
            get { throw new NotImplementedException(); }
        }

        public event MSNetVoidObjectDelegate OnLoopExit;
    }
}
