﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Concord/WindowTest.cs#3 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.IED.Concord.Application;

// The purpose of this test class is to make sure that our implementation's behaviour 
// is close enough to the original (including illogical things)
namespace MSDotNet.MSGui.Testing.Concord
{
  [TestClass]
  public abstract class WindowTest
  {
    public abstract IWindow CreateInstance();
    protected abstract IWindow CreateLinkableInstance();

    [TestMethod]
    public void IWindowConstructorTest()
    {
      IWindow window = CreateInstance();
    }

    [TestMethod]
    public void IWindowFormInitialTextNotSet()
    {
      IWindow window = CreateInstance();
      Assert.AreNotEqual(window.Caption, window.Form.Text);
    }

    [TestMethod]
    public void IWindowAffectsFormText()
    {
      IWindow window = CreateInstance();
      const string text = "new";
      window.Caption = text;
      Assert.AreEqual(text, window.Form.Text);
    }

    [TestMethod]
    public void IWindowNotAffectedByFormText()
    {
      IWindow window = CreateInstance();
      const string text = "new";
      window.Form.Text = text;
      Assert.AreNotEqual(text, window.Caption);
    }

    [TestMethod]
    public void IWindowCloseEvents()
    {
      IWindow window = CreateInstance();      
      bool closingCalled = false;
      bool closedCalled = false;
      bool formClosingCalled = false;
      bool formClosedCalled = false;
      window.Form.Closing += (sender_, args_) => closingCalled = true;
      window.Form.Closed += (sender_, args_) => closedCalled = true;
      window.Form.FormClosing += (sender_, args_) => formClosingCalled = true;
      window.Form.FormClosed += (sender_, args_) => formClosedCalled = true;      
      window.Close();
      System.Threading.Thread.Sleep(1000);
      Assert.IsTrue(closingCalled, "Closing was not called");
      Assert.IsTrue(closedCalled, "Closed was not called");
      Assert.IsTrue(formClosingCalled, "FormClosing was not called");
      Assert.IsTrue(formClosedCalled, "FormClosed was not called");
      //cancelling form closing does not cancel parent window closing - I don't know how to write a test for it
    }

    [TestMethod] 
    public void IWindowLinkControlInitiallyNullForNonLinkable()
    {
      IWindow window = CreateInstance();
      Assert.IsNull(window.LinkControl);
    }

    [TestMethod]
    public void IWindowLinkControlInitiallyNullForLinkable()
    {
      IWindow window = CreateLinkableInstance();
      Assert.IsNull(window.LinkControl);
    }

    [TestMethod]
    public void IWindowLinkControlSetForNonLinkable()
    {
      IWindow window = CreateInstance();
      window.LinkControl = new MockLinkControl();
      Assert.IsNotNull(window.LinkControl);
    }

    [TestMethod]
    public void IWindowLinkControlSetForLinkable()
    {
      IWindow window = CreateLinkableInstance();
      window.LinkControl = new MockLinkControl();
      Assert.IsNotNull(window.LinkControl);
    }

    [TestMethod]
    public void LinkControlCanBeSetOnlyOnce()
    {
      IWindow window = CreateInstance();
      var linkControl1 = new MockLinkControl();
      var linkControl2 = new MockLinkControl();
      window.LinkControl = linkControl1;
      window.LinkControl = linkControl2;
      Assert.AreSame(linkControl1, window.LinkControl);
    }

    [TestMethod]
    public void LinkControlCanBeSetOnlyOnce2()
    {
      IWindow window = CreateLinkableInstance();
      var linkControl1 = new MockLinkControl();
      var linkControl2 = new MockLinkControl();
      window.LinkControl = linkControl1;
      window.LinkControl = linkControl2;
      Assert.AreSame(linkControl1, window.LinkControl);
    }

    [TestMethod]
    public void IWindowActivationActivatesLinkControl()
    {
      IWindow window = CreateLinkableInstance();
      MockLinkControl linkControl1 = new MockLinkControl();      
      window.LinkControl = linkControl1;      
      window.SetFocus();
      Assert.IsTrue(linkControl1.Calls.Count > 0 && linkControl1.Calls[0] == "SetActive");
    }

    [TestMethod]
    public void SetFocusDoesNotSetFormFocus()
    {
      IWindow window = CreateInstance();
      Assert.IsFalse(window.Form.Focused);
      window.SetFocus();
      Assert.IsFalse(window.Form.Focused);
    }
  }

  internal class MockLinkControl : ILinkControl
  {
    readonly List<string> m_calls = new List<string>();
    public List<string> Calls
    {
      get
      {
        return m_calls;
      }
    }
    public void Delete()
    {
      m_calls.Add("Delete");
    }

    public Image LinkImage(int height)
    {
      throw new NotImplementedException();
    }

    public void SetActive(bool activate_)
    {
      m_calls.Add("SetActive");
    }

    public void HandleActivate()
    {
      m_calls.Add("HandleActivate");
    }

    public void HandleDeactivate()
    {
      m_calls.Add("HandleDeactivate");
    }

    public void HandleMouseClick(bool leftClick_, Point defaultClickLoc_)
    {
      m_calls.Add("HandleMouseClick");
    }

    public void HandleMouseLeave()
    {
      m_calls.Add("HandleMouseLeave");
    }

    public void HandleMouseMove()
    {
      m_calls.Add("HandleMouseMove");
    }

    public IntPtr GetActiveWindowHandle(bool isLaunched_)
    {
      throw new NotImplementedException();
    }

    public void LoadState(XmlNode state_, ref bool cancel_)
    {
      m_calls.Add("LoadState");
    }

    public void SaveState(XmlNode state_, ref bool cancel_)
    {
      m_calls.Add("SaveState");
    }
    
    public Form ClientForm
    {
      get;
      set;
    }

    private IColleague m_col = new Col();
    public IColleague Colleague
    {
      get { return m_col; }
    }

    public bool Enabled
    {
      get;
      set;
    }

    public bool IsFloating
    {
      get;
      set;
    }

    public event EventHandler LinkStateChanged;
  }
}
