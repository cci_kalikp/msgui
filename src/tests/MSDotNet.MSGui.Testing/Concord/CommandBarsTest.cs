﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Concord/CommandBarsTest.cs#8 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Drawing;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSNet;

namespace MSDotNet.MSGui.Testing.Concord
{
  /// <summary>
  /// Summary description for CommandBarsTest
  /// </summary>
  [TestClass]
  public abstract class CommandBarsTest
  {
	  [TestInitialize]
	  public void SetUp()
	  {
		  typeof(System.Windows.Application).GetField("_appInstance",
													   BindingFlags.NonPublic | BindingFlags.FlattenHierarchy |
													   BindingFlags.Static).SetValue
			  (null, null);
		  typeof(System.Windows.Application).GetField("_appCreatedInThisAppDomain",
													   BindingFlags.NonPublic | BindingFlags.FlattenHierarchy |
													   BindingFlags.Static).SetValue(null, false);
		  var application = new System.Windows.Application { MainWindow = null };
	  }

    public abstract ICommandBars CreateInstance();    

    [TestMethod]
    public void TestCommandBarsConstructor()
    {
      ICommandBars commandBars = CreateInstance();      
    }

    [TestMethod]
    public void TestCommandBarsInitialCount()
    {
      ICommandBars commandBars = CreateInstance();
      Assert.AreEqual(0, commandBars.Count);
    }

    [TestMethod]
    public void CommandBarsAddCount()
    {
      ICommandBars commandBars = CreateInstance();
      Assert.AreEqual(0, commandBars.Count);
      ICommandBar bar1 = commandBars.AddCommandBar("testName1", CommandBarType.Normal);
      Assert.AreEqual(1, commandBars.Count);
      ICommandBar bar2 = commandBars.AddCommandBar("testName2", CommandBarType.Menu);
      Assert.AreEqual(2, commandBars.Count);
      ICommandBar bar3 = commandBars.AddCommandBar("testName3", CommandBarType.Popup);
      Assert.AreEqual(2, commandBars.Count, "In the original implementation popup command bars are not added to the list of available bars");
    }

    [TestMethod,Ignore]
    public void CommandBarsStringIndex()
    {
      ICommandBars commandBars = CreateInstance();
      Assert.AreEqual(0, commandBars.Count);
      ICommandBar bar1 = commandBars.AddCommandBar("testName1", CommandBarType.Normal);
      Assert.AreEqual(1, commandBars.Count);
      ICommandBar bar2 = commandBars.AddCommandBar("testName2", CommandBarType.Menu);
      Assert.AreEqual(2, commandBars.Count);
      ICommandBar bar3 = commandBars.AddCommandBar("testName3", CommandBarType.Popup);
      Assert.AreEqual(2, commandBars.Count, "In the original implementation popup command bars are not added to the list of available bars");
      Assert.AreSame(commandBars["testName1"], bar1, "Normal Bar");
      Assert.AreSame(commandBars["testName2"], bar2, "Menu Bar");
      Assert.AreSame(commandBars["testName3"], bar3, "Popup Bar");
    }

    //this and the following test fail for concord.host
    //common sense tells me that they should pass, but actually, 
    //in Concord.Host Hashtable is used to store bars, hence numeric indices are hashtables's 
    [TestMethod]
    public void CommandBarsAccessAddedBarByIndex()
    {
      ICommandBars commandBars = CreateInstance();
      Assert.AreEqual(0, commandBars.Count);
      ICommandBar bar1 = commandBars.AddCommandBar("testName1", CommandBarType.Normal);
      Assert.AreEqual(bar1, commandBars[0], "by index");      
    }

    [TestMethod]
    public void CommandBarsUseIntegerIndex()
    {
      ICommandBars commandBars = CreateInstance();
      Assert.AreEqual(0, commandBars.Count);
      ICommandBar bar1 = commandBars.AddCommandBar("testName1", CommandBarType.Normal);
      Assert.AreEqual(1, commandBars.Count);
      ICommandBar bar2 = commandBars.AddCommandBar("testName2", CommandBarType.Menu);
      Assert.AreEqual(2, commandBars.Count);
      ICommandBar bar3 = commandBars.AddCommandBar("testName3", CommandBarType.Popup);
      Assert.AreEqual(2, commandBars.Count, "In the original implementation popup command bars are not added to the list of available bars");
      Assert.AreSame(commandBars[0], bar1, "Normal Bar");
      Assert.AreSame(commandBars[1], bar2, "Menu Bar");     
    }

    [TestMethod]
    public void CommandBarsRemoveCount()
    {
      ICommandBars commandBars = CreateInstance();
      Assert.AreEqual(0, commandBars.Count);
      ICommandBar bar1 = commandBars.AddCommandBar("testName1", CommandBarType.Normal);
      Assert.AreEqual(1, commandBars.Count);
      ICommandBar bar2 = commandBars.AddCommandBar("testName2", CommandBarType.Menu);
      Assert.AreEqual(2, commandBars.Count);
      ICommandBar bar3 = commandBars.AddCommandBar("testName3", CommandBarType.Popup);
      Assert.AreEqual(2, commandBars.Count, "In the original implementation popup command bars are not added to the list of available bars");
      commandBars.RemoveCommandBar(bar1);
      Assert.AreEqual(1, commandBars.Count);
      commandBars.RemoveCommandBar(bar2);
      Assert.AreEqual(0, commandBars.Count);
    }

    

    [TestMethod]
    public void CommandBarsRemoveAndCheckLeft()
    {
      ICommandBars commandBars = CreateInstance();
      Assert.AreEqual(0, commandBars.Count);
      ICommandBar bar1 = commandBars.AddCommandBar("testName1", CommandBarType.Normal);
      Assert.AreEqual(1, commandBars.Count);
      ICommandBar bar2 = commandBars.AddCommandBar("testName2", CommandBarType.Menu);
      Assert.AreEqual(2, commandBars.Count);
      ICommandBar bar3 = commandBars.AddCommandBar("testName3", CommandBarType.Popup);
      Assert.AreEqual(2, commandBars.Count, "In the original implementation popup command bars are not added to the list of available bars");      
      commandBars.RemoveCommandBar(bar2);      
      Assert.AreSame(commandBars["testName1"], bar1, "by name");
      Assert.AreEqual(bar1, commandBars[0], "by index");
    }    

    [TestMethod]
    public void CommandBarsAddingCommand()
    {
      ICommandBars commandBars = CreateInstance();
      commandBars.AddNamedCommand(
        "newcommandname", 
        "somecaption", 
        null, 
        CommandBarControlType.Button,                          
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());
      Assert.AreEqual(0, commandBars.Count, "No command bars should be created at this point");
    }

    [TestMethod]
    public void CommandBarsAddingManyCommands()
    {
      ICommandBars commandBars = CreateInstance();
      commandBars.AddNamedCommand(
        "newcommandname",
        "somecaption",
        null,
        CommandBarControlType.Button,
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());

      commandBars.AddNamedCommand(
        "newcommandname2",
        "somecaption2",
        null,
        CommandBarControlType.ComboBox,
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());

      commandBars.AddNamedCommand(
        "newcommandname3",
        "somecaption3",
        null,
        CommandBarControlType.DropDown,
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());

      commandBars.AddNamedCommand(
        "newcommandname4",
        "somecaption4",
        null,
        CommandBarControlType.Popup,
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());

      Assert.AreEqual(0, commandBars.Count, "No command bars should be created at this point");
    }

    [TestMethod]
    public void CommandBarsAddingAndRemovingCommands()
    {
      ICommandBars commandBars = CreateInstance();
      var command1 = commandBars.AddNamedCommand(
        "newcommandname",
        "somecaption",
        null,
        CommandBarControlType.Button,
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());

      var command2 = commandBars.AddNamedCommand(
        "newcommandname2",
        "somecaption2",
        null,
        CommandBarControlType.ComboBox,
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());

      var command3 = commandBars.AddNamedCommand(
        "newcommandname3",
        "somecaption3",
        null,
        CommandBarControlType.DropDown,
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());

      var command4 = commandBars.AddNamedCommand(
        "newcommandname4",
        "somecaption4",
        null,
        CommandBarControlType.Popup,
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());

      commandBars.RemoveCommand(command1);
      commandBars.RemoveCommand(command2);
      commandBars.RemoveCommand(command3);
      commandBars.RemoveCommand(command4);
	  
      Assert.AreEqual(0, commandBars.Count, "No command bars should be created at this point");
    }

    [TestMethod]
    public void CommandBarsAddCommandToBar()
    {
      ICommandBars commandBars = CreateInstance();
      var bar = commandBars.AddCommandBar("bar", CommandBarType.Normal);

      var command1 = commandBars.AddNamedCommand(
        "newcommandname",
        "somecaption",
        null,
        CommandBarControlType.Button,
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());

      bar.AddCommand(command1);      
    }

    [TestMethod]
    public void CommandBarsAddCommands()
    {
      ICommandBars commandBars = CreateInstance();
      ICommandBar bar = commandBars.AddCommandBar("bar", CommandBarType.Normal);

      ICommandBarControl command1 = commandBars.AddNamedCommand(
        "newcommandname",
        "somecaption",
        null,
        CommandBarControlType.Button,
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());

      Assert.IsTrue(command1 is ICommandBarButton, "CommandBarControlType.Button must produce ICommandBarButton");

      ICommandBarControl command2 = commandBars.AddNamedCommand(
        "newcommandname2",
        "somecaption2",
        null,
        CommandBarControlType.ComboBox,
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());

      Assert.IsTrue(command2 is ICommandBarDropDownButton, "CommandBarControlType.ComboBox must produce ICommandBarDropDownButton but was " + command1.GetType());

      ICommandBarControl command3 = commandBars.AddNamedCommand(
        "newcommandname3",
        "somecaption3",
        null,
        CommandBarControlType.DropDown,
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());

      Assert.IsTrue(command3 is ICommandBarDropDownButton, "CommandBarControlType.DropDown must produce ICommandBarDropDownButton");

      var command4 = commandBars.AddNamedCommand(
        "newcommandname4",
        "somecaption4",
        null,
        CommandBarControlType.Popup,
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());

      Assert.IsTrue(command4 is ICommandBarPopupControl, "CommandBarControlType.Popup must produce ICommandBarDropDownButton");

      bar.AddCommand(command1);
      bar.AddCommand(command2);
      bar.InsertCommand(1, command3);
      bar.InsertCommand(2, command4);
    }

    [TestMethod]
    public void CommandBarsAddAndRemoveCommand()
    {
      ICommandBars commandBars = CreateInstance();
      ICommandBar bar = commandBars.AddCommandBar("bar", CommandBarType.Normal);
      ICommandBarControl command1 = commandBars.AddNamedCommand(
        "newcommandname",
        "somecaption",
        null,
        CommandBarControlType.Button,
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());
      bar.AddCommand(command1);
      bar.RemoveCommand(command1);
    }

    //Both implementations don't throw any exceptions here
    [TestMethod]
    public void CommandBarsRemoveNonexistingCommand()
    {
      ICommandBars commandBars = CreateInstance();
      ICommandBar bar = commandBars.AddCommandBar("bar", CommandBarType.Normal);
      ICommandBarControl command1 = commandBars.AddNamedCommand(
        "newcommandname",
        "somecaption",
        null,
        CommandBarControlType.Button,
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());      
      bar.RemoveCommand(command1);
    }

    [TestMethod]
    public void CommandBarsRemoveExistingBar()
    {
      ICommandBars commandBars = CreateInstance();
      ICommandBar bar = commandBars.AddCommandBar("bar", CommandBarType.Normal);      
      commandBars.RemoveCommandBar(bar);
    }

    [TestMethod]
    //[ExpectedException(typeof(System.NullReferenceException))]
    //we don't expect any exceptions
    public void CommandBarsRemoveNonexistingBar()
    {
      ICommandBars commandBars = CreateInstance();      
      ICommandBar bar = commandBars.AddCommandBar("bar", CommandBarType.Normal);
      commandBars.RemoveCommandBar(bar);
      commandBars.RemoveCommandBar(bar);
    }

    [TestMethod,Ignore]
    [ExpectedException(typeof(System.NullReferenceException))]
    public void CommandBarsRemoveExistingPopupBar()
    {
      ICommandBars commandBars = CreateInstance();
      ICommandBar bar = commandBars.AddCommandBar("bar", CommandBarType.Popup);      
      commandBars.RemoveCommandBar(bar);
    }

    [TestMethod]
    public void CommandBarsClearBar()
    {
      ICommandBars commandBars = CreateInstance();
      ICommandBar bar = commandBars.AddCommandBar("bar", CommandBarType.Normal);
      ICommandBarControl command1 = commandBars.AddNamedCommand(
        "newcommandname",
        "somecaption",
        null,
        CommandBarControlType.Button,
        delegate(object sender_, ItemClickEventArgs e_)
        {
          throw new NotImplementedException("The command should not be called at this time");
        },
        MSNetLoopWithAffinityBase.GetLoop());
      bar.Clear();
    }

    [TestMethod]
    public void CommandBarsGetDefaultBarColor()
    {
      ICommandBars commandBars = CreateInstance();
      ICommandBar bar = commandBars.AddCommandBar("bar", CommandBarType.Normal);
      var color = bar.BackColor;
      Assert.AreEqual(color, Color.Empty);
    }

    //does not work as expected
    [TestMethod]
    public void CommandBarsSetAndGetBarColor()
    {
      ICommandBars commandBars = CreateInstance();
      ICommandBar bar = commandBars.AddCommandBar("bar", CommandBarType.Normal);
      bar.BackColor = Color.DarkViolet;
      Color color = bar.BackColor;
      //not supported neither in panewindow manager nor in our
      Assert.AreEqual(Color.Empty, color);
    }

    //it's arguable what should be here
    //it's true in Host and false in our implementation (actual visibility of a control is false)
    [TestMethod]
    public void CommandBarsGetDefaultVisible()
    {
      ICommandBars commandBars = CreateInstance();
      ICommandBar bar = commandBars.AddCommandBar("bar", CommandBarType.Normal);
      Assert.AreEqual(true, bar.Visible);
    }

    [TestMethod]
    public void CommandBarsCommandBarType()
    {
      foreach (object i in new System.Collections.Generic.List<string>())
      {
        
      }
      ICommandBars commandBars = CreateInstance();      
      ICommandBar bar1 = commandBars.AddCommandBar("testName1", CommandBarType.Normal);
      Assert.AreEqual(CommandBarType.Normal, bar1.Type);
      ICommandBar bar2 = commandBars.AddCommandBar("testName2", CommandBarType.Menu);
      Assert.AreEqual(CommandBarType.Menu, bar2.Type);
      ICommandBar bar3 = commandBars.AddCommandBar("testName3", CommandBarType.Popup);
      Assert.AreEqual(CommandBarType.Popup, bar3.Type);
    }
    [TestMethod]
    public void CommandBarsGetBarName()
    {
      ICommandBars commandBars = CreateInstance();
      ICommandBar bar = commandBars.AddCommandBar("bar", CommandBarType.Normal);
      Assert.AreEqual("bar", bar.Name);
    }

    [TestMethod]
    public void CommandBarsChangeBarName()
    {
      ICommandBars commandBars = CreateInstance();
      ICommandBar bar = commandBars.AddCommandBar("bar", CommandBarType.Normal);
      Assert.AreEqual("bar", bar.Name);
      bar.Name = "newname";
      Assert.AreEqual("newname", bar.Name);
      Assert.AreEqual(commandBars.Count, 1);
      //they are not same Concord.Host (legacy tests) and I think it is a bug in Concord's version
      //we repeated this error in our implementation
      Assert.AreNotSame(bar, commandBars["newname"]);
    }

    [TestMethod,Ignore]
    public void CommandBarsAddPopup()
    {
      ICommandBars commandBars = CreateInstance();
      ICommandBar bar = commandBars.AddCommandBar("popupBar", CommandBarType.Popup);
      Assert.AreEqual(bar, null);
    }
  }
}
