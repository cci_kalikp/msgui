﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/ModuleToModuleCommunication/AdapterTest.cs#3 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace MSDotNet.MSGui.Testing.ModuleToModuleCommunication
{
    /// <summary>
    /// Tests the adapter mechanism
    /// </summary>
    [TestClass]
    public class AdapterTest
    {
        private class Message1
        {
            public string Text { get; set; }
        }

        private class Message2
        {
            public string Text { get; set; }
        }

        private class Message3
        {
            public string Text { get; set; }
        }

        public class Message4
        {
            public string Text { get; set; }
        }

// ReSharper disable ClassNeverInstantiated.Local
        private class Module1 : IModule
// ReSharper restore ClassNeverInstantiated.Local
        {
            public void Initialize() {}
            public static SubscriptionToken Subscribe<TMessageType, TPayload>(IEventAggregator eventAggregator_, Action<TPayload> action_)
                where TMessageType : ModuleToModuleMessageEvent<TPayload>
            {
                return eventAggregator_.GetEvent<TMessageType>().Subscribe(action_);
            }
            public static void Unsubscribe<TMessageType, TPayload>(IEventAggregator eventAggregator_, SubscriptionToken token_)
                where TMessageType : ModuleToModuleMessageEvent<TPayload>
            {
                eventAggregator_.GetEvent<TMessageType>().Unsubscribe(token_);
            }
            public static void Unsubscribe<TMessageType, TPayload>(IEventAggregator eventAggregator_, Action<TPayload> action_)
                where TMessageType : ModuleToModuleMessageEvent<TPayload>
            {
                eventAggregator_.GetEvent<TMessageType>().Unsubscribe(action_);
            }
            public static void Publish<TMessageType, TPayload>(IEventAggregator eventAggregator_, TPayload payload_)
                where TMessageType : ModuleToModuleMessageEvent<TPayload>
            {
                eventAggregator_.GetEvent<TMessageType>().Publish(payload_);
            }
        }

// ReSharper disable ClassNeverInstantiated.Local
        private class Module2 : IModule
// ReSharper restore ClassNeverInstantiated.Local
        {
            public void Initialize() { }
            public static SubscriptionToken Subscribe<TMessageType, TPayload>(IEventAggregator eventAggregator_, Action<TPayload> action_)
                where TMessageType : ModuleToModuleMessageEvent<TPayload>
            {
                return eventAggregator_.GetEvent<TMessageType>().Subscribe(action_);
            }
            public static void Unsubscribe<TMessageType, TPayload>(IEventAggregator eventAggregator_, SubscriptionToken token_)
                where TMessageType : ModuleToModuleMessageEvent<TPayload>
            {
                eventAggregator_.GetEvent<TMessageType>().Unsubscribe(token_);
            }
            public static void Unsubscribe<TMessageType, TPayload>(IEventAggregator eventAggregator_, Action<TPayload> action_)
                where TMessageType : ModuleToModuleMessageEvent<TPayload>
            {
                eventAggregator_.GetEvent<TMessageType>().Unsubscribe(action_);
            }
            public static void Publish<TMessageType, TPayload>(IEventAggregator eventAggregator_, TPayload payload_)
                where TMessageType : ModuleToModuleMessageEvent<TPayload>
            {
                eventAggregator_.GetEvent<TMessageType>().Publish(payload_);
            }
        }

        private ModuleToModuleMessageEventAggregator m_eventAggregator;
        private AdapterService m_adapterService;

        [TestInitialize]
        public void Initialize()
        {
            m_eventAggregator = new ModuleToModuleMessageEventAggregator();
            Assert.IsNotNull(m_eventAggregator);
            Assert.IsInstanceOfType(m_eventAggregator, typeof(IEventAggregator));
            m_adapterService = new AdapterService(m_eventAggregator);
        }

        [TestMethod]
        public void DirectMessaging()
        {
            var message1 = new Message1 { Text = "msg1" };
            Module1.Subscribe<ModuleToModuleMessageEvent<Message1>, Message1>(m_eventAggregator,
                msg_ =>
                    {
                        Assert.AreEqual(message1, msg_);
                        Assert.AreSame(message1, msg_);
                    });
            Module2.Publish<ModuleToModuleMessageEvent<Message1>, Message1>(m_eventAggregator, message1);
        }

        [TestMethod]
        public void SingeStepAdaptation()
        {
            var message1 = new Message1 { Text = "msg1" };
            m_adapterService.AddAdapter<Message1, Message2>(msg_ => new Message2 { Text = msg_.Text });
            Module1.Subscribe<ModuleToModuleMessageEvent<Message2>, Message2>(m_eventAggregator,
                msg_ =>
                    {
                        Assert.AreEqual(message1, msg_);
                        Assert.AreNotSame(message1, msg_);
                    });
            Module2.Publish<ModuleToModuleMessageEvent<Message1>, Message1>(m_eventAggregator, message1);
        }

        [TestMethod]
        public void TwoStepAdaptation()
        {
            var message1 = new Message1 { Text = "msg1" };
            m_adapterService.AddAdapter<Message1, Message2>(msg_ => new Message2 {Text = msg_.Text});
            m_adapterService.AddAdapter<Message2, Message3>(msg_ => new Message3 {Text = msg_.Text});
            Module1.Subscribe<ModuleToModuleMessageEvent<Message3>, Message3>(m_eventAggregator,
                msg_ =>
                {
                    Assert.AreEqual(message1, msg_);
                    Assert.AreNotSame(message1, msg_);
                });
            Module2.Publish<ModuleToModuleMessageEvent<Message1>, Message1>(m_eventAggregator, message1);
        }

        [TestMethod]
        public void ThreeStepAdaptation()
        {
            var message1 = new Message1 { Text = "msg1" };
            m_adapterService.AddAdapter<Message1, Message2>(msg_ => new Message2 { Text = msg_.Text });
            m_adapterService.AddAdapter<Message2, Message3>(msg_ => new Message3 { Text = msg_.Text });
            m_adapterService.AddAdapter<Message3, Message4>(msg_ => new Message4 { Text = msg_.Text });
            Module1.Subscribe<ModuleToModuleMessageEvent<Message4>, Message4>(m_eventAggregator,
                msg_ =>
                {
                    Assert.AreEqual(message1, msg_);
                    Assert.AreNotSame(message1, msg_);
                });
            Module2.Publish<ModuleToModuleMessageEvent<Message1>, Message1>(m_eventAggregator, message1);
        }

        [TestMethod]
        public void ParralelPaths()
        {
            var message1 = new Message1 { Text = "msg1" };
            m_adapterService.AddAdapter<Message1, Message2>(msg_ =>
                                                                {
                                                                    Console.WriteLine("1->2");
                                                                    return new Message2 {Text = msg_.Text};
                                                                });
            m_adapterService.AddAdapter<Message2, Message3>(msg_ =>
                                                                {
                                                                    Console.WriteLine("2->3");
                                                                    return new Message3 {Text = msg_.Text};
                                                                });
            m_adapterService.AddAdapter<Message1, Message4>(msg_ =>
                                                                {
                                                                    Console.WriteLine("1->4");
                                                                    return new Message4 {Text = msg_.Text};
                                                                });
            m_adapterService.AddAdapter<Message4, Message3>(msg_ =>
                                                                {
                                                                    Console.WriteLine("4->3");
                                                                    return new Message3 {Text = msg_.Text};
                                                                });
            Module1.Subscribe<ModuleToModuleMessageEvent<Message3>, Message3>(m_eventAggregator,
                msg_ =>
                {
                    Assert.AreEqual(message1, msg_);
                    Assert.AreNotSame(message1, msg_);
                });
            Module2.Publish<ModuleToModuleMessageEvent<Message1>, Message1>(m_eventAggregator, message1);
        }
    }
}
