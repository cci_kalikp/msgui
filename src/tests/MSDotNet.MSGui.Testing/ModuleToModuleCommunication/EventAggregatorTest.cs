﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/ModuleToModuleCommunication/EventAggregatorTest.cs#3 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Linq;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace MSDotNet.MSGui.Testing.ModuleToModuleCommunication
{
    /// <summary>
    /// Tests the custom event aggregator
    /// </summary>
    [TestClass]
    public class EventAggregatorTest
    {

        private ModuleToModuleMessageEventAggregator m_eventAggregator;

        [TestInitialize]
        public void Initialize()
        {
            m_eventAggregator = new ModuleToModuleMessageEventAggregator();
            Assert.IsNotNull(m_eventAggregator);
            Assert.IsInstanceOfType(m_eventAggregator, typeof(IEventAggregator));
        }

        [TestMethod]
        public void TestEventAggregator()
        {
            m_eventAggregator = new ModuleToModuleMessageEventAggregator();
            var count = m_eventAggregator.EventTypes.Count;
            Assert.AreEqual(0, count);
            var eventBase = m_eventAggregator.GetEvent<CompositePresentationEvent<int>>();
            Assert.IsNotNull(eventBase);
            Assert.IsInstanceOfType(eventBase, typeof(EventBase));
            Assert.IsInstanceOfType(eventBase, typeof(CompositePresentationEvent<int>));
            Assert.AreEqual(1, m_eventAggregator.EventTypes.Count);
            Assert.AreEqual(eventBase, m_eventAggregator.EventTypes.Keys.First());
            Assert.AreEqual(typeof(int), m_eventAggregator.EventTypes[eventBase]);
        }
    }
}
