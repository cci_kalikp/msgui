﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Options/OptionsWindowTest.cs#6 $
// $Change: 851091 $
// $DateTime: 2013/10/25 00:44:42 $
// $Author: caijin $

using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.MSDotNet.MSGui.Impl.Options;

namespace MSDotNet.MSGui.Testing.Options
{
  [TestClass]
  public class OptionsWindowTest
  {
    [TestMethod]
    public void TestConstructor()
    {
      OptionsWindow ow = new OptionsWindow(null, null, null);
      Assert.IsTrue(ow != null, "The OptionsWindow constructor failed, it returned null.");
    }
  }
}