﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/View/ViewContainerTests.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.View;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MSDotNet.MSGui.Testing.View
{
  [TestClass]
  public class ViewContainerTests
  {    
    [TestMethod]
    public void ViewContainerCloseEventsCalled()
    {
      IViewContainer viewContainer = new WindowViewContainer("");
      bool closed = false;
      bool closing = false;
      viewContainer.Closing += (sender_, args_) => closing = true;
      viewContainer.Closed += (sender_, args_) => closed = true;
      viewContainer.Close();
      Assert.IsTrue(closing);
      Assert.IsTrue(closed);
    }

    [TestMethod]
    public void ViewContainerClosingCanCancel()
    {
		IViewContainer viewContainer = new WindowViewContainer("");
      bool closed = false;
      bool closing = false;
      viewContainer.Closing += (sender_, args_) =>
                                 {
                                   closing = true;
                                   args_.Cancel = true;
                                 };
      viewContainer.Closed += (sender_, args_) => closed = true;
      viewContainer.Close();
      Assert.IsTrue(closing);
      Assert.IsFalse(closed);
    }
  }
}
