﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Persistence/MultipleFilePersistenceStorageInvalidFoldersTest.cs#3 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSDotNet.MSGui.Testing.Persistence
{
  [TestClass]
  public class MultipleFilePersistenceStorageInvalidFoldersTest
  {
    private string m_appName = "TestAppName";

    [TestMethod]
    public void IllegalFoldersTest()
    {

      var fps = new MultipleFilePersistenceStorage(m_appName, "AAABBB:\a\n\tsdasdsad");
      fps.BaseFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.AlternativeFolder = ("AAABBB:\a\n\tsdasdsad");
      //no exceptions expected
    }

    [TestMethod]
    public void IllegalAppnameTest()
    {
      if (Directory.Exists("U:") && Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)))
      {
        var fps = new MultipleFilePersistenceStorage("11+++\a\n\tsdasdsa&*!Ad");
        Assert.IsNotNull(fps.BaseFolder);
        Assert.IsNotNull(fps.AlternativeFolder);
        //names are sanitized, dirs created
        Assert.AreEqual("U:\\Application Data\\Morgan Stanley", Directory.GetParent(fps.BaseFolder).FullName);
        Assert.AreEqual(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                                           "Morgan Stanley"), Directory.GetParent(fps.AlternativeFolder).FullName);
        Directory.Delete(fps.BaseFolder);
        Directory.Delete(fps.AlternativeFolder);
      }
    }

    [TestMethod]
    public void IllegalAppnameTest2()
    {
      if (Directory.Exists("U:") && Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)))
      {
        string expectedBase = "U:\\Application Data\\Morgan Stanley\\_____.";
        string expectedAlternative = Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                                           "Morgan Stanley"), "_____.");
        var fps = new MultipleFilePersistenceStorage("..\\../...");
        Assert.AreEqual(expectedBase, fps.BaseFolder);
        Assert.AreEqual(expectedAlternative, fps.AlternativeFolder);
        //names are sanitized, dirs created
        Directory.Delete(expectedBase);
        Directory.Delete(expectedAlternative);
        //if exception here, smth was wrong with dir creation        
      }
    }

    [TestMethod]
    [ExpectedException(
      typeof(System.InvalidOperationException),
      "Unable to rename state Default to Default1")]
    public void IllegalFoldersRename()
    {
      var fps = new MultipleFilePersistenceStorage(m_appName, "AAABBB:\a\n\tsdasdsad");
      fps.BaseFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.AlternativeFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.RenameState("Default", "Default1");
    }

    [TestMethod]
    [ExpectedException(
      typeof(System.InvalidOperationException),
      "Unable to delete state Default")]
    public void IllegalFoldersDelete()
    {
      var fps = new MultipleFilePersistenceStorage(m_appName, "AAABBB:\a\n\tsdasdsad");
      fps.BaseFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.AlternativeFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.DeleteState("Default");
    }

    [TestMethod]
    [ExpectedException(
      typeof(System.InvalidOperationException),
      "Failed to Save item SomePersistorID1234 from profile Default to any location")]
    public void IllegalFoldersSaveItem()
    {
      var fps = new MultipleFilePersistenceStorage(m_appName, "AAABBB:\a\n\tsdasdsad");
      fps.BaseFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.AlternativeFolder = ("AAABBB:\a\n\tsdasdsad");
      XElement state = new XElement("Qwerty");
      fps.SaveState("Default", new Dictionary<string, XElement>(){{"SomePersistorID1234", state}}, null, false);
    }

    [TestMethod]
    [ExpectedException(
      typeof(System.InvalidOperationException),
      "Unable to clean profile Default")]
    public void IllegalFoldersSaveItemAndClean()
    {
      var fps = new MultipleFilePersistenceStorage(m_appName, "AAABBB:\a\n\tsdasdsad");
      fps.BaseFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.AlternativeFolder = ("AAABBB:\a\n\tsdasdsad");
      XElement state = new XElement("Qwerty");
      fps.SaveState("Default", new Dictionary<string, XElement>() { { "SomePersistorID1234", state } }, null, true);
    }

    [TestMethod]
    [ExpectedException(
      typeof(System.InvalidOperationException),
      "Failed to Save subitem 'SomeSubitemID1234' for profile 'Default' to any location")]
    public void IllegalFoldersSaveSubItem()
    {
      var fps = new MultipleFilePersistenceStorage(m_appName, "AAABBB:\a\n\tsdasdsad");
      fps.BaseFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.AlternativeFolder = ("AAABBB:\a\n\tsdasdsad");
      XElement state = new XElement("Qwerty");      
      fps.SaveState("Default", null, new Dictionary<string, XElement>() { { "SomeSubitemID1234", state } }, false);
    }

    [TestMethod]
    [ExpectedException(
      typeof(System.InvalidOperationException),
      "Unable to clean profile Default")]
    public void IllegalFoldersSaveSubItemAndClean()
    {
      var fps = new MultipleFilePersistenceStorage(m_appName, "AAABBB:\a\n\tsdasdsad");
      fps.BaseFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.AlternativeFolder = ("AAABBB:\a\n\tsdasdsad");
      XElement state = new XElement("Qwerty");
      fps.SaveState("Default", null, new Dictionary<string, XElement>() { { "SomeSubitemID1234", state } }, true);
    }

    [TestMethod]
    [ExpectedException(
      typeof(System.InvalidOperationException),
      "Unable to load profile 'Default'. It is not available.")]
    public void IllegalFoldersLoad()
    {
      var fps = new MultipleFilePersistenceStorage(m_appName, "AAABBB:\a\n\tsdasdsad");
      fps.BaseFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.AlternativeFolder = ("AAABBB:\a\n\tsdasdsad");
      Dictionary<string, XElement> items;
      Dictionary<string, XElement> subitems;
      fps.LoadState("Default", out items, out subitems);      
    }

    [TestMethod]
    [ExpectedException(
      typeof(System.InvalidOperationException),
      "Application name must be not empty")]
    public void EmptyApplicationName()
    {
      var fps = new MultipleFilePersistenceStorage("");
    }

    [TestMethod]
    [ExpectedException(
      typeof(System.InvalidOperationException),
      "Application name must be not empty")]
    public void NullApplicationName()
    {
      var fps = new MultipleFilePersistenceStorage((string)null);
    }
  }
}
