﻿////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Persistence/StaticFunctionsPersistenceProfileServiceTest.cs#3 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Collections.Generic;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace MSDotNet.MSGui.Testing.Persistence
{
  //Classes: PackItem UnpackItem BuildXPath XDocumentToDict DictToXDocument BuildParentPath FindOrder

  //Calls: 
  //1) XDocumentToDict --> UnpackItem
  //2) DictToXDocument --> PackItem -> BuildXPath --> BuildParentPath --> FindOrder

  [TestClass]
  public class StaticFunctionsPersistenceProfileServiceTest
  {
    [TestMethod]
    public void TestXDocumentToDictSimpleItems()
    {
      //gets <State>
      //       <Item Id="x">...</Item>
      //       <Item Id="y">...</Item>
      //       <Item Id="z">...</Item>
      //     </State>
      //returns dict of children of Items with Id's as keys      

      XDocument document =
        XDocument.Parse("<State><Item Id=\"Id1\"><State1/></Item><Item Id=\"Id2\"><A E=\"F\"><B/><C>D</C></A></Item><Item Id=\"Id3\"><Item/></Item></State>");

      Dictionary<string, XElement> resultDictionary = PersistenceProfileService.XDocumentToDict(document);
      Assert.AreEqual(3, resultDictionary.Count);
      Assert.AreEqual("<State1 />", resultDictionary["Id1"].ToString());
      Assert.AreEqual("<A E=\"F\">\r\n  <B />\r\n  <C>D</C>\r\n</A>", resultDictionary["Id2"].ToString());
      Assert.AreEqual("<Item />", resultDictionary["Id3"].ToString());
    }

    [TestMethod]
    public void TestXDocumentToDictNoItems()
    {       
      XDocument document =
        XDocument.Parse("<State></State>");

      Dictionary<string, XElement> resultDictionary = PersistenceProfileService.XDocumentToDict(document);
      Assert.AreEqual(0, resultDictionary.Count);      
    }

    [TestMethod]    
    public void TestXDocumentToDictEmptyItem()
    {
      //either one child - state or 2 children - subitems
      //we don't throw an exception, and don't add this to the dictionary
      XDocument document =
        XDocument.Parse("<State><Item Id=\"aaa\"/></State>");

      Dictionary<string, XElement> resultDictionary = PersistenceProfileService.XDocumentToDict(document);
      Assert.AreEqual(0, resultDictionary.Count);
    }

    [TestMethod]    
    public void TestXDocumentToDictItemWithoutId()
    {
      //no Id - ignore
      XDocument document =
        XDocument.Parse("<State><Item/><A/></State>");

      Dictionary<string, XElement> resultDictionary = PersistenceProfileService.XDocumentToDict(document);
      Assert.AreEqual(0, resultDictionary.Count);
    }

    [TestMethod]
    public void TestXDocumentToDictItemsAndSubitems()
    {      
      XDocument document =
        XDocument.Parse(
        "<State>" +
        "  <Item Id=\"aaa\">" +
        "    <Content>" +
        "      <AAA>" +
        "        <BBB Attr=\"attr1\"/>" +
        "      </AAA>" +
        "    </Content>" +
        "    <Subitems>" +
        "      <Subitem Id=\"Id1\" XPath=\"AAA[1]/BBB[1]\" />" +
        "    </Subitems>" +
        "  </Item>" +
        "</State>" 
        );

      Dictionary<string, XElement> resultDictionary = PersistenceProfileService.XDocumentToDict(document);
      Assert.AreEqual(1, resultDictionary.Count);
      Assert.IsTrue(resultDictionary["aaa"] is XElementWithSubitems);
      XElementWithSubitems savedElement = (XElementWithSubitems)resultDictionary["aaa"];
      Assert.AreEqual(1, savedElement.Subitems.Count);
      Assert.AreEqual("<BBB Attr=\"attr1\" />", savedElement.Subitems["Id1"].ToString());
    }

    [TestMethod]
    public void TestUnpackItemsAndSubitems()
    { 
      XElement itemElement =
        XElement.Parse(        
        "<Item Id=\"aaa\">" +
        "  <Content>" +
        "    <AAA>" +
        "      <BBB Attr=\"attr1\"/>" +
        "    </AAA>" +
        "  </Content>" +
        "  <Subitems>" +
        "    <Subitem Id=\"Id1\" XPath=\"AAA[1]/BBB[1]\" />" +
        "  </Subitems>" +
        "</Item>"
        );

      KeyValuePair<string, XElement> unpacked = PersistenceProfileService.UnpackItem(itemElement);

      Assert.AreEqual("aaa", unpacked.Key);
      Assert.IsTrue(unpacked.Value is XElementWithSubitems);
      XElementWithSubitems savedElement = (XElementWithSubitems)unpacked.Value;
      Assert.AreEqual(1, savedElement.Subitems.Count);
      Assert.AreEqual("<BBB Attr=\"attr1\" />", savedElement.Subitems["Id1"].ToString());
    }

    [TestMethod]
    public void TestUnpackItemsAndSubitems2()
    {
      XElement itemElement =
        XElement.Parse(
        "<Item Id=\"aaa\">" +
        "  <Content>" +
        "    <AAA>" +
        "      <BBB Attr=\"attr1\"/>" +
        "    </AAA>" +
        "    <AAA>" +
        "      <CCC Attr=\"attr2\"/>" +
        "    </AAA>" +
        "  </Content>" +
        "  <Subitems>" +
        "    <Subitem Id=\"Id1\" XPath=\"AAA[2]/CCC[1]\" />" +
        "  </Subitems>" +
        "</Item>"
        );

      KeyValuePair<string, XElement> unpacked = PersistenceProfileService.UnpackItem(itemElement);

      Assert.AreEqual("aaa", unpacked.Key);
      Assert.IsTrue(unpacked.Value is XElementWithSubitems);
      XElementWithSubitems savedElement = (XElementWithSubitems)unpacked.Value;
      Assert.AreEqual(1, savedElement.Subitems.Count);
      Assert.AreEqual("<CCC Attr=\"attr2\" />", savedElement.Subitems["Id1"].ToString());
    }

    [TestMethod]
    public void TestUnpackItemsAndSubitems3()
    {
      XElement itemElement =
        XElement.Parse(
        "<Item Id=\"aaa\">" +
        "  <Content>" +
        "    <AAA>" +
        "      <BBB Attr=\"attr1\"/>" +
        "    </AAA>" +
        "    <AAA>" +
        "      <CCC Attr=\"attr2\"/>" +
        "    </AAA>" +
        "  </Content>" +
        "  <Subitems>" +
        "    <Subitem Id=\"Id1\" XPath=\".\" />" +
        "  </Subitems>" +
        "</Item>"
        );
      string expectedSubitemString =
        "<Content>\r\n" +
        "  <AAA>\r\n" +
        "    <BBB Attr=\"attr1\" />\r\n" +
        "  </AAA>\r\n" +
        "  <AAA>\r\n" +
        "    <CCC Attr=\"attr2\" />\r\n" +
        "  </AAA>\r\n" +
        "</Content>";

      KeyValuePair<string, XElement> unpacked = PersistenceProfileService.UnpackItem(itemElement);

      Assert.AreEqual("aaa", unpacked.Key);
      Assert.IsTrue(unpacked.Value is XElementWithSubitems);
      XElementWithSubitems savedElement = (XElementWithSubitems)unpacked.Value;
      Assert.AreEqual(1, savedElement.Subitems.Count);
      Assert.AreEqual(expectedSubitemString, savedElement.Subitems["Id1"].ToString());
    }

    [TestMethod]
    public void TestPackAndRemove()
    {
      var subitem = new XElement("CCC", new XAttribute("Attr", "attr2"), new XElement("DDD"));
      XElementWithSubitems toPack = new XElementWithSubitems(
        "Content", 
        new XElement("AAA",
          new XElement("BBB", new XAttribute("Attr", "attr1"))),
        new XElement("AAA", subitem));
      toPack.Subitems.Add("Id1", subitem);

      var expected = new XElement (
        "Content", 
        new XElement("AAA",
          new XElement("BBB", new XAttribute("Attr", "attr1"))),
        new XElement("AAA", new XElement("CCC")));

      XElement result = PersistenceProfileService.PackItem("aaa", toPack, true);
      Assert.IsNotNull(result.FirstNode);
      Assert.IsNotNull(result.Element("Subitems"));
      Assert.AreEqual(result.FirstNode.ToString(), expected.ToString());
      Assert.AreEqual(1, new List<XElement>(result.Element("Subitems").Elements("Subitem")).Count);
      Assert.AreEqual(result.Element("Subitems").Element("Subitem").Attribute("Id").Value, "Id1");
      Assert.AreEqual(result.Element("Subitems").Element("Subitem").Attribute("XPath").Value, "AAA[2]/CCC[1]");
    }

    [TestMethod]
    public void TestPackAndNotRemove()
    {
      var subitem = new XElement("CCC", new XAttribute("Attr", "attr2"), new XElement("DDD"));
      XElementWithSubitems toPack = new XElementWithSubitems(
        "Content",
        new XElement("AAA",
          new XElement("BBB", new XAttribute("Attr", "attr1"))),
        new XElement("AAA", subitem));
      toPack.Subitems.Add("Id1", subitem);

      string expectedString = toPack.ToString();

      XElement result = PersistenceProfileService.PackItem("aaa", toPack, false);
      Assert.IsNotNull(result.FirstNode);
      Assert.IsNotNull(result.Element("Subitems"));
      Assert.AreEqual(result.FirstNode.ToString(), expectedString);
      Assert.AreEqual(1, new List<XElement>(result.Element("Subitems").Elements("Subitem")).Count);
      Assert.AreEqual(result.Element("Subitems").Element("Subitem").Attribute("Id").Value, "Id1");
      Assert.AreEqual(result.Element("Subitems").Element("Subitem").Attribute("XPath").Value, "AAA[2]/CCC[1]");
    }

    [TestMethod]
    public void TestPackandRemoveRoot()
    {      
      XElementWithSubitems toPack = new XElementWithSubitems(
        "Content",
        new XElement("AAA",
          new XElement("BBB", new XAttribute("Attr", "attr1"))),
        new XElement("AAA", 
          new XElement("CCC", new XAttribute("Attr", "attr2"), 
            new XElement("DDD"))));
      toPack.Subitems.Add("Id1", toPack);

      string expectedString = "<Content />";      

      XElement result = PersistenceProfileService.PackItem("aaa", toPack, true);
      Assert.IsNotNull(result.FirstNode);
      Assert.IsNotNull(result.Element("Subitems"));
      Assert.AreEqual(result.FirstNode.ToString(), expectedString);
      Assert.AreEqual(1, new List<XElement>(result.Element("Subitems").Elements("Subitem")).Count);
      Assert.AreEqual(result.Element("Subitems").Element("Subitem").Attribute("Id").Value, "Id1");
      Assert.AreEqual(result.Element("Subitems").Element("Subitem").Attribute("XPath").Value, ".");
    }

    [TestMethod]
    public void TestPackWithoutSubitems()
    {
      XElement toPack = new XElement(
        "Content",
        new XElement("AAA",
          new XElement("BBB", new XAttribute("Attr", "attr1"))),
        new XElement("AAA", new XElement("CCC", new XAttribute("Attr", "attr2"), new XElement("DDD"))));      

      string expectedString = toPack.ToString();

      XElement result = PersistenceProfileService.PackItem("aaa", toPack, false);
      Assert.AreEqual(1, new List<XElement>(result.Elements()).Count);      
      Assert.AreEqual(result.FirstNode.ToString(), expectedString);      
    }

    [TestMethod]
    public void TestDictToXDocument()
    {
      XElement toPack = new XElement(
        "Content",
        new XElement("AAA",
          new XElement("BBB", new XAttribute("Attr", "attr1"))),
        new XElement("AAA", new XElement("CCC", new XAttribute("Attr", "attr2"), new XElement("DDD"))));

      string expectedString = new XDocument(
        new XElement("State",
          new XElement("Item", new XAttribute("Id", "ItemId"), toPack))).ToString();

      XDocument document = PersistenceProfileService.DictToXDocument(
        new Dictionary<string, XElement> {{"ItemId", toPack}});
      
      Assert.AreEqual(expectedString, document.ToString());
    }

    [TestMethod]
    public void TestDictToXDocumentWithSubitems()
    {
      var subitem = new XElement("CCC", new XAttribute("Attr", "attr2"), new XElement("DDD"));
      XElementWithSubitems toPack = new XElementWithSubitems(
        "Content",
        new XElement("AAA",
          new XElement("BBB", new XAttribute("Attr", "attr1"))),
        new XElement("AAA", subitem));
      toPack.Subitems.Add("Id1", subitem);

      string expectedString = new XDocument(
        new XElement("State",
          new XElement("Item", 
            new XAttribute("Id", "ItemId"), 
            toPack,
            new XElement("Subitems",
              new XElement("Subitem", 
                new XAttribute("Id", "Id1"),
                new XAttribute("XPath", "AAA[2]/CCC[1]")))))).ToString();

      XDocument document = PersistenceProfileService.DictToXDocument(
        new Dictionary<string, XElement> { { "ItemId", toPack } });

      Assert.AreEqual(expectedString, document.ToString());
    }

   
  }
}
