﻿////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Persistence/PersistenceProfileServiceTest.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.Threading;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using Microsoft.VisualStudio.TestTools.UnitTesting;

//TODO

//using Rhino.Mocks;

//namespace MorganStanley.MSDotNet.MSGui.Persistence
//{
//  [TestClass]
//  public class PersistenceProfileServiceTest
//  {    

//    private static PersistenceProfileService Create(
//      out MockRepository mocks_,
//      out IPersistenceStorage persistenceStorage_,
//      out IItemsDictionaryService itemsDictionaryService_,
//      out IStatusBar statusBar_)
//    {
//      mocks_ = new MockRepository();
//      persistenceStorage_ = mocks_.StrictMock<IPersistenceStorage>();
//      itemsDictionaryService_ = new PersistenceService(); //(IItemsDictionaryService)mocks_.CreateMock(typeof(IItemsDictionaryService));
//      statusBar_ = mocks_.StrictMock<IStatusBar>();
//      var pps = new PersistenceProfileService(
//        persistenceStorage_, itemsDictionaryService_, statusBar_);            
//      return pps;
//    }    

//    [TestMethod]
//    public void TestConstructor()
//    {
//      MockRepository mocks_;
//      IPersistenceStorage persistenceStorage_;
//      IItemsDictionaryService itemsDictionaryService_;
//      IStatusBar statusBar_;
//      PersistenceProfileService pps = Create(out mocks_, out persistenceStorage_, out itemsDictionaryService_, out statusBar_);
//    }

//    [TestMethod]
//    public void TestAvailableProfiles()
//    {
//      MockRepository mocks_;
//      IPersistenceStorage persistenceStorage_;
//      IItemsDictionaryService itemsDictionaryService_;
//      IStatusBar statusBar_;
//      PersistenceProfileService pps = Create(out mocks_, out persistenceStorage_, out itemsDictionaryService_, out statusBar_);

//      ProfilesCollection testProfiles = new ProfilesCollection() { "aaa", "bbb", "ccc" };
//      Expect.Call(persistenceStorage_.AvailableProfiles).Return(
//        new ReadOnlyProfilesCollection(
//          testProfiles));
//      mocks_.ReplayAll();

//      ReadOnlyProfilesCollection availableProfiles = pps.AvailableProfiles;
//      Assert.AreEqual(testProfiles.Count, availableProfiles.Count);
//      for (int i = 0; i < testProfiles.Count; i++)
//      {
//        Assert.AreEqual(testProfiles[i], availableProfiles[i]);
//      }
//      mocks_.VerifyAll();
//    }

//    [TestMethod]
//    public void TestCurrentProfileAfterLoadEmpty()
//    {
//      MockStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);
//      bool eventCalled = false;
//      pps.CurrentProfileChanged += delegate
//                                     {
//                                       eventCalled = true;
//                                     };
//      pps.LoadProfile("aaa");
      
//      Assert.AreEqual(1, persistenceStorage.Calls.Count);
//      Assert.AreEqual("LoadState(aaa)", persistenceStorage.Calls[0]);
//      Assert.AreEqual("aaa", pps.CurrentProfile);
//      Assert.IsTrue(eventCalled);
//    }

//    [TestMethod]
//    public void TestCurrentProfileAfterLoadWithPartialStorageEmpty()
//    {
//      MockPartialStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);
//      bool eventCalled = false;
//      pps.CurrentProfileChanged += delegate
//                                     {
//                                       eventCalled = true;
//                                     };
//      pps.LoadProfile("aaa");
      
//      Assert.AreEqual(1, persistenceStorage.Calls.Count);
//      Assert.AreEqual("LoadState(aaa,out,out)", persistenceStorage.Calls[0]);
//      Assert.AreEqual("aaa", pps.CurrentProfile);
//      Assert.IsTrue(eventCalled);
//    }


//    [TestMethod]
//    public void TestCurrentProfileAfterLoad()
//    {
//      MockStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);
//      persistenceStorage.ToLoad = XDocument.Parse(
//        "<State>" +
//        "  <Item Id=\"aaa\">" +
//        "    <Content>" +
//        "      <AAA>" +
//        "        <BBB Attr=\"attr1\"/>" +
//        "      </AAA>" +
//        "    </Content>" +
//        "    <Subitems>" +
//        "      <Subitem Id=\"Id1\" XPath=\"AAA[1]/BBB[1]\" />" +
//        "    </Subitems>" +
//        "  </Item>" +
//        "</State>"
//        );

//      bool eventCalled = false;
//      pps.CurrentProfileChanged += delegate
//                                     {
//                                       eventCalled = true;
//                                     };
//      pps.LoadProfile("aaa");
      
//      Assert.AreEqual(1, persistenceStorage.Calls.Count);
//      Assert.AreEqual("LoadState(aaa)", persistenceStorage.Calls[0]);
//      Assert.AreEqual("aaa", pps.CurrentProfile);
//      Assert.IsTrue(eventCalled);
//      Assert.IsTrue(eventCalled);
//    }

//    [TestMethod]
//    public void TestCurrentProfileAfterLoadWithPartialStorage()
//    {
//      MockPartialStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);
//      persistenceStorage.ItemsToLoad = new Dictionary<string, XElement>
//      {{"ItemID1",XElement.Parse(       
//        "  <Item Id=\"ItemID1\">" +
//        "    <Content>" +
//        "      <AAA>" +
//        "        <BBB Attr=\"attr1\"/>" +
//        "      </AAA>" +
//        "    </Content>" +
//        "  </Item>"
//        )},
//        {"ItemID2",XElement.Parse( 
//        "  <Item Id=\"ItemID2\">" + 
//        "    <Content2>" +
//        "      <DDD>" +
//        "        <CCC Attr=\"attr1\"/>" +
//        "      </DDD>" +
//        "    </Content2>" +
//        "  </Item>"
//        )}
//      };      

//      bool eventCalled = false;
//      pps.CurrentProfileChanged += delegate
//      {
//        eventCalled = true;
//      };
//      pps.LoadProfile("aaa");
      
//      Assert.AreEqual(1, persistenceStorage.Calls.Count);
//      Assert.AreEqual("LoadState(aaa,out,out)", persistenceStorage.Calls[0]);
//      Assert.AreEqual("aaa", pps.CurrentProfile);
//      Assert.IsTrue(eventCalled);
//    }


//    [TestMethod]
//    public void TestCurrentProfileAfterLoadWithPartialStorageWithSubitems()
//    {
//      MockPartialStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);
//      persistenceStorage.ItemsToLoad = new Dictionary<string, XElement>
//      {{"ItemID1",XElement.Parse(       
//        "<Item Id=\"aaa\">" +
//        "    <Content>" +
//        "      <AAA>" +
//        "        <BBB Attr=\"attr1\"/>" +
//        "      </AAA>" +
//        "    </Content>" +
//        "    <Subitems>" +
//        "      <Subitem Id=\"Id1\" XPath=\"AAA[1]/BBB[1]\" />" +
//        "    </Subitems>" +
//        "  </Item>"
//        )},
//        {"ItemID2",XElement.Parse( 
//        "  <Item Id=\"ItemID2\">" + 
//        "    <Content2>" +
//        "      <DDD>" +
//        "        <CCC/>" +
//        "      </DDD>" +
//        "    </Content2>" +
//        "  </Item>"
//        )}
//      };

//      persistenceStorage.SubItemsToLoad = new Dictionary<string, XElement>
//                                            {
//                                              {"Id1",XElement.Parse("<BBB Attr=\"attr1\"/>")}
//                                            };

//      bool eventCalled = false;
//      pps.CurrentProfileChanged += delegate
//      {
//        eventCalled = true;
//      };
//      pps.LoadProfile("aaa");
      
//      Assert.AreEqual(1, persistenceStorage.Calls.Count);
//      Assert.AreEqual("LoadState(aaa,out,out)", persistenceStorage.Calls[0]);
//      Assert.AreEqual("aaa", pps.CurrentProfile);
//      Assert.IsTrue(eventCalled);
//    }

//    [TestMethod]
//    public void TestCurrentProfileAfterLoadWithPartialStorageWithSubitemsWithExtraUnexpectedSubitem()
//    {
//      //ignoring the unexpected item
//      MockPartialStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);
//      persistenceStorage.ItemsToLoad = new Dictionary<string, XElement>
//      {{"ItemID1",XElementWithSubitems.Parse(       
//        "<Item Id=\"aaa\">" +
//        "    <Content>" +
//        "      <AAA>" +
//        "        <BBB Attr=\"attr1\"/>" +
//        "      </AAA>" +
//        "    </Content>" +
//        "    <Subitems>" +
//        "      <Subitem Id=\"Id1\" XPath=\"AAA[1]/BBB[1]\" />" +
//        "    </Subitems>" +
//        "  </Item>"
//        )},
//        {"ItemID2",XElement.Parse( 
//        "  <Item Id=\"ItemID2\">" + 
//        "    <Content2>" +
//        "      <DDD>" +
//        "        <CCC/>" +
//        "      </DDD>" +
//        "    </Content2>" +
//        "  </Item>"
//        )}
//      };

//      persistenceStorage.SubItemsToLoad = new Dictionary<string, XElement>
//                                            {
//                                              {"Id1",XElement.Parse("<BBB Attr=\"attr1\"/>")},
//                                              {"Id2",XElement.Parse("<BBB Attr=\"attr1\"/>")}
//                                            };

//      bool eventCalled = false;
//      pps.CurrentProfileChanged += delegate
//      {
//        eventCalled = true;
//      };
//      pps.LoadProfile("aaa");
      
//      Assert.AreEqual(1, persistenceStorage.Calls.Count);
//      Assert.AreEqual("LoadState(aaa,out,out)", persistenceStorage.Calls[0]);
//      Assert.AreEqual("aaa", pps.CurrentProfile);
//      Assert.IsTrue(eventCalled);
//    }

//    [TestMethod]
//    public void TestSaveForNotSetCurrentProfile()
//    {
//      MockStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);
//      pps.SaveCurrentProfile();
      
//      Assert.AreEqual(0, persistenceStorage.Calls.Count);
//    }

//    [TestMethod]
//    public void TestSaveNothingForCurrentProfile()
//    {
//      MockStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);

//      persistenceStorage.ToLoad = XDocument.Parse(
//       "<State>" +
//       "  <Item Id=\"aaa\">" +
//       "    <Content>" +
//       "      <AAA>" +
//       "        <BBB Attr=\"attr1\"/>" +
//       "      </AAA>" +
//       "    </Content>" +
//       "    <Subitems>" +
//       "      <Subitem Id=\"Id1\" XPath=\"AAA[1]/BBB[1]\" />" +
//       "    </Subitems>" +
//       "  </Item>" +
//       "</State>"
//       );

//      bool eventCalled = false;
//      pps.CurrentProfileChanged += delegate
//      {
//        eventCalled = true;
//      };

//      pps.LoadProfile("Default");
      
//      pps.SaveCurrentProfile();
      
//      Assert.AreEqual(2, persistenceStorage.Calls.Count);
//      Assert.AreEqual("LoadState(Default)", persistenceStorage.Calls[0]);
//      Assert.AreEqual("SaveState(<State />,Default)", persistenceStorage.Calls[1]);
//    }
    
//    [TestMethod]
//    public void TestSaveForCurrentProfile()
//    {
//      MockStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);

//      persistenceStorage.ToLoad = XDocument.Parse(
//       "<State>" +
//       "  <Item Id=\"aaa\">" +
//       "    <Content>" +
//       "      <AAA>" +
//       "        <BBB Attr=\"attr1\"/>" +
//       "      </AAA>" +
//       "    </Content>" +
//       "    <Subitems>" +
//       "      <Subitem Id=\"Id1\" XPath=\"AAA[1]/BBB[1]\" />" +
//       "    </Subitems>" +
//       "  </Item>" +
//       "</State>"
//       );

//      string expectedSaved =
//        "<State>\r\n" +
//       "  <Item Id=\"Item111\">\r\n" +
//       "    <SavedContent />\r\n" +
//       "  </Item>\r\n" +
//       "</State>";

//      bool eventCalled = false;
//      pps.CurrentProfileChanged += delegate
//      {
//        eventCalled = true;
//      };

//      persistenceService.AddPersistor("Item111",
//                                      (XDocument state_) => { },
//                                      () => new XDocument(new XElement("SavedContent")));

//      pps.LoadProfile("Default");
      
//      pps.SaveCurrentProfile();
      
//      Assert.AreEqual(2, persistenceStorage.Calls.Count);
//      Assert.AreEqual("LoadState(Default)", persistenceStorage.Calls[0]);
//      Assert.AreEqual("SaveState(" + expectedSaved + ",Default)", persistenceStorage.Calls[1]);
//    }

//    [TestMethod]
//    public void TestSaveProfileWithPartialStorage()
//    {
//      MockPartialStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);

//      string expectedSaved =
//        "<State>\r\n" +
//       "  <Item Id=\"Item111\">\r\n" +
//       "    <SavedContent />\r\n" +
//       "  </Item>\r\n" +
//       "</State>";

//      persistenceService.AddPersistor("Item111",
//                                      (XDocument state_) => { },
//                                      () => new XDocument(new XElement("SavedContent")));
      
//      pps.SaveCurrentProfileAs("Test");
      
//      Assert.AreEqual(1, persistenceStorage.Calls.Count);      
//      Assert.AreEqual("SaveState(Test,1,0)", persistenceStorage.Calls[0]);
//    }

//    [TestMethod]
//    public void TestSaveForProfile()
//    {
//      MockStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);

//      string expectedSaved =
//        "<State>\r\n" +
//       "  <Item Id=\"Item111\">\r\n" +
//       "    <SavedContent />\r\n" +
//       "  </Item>\r\n" +
//       "</State>";

//      persistenceService.AddPersistor("Item111",
//                                      (XDocument state_) => { },
//                                      () => new XDocument(new XElement("SavedContent")));

//      pps.SaveCurrentProfileAs("Test");
      
//      Assert.AreEqual(1, persistenceStorage.Calls.Count);
//      Assert.AreEqual("SaveState(" + expectedSaved + ",Test)", persistenceStorage.Calls[0]);
//    }

//    [TestMethod]
//    public void TestDeleteProfile()
//    {
//      MockStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);

//      persistenceStorage.ToLoad = XDocument.Parse(
//       "<State>" +
//       "  <Item Id=\"aaa\">" +
//       "    <Content>" +
//       "      <AAA>" +
//       "        <BBB Attr=\"attr1\"/>" +
//       "      </AAA>" +
//       "    </Content>" +
//       "    <Subitems>" +
//       "      <Subitem Id=\"Id1\" XPath=\"AAA[1]/BBB[1]\" />" +
//       "    </Subitems>" +
//       "  </Item>" +
//       "</State>"
//       );      

//      int eventCalledCounter = 0;
//      pps.CurrentProfileChanged += delegate
//      {
//        eventCalledCounter++;
//      };      
//      pps.LoadProfile("Default");
      
//      Assert.AreEqual(1, eventCalledCounter);
//      Assert.AreEqual("Default", pps.CurrentProfile);      

//      pps.DeleteProfile("Default");
      
//      Assert.AreEqual(2, eventCalledCounter);
//      Assert.AreEqual(null, pps.CurrentProfile);      
      
//      Assert.AreEqual(2, persistenceStorage.Calls.Count);
//      Assert.AreEqual("LoadState(Default)", persistenceStorage.Calls[0]);
//      Assert.AreEqual("DeleteState(Default)", persistenceStorage.Calls[1]);
//    }

//    [TestMethod]
//    public void TestRenameProfile()
//    {
//      MockStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);

//      persistenceStorage.ToLoad = XDocument.Parse(
//       "<State>" +
//       "  <Item Id=\"aaa\">" +
//       "    <Content>" +
//       "      <AAA>" +
//       "        <BBB Attr=\"attr1\"/>" +
//       "      </AAA>" +
//       "    </Content>" +
//       "    <Subitems>" +
//       "      <Subitem Id=\"Id1\" XPath=\"AAA[1]/BBB[1]\" />" +
//       "    </Subitems>" +
//       "  </Item>" +
//       "</State>"
//       );

//      int eventCalledCounter = 0;
//      pps.CurrentProfileChanged += delegate
//      {
//        eventCalledCounter++;
//      };
//      pps.LoadProfile("Default");
      
//      Assert.AreEqual(1, eventCalledCounter);
//      Assert.AreEqual("Default", pps.CurrentProfile);      

//      pps.RenameProfile("Default", "NewName");
      
//      Assert.AreEqual(2, eventCalledCounter);
//      Assert.AreEqual("NewName", pps.CurrentProfile);      

//      Assert.AreEqual(2, persistenceStorage.Calls.Count);
//      Assert.AreEqual("LoadState(Default)", persistenceStorage.Calls[0]);
//      Assert.AreEqual("RenameState(Default,NewName)", persistenceStorage.Calls[1]);
//    }

//    [TestMethod]
//    public void TestCheckIsUnchangedWhenUnchanged()
//    {
//      MockStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);
//      string expectedSaved =
//        "<State>\r\n" +
//       "  <Item Id=\"Item111\">\r\n" +
//       "    <SavedContent />\r\n" +
//       "  </Item>\r\n" +
//       "</State>";

//      int itemSaveCallsCounter = 0;
//      persistenceService.AddPersistor("Item111",
//                                      (XDocument state_) => { },
//                                      () =>
//                                      {
//                                        itemSaveCallsCounter++;
//                                        return new XDocument(new XElement("SavedContent")); 
//                                      });

//      pps.SaveCurrentProfileAs("Test");
      

//      Dictionary<string, XElement> dict;
//      Assert.IsTrue(pps.CheckIsUnchanged(out dict));

//      Assert.AreEqual(2, itemSaveCallsCounter);
//      Assert.AreEqual(1, persistenceStorage.Calls.Count);
//      Assert.AreEqual("SaveState(" + expectedSaved + ",Test)", persistenceStorage.Calls[0]);
//    }

//    [TestMethod]
//    public void TestCheckIsUnchangedWhenChanged()
//    {
//      MockStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);
//      string expectedSaved =
//        "<State>\r\n" +
//       "  <Item Id=\"Item111\">\r\n" +
//       "    <SavedContent1 />\r\n" +
//       "  </Item>\r\n" +
//       "</State>";

//      int modifier = 1;
//      int itemSaveCallsCounter = 0;
//      persistenceService.AddPersistor("Item111",
//                                      (XDocument state_) => { },
//                                      () =>
//                                        {
//                                          itemSaveCallsCounter++;
//                                          return new XDocument(new XElement("SavedContent" + modifier));
//                                        });

//      pps.SaveCurrentProfileAs("Test");
      
//      modifier++;
//      Dictionary<string, XElement> dict;
//      Assert.IsFalse(pps.CheckIsUnchanged(out dict));

//      Assert.AreEqual(2, itemSaveCallsCounter);
//      Assert.AreEqual(1, persistenceStorage.Calls.Count);
//      Assert.AreEqual("SaveState(" + expectedSaved + ",Test)", persistenceStorage.Calls[0]);
//    }

//    [TestMethod]
//    public void TestSaveItemsDict()
//    {
//      MockStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);
//      string expectedSaved =
//        "<State>\r\n" +
//       "  <Item Id=\"Item111\">\r\n" +
//       "    <SavedContent />\r\n" +
//       "  </Item>\r\n" +
//       "</State>";

//      persistenceService.AddPersistor("Item111",
//                                      (XDocument state_) => { },
//                                      () => new XDocument(new XElement("SavedContent")));
//      pps.SaveCurrentProfileAs("Test");
      
//      Assert.AreEqual("Test", pps.CurrentProfile);
//      pps.SaveItemsDictionary(new Dictionary<string, XElement>
//        {{"AnotherItem", XElement.Parse(
//           "  <Item Id=\"Item222\">\r\n" +
//           "    <AnotherSavedContent />\r\n" +
//           "  </Item>\r\n")}});
      
//      Assert.AreEqual("Test", pps.CurrentProfile);
//      Assert.AreEqual(2, persistenceStorage.Calls.Count);
//    }

//    [TestMethod]
//    public void TestSaveItemsDictAs()
//    {
//      MockStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);
//      string expectedSaved =
//        "<State>\r\n" +
//       "  <Item Id=\"Item111\">\r\n" +
//       "    <SavedContent />\r\n" +
//       "  </Item>\r\n" +
//       "</State>";

//      persistenceService.AddPersistor("Item111",
//                                      (XDocument state_) => { },
//                                      () => new XDocument(new XElement("SavedContent")));
//      pps.SaveCurrentProfileAs("Test");
      
//      Assert.AreEqual("Test", pps.CurrentProfile);
//      pps.SaveItemsDictionaryAs(new Dictionary<string, XElement>
//        {{"AnotherItem", XElement.Parse(
//           "  <Item Id=\"Item222\">\r\n" +
//           "    <AnotherSavedContent />\r\n" +
//           "  </Item>\r\n")}}, "Test2");
      
//      Assert.AreEqual("Test", pps.CurrentProfile); //Current profile unchanged!
//      Assert.AreEqual(2, persistenceStorage.Calls.Count); 
//    }

//    [TestMethod]
//    public void TestSaveItemsDictAsWithPartialStorage()
//    {
//      MockPartialStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);

//      string expectedSaved =
//        "<State>\r\n" +
//       "  <Item Id=\"Item111\">\r\n" +
//       "    <SavedContent />\r\n" +
//       "  </Item>\r\n" +
//       "</State>";

//      persistenceService.AddPersistor("Item111",
//                                      (XDocument state_) => { },
//                                      () => new XDocument(new XElement("SavedContent")));
//      pps.SaveCurrentProfileAs("Test");
      
//      Assert.AreEqual("Test", pps.CurrentProfile);
//      pps.SaveItemsDictionaryAs(new Dictionary<string, XElement>
//        {{"AnotherItem", XElement.Parse(
//           "  <Item Id=\"Item222\">\r\n" +
//           "    <AnotherSavedContent />\r\n" +
//           "  </Item>\r\n")}}, "Test2");
      
//      Assert.AreEqual("Test", pps.CurrentProfile); //Current profile unchanged!
//      Assert.AreEqual(2, persistenceStorage.Calls.Count);
//    }

//    [TestMethod]
//    public void TestAddSubItemToExistingItem()
//    {
//      MockStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);
//      pps.StorageAccessorRunner.Async = false;

//      persistenceStorage.ToLoad = XDocument.Parse(
//        "<State>" +
//        "  <Item Id=\"aaa\">" +
//        "    <Content>" +
//        "      <AAA>" +
//        "        <BBB Attr=\"attr1\" />" +
//        "      </AAA>" +
//        "    </Content>" +
//        "    <Subitems>" +
//        "      <Subitem Id=\"Id1\" XPath=\"AAA[1]/BBB[1]\" />" +
//        "    </Subitems>" +
//        "  </Item>" +
//        "</State>"
//        );

//      string expectedSaved =
//        "<State>\r\n" +
//        "  <Item Id=\"aaa\">\r\n" +
//        "    <Content>\r\n" +
//        "      <AAA>\r\n" +
//        "        <BBB Attr=\"attr1\" />\r\n" +
//        "      </AAA>\r\n" +
//        "      <View>\r\n" +
//        "        <ViewInfo />\r\n" +
//        "      </View>\r\n" +
//        "    </Content>\r\n" +
//        "    <Subitems>\r\n" +
//        "      <Subitem Id=\"Id1\" XPath=\"AAA[1]/BBB[1]\" />\r\n" +
//        "      <Subitem Id=\"NewSubItemId\" XPath=\"View[1]\" />\r\n" +
//        "    </Subitems>\r\n" +
//        "  </Item>\r\n" +
//        "</State>";

//      int eventCalledCounter = 0;
//      pps.CurrentProfileChanged += delegate
//                                     {
//                                       eventCalledCounter++;
//                                     };
//      pps.LoadProfile("Default");
      
//      Assert.AreEqual(1, eventCalledCounter);
//      Assert.AreEqual("Default", pps.CurrentProfile);

//      pps.SaveSubitem(
//        "aaa",
//        "NewSubItemId",
//        new XElement("View", new XElement("ViewInfo")),
//        (XElementWithSubitems arg1_, XElement arg2_) =>
//          {
//            arg1_.Add(arg2_);
//          },
//        new XElement("CleanItem"));
      
//      Assert.AreEqual(2, persistenceStorage.Calls.Count);
//      Assert.AreEqual("LoadState(Default)", persistenceStorage.Calls[0]);
//      Assert.AreEqual("SaveState(" + expectedSaved + ",Default)", persistenceStorage.Calls[1]);
//    }

//    [TestMethod]
//    public void TestAddSubItemToNewItem()
//    {
//      MockStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);

//      persistenceStorage.ToLoad = XDocument.Parse(
//       "<State>" +
//       "  <Item Id=\"aaa\">" +
//       "    <Content>" +
//       "      <AAA>" +
//       "        <BBB Attr=\"attr1\" />" +
//       "      </AAA>" +
//       "    </Content>" +
//       "    <Subitems>" +
//       "      <Subitem Id=\"Id1\" XPath=\"AAA[1]/BBB[1]\" />" +
//       "    </Subitems>" +
//       "  </Item>" +
//       "</State>"
//       );

//      string expectedSaved =
//        "<State>\r\n" +
//        "  <Item Id=\"aaa\">\r\n" +
//        "    <Content>\r\n" +
//        "      <AAA>\r\n" +
//        "        <BBB Attr=\"attr1\" />\r\n" +
//        "      </AAA>\r\n" +        
//        "    </Content>\r\n" +
//        "    <Subitems>\r\n" +
//        "      <Subitem Id=\"Id1\" XPath=\"AAA[1]/BBB[1]\" />\r\n" +
//        "    </Subitems>\r\n" +
//        "  </Item>\r\n" +
//        "  <Item Id=\"NewItemId\">\r\n" +
//        "    <CleanItem>\r\n" +
//        "      <View>\r\n" +
//        "        <ViewInfo />\r\n" +
//        "      </View>\r\n" +
//        "    </CleanItem>\r\n" +
//        "    <Subitems>\r\n" +
//        "      <Subitem Id=\"NewSubItemId\" XPath=\"View[1]\" />\r\n" +
//        "    </Subitems>\r\n" +
//        "  </Item>\r\n" +
//        "</State>";

//      int eventCalledCounter = 0;
//      pps.CurrentProfileChanged += delegate
//      {
//        eventCalledCounter++;
//      };
//      pps.LoadProfile("Default");
      
//      Assert.AreEqual(1, eventCalledCounter);
//      Assert.AreEqual("Default", pps.CurrentProfile);

//      pps.SaveSubitem(
//        "NewItemId",
//        "NewSubItemId",
//        new XElement("View", new XElement("ViewInfo")),
//        (XElementWithSubitems arg1_, XElement arg2_) =>
//        {
//          arg1_.Add(arg2_);
//        },
//        new XElement("CleanItem"));
      
//      Assert.AreEqual(2, persistenceStorage.Calls.Count);
//      Assert.AreEqual("LoadState(Default)", persistenceStorage.Calls[0]);
//      Assert.AreEqual("SaveState(" + expectedSaved + ",Default)", persistenceStorage.Calls[1]);
//    }

//    [TestMethod]
//    public void TestSaveCurrentProfileForKeys()
//    {
//      MockStorage persistenceStorage;
//      PersistenceService persistenceService;
//      PersistenceProfileService pps = CreateSimpleInstance(out persistenceStorage, out persistenceService);

//      persistenceStorage.ToLoad = XDocument.Parse(
//       "<State>" +
//       "  <Item Id=\"aaa\">" +
//       "    <Content>" +
//       "      <AAA>" +
//       "        <BBB Attr=\"attr1\" />" +
//       "      </AAA>" +
//       "    </Content>" +
//       "    <Subitems>" +
//       "      <Subitem Id=\"Id1\" XPath=\"AAA[1]/BBB[1]\" />" +
//       "    </Subitems>" +
//       "  </Item>" +
//       "</State>");

//      string expectedSaved =
//       "<State>\r\n" +
//       "  <Item Id=\"aaa\">\r\n" +
//       "    <Content>\r\n" +
//       "      <AAA>\r\n" +
//       "        <BBB Attr=\"attr1\" />\r\n" +
//       "      </AAA>\r\n" +
//       "    </Content>\r\n" +
//       "    <Subitems>\r\n" +
//       "      <Subitem Id=\"Id1\" XPath=\"AAA[1]/BBB[1]\" />\r\n" +
//       "    </Subitems>\r\n" +
//       "  </Item>\r\n" +
//       "  <Item Id=\"Item111\">\r\n" +
//       "    <SavedContent />\r\n" +
//       "  </Item>\r\n" +
//       "</State>";

//      pps.LoadProfile("Test");
      
//      persistenceService.AddPersistor("Item111",
//                                     (XDocument state_) => { },
//                                     () => new XDocument(new XElement("SavedContent")));


//      pps.SaveCurrentProfileForKeys(new[] {"Item111"});
      

//      Assert.AreEqual(2, persistenceStorage.Calls.Count);
//      Assert.AreEqual("LoadState(Test)", persistenceStorage.Calls[0]);
//      Assert.AreEqual("SaveState(" + expectedSaved + ",Test)", persistenceStorage.Calls[1]);
//    }
    

//    [TestMethod]
//    public void TestBrokenStorage()
//    {
//      MockBrokenStorage persistenceStorage = new MockBrokenStorage();
//      PersistenceService persistenceService = new PersistenceService();
//      IStatusBar statusBar = new MockStatusBar();
//      PersistenceProfileService pps = new PersistenceProfileService(persistenceStorage, persistenceService,
//                                                                    statusBar);
//      //pps.StorageAccessorRunner.Async = false;

//      persistenceService.AddPersistor("Item111",
//                                      (XDocument state_) => { },
//                                      () => new XDocument(new XElement("SavedContent")));

//      var profile = pps.CurrentProfile;
//      pps.SaveCurrentProfile();
//      pps.SaveCurrentProfileAs("NewName");
//      pps.DeleteProfile("NewName");
//      pps.RenameProfile("NewName", "NewName2");
//      pps.LoadProfile("NewName2");
      
//      //no exceptions. only log messages
//    }


//    [TestMethod]
//    public void TestNullStorage()
//    {
//      MockBrokenStorage persistenceStorage = null;
//      PersistenceService persistenceService = new PersistenceService();
//      IStatusBar statusBar = new MockStatusBar();
//      PersistenceProfileService pps = new PersistenceProfileService(persistenceStorage, persistenceService,
//                                                                    statusBar);
//      pps.StorageAccessorRunner.Async = false;

//      persistenceService.AddPersistor("Item111",
//                                      (XDocument state_) => { },
//                                      () => new XDocument(new XElement("SavedContent")));

//      var profile = pps.CurrentProfile;
//      pps.SaveCurrentProfile();
//      pps.SaveCurrentProfileAs("NewName");
//      pps.DeleteProfile("NewName");
//      pps.RenameProfile("NewName", "NewName2");
//      pps.LoadProfile("NewName2");
      
//      //no exceptions
//    }

//    [TestMethod]
//    [ExpectedException(ExpectedMessage = "Store error")]
//    public void TestBrokenPersistorSyncSave()
//    {
//      MockStorage persistenceStorage = new MockStorage();
//      PersistenceService persistenceService = new PersistenceService();
//      IStatusBar statusBar = new MockStatusBar();
//      PersistenceProfileService pps = new PersistenceProfileService(persistenceStorage, persistenceService,
//                                                                    statusBar);
//      pps.StorageAccessorRunner.Async = false;

//      persistenceService.AddPersistor("Item111",
//                                      state_ => { throw new Exception("Restore error"); },
//                                      () => { throw new Exception("Store error"); });

//      pps.SaveCurrentProfileAs("NewName");      
//    }

//    [TestMethod]
//    [ExpectedException(ExpectedMessage = "Error caught")]
//    public void TestBrokenPersistorSyncLoad()
//    {
//      MockStorage persistenceStorage = new MockStorage();
//      PersistenceService persistenceService = new PersistenceService();
//      IStatusBar statusBar = new MockStatusBar();
//      PersistenceProfileService pps = new PersistenceProfileService(persistenceStorage, persistenceService,
//                                                                    statusBar);
//      pps.StorageAccessorRunner.Async = false;

//      pps.StorageAccessorRunner.ExecutionExceptionHappened +=
//        delegate
//        {
//          throw new Exception("Error caught");
//        };

//      persistenceService.AddPersistor("Item111",
//                                      state_ => { throw new Exception("Restore error"); },
//                                      () => { throw new Exception("Store error"); });
//      persistenceStorage.ToLoad = XDocument.Parse(
//      "<State>" +
//      "  <Item Id=\"Item111\">" +
//      "    <Content>" +
//      "      <AAA>" +
//      "        <BBB Attr=\"attr1\" />" +
//      "      </AAA>" +
//      "    </Content>" +
//      "    <Subitems>" +
//      "      <Subitem Id=\"Id1\" XPath=\"AAA[1]/BBB[1]\" />" +
//      "    </Subitems>" +
//      "  </Item>" +
//      "</State>");
//      pps.LoadProfile("NewName");
      
//    }

//    private PersistenceProfileService CreateSimpleInstance(out MockStorage persistenceStorage_, out PersistenceService persistenceService_)
//    {
//      persistenceStorage_ = new MockStorage();
//      persistenceService_ = new PersistenceService();
//      IStatusBar statusBar = new MockStatusBar();
//      var result = new PersistenceProfileService(persistenceStorage_, persistenceService_, statusBar);
//      result.StorageAccessorRunner.Async = false;
//      return result;
//    }

//    private PersistenceProfileService CreateSimpleInstance(out MockPartialStorage persistenceStorage_, out PersistenceService persistenceService_)
//    {
//      persistenceStorage_ = new MockPartialStorage();
//      persistenceService_ = new PersistenceService();
//      IStatusBar statusBar = new MockStatusBar();
//      var result = new PersistenceProfileService(persistenceStorage_, persistenceService_, statusBar);
//      result.StorageAccessorRunner.Async = false;
//      return result;
//    }
//  }

//  public class MockStorage : IPersistenceStorage
//  {
//    List<string> m_calls = new List<string>();
//    public List<string> Calls
//    { 
//      get
//      {
//        return m_calls;
//      }
//    }

//    public XDocument ToLoad
//    {
//      get; set;
//    }

//    public ProfilesCollection Available
//    {
//      get;
//      set;
//    }

//    public void SaveState(XDocument state_, string profile_)
//    {
//      Calls.Add(string.Format("SaveState({0},{1})", state_, profile_));
//    }

//    public XDocument LoadState(string profile_)
//    {
//      Calls.Add(string.Format("LoadState({0})", profile_));
//      return ToLoad;
//    }

//    public void DeleteState(string profile_)
//    {
//      Calls.Add(string.Format("DeleteState({0})", profile_));
//    }

//    public void RenameState(string oldName_, string newName_)
//    {
//      Calls.Add(string.Format("RenameState({0},{1})", oldName_, newName_));
//    }

//    public ReadOnlyProfilesCollection AvailableProfiles
//    {
//      get { return new ReadOnlyProfilesCollection(Available); }
//    }
//  }

//  public class MockPartialStorage : IPartialPersistenceStorage
//  {
//    List<string> m_calls = new List<string>();
//    public List<string> Calls
//    {
//      get
//      {
//        return m_calls;
//      }
//    }

//    public Dictionary<string, XElement> ItemsToLoad
//    {
//      get;
//      set;
//    }

//    public Dictionary<string, XElement> SubItemsToLoad
//    {
//      get;
//      set;
//    }


//    public ProfilesCollection Available
//    {
//      get;
//      set;
//    }

//    public void SaveState(XDocument state_, string profile_)
//    {
//      throw new NotImplementedException();
//    }

//    public XDocument LoadState(string profile_)
//    {
//      throw new NotImplementedException();
//    }

//    public void DeleteState(string profile_)
//    {
//      Calls.Add(string.Format("DeleteState({0})", profile_));
//    }

//    public void RenameState(string oldName_, string newName_)
//    {
//      Calls.Add(string.Format("RenameState({0},{1})", oldName_, newName_));
//    }

//    public ReadOnlyProfilesCollection AvailableProfiles
//    {
//      get { return new ReadOnlyProfilesCollection(Available); }
//    }

//    public void SaveState(
//      string profile_, Dictionary<string, XElement> items_, Dictionary<string, XElement> subitems_, bool clean_)
//    {
//      Calls.Add(string.Format("SaveState({0},{1},{2})", profile_, items_.Count, subitems_.Count));
//    }

//    public void LoadState(
//      string profile_, out Dictionary<string, XElement> items_, out Dictionary<string, XElement> subitems_)
//    {
//      Calls.Add(string.Format("LoadState({0},out,out)", profile_));
//      items_ = ItemsToLoad;
//      subitems_ = SubItemsToLoad;
//    }
//  }

//  public class MockBrokenStorage : IPersistenceStorage
//  {
//    public void SaveState(XDocument state_, string profile_)
//    {
//      throw new NotImplementedException();
//    }

//    public XDocument LoadState(string profile_)
//    {
//      throw new NotImplementedException();
//    }

//    public void DeleteState(string profile_)
//    {
//      throw new NotImplementedException();
//    }

//    public void RenameState(string oldName_, string newName_)
//    {
//      throw new NotImplementedException();
//    }

//    public ReadOnlyProfilesCollection AvailableProfiles
//    {
//      get { throw new NotImplementedException(); }
//    }
//  }

//  public class MockStatusBar : IStatusBar
//  {    
//    public ICollection<string> Keys
//    {
//      get { throw new NotImplementedException(); }
//    }

//    public IStatusBarItem this[string statusBarItemKey_]
//    {
//      get { throw new NotImplementedException(); }
//    }

//    private IStatusBarItem m_mainItem = new MockStatusBarItem();
//    public IStatusBarItem MainItem
//    {
//      get { return m_mainItem; }
//    }

//    public void AddItem(string key_, IStatusBarItem item_, StatusBarItemAlignment alignment_)
//    {
//      throw new NotImplementedException();
//    }

//    public string AddItem(IStatusBarItem item_, StatusBarItemAlignment alignment_)
//    {
//      throw new NotImplementedException();
//    }

//    public void AddTextStatusItem(string key_, StatusBarItemAlignment alignment_)
//    {
//      throw new NotImplementedException();
//    }

//    public string AddTextStatusItem(StatusBarItemAlignment alignment_)
//    {
//      throw new NotImplementedException();
//    }

//    public void AddProgressBarStatusItem(string key_, double from_, double to_, StatusBarItemAlignment alignment_)
//    {
//      throw new NotImplementedException();
//    }

//    public string AddProgressBarStatusItem(double from_, double to_, StatusBarItemAlignment alignment_)
//    {
//      throw new NotImplementedException();
//    }

//    public void AddSeparator(string key_, StatusBarItemAlignment alignment_)
//    {
//      throw new NotImplementedException();
//    }

//    public string AddSeparator(StatusBarItemAlignment alignment_)
//    {
//      throw new NotImplementedException();
//    }

//    public void RemoveItem(string key_)
//    {
//      throw new NotImplementedException();
//    }

//    public bool ContainsItem(string key_)
//    {
//      throw new NotImplementedException();
//    }
//    private class MockStatusBarItem : IStatusBarItem
//    {
//      public void UpdateStatus(StatusLevel level_, string statusText_)
//      {
        
//      }

//      public void ClearStatus()
//      {
        
//      }

//      public object Control
//      {
//        get { throw new NotImplementedException(); }
//      }

//      public event StatusUpdatedEventHandler StatusUpdated;
//    }
//  }
// }
