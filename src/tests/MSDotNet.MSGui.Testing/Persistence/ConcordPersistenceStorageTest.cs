﻿using System.Xml;
using System.Xml.Linq;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSDotNet.MSGui.Testing.Persistence
{
  [TestClass]
  public class ConcordPersistenceStorageTest
  {
    [TestInitialize]
    [TestCleanup]
    public void Init()
    {
      MyConcordProvider.m_configs.Clear();
    }

    [TestMethod]
    public void ConstructorTest()
    {      
      ConcordPersistenceStorage cps = Create();
      Assert.IsNotNull(cps);
    }

    [TestMethod]
    public void TestSave()
    {
      XDocument docToSave = new XDocument(
        new XElement("SomeState", 
          new XAttribute("Attr", "attribute1"),
          new XElement("content")));

      ConcordPersistenceStorage cps = Create();
      cps.SaveState(docToSave, "Test");      
      XmlNode layouts = MyConcordProvider.m_configs["MSDesktopLayouts"];
      string layoutsString = layouts.OuterXml;
	  Assert.AreEqual("<MSDesktopLayouts><WindowManager type=\"MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.ConcordWindowManager, MSDotNet.MSGui.Impl\"><Layouts><Layout name=\"Test\"><SomeState Attr=\"attribute1\"><content /></SomeState></Layout></Layouts></WindowManager></MSDesktopLayouts>", layoutsString);
    }

    [TestMethod]
    public void TestLoad()
    {
      XmlDocument xmlDoc = new XmlDocument();
      xmlDoc.LoadXml(
		"<MSDesktopLayouts><WindowManager type=\"MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.ConcordWindowManager, MSDotNet.MSGui.Impl\">" +
        "<Layouts><Layout name=\"TestProfile\">"+
        "<SomeState Attr=\"attribute2\"><SomeContent /></SomeState>"+
        "</Layout></Layouts>" +
		"</WindowManager></MSDesktopLayouts>");

      const string expectedLoaded = "<SomeState Attr=\"attribute2\">\r\n" +
                                    "  <SomeContent />\r\n" +
                                    "</SomeState>";

	  MyConcordProvider.m_configs["MSDesktopLayouts"] = xmlDoc.FirstChild; //mimicking existing files with saved profile


      ConcordPersistenceStorage cps = Create();      
      XDocument loaded = cps.LoadState("TestProfile");
      Assert.IsNotNull(loaded);
      Assert.AreEqual(expectedLoaded, loaded.ToString());
    }

    [TestMethod]
    public void TestSaveAsExisting()
    {
      XmlDocument xmlDoc = new XmlDocument();
      xmlDoc.LoadXml(
        "<Layouts><WindowManager type=\"MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.ConcordWindowManager, MSDotNet.MSGui.Impl\">" +
        "<Layouts><Layout name=\"TestProfile\">" +
        "  <SomeState Attr=\"attribute2\"><SomeContent /></SomeState>" +
        "</Layout></Layouts>" +
        "</WindowManager></Layouts>");


	  MyConcordProvider.m_configs["MSDesktopLayouts"] = xmlDoc.FirstChild; //mimicking existing files with saved profile

      ConcordPersistenceStorage cps = Create();      

      XDocument docToSave = new XDocument(
        new XElement("SomeState",
          new XAttribute("Attr", "attribute1"),
          new XElement("content")));

      cps.SaveState(docToSave, "TestProfile");
      Assert.AreEqual(1, cps.AvailableProfiles.Count);
	  XmlNode layouts = MyConcordProvider.m_configs["MSDesktopLayouts"];
      string layoutsString = layouts.OuterXml;
	  Assert.AreEqual("<Layouts><WindowManager type=\"MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.ConcordWindowManager, MSDotNet.MSGui.Impl\"><Layouts><Layout name=\"TestProfile\"><SomeState Attr=\"attribute1\"><content /></SomeState></Layout></Layouts></WindowManager></Layouts>", layoutsString);

    }

    [TestMethod]
    public void TestDelete()
    {
      XDocument docToSave = new XDocument(
        new XElement("SomeState",
          new XAttribute("Attr", "attribute1"),
          new XElement("content")));

      ConcordPersistenceStorage cps = Create();

      Assert.AreEqual(0, cps.AvailableProfiles.Count);

      cps.SaveState(docToSave, "Test");

      Assert.AreEqual(1, cps.AvailableProfiles.Count);

      cps.DeleteState("Test");

      Assert.AreEqual(0, cps.AvailableProfiles.Count);

	  XmlNode layouts = MyConcordProvider.m_configs["MSDesktopLayouts"];
      string layoutsString = layouts.OuterXml;
	  Assert.AreEqual("<MSDesktopLayouts><WindowManager type=\"MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.ConcordWindowManager, MSDotNet.MSGui.Impl\"><Layouts></Layouts></WindowManager></MSDesktopLayouts>", layoutsString);
    }

    [TestMethod]
    public void TestRename()
    {
      XDocument docToSave = new XDocument(
        new XElement("SomeState",
          new XAttribute("Attr", "attribute1"),
          new XElement("content")));

      ConcordPersistenceStorage cps = Create();

      Assert.AreEqual(0, cps.AvailableProfiles.Count);

      cps.SaveState(docToSave, "Test");

      Assert.AreEqual(1, cps.AvailableProfiles.Count);

      cps.RenameState("Test", "NewName");

      Assert.AreEqual(1, cps.AvailableProfiles.Count);

	  XmlNode layouts = MyConcordProvider.m_configs["MSDesktopLayouts"];
      string layoutsString = layouts.OuterXml;
	  Assert.AreEqual("<MSDesktopLayouts><WindowManager type=\"MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.ConcordWindowManager, MSDotNet.MSGui.Impl\">" +
        "<Layouts><Layout name=\"NewName\">" +
        "<SomeState Attr=\"attribute1\"><content /></SomeState>"+
        "</Layout></Layouts>" +
		"</WindowManager></MSDesktopLayouts>", layoutsString);
    }

    private static ConcordPersistenceStorage Create()
    {
      ConcordPersistenceStorage cps = new ConcordPersistenceStorage(false);
      string profileString = "app=TestApplication;region=ln;env=prod;etscfg=Production";      
      ConfigurationManager.Reset();
      ConfigurationManager.Initialize(profileString, typeof(MyConcordProvider));
      cps.InitLayouts();
      return cps;
    }    
  }
}
