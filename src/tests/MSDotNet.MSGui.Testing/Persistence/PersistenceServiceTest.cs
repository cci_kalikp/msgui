﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Persistence/PersistenceServiceTest.cs#4 $
// $Change: 839595 $
// $DateTime: 2013/07/29 12:06:16 $
// $Author: milosp $

using System.Collections.Generic;
using System.Reflection;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace MSDotNet.MSGui.Testing.Persistence
{
  [TestClass]
  public class PersistenceServiceTest
  {
    [TestMethod]
    public void TestConstructor()
    {
      var ps = new PersistenceService();
      ps.AddPersistor("UniqueId1",
                       state_ => { },
                       () => new XDocument());
      Assert.IsNotNull(ps);      
    }

    [TestMethod]
    [ExpectedException(typeof(System.InvalidOperationException), "Key collision! Store&Restore functions are already generated for the key UniqueId")]
    public void TestKeyCollision()
    {
      PersistenceService ps = new PersistenceService();
      XElement elementToSave1 = new XElement("TestItem1", new XElement("aaaa"), new XAttribute("bbb", "ccc"));
      XElement elementToSave2 = new XElement("TestItem2", new XElement("aaaa"), new XAttribute("bbb", "ccc"));
      const string persistorId = "UniqueId";
      
      ps.AddPersistor(persistorId,
                       state_ => { },
                       () => new XDocument(elementToSave1));

      ps.AddPersistor(persistorId,
                       state_ => { },
                       () => new XDocument(elementToSave2));      
    }

    [TestMethod]
    public void TestSaveToDict()
    {
        SetEntryAsseblyProvider();
        var storage = new ConcordPersistenceStorage(true);
        storage.InitializeStorage();
        PersistenceService ps = new PersistenceService();
        XElement elementToSave = new XElement("TestItem1", new XElement("aaaa"), new XAttribute("bbb", "ccc"));
        const string persistorId = "UniqueId1";
        ps.AddPersistor(persistorId,
                        state_ => { },
                        () => new XDocument(elementToSave));

        Dictionary<string, XElement> saved = ps.SaveItemsToDict();

        Assert.AreEqual(1, saved.Count);
        Assert.AreEqual(elementToSave.ToString(), saved[persistorId].ToString());
    }

    [TestMethod]
    public void TestSaveMoreToDict()
    {
      PersistenceService ps = new PersistenceService();
      XElement elementToSave1 = new XElement("TestItem1", new XElement("aaaa"), new XAttribute("bbb", "ccc"));
      XElement elementToSave2 = new XElement("TestItem2", new XElement("aaaa"), new XAttribute("bbb", "ccc"));
      const string persistorId1 = "UniqueId1";
      const string persistorId2 = "UniqueId2";
      ps.AddPersistor(persistorId1,
                       state_ => { },
                       () => new XDocument(elementToSave1));

      ps.AddPersistor(persistorId2,
                       state_ => { },
                       () => new XDocument(elementToSave2));

      Dictionary<string, XElement> saved = ps.SaveItemsToDict();

      Assert.AreEqual(2, saved.Count);
      Assert.AreEqual(elementToSave1.ToString(), saved[persistorId1].ToString());
      Assert.AreEqual(elementToSave2.ToString(), saved[persistorId2].ToString());
    }

    [TestMethod]
    public void TestSaveKeyToDict()
    {
      PersistenceService ps = new PersistenceService();
      XElement elementToSave1 = new XElement("TestItem1", new XElement("aaaa"), new XAttribute("bbb", "ccc"));
      XElement elementToSave2 = new XElement("TestItem2", new XElement("aaaa"), new XAttribute("bbb", "ccc"));
      const string persistorId1 = "UniqueId1";
      const string persistorId2 = "UniqueId2";
      ps.AddPersistor(persistorId1,
                       state_ => { },
                       () => new XDocument(elementToSave1));

      ps.AddPersistor(persistorId2,
                       state_ => { },
                       () => new XDocument(elementToSave2));

      Dictionary<string, XElement> saved = ps.SaveItemsToDict(new[] {persistorId2});

      Assert.AreEqual(1, saved.Count);      
      Assert.AreEqual(elementToSave2.ToString(), saved[persistorId2].ToString());
    }

    [TestMethod]
    public void TestSaveRestore()
    {
      PersistenceService ps = new PersistenceService();
      XElement elementToSave = new XElement("TestItem1", new XElement("aaaa"), new XAttribute("bbb", "ccc"));
      XElement elementToRestore = new XElement("TestItem2", new XElement("smth_new"), new XAttribute("zzzz", "mmmm"));
      const string persistorId = "UniqueId1";
      int restoreCallCounter = 0;
      ps.AddPersistor(persistorId,
                       state_ =>
                         {
                           Assert.AreEqual(elementToRestore.ToString(), state_.ToString()); 
                           restoreCallCounter++;
                         },
                       () => new XDocument(elementToSave));

      Dictionary<string, XElement> saved = ps.SaveItemsToDict();
      Assert.AreEqual(1, saved.Count);
      Assert.AreEqual(elementToSave.ToString(), saved[persistorId].ToString());

      ps.RestoreItemsFromDict(new Dictionary<string, XElement> { { persistorId, elementToRestore } });
      Assert.AreEqual(1, restoreCallCounter);      
    }

    [TestMethod]
    public void TestSaveRestoreMore()
    {
      PersistenceService ps = new PersistenceService();
      XElement elementToSave1 = new XElement("TestItem1", new XElement("aaaa"), new XAttribute("bbb", "ccc"));
      XElement elementToSave2 = new XElement("TestItem2", new XElement("aaaa"), new XAttribute("bbb", "ccc"));
      XElement elementToRestore1 = new XElement("TestItem3", new XElement("smth_new"), new XAttribute("zzzz", "mmmm"));
      XElement elementToRestore2 = new XElement("TestItem3", new XElement("anything"), new XAttribute("zaaazzz", "mmmmaa"));
      const string persistorId1 = "UniqueId1";
      const string persistorId2 = "UniqueId2";
      int restoreCallCounter = 0;
      ps.AddPersistor(persistorId1,
                       state_ =>
                         {
                           Assert.AreEqual(elementToRestore1.ToString(), state_.ToString());
                           restoreCallCounter++;
                         },
                       () => new XDocument(elementToSave1));

      ps.AddPersistor(persistorId2,
                       state_ =>
                         {
                           Assert.AreEqual(elementToRestore2.ToString(), state_.ToString());
                           restoreCallCounter++;
                         },
                       () => new XDocument(elementToSave2));

      Dictionary<string, XElement> saved = ps.SaveItemsToDict();

      Assert.AreEqual(2, saved.Count);
      Assert.AreEqual(elementToSave1.ToString(), saved[persistorId1].ToString());
      Assert.AreEqual(elementToSave2.ToString(), saved[persistorId2].ToString());

      ps.RestoreItemsFromDict(new Dictionary<string, XElement> { { persistorId1, elementToRestore1 }, { persistorId2, elementToRestore2 } });
      Assert.AreEqual(2, restoreCallCounter);  
    }

    [TestMethod]
    public void TestRestoreNonexistingKey()
    {
      PersistenceService ps = new PersistenceService();
      ps.RestoreItemsFromDict(new Dictionary<string, XElement> { { "NonExistingkey", new XElement("aaa") } });
      //no exception because we could load a profile with some old persistor that does not exist anymore
    }

    [TestMethod]
    public void TestRestoreNullElement()
    {
        SetEntryAsseblyProvider();
        var storage = new ConcordPersistenceStorage(true);
        storage.InitializeStorage();
        PersistenceService ps = new PersistenceService();
        ps.RestoreItemsFromDict(new Dictionary<string, XElement> { { "NonExistingkey", null } });
        //todo
    }

      private static void SetEntryAsseblyProvider()
      {
          var stubbedAssemblyProvider = MockRepository.GenerateStub<IEntryAssemblyProvider>();
          stubbedAssemblyProvider.Stub(x => x.GetEntryAssembly()).Return(Assembly.GetExecutingAssembly());
          EntryAssemblyHelper.EntryAssemblyProvider = stubbedAssemblyProvider;
      }
  }
}
