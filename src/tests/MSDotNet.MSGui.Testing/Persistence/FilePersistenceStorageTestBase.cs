﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Persistence/FilePersistenceStorageTestBase.cs#3 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSDotNet.MSGui.Testing.Persistence
{  
  [TestClass] 
  abstract public class FilePersistenceStorageTestBase
  {
    public abstract string AppName
    {
      get;
    }

    public abstract string PresetLocation
    {
      get;
    }

    public abstract string BaseLocation
    {
      get;    
    }

    public abstract string AlternativeLocation
    {
      get;
    }

    public abstract Dictionary<string, XDocument> ExpectedProfiles
    {
      get;
    }

    public abstract void CreateProfiles();        
    
    [TestInitialize]
    public void Init()
    {
      DeleteProfiles();     
      CreateProfiles();      
    }
    
    [TestCleanup]
    public void Dispose()
    {
      DeleteProfiles();      
    }

    #region Tests
    [TestMethod]
    public void TestConstructor()
    {
      var fps = new FilePersistenceStorage(AppName);
      var fps2 = new FilePersistenceStorage(AppName, "AAABBB:\asdasdsad");
      //No exceptions - ok
    }

    [TestMethod]
    public void TestConstructor2()
    {
      var fps = new FilePersistenceStorage(AppName, PresetLocation);
      Assert.AreEqual(PresetLocation, fps.PresetFolder);
    }

    [TestMethod]
    public void TestUnsuccesfulStorageDirectoryCreation()
    {
      var fps = new FilePersistenceStorage(AppName) {BaseFolder = null, AlternativeFolder = null};
      
    }

    [TestMethod]
    public void TestList()
    {
      FilePersistenceStorage fps = CreateStorage1();
      CheckProfileItems(ExpectedProfiles.Keys, fps.AvailableProfiles);
    }

    [TestMethod]
    public void TestLoadExisting()
    {
      FilePersistenceStorage fps = CreateStorage1();      
      string key = fps.AvailableProfiles[0];
      XDocument state = fps.LoadState(key);
      Assert.AreEqual(state.ToString(), ExpectedProfiles[key].ToString(), "Wrong or no state loaded");
      Assert.AreEqual(ExpectedProfiles.Keys.Count, fps.AvailableProfiles.Count, "Different number of loaded and expected profiles");
    }    

    [TestMethod]
    [ExpectedException(typeof(InvalidOperationException), "Unable to load profile 'jkhkjhlkhj'. It is not available.")]
    public void TestLoadNonextant()
    {
      FilePersistenceStorage fps = CreateStorage1();
      string improbableName = "jkhkjhlkhj";
      fps.LoadState(improbableName);       
    }

    [TestMethod]
    public void TestSaveNew()
    {
      FilePersistenceStorage fps = CreateStorage1();
      string improbableName = "jkhkjhlkhj";
      fps.SaveState(new XDocument(new XElement("Blah")), improbableName);
      var newexpected = new List<string>(ExpectedProfiles.Keys) { improbableName };
      CheckProfileItems(newexpected, fps.AvailableProfiles);
      CheckProfileFileExists(improbableName);
    }

    [TestMethod]
    public void TestSaveExisting()
    {
      FilePersistenceStorage fps = CreateStorage1();
      var key = fps.AvailableProfiles[0];
      fps.SaveState(new XDocument(new XElement("Blah")), key);
      CheckProfileItems(ExpectedProfiles.Keys, fps.AvailableProfiles);
      CheckProfileFileExists(key);
    }

    [TestMethod]
    public void TestSaveAndLoad()
    {
      FilePersistenceStorage fps = CreateStorage1();

      XDocument docToSave = new XDocument(
        new XElement("State", 
          new XElement("Foo"), 
          new XElement("Foo", 
            new XElement("Bar"),
            new XElement("Bar"))));
      //also used to check that we don't modify the original xml after operations
      var docString = docToSave.ToString(); 

      string key = "AnotherImprobableKey123";
      //vvvvvvvv
      fps.SaveState(docToSave, key);
      XDocument loadedDoc = fps.LoadState(key);
      //^^^^^^^^
      var newexpected = new List<string>(ExpectedProfiles.Keys) { key };
      CheckProfileItems(newexpected, fps.AvailableProfiles);
      CheckProfileFileExists(key);
      Assert.AreEqual(docString, loadedDoc.ToString());
    }

    [TestMethod]
    public void TestDeleteExisting()
    {
      FilePersistenceStorage fps = CreateStorage1();
      string key = fps.AvailableProfiles[0];
      fps.DeleteState(key);
      var newexpectedKeys = new List<string>(ExpectedProfiles.Keys);
      newexpectedKeys.Remove(key);
      CheckProfileItems(fps.AvailableProfiles, newexpectedKeys);
      Assert.AreEqual(ExpectedProfiles.Keys.Count - 1, fps.AvailableProfiles.Count, "Different number of loaded and expected profiles");
    }

    [TestMethod]
    public void TestRenameExistingToNew()
    {
      FilePersistenceStorage fps = CreateStorage1();
      string newName = "SomeNewNonexistingProfileName";
      string key = fps.AvailableProfiles[0];
      fps.RenameState(key, newName);
      var newexpectedKeys = new List<string>(ExpectedProfiles.Keys);
      newexpectedKeys.Remove(key);
      newexpectedKeys.Insert(0, newName);
      CheckProfileItems(fps.AvailableProfiles, newexpectedKeys);
      Assert.AreEqual(ExpectedProfiles.Keys.Count, fps.AvailableProfiles.Count, "Different number of loaded and expected profiles");
    }

    [TestMethod]
    public void TestRenameExistingToExisting()
    {
      FilePersistenceStorage fps = CreateStorage1();      
      string key = fps.AvailableProfiles[0];
      string newName = fps.AvailableProfiles[1];
      fps.RenameState(key, newName);
      var newexpectedKeys = new List<string>(ExpectedProfiles.Keys);
      newexpectedKeys.Remove(key);
      CheckProfileItems(fps.AvailableProfiles, newexpectedKeys);
      Assert.AreEqual(ExpectedProfiles.Keys.Count - 1, fps.AvailableProfiles.Count, "Different number of loaded and expected profiles");
    }

    #endregion

    public void DeleteProfiles()
    {
      if (PresetLocation != null && Directory.Exists(PresetLocation))
      {
        Directory.Delete(PresetLocation, true);
      }

      if (BaseLocation != null && Directory.Exists(BaseLocation))
      {
        Directory.Delete(BaseLocation, true);
      }

      if (AlternativeLocation != null && Directory.Exists(AlternativeLocation))
      {
        Directory.Delete(AlternativeLocation, true);
      }
    }    

    private static void CheckProfileItems(ICollection<string> itemsToCheck1_, ICollection<string> itemsToCheck2_)
    {
      Assert.AreEqual(itemsToCheck1_.Count, itemsToCheck2_.Count, "Unexpected number of profiles");
      foreach (string expectedProfile in itemsToCheck1_)
      {
        Assert.IsTrue(itemsToCheck2_.Contains(expectedProfile));
      }
    }

    protected abstract void CheckProfileFileExists(string profile_);

    private FilePersistenceStorage CreateStorage1()
    {
      return new FilePersistenceStorage(AppName) { BaseFolder = BaseLocation, AlternativeFolder = AlternativeLocation};
    }
    

    // * could be created +
    // * list of available profiles + 
    // * Save state +
    // * Load state + 
    // * Delete state +
    // * Rename state
  }  
}
