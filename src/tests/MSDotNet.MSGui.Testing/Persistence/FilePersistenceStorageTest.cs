﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Persistence/FilePersistenceStorageTest.cs#3 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSDotNet.MSGui.Testing.Persistence
{
  public class FilePersistenceStorageTestWhenAllLocationsAvailable : FilePersistenceStorageTestBase
  {
    public override string AppName
    {
      get { return "TestApp1"; }
    }

    private readonly string m_presetLocation = Path.Combine(Path.GetTempPath(), @"MSGuiTest/Profiles");
    public override string PresetLocation
    {
      get { return m_presetLocation; }
    }

    private readonly string m_baseLocation = Path.Combine(Path.GetTempPath(), @"MSGuiTest/BaseLocation");
    public override string BaseLocation
    {
      get { return m_baseLocation; }
    }

    private readonly string m_altLocation = Path.Combine(Path.GetTempPath(), @"MSGuiTest/AltLocation");
    public override string AlternativeLocation
    {
      get { return m_altLocation; }
    }

    private Dictionary<string, XDocument> m_expectedProfilesDict = 
      new Dictionary<string, XDocument>();

    public override Dictionary<string, XDocument> ExpectedProfiles
    {
      get
      {
        return m_expectedProfilesDict;
      }
    }

    public override void CreateProfiles()
    {
      CreateSavedProfiles(PresetLocation);
      CreateSavedProfiles(BaseLocation);
      CreateSavedProfiles(AlternativeLocation);
    }

    protected override void CheckProfileFileExists(string profile_)
    {
      Assert.IsTrue(File.Exists(Path.Combine(BaseLocation, profile_ + ".state"))); 
      Assert.IsTrue(File.Exists(Path.Combine(AlternativeLocation, profile_ + ".state"))); 
    }

    private void CreateSavedProfiles(string folder)
    {
      if (Directory.Exists(folder))
      {
        Directory.Delete(folder);
      }
      Directory.CreateDirectory(folder);
      XDocument teststateDoc1 = new XDocument(new XElement("TestState",
                                     new XElement("ContentItem1"),
                                     new XElement("Item2"),
                                     new XElement("Item"),
                                     new XElement("Blah")));
      teststateDoc1.Save(Path.Combine(folder, "teststate.state"));
      m_expectedProfilesDict["teststate"] = teststateDoc1;
      teststateDoc1.Save(Path.Combine(folder, "teststate2.state"));
      m_expectedProfilesDict["teststate2"] = teststateDoc1;
      teststateDoc1.Save(Path.Combine(folder, "teststate3.state"));
      m_expectedProfilesDict["teststate3"] = teststateDoc1;
    }    
  }

  public class FilePersistenceStorageTestWhenOnlyBaseLocationAvailable : FilePersistenceStorageTestBase
  {
    public override string AppName
    {
      get { return "TestApp1"; }
    }
    
    public override string PresetLocation
    {
      get { return null; }
    }

    private readonly string m_baseLocation = Path.Combine(Path.GetTempPath(), @"MSGuiTest/BaseLocation");
    public override string BaseLocation
    {
      get { return m_baseLocation; }
    }

    public override string AlternativeLocation
    {
      get { return null; }
    }

    private Dictionary<string, XDocument> m_expectedProfilesDict =
      new Dictionary<string, XDocument>();

    public override Dictionary<string, XDocument> ExpectedProfiles
    {
      get
      {
        return m_expectedProfilesDict;
      }
    }

    public override void CreateProfiles()
    {
      CreateSavedProfiles(BaseLocation);      
    }

    protected override void CheckProfileFileExists(string profile_)
    {
      Assert.IsTrue(File.Exists(Path.Combine(BaseLocation, profile_ + ".state"))); 
    }

    private void CreateSavedProfiles(string folder)
    {
      if (Directory.Exists(folder))
      {
        Directory.Delete(folder);
      }
      Directory.CreateDirectory(folder);
      XDocument teststateDoc1 = new XDocument(new XElement("TestState",
                                     new XElement("ContentItem1"),
                                     new XElement("Item2"),
                                     new XElement("Item"),
                                     new XElement("Blah")));
      teststateDoc1.Save(Path.Combine(folder, "teststate.state"));
      m_expectedProfilesDict["teststate"] = teststateDoc1;
      teststateDoc1.Save(Path.Combine(folder, "teststate2.state"));
      m_expectedProfilesDict["teststate2"] = teststateDoc1;
      teststateDoc1.Save(Path.Combine(folder, "teststate3.state"));
      m_expectedProfilesDict["teststate3"] = teststateDoc1;
    }    
  }

  public class FilePersistenceStorageTestWhenOnlyAlternativeLocationAvailable : FilePersistenceStorageTestBase
  {
    public override string AppName
    {
      get { return "TestApp1"; }
    }

    private readonly string m_presetLocation = null;
    public override string PresetLocation
    {
      get { return m_presetLocation; }
    }
    
    public override string BaseLocation
    {
      get { return null; }
    }

    private readonly string m_altLocation = Path.Combine(Path.GetTempPath(), @"MSGuiTest/AltLocation");
    public override string AlternativeLocation
    {
      get { return m_altLocation; }
    }

    private Dictionary<string, XDocument> m_expectedProfilesDict =
      new Dictionary<string, XDocument>();

    public override Dictionary<string, XDocument> ExpectedProfiles
    {
      get
      {
        return m_expectedProfilesDict;
      }
    }

    public override void CreateProfiles()
    {
      CreateSavedProfiles(AlternativeLocation);
    }

    protected override void CheckProfileFileExists(string profile_)
    {      
      Assert.IsTrue(File.Exists(Path.Combine(AlternativeLocation, profile_ + ".state"))); 
    }

    private void CreateSavedProfiles(string folder)
    {
      if (Directory.Exists(folder))
      {
        Directory.Delete(folder);
      }
      Directory.CreateDirectory(folder);
      XDocument teststateDoc1 = new XDocument(new XElement("TestState",
                                     new XElement("ContentItem1"),
                                     new XElement("Item2"),
                                     new XElement("Item"),
                                     new XElement("Blah")));
      teststateDoc1.Save(Path.Combine(folder, "teststate.state"));
      m_expectedProfilesDict["teststate"] = teststateDoc1;
      teststateDoc1.Save(Path.Combine(folder, "teststate2.state"));
      m_expectedProfilesDict["teststate2"] = teststateDoc1;
      teststateDoc1.Save(Path.Combine(folder, "teststate3.state"));
      m_expectedProfilesDict["teststate3"] = teststateDoc1;
    }    
  }
}
