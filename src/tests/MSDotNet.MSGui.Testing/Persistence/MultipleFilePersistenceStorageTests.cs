/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Persistence/MultipleFilePersistenceStorageTests.cs#3 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSDotNet.MSGui.Testing.Persistence
{
  [TestClass]
  public class MultipleFilePersistenceStorageTestWhenAllLocationsAvailable : MultipleFilePersistenceStorageTestBase
  {
    public override string AppName
    {
      get { return "Appname1";  }
    }

    public override string PresetLocation
    {
      get { return @"MSGuiTest/Profiles"; }
    }

    public override string BaseLocation
    {
      get { return @"MSGuiTest/BaseLocation"; }
    }

    public override string AlternativeLocation
    {
      get { return @"MSGuiTest/AltLocation"; }
    }

    private readonly Dictionary<string, Dictionary<string, XElement>> m_expectedProfilesDict = new Dictionary<string, Dictionary<string, XElement>>();
    public override Dictionary<string, Dictionary<string, XElement>> ExpectedProfiles
    {
      get { return m_expectedProfilesDict; }
    }

    public override void CreateProfiles()
    {
      CreateSavedProfiles(PresetLocation);
      CreateSavedProfiles(BaseLocation);
      CreateSavedProfiles(AlternativeLocation);
    }

    private void CreateSavedProfiles(string folder)
    {
      if (Directory.Exists(folder))
      {
        Directory.Delete(folder);
      }
      Directory.CreateDirectory(folder);

      m_expectedProfilesDict["Profile1"] = CreateState("Profile1", folder);
      m_expectedProfilesDict["Profile2"] = CreateState("Profile2", folder);
      m_expectedProfilesDict["Profile3"] = CreateState("Profile3", folder); 
    }

    private Dictionary<string, XElement> CreateState(string profile_, string folder_)
    {
      var path = Path.Combine(folder_, profile_ + "_state");
      if (Directory.Exists(path))
      {
        Directory.Delete(path);
      }
      Directory.CreateDirectory(path);
      var result = new Dictionary<string, XElement>();
      result["ItemAAA"] = (CreateItem("ItemAAA", path));
      result["ItemBBB"] = (CreateItem("ItemBBB", path));
      result["ItemCCC"] = (CreateItem("ItemCCC", path));
      result["ViewDDD"] = (CreateSubitem("ViewDDD", path));
      return result;
    }

    XElement CreateItem(string name_, string profilefolder_)
    {
      XElement teststate = new XElement(name_,
                                     new XElement("ContentItem1"),
                                     new XElement("Item2"),
                                     new XElement("Item"),
                                     new XElement("Blah"));
      teststate.Save(Path.Combine(profilefolder_, name_ + ".msguistate"));
      return teststate;
    }

    XElement CreateSubitem(string name_, string profilefolder_)
    {
      XElement teststate = new XElement("View",
                                     new XElement("State"),
                                     new XElement("Item2"),
                                     new XElement("Item"),
                                     new XElement("Blah"));
      teststate.Save(Path.Combine(profilefolder_, name_ + ".msguisubstate"));
      return teststate;
    }

    protected override void CheckProfileFolderExists(string profile_)
    {      
      Assert.IsTrue(Directory.Exists(Path.Combine(BaseLocation, profile_ + "_state")));
      Assert.IsTrue(Directory.Exists(Path.Combine(AlternativeLocation, profile_ + "_state"))); 
    }

    protected override int CountProfileFolders()
    {
      int inbase = CountProfileFolders(BaseLocation);
      int inalt = CountProfileFolders(AlternativeLocation);
      Assert.IsTrue(inbase == inalt);
      return inbase;
    }

    private int CountProfileFolders(string folder_)
    {
      var dirInfo = new DirectoryInfo(folder_);
      return dirInfo.GetDirectories("*_state").Length;
    }

    protected override void CheckProfileItemExists(string profile_, string itemId_)
    {
      Assert.IsTrue(
        File.Exists(
          Path.Combine(
            Path.Combine(
              BaseLocation, profile_ + "_state"),
              itemId_ + ".msguistate")));
      Assert.IsTrue(
        File.Exists(
          Path.Combine(
            Path.Combine(
              AlternativeLocation, profile_ + "_state"),
              itemId_ + ".msguistate")));
    }

    protected override void CheckProfileSubItemExists(string profile_, string subitemId_)
    {
      Assert.IsTrue(
       File.Exists(
         Path.Combine(
           Path.Combine(
             BaseLocation, profile_ + "_state"),
             subitemId_ + ".msguisubstate")));
      Assert.IsTrue(
        File.Exists(
          Path.Combine(
            Path.Combine(
              AlternativeLocation, profile_ + "_state"),
              subitemId_ + ".msguisubstate")));
    }
  }

  [TestClass]
  public class MultipleFilePersistenceStorageTestWhenOnlyBaseAvailable : MultipleFilePersistenceStorageTestBase
  {
    public override string AppName
    {
      get { return "Appname1"; }
    }

    public override string PresetLocation
    {
      get { return null; }
    }

    public override string BaseLocation
    {
      get { return @"MSGuiTest/BaseLocation"; }
    }

    public override string AlternativeLocation
    {
      get { return null; }
    }

    private readonly Dictionary<string, Dictionary<string, XElement>> m_expectedProfilesDict = new Dictionary<string, Dictionary<string, XElement>>();
    public override Dictionary<string, Dictionary<string, XElement>> ExpectedProfiles
    {
      get { return m_expectedProfilesDict; }
    }

    public override void CreateProfiles()
    {      
      CreateSavedProfiles(BaseLocation);
    }

    private void CreateSavedProfiles(string folder)
    {
      if (Directory.Exists(folder))
      {
        Directory.Delete(folder);
      }
      Directory.CreateDirectory(folder);

      m_expectedProfilesDict["Profile1"] = CreateState("Profile1", folder);
      m_expectedProfilesDict["Profile2"] = CreateState("Profile2", folder);
      m_expectedProfilesDict["Profile3"] = CreateState("Profile3", folder);
    }

    private Dictionary<string, XElement> CreateState(string profile_, string folder_)
    {
      var path = Path.Combine(folder_, profile_ + "_state");
      if (Directory.Exists(path))
      {
        Directory.Delete(path);
      }
      Directory.CreateDirectory(path);
      var result = new Dictionary<string, XElement>();
      result["ItemAAA"] = (CreateItem("ItemAAA", path));
      result["ItemBBB"] = (CreateItem("ItemBBB", path));
      result["ItemCCC"] = (CreateItem("ItemCCC", path));
      result["ViewDDD"] = (CreateSubitem("ViewDDD", path));
      return result;
    }

    XElement CreateItem(string name_, string profilefolder_)
    {
      XElement teststate = new XElement(name_,
                                     new XElement("ContentItem1"),
                                     new XElement("Item2"),
                                     new XElement("Item"),
                                     new XElement("Blah"));
      teststate.Save(Path.Combine(profilefolder_, name_ + ".msguistate"));
      return teststate;
    }

    XElement CreateSubitem(string name_, string profilefolder_)
    {
      XElement teststate = new XElement("View",
                                     new XElement("State"),
                                     new XElement("Item2"),
                                     new XElement("Item"),
                                     new XElement("Blah"));
      teststate.Save(Path.Combine(profilefolder_, name_ + ".msguisubstate"));
      return teststate;
    }

    protected override void CheckProfileFolderExists(string profile_)
    {
      Assert.IsTrue(Directory.Exists(Path.Combine(BaseLocation, profile_ + "_state")));      
    }

    protected override int CountProfileFolders()
    {
      int inbase = CountProfileFolders(BaseLocation);
      int inalt = CountProfileFolders(BaseLocation);
      Assert.IsTrue(inbase == inalt);
      return inbase;
    }

    private int CountProfileFolders(string folder_)
    {
      var dirInfo = new DirectoryInfo(folder_);
      return dirInfo.GetDirectories("*_state").Length;
    }

    protected override void CheckProfileItemExists(string profile_, string itemId_)
    {
      Assert.IsTrue(
        File.Exists(
          Path.Combine(
            Path.Combine(
              BaseLocation, profile_ + "_state"),
              itemId_ + ".msguistate")));
      }

    protected override void CheckProfileSubItemExists(string profile_, string subitemId_)
    {
      Assert.IsTrue(
       File.Exists(
         Path.Combine(
           Path.Combine(
             BaseLocation, profile_ + "_state"),
             subitemId_ + ".msguisubstate")));
      }
  }

  [TestClass]
  public class MultipleFilePersistenceStorageTestWhenOnlyAlternativeAvailable : MultipleFilePersistenceStorageTestBase
  {
    public override string AppName
    {
      get { return "Appname1"; }
    }

    public override string PresetLocation
    {
      get { return null; }
    }

    public override string BaseLocation
    {
      get { return null; }
    }

    public override string AlternativeLocation
    {
      get { return @"MSGuiTest/AltLocation"; }
    }

    private readonly Dictionary<string, Dictionary<string, XElement>> m_expectedProfilesDict = new Dictionary<string, Dictionary<string, XElement>>();
    public override Dictionary<string, Dictionary<string, XElement>> ExpectedProfiles
    {
      get { return m_expectedProfilesDict; }
    }

    public override void CreateProfiles()
    {            
      CreateSavedProfiles(AlternativeLocation);
    }

    private void CreateSavedProfiles(string folder)
    {
      if (Directory.Exists(folder))
      {
        Directory.Delete(folder);
      }
      Directory.CreateDirectory(folder);

      m_expectedProfilesDict["Profile1"] = CreateState("Profile1", folder);
      m_expectedProfilesDict["Profile2"] = CreateState("Profile2", folder);
      m_expectedProfilesDict["Profile3"] = CreateState("Profile3", folder);
    }

    private Dictionary<string, XElement> CreateState(string profile_, string folder_)
    {
      var path = Path.Combine(folder_, profile_ + "_state");
      if (Directory.Exists(path))
      {
        Directory.Delete(path);
      }
      Directory.CreateDirectory(path);
      var result = new Dictionary<string, XElement>();
      result["ItemAAA"] = (CreateItem("ItemAAA", path));
      result["ItemBBB"] = (CreateItem("ItemBBB", path));
      result["ItemCCC"] = (CreateItem("ItemCCC", path));
      result["ViewDDD"] = (CreateSubitem("ViewDDD", path));
      return result;
    }

    XElement CreateItem(string name_, string profilefolder_)
    {
      XElement teststate = new XElement(name_,
                                     new XElement("ContentItem1"),
                                     new XElement("Item2"),
                                     new XElement("Item"),
                                     new XElement("Blah"));
      teststate.Save(Path.Combine(profilefolder_, name_ + ".msguistate"));
      return teststate;
    }

    XElement CreateSubitem(string name_, string profilefolder_)
    {
      XElement teststate = new XElement("View",
                                     new XElement("State"),
                                     new XElement("Item2"),
                                     new XElement("Item"),
                                     new XElement("Blah"));
      teststate.Save(Path.Combine(profilefolder_, name_ + ".msguisubstate"));
      return teststate;
    }

    protected override void CheckProfileFolderExists(string profile_)
    {      
      Assert.IsTrue(Directory.Exists(Path.Combine(AlternativeLocation, profile_ + "_state")));
    }

    protected override int CountProfileFolders()
    {      
      int inalt = CountProfileFolders(AlternativeLocation);     
      return inalt;
    }

    private int CountProfileFolders(string folder_)
    {
      var dirInfo = new DirectoryInfo(folder_);
      return dirInfo.GetDirectories("*_state").Length;
    }

    protected override void CheckProfileItemExists(string profile_, string itemId_)
    {
      Assert.IsTrue(
        File.Exists(
          Path.Combine(
            Path.Combine(
              AlternativeLocation, profile_ + "_state"),
              itemId_ + ".msguistate")));
    }

    protected override void CheckProfileSubItemExists(string profile_, string subitemId_)
    {     
      Assert.IsTrue(
        File.Exists(
          Path.Combine(
            Path.Combine(
              AlternativeLocation, profile_ + "_state"),
              subitemId_ + ".msguisubstate")));
    }
  }

}
