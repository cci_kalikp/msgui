﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Persistence/FilePersistenceStorageInvalidFoldersTest.cs#3 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.IO;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSDotNet.MSGui.Testing.Persistence
{
  [TestClass]
  public class FilePersistenceStorageInvalidFoldersTest
  {
    private string m_appName = "TestAppName";

    [TestMethod]
    public void IllegalFoldersTest()
    {

      var fps = new FilePersistenceStorage(m_appName, "AAABBB:\a\n\tsdasdsad");
      fps.BaseFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.AlternativeFolder = ("AAABBB:\a\n\tsdasdsad");
      //no exceptions expected
    }

    [TestMethod]
    public void IllegalAppnameTest()
    {
      if (Directory.Exists("U:") && Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)))
      {
        var fps = new FilePersistenceStorage("11+++\a\n\tsdasdsa&*!Ad");
        Assert.IsNotNull(fps.BaseFolder);
        Assert.IsNotNull(fps.AlternativeFolder);
        //names are sanitized, dirs created
        Assert.AreEqual("U:\\Application Data\\Morgan Stanley", Directory.GetParent(fps.BaseFolder).FullName);
        Assert.AreEqual(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                                           "Morgan Stanley"), Directory.GetParent(fps.AlternativeFolder).FullName);
        Directory.Delete(fps.BaseFolder);
        Directory.Delete(fps.AlternativeFolder);

      }
    }

    [TestMethod]
    public void IllegalAppnameTest2()
    {
      if (Directory.Exists("U:") && Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)))
      {
        string expectedBase = "U:\\Application Data\\Morgan Stanley\\_____.";
        string expectedAlternative = Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                                           "Morgan Stanley"), "_____.");
        var fps = new FilePersistenceStorage("..\\../...");
        Assert.AreEqual(expectedBase, fps.BaseFolder);
        Assert.AreEqual(expectedAlternative, fps.AlternativeFolder);
        //names are sanitized, dirs created
        Directory.Delete(expectedBase);
        Directory.Delete(expectedAlternative);
        //if exception here, smth was wrong with dir creation        
      }
    }

    [TestMethod]
    [ExpectedException(typeof(System.InvalidOperationException),
      "Unable to load profile 'Default'. It is not available.")]
    public void IllegalFoldersLoad()
    {
      var fps = new FilePersistenceStorage(m_appName, "AAABBB:\a\n\tsdasdsad");
      fps.BaseFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.AlternativeFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.LoadState("Default");
    }

    [TestMethod]
    [ExpectedException(
      typeof(System.InvalidOperationException),
      "Failed to Save profile Default to any location")]
    public void IllegalFoldersSave()
    {
      var fps = new FilePersistenceStorage(m_appName, "AAABBB:\a\n\tsdasdsad");
      fps.BaseFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.AlternativeFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.SaveState(new XDocument(), "Default");
    }

    [TestMethod]
    [ExpectedException(
      typeof(System.InvalidOperationException),
      "Unable to rename state Default to Default1")]
    public void IllegalFoldersRename()
    {
      var fps = new FilePersistenceStorage(m_appName, "AAABBB:\a\n\tsdasdsad");
      fps.BaseFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.AlternativeFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.RenameState("Default", "Default1");
    }

    [TestMethod]
    [ExpectedException(
      typeof(System.InvalidOperationException),
      "Unable to delete state Default")]
    public void IllegalFoldersDelete()
    {
      var fps = new FilePersistenceStorage(m_appName, "AAABBB:\a\n\tsdasdsad");
      fps.BaseFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.AlternativeFolder = ("AAABBB:\a\n\tsdasdsad");
      fps.DeleteState("Default");
    }

    [TestMethod]
    [ExpectedException(
      typeof(System.InvalidOperationException),
      "Application name must be not empty")]
    public void EmptyApplicationName()
    {
      var fps = new FilePersistenceStorage("");
    }

    [TestMethod]
    [ExpectedException(
      typeof(System.InvalidOperationException),
      "Application name must be not empty")]
    public void NullApplicationName()
    {
      var fps = new FilePersistenceStorage(null);
    }
  }
}
