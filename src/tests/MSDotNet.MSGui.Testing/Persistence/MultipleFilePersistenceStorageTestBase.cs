﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/tests/MSDotNet.MSGui.Testing/Persistence/MultipleFilePersistenceStorageTestBase.cs#4 $
// $Change: 839595 $
// $DateTime: 2013/07/29 12:06:16 $
// $Author: milosp $

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSDotNet.MSGui.Testing.Persistence
{
  [TestClass] 
  abstract public class MultipleFilePersistenceStorageTestBase
  {
    public abstract string AppName
    {
      get;
    }

    public abstract string PresetLocation
    {
      get;
    }

    public abstract string BaseLocation
    {
      get;
    }

    public abstract string AlternativeLocation
    {
      get;
    }

    public abstract Dictionary<string, Dictionary<string, XElement>> ExpectedProfiles
    {
      get;
    }

    public abstract void CreateProfiles();

    [TestInitialize]
    public void Init()
    {
      DeleteProfiles();
      CreateProfiles();
    }

    [TestCleanup]
    public void Dispose()
    {
      DeleteProfiles();
    }

    #region Tests
    [TestMethod]
    public void TestConstructor()
    {
      var fps = new MultipleFilePersistenceStorage(AppName);
      var fps2 = new MultipleFilePersistenceStorage(AppName, "AAABBB:\asdasdsad");
      //No exceptions - ok
    }

    [TestMethod]
    public void TestConstructor2()
    {
      var fps = new MultipleFilePersistenceStorage(AppName, PresetLocation);
      Assert.AreEqual(PresetLocation, fps.PresetFolder);
    }

    [TestMethod]
    public void TestUnsuccesfulStorageDirectoryCreation()
    {
      var fps = new MultipleFilePersistenceStorage(AppName) { BaseFolder = null, AlternativeFolder = null };
    }

    [TestMethod]
    public void TestList()
    {
      MultipleFilePersistenceStorage fps = CreateStorage1();
      CompareCollections(ExpectedProfiles.Keys, fps.AvailableProfiles);
    }  

    [TestMethod]
    public void TestDeleteExisting()
    {
      MultipleFilePersistenceStorage fps = CreateStorage1();
      string key = fps.AvailableProfiles[0];
      fps.DeleteState(key);
      var newexpectedKeys = new List<string>(ExpectedProfiles.Keys);
      newexpectedKeys.Remove(key);
      CompareCollections(fps.AvailableProfiles, newexpectedKeys);
      Assert.AreEqual(ExpectedProfiles.Keys.Count - 1, fps.AvailableProfiles.Count, "Different number of loaded and expected profiles");
    }

    [TestMethod]
    public void TestRenameExistingToNew()
    {
      MultipleFilePersistenceStorage fps = CreateStorage1();
      string newName = "SomeNewNonexistingProfileName";
      string key = fps.AvailableProfiles[0];
      fps.RenameState(key, newName);
      var newexpectedKeys = new List<string>(ExpectedProfiles.Keys);
      newexpectedKeys.Remove(key);
      newexpectedKeys.Insert(0, newName);
      CompareCollections(fps.AvailableProfiles, newexpectedKeys);
      Assert.AreEqual(ExpectedProfiles.Keys.Count, fps.AvailableProfiles.Count, "Different number of loaded and expected profiles");
    }

    [TestMethod]
    public void TestRenameExistingToExisting()
    {
      MultipleFilePersistenceStorage fps = CreateStorage1();
      string key = fps.AvailableProfiles[0];
      string newName = fps.AvailableProfiles[1];
      fps.RenameState(key, newName);
      var newexpectedKeys = new List<string>(ExpectedProfiles.Keys);
      newexpectedKeys.Remove(key);
      CompareCollections(fps.AvailableProfiles, newexpectedKeys);
      Assert.AreEqual(fps.AvailableProfiles.Count, CountProfileFolders());
      Assert.AreEqual(ExpectedProfiles.Keys.Count - 1, fps.AvailableProfiles.Count, "Different number of loaded and expected profiles");
    }

    [TestMethod]
    public void TestSaveNewItemForExistingProfile()
    {
      MultipleFilePersistenceStorage fps = CreateStorage1();
      var profile = fps.AvailableProfiles[0];
      fps.SaveState(profile, 
        new Dictionary<string, XElement> {{"NetItemID", new XElement("NewItem")}}, null, false);
      CheckProfileItemExists(profile, "NetItemID");
      Assert.AreEqual(fps.AvailableProfiles.Count, CountProfileFolders());
      Assert.AreEqual(ExpectedProfiles.Count, fps.AvailableProfiles.Count);
    }

    [TestMethod]
    public void TestSaveNewItemReplacingExistingProfile()
    {
      MultipleFilePersistenceStorage fps = CreateStorage1();
      var profile = fps.AvailableProfiles[0];
      fps.SaveState(profile,
        new Dictionary<string, XElement> { { "NetItemID", new XElement("NewItem") } }, null, true);
      CheckProfileItemExists(profile, "NetItemID");
      Assert.AreEqual(fps.AvailableProfiles.Count, CountProfileFolders());
      Assert.AreEqual(ExpectedProfiles.Count, fps.AvailableProfiles.Count);
    }

    [TestMethod]
    public void TestSaveNewItemForNonExistingProfile()
    {
      MultipleFilePersistenceStorage fps = CreateStorage1();
      fps.SaveState("NewProfile",
       new Dictionary<string, XElement> {{"NetItemID",  new XElement("NewItem")}}, null, false);
      CheckProfileFolderExists("NewProfile");
      CheckProfileItemExists("NewProfile", "NetItemID");
      Assert.AreEqual(fps.AvailableProfiles.Count, CountProfileFolders());
      Assert.AreEqual(ExpectedProfiles.Count + 1, fps.AvailableProfiles.Count);
    }

    [TestMethod]
    public void TestSaveNewItemForNonExistingProfileWithClean()
    {
      MultipleFilePersistenceStorage fps = CreateStorage1();
      fps.SaveState("NewProfile",
       new Dictionary<string, XElement> { { "NetItemID", new XElement("NewItem") } }, null, true);
      CheckProfileFolderExists("NewProfile");
      CheckProfileItemExists("NewProfile", "NetItemID");
      Assert.AreEqual(fps.AvailableProfiles.Count, CountProfileFolders());
      Assert.AreEqual(ExpectedProfiles.Count + 1, fps.AvailableProfiles.Count);
    }

    [TestMethod]
    public void TestSaveNewSubItemForExistingProfile()
    {
      MultipleFilePersistenceStorage fps = CreateStorage1();
      var profile = fps.AvailableProfiles[0];
      fps.SaveState(profile, 
        null, new Dictionary<string, XElement> {{"NetSubItemID",new XElement("SubItem")}}, false);
      CheckProfileSubItemExists(profile, "NetSubItemID");
      Assert.AreEqual(fps.AvailableProfiles.Count, CountProfileFolders());
      Assert.AreEqual(ExpectedProfiles.Count, fps.AvailableProfiles.Count);    
    }

    [TestMethod]
    public void TestSaveNewSubItemReplacingExistingProfile()
    {
      MultipleFilePersistenceStorage fps = CreateStorage1();
      var profile = fps.AvailableProfiles[0];
      fps.SaveState(profile,
        null, new Dictionary<string, XElement> { { "NetSubItemID", new XElement("SubItem") } }, true);
      CheckProfileSubItemExists(profile, "NetSubItemID");
      Assert.AreEqual(fps.AvailableProfiles.Count, CountProfileFolders());
      Assert.AreEqual(ExpectedProfiles.Count, fps.AvailableProfiles.Count);
    }

    [TestMethod]
    public void TestSaveNewSubItemForNonExistingProfile()
    {
      MultipleFilePersistenceStorage fps = CreateStorage1();
      fps.SaveState("NewProfile",
        null, new Dictionary<string, XElement> { { "NetSubItemID", new XElement("SubItem") } }, false);
      CheckProfileSubItemExists("NewProfile", "NetSubItemID");
      Assert.AreEqual(fps.AvailableProfiles.Count, CountProfileFolders());      
      Assert.AreEqual(ExpectedProfiles.Count + 1, fps.AvailableProfiles.Count);
    }

    [TestMethod]
    public void TestSaveNewSubItemForNonExistingProfileWithClean()
    {
      MultipleFilePersistenceStorage fps = CreateStorage1();
      fps.SaveState("NewProfile",
        null, new Dictionary<string, XElement> { { "NetSubItemID", new XElement("SubItem") } }, true);
      CheckProfileSubItemExists("NewProfile", "NetSubItemID");
      Assert.AreEqual(fps.AvailableProfiles.Count, CountProfileFolders());
      Assert.AreEqual(ExpectedProfiles.Count + 1, fps.AvailableProfiles.Count);
    }

    [TestMethod]
    public void TestLoadForExistingProfile()
    {
      MultipleFilePersistenceStorage fps = CreateStorage1();
      var profile = fps.AvailableProfiles[0];
      var items = new Dictionary<string, XElement>();
      var subitems = new Dictionary<string, XElement>();

      fps.LoadState(profile, out items, out subitems);
      List<string> allKeys = new List<string>(items.Keys);
      allKeys.AddRange(subitems.Keys);
      CompareCollections(ExpectedProfiles[profile].Keys, allKeys);

      Assert.AreEqual(fps.AvailableProfiles.Count, CountProfileFolders());
      Assert.AreEqual(ExpectedProfiles.Count, fps.AvailableProfiles.Count);
    }

    [TestMethod]
    [ExpectedException(typeof(InvalidOperationException), "Unable to load profile 'NewProfile1234'. It is not available.")]
    public void TestLoadForNonExistingProfile()
    {
      MultipleFilePersistenceStorage fps = CreateStorage1();      
      var items = new Dictionary<string, XElement>();
      var subitems = new Dictionary<string, XElement>();

      fps.LoadState("NewProfile1234", out items, out subitems);
    }

    [TestMethod]
    public void TestSaveAndLoadReplacingExistingProfile()
    {
      MultipleFilePersistenceStorage fps = CreateStorage1();
      var profile = fps.AvailableProfiles[0];

      var itemsToSave = new Dictionary<string, XElement>
                          {
                            {"NetItemID1", new XElement("NewItem1")},
                            {"NetItemID2", new XElement("NewItem2")}
                          };
      var subitemsToSave = new Dictionary<string, XElement>
                          {
                            {"NetSubItemID1", new XElement("NewSubItem1")},
                            {"NetSubItemID2", new XElement("NewSubItem2")}
                          };

      fps.SaveState(profile,
        itemsToSave, subitemsToSave, true);

      var items = new Dictionary<string, XElement>();
      var subitems = new Dictionary<string, XElement>();

      fps.LoadState(profile, out items, out subitems);

      CompareCollections(itemsToSave.Keys, items.Keys);
      CompareCollections(subitemsToSave.Keys, subitems.Keys);

      Assert.AreEqual(fps.AvailableProfiles.Count, CountProfileFolders());
      Assert.AreEqual(ExpectedProfiles.Count, fps.AvailableProfiles.Count);
    }

    [TestMethod]
    public void TestSaveAndLoadAddingToExistingProfile()
    {
      MultipleFilePersistenceStorage fps = CreateStorage1();
      var profile = fps.AvailableProfiles[0];

      var itemsToSave = new Dictionary<string, XElement>
                          {
                            {"NetItemID1", new XElement("NewItem1")},
                            {"NetItemID2", new XElement("NewItem2")}
                          };
      var subitemsToSave = new Dictionary<string, XElement>
                          {
                            {"NetSubItemID1", new XElement("NewSubItem1")},
                            {"NetSubItemID2", new XElement("NewSubItem2")}
                          };

      fps.SaveState(profile,
        itemsToSave, subitemsToSave, false);

      var items = new Dictionary<string, XElement>();
      var subitems = new Dictionary<string, XElement>();

      fps.LoadState(profile, out items, out subitems);

      List<string> allloadedkeys = new List<string>(items.Keys);
      allloadedkeys.AddRange(subitems.Keys);

      List<string> allexpectedkeys = new List<string>(ExpectedProfiles[profile].Keys);
      allexpectedkeys.AddRange(itemsToSave.Keys);
      allexpectedkeys.AddRange(subitemsToSave.Keys);

      CompareCollections(allloadedkeys, allexpectedkeys);      

      Assert.AreEqual(fps.AvailableProfiles.Count, CountProfileFolders());
      Assert.AreEqual(ExpectedProfiles.Count, fps.AvailableProfiles.Count);
    }


    #endregion

    public void DeleteProfiles()
    {
        try
        {
            if (PresetLocation != null && Directory.Exists(PresetLocation))
            {
                Directory.Delete(PresetLocation, true);
            }

            if (BaseLocation != null && Directory.Exists(BaseLocation))
            {
                Directory.Delete(BaseLocation, true);
            }

            if (AlternativeLocation != null && Directory.Exists(AlternativeLocation))
            {
                Directory.Delete(AlternativeLocation, true);
            }
        }
        catch (Exception)
        {
        }
    }

    private static void CompareCollections(ICollection<string> itemsToCheck1_, ICollection<string> itemsToCheck2_)
    {
      Assert.AreEqual(itemsToCheck1_.Count, itemsToCheck2_.Count, "Unexpected number of profiles");
      foreach (string expectedProfile in itemsToCheck1_)
      {
        Assert.IsTrue(itemsToCheck2_.Contains(expectedProfile));
      }
    }

    protected abstract void CheckProfileFolderExists(string profile_);
    protected abstract int CountProfileFolders();
    protected abstract void CheckProfileItemExists(string profile_, string itemId_);
    protected abstract void CheckProfileSubItemExists(string profile_, string subitemId_);

    private MultipleFilePersistenceStorage CreateStorage1()
    {
      return new MultipleFilePersistenceStorage(AppName) { BaseFolder = BaseLocation, AlternativeFolder = AlternativeLocation };
    }

    private MultipleFilePersistenceStorage CreateStorage2()
    {
      return new MultipleFilePersistenceStorage(AppName, PresetLocation) { BaseFolder = BaseLocation, AlternativeFolder = AlternativeLocation };
    }
  }
}
