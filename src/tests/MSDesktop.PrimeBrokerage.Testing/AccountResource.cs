﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Entitlement;

namespace MSDesktop.PrimeBrokerage.Testing
{
    [EntitlementResource(ResourceType = "Account")]
    public class AccountResource
    {
        public AccountResource()
        {
            Client = FSAttribute = TradingName = Description = string.Empty;
        }

        [EntitlementResourceProperty(PBEntitlementService.ResourceName)]
        public string Name { get; set; }

        [EntitlementResourceProperty]
        public string Client { get; set; }

        [EntitlementResourceProperty("FS Attribute")]
        public string FSAttribute { get; set; }

        [EntitlementResourceProperty]
        public string TradingName { get; set; }

        [EntitlementResourceProperty(PBEntitlementService.ResourceDescription)]
        public string Description { get; set; }

    }
}
