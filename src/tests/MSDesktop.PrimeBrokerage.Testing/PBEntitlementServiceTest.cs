﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MorganStanley.Desktop.Entitlement;
using NUnit.Framework;

namespace MSDesktop.PrimeBrokerage.Testing
{
    [TestFixture]
    public class PBEntitlementServiceTest
    {
         
        [SetUp]
        public void SetUp()
        {
            
        }

        [Test, Explicit]
        public void TestGetPermittedResources()
        {
            var service = new PBEntitlementService(PBEnvironment.Uat, "CANS", null, "579008");
            var result = service.GetPermittedResources("view", "Account").ToList(); 
            Assert.IsTrue(result.Count == 20);
            Assert.IsTrue(result[0].GetAttributeNames().Contains(PBEntitlementService.ResourceName));
            Assert.IsTrue(result[0][PBEntitlementService.ResourceName] == "098580MS9");  
            Assert.IsTrue(result[0].GetAttributeNames().Contains("Client"));
            Assert.IsTrue(result[0]["Client"] == "IPBC1176");

            var result2 = service.GetPermittedResources<AccountResource>("view").ToList();
            Assert.IsTrue(result2.Count == result.Count);
            Assert.IsTrue(result2[0].Client == result[0]["Client"]);
            Assert.IsTrue(result2[0].Description == result[0][PBEntitlementService.ResourceDescription]);
            Assert.IsNotNullOrEmpty(result2[0].Description);
            Assert.IsTrue((result2[0].TradingName == result[0]["TradingName"]));
            Assert.IsNotNullOrEmpty(result2[0].TradingName);
            Assert.IsTrue(result2[0].FSAttribute == result[0]["FS Attribute"]);
            Assert.IsNotNullOrEmpty(result2[0].FSAttribute);
        }

        [Test, Explicit]
        public void TestGetPermittedActions()
        {
            var svc = new PBEntitlementService(PBEnvironment.Uat, "CANS", null, "579008");
            var testResc = new AccountResource() {Name = "098580MS9"}; 
            var result = svc.GetPermittedActions(testResc).ToList();
            Assert.IsTrue(result.Count == 2);
            Assert.IsTrue(result[0] == "view");
            Assert.IsTrue(result[1] == "edit"); 
            
        }

        [Test, Explicit] 
        public void TestIsUserAllowed()
        {
            var svc = new PBEntitlementService(PBEnvironment.Uat, "CANS", null, "579008");
            var testResc = new AccountResource() { Name = "098580MS9" }; 
            bool allowed = svc.IsUserAllowed("edit", testResc);
            Assert.IsTrue(allowed);
            allowed = svc.IsUserAllowed("view", testResc);
            Assert.IsTrue(allowed);
            allowed = svc.IsUserAllowed("delete", testResc);
            Assert.IsFalse(allowed);
            testResc = new AccountResource() {Name = "999999"};
            allowed = svc.IsUserAllowed("view", testResc);
            Assert.IsFalse(allowed);
        }
 
          
    }
     
}
