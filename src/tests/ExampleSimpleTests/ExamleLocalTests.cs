﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using MSDesktop.CUIT.Common;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace ExampleSimpleTests
{
    /// <summary>
    /// Summary description for ExamleLocalTests
    /// </summary>
    [CodedUITest]
    [DeploymentItem("LocalExampleList.xml")]
    public class ExamleLocalTests
    {
       
        private UITestProvider _testProvider; 
        private static UITestProviderFactory _uiTestProviderFactory = new UITestProviderFactory();

        private static  string _baseDirectory;
        private static  string _execDirectory;


       

        [ClassInitialize()]
        public static void SetupTestForAll(TestContext testContext)
        {
            _baseDirectory = Environment.GetEnvironmentVariable("ExamplesRoot") ?? @"..\..\..\examples";
            _execDirectory = Environment.GetEnvironmentVariable("ExamplesRelative") ?? @"bin\debug";

            #region Specific Behaviors

            var testProvider = _uiTestProviderFactory.Providers["SimpleCWTest"];
            Action<UITestProvider> taskDialogExampleBehavior
                = (x) =>
                {
                    var provider = (SingleUITestProvider)x;
                    var app = provider.Application;
                    var mainWindow = app.MainWindow;
                    var task = app.GetTaskDialog();
                    task.ClickButton("Close");
                    task.WaitForControlNotExist(1000);

                    task = app.GetTaskDialog();
                    task.ClickButton("OK");
                    task.WaitForControlNotExist(1000);

                    task = app.GetTaskDialog();
                    task.ClickButton("Close");
                    task.WaitForControlNotExist(1000);

                    task = app.GetTaskDialog();
                    task.ClickButton("remove");
                    task.WaitForControlNotExist(1000);

                    task = app.GetTaskDialog();
                    task.ClickButton("Close");
                    task.WaitForControlNotExist(1000);
                };

            testProvider.RegisterBehavior("TaskDialogExamples", taskDialogExampleBehavior);

            Action<UITestProvider> exampleFrameworkAppBehavior
                = (x) =>
                {
                    var provider = (SingleUITestProvider)x;
                    var app = provider.Application;
                    var mainWindow = app.MainWindow;

                    ShellWindowHelper.ClickMenuItem(mainWindow, "Exit");
                    var quitDialog = app.GetTaskDialog();
                    quitDialog.WaitForControlExist(1000);
                    quitDialog.ClickButton("Quit");
                    quitDialog.WaitForControlNotExist(1000);

                    var dialog = app.GetTaskDialog();
                    dialog.WaitForControlExist(1000);
                    dialog.ClickButton("Close");
                    Assert.IsTrue(mainWindow.CurrentProcess.WaitForExit(10000),
                                      "Application not quite in time for {0}", provider.TestName);
                    testContext.WriteLine("Customized quit {0}", provider.TestName);
                };

            testProvider.RegisterQuitBehavior("ExampleFrameworkApp", exampleFrameworkAppBehavior);

            #endregion
        }

        [ClassCleanup]
        public static void ReleaseResources()
        {
            _uiTestProviderFactory = null;
        }
       
        
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
               "|DataDirectory|\\LocalExampleList.xml",
               "Example",
                DataAccessMethod.Sequential)]
        [TestMethod]
        public void ExampleLocalLaunchAndQuitTest()
        {
           
           _testProvider.RunTest();
        }


        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void MyTestInitialize()
        {        
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463

            var exampleInfo = LoadTestConfig(TestContext.DataRow);
            _testProvider = _uiTestProviderFactory.Providers[exampleInfo.TestProviderName];

            _testProvider.SetupTest(exampleInfo,TestContext);
        }

        ////Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            _testProvider.TestCleanUp();
        }

        #endregion



        //convert xml snip to test config
        public TestConfiguration LoadTestConfig(DataRow data)
        {
            var folder = ((string)data["Folder"]).Trim();
            var exec = ((string)data["Exec"]).Trim();
            var dir = System.IO.Path.Combine(_baseDirectory, folder, _execDirectory, exec);
            var arguments = ((string)data["CommandLine"]).Trim();
            return new TestConfiguration()
            {
                LoginProfile = ExamplesLoader.GetColumnData(data, "LoginProfile", ""),
                TestName = folder,
                //ApplicationName = ((string) data["ApplicationName"]).Trim(),
                FullPath = dir,
                CommandLine = arguments,
                TestProviderName = "SimpleCWTest"
            };
        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
