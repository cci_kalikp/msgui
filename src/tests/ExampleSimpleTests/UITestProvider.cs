﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UITest.Common.UIMap;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExampleSimpleTests
{
    public abstract class UITestProvider
    {
        protected abstract void StartApplications();

       

        protected abstract void PlaybackBehavior();

        protected abstract void VerifyVisual();

        protected abstract void VerifyResults();
        

        protected abstract void QuitApplications();

        public abstract string TestName { get;  }

        protected Process Process { get; set; }

        protected TestContext TestContext { get; private set; }

        private TestConfiguration _testConfiguration;

        protected virtual TestConfiguration TestConfiguration
        {
            get
            {
                return _testConfiguration;
            }
            set { _testConfiguration = value; }
        }

       

        public virtual void SetupTest(TestConfiguration testConfiguration, TestContext testContext)
        {
            _testConfiguration = testConfiguration;
            TestContext = testContext;
        }

        public virtual void RunTest()
        {
            StartApplications();
            PlaybackBehavior();
            VerifyVisual();
            VerifyResults();
        }

        public virtual void TestCleanUp()
        {
            try
            {
                QuitApplications();
                TestConfiguration = null; 

            }
            finally
            {
                Process.Refresh();
                
                if (Process != null && !Process.HasExited)
                {   
                    Process.Kill();
                    Process.WaitForExit(600000);
                    Assert.Fail("Application {0} not finished properly", TestName);
                }
            }
        }


        public UIMap UIMap { set; get; }


        
        #region Playbacks
        private IDictionary<string, Action<UITestProvider>> _behaviors = new Dictionary<string, Action<UITestProvider>>();
        private IDictionary<string, Action<UITestProvider>> _quitBehaviors = new Dictionary<string, Action<UITestProvider>>();
        private IDictionary<string, Action<UITestProvider>> _verifications = new Dictionary<string, Action<UITestProvider>>();

        
        protected IDictionary<string, Action<UITestProvider>> Behaviors { get { return _behaviors; } }
        protected IDictionary<string, Action<UITestProvider>> QuitBehaviors { get { return _quitBehaviors; } }
        protected IDictionary<string, Action<UITestProvider>> Verifications { get { return _verifications; } }


        internal void RegisterBehavior(string name, Action<UITestProvider> behavior)
        {
            Assert.IsFalse(_behaviors.ContainsKey(name), "Behavior name conflicts {0}", name);
            _behaviors[name] = behavior;

        }

        internal void RegisterQuitBehavior(string name, Action<UITestProvider> quitBehavior)
        {
            Assert.IsFalse(_quitBehaviors.ContainsKey(name), "Quit Application behavior name conflicts {0}", name);
            _quitBehaviors[name] = quitBehavior;
        }

        internal void RegisterVerification(string name, Action<UITestProvider> verification)
        {
            Assert.IsFalse(_verifications.ContainsKey(name), "Application verification name conflicts {0}", name);
            _verifications[name] = verification;
        }
        #endregion
    }
}