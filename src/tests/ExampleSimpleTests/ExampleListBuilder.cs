﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ExampleSimpleTests
{
    class ExampleListBuilder
    {
        public static IEnumerable<string>  GetExampleList(string folder)
        {
            return from file in Directory.GetFiles(folder)
                   where System.IO.Path.GetExtension(file) == ".bat"
                   select file;
        }

        public static void WriteToCSV(IEnumerable<string> files, string csvPath)
        {
            using (var csv = new FileStream(csvPath, FileMode.Create))
            {
                using (var os = new StreamWriter(csv))
                {
                    foreach (var file in files)
                    {
                        os.WriteLine(file);
                    }
                }
            }
        }
    }
}
