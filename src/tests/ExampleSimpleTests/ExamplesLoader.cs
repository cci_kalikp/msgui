﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace ExampleSimpleTests
{
    class ExamplesLoader
    {
        
        private ExamplesLoader()
        {
        }
       
        public static string GetColumnData(DataRow data, string column, string defaultValue)
        {
            var result = defaultValue;
            try
            {
                var name = data[column];
                if (!(name is DBNull))
                {
                    result = (string)data[column];
                }
            }
            catch (ArgumentException ex)
            {
                data.ClearErrors();
            }
            return result.Trim();
        }
      
        public static ApplicationRunBook LoadRunbook(string filePath)
        {
            if(!File.Exists(filePath))
                return new ApplicationRunBook();

            var deserializer = new XmlSerializer(typeof(ApplicationRunBook));
            TextReader reader = new StreamReader(filePath);
            var obj = deserializer.Deserialize(reader);
            var runbook = (ApplicationRunBook)obj;
            reader.Close();

            return runbook;

        }

    }
}
