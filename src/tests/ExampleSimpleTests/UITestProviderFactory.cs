﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExampleSimpleTests
{
    public class UITestProviderFactory
    {
        private IDictionary<string, UITestProvider> _providers = new Dictionary<string, UITestProvider>();
       
        public IDictionary<string, UITestProvider> Providers {get { return _providers; }}

        
        public  UITestProviderFactory()
        {
            RegisterProvider<SingleUITestProvider>("SimpleCWTest");
            RegisterProvider<NonUITestProvider>("NonUITest");
        }

        public void RegisterProvider<T>(string category) where T:UITestProvider 
        {
            Assert.IsFalse(_providers.ContainsKey(category));
            _providers[category] = (T)Activator.CreateInstance(typeof (T));
        }
    }

    class NonUITestProvider : UITestProvider
    {
        #region Overrides of UITestProvider

        protected override void StartApplications()
        {
            
        }

        protected override void PlaybackBehavior()
        {
            
        }

        protected override void VerifyVisual()
        {
            
        }

        protected override void VerifyResults()
        {
            
        }

        protected override void QuitApplications()
        {
            
        }

        public override string TestName
        {
            get { return TestConfiguration.TestName; }
        }

        

        public override void TestCleanUp()
        {
            
        }

        #endregion
    }
}