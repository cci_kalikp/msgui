import zipfile
import os.path
import os
import sys

import subprocess



def extractProj(source, tarRoot):
    if not os.path.exists(source) or not os.path.isfile(source):
        raise Exception('source file dose not exist:', source)
    
    fullname = os.path.basename(source)
    filename, ext = os.path.splitext(fullname)
    
    if ext != '.zip':
        raise Exception('soucre file must be zip file', ext)
    
    if len(sys.argv) >=4: 
        target = sys.argv[2]
    else:
        target = filename
    
    base = os.path.curdir
    target = os.path.join(base, tarRoot, target)
    
    if os.path.exists(target) and os.path.isfile(target):
        raise Exception('target must be a folder')
    
    
    zfile = zipfile.ZipFile(source)
    for name in zfile.namelist():
        (dirname, filename) = os.path.split(name)
        root = os.path.join(target, dirname)        
        if not os.path.exists(root):
            os.makedirs(root)
        zfile.extract(name, target)
    zfile.close()
    
    return os.path.relpath(target, tarRoot)


def _getProjectFile(folder):    
    for f in os.listdir(folder):
        fp = os.path.join(folder, f)
        if os.path.isfile(fp):            
            _, ext = os.path.splitext(f)            
            if ext == '.csproj':
                return f

def _getExe(folder, name):
    print 'name', name
    sub = os.path.join(folder, 'bin', 'debug')
        
    default = os.path.relpath(os.path.join(sub, str(name + '.exe')), folder)
    actual = os.path.join(folder, default)
    if os.path.exists(actual):    
        return os.path.relpath(actual, folder)
    
    for f in os.listdir(sub):
        fp = os.path.join(sub, f)
        if os.path.isfile(fp):
            _, ext = os.path.splitext(fp)
            if ext == '.exe':
                print 'result', os.path.relpath(fp, folder)
                return os.path.relpath(fp, folder)
            
            
def _findMsbuild():
    sysRoot = os.environ['SYSTEMROOT']
    return os.path.join(sysRoot, 'Microsoft.NET', 'Framework', 'v4.0.30319', 'msbuild.exe')
    
    

def main():
    source = sys.argv[1]
    tarRoot = sys.argv[2]
    
    sub = extractProj(source, tarRoot)
    folder = os.path.join(tarRoot, sub)
    proj = _getProjectFile(folder)
    name, _ = os.path.splitext(proj)
    msbuild = _findMsbuild()
    errorCode = subprocess.call([msbuild, os.path.join(folder, proj), '/property:Configuration=Debug'])
    
    
    if errorCode != 0:
        raise Exception("Build failed", proj)
    
    exe = _getExe(folder, name)        
    
    
    #os.system(' '.join(['start', os.path.join(folder, exe), '-env:DEV']))
    print os.path.abspath(os.path.join(folder, exe)) #must be the last print out

try:
    main()
except:
    print >> sys.stderr, sys.exc_info()[0]
    print >> sys.stderr, sys.exc_info()[1]
    exit(1)
    pass
