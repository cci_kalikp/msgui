﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace ExampleSimpleTests
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    [DeploymentItem("ZipFileList.csv")]
    [DeploymentItem(@"Utils\callpython.bat", "Utils")]
    [DeploymentItem(@"Utils\CompileZipHelper.py", "Utils")]

    public class CompileAndRunTests
    {
        private UITestProvider _testProvider;
        private static UITestProviderFactory _uiTestProviderFactory = new UITestProviderFactory();
        private const string SourceFolder = @"\\v\campus\ln\cs\msdotnet_build\msdotnet\codewiki_build_QA\static\example";
        private static string temp;

        private bool _skipTesting;

        private static HashSet<string> _testedExecutables = new HashSet<string>();

        [ClassInitialize]
        public static void SetupTests(TestContext testContext)
        {
            temp = System.IO.Path.Combine(System.Environment.GetEnvironmentVariable("TMP"), "CUIT");
        }

        public CompileAndRunTests()
        {

        }

        [TestMethod, Timeout(132*10*60*1000)] //10min for each row. 
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
            "|DataDirectory|\\ZipFileList.csv",
            "ZipFileList#csv",
            DataAccessMethod.Sequential)
        ]
        public void CompileAndRun()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
            if (!_testedExecutables.Contains(_testProvider.TestName))
            {

                _skipTesting = false;
                _testedExecutables.Add(_testProvider.TestName);
                _testProvider.RunTest();
            }
            else
            {
                _skipTesting = true;
            }

        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
            //var exampleInfo = _examplesLoader.LoadExampleInfo(TestContext.DataRow);
            var data = TestContext.DataRow;
            var zipPath = Path.Combine(SourceFolder, (string)data["Path"]);
            string fullpath = null;
            
            
            var process = new Process();
            process.StartInfo = new ProcessStartInfo()
                                    {
                                        FileName = @"Utils\callpython.bat",
                                        Arguments = @"Utils\CompileZipHelper.py" + " " + zipPath + " " + temp,
                                        RedirectStandardError = true,
                                        RedirectStandardOutput = true,
                                        UseShellExecute = false
                                    };
            
            var error = "";
            var extra = "";
            
            process.OutputDataReceived += (s,e) =>
                                              {
                                                  if (!string.IsNullOrEmpty(e.Data))
                                                  {
                                                      extra += e.Data;
                                                      fullpath = e.Data;
                                                  }
                                                  
                                              };

            process.ErrorDataReceived += (s, e) =>
                                             {
                                                 error += e.Data;
                                             };


            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
            
            Assert.IsTrue(string.IsNullOrEmpty(error), error);
            Assert.AreEqual(0, process.ExitCode,extra);

            process.Close();

            Assert.IsTrue(File.Exists(fullpath), "file not exist: " + fullpath);

            var root = Path.GetDirectoryName(fullpath);
            var isMSDesktopApp = false;
            foreach (var f in Directory.GetFiles(root))
            {
                if (Path.GetFileName(f) == "MSDotNet.MSGui.Core.dll")
                {
                    isMSDesktopApp = true;
                    break;
                }
            }

            var exampleInfo = new TestConfiguration()
            {
                TestProviderName = isMSDesktopApp ? "SimpleCWTest" : "NonUITest",   //todo: support Dnaprts
                TestName = Path.GetFileName(fullpath),
                FullPath = fullpath,
                LoginProfile = ExamplesLoader.GetColumnData(data, "LoginProfile", ""),
                CommandLine = ExamplesLoader.GetColumnData(data, "CommandLine", "-env:DEV"),
            };

            _testProvider = _uiTestProviderFactory.Providers[exampleInfo.TestProviderName];

            _testProvider.SetupTest(exampleInfo, TestContext);
        }

        ////Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {        
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
            if (!_skipTesting)
            {
                _testProvider.TestCleanUp();
            }
            else
            {
                Assert.IsTrue(_skipTesting, "Skipped");    
            }
            
            
           
        }


        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
