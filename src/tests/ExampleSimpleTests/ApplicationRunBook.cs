﻿using System.Xml.Serialization;
using Microsoft.VisualStudio.TestTools.UITest.Common.UIMap;
using Microsoft.VisualStudio.TestTools.UITesting;

namespace ExampleSimpleTests
{
    public class TestConfiguration
    {
        public string TestName { get; set; }
        public string FullPath { get; set; }
        public string CommandLine { get; set; }
        public string TestProviderName { get; set; }
        public string LoginProfile { get; set; }
    }

    [XmlRoot("Runbook", IsNullable = false)]
    public class ApplicationRunBook
    {   
        public string Layout { get; set; }
        public string Playback { get; set; }
        public bool HasQuitDialog { get; set; }
        public string QuitBehavior { get; set; }
        public bool SkipVisualTest { get; set; }
        public string VerifyBehavior { get; set; }
        
        public ApplicationRunBook()
        {
            //Default values
            Playback = CommonBehaviors.NoActions.ToString();
            HasQuitDialog = true;
            SkipVisualTest = true;  //TODO:  set to false when visual test become stable
            QuitBehavior = CommonBehaviors.QuitApplicationNotSavingLayout.ToString();
            VerifyBehavior = CommonBehaviors.NoneVerification.ToString();
        }
    }
}