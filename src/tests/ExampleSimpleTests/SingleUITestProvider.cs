﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSDesktop.CUIT.Common;

namespace ExampleSimpleTests
{

    enum CommonBehaviors
    {
        QuitApplicationNotSavingLayout, ExpectExceptions, NoneVerification, NoActions
    }

    public class SingleUITestProvider: UITestProvider
    {
       
        private UIApplication _app;
        
        
        private UIAppMainWindow _mainWindow;

        

        public UIApplication Application { get { return _app; } }

        

        public override string TestName
        {
            get { return TestConfiguration.TestName; }
        }

        public virtual ApplicationRunBook ApplicationRunBook { get; set; }


        public SingleUITestProvider()
        {
            RegisterCommonBehaviors();
        }

        protected override void StartApplications()
        {
            var procStartInfo = new ProcessStartInfo()
                                    {
                                        FileName = TestConfiguration.FullPath,
                                        Arguments = TestConfiguration.CommandLine
                                    };
            Process = Process.Start(procStartInfo);

            if (!string.IsNullOrEmpty(TestConfiguration.LoginProfile))
            {
                Thread.Sleep(5000);
                ApplicationHelper.LoginWithProfile(TestConfiguration.LoginProfile);
            }
            
            Thread.Sleep(5000);
            Assert.IsFalse(Process.HasExited, "Crashed at startup " + TestName);
            if(!ApplicationHelper.WaitforSplahwindowDone(50000))
            {
                TestContext.WriteLine(string.Format("Splash window didn't close in {0}ms for {1}",50000, TestName));
            }
            Thread.Sleep(2000);
            Assert.IsFalse(Process.HasExited, "Crashed at startup " + TestName);
            var apps = ApplicationHelper.GetApplications();

            
            Assert.IsTrue(apps.Count() >= 1, "Cannot start process {0} in time", TestName);
            Assert.AreEqual(1, apps.Count(), "{0} Simple test runs one applications only", TestName);
            _app = apps.First();
            _app.SetToPlaybackMode();

            _mainWindow = _app.MainWindow;

            Process = _mainWindow.CurrentProcess;
            Assert.IsNotNull(Process, "Cannot get main window process for {0}", TestName);
            

            
            var dir = Path.GetDirectoryName(GetProcessFullPath(Process.Id));
            var config = Path.Combine(dir, "CUIT", "default.config");
            
            ApplicationRunBook = ExamplesLoader.LoadRunbook(config);

            if (!string.IsNullOrEmpty(ApplicationRunBook.Layout))
            {
                if (_app.ShellMode == ShellMode.Ribbon)
                {
                    ShellWindowHelper.LoadLayout(_mainWindow, ApplicationRunBook.Layout);
                }
                else
                {
                    //TODO: 
                }
            }
        }

        protected override void PlaybackBehavior()
        {
            Assert.IsTrue(Behaviors.ContainsKey(ApplicationRunBook.Playback),
                          "Unsupported quit playback {0} for {1}", ApplicationRunBook.Playback, TestName);
            Behaviors[ApplicationRunBook.Playback](this);
        }


        protected override void VerifyVisual()
        {
            if(ApplicationRunBook.SkipVisualTest)
                return;

            foreach (var viewId in _mainWindow.GetDockedViewIds())
            {
                var pane = ViewsHelper.GetCustomContentPaneControl(_mainWindow, viewId);
                pane.AssertImageEqual(0.10, TestName);
            }
            foreach (var viewId in _mainWindow.GetFloatingViewIds())
            {
                var toolWindow = ViewsHelper.GetCustomToolWindowControl(_mainWindow, viewId);
                toolWindow.AssertImageEqual(0.10, TestName);
            }
            _mainWindow.AssertImageEqual(0.10, TestName);
        }

        protected override void VerifyResults()
        {
            Assert.IsTrue(Verifications.ContainsKey(ApplicationRunBook.VerifyBehavior),
                          "Unsupported quit playback {0} for {1}", ApplicationRunBook.VerifyBehavior, TestName);
            Verifications[ApplicationRunBook.VerifyBehavior](this);
        }

        protected override void QuitApplications()
        {
            Assert.IsTrue(QuitBehaviors.ContainsKey(ApplicationRunBook.QuitBehavior),
                          "Unsupported quit playback {0} for {1}", ApplicationRunBook.QuitBehavior, TestName);
            QuitBehaviors[ApplicationRunBook.QuitBehavior](this);
        }


        private void RegisterCommonBehaviors()
        {

            Action<UITestProvider> quitAppWithQuitDialog_notSaving
                = (provider) =>
                      {
                          var testProvider = (SingleUITestProvider) provider;
                          var app = testProvider.Application;

                          var mainWindow = app.MainWindow;
                          if (app.ShellMode == ShellMode.Ribbon)
                          {
                              ShellWindowHelper.ClickMenuItem(mainWindow, "Exit");
                          }
                          else
                          {
                              LauncherBarWindowHelper.ClickMenuItem(mainWindow,"Exit");
                          }

                          if (ApplicationRunBook.HasQuitDialog)
                          {

                              var quitDialog = app.GetTaskDialog();
                              quitDialog.WaitForControlExist(2000);
                              quitDialog.ClickButton("Quit");
                              Assert.IsTrue(mainWindow.CurrentProcess.WaitForExit(20000),
                                            "Application not quite in time for {0}", provider.TestName);
                          }
                          else
                          {
                              Assert.IsTrue(mainWindow.CurrentProcess.WaitForExit(20000),
                                            "Application not quite in time for {0}", provider.TestName);
                          }

                      };

            RegisterQuitBehavior(CommonBehaviors.QuitApplicationNotSavingLayout.ToString(),
                                 quitAppWithQuitDialog_notSaving);

            RegisterQuitBehavior(CommonBehaviors.NoActions.ToString(), (x)=> { });

            Action<UITestProvider> expectExceptions
                = (provider) =>
                      {
                          var testProvider = (SingleUITestProvider) provider;
                          var app = testProvider.Application;
                          var dialog = app.GetTaskDialog();
                          dialog.ClickButton("Terminate");
                          app.MainWindow.CurrentProcess.WaitForExit(10000);
                      };
            RegisterBehavior(CommonBehaviors.ExpectExceptions.ToString(), expectExceptions);

            RegisterBehavior(CommonBehaviors.NoActions.ToString(), (x) => { });

            RegisterVerification(CommonBehaviors.NoneVerification.ToString(), (x) => { });


        }

        private string GetProcessFullPath(int processId)
        {
            var wmiQueryString = "SELECT ProcessId, ExecutablePath, CommandLine FROM Win32_Process";
            using (var searcher = new ManagementObjectSearcher(wmiQueryString))
            using (var results = searcher.Get())
            {
                var res = (from mo in results.Cast<ManagementObject>()
                            where  (int) (uint) mo["ProcessId"] == processId
                            select new{   
                                           Path = (string) mo["ExecutablePath"],
                                           CommandLine = (string) mo["CommandLine"],
                                       }).FirstOrDefault();
                return res.Path;
            }
        }
    }
}