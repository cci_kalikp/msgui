﻿using System;
using System.Diagnostics;
using System.Windows.Automation;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;

//using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MSDesktop.CUIT.Extension
{
    public class CustomContentPaneControl: WpfControl
    {
        public new const string ClassName = "Uia.CustomContentPane";
        public CustomContentPaneControl(UITestControl owner):base(owner)
        {
            Trace.WriteLine("Create CustomPaneControl: " + owner);
            SearchProperties.Add(CustomContentPaneControl.PropertyNames.ClassName, ClassName);
        }



        public CustomContentPaneControl()
        {
            Trace.WriteLine("Create CustomPaneControl");
        }

        

        public virtual string ViewId
        {
            get
            {
                try
                {
                    Trace.WriteLine("Get ViewId From Control");

                   return (string)this.GetProperty(CustomContentPaneControl.PropertyNames.ViewId);
                }
                catch (Exception e)
                {
                    Trace.WriteLine("Exception when get property from control");

                    return "Blah";
                }
            }
        }

       /* public virtual string Value
        {
            get
            {
                try
                {
                    Trace.WriteLine("Get Peer value From Control");
                    var ae = this.NativeElement as AutomationElement;
                    var ip = ae.GetCurrentPattern(ValuePattern.Pattern) as ValuePattern;
                    if (ip != null)
                    {
                        
                        var val = ip.Current.Value;
                        Trace.WriteLine("Get Peer value From Control result:" + val);
                        return val;
                    }
                        
                    
                }
                catch (Exception e)
                {
                    Trace.WriteLine("Get peer Value: " + e.Message);
                }
                throw new NotSupportedException();
            }
        }*/

        new public abstract class PropertyNames : WpfControl.PropertyNames
        {
            public static readonly string ViewId = "ViewId";
        }
    }
}
