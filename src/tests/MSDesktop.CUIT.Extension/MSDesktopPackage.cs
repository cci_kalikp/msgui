﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UITest.Common;
using Microsoft.VisualStudio.TestTools.UITest.Common.UIMap;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using MSDesktop.CUIT.Extension;

[assembly: UITestExtensionPackage("MSDesktopPackage",
           typeof(MSDesktopPackage))]

namespace MSDesktop.CUIT.Extension
{
    public class MSDesktopPackage:UITestExtensionPackage
    {
        private readonly MSDesktopPropertyProvider _msDesktopPropertyProvider = new MSDesktopPropertyProvider();
        #region Overrides of UITestExtensionPackage

      

        public override object GetService(Type serviceType)
        {
            Trace.WriteLine("Service Type: " + serviceType.Name);
           
            if (serviceType == typeof(UITestPropertyProvider))
            {
                return _msDesktopPropertyProvider;
            }

            return null;
        }

        

        

        public override void Dispose()
        {
            //UITest.Saving -= OnUITestSave;
        }

        public override string PackageName
        {
            get { return "CUIT Extension For MSDesktop"; }
        }

        public override string PackageDescription
        {
            get { return "CUIT Extension For MSDesktop --- first example"; }
        }

        public override Version PackageVersion
        {
            get { return new Version(0, 0, 0, 0);}
        }

        public override string PackageVendor
        {
            get { return "MSDesktop Team"; }
        }

        public override Version VSVersion
        {
            get { return new Version(10, 0); }
        }

        #endregion
    }
}
