﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Automation;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;

namespace MSDesktop.CUIT.Extension
{
    public class ShellWindowControl : WpfWindow
    {
        public new const string ClassName = "Uia.ShellWindow";
        public new const string AutomationId = "Window_MSDesktop_Main_Window_ShellWindow";
        public ShellWindowControl(UITestControl owner)
            : base(owner)
        {
            Trace.WriteLine("Construct Shell window control <br/>");
        }

        private AutomationElement _automationElement;

        private AutomationElement AutomationElement
        {
            get
            {
                if (_automationElement == null)
                {
                    _automationElement = AutomationElement.RootElement.FindFirst(
                        TreeScope.Children | TreeScope.Element,
                        new PropertyCondition(AutomationElement.AutomationIdProperty,
                            "MSDesktop_Main_Window_ShellWindow",
                            PropertyConditionFlags.IgnoreCase));
                }

                if (_automationElement == null)
                    throw new ElementNotAvailableException("Cannot find the application ");

                return _automationElement;
            }
        }

        public ShellWindowControl()
            : base()
        {
            if (this.SearchProperties.Contains(ShellWindowControl.PropertyNames.Name))
            {
                var name = this.SearchProperties[ShellWindowControl.PropertyNames.Name];
                this.SearchProperties.Remove(ShellWindowControl.PropertyNames.Name);
                this.SearchProperties.Add(ShellWindowControl.PropertyNames.Name, name, PropertyExpressionOperator.Contains);   
            }
           
        }

        public string GetLastViewId()
        {
            var ae = this.AutomationElement;
            var vp = ae.GetCurrentPattern(ValuePattern.Pattern) as ValuePattern;
            if(vp == null)
                throw new NotSupportedException("GetLastViewId");
            vp.SetValue("GetLastCreatedView");
            return vp.Current.Value;
        }


        public IEnumerable<string> GetAllViewIds()
        {
            var ae = this.AutomationElement;
            var vp = ae.GetCurrentPattern(ValuePattern.Pattern) as ValuePattern;
            vp.SetValue("GetAllViews");

            return vp.Current.Value.Split(';').Where((id) => !string.IsNullOrEmpty(id));
        }

        public IEnumerable<string> GetFloatingViewIds()
        {
            var ae = this.AutomationElement;
            var vp = ae.GetCurrentPattern(ValuePattern.Pattern) as ValuePattern;
            vp.SetValue("GetFloatingViews");
            return vp.Current.Value.Split(';').Where((id) => !string.IsNullOrEmpty(id));
        }

        public IEnumerable<string> GetDockedViewIds()
        {
            var ae = this.AutomationElement;
            var vp = ae.GetCurrentPattern(ValuePattern.Pattern) as ValuePattern;
            vp.SetValue("GetDockedViews");
            return vp.Current.Value.Split(';').Where((id) => !string.IsNullOrEmpty(id));
        }

        public new object GetProperty(string name)
        {
            Trace.WriteLine("Get name for Shell window: " + name + "<br/>");
            return base.GetProperty(name);
        }

        public void SetToPlaybackMode()
        {
            var ae = this.AutomationElement;

            var ip = ae.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
            ip.Invoke();
        }
    }



}
