﻿using System;
using System.Windows.Automation;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;

namespace MSDesktop.CUIT.Extension
{
    internal static class Utilities
    {
        /// <summary>
        /// Throws ArgumentNullException if the parameter value is null.
        /// </summary>
        /// <param name="parameterName">The name of the parameter.</param>
        /// <param name="parameterValue">The value of the parameter.</param>
        public static void IfNullThrow(string parameterName, object parameterValue)
        {
            if (parameterValue == null)
            {
                throw new ArgumentNullException(parameterName);
            }
        }

      
/*
       
      
/// <summary>
        /// Gets the original Uia element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>The Uia element.</returns>
        public static IUITechnologyElement GetUiaElement(IUITechnologyElement element)
        {
            WrappedTechnologyElement te = element as WrappedTechnologyElement;
            if (te != null)
            {
                return te.InnerElement;
            }

            return null;
        }*/

        /// <summary>
        /// Checks if this element is from Uia technology manager.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>True if this is Uia technology element, false otherwise.</returns>
        public static bool IsUiaElement(IUITechnologyElement element)
        {
            return element != null &&
                   string.Equals(element.TechnologyName, UiaTechnologyName, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Gets the UIA control from UIA.NUD control by copying.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns>The copied UIA control.</returns>
        public static UITestControl GetCopiedUiaControl(UITestControl control)
        {
            UITestControl wpfControl = new UITestControl();
            wpfControl.CopyFrom(control);
            wpfControl.TechnologyName = Utilities.UiaTechnologyName;
            return wpfControl;
        }

        /// <summary>
        /// Gets the UIA control from UIA.NUD control by using the live AutomationElement.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns>The live UIA control.</returns>
        public static UITestControl GetLiveUiaControl(UITestControl control)
        {
          /*  UITechnologyElement element = control.GetProperty(UITestControl.PropertyNames.UITechnologyElement) as UITechnologyElement;
            if (element != null)
            {
                AutomationElement ae = element.NativeElement as AutomationElement;
                if (ae != null)
                {
                    return UITestControlFactory.FromNativeElement(ae, Utilities.UiaTechnologyName);
                }
            }

            throw new InvalidOperationException();*/

            var ae = control.NativeElement as AutomationElement;
            if (ae != null)
            {
                return UITestControlFactory.FromNativeElement(ae, Utilities.UiaTechnologyName);
            }
            throw new InvalidOperationException();
        }

        /// <summary>
        /// Technology name for UIA.
        /// </summary>
        public static readonly string UiaTechnologyName = "UIA";

        /// <summary>
        /// Technology name for this extension.
        /// </summary>
        public static readonly string TechnologyName = "UIA.MSDesktop";
    }
}
