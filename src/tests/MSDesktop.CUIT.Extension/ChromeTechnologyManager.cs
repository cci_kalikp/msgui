﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;

namespace MSDesktop.CUIT.Extension
{
    [ComVisible(true)]
    public class ChromeTechnologyManager : UITechnologyManager
    {

        private UITechnologyManager _innerManager;
        /// <summary>
        /// Gets the UIA technology manager that this plugin is wrapping.
        /// </summary>
        internal UITechnologyManager InnerManager
        {
            get
            {
                if (_innerManager == null)
                {
                    _innerManager = Playback.GetCoreTechnologyManager("UIA");
                }
                
                

                return _innerManager;
            }
        }


        /// <summary>
        /// Gets the appropriate technology element - either the original or
        /// the NumericUpDown child element in this technology.
        /// </summary>
        /// <param name="element">The original element.</param>
        /// <returns></returns>
        internal IUITechnologyElement GetElement(IUITechnologyElement element)
        {
            return new WrappedTechnologyElement(this, element);
        }



        #region Overrides of UITechnologyManager

        /// <summary>
        /// Performs any initialization required by this technology manager for starting a session.
        /// </summary>
        /// <param name="recordingSession">true if this is a recording session; false for any other kind of session, such as playback.</param>
        public override void StartSession(bool recordingSession)
        {
            Trace.WriteLine("Start ChromeTechnologyManager");
            InnerManager.StartSession(recordingSession);
        }

        /// <summary>
        /// Performs any cleanup required by this technology manager for stopping the current session.
        /// </summary>
        public override void StopSession()
        {
            Trace.WriteLine("Stop ChromeTechnologyManager");
            InnerManager.StopSession();
        }

        /// <summary>
        /// Returns an element from this technology that corresponds to the provided native element.
        /// </summary>
        /// <returns>
        /// The element in the technology represented by this manager.
        /// </returns>
        /// <param name="nativeElement">An <see cref="T:System.Object"/> that represents a native element such as <see cref="T:Accessibility.IAccessible"/>.</param>
        public override IUITechnologyElement GetElementFromNativeElement(object nativeElement)
        {

            return InnerManager.GetElementFromNativeElement(nativeElement);
        }

        /// <summary>
        /// Returns an enumerator for the child elements within the given element.
        /// </summary>
        /// <returns>
        /// An enumerator for the collection of child elements.
        /// </returns>
        /// <param name="element">An <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement"/> object.</param><param name="parsedQueryIdCookie">The cookie of a previously parsed <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IQueryElement"/> to filter matching children using each element's <see cref="P:Microsoft.VisualStudio.TestTools.UITest.Extension.UITechnologyElement.QueryId"/> property.</param>
        public override IEnumerator GetChildren(IUITechnologyElement element, object parsedQueryIdCookie)
        {
            return InnerManager.GetChildren(element, parsedQueryIdCookie);
        }

        /// <summary>
        /// Retrieves the element identified by the provided window handle.
        /// </summary>
        /// <returns>
        /// The specified element.
        /// </returns>
        /// <param name="handle">A <see cref="T:System.IntPtr"/> that identifies an existing element.</param>
        public override IUITechnologyElement GetElementFromWindowHandle(IntPtr handle)
        {
            return InnerManager.GetElementFromWindowHandle(handle);
        }

        /// <summary>
        /// Retrieves the element located at the given screen coordinates.
        /// </summary>
        /// <returns>
        /// The element at the provided location.
        /// </returns>
        /// <param name="pointX">The x coordinate of the screen location.</param><param name="pointY">The y coordinate of the screen location.</param>
        public override IUITechnologyElement GetElementFromPoint(int pointX, int pointY)
        {
            var element  = InnerManager.GetElementFromPoint(pointX, pointY);
            return element;
        }

        /// <summary>
        /// Gets the parent of the given element in the UI hierarchy.
        /// </summary>
        /// <returns>
        /// The parent element.
        /// </returns>
        /// <param name="element">An <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement"/> object.</param>
        public override IUITechnologyElement GetParent(IUITechnologyElement element)
        {
            return InnerManager.GetParent(element);
        }

        /// <summary>
        /// Returns the next sibling of the given element in the UI hierarchy.
        /// </summary>
        /// <returns>
        /// The next sibling element.
        /// </returns>
        /// <param name="element">An <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement"/>.</param>
        public override IUITechnologyElement GetNextSibling(IUITechnologyElement element)
        {
            return InnerManager.GetNextSibling(element);
        }

        /// <summary>
        /// Returns the previous sibling of the given element in the UI hierarchy.
        /// </summary>
        /// <returns>
        /// The previous sibling element.
        /// </returns>
        /// <param name="element">An <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement"/>.</param>
        public override IUITechnologyElement GetPreviousSibling(IUITechnologyElement element)
        {
            return InnerManager.GetPreviousSibling(element);
        }

        /// <summary>
        /// Returns the windows pointer for the element that currently has the focus.
        /// </summary>
        /// <returns>
        /// The element that has the focus, or null if no element has the focus.
        /// </returns>
        /// <param name="handle">The <see cref="T:System.IntPtr"/> for the window that has the focus.</param>
        public override IUITechnologyElement GetFocusedElement(IntPtr handle)
        {
            return InnerManager.GetFocusedElement(handle);
        }

        /// <summary>
        /// Adds an event handler to this technology manager.
        /// </summary>
        /// <returns>
        /// true if the event type is supported and is successfully added; otherwise, false.
        /// </returns>
        /// <param name="element">The <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement"/> and its descendents for which this event should be raised.</param><param name="eventType">A member of the <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.UITestEventType"/> enumeration that specifies the type of event.</param><param name="eventSink">An <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITestEventNotify"/> that logs the events.</param>
        public override bool AddEventHandler(IUITechnologyElement element, UITestEventType eventType, IUITestEventNotify eventSink)
        {
            if (eventType == UITestEventType.MouseDown)
            {
                Trace.WriteLine("Mouse Down" + element.FriendlyName);
                Debugger.Break();
            }
            
            return InnerManager.AddEventHandler(element, eventType, eventSink);
        }

        /// <summary>
        /// Removes the specified event from the given element and all its descendents.
        /// </summary>
        /// <returns>
        /// true if the event was successfully removed; otherwise, false.
        /// </returns>
        /// <param name="element">An <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement"/> object.</param><param name="eventType">A member of the <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.UITestEventType"/> enumeration.</param><param name="eventSink">An <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITestEventNotify"/> object representing the registered event sink.</param>
        public override bool RemoveEventHandler(IUITechnologyElement element, UITestEventType eventType, IUITestEventNotify eventSink)
        {
            return InnerManager.RemoveEventHandler(element, eventType, eventSink);
        }

        /// <summary>
        /// Returns a <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IUISynchronizationWaiter"/> using the provided element and event type.
        /// </summary>
        /// <returns>
        /// The requested waiter.
        /// </returns>
        /// <param name="element">An <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement"/> object.</param><param name="eventType">A <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.UITestEventType"/> object for which a waiter is required.</param><exception cref="T:System.NotSupportedException">Throw only if this method is not supported by this technology manager. </exception>
        public override IUISynchronizationWaiter GetSynchronizationWaiter(IUITechnologyElement element, UITestEventType eventType)
        {
            return InnerManager.GetSynchronizationWaiter(element, eventType);
        }

        /// <summary>
        /// Searches for an element within the parent element that matches the condition of the provided query ID cookie.
        /// </summary>
        /// <returns>
        /// An array of elements that match the query, or null if none are found.
        /// </returns>
        /// <param name="parsedQueryIdCookie">A query cookie previously parsed with the <see cref="M:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyManager.ParseQueryId(System.String,System.Object@)"/> method.</param><param name="parentElement">An <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement"/> object.</param><param name="maxDepth">An int indicating how deep into the control hierarchy to search.</param><exception cref="T:System.NotSupportedException">This implementation does not support this method.In processing this exception, the testing framework uses the <see cref="M:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyManager.GetChildren(Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement,System.Object)"/> method to traverse the hierarchy, and uses the <see cref="M:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyManager.MatchElement(Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement,System.Object,System.Boolean@)"/> method on each element to determine if it matches the query.</exception>
        public override object[] Search(object parsedQueryIdCookie, IUITechnologyElement parentElement, int maxDepth)
        {
            return Search(parsedQueryIdCookie, parentElement, maxDepth);
        }

        /// <summary>
        /// Parses the provided string representation of a query element and returns a string representation of the query element as a cookie.
        /// </summary>
        /// <returns>
        /// The query element cookie as a string.
        /// </returns>
        /// <param name="queryElement">The string representation of a <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.QueryElement"/> object.</param><param name="parsedQueryIdCookie">An object representing the parsed query element to be used later in such methods as <see cref="M:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyManager.Search(System.Object,Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement,System.Int32)"/>, <see cref="M:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyManager.MatchElement(Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement,System.Object,System.Boolean@)"/>, or <see cref="M:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyManager.GetChildren(Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement,System.Object)"/>.</param>
        public override string ParseQueryId(string queryElement, out object parsedQueryIdCookie)
        {
            return InnerManager.ParseQueryId(queryElement, out parsedQueryIdCookie);
        }

        /// <summary>
        /// Returns whether the provided element matches the previously parsed query ID cookie.
        /// </summary>
        /// <returns>
        /// true if the element matches the query element; otherwise, false.
        /// </returns>
        /// <param name="element">An <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement"/> object.</param><param name="parsedQueryIdCookie">An <see cref="T:System.Object"/> representing the cookie of a previously parsed <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IQueryElement"/>.</param><param name="useEngine">(Output) A Boolean value that indicates whether this technology manager has chosen to use the testing framework for matching all or part of the query element.</param><exception cref="T:System.NotSupportedException">This method is not supported by this technology manager.</exception>
        public override bool MatchElement(IUITechnologyElement element, object parsedQueryIdCookie, out bool useEngine)
        {
            return InnerManager.MatchElement(element, parsedQueryIdCookie, out useEngine);
        }

        /// <summary>
        /// Returns information about the last test action invoked by this manager.
        /// </summary>
        /// <returns>
        /// Information about the last invocation.
        /// </returns>
        public override ILastInvocationInfo GetLastInvocationInfo()
        {
            return InnerManager.GetLastInvocationInfo();
        }

        /// <summary>
        /// Cancels any wait or search operation being performed by this technology manager because of calls to <see cref="M:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement.WaitForReady"/> or <see cref="M:Microsoft.VisualStudio.TestTools.UITest.Extension.UITechnologyManager.Search(System.Object,Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement,System.Int32)"/>.
        /// </summary>
        public override void CancelStep()
        {
            InnerManager.CancelStep();
        }

        /// <summary>
        /// Takes an element from another technology and returns an element compatible for the current technology along with the level of support it has for the converted element.
        /// </summary>
        /// <returns>
        /// The new element.
        /// </returns>
        /// <param name="elementToConvert">An <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITechnologyElement"/> object.</param><param name="supportLevel">(Output) The support level of the provided element in the new technology.</param>
        public override IUITechnologyElement ConvertToThisTechnology(IUITechnologyElement elementToConvert, out int supportLevel)
        {
            return InnerManager.ConvertToThisTechnology(elementToConvert, out supportLevel);
        }

        /// <summary>
        /// Returns this technology manager's indication of confidence that it can support the control identified with the provided handle.
        /// </summary>
        /// <returns>
        /// An int indicating the level of support.
        /// </returns>
        /// <param name="windowHandle">The window handle for a specific element.</param>
        public override int GetControlSupportLevel(IntPtr windowHandle)
        {
            return InnerManager.GetControlSupportLevel(windowHandle);

        }

        /// <summary>
        /// Gets the technology name.
        /// </summary>
        /// <returns>
        /// The name of the technology (i.e. HTML).
        /// </returns>
        public override string TechnologyName
        {
            get { return Utilities.TechnologyName; }
        }

        /// <summary>
        /// Adds a global event sink to this technology manager.
        /// </summary>
        /// <returns>
        /// true if the event type is supported and is successfully added; otherwise, false.
        /// </returns>
        /// <param name="eventType">A member of the <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.UITestEventType"/> enumeration that specifies the type of event.</param><param name="eventSink">An <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITestEventNotify"/> that logs the events.</param>
        public override bool AddGlobalEventHandler(UITestEventType eventType, IUITestEventNotify eventSink)
        {
            return InnerManager.AddGlobalEventHandler(eventType, eventSink);
        }

        /// <summary>
        /// Removes the specified event.
        /// </summary>
        /// <returns>
        /// true if the event was successfully removed; otherwise, false.
        /// </returns>
        /// <param name="eventType">A member of the <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.UITestEventType"/> enumeration.</param><param name="eventSink">An <see cref="T:Microsoft.VisualStudio.TestTools.UITest.Extension.IUITestEventNotify"/> object representing the registered event sink.</param>
        public override bool RemoveGlobalEventHandler(UITestEventType eventType, IUITestEventNotify eventSink)
        {
            return InnerManager.RemoveGlobalEventHandler(eventType, eventSink);
        }

        /// <summary>
        /// Processes the mouse enter event for the specified window or control.
        /// </summary>
        /// <param name="handle">A window handle.</param>
        public override void ProcessMouseEnter(IntPtr handle)
        {
            InnerManager.ProcessMouseEnter(handle);
        }

        #endregion
    }
}
