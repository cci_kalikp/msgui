﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Automation;
using Microsoft.VisualStudio.TestTools.UITest.Common;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
using ControlType = Microsoft.VisualStudio.TestTools.UITesting.ControlType;

namespace MSDesktop.CUIT.Extension
{
    class MSDesktopPropertyProvider : UITestPropertyProvider
    {
        #region Overrides of UITestPropertyProvider

        private UITestPropertyProvider _corePropertyProvider;

        private bool _isInThisProvider;

        public override ICollection<string> GetPropertyNames(UITestControl uiTestControl)
        {
            return GetAllPropertiesMap(uiTestControl).Keys;
        }

        public override UITestPropertyDescriptor GetPropertyDescriptor(UITestControl uiTestControl, string propertyName)
        {
            Trace.WriteLine("Property descriptor for " + propertyName + "<br/>");
            var map = GetAllPropertiesMap(uiTestControl);
            if (map.ContainsKey(propertyName))
            {
                return map[propertyName];
            }
            return null;
        }

        public override object GetPropertyValue(UITestControl uiTestControl, string propertyName)
        {
            //Trace.WriteLine("Property value for " + propertyName + " of " + uiTestControl.GetType().FullName + "<br/>");


            if (ExtraPropertiesMap.ContainsKey(propertyName) &&
              (ExtraPropertiesMap[propertyName].Attributes & UITestPropertyAttributes.Readable) == UITestPropertyAttributes.Readable)
            {
                var result = GetPropertyValueInternal(uiTestControl, propertyName);
                Trace.WriteLine("Out Property Value is: " + result + "<br/>");
                
                return result;
            }



            _isInThisProvider = true;
            try
            {
                var cc = GetCopiedControl(uiTestControl);
                
                var val =  cc.GetProperty(propertyName);
                Trace.WriteLine("Copied control " + cc.GetType().Name + " result " + val + " <br/>");
                return val;
            }
            catch (Exception e)
            {
                Trace.WriteLine("Exception in GetPropertyValue: " + e.Message + "<br/>");
            }
            finally
            {
                _isInThisProvider = false;
            }

            return null;
        }

        public override void SetPropertyValue(UITestControl uiTestControl, string propertyName, object value)
        {

          //  Trace.WriteLine("set Property value for " + propertyName + "<br/>");

            _isInThisProvider = true;
            try
            {
                GetCopiedControl(uiTestControl).SetProperty(propertyName, value);
            }
            finally
            {
                _isInThisProvider = false;
            }

        }

        public override int GetControlSupportLevel(UITestControl uiTestControl)
        {


            //throw new NotImplementedException();

            try
            {
                if (!_isInThisProvider && IsSupported(uiTestControl))
                {
                    //Trace.WriteLine("customized support: " + uiTestControl.ControlType + "<br/>");
                    return (int)ControlSupport.ControlSpecificSupport;
                }


            }
            catch (Exception e)
            {
                Trace.WriteLine("Error: " + e.Message);
            }
           /* if(!_isInThisProvider)
                Trace.WriteLine("no customized support: " + uiTestControl.ControlType + "<br/>");
            else
                Trace.WriteLine("In this, but to use core for" + uiTestControl.GetType().Name + "<br/>");
*/
            return (int)ControlSupport.NoSupport;

        }

        public override string GetPropertyForAction(UITestControl uiTestControl, UITestAction action)
        {
            Trace.WriteLine("GetPropertyForAction ");
            throw new NotSupportedException();
        }

        public override string[] GetPropertyForControlState(UITestControl uiTestControl, ControlStates uiState, out bool[] stateValues)
        {
            Trace.WriteLine("GetPropertyForControlState ");
            stateValues = null;
            return null;

        }

        public override Type GetPropertyNamesClassType(UITestControl uiTestControl)
        {
            Trace.WriteLine("GetPropertyForControlState ");

            if (string.Equals(uiTestControl.SearchProperties["ClassName"], CustomContentPaneControl.ClassName))
                return typeof(CustomContentPaneControl.PropertyNames);
            if (string.Equals(uiTestControl.SearchProperties["ClassName"], CustomToolWindowControl.ClassName))
                return typeof(CustomToolWindowControl.PropertyNames);

          /*  if (uiTestControl.ControlType == ControlType.Window)
                return typeof (ShellWindowControl.PropertyNames);*/

            throw new NotSupportedException();
        }

        public override Type GetSpecializedClass(UITestControl uiTestControl)
        {
            //Trace.WriteLine("GetSpecializedClass for " + uiTestControl.ControlType + "<br/>");
            if (string.Equals(uiTestControl.SearchProperties["ClassName"], CustomContentPaneControl.ClassName))
                return typeof(CustomContentPaneControl);
            if (string.Equals(uiTestControl.SearchProperties["ClassName"], CustomToolWindowControl.ClassName))
                return typeof(CustomToolWindowControl);

         /*   if (uiTestControl.ControlType == ControlType.Window)
                return typeof (ShellWindowControl);*/

            throw new NotSupportedException(uiTestControl.SearchProperties["ClassName"]);
        }

        public override string[] GetPredefinedSearchProperties(Type specializedClass)
        {
            Trace.WriteLine("GetPredefinedSearchProperties ");
            
            /*if(specializedClass == typeof(CustomPaneControl))
                return new string[] {CustomPaneControl.PropertyNames.ViewId};*/

            if (specializedClass == typeof(CustomContentPaneControl))
                return new string[] { CustomContentPaneControl.PropertyNames.ClassName };

            if (specializedClass == typeof(CustomToolWindowControl))
                return new string[] { CustomToolWindowControl.PropertyNames.ClassName };

         /*   if (specializedClass == typeof(ShellWindowControl))
                return new string[] {ShellWindowControl.PropertyNames.Name};*/

            

            return new string[] { UITestControl.PropertyNames.ControlType };


        }

        #endregion


        private static bool IsSupported(UITestControl uiTestControl)
        {
            Trace.WriteLine("testing if supported" + "<br/>");
            try
            {
               /* if (!string.Equals(uiTestControl.TechnologyName, "UIA", StringComparison.OrdinalIgnoreCase))
                    return false;*/

                if (string.Equals(uiTestControl.TechnologyName, "UIA", StringComparison.OrdinalIgnoreCase) &&
                            uiTestControl.ControlType
                            == ControlType.Custom
                            && string.Equals(uiTestControl.SearchProperties["ClassName"], CustomContentPaneControl.ClassName, StringComparison.OrdinalIgnoreCase))
                    return true;


                if (string.Equals(uiTestControl.TechnologyName, "UIA", StringComparison.OrdinalIgnoreCase) &&
                            uiTestControl.ControlType
                            == ControlType.Custom
                            && string.Equals(uiTestControl.SearchProperties["ClassName"], CustomToolWindowControl.ClassName, StringComparison.OrdinalIgnoreCase))
                    return true;

                //Trace.WriteLine("IsSupport: " + uiTestControl.ControlType + ", " + uiTestControl.ClassName + " top " + uiTestControl.IsTopParent + uiTestControl.TechnologyName + "<br/>" );

              /*  if (string.Equals(uiTestControl.TechnologyName, "UIA", StringComparison.OrdinalIgnoreCase) &&
                    uiTestControl.SearchProperties[WpfControl.PropertyNames.ClassName].Contains("HwndWrapper") 
                    // && uiTestControl.ControlType == ControlType.Window  
                    )
                    return true;*/


                return false;


            }
            catch (Exception e)
            {
                Trace.WriteLine("Error in testing: " + e.Message + "<br/>");
            }
            return false;
        }

        public static UITestControl GetCopiedControl(UITestControl control)
        {
            UITestControl winControl = new UITestControl();
            winControl.CopyFrom(control);
            return winControl;
        }


        private static Dictionary<string, UITestPropertyDescriptor> _allPropertiesMap;


        private static Dictionary<string, UITestPropertyDescriptor> GetAllPropertiesMap(UITestControl control)
        {
            if (_allPropertiesMap == null)
            {
                _allPropertiesMap = new Dictionary<string, UITestPropertyDescriptor>(StringComparer.OrdinalIgnoreCase);

                bool initializedByMe = false;
                if (!Playback.IsInitialized)
                {
                    Playback.Initialize();
                    initializedByMe = true;
                }

                try
                {
                    UITestPropertyProvider provider = Playback.GetCorePropertyProvider(control);
                    foreach (var property in provider.GetPropertyNames(control))
                    {
                        var descriptor = provider.GetPropertyDescriptor(control, property);
                        _allPropertiesMap.Add(property, descriptor);
                    }
                }
                finally
                {
                    if (initializedByMe)
                    {
                        Playback.Cleanup();
                    }
                }

                foreach (var item in ExtraPropertiesMap)
                {
                    _allPropertiesMap.Add(item.Key, item.Value);
                }
            }

            return _allPropertiesMap;
        }

        private static Dictionary<string, UITestPropertyDescriptor> _extraPropertiesMap;
        private static Dictionary<string, UITestPropertyDescriptor> ExtraPropertiesMap
        {
            get
            {
                if (_extraPropertiesMap == null)
                {
                    _extraPropertiesMap = new Dictionary<string, UITestPropertyDescriptor>(StringComparer.OrdinalIgnoreCase);
                    _extraPropertiesMap.Add(CustomContentPaneControl.PropertyNames.ViewId,
                        new UITestPropertyDescriptor(typeof(string), UITestPropertyAttributes.Readable | UITestPropertyAttributes.DoNotGenerateProperties));

                    _extraPropertiesMap.Add(CustomToolWindowControl.PropertyNames.ViewIds,
                        new UITestPropertyDescriptor(typeof(ICollection<string>), UITestPropertyAttributes.Readable | UITestPropertyAttributes.DoNotGenerateProperties));

                }

                return _extraPropertiesMap;
            }
        }

        private object GetPropertyValueInternal(UITestControl uiTestControl, string propertyName)
        {

            try
            {
                Trace.WriteLine("GetPropertyValueInternal for " + propertyName + " of " + uiTestControl.GetType().Name + "<br/>");


                if (uiTestControl.GetType().FullName == typeof(CustomToolWindowControl).FullName && string.Equals(propertyName, CustomToolWindowControl.PropertyNames.ViewIds))
                {
                    var a = (AutomationElement)(uiTestControl.NativeElement);
                    return a.Current.ItemStatus;
                }

                if (uiTestControl.GetType().FullName == typeof(CustomContentPaneControl).FullName && string.Equals(propertyName, CustomContentPaneControl.PropertyNames.ViewId))
                {
                    var a = (AutomationElement) (uiTestControl.NativeElement);
                    return a.Current.AutomationId.Substring("contentPane".Length);
                }
                
                throw new NotSupportedException();

            }
            catch (Exception e)
            {
                Trace.WriteLine("GetPropertyValueInternal: " + e.TargetSite + " " + e.Message + "<br/>");
                throw e;
            }

        }

    }
}
