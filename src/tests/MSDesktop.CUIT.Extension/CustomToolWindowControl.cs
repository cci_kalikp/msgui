﻿using System.Diagnostics;
using System.Windows.Automation;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;

namespace MSDesktop.CUIT.Extension
{
    public class CustomToolWindowControl: WpfControl
    {
        public new const string ClassName = "Uia.CustomPaneToolWindow";
        public CustomToolWindowControl(UITestControl parent)
            : base(parent)
        {
            SearchProperties.Add(CustomToolWindowControl.PropertyNames.ClassName, ClassName);
        }

        public CustomToolWindowControl()
        {
            
        }

        

        public virtual WpfControl PaneHeader { get { return null; } }
        
        public virtual string ViewIds
        {
            get
            {
                Trace.WriteLine("Get ViewIds From Control");
                return (string)this.GetProperty(CustomToolWindowControl.PropertyNames.ViewIds);
               
            }
        }

        new public abstract class PropertyNames : WpfCustom.PropertyNames
        {
            public static readonly string ViewIds = "ViewIds";
        }
    }
}
