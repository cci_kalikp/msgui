﻿using System;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices.TestingCommon.Entities
{
    public class UnserializableTestEntityQuery : IQuery<UnserializableTestEntity>, IEquatable<UnserializableTestEntityQuery>
    {
        private readonly string _key;

        public UnserializableTestEntityQuery(string key)
        {
            _key = key;
        }

        public bool Equals(IQuery<UnserializableTestEntity> other)
        {
            return Equals(other as UnserializableTestEntityQuery);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as UnserializableTestEntityQuery);
        }

        public bool Equals(UnserializableTestEntityQuery other)
        {
            if (other == null)
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            if (_key == null)
            {
                return other._key == null;
            }
            return _key.Equals(other._key);
        }

        public override int GetHashCode()
        {
            return (_key != null ? _key.GetHashCode() : 0);
        }

        public static bool operator ==(UnserializableTestEntityQuery left, UnserializableTestEntityQuery right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(UnserializableTestEntityQuery left, UnserializableTestEntityQuery right)
        {
            return !Equals(left, right);
        }
    }
}