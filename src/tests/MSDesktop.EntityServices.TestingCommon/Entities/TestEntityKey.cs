﻿using System;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices.TestingCommon.Entities
{
    [Serializable]
    public class TestEntityKey : BasicKey<TestEntity>, IForceFetch
    {
        public TestEntityKey(string key)
            : base(key)
        {
        }

        public bool ForceFetch { get; set; }
    }
}