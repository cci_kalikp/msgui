﻿using System;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices.TestingCommon.Entities
{
    public class TestEntityQuery : IQuery<TestEntity>, IEquatable<TestEntityQuery>
    {
        private readonly string _key;

        public TestEntityQuery(string key)
        {
            _key = key;
        }

        public bool Equals(IQuery<TestEntity> other)
        {
            return Equals(other as TestEntityQuery);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as TestEntityQuery);
        }

        public bool Equals(TestEntityQuery other)
        {
            if (other == null)
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            if (_key == null)
            {
                return other._key == null;
            }
            return _key.Equals(other._key);
        }

        public override int GetHashCode()
        {
            return (_key != null ? _key.GetHashCode() : 0);
        }

        public static bool operator ==(TestEntityQuery left, TestEntityQuery right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(TestEntityQuery left, TestEntityQuery right)
        {
            return !Equals(left, right);
        }
    }
}