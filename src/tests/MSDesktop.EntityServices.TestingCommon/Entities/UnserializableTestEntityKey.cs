﻿using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices.TestingCommon.Entities
{
    public class UnserializableTestEntityKey : BasicKey<UnserializableTestEntity>, IForceFetch
    {
        public UnserializableTestEntityKey(string key)
            : base(key)
        {
        }

        public bool ForceFetch { get; set; }
    }
}