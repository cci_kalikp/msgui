﻿using System;
using MorganStanley.Desktop.EntityServices.Common;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices.TestingCommon.Entities
{
    public class UnserializableTestEntity : IEntity<BasicKey<UnserializableTestEntity>>, IPublishable, IEquatable<UnserializableTestEntity>, ISubscribable, IFetchable, ITimestampedEntity, IVersionable, IDeletable
    {
        private readonly UnserializableTestEntityKey _key;

        public UnserializableTestEntity(UnserializableTestEntityKey key)
        {
            _key = key;
        }

        public UnserializableTestEntity() : this(new UnserializableTestEntityKey("testkey"))
        {
        }

        IKey IEntity.Key
        {
            get { return ((IEntity<BasicKey<UnserializableTestEntity>>)this).Key; }
        }

        BasicKey<UnserializableTestEntity> IEntity<BasicKey<UnserializableTestEntity>>.Key
        {
            get { return _key; }
        }

        public bool Equals(UnserializableTestEntity other)
        {
            throw new NotImplementedException();
        }

        DateTime ITimestampedEntity.Timestamp { get; set; }

        public DateTime? ReferenceDate
        {
            get { return null; }
        }

        DateTime IVersionable.Timestamp
        {
            get { throw new NotImplementedException(); }
        }
    }
}