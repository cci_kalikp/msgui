﻿using System;

namespace MorganStanley.Desktop.EntityServices.TestingCommon
{
    public class ExceptionTester
    {
        private Exception _exc;

        public void Act(Action method)
        {
            try
            {
                method();
            }
            catch (Exception exc)
            {
                _exc = exc;
            }
        }

        public void ShouldMatchExceptionMessage(string message)
        {
            _exc.Message.ShouldEqual(message);
        }

        public void ShouldThrow<T>()
        {
            typeof(T).IsAssignableFrom(_exc.GetType()).ShouldBeTrue(String.Format("The expected type was {0} and the actual type was {1}", typeof(T).FullName, _exc.GetType().FullName));
        }

        public void ShouldThrow<T>(string message)
        {
            _exc.ShouldNotBeNull();
            typeof(T).IsAssignableFrom(_exc.GetType()).ShouldBeTrue(String.Format("The expected type was {0} and the actual type was {1}", typeof(T).FullName, _exc.GetType().FullName));
            ShouldMatchExceptionMessage(message);
        }

        public void ShouldContainInnerException(Exception expectedInnerException)
        {
            _exc.InnerException.ShouldEqual(expectedInnerException);
        }

    }
}
