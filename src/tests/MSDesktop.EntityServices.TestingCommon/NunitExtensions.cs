﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace MorganStanley.Desktop.EntityServices.TestingCommon
{
    public static class NunitExtensions
    {

        public static void ShouldBeNull(this object actual)
        {
            Assert.That(actual, Is.Null);
        }

        public static void ShouldNotBeNull(this object actual)
        {
            Assert.That(actual, Is.Not.Null);
        }

        public static void ShouldEqual(this object actual, object expected)
        {
            Assert.That(actual, Is.EqualTo(expected));
        }

        public static void ShouldNotContain<T>(this IEnumerable<T> collection, T expected)
        {
            Assert.That(collection.Contains(expected), Is.False);
        }

        public static void ShouldContain<T>(this IEnumerable<T> collection, T expected)
        {
            Assert.That(collection.Contains(expected), Is.True);
        }

        public static void ShouldContain(this string actual, string expected)
        {
            Assert.That(actual.Contains(expected), Is.True);
        }

        public static void ShouldNotContain(this string actual, string expected)
        {
            Assert.That(actual.Contains(expected), Is.False);
        }

        public static void ShouldBeLessThan(this object actual, object expected)
        {
            Assert.That(actual, Is.LessThan(expected));
        }

        public static void ShouldBeGreaterThan(this object actual, object expected)
        {
            Assert.That(actual, Is.GreaterThan(expected));
        }

        public static void ShouldBeTrue(this bool actual)
        {
            Assert.IsTrue(actual);
        }

        public static void ShouldBeTrue(this bool actual, string msg)
        {
            Assert.IsTrue(actual, msg);

        }

        public static void ShouldBeFalse(this bool actual)
        {
            Assert.IsFalse(actual);
        }

        public static void ShouldBeFalse(this bool actual, string msg)
        {
            Assert.IsFalse(actual, msg);
        }

    }

}
