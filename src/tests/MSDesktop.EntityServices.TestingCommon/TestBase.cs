﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MorganStanley.Desktop.EntityServices.Common.Threading;

namespace MorganStanley.Desktop.EntityServices.TestingCommon
{
    public abstract class TestBase
    {
        protected static void Run(Action callback)
        {
            Task.Factory.StartNew(callback, CancellationToken.None, TaskCreationOptions.None, new CurrentThreadTaskScheduler());
        }

        protected static void Wait(WaitHandle handle, int timeout = 10000)
        {
            if (!handle.WaitOne(timeout))
            {
                throw new InvalidOperationException("Timeout exceeded waiting for WaitHandle");
            }
        }

        protected static void Wait(Task handle, int timeout = 10000)
        {
            if (!handle.Wait(timeout))
            {
                throw new InvalidOperationException("Timeout exceeded waiting for Task");
            }
        }
    }
}
