﻿using NUnit.Framework;

namespace MorganStanley.Desktop.EntityServices.TestingCommon
{
    [TestFixture]
    public abstract class BasicContext
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            PreArrange();
            Arrange();
            Act();
        }

        public virtual void PreArrange() { }
        protected abstract void Arrange();
        protected abstract void Act();
    }
}
