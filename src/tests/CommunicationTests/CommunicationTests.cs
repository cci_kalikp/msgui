﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace CommunicationTests
{
    /// <summary>
    /// Summary description for CommunicationTests
    /// </summary>
    [CodedUITest]
    public class CommunicationTests
    {
        public CommunicationTests()
        {
            System.Diagnostics.Process.Start(@"..\..\..\..\build\common\ms\csc\Debug\examples\CommunicationExamples\CommunicationExamples.exe");
        }

        [TestMethod]
        public void M2MPubSubTestMethod()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
            this.UIMap.RecordedTypeMessage();
            this.UIMap.AssertM2MReceivedMessage();
            this.UIMap.RecordedQuitApp();
        }

        [TestMethod]
        public void V2VAutoHookIslandNotReceivingTest()
        {
            this.UIMap.RecordedV2vCreatePublisher();
            this.UIMap.RecordedV2vCreateSubscriber();
            this.UIMap.RecordedV2vSendMsg1();
            this.UIMap.AssertV2vNoMessageReceived();
            this.UIMap.RecordedQuitApp();
        }

        [TestMethod]
        public void V2VConnectToReceiveTest()
        {
            this.UIMap.RecordedV2vCreatePublisher();
            this.UIMap.RecordedV2vCreateSubscriber();
            this.UIMap.RecordedV2vConnectPubSub();
            this.UIMap.AssertV2vConnected();
            this.UIMap.RecordedV2vSaveConnection();
            this.UIMap.RecordedV2vSendMsg2();
            this.UIMap.AssertV2vMessageReceived();
            this.UIMap.RecordedQuitApp();
        }

        [TestMethod]
        public void V2VDisconnectFromConnectedAndNotReceivingTest()
        {
            this.UIMap.RecordedV2vCreatePublisher();
            this.UIMap.RecordedV2vCreateSubscriber();
            this.UIMap.RecordedV2vConnectPubSub();
            this.UIMap.AssertV2vConnected();
            this.UIMap.RecordedV2vSaveConnection();

            this.UIMap.RecordedV2vDisconnectFromConnected();
            this.UIMap.AssertV2vDisconnected();
            this.UIMap.RecordedV2vSaveConnection();

            this.UIMap.RecordedV2vSendMsg4();
            this.UIMap.AssertV2vNoMessageReceived();
            this.UIMap.RecordedQuitApp();
        }
        
        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //    // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //    // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        public UIMap UIMap
        {
            get
            {
                if ((this.m_map == null))
                {
                    this.m_map = new UIMap();
                }

                return this.m_map;
            }
        }

        private UIMap m_map;
    }
}
