call "%VS100COMNTOOLS%vsvars32.bat"
set AddAspectBatchGeneratorCode=%cd%\assemblies\MSDesktop.AspectProvider\AddAspectBatchGenerator.cs
set Root=%cd%
pushd "%cd%\..\install\common\assemblies\" 
xcopy /R /Y "%POSTSHARP20%Release\PostSharp.dll" .
csc /define:TRACE /reference:MSDesktop.AspectProvider.dll /out:AddAspectBatchGenerator.exe %AddAspectBatchGeneratorCode% /optimize+ /debug-
AddAspectBatchGenerator.exe AddAspectCore.bat
call AddAspectCore.bat
DEL /S /Q AddAspectCore.bat 
DEL /S /Q AddAspectBatchGenerator.*

REM no need to do this if PostSharp.dll is available on some static place on \\ms\dist
mkdir ..\3rd
xcopy /R /Y PostSharp.dll  ..\3rd\

DEL /S /Q PostSharp.dll
popd

pushd "%cd%\..\install\common\assemblies\.debug\"  
xcopy /R /Y "%POSTSHARP20%Release\PostSharp.dll" .
csc /define:DEBUG;TRACE /reference:MSDesktop.AspectProvider.dll /out:AddAspectBatchGenerator.exe %AddAspectBatchGeneratorCode% /debug+ /debug:full /optimize-
AddAspectBatchGenerator.exe AddAspectCore.bat
call AddAspectCore.bat
DEL /S /Q AddAspectCore.bat 
DEL /S /Q AddAspectBatchGenerator.*
DEL /S /Q PostSharp.dll
popd

 