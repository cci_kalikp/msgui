﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;
using MorganStanley.MSDotNet.MSGui.Themes.Resources;

namespace MorganStanley.MSDotNet.MSGui.Themes
{
    public class WhiteTheme : Theme
    {
        public const string DefaultThemeName = "White";
        public static ImageSource DefaultIcon;

        static WhiteTheme()
        {
            DefaultIcon = new BitmapImage(new Uri(@"pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/Theme_White.png"));
            DefaultIcon.Freeze();
        }

        //used to make sure custom theme overridden can still take effect
        private int lastMergedDictionaryCount;
        public WhiteTheme()
        {
            Source =
                new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/ApplicationStyleWhite.xaml",
                        UriKind.RelativeOrAbsolute);
            lastMergedDictionaryCount = this.MergedDictionaries.Count;
            Icon = DefaultIcon;
            Name = DefaultThemeName;
        }

        protected override void InitializeCore(ShellMode shellMode_)
        { 
            if (lastMergedDictionaryCount < MergedDictionaries.Count)
            {
                lastMergedDictionaryCount = MergedDictionaries.Count; //user removed some rd
            }
            int start = 0;
            if (shellMode_ != ShellMode.LauncherBarAndFloatingWindows)
            {
                var resource = new ResourceDictionary()
                {
                    Source =
                        new Uri(
                            "pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/InfragisticsControlsStyleWhite.xaml",
                            UriKind.RelativeOrAbsolute)
                };
                foreach (var mergedResource in resource.MergedDictionaries)
                {
                    this.MergedDictionaries.Insert(start++, mergedResource);
                    lastMergedDictionaryCount++;
                }
            }
            if (!DockViewModel.UseHarmonia)
            {
                this.MergedDictionaries.Insert(start, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/InfragisticsDockManagerStyleWhite.xaml",
                        UriKind.RelativeOrAbsolute)
                });
                 
                this.MergedDictionaries.Insert(lastMergedDictionaryCount ++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/DockPanelsStyleDict.xaml",
                        UriKind.RelativeOrAbsolute)
                });
                this.MergedDictionaries.Insert(lastMergedDictionaryCount ++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/ViewContainerHeaderControlStyle.xaml",
                        UriKind.RelativeOrAbsolute)
                });
            }
            else
            {
                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/DockManagerColorPaletteWhite.xaml",
                        UriKind.RelativeOrAbsolute)
                });
                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/DockManager.xaml",
                        UriKind.RelativeOrAbsolute)
                });
                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/DockManagerShared.xaml",
                        UriKind.RelativeOrAbsolute)
                });
            }
            if (shellMode_ == ShellMode.LauncherBarAndFloatingWindows ||
                shellMode_ == ShellMode.LauncherBarAndWindow)
            {
                var xamPagerDict = new ResourceDictionary
                {
                    Source =
                        new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/XamPager.xaml",
                                UriKind.RelativeOrAbsolute)
                };
                xamPagerDict.MergedDictionaries.Add(new ResourceDictionary
                {
                    Source =
                        new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/XamPagerWhiteBrushes.xaml",
                                UriKind.RelativeOrAbsolute)
                });
                this.MergedDictionaries.Add(xamPagerDict); 

            }
        } 
    }
}
