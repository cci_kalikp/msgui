﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/themes/MSDotNet.MSGui.Themes/ThemeCreator/ThemeCreator.cs#1 $
// $Change: 898612 $
// $DateTime: 2014/09/28 23:36:02 $
// $Author: caijin $

using System; 
using System.Collections.Generic;
using System.ComponentModel; 
using System.Reflection;
using System.Windows;
using System.Windows.Media; 
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Application;  
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.UserPreference;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions; 
using MorganStanley.MSDotNet.MSGui.Themes.MSDesktop2014;
using Application = System.Windows.Application;

namespace MorganStanley.MSDotNet.MSGui.Themes
{
    public static class ThemeCreator
    {
  
        [Obsolete("create an instance of WhiteTheme instead")]
        public static Theme CreateWhiteTheme()
        { 
           return new WhiteTheme();  
        }

        [Obsolete("create an instance of SimpleTheme instead")]
        public static Theme CreateSimpleTheme()
        { 
            return new SimpleTheme();  
        }


        [Obsolete("create an instance of BlackTheme instead")]
        public static Theme CreateBlackTheme()
        { 
            return new BlackTheme();  
        }

        [Obsolete("create an instance of BlueTheme instead")]
        public static Theme CreateBlueTheme(Framework framework)
        {
            return CreateBlueTheme();
        }

        [Obsolete("create an instance of BlueTheme instead")]
        public static Theme CreateBlueTheme()
        { 
            return new BlueTheme();
        }
         

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void AddBackgroundModifiedBlueTheme(this Framework framework_)
        { 
            framework_.AddTheme(new BackgroundModifiedBlueTheme());
        }

        public static Theme CreateModernTheme(Framework f, Color accentColor, ModernThemeBase baseColor,
                                              ModernThemeOptions options)
        {
            return CreateModernTheme(accentColor, baseColor, options);
        }

        public static Theme CreateModernTheme(Color accentColor, ModernThemeBase baseColor, ModernThemeOptions options, bool setAsStartupTheme=true)
        {
            var theme = new ModernTheme(baseColor, accentColor, options);
            if (setAsStartupTheme)
            {
                Application.Current.Resources.MergedDictionaries.Add(theme);
                ModernThemeHelper.IsModernThemeEnabled = true;
            } 
 
            return theme;
        }
         
        public static void AddWhiteTheme(this Framework framework_)
        {
            framework_.AddTheme(new WhiteTheme());
        }

        public static void AddSimpleTheme(this Framework framework_)
        {
            framework_.AddTheme(new SimpleTheme());
        }

        public static void AddBlueTheme(this Framework framework_)
        {
            framework_.AddTheme(new BlueTheme());
        }
        public static void AddBlackTheme(this Framework framework_)
        {
            framework_.AddTheme(new BlackTheme());
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void AddModernTheme(this Framework f, Color accentColour, ModernThemeBase baseColour)
        {
            AddModernTheme(f, accentColour, baseColour, new ModernThemeOptions());
        } 

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void AddModernTheme(this Framework f, Color accentColour, ModernThemeBase baseColour, ModernThemeOptions options)
        { 
            f.AddTheme(CreateModernTheme(f, accentColour, baseColour, options));

            //if (Debugger.IsAttached)
            //{
            //    f.AddModule<ModernThemeConfigModule>();
            //}
        }
         
        public static void AddSimpleTheme(this Framework framework_, bool addDefaultControlStyles_)
        {
            framework_.AddTheme(new SimpleTheme(addDefaultControlStyles_));
        }
        public static void AddBlueTheme(this Framework framework_, bool addDefaultControlStyles_)
        {
            framework_.AddTheme(new BlueTheme(addDefaultControlStyles_));
        }
        public static void AddBlackTheme(this Framework framework_, bool addDefaultControlStyles_)
        {
            framework_.AddTheme(new BlackTheme(addDefaultControlStyles_));
        }
          

        public static void AddConfigurationDependentTheme(this Framework framework_, string default_,  Color? modernThemeAccentColor_ = null)
        {
            framework_.Bootstrapper.ConfigurationDependentTheme = true;
            if (!string.IsNullOrEmpty(default_))
            {
                UserPreferenceInitialSettings.DefaultTheme = default_;
            }
            framework_.Bootstrapper.ConfigurationDependentThemeDetermined += (sender_, args_) =>
            {
                var themeName = string.IsNullOrWhiteSpace(args_.Value1) ? default_ : args_.Value1;
                switch (themeName)
                {
                    case BlackTheme.DefaultThemeName:
                        framework_.AddBlackTheme();
                        break;
                    case WhiteTheme.DefaultThemeName:
                        framework_.AddWhiteTheme();
                        break;
                    case SimpleTheme.DefaultThemeName:
                        framework_.AddSimpleTheme();
                        break;
                    case BlueTheme.DefaultThemeName:
                        framework_.AddBlueTheme();
                        break;
                    case  BackgroundModifiedBlueTheme.DefaultThemeName:
                        framework_.AddBackgroundModifiedBlueTheme();
                        break;
                    case  ModernTheme.DarkThemeName:
                        if (modernThemeAccentColor_ == null)
                        {
                            throw new ArgumentNullException("modernThemeAccentColor_", "modernThemeAccentColor_ must be specified to enable modern theme");
                        }
                        framework_.AddModernTheme(modernThemeAccentColor_.Value, ModernThemeBase.Dark); 

                        break;
                    case  ModernTheme.LightThemeName:
                        if (modernThemeAccentColor_ == null)
                        {
                            throw new ArgumentNullException("modernThemeAccentColor_", "modernThemeAccentColor_ must be specified to enable modern theme");
                        }
                        framework_.AddModernTheme(modernThemeAccentColor_.Value, ModernThemeBase.Light);

                        break;

                }
            };
        }

        private static readonly Dictionary<string, Theme> customThemes = new Dictionary<string, Theme>(); 
        public static void AddConfigurationDependentTheme(this Framework framework_, string default_, bool userConfigurable_, IEnumerable<Theme> customThemes_, bool includeSystemThemes_=true, Color? modernThemeAccentColor_=null, params string[] systemThemes_)
        {
            framework_.Bootstrapper.ConfigurationDependentTheme = true;
            Bootstrapper.ThemeUserConfigurable = userConfigurable_;
            if (!string.IsNullOrEmpty(default_))
            { 
                UserPreferenceInitialSettings.DefaultTheme = default_;
            }
            if (customThemes_ != null)
            {
                foreach (var customTheme in customThemes_)
                {
                    framework_.Bootstrapper.AddAvailableTheme(customTheme.Name, customTheme.Icon);
                    customThemes[customTheme.Name] = customTheme;
                }
            } 
            if (includeSystemThemes_)
            {
                IList<string> systemThemes = new List<string>(systemThemes_);
                if (systemThemes_ == null || systemThemes.Contains(SimpleTheme.DefaultThemeName))
                {
                    framework_.Bootstrapper.AddAvailableTheme(SimpleTheme.DefaultThemeName, SimpleTheme.DefaultIcon);
                } 
                if (systemThemes_ == null || systemThemes.Contains(BlackTheme.DefaultThemeName))
                {
                    framework_.Bootstrapper.AddAvailableTheme(BlackTheme.DefaultThemeName, BlackTheme.DefaultIcon);
                }
                if (systemThemes_ == null || systemThemes.Contains(BlueTheme.DefaultThemeName))
                {
                    framework_.Bootstrapper.AddAvailableTheme(BlueTheme.DefaultThemeName, BlueTheme.DefaultIcon);
                } 
                if (systemThemes_ == null || systemThemes.Contains(WhiteTheme.DefaultThemeName))
                {
                    framework_.Bootstrapper.AddAvailableTheme(WhiteTheme.DefaultThemeName, BlueTheme.DefaultIcon);
                }
                if (systemThemes_ == null || systemThemes.Contains(BackgroundModifiedBlueTheme.DefaultThemeName))
                {
                    framework_.Bootstrapper.AddAvailableTheme(BackgroundModifiedBlueTheme.DefaultThemeName, BlueTheme.DefaultIcon);
                }
                if (systemThemes_ == null || systemThemes.Contains(ModernTheme.DarkThemeName))
                {
                    framework_.Bootstrapper.AddAvailableTheme(ModernTheme.DarkThemeName, ModernTheme.DarkThemeIcon);
                }
                if (systemThemes_ == null || systemThemes.Contains(ModernTheme.LightThemeName))
                {
                    framework_.Bootstrapper.AddAvailableTheme(ModernTheme.LightThemeName, ModernTheme.LightThemeIcon);
                }
            } 

            framework_.Bootstrapper.ConfigurationDependentThemeDetermined += (sender_, args_) =>
            {
                var themeName = string.IsNullOrWhiteSpace(args_.Value1) ? default_ : args_.Value1;
                switch (themeName)
                {
                    case BlackTheme.DefaultThemeName:
                        framework_.AddBlackTheme();
                        break;
                    case WhiteTheme.DefaultThemeName:
                        framework_.AddWhiteTheme();
                        break;
                    case SimpleTheme.DefaultThemeName:
                        framework_.AddSimpleTheme();
                        break;
                    case BlueTheme.DefaultThemeName:
                        framework_.AddBlueTheme();
                        break;
                    case BackgroundModifiedBlueTheme.DefaultThemeName:
                        framework_.AddBackgroundModifiedBlueTheme();
                        break;
                    case ModernTheme.DarkThemeName:
                        if (modernThemeAccentColor_ == null)
                        {
                            throw new ArgumentNullException("modernThemeAccentColor_", "modernThemeAccentColor_ must be specified to enable modern theme");
                        }
                        framework_.AddModernTheme(modernThemeAccentColor_.Value, ModernThemeBase.Dark);

                        break;
                    case ModernTheme.LightThemeName:
                        if (modernThemeAccentColor_ == null)
                        {
                            throw new ArgumentNullException("modernThemeAccentColor_", "modernThemeAccentColor_ must be specified to enable modern theme");
                        }
                        framework_.AddModernTheme(modernThemeAccentColor_.Value, ModernThemeBase.Light);
                        break;
                    default:
                        if (string.IsNullOrEmpty(themeName)) return;
                        Theme customTheme;
                        if (customThemes.TryGetValue(themeName, out customTheme))
                        {
                            framework_.AddTheme(customTheme);
                        }
                        break;
                }
            };
        }

        public static void SetTextHightlight(Color color_)
        {
            var colorCache = (Color[])typeof(SystemColors).GetField("_colorCache", BindingFlags.NonPublic | BindingFlags.Static).GetValue(null);
            colorCache[14] = color_;
        }

        public static void SetWindowBackground(Color color_)
        {
            var colorCache = (Color[])typeof(SystemColors).GetField("_colorCache", BindingFlags.NonPublic | BindingFlags.Static).GetValue(null);
            colorCache[27] = color_;
        }

        public static Color GetWindowBackground()
        {
            var colorCache = (Color[])typeof(SystemColors).GetField("_colorCache", BindingFlags.NonPublic | BindingFlags.Static).GetValue(null);
            return colorCache[27];
        }

        public static Color GetTextHightlight()
        {
            var colorCache = (Color[])typeof(SystemColors).GetField("_colorCache", BindingFlags.NonPublic | BindingFlags.Static).GetValue(null);
            return colorCache[14];
        }
         
    }
}
