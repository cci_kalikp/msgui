﻿ 
using System.Windows;
using System.Windows.Media; 
using MorganStanley.MSDotNet.MSGui.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions; 

namespace MorganStanley.MSDotNet.MSGui.Themes
{
    public class BackgroundModifiedBlueTheme : BlueTheme
    {
        public new const string DefaultThemeName = "BackgroundModifiedBlue"; 
        public BackgroundModifiedBlueTheme() : base(false)
        {
            
        }
        protected override void InitializeCore(ShellMode shellMode_)
        {
            base.InitializeCore(shellMode_);
             

            foreach (ResourceDictionary resourceDictionary in this.MergedDictionaries)
            {
                foreach (var dictionary in resourceDictionary.MergedDictionaries)
                {
                    if (dictionary.Contains("MSGuiInternal_TextInputBackgroundBrushKey"))
                    {
                        dictionary["MSGuiInternal_TextInputBackgroundBrushKey"] =
                            new SolidColorBrush(new Color() { A = 255, R = 19, G = 32, B = 49 });
                    }
                    if (dictionary.Contains(MSGuiColors.TextInputBackgroundBrushKey))
                    {
                        dictionary[MSGuiColors.TextInputBackgroundBrushKey] = new SolidColorBrush(new Color() { A = 255, R = 19, G = 32, B = 49 });
                    }
                }
                if (resourceDictionary.Contains("MSGuiInternal_TextInputBackgroundBrushKey"))
                {
                    resourceDictionary["MSGuiInternal_TextInputBackgroundBrushKey"] =
                        new SolidColorBrush(new Color() { A = 255, R = 19, G = 32, B = 49 });
                }
                if (resourceDictionary.Contains(MSGuiColors.TextInputBackgroundBrushKey))
                {
                    resourceDictionary[MSGuiColors.TextInputBackgroundBrushKey] = new SolidColorBrush(new Color() { A = 255, R = 19, G = 32, B = 49 });
                }
            }
        }
    }
}
