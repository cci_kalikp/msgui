﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;
using MorganStanley.MSDotNet.MSGui.Themes.Resources;

namespace MorganStanley.MSDotNet.MSGui.Themes
{
    public class SimpleTheme : Theme
    {
        public const string DefaultThemeName = "Simple";
        public static ImageSource DefaultIcon;

        static SimpleTheme()
        {
            DefaultIcon = new BitmapImage(new Uri(@"pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/Theme_White.png"));
            DefaultIcon.Freeze();
        }

        //used to make sure custom theme overridden can still take effect
        private int lastMergedDictionaryCount;
        private readonly bool useThemedTextForeground;
        public SimpleTheme(bool useThemedTextForeground_ = false)
        {
            Source =
                new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/ApplicationStyleSimple.xaml",
                        UriKind.RelativeOrAbsolute);
            lastMergedDictionaryCount = this.MergedDictionaries.Count;
            Icon = DefaultIcon;
            Name = DefaultThemeName;
            useThemedTextForeground = useThemedTextForeground_;
        }

        protected override void SetDefaultValues()
        {
            if (Core.CoreUtilities.AreClose(ThemeExtensions.HeaderHeightDefault, 22.0))
                ThemeExtensions.HeaderHeightDefault = 14.0;
            ThemeExtensions.DefaultDockContentPadding = 2.0;
            ThemeExtensions.FloatingWindowBorderPadding = new Thickness(0.0);
            ThemeExtensions.ContentPaneFlashBorderThickness = new Thickness(1.0);
            ThemeExtensions.ContentPaneFlashBorderMargin = new Thickness(0.0);
            ThemeExtensions.FloatingWindowCaptionBarMargin = new Thickness(0);
            ThemeExtensions.ContentPaneMargin = new Thickness(0);
            ThemeExtensions.FloatingWindowBorderCornerRadius = new CornerRadius(2);
            ContentPaneWrapper.TabItemBorderCornerRadius = new CornerRadius(0);
            ThemeExtensions.TileItemBorderThickness = new Thickness(1);
        }

        protected override void InitializeCore(ShellMode shellMode_)
        { 
            if (lastMergedDictionaryCount < MergedDictionaries.Count)
            {
                lastMergedDictionaryCount = MergedDictionaries.Count; //user removed some rd
            }
            int start = 0;
            if (shellMode_ != ShellMode.LauncherBarAndFloatingWindows)
            {
                var resource = new ResourceDictionary()
                {
                    Source =
                        new Uri(
                            "pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/InfragisticsControlsStyleSimple.xaml",
                            UriKind.RelativeOrAbsolute)
                };
                foreach (var mergedResource in resource.MergedDictionaries)
                {
                    this.MergedDictionaries.Insert(start++, mergedResource);
                    lastMergedDictionaryCount++;
                }
            }
            if (!DockViewModel.UseHarmonia)
            {
                this.MergedDictionaries.Insert(lastMergedDictionaryCount ++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/SimpleDockPanelsStyleDict.xaml",
                        UriKind.RelativeOrAbsolute)
                });
                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/ViewContainerHeaderControlStyle.xaml",
                        UriKind.RelativeOrAbsolute)
                });
                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/InfragisticsDockManagerStyleSimple.xaml",
                        UriKind.RelativeOrAbsolute)
                }); 
                 
            }
            else
            {
                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/SimpleDockManagerColorPalette.xaml",
                        UriKind.RelativeOrAbsolute)
                });
                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/SimpleDockManager.xaml",
                        UriKind.RelativeOrAbsolute)
                });
                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/DockManagerShared.xaml",
                        UriKind.RelativeOrAbsolute)
                });
                 
            }
            //add it to end, to make it take effect always
            if (useThemedTextForeground)
            {
                this.MergedDictionaries.Add(new ResourceDictionary
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/StandardWindowsElementsLunaDetachable.xaml",
                                      UriKind.RelativeOrAbsolute)
                });
            }
            if (shellMode_ == ShellMode.LauncherBarAndFloatingWindows ||
                shellMode_ == ShellMode.LauncherBarAndWindow)
            {
                var xamPagerDict = new ResourceDictionary
                {
                    Source =
                        new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/XamPager.xaml",
                                UriKind.RelativeOrAbsolute)
                };
                xamPagerDict.MergedDictionaries.Add(new ResourceDictionary
                {
                    Source =
                        new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/XamPagerSimpleBrushes.xaml",
                                UriKind.RelativeOrAbsolute)
                });
                this.MergedDictionaries.Add(xamPagerDict); 

            }
        } 
    }
}
