﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;
using MorganStanley.MSDotNet.MSGui.Themes.Resources;

namespace MorganStanley.MSDotNet.MSGui.Themes
{
    public class BlueTheme : Theme
    {
        public const string DefaultThemeName = "Blue";
        public static ImageSource DefaultIcon;

        static BlueTheme()
        {
            DefaultIcon = new BitmapImage(new Uri(@"pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/Theme_Blue.png"));
            DefaultIcon.Freeze();
        }

        //used to make sure custom theme overridden can still take effect
        private int lastMergedDictionaryCount;
        Color oldTextSelectionColor = Colors.Blue;
        Color oldWindowBackgroundColor = Colors.White;
        private readonly bool useThemedTextForeground;
        public BlueTheme(bool useThemedTextForeground_=true) 
        {
            Source =
                new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/ApplicationStyleDarkBlue.xaml",
                        UriKind.RelativeOrAbsolute);
            lastMergedDictionaryCount = this.MergedDictionaries.Count;
            Icon = DefaultIcon;
            Name = DefaultThemeName;
            useThemedTextForeground = useThemedTextForeground_;
        }
         
        
        protected override void SetDefaultValues()
        {
            base.SetDefaultValues();
            oldTextSelectionColor = ThemeCreator.GetTextHightlight();
            ThemeCreator.SetTextHightlight(Color.FromArgb(0xFF, 0x00, 0x88, 0x88));

            //we set window background color to make default caret sign color white
            //if background color of a textbox is set to anything then this solution won't work
            oldWindowBackgroundColor = ThemeCreator.GetWindowBackground();
            ThemeCreator.SetWindowBackground(Colors.Black);
             
        }

        internal override void ClearDefaultValues()
        {
            ThemeCreator.SetTextHightlight(oldTextSelectionColor);
            ThemeCreator.SetWindowBackground(oldWindowBackgroundColor);
        }

        protected override void InitializeCore(ShellMode shellMode_)
        {
            if (lastMergedDictionaryCount < MergedDictionaries.Count)
            {
                lastMergedDictionaryCount = MergedDictionaries.Count; //user removed some rd
            }
            int start = 0;
            if (shellMode_ != ShellMode.LauncherBarAndFloatingWindows)
            {
                var resource = new ResourceDictionary()
                {
                    Source =
                        new Uri(
                            "pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/InfragisticsControlsStyleDarkBlue.xaml",
                            UriKind.RelativeOrAbsolute)
                };
                foreach (var mergedResource in resource.MergedDictionaries)
                {
                    this.MergedDictionaries.Insert(start++, mergedResource);
                    lastMergedDictionaryCount++;
                } 

            }
            if (!DockViewModel.UseHarmonia)
            {
                this.MergedDictionaries.Insert(start, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/InfragisticsDockManagerStyleDarkBlue.xaml",
                        UriKind.RelativeOrAbsolute)
                });

                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/DockPanelsStyleDict.xaml",
                        UriKind.RelativeOrAbsolute)
                });
                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/ViewContainerHeaderControlStyle.xaml",
                        UriKind.RelativeOrAbsolute)
                });
            }
            else
            {
                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/DockManagerColorPaletteDarkBlue.xaml",
                        UriKind.RelativeOrAbsolute)
                });
                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/DockManager.xaml",
                        UriKind.RelativeOrAbsolute)
                });
                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/DockManagerShared.xaml",
                        UriKind.RelativeOrAbsolute)
                });
            }

            //add it to end, to make it take effect always
            if (useThemedTextForeground)
            {
                this.MergedDictionaries.Add(new ResourceDictionary
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/StandardWindowsElementsLunaDetachable.xaml",
                                      UriKind.RelativeOrAbsolute)
                });
            }
            if (shellMode_ == ShellMode.LauncherBarAndFloatingWindows ||
                shellMode_ == ShellMode.LauncherBarAndWindow)
            {
                var xamPagerDict = new ResourceDictionary
                {
                    Source =
                        new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/XamPager.xaml",
                                UriKind.RelativeOrAbsolute)
                };
                xamPagerDict.MergedDictionaries.Add(new XamPagerDarkBlueBrushes());
                this.MergedDictionaries.Add(xamPagerDict);  
            }
        }
    }
}
