﻿using System;
using System.Collections.Generic;
using System.ComponentModel; 
using System.Windows;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Themes.ResourceWasher
{
    public class WashableResourceDictionary:ResourceDictionary, ISupportInitialize
    {
        // Fields 
        private float _hue; 
        private float _saturation; 
        private Color _washColor = Colors.Transparent;
        private WashGroupList _washGroups;
        private WashMode _washMode;

        public static readonly DependencyProperty IsExcludedFromWashProperty =
            DependencyProperty.RegisterAttached("IsExcludedFromWash", typeof (bool), typeof (WashableResourceDictionary),
                                                new FrameworkPropertyMetadata(false));

        public static readonly DependencyProperty WashGroupProperty = DependencyProperty.RegisterAttached("WashGroup",
                                                                                                          typeof (string
                                                                                                              ),
                                                                                                          typeof (
                                                                                                              WashableResourceDictionary
                                                                                                              ),
                                                                                                          new FrameworkPropertyMetadata
                                                                                                              (null));
         
        public void WashResources()
        {
            Dictionary<object,object> resources = new Dictionary<object, object>(); 
            DependencyObjectHelper.WalkDictionary(this, entry_ =>
                {
                    Brush b = entry_.Value as Brush;
                    resources[entry_.Key] = entry_.Value;
                    if (b == null) return; 
                    b = WashBrushHelper(b, GetWashGroup(b));
                    b.Freeze();   
                    resources[entry_.Key] = b;
                });

            this.Clear();
            this.MergedDictionaries.Clear();
            foreach (var resource in resources)
            {
                this[resource.Key] = resource.Value;
            }
              
        }


        private static int AdjustColorComponentValue(int startValue, int baseValue)
        {
            int num = (startValue*baseValue)/0xff;
            return (num + ((startValue*((0xff - (((0xff - startValue)*(0xff - baseValue))/0xff)) - num))/0xff));
        }

  

        private Brush CloneBrush(Brush sourceBrush)
        {
            return this.CloneBrush(sourceBrush, false, null);
        }

        private Brush CloneBrush(Brush sourceBrush, bool washColor, WashGroup group)
        {
            return sourceBrush.Clone();
        }

        private static Color ColorFromHLS(byte alpha, float hue, float luminance, float saturation)
        {
            int num;
            int num2;
            int num3;
            if (saturation == 0.0)
            {
                num = num2 = num3 = (int) (luminance*255.0);
            }
            else
            {
                float num5;
                if (luminance <= 0.5f)
                {
                    num5 = luminance + (luminance*saturation);
                }
                else
                {
                    num5 = (luminance + saturation) - (luminance*saturation);
                }
                float num4 = (2f*luminance) - num5;
                num = (int) ToRGBHelper(num4, num5, hue + 120f);
                num2 = (int) ToRGBHelper(num4, num5, hue);
                num3 = (int) ToRGBHelper(num4, num5, hue - 120f);
            }
            num = Math.Max(0, Math.Min(0xff, num));
            num2 = Math.Max(0, Math.Min(0xff, num2));
            num3 = Math.Max(0, Math.Min(0xff, num3));
            return Color.FromArgb(alpha, (byte) num, (byte) num2, (byte) num3);
        }

        private void CopyBrush(Brush sourceBrush, Brush destinationBrush)
        {
            this.CopyBrush(sourceBrush, destinationBrush, false, null);
        }

        private void CopyBrush(Brush sourceBrush, Brush destinationBrush, bool washColor, WashGroup group)
        {
            SolidColorBrush brush = sourceBrush as SolidColorBrush;
            SetWashGroup(destinationBrush, GetWashGroup(sourceBrush));
            SetIsExcludedFromWash(destinationBrush, GetIsExcludedFromWash(sourceBrush));
            destinationBrush.Opacity = sourceBrush.Opacity;
            if (brush != null)
            {
                SolidColorBrush brush2 = destinationBrush as SolidColorBrush;
                if (washColor)
                {
                    brush2.Color = this.WashColorHelper(brush.Color, group);
                }
                else
                {
                    brush2.Color = brush.Color;
                }
            }
            else
            {
                GradientBrush brush3 = sourceBrush as GradientBrush;
                if (brush3 != null)
                {
                    GradientBrush brush4 = destinationBrush as GradientBrush;
                    if (brush4 != null)
                    {
                        brush4.GradientStops.Clear();
                        GradientStopCollection gradientStops = brush3.GradientStops;
                        int count = gradientStops.Count;
                        for (int i = 0; i < count; i++)
                        {
                            GradientStop stop = gradientStops[i];
                            GradientStop stop2 = new GradientStop();
                            if (washColor)
                            {
                                stop2.Color = this.WashColorHelper(stop.Color, group);
                            }
                            else
                            {
                                stop2.Color = stop.Color;
                            }
                            stop2.Offset = stop.Offset;
                            brush4.GradientStops.Add(stop2);
                        }
                        if ((brush3 is LinearGradientBrush) && (destinationBrush is LinearGradientBrush))
                        {
                            LinearGradientBrush brush5 = (LinearGradientBrush) brush3;
                            LinearGradientBrush brush6 = (LinearGradientBrush) destinationBrush;
                            brush6.StartPoint = brush5.StartPoint;
                            brush6.EndPoint = brush5.EndPoint;
                        }
                        else if ((brush3 is RadialGradientBrush) && (destinationBrush is RadialGradientBrush))
                        {
                            RadialGradientBrush brush7 = (RadialGradientBrush) brush3;
                            RadialGradientBrush brush8 = (RadialGradientBrush) destinationBrush;
                            brush8.Center = brush7.Center;
                            brush8.RadiusX = brush7.RadiusX;
                            brush8.RadiusY = brush7.RadiusY;
                            brush8.GradientOrigin = brush7.GradientOrigin;
                        }
                    }
                }
            }
        }

       
        internal static float GetBrightness(Color color)
        {
            float num = ((float) color.R)/255f;
            float num2 = ((float) color.G)/255f;
            float num3 = ((float) color.B)/255f;
            float num4 = (num > num2) ? num : num2;
            if (num3 > num4)
            {
                num4 = num3;
            }
            float num5 = (num < num2) ? num : num2;
            if (num3 < num5)
            {
                num5 = num3;
            }
            float num6 = num4 + num5;
            return (num6/2f);
        }

        private WashGroup GetGroupFromName(string groupName)
        {
            if (((groupName != null) && (groupName.Length != 0)) && (this._washGroups != null))
            {
                int count = this._washGroups.Count;
                if (count != 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        WashGroup group = this._washGroups[i];
                        if (group.Name == groupName)
                        {
                            return group;
                        }
                    }
                }
            }
            return null;
        }

        internal static float GetHue(Color color)
        {
            float num = ((float) color.R)/255f;
            float num2 = ((float) color.G)/255f;
            float num3 = ((float) color.B)/255f;
            if ((num == num2) && (num == num3))
            {
                return 0f;
            }
            float num4 = (num > num2) ? num : num2;
            if (num3 > num4)
            {
                num4 = num3;
            }
            float num5 = (num < num2) ? num : num2;
            if (num3 < num5)
            {
                num5 = num3;
            }
            float num6 = num4 - num5;
            float num7 = 0f;
            if (num == num4)
            {
                num7 = ((num2 - num3)/num6)*60f;
            }
            else if (num2 == num4)
            {
                num7 = ((num3 - num)/num6)*60f;
                num7 += 120f;
            }
            else if (num3 == num4)
            {
                num7 = ((num - num2)/num6)*60f;
                num7 += 240f;
            }
            if (num7 < 0f)
            {
                num7 += 360f;
            }
            return num7;
        }

        [AttachedPropertyBrowsableForType(typeof (Pen)), AttachedPropertyBrowsableForType(typeof (Brush))]
        public static bool GetIsExcludedFromWash(DependencyObject d)
        {
            return (bool) d.GetValue(IsExcludedFromWashProperty);
        }

        internal static float GetSaturation(Color color)
        {
            float num = ((float) color.R)/255f;
            float num2 = ((float) color.G)/255f;
            float num3 = ((float) color.B)/255f;
            float num4 = (num > num2) ? num : num2;
            if (num3 > num4)
            {
                num4 = num3;
            }
            float num5 = (num < num2) ? num : num2;
            if (num3 < num5)
            {
                num5 = num3;
            }
            double num6 = num4 - num5;
            if (num6 == 0.0)
            {
                return 0f;
            }
            double num7 = num4 + num5;
            double num8 = num7/2.0;
            if (num8 > 0.5)
            {
                return (float) (num6/(2.0 - num7));
            }
            return (float) (num6/num7);
        }

        private Color GetWashColor(WashGroup group)
        {
            if ((group != null) && group.WashColor.HasValue)
            {
                return group.WashColor.Value;
            }
            return this.WashColor;
        }

        [AttachedPropertyBrowsableForType(typeof (Pen)), AttachedPropertyBrowsableForType(typeof (Brush))]
        public static string GetWashGroup(DependencyObject d)
        {
            return (string) d.GetValue(WashGroupProperty);
        }
         
        protected virtual Color PerformColorWash(Color startColor, Color washColor, WashGroup group)
        {
            WashMode mode;
            float hue;
            float saturation;
            if (washColor == Colors.Transparent)
            {
                return startColor;
            }
            if ((group != null) && group.WashMode.HasValue)
            {
                mode = group.WashMode.Value;
            }
            else
            {
                mode = this._washMode;
            }
            if (mode == WashMode.SoftLightBlend)
            {
                int num = AdjustColorComponentValue(startColor.R, washColor.R);
                int num2 = AdjustColorComponentValue(startColor.G, washColor.G);
                int num3 = AdjustColorComponentValue(startColor.B, washColor.B);
                return Color.FromArgb(startColor.A, (byte) (num & 0xff), (byte) (num2 & 0xff), (byte) (num3 & 0xff));
            }
            if (group != null)
            {
                hue = group.Hue;
                saturation = group.Saturation;
            }
            else
            {
                hue = this._hue;
                saturation = this._saturation;
            }
            return ColorFromHLS(startColor.A, hue, GetBrightness(startColor), saturation);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ResetWashColor()
        {
            this._washColor = Colors.Transparent;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ResetWashGroups()
        {
            if (this._washGroups != null)
            {
                this._washGroups.Clear();
            }
        }

        public static void SetIsExcludedFromWash(DependencyObject d, bool value)
        {
            d.SetValue(IsExcludedFromWashProperty, value);
        }

        public static void SetWashGroup(DependencyObject d, string value)
        {
            d.SetValue(WashGroupProperty, value);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeWashColor()
        {
            return (this._washColor != Colors.Transparent);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeWashGroups()
        {
            return ((this._washGroups != null) && (this._washGroups.Count > 0));
        }

        private static float ToRGBHelper(float rm1, float rm2, float rh)
        {
            if (rh > 360f)
            {
                rh -= 360f;
            }
            else if (rh < 0f)
            {
                rh += 360f;
            }
            if (rh < 60f)
            {
                rm1 += ((rm2 - rm1)*rh)/60f;
            }
            else if (rh < 180f)
            {
                rm1 = rm2;
            }
            else if (rh < 240f)
            {
                rm1 += ((rm2 - rm1)*(240f - rh))/60f;
            }
            return (rm1*255f);
        }

        
 

        private Brush WashBrushHelper(Brush sourceBrush, WashGroup group)
        {
            if (this.GetWashColor(group) == Colors.Transparent)
            {
                return sourceBrush;
            }
            Brush brush = sourceBrush.Clone();
            SolidColorBrush brush2 = brush as SolidColorBrush;
            if (brush2 != null)
            {
                brush2.Color = this.WashColorHelper(brush2.Color, group);
                return brush2;
            }
            GradientBrush brush3 = brush as GradientBrush;
            if (brush3 == null)
            {
                return brush;
            }
            GradientStopCollection gradientStops = brush3.GradientStops;
            int count = gradientStops.Count;
            for (int i = 0; i < count; i++)
            {
                GradientStop stop = gradientStops[i];
                stop.Color = this.WashColorHelper(stop.Color, group);
            }
            return brush3;
        }

        private Brush WashBrushHelper(Brush sourceBrush, object sourceObject)
        {
            if (GetIsExcludedFromWash(sourceBrush))
            {
                return sourceBrush;
            }
            string washGroup = GetWashGroup(sourceBrush);
            WashGroup groupFromName = this.GetGroupFromName(washGroup);
            Brush brush = this.WashBrushHelper(sourceBrush, groupFromName);
            Freezable freezable = sourceObject as Freezable;
            if ((freezable != null) && freezable.IsFrozen)
            {
                brush.Freeze();
            }
            return brush;
        }

        internal Color WashColorHelper(Color startColor, WashGroup group)
        {
            Color washColor = this.GetWashColor(group);
            return this.PerformColorWash(startColor, washColor, group);
        }

        internal Pen WashPenHelper(Pen sourcePen, object sourceObject)
        {
            if (GetIsExcludedFromWash(sourcePen))
            {
                return sourcePen;
            }
            string washGroup = GetWashGroup(sourcePen);
            WashGroup groupFromName = this.GetGroupFromName(washGroup);
            if (this.GetWashColor(groupFromName) == Colors.Transparent)
            {
                return sourcePen;
            }
            Pen pen = sourcePen.Clone();
            pen.Brush = this.WashBrushHelper(sourcePen.Brush, groupFromName);
            Freezable freezable = sourceObject as Freezable;
            if ((freezable != null) && freezable.IsFrozen)
            {
                pen.Freeze();
            }
            return pen;
        }
         

        public Color WashColor
        {
            get { return this._washColor; }
            set
            {
                if (value != this._washColor)
                { 
                    this._washColor = value;
                    this._hue = GetHue(value);
                    this._saturation = GetSaturation(value); 
                }
            }
        }

        public WashGroupList WashGroups
        {
            get { return this._washGroups; }
            set
            {
                if (this._washGroups != value)
                { 
                    this._washGroups = value;  
                }
            }
        }

        [DefaultValue(0)]
        public WashMode WashMode
        {
            get { return this._washMode; }
            set
            {
                if (value != this._washMode)
                { 
                    if (!Enum.IsDefined(typeof (WashMode), value))
                    {
                        throw new InvalidEnumArgumentException("WashMode", (int) value, typeof (WashMode));
                    }
                    this._washMode = value; 
                }
            }
        }

        void ISupportInitialize.BeginInit()
        {
             
        }

        void ISupportInitialize.EndInit()
        {
           WashResources();
        }
    }



}
