﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Media; 

namespace MorganStanley.MSDotNet.MSGui.Themes.ResourceWasher
{
    public class WashGroup
    {
        // Fields
        private float _hue;
        private string _name;
        private float _saturation;
        private Color? _washColor = null;
        private WashMode? _washMode = null;

        // Methods
        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ResetWashColor()
        {
            this._washColor = null;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ResetWashMode()
        {
            this._washMode = null;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeWashColor()
        {
            return this._washColor.HasValue;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeWashMode()
        {
            return this._washMode.HasValue;
        }

        // Properties
        internal float Hue
        {
            get
            {
                return this._hue;
            }
        }

        [DefaultValue((string)null)]
        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        internal float Saturation
        {
            get
            {
                return this._saturation;
            }
        }

        [TypeConverter(typeof(NullableConverter<Color>))]
        public Color? WashColor
        {
            get
            {
                return this._washColor;
            }
            set
            {
                this._washColor = value;
                Color? nullable = value;
                Color color = nullable.HasValue ? nullable.GetValueOrDefault() : Colors.Transparent;
                this._hue = WashableResourceDictionary.GetHue(color);
                this._saturation = WashableResourceDictionary.GetSaturation(color);
            }
        }

        [TypeConverter(typeof(NullableConverter<WashMode>))]
        public WashMode? WashMode
        {
            get
            {
                return this._washMode;
            }
            set
            {
                this._washMode = value;
            }
        }
    }

    public class WashGroupList:List<WashGroup>
    {
        
    }
}
