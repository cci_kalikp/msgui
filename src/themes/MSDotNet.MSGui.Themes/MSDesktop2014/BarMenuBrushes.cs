﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;

namespace MorganStanley.MSDotNet.MSGui.Themes.MSDesktop2014
{
    public partial class BarMenuBrushes
    {
        public BarMenuBrushes()
        {
            InitializeComponent();
            if (ModernTheme.BaseColour == ModernThemeBase.Light)
                this.MergedDictionaries.Add(new ResourceDictionary { Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/LightColorPalette.xaml") });
            else
                this.MergedDictionaries.Add(new ResourceDictionary { Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/DarkColorPalette.xaml") });
        }
    }
}
