﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Themes.MSDesktop2014.Settings
{
    public class ModernThemeConfigModule : IModule
    {
        private readonly IApplicationOptions applicationOptions;

        public ModernThemeConfigModule(IApplicationOptions applicationOptions)
        {
            this.applicationOptions = applicationOptions;
        }

        public void Initialize()
        {
            var optionView = new ModernThemeOptionView();

            this.applicationOptions.AddOptionPage("MSDesktop", "Appearance", optionView);
        }


        class ModernThemeOptionView : IOptionView
        {
            private Color originalAccentColor;
            private Color selectedAccentColor;
            private ModernThemeOptionControl control;

            public ModernThemeOptionView()
            {
                var vm = new ModernThemeConfigViewModel
                {
                    R = originalAccentColor.R,
                    G = originalAccentColor.G,
                    B = originalAccentColor.B,
                };
                this.control = new ModernThemeOptionControl
                {
                    DataContext = vm
                };
            }

            public void OnOK()
            {
                ModernTheme.AccentColor = this.selectedAccentColor;
            }

            public void OnCancel()
            {
                ModernTheme.AccentColor = this.originalAccentColor;
            }

            public void OnDisplay()
            {
                this.originalAccentColor = ModernTheme.AccentColor;
                this.selectedAccentColor = ModernTheme.AccentColor;
            }

            public void OnHelp()
            {

            }

            public UIElement Content
            {
                get { return this.control; }
            }
        }
    }


}
