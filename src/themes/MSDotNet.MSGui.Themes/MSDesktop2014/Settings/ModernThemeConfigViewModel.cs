﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Themes.MSDesktop2014.Settings
{
    public class ModernThemeConfigViewModel : INotifyPropertyChanged
    {
        private byte r;
        private byte g;
        private byte b;

        public byte R
        {
            get { return r; }
            set
            {
                try
                {
                    r = value;
                    ModernTheme.AccentColor = new Color { A = 255, R = this.R, G = this.G, B = this.B };
                    OnPropertyChanged("R");
                }
                catch
                {
                    //yepp.
                }
            }
        }

        public byte G
        {
            get { return g; }
            set
            {
                try
                {
                    g = value;
                    ModernTheme.AccentColor = new Color { A = 255, R = this.R, G = this.G, B = this.B };
                    OnPropertyChanged("G");
                }
                catch
                {
                    //yepp.
                }
            }
        }

        public byte B
        {
            get { return b; }
            set
            {
                try
                {
                    b = value;
                    ModernTheme.AccentColor = new Color { A = 255, R = this.R, G = this.G, B = this.B };
                    OnPropertyChanged("B");
                }
                catch
                {
                    //yepp.
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
