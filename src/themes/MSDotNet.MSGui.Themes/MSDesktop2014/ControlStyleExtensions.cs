﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Themes.MSDesktop2014
{
    public static class ControlStyleExtensions
    {

        public static readonly DependencyProperty IsGroupBoxHeaderProperty =
            DependencyProperty.RegisterAttached("IsGroupBoxHeader", typeof(bool), typeof(ControlStyleExtensions), new PropertyMetadata(default(bool), IsGroupBoxHeaderChanged));
 
        
        private static void IsGroupBoxHeaderChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            FrameworkElement header = dependencyObject_ as FrameworkElement;
            if (header != null)
            {
                if (Convert.ToBoolean(dependencyPropertyChangedEventArgs_.NewValue))
                {
                    header.Initialized += header_Initialized; 
                }
                else
                {
                    header.Initialized -= header_Initialized;
                }
            }
        }

        static void header_Initialized(object sender, EventArgs e)
        {
            FrameworkElement header = (FrameworkElement)sender;
            ContentPresenter presenter = header as ContentPresenter;
            if (presenter != null)
            {
                presenter.ApplyTemplate();
            }
            foreach (var child in header.FindVisualChildren<FrameworkElement>())
            {
                ContentPresenter childPresenter = child as ContentPresenter;
                if (childPresenter != null)
                {
                    childPresenter.ApplyTemplate();
                }
                SetIsGroupBoxHeaderElement(child, true);
            }
            
            header.Initialized -= header_Initialized;
        }

        public static void SetIsGroupBoxHeader(UIElement element, bool value)
        {
            element.SetValue(IsGroupBoxHeaderProperty, value);
        }

        public static bool GetIsGroupBoxHeader(UIElement element)
        {
            return (bool) element.GetValue(IsGroupBoxHeaderProperty);
        }

        public static readonly DependencyProperty IsGroupBoxHeaderElementProperty =
            DependencyProperty.RegisterAttached("IsGroupBoxHeaderElement", typeof (bool), typeof (ControlStyleExtensions), new PropertyMetadata(default(bool)));

        public static void SetIsGroupBoxHeaderElement(UIElement element, bool value)
        {
            element.SetValue(IsGroupBoxHeaderElementProperty, value);
        }

        public static bool GetIsGroupBoxHeaderElement(UIElement element)
        {
            return (bool) element.GetValue(IsGroupBoxHeaderElementProperty);
        }

    }
}
