﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme; 

namespace MorganStanley.MSDotNet.MSGui.Themes.MSDesktop2014
{
    public partial class CommonResources : ResourceDictionary
    {
        private const int OpacityFactor = 144;
        public CommonResources()
        {
            if (ModernTheme.BaseColour == ModernThemeBase.Light)
                this.MergedDictionaries.Add(new ResourceDictionary { Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/LightColorPalette.xaml") });
            else
                this.MergedDictionaries.Add(new ResourceDictionary { Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/DarkColorPalette.xaml") });

            InitializeBrushes();
            if (!ModernTheme.Options.UseAccentColorOnStatusBar)
            {
                if (ModernTheme.BaseColour == ModernThemeBase.Light)
                {
                    var brush = new SolidColorBrush(Color.FromArgb(255, 185, 185, 185));
                    brush.Freeze();
                    this.Add("RibbonWindowStatusBarBackground", brush);
                }
                else
                {
                    var brush = new SolidColorBrush(Color.FromArgb(255, 66, 66, 66));
                    brush.Freeze();
                    this.Add("RibbonWindowStatusBarBackground", brush);
                }
            }
             
            ModernTheme.AccentChanged += (sender, args) => InitializeBrushes();
        }

        private void InitializeBrushes()
        { 
            this[ModernTheme.AccentBrushKey] = ModernTheme.AccentBrush;
            this[ModernTheme.SecondaryAccentBrushKey] = ModernTheme.SecondaryAccentBrush;
            this[ModernTheme.AccentForegroundBrushKey] = ModernTheme.AccentForegroundBrush;
            this[ModernTheme.SecondaryAccentForegroundBrushKey] = ModernTheme.SecondaryAccentForegroundBrush; 
            this[ModernTheme.AccentForegroundFadedBrushKey] = new SolidColorBrush(ModernTheme.AccentForegroundColor) { Opacity = OpacityFactor / 255.0 }; 
            this[ModernTheme.SecondaryAccentForegroundFadedBrushKey] = new SolidColorBrush(ModernTheme.SecondaryAccentForegroundColor) { Opacity = OpacityFactor / 255.0 };
            this[ModernTheme.AccentHighlightBrushKey] = ModernTheme.AccentHighlightBrush;
            this[ModernTheme.AccentHighlightColorKey] = ModernTheme.AccentHighlightColor; 
            this[ModernTheme.AccentColourKey] = ModernTheme.AccentColor;
            this[ModernTheme.SecondaryAccentColourKey] = ModernTheme.SecondaryAccentColor;
            this[ModernTheme.AccentForegroundColourKey] = ModernTheme.AccentForegroundColor;
            this[ModernTheme.SecondaryAccentForegroundColourKey] = ModernTheme.SecondaryAccentForegroundColor;
            this[ModernTheme.AccentForegroundFadedColourKey] = Color.FromArgb(OpacityFactor, ModernTheme.AccentForegroundColor.R, ModernTheme.AccentForegroundColor.G, ModernTheme.AccentForegroundColor.B);
            this[ModernTheme.SecondaryAccentForegroundFadedColourKey] = Color.FromArgb(OpacityFactor, ModernTheme.SecondaryAccentForegroundColor.R, ModernTheme.SecondaryAccentForegroundColor.G, ModernTheme.SecondaryAccentForegroundColor.B);
            this["MSDesktop_Modern_ScrollBar_Foreground_HotTrack"] = ModernTheme.SecondaryAccentBrush;
            this["MSDesktop_Modern_ScrollBar_Foreground_Active"] = ModernTheme.AccentBrush;
            // yes, this is fugly.
            if (ModernTheme.Options.UseAccentColorOnStatusBar)
            {
                this["RibbonWindowStatusBarBackground"] = ModernTheme.AccentBrush;
            }

        }
    }
}
