﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Themes.MSDesktop2014
{
    public sealed class ModernThemeOptions
    {
        public bool UseAccentColorOnStatusBar { get; set; }

        public ModernThemeOptions()
        {
            UseAccentColorOnStatusBar = false;
        }
    }
}
