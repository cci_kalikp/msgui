﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;
using MorganStanley.MSDotNet.MSGui.Themes.Resources;

namespace MorganStanley.MSDotNet.MSGui.Themes.MSDesktop2014
{
    public sealed class ModernTheme:Theme
    {
        #region resource keys

        private const string Prefix = "MSDesktop_Modern_";

        public static string AccentColourKey = Prefix + "AccentColourKey";
        public static string AccentForegroundColourKey = Prefix + "AccentForegroundColourKey";
        public static string AccentForegroundFadedColourKey = Prefix + "AccentForegroundFadedColourKey";

        public static string SecondaryAccentColourKey = Prefix + "SecondaryAccentColourKey";
        public static string SecondaryAccentForegroundColourKey = Prefix + "SecondaryAccentForegroundColourKey";
        public static string SecondaryAccentForegroundFadedColourKey = Prefix + "SecondaryAccentForegroundFadedColourKey";

        public static string AccentBrushKey = Prefix + "ApplicationAccentBrush";
        public static string AccentForegroundBrushKey = Prefix + "AccentForegroundBrushKey";
        public static string AccentForegroundFadedBrushKey = Prefix + "AccentForegroundFadedBrushKey";

        public static string SecondaryAccentBrushKey = Prefix + "SecondaryAccentBrushKey";
        public static string SecondaryAccentForegroundBrushKey = Prefix + "SecondaryAccentForegroundBrushKey";
        public static string SecondaryAccentForegroundFadedBrushKey = Prefix + "SecondaryAccentForegroundFadedBrushKey";

        public static string AccentHighlightColorKey = Prefix + "AccentHighlightColorKey";
        public static string AccentHighlightBrushKey = Prefix + "AccentHighlightBrushKey";  
        #endregion


        private static Color _accentColor;
        private static Brush accentBrush = null;
        private static Color _accentForegroundColor;
        private static Brush accentForegroundBrush = null;
        private static Color _accentFadedForegroundColor;
        private static Brush accentFadedForegroundBrush = null;

        private static Color _secondaryAccentColor;
        private static Brush secondaryAccentBrush = null;
        private static Color _secondaryAccentForegroundColor;
        private static Brush secondaryAccentForegroundBrush = null;
        private static Color _secondaryAccentFadedForegroundColor;
        private static Brush secondaryAccentFadedForegroundBrush = null;


        private static Color _accentHighlightColor;
        private static Brush accentHighlightBrush = null;

        public override bool IsModernTheme
        {
            get
            {
                return true;
            }
        }
        public static Color AccentColor
        {
            get { return _accentColor; }
            internal set
            {
                _accentColor = value;

                var baseColourResolved = baseColour == ModernThemeBase.Dark
                    ? Color.FromRgb(54, 54, 54)
                    : Color.FromRgb(255, 255, 255);

                var invertedBaseColourResolved = baseColour == ModernThemeBase.Light
                    ? Color.FromRgb(54, 54, 54)
                    : Color.FromRgb(255, 255, 255);


                var blendFactor = baseColour == ModernThemeBase.Dark ? 0.6f : 0.2f;
                _secondaryAccentColor = Color.Add(_accentColor, Color.Multiply(Color.Subtract(baseColourResolved, _accentColor), blendFactor));

                _accentHighlightColor = Color.Add(_accentColor, Color.Multiply(Color.Subtract(invertedBaseColourResolved, _accentColor), 0.2f));


                _accentForegroundColor = (_accentColor.Luminosity() > 0.65)
                    ? Colors.Black
                    : Colors.White;
                _accentFadedForegroundColor = new Color
                {
                    A = 128,
                    R = _accentForegroundColor.R,
                    G = _accentForegroundColor.G,
                    B = _accentForegroundColor.B
                };

                _secondaryAccentForegroundColor = (_secondaryAccentColor.Luminosity() > 0.65)
                    ? Colors.Black
                    : Colors.White;
                _secondaryAccentFadedForegroundColor = new Color
                {
                    A = 128,
                    R = _secondaryAccentForegroundColor.R,
                    G = _secondaryAccentForegroundColor.G,
                    B = _secondaryAccentForegroundColor.B
                };

                ModernThemeHelper.AccentColor = _accentColor;
                ModernThemeHelper.AccentForegroundColor = _accentForegroundColor;
                ModernThemeHelper.SecondaryAccentColor = _secondaryAccentColor;
                ModernThemeHelper.SecondaryAccentForegroundColor = _secondaryAccentForegroundColor;

                accentBrush = null;
                secondaryAccentBrush = null;
                accentForegroundBrush = null;
                secondaryAccentForegroundBrush = null;

                OnAccentChanged();
            }
        }
        public static Brush AccentBrush
        {
            get
            {
                return accentBrush ?? (accentBrush = new SolidColorBrush(AccentColor));
            }
        }

        public static Color SecondaryAccentColor { get { return _secondaryAccentColor; } }
        public static Brush SecondaryAccentBrush
        {
            get
            {
                return secondaryAccentBrush ??
                       (secondaryAccentBrush = new SolidColorBrush(_secondaryAccentColor));
            }
        }

        public static Color AccentHighlightColor { get { return _secondaryAccentColor; } }
        public static Brush AccentHighlightBrush
        {
            get
            {
                return accentHighlightBrush ??
                       (accentHighlightBrush = new SolidColorBrush(_accentHighlightColor));
            }
        }

        public static Color AccentForegroundColor { get { return _accentForegroundColor; } }
        public static Brush AccentForegroundBrush
        {
            get
            {
                return accentForegroundBrush ??
                       (accentForegroundBrush = new SolidColorBrush(_accentForegroundColor));
            }
        }

        public static Color SecondaryAccentForegroundColor { get { return _secondaryAccentForegroundColor; } }
        public static Brush SecondaryAccentForegroundBrush
        {
            get
            {
                return secondaryAccentForegroundBrush ??
                       (secondaryAccentForegroundBrush =
                           new SolidColorBrush(_secondaryAccentForegroundColor));
            }
        }

        public static ModernThemeBase baseColour = ModernThemeBase.Light;

        public static ModernThemeBase BaseColour
        {
            get { return baseColour; }
            set { baseColour = value; }
        }

        public static ModernThemeOptions Options { get; internal set; }

        public const string DarkThemeName = "ModernDark";
        public const string LightThemeName = "ModernWhite";
        public static ImageSource DarkThemeIcon;
        public static ImageSource LightThemeIcon;

        static ModernTheme()
        {
            DarkThemeIcon = new BitmapImage(new Uri(@"pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/Theme_Black.png"));
            DarkThemeIcon.Freeze();
            LightThemeIcon = new BitmapImage(new Uri(@"pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/Theme_White.png"));
            LightThemeIcon.Freeze();
        }
        private int lastMergedDictionaryCount;
        Color oldWindowBackgroundColor = Colors.White;
        public ModernTheme(ModernThemeBase baseColor_, Color accentColor_, ModernThemeOptions options_)
        {
            BaseColour = baseColor_;
            AccentColor = accentColor_;
            Options = options_;
            this.Name = baseColor_ == ModernThemeBase.Dark ? DarkThemeName : LightThemeName;
            Source =
                new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/ModernTheme.xaml",
                        UriKind.RelativeOrAbsolute);
            if (BaseColour == ModernThemeBase.Light)
                this.MergedDictionaries.Add(new ResourceDictionary
                {
                    Source =
                        new Uri(
                            "pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/LightColorPalette.xaml")
                });
            else
                this.MergedDictionaries.Add(new ResourceDictionary
                {
                    Source =
                        new Uri(
                            "pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/DarkColorPalette.xaml")
                });
            lastMergedDictionaryCount = this.MergedDictionaries.Count;
        }

        protected override void SetDefaultValues()
        {
            if (Core.CoreUtilities.AreClose(ThemeExtensions.HeaderHeightDefault, 22.0))
                ThemeExtensions.HeaderHeightDefault = 14.0;
            ThemeExtensions.DefaultDockContentPadding = 2.0;
            ThemeExtensions.FloatingWindowBorderPadding = new Thickness(0.0);
            ThemeExtensions.ContentPaneFlashBorderThickness = new Thickness(1.0);
            ThemeExtensions.ContentPaneFlashBorderMargin = new Thickness(0.0);
            ThemeExtensions.FloatingWindowCaptionBarMargin = new Thickness(0);
            ThemeExtensions.ContentPaneMargin = new Thickness(0);
            ThemeExtensions.FloatingWindowBorderCornerRadius = new CornerRadius(2);
            ContentPaneWrapper.TabItemBorderCornerRadius = new CornerRadius(0);
            ThemeExtensions.TileItemBorderThickness = new Thickness(1);
            oldWindowBackgroundColor = ThemeCreator.GetWindowBackground();
            if (BaseColour == ModernThemeBase.Dark)
            {
                ThemeCreator.SetWindowBackground(Colors.Black);
            }
        }

        internal override void ClearDefaultValues()
        {
            ThemeCreator.SetWindowBackground(oldWindowBackgroundColor);

        }

        protected override void InitializeCore(ShellMode shellMode_)
        {
            if (lastMergedDictionaryCount < MergedDictionaries.Count)
            {
                lastMergedDictionaryCount = MergedDictionaries.Count; //user removed some rd
            }
            int start = 0;
            if (shellMode_ != ShellMode.LauncherBarAndFloatingWindows)
            {
                var resource = new ResourceDictionary()
                {
                    Source =
                        new Uri(
                            "pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/InfragisticsControlsStyle.xaml",
                            UriKind.RelativeOrAbsolute)
                };
                foreach (var mergedResource in resource.MergedDictionaries)
                {
                    this.MergedDictionaries.Insert(start++, mergedResource);
                    lastMergedDictionaryCount++;
                }
            }
          
            if (!DockViewModel.UseHarmonia)
            {
                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/SimpleDockPanelsStyleDict.xaml",
                        UriKind.RelativeOrAbsolute)
                });
                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/ViewContainerHeaderControlStyle.xaml",
                        UriKind.RelativeOrAbsolute)
                });
            }
            else
            {
                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/SimpleDockManager.xaml",
                        UriKind.RelativeOrAbsolute)
                });
                this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/DockManagerShared.xaml",
                        UriKind.RelativeOrAbsolute)
                });
                if (BaseColour == ModernThemeBase.Dark)
                {
                    this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                    {
                        Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/DockManagerDarkColorPalette.xaml",
                            UriKind.RelativeOrAbsolute)
                    });
                }
                else
                {
                    this.MergedDictionaries.Insert(lastMergedDictionaryCount++, new ResourceDictionary()
                    {
                        Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/DockManagerLightColorPalette.xaml",
                            UriKind.RelativeOrAbsolute)
                    });
                }
            }

            if (shellMode_ == ShellMode.LauncherBarAndFloatingWindows ||
                shellMode_ == ShellMode.LauncherBarAndWindow)
            {
                var xamPagerDict = new ResourceDictionary
                    {
                        Source =
                            new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/XamPager.xaml",
                                    UriKind.RelativeOrAbsolute)
                    };
                xamPagerDict.MergedDictionaries.Add(new ResourceDictionary
                    {
                        Source =
                            new Uri(
                                                        "pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/XamPagerBrushes.xaml",
                                                        UriKind.RelativeOrAbsolute)
                    });
                this.MergedDictionaries.Add(xamPagerDict);
                var backstageRs = new ResourceDictionary()
                    {
                        Source =
                            new Uri(
                                "pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/Controls/LauncherBarBackstage.xaml",
                                UriKind.RelativeOrAbsolute)
                    };

                this.MergedDictionaries.Add(backstageRs);

            }

            else
            {
                var backstageRs = new ResourceDictionary()
                {
                    Source =
                        new Uri(
                            "pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/Controls/RibbonWindowBackstage.xaml",
                            UriKind.RelativeOrAbsolute)
                };

                this.MergedDictionaries.Add(backstageRs);
            }
        }

        public static void InjectResources(FrameworkElement element)
        {
            element.Resources.MergedDictionaries.Add(new ResourceDictionary
            {
                Source =
                    new Uri(
                        "pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/RibbonOverrides.xaml")
            });
        }

        public static event EventHandler AccentChanged;

        private static void OnAccentChanged()
        {
            var copy = AccentChanged;
            if (copy != null)
            {
                copy(null, new EventArgs());
            }
        }
    }

    public static class ColorHelper
    {
        public static double Luminosity(this Color c)
        {
            float _R = (c.R / 255f);
            float _G = (c.G / 255f);
            float _B = (c.B / 255f);

            float _Min = Math.Min(Math.Min(_R, _G), _B);
            float _Max = Math.Max(Math.Max(_R, _G), _B);
            float _Delta = _Max - _Min;

            float H = 0;
            float S = 0;
            float L = (float)((_Max + _Min) / 2.0f);

            if (_Delta != 0)
            {
                if (L < 0.5f)
                {
                    S = (float)(_Delta / (_Max + _Min));
                }
                else
                {
                    S = (float)(_Delta / (2.0f - _Max - _Min));
                }


                if (_R == _Max)
                {
                    H = (_G - _B) / _Delta;
                }
                else if (_G == _Max)
                {
                    H = 2f + (_B - _R) / _Delta;
                }
                else if (_B == _Max)
                {
                    H = 4f + (_R - _G) / _Delta;
                }
            }

            return L;
        }
    }

    public class IsNotNullConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
