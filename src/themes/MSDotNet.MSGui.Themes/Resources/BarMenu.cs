﻿using System; 
using System.Windows; 
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;
using MorganStanley.MSDotNet.MSGui.Impl.Themes;
using MorganStanley.MSDotNet.MSGui.Themes.MSDesktop2014;

namespace MorganStanley.MSDotNet.MSGui.Themes.Resources
{
    public partial class BarMenu : IDynamicResourceDictionary
    {
        public BarMenu()
        {
            InitializeComponent();
        }

        void IDynamicResourceDictionary.Initialize(Theme theme_, InitialSettings initialSettings_)
        { 
            if (theme_ is BlackTheme)
            {
                this.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/BarMenuBlackBrushes.xaml",
                        UriKind.RelativeOrAbsolute)
                });


            }
            else if (theme_ is BlueTheme)
            {
                this.MergedDictionaries.Add(new BarMenuDarkBlueBrushes());  
            }
            else if (theme_ is WhiteTheme)
            {
                this.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/BarMenuWhiteBrushes.xaml",
                        UriKind.RelativeOrAbsolute)
                });
            } 
            else
            {
                this.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/BarMenuSimpleBrushes.xaml",
                        UriKind.RelativeOrAbsolute)
                });

  
            }
              

 

        }
    }
}
