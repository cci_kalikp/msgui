﻿using System; 
using System.Collections.Generic; 
using System.Globalization;
using System.Linq; 
using System.Windows;
using System.Windows.Data;
using System.Windows.Media; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.BarWithWindow;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell;

#pragma warning disable 1591
namespace MorganStanley.MSDotNet.MSGui.Themes.Resources
{
    [ValueConversion(typeof(object), typeof(UIElement))]    
    public  class ShellWindowTitleBarConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var shellWindow = value as ShellWindow;
            if (shellWindow == null) return null;

            return shellWindow.TitleBar;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(object), typeof(Thickness))]
    public class ShellWindowTitleOuterMarginHack: IValueConverter
    {
        #region Implementation of IValueConverter

        /// <summary>
        /// Converts a value. 
        /// </summary>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <param name="value">The value produced by the binding source.</param><param name="targetType">The type of the binding target property.</param><param name="parameter">The converter parameter to use.</param><param name="culture">The culture to use in the converter.</param>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var windowChrome = new WindowChrome();
            var top = windowChrome.ResizeBorderThickness.Left;
            return new Thickness(0, top, 0, -top);
        }

        /// <summary>
        /// Converts a value. 
        /// </summary>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <param name="value">The value that is produced by the binding target.</param><param name="targetType">The type to convert to.</param><param name="parameter">The converter parameter to use.</param><param name="culture">The culture to use in the converter.</param>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    [ValueConversion(typeof(object), typeof(Thickness))]
    public class ShellWindowTitleInnerMarginHack : IValueConverter
    {
        #region Implementation of IValueConverter

        /// <summary>
        /// Converts a value. 
        /// </summary>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <param name="value">The value produced by the binding source.</param><param name="targetType">The type of the binding target property.</param><param name="parameter">The converter parameter to use.</param><param name="culture">The culture to use in the converter.</param>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var windowChrome = new WindowChrome();
            var top = windowChrome.ResizeBorderThickness.Left;
            return new Thickness(0,  0, 0, top);
        }

        /// <summary>
        /// Converts a value. 
        /// </summary>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <param name="value">The value that is produced by the binding target.</param><param name="targetType">The type to convert to.</param><param name="parameter">The converter parameter to use.</param><param name="culture">The culture to use in the converter.</param>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    [ValueConversion(typeof(object), typeof(Thickness))]
    public class ShellWindowIconMarginHack : IValueConverter
    {
        #region Implementation of IValueConverter

        /// <summary>
        /// Converts a value. 
        /// </summary>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <param name="value">The value produced by the binding source.</param><param name="targetType">The type of the binding target property.</param><param name="parameter">The converter parameter to use.</param><param name="culture">The culture to use in the converter.</param>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var windowChrome = new WindowChrome();
            var top = windowChrome.ResizeBorderThickness.Left;
            return new Thickness(0, 0, 6, top);
        }

        /// <summary>
        /// Converts a value. 
        /// </summary>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <param name="value">The value that is produced by the binding target.</param><param name="targetType">The type to convert to.</param><param name="parameter">The converter parameter to use.</param><param name="culture">The culture to use in the converter.</param>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }


     [ValueConversion(typeof(TitleBarPlaceHolder), typeof(Thickness))]  
    public class GlassBorderThicknessConverter: IValueConverter
     {
         public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
         {
             var titlebarHeight = (double)value;
             var left = SystemParameters2.Current.WindowNonClientFrameThickness.Left;
             return new Thickness(left, left + titlebarHeight, left, left);

         }

         public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
         {
             throw new NotImplementedException();
         }
     }


     [ValueConversion(typeof(TitleBarPlaceHolder), typeof(double))]
     public class GlassCaptionHeightConverter : IValueConverter
     {
         public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
         {
             var titlebar = value as TitleBarPlaceHolder;
             var left = SystemParameters2.Current.WindowNonClientFrameThickness.Left;
             return left + titlebar.Height;

         }

         public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
         {
             throw new NotImplementedException();
         }
     }

    [ValueConversion(typeof(object), typeof(bool))]
    public class NotNullConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    [ValueConversion(typeof(Color?), typeof(Brush))]
    public class AeroTitleBarColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var color = (Color?) value;
            return !color.HasValue ? Brushes.Transparent : new SolidColorBrush(color.Value) {Opacity = 0.4};
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    [ValueConversion(typeof(Color?), typeof(Brush))]
    public class NonAeroTitleBarColorConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var color = (Color?)values[0];
            var control = (Infragistics.Windows.Ribbon.XamRibbon)values[1];
            if (!color.HasValue) return control.FindResource(Infragistics.Windows.Ribbon.RibbonBrushKeys.CaptionPanelFillKey);
            return new SolidColorBrush(color.Value);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[] {};
        }
    }

    public class ItemSourceToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return Visibility.Collapsed;
            var source = value as IEnumerable<object>;
            if (source == null) return Visibility.Collapsed;
            if (source.Any()) return Visibility.Visible;
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class BarHorizontalResizeToWindowChromeThickness : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        { 
            return (bool)value ? new Thickness(3, 0, 3, 0) : new Thickness(0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class RibbonCaptionButtonAreaWidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ThemeExtensions.RibbonCaptionButtonAreaFixedWidth.HasValue ? ThemeExtensions.RibbonCaptionButtonAreaFixedWidth.Value : value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class WindowWithBarChromeConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            WindowWithBar window = value as WindowWithBar;
            if (window == null) return null;
            var chrome = new WindowChrome();
            chrome.UseAeroCaptionButtons = true;
            chrome.CaptionHeight = 0;

            BindingOperations.SetBinding(chrome, WindowChrome.GlassFrameThicknessProperty,
                                         new Binding()
                                             {
                                                 Source = window.TitleBar,
                                                 Path = new PropertyPath("Height"),
                                                 Converter = new GlassBorderThicknessConverter()
                                             }); 
            return chrome;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(DockLocation), typeof(string))]
    public class DockLocationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DockLocation dockLocation = (DockLocation)value;
            switch (dockLocation)
            {
                case DockLocation.TopLeft:
                    return "Top Left";
                case DockLocation.Top:
                    return "Top";
                case DockLocation.TopRight:
                    return "Top Right";
                case DockLocation.Left:
                    return "Left";
                case DockLocation.Center:
                    return "Tabbed";
                case DockLocation.Right:
                    return "Right";
                case DockLocation.BottomLeft:
                    return "Bottom Left";
                case DockLocation.Bottom:
                    return "Bottom";
                case DockLocation.BottomRight:
                    return "Bottom Right";
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class DockLocationConverter2 : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DockLocation dockLocation = (DockLocation)value;
            switch (dockLocation)
            {
                case DockLocation.TopLeft:
                    return "Dock active pane(in red)to Top Left of this pane(in green)";
                case DockLocation.Top:
                    return "Dock active pane(in red) to Top of this pane(in green)";
                case DockLocation.TopRight:
                    return "Dock active pane(in red) to Top Right of this pane(in green)";
                case DockLocation.Left:
                    return "Dock active pane(in red) to Left of this pane(in green)";
                case DockLocation.Center:
                    return "Dock active pane(in red) as a tab page this pane(in green)";
                case DockLocation.Right:
                    return "Dock active pane(in red) to Right of this pane(in green)";
                case DockLocation.BottomLeft:
                    return "Dock active pane(in red) to Bottom Left of this pane(in green)";
                case DockLocation.Bottom:
                    return "Dock active pane(in red) to Bottom of this pane(in green)";
                case DockLocation.BottomRight:
                    return "Dock active pane(in red) to Bottom Right of this pane(in green)";
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class StringWithoutMnemonicsConverter : IValueConverter
    {
        // Methods
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string text = value as string;
            if (text == null)
            {
                return DependencyProperty.UnsetValue;
            }
            return CoreUtilities.StripMnemonics(text, false);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
     
 

}
#pragma warning restore 1591
