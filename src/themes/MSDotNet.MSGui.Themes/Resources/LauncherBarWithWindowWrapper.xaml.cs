﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MorganStanley.MSDotNet.MSGui.Themes
{
    /// <summary>
    /// Interaction logic for LauncherBarWithWindowWrapper.xaml
    /// </summary>
    public partial class LauncherBarWithWindowWrapper : UserControl
    {
        public LauncherBarWithWindowWrapper()
        {
            InitializeComponent();
        }


        public static DependencyProperty GripVisibilityProperty =
            DependencyProperty.Register("GripVisibility", typeof (Visibility), typeof (LauncherBarWithWindowWrapper));

        public Visibility GripVisibility
        {
            get { return (Visibility) GetValue(GripVisibilityProperty); }
            set {SetValue(GripVisibilityProperty, value);}
        }
    }
}
