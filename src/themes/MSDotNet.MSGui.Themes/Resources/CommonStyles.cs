﻿  
using System;
using System.Windows; 
using MorganStanley.MSDotNet.MSGui.Impl.Application; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;
using MorganStanley.MSDotNet.MSGui.Impl.Themes; 

namespace MorganStanley.MSDotNet.MSGui.Themes.Resources
{
    public class CommonStyles : ResourceDictionary, IDynamicResourceDictionary
    {
        public CommonStyles()
        {
            Source =
                new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/CommonStyles.xaml",
                        UriKind.RelativeOrAbsolute);
        }
         
        void IDynamicResourceDictionary.Initialize(Theme theme_, InitialSettings initialSettings_)
        {
            if (theme_ is BlackTheme)
            {
                this.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/CaptionButtonBlackBrushes.xaml",
                        UriKind.RelativeOrAbsolute)
                });
            }
            else if (theme_ is BlueTheme)
            {
                this.MergedDictionaries.Add(new CaptionButtonDarkBlueBrushes());

            }
            else if (theme_ is WhiteTheme)
            {
                this.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/CaptionButtonWhiteBrushes.xaml",
                        UriKind.RelativeOrAbsolute)
                });
            }
            else
            {
                this.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/Resources/CaptionButtonSimpleBrushes.xaml",
                        UriKind.RelativeOrAbsolute)
                });
            }
             
        }
    }
}
