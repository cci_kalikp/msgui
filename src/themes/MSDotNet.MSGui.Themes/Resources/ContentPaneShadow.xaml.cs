﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Themes.Resources
{
    /// <summary>
    /// Interaction logic for ContentPaneShadow.xaml
    /// </summary>
    public partial class ContentPaneShadow
    {
        public ContentPaneShadow()
        {
            InitializeComponent();
        }

        protected override Geometry GetLayoutClip(Size layoutSlotSize_)
        {
            return null;
        }

        protected override Size ArrangeOverride(Size finalSize_)
        {
            return new Size(Math.Max(finalSize_.Width + Margin.Left - TopLeftOffset, 0),
                Math.Max(finalSize_.Height + Margin.Top - TopLeftOffset, 0));
        }

        public double TopLeftOffset
        {
            get { return (double)GetValue(TopLeftOffsetProperty); }
            set { SetValue(TopLeftOffsetProperty, value); }
        }

        public static readonly DependencyProperty TopLeftOffsetProperty =
            DependencyProperty.Register("TopLeftOffset", typeof(double), typeof(ContentPaneShadow), new UIPropertyMetadata(.0));
    }
}
