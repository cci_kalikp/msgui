﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;

namespace MSDotNet.MSGui.UITesting.RiskViewer
{
    public static class TestSetup
    {
        /// <summary>
        /// Try to attch to an instance of RiskViewer if it is running. 
        /// If not, launch a new instance.
        /// </summary>
        /// <returns>RiskViewer application</returns>
        public static Application GetApplication()
        {
            Application mainApp;

            try
            {
				mainApp = Application.Attach("Ecdev.RiskViewer.MSGUI");
            }
            catch
            {
                Assembly testAssm = typeof(CPFParamsEditorTest).Assembly;
                string assmPath = new Uri(testAssm.CodeBase).LocalPath;
                var assemblyFile = new FileInfo(assmPath);

                string directoryLocation;
                if (!(new System.Diagnostics.StackTrace(true).ToString().Contains(".UnitTestAdapter")))
					directoryLocation = assemblyFile.Directory.FullName + @"\..\..\..\..\build\common\ms\csc\Debug\examples\Ecdev.RiskViewer.MSGUI\";
                else
					directoryLocation = assemblyFile.Directory.FullName + @"\..\..\examples\Ecdev.RiskViewer.MSGUI\";

                Assert.IsTrue(Directory.Exists(directoryLocation), "Unable to locate the RiskViewer app directory");

                DirectoryInfo dir = new DirectoryInfo(directoryLocation);
                FileInfo[] files = dir.GetFiles("RR36LNUAT.bat");

                Assert.IsTrue(files.Length == 1, "Unable to find one unique instance of the RiskViewer app to launch");

                string rvAppFilePath = files[0].FullName;

                mainApp = Application.Launch(new ProcessStartInfo(rvAppFilePath) { UseShellExecute = true, WorkingDirectory = directoryLocation });
            }

            Assert.IsNotNull(mainApp, "Unable to launch the main RiskViewer application");

            return mainApp;
        }

        /// <summary>
        /// Get the main window of the RiskViewer application
        /// </summary>
        /// <param name="mainApp_">RiskViewer application</param>
        /// <returns></returns>
        public static Window GetMainWindow(Application mainApp_)
        {
            Window mainWindow = null;

            int retryCounter = 0;
            int MAX_RETRIES = 20;
            while (retryCounter < MAX_RETRIES)
            {
                Thread.Sleep(10000);
                if (mainApp_ == null || mainApp_.HasExited)
                {
                    try
                    {
						mainApp_ = Application.Attach("Ecdev.RiskViewer.MSGUI");
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                }

                List<Window> loadedWindows = mainApp_.GetWindows();

                foreach (var window in loadedWindows)
                {
                    if (window.NameMatches("RiskViewerConcordExample - (v1.0.0.0) Default"))
                    {
                        mainWindow = window;
                        retryCounter = MAX_RETRIES;
                        break;
                    }
                }
                retryCounter++;
            }
            Assert.IsNotNull(mainWindow, "Unable to locate the main window");

            return mainWindow;
        }

        /// <summary>
        /// Check if an exception is thrown, which will create an exception notice window.
        /// If it exists, it will be closed after doing the check.
        /// </summary>
        /// <param name="mainApp_">RiskViewer application</param>
        /// <returns>Exception window</returns>
        public static Window CheckForExceptionWindow(Application mainApp_)
        {
            Window exceptionWindow = null;
            var windows = mainApp_.GetWindows();

            foreach (var window in windows)
            {
                if (window.NameMatches("Microsoft .NET Framework"))
                {
                    exceptionWindow = window;
                    break;
                }
            }

            if (exceptionWindow != null)
            {
                exceptionWindow.Close();
            }

            return exceptionWindow;
        }
    }
}
