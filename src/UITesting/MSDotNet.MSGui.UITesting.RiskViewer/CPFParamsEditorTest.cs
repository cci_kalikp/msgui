﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.Finders;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems.WindowStripControls;

namespace MSDotNet.MSGui.UITesting.RiskViewer
{
    [TestClass]
    public class CPFParamsEditorTest
    {
        private static Application m_mainApp;
        private ManualResetEvent m_waitHandle = new ManualResetEvent(false);

        [ClassInitialize]
        public static void InitializeRiskViewerApp(TestContext context)
        {
            m_mainApp = TestSetup.GetApplication();
        }

        [ClassCleanup]
        public static void CloseRiskViewerApp()
        {
            m_mainApp.Kill();
        }

        [TestMethod]
        public void AddingCpfParamsAndClearingShouldNotRaiseAnException()
        {
            Window mainWindow = TestSetup.GetMainWindow(m_mainApp);

            OpenCpfParamsWindow(mainWindow);

            Window cpfParamsWindow = LocateCpfParamsWindow();

            ClickOnAddCpfParamsLink(cpfParamsWindow);

            ClickOnClearButton(cpfParamsWindow);

            Window exceptionWindow = TestSetup.CheckForExceptionWindow(m_mainApp);

            cpfParamsWindow.Close();

            Assert.IsNull(exceptionWindow, "The application launched an exception notification window");
        }

        private void OpenCpfParamsWindow(Window mainWindow_)
        {
            MenuBar toolsMenuBar = mainWindow_.GetMenuBar(SearchCriteria.ByText("Tools"));
            Assert.IsNotNull(toolsMenuBar, "Unable to locate the Tools menu button");

            ThreadPool.QueueUserWorkItem(LocateAndClickCpfParamsEditorMenu, mainWindow_);
            toolsMenuBar.Click();

            // Hold the dropdown menu open while the background thread locates and clicks on the CPS Params menu item
            m_waitHandle.WaitOne();
        }

        private void LocateAndClickCpfParamsEditorMenu(object o_)
        {
            // Wait for the tools menu bar to be clicked
            Thread.Sleep(1000);

            Window window = o_ as Window;
            var cpfParamsMenuItem = window.Get(SearchCriteria.ByText("CPF Params Editor"));
            cpfParamsMenuItem.Click();

            m_waitHandle.Set();
        }

        private Window LocateCpfParamsWindow()
        {
            Window cpfParamsWindow = null;

            // If the CPF Params Editor window hasn't loaded, wait 1s before checking again, up to a max of 5s.
            int retryCounter = 0;
            int MAX_RETRIES = 5;
            while (retryCounter < MAX_RETRIES)
            {
                if (m_mainApp.HasExited)
                {
                    m_mainApp = TestSetup.GetApplication();
                }

                List<Window> availableWindows = m_mainApp.GetWindows();
                foreach (var window in availableWindows)
                {
                    if (window.NameMatches("CPF Params Editor"))
                    {
                        cpfParamsWindow = window;
                        retryCounter = MAX_RETRIES;
                        break;
                    }
                }
                retryCounter++;
            }
            Assert.IsNotNull(cpfParamsWindow, "Unable to locate the CPF Params Editor window");

            return cpfParamsWindow;
        }

        private void ClickOnAddCpfParamsLink(Window cpfParamsWindow_)
        {
            Hyperlink addCpfParamLink =
                cpfParamsWindow_.Get<Hyperlink>(SearchCriteria.ByAutomationId("m_linkLabel").AndByText("CPF Params"));

            Point clickablePoint = addCpfParamLink.Location;
            // Force the focus to be within the area of the hyperlink which can be clicked
            clickablePoint.Offset(10, addCpfParamLink.Bounds.Height/2);

            cpfParamsWindow_.Mouse.Click(clickablePoint);
        }

        private void ClickOnClearButton(Window cpfParamsWindow_)
        {
            Button clearButton = cpfParamsWindow_.Get<Button>(SearchCriteria.ByText("Clear"));
            Assert.IsNotNull(clearButton, "Unable to locate the 'Clear' button");

            clearButton.Click();
        }
    }
}
