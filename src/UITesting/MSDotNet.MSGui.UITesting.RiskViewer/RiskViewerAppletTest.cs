﻿using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Automation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.Finders;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.WindowItems;

namespace MSDotNet.MSGui.UITesting.RiskViewer
{
    [TestClass]
    public class RiskViewerAppletTest
    {
        private static Application m_mainApp;
        private const int MAX_RETRIES = 3;

        [ClassInitialize]
        public static void InitializeRiskViewerApp(TestContext context)
        {
            m_mainApp = TestSetup.GetApplication();
        }
        
        [ClassCleanup]
        public static void CloseRiskViewerApp()
        {
            m_mainApp.Kill();
        }

        [TestMethod]
        public void ClickOnGrayButtonInScenarioAnalysisProfile()
        {
            Window rvAppWindow = LaunchRVAppWindow("daropau - ScenarioAnalysis LN");
            
            ClickOnGrayButton(rvAppWindow);

            Window exceptionWindow = TestSetup.CheckForExceptionWindow(m_mainApp);

            // Close all windows except the main window for subsequent tests to run, if any.
            rvAppWindow.Close();

            Assert.IsNull(exceptionWindow, "The application launched an exception notification window");
        }

        [TestMethod]
        public void ClickOnFilterDropDownInScenarioAnalysisProfile()
        {
            Window rvAppWindow = LaunchRVAppWindow("daropau - ScenarioAnalysis LN");
            ClickOnFilterDropDown(rvAppWindow);

            Window exceptionWindow = TestSetup.CheckForExceptionWindow(m_mainApp);

            // Close all windows except the main window for subsequent tests to run, if any.
            rvAppWindow.Close();

            Assert.IsNull(exceptionWindow, "The application launched an exception notification window");
        }

        #region Generic setup helper methods
        
        private Window LaunchRVAppWindow(string portfolioToSelect_)
        {
            //m_mainApp = TestSetup.GetApplication();

            Window mainWindow = TestSetup.GetMainWindow(m_mainApp);

            Window selectPortfolioWindow = LaunchSelectPorfolioWindow(mainWindow);

            Window rvAppWindow = SelectScenarioAnalysisPortfolio(selectPortfolioWindow, portfolioToSelect_);

            return rvAppWindow;
        }

        private Window LaunchSelectPorfolioWindow(Window mainWindow_)
        {
            Button riskViewerButton = mainWindow_.Get<Button>(SearchCriteria.ByText("Risk Viewer"));
            riskViewerButton.Click();

            if (m_mainApp.HasExited)
            {
                m_mainApp = TestSetup.GetApplication();
            }
            List<Window> windows = m_mainApp.GetWindows();
            
            Window selectPortfolioWindow = null;
            foreach (var window in windows)
            {
                if (window.NameMatches("Risk Viewer"))
                {
                    selectPortfolioWindow = window;
                    break;
                }
            }
            
            Assert.IsNotNull(selectPortfolioWindow, "Unable to find the Select Portfolio window");

            return selectPortfolioWindow;
        }

        private Window SelectScenarioAnalysisPortfolio(Window selectPortfolioWindow_, string portfolioToSelect_)
        {
            ComboBox portfolios = null;
            int retryAttempts = 0;
            while (portfolios == null && retryAttempts < MAX_RETRIES)
            {
                // If selectPortfolioWindow has not opened, we cannot get the porfolios, so we wait until the window has opened.
                Thread.Sleep(500);
                portfolios = selectPortfolioWindow_.Get<ComboBox>(SearchCriteria.ByControlType(typeof(ComboBox)));
                retryAttempts++;
            }

            Assert.IsNotNull(portfolios, "Unable to find the ComboBox from which to select the desired portfolio");
            portfolios.Select(portfolioToSelect_);

            Button loadButton = selectPortfolioWindow_.Get<Button>(SearchCriteria.ByText("Load"));
            loadButton.Click();

            if (m_mainApp.HasExited)
            {
                m_mainApp = TestSetup.GetApplication();
            }
            List<Window> windows = m_mainApp.GetWindows();
            
            Window rvAppWindow = null; 
            foreach (var window in windows)
            {
                if (window.NameMatches("Risk Viewer"))
                {
                    rvAppWindow = window;
                    break;
                }
            }

            Assert.IsNotNull(rvAppWindow, "Unable to find the RiskViewer applet window");
            return rvAppWindow;
        }

        #endregion Generic setup helper methods

        #region Test-specific helper methods
        private void ClickOnGrayButton(Window rvAppWindow_)
        {
            //TODO: Replace this with 'search by text' when there is text on the button
            Button grayButton1 = rvAppWindow_.Get<Button>(SearchCriteria.Indexed(3));
            grayButton1.Click();
        }

        private void ClickOnFilterDropDown(Window rvAppWindow_)
        {
            // Resize window to be 960x722px
            TransformPattern pattern =
                rvAppWindow_.AutomationElement.GetCurrentPattern(TransformPattern.Pattern) as TransformPattern;
            
            Assert.IsNotNull(pattern, "Unable to obtain the rvAppWindow for resizing");
            
            pattern.Resize(960, 722);

            // Locate the Filter drop-down by pixel offset from the top-right corner of the window
            Point topRightPoint = rvAppWindow_.Bounds.TopRight;
            topRightPoint.Offset(-20, 50);
            rvAppWindow_.Mouse.Click(topRightPoint);
            topRightPoint.Offset(-60, 50);
            rvAppWindow_.Mouse.Click(topRightPoint);
        }

        #endregion Test-specific helper methods
    }
}
