pushd %cd%\..\install\common\examples\LoaderExample
DEL /S /Q log4net.*
DEL /S /Q Microsoft.Practices.ServiceLocation.*
DEL /S /Q MSDotNet.MSXml.*
DEL /S /Q MSDotNet.MSTools.*
DEL /S /Q MSDotNet.MSSoap.*
DEL /S /Q MSDotNet.MSNetMQ.MQ.*
DEL /S /Q MSDotNet.Channels.*
DEL /S /Q InfragisticsWPF4*.*
DEL /S /Q MSDesktop.Isolation.msde.config
popd

pushd %cd%\..\install\common\bin
DEL /S /Q MSDotNet.MSNetMQ.*
DEL /S /Q MSDotNet.ResourceDiscovery.* 
DEL /S /Q MSDotNet.Channels.Mock.*
popd

pushd %cd%\..\install\common\examples
DEL /S /Q MSDotNet.MSNetMQ.*
DEL /S /Q MSDotNet.ResourceDiscovery.* 
DEL /S /Q MSDotNet.Channels.Mock.*
popd


