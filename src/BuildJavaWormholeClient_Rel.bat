pushd %cd%\examples\JavaIntegrationExample\JavaWormholeClient\src 
call build.cmd
call ant generate-train-metadata
popd 
call xcopy /S /C /H /R /Y %cd%\examples\JavaIntegrationExample\JavaWormholeClient\install\common\lib\* %cd%\..\install\common\lib\* 
call xcopy /R /Y %cd%\..\install\common\lib\msdesktop_wormhole_client.jar %cd%\..\install\examples_release\JavaIntegrationExample\lib\
call xcopy /R /Y %cd%\..\install\common\lib\msdesktop_wormhole_client_examples.jar %cd%\..\install\examples_release\JavaIntegrationExample\lib\
call xcopy /R /Y %cd%\examples\JavaIntegrationExample\JavaWormholeClient\install\common\bin\run-prod.cmd %cd%\..\install\examples_release\JavaIntegrationExample\bin\
call xcopy /R /Y %cd%\..\install\common\lib\msdesktop_wormhole_client.jar %cd%\..\install\examples_release\JavaIntegrationExample\.debug\lib\
call xcopy /R /Y %cd%\..\install\common\lib\msdesktop_wormhole_client_examples.jar %cd%\..\install\examples_release\JavaIntegrationExample\.debug\lib\
call xcopy /R /Y %cd%\examples\JavaIntegrationExample\JavaWormholeClient\install\common\bin\run-prod.cmd %cd%\..\install\examples_release\JavaIntegrationExample\.debug\bin\
