#!/usr/bin/perl -w
use MSDW::Version
  'Algorithm-Diff'     => '1.1902',
  ;
use Algorithm::Diff;
use strict;
use Algorithm::Diff qw(traverse_sequences);

use File::Find;

my $errorlevel = 0;

if( @ARGV==3 && $ARGV[0] =~ m/create/i)
{
  my $localdir = $ARGV[1];
  print "generating list for directory $localdir into $ARGV[2]\n";
  open (OUTFILE,'>', $ARGV[2]) or die $!;
  find( { wanted => sub {
							my $path = $File::Find::name;
							if(!-d $path)
							{
								my $filesize = -s $path;
								$path =~ s/\Q$localdir//i;
								print OUTFILE "$path,$filesize\n";
							}
						},
		  preprocess => sub { return sort {$b cmp $a} @_; }
		}
		,$localdir);
  close (OUTFILE);
}
elsif (@ARGV==3 && $ARGV[0] =~ m/test/i)
{ # IF two arguments then fist is the path the second is the expected files list
my $localdir = $ARGV[1];
my %files = ();

 open (TESTFILE, $ARGV[2]);
while (<TESTFILE>) {
        my $path;
        my $size;
               chomp;
		next if $_ eq ''; # strip out the empty lines
        ($path,$size) = split(/,/);
        $files{$path} = $size;
}
close (TESTFILE);
  my $expectedCount = keys( %files );
  print "Expecting $expectedCount files.\n";
  open (OUTNEWFILE,'>', "$localdir/../build/StructureSnapshot.txt") or die $!;
  find( { wanted => sub {
							my $path = $File::Find::name;
							my $fullpath = $path;
							my $filesize = -s $path;
							$path =~ s/\Q$localdir//i;
							if(!(-d $fullpath))
							{
								print OUTNEWFILE "$path,$filesize\n";
							}
							if(exists $files{$path})
							{
								if( abs($filesize - $files{$path}) > ($filesize / 2)) ## test if within 50%
								{
									print "CHANGED FILE $path    Size changed by more than 50% ($filesize vrs $files{$path})\n";
								}
								delete $files{$path};
							}
							else
							{
								if(!( -d $fullpath))
								{
									print "ADDED FILE   $path\n";
									$errorlevel++;
								}
							
							}
						},
		  preprocess => sub { return sort {$b cmp $a} @_; }
		}
		,$localdir); 

  foreach (keys %files) 
  {
    print "MISSING FILE $_\n";
    $errorlevel++;
  }
  print "$errorlevel incorrect files.\n";
  close(OUTNEWFILE);
}
else
{
   print "perl TestFiles.pl create will allow you to create a file as a snapshot of files generated during a build\n";
   print "then you will be able to test with perl TestFiles.pl test\n";
   print "perl TestFiles.pl create path_to_directory_to_enumerate file_to_create\n";
   print "perl TestFiles.pl test path_to_directory_to_test file_created_before\n";
   print "output will be added, missing and _file lengths changed by more than 50%\n";
   print "The snapshot generated for the current build during test will be saved in build\\StructureSnapshot.txt\n";
}
exit $errorlevel;
