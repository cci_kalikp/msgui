﻿//cw Persistence_Custom_Data_Persistence :1
/**##Description*/
/**Apart from persisting layouts and states of windows and widgets, MSDesktop supports persistence
 * of custom data, not related to any view or widget. There are two kinds of persistence:
 * <ul>
 *   <li><b>global</b> - there is one version of data for all layouts
 *   <li><b>local</b> - the data is persisted on per layout basis
 * </ul>
 */

using System;
using System.Linq;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
/**##Namespaces*/
//{
using MorganStanley.MSDotNet.MSGui.Core;
//}

namespace PersistenceExamples
{
    public class PersistenceDemoModule : IModule
    {
        private readonly IPersistenceService m_persistenceService;
        private const string MyPersistorName = "MyPersistor";

        /**##Context*/
        /**Get an instance of IPersistenceService from the container:*/
        //{
        public PersistenceDemoModule(IPersistenceService persistenceService)
        {
            m_persistenceService = persistenceService;
        }
        //}

        /**##Code*/
        /**Create a function to persist custom state. The function should produce an XDocument:*/
        //{
        private XDocument SaveCustomState()
        {
            var stateDoc = new XDocument();
            var stateEl = new XElement("MyCustomState", "SomeValue");
            stateDoc.Add(stateEl);
            return stateDoc;
        }
        //}

        /**Create a function to restore custom state. The function takes an XDocument and reflects 
         * it in the state of the application:*/
        //{
        private void RestoreCustomState(XDocument state)
        {
            var stateEl = state.Descendants("MyCustomState").First();
            Console.WriteLine(stateEl.Value);
        }
        //}

        public void Initialize()
        {
            /**Create a new persistor based on the above function pair. You need also to provide
             * a unique name:*/
            //{
            m_persistenceService.AddPersistor(MyPersistorName, RestoreCustomState, SaveCustomState);
            //}
            /**In order to use global persistance, use the following method:*/
            //{
            m_persistenceService.AddGlobalPersistor(MyPersistorName, RestoreCustomState, SaveCustomState);
            //}
        }
    }
}
