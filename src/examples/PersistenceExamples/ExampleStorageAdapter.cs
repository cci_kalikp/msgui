﻿//cw Persistence_Custom_Storage :1
/**##Description*/
/**By default, MSDesktop uses file-based persistence storage. However, it is possible to provide
 * a different way of persisting user profile data. For example, you may want to store the user data
 * on a remote server. This example describes how to configure MSDesktop to use custom storage.
 */

using System;
using System.Xml;

/**##Namespaces*/
//{
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
//}

//cw Persistence_Custom_Storage :3
/**##Context*/
/**It is possible to provide your own storage using Concord Configuration interception. MSDesktop
 * uses Concord Configuration to represent user profile data. You can provide an interceptor which would
 * be called when configuration is about to be written/read. Instead of writing/reading it from/to a file,
 * you may provide your own logic for saving/retrieving the list of available profiles.*/

namespace PersistenceExamples
{
    /**##Code*/
    /**Firstly, create the interceptor. It <b>MUST</b> inherit from IConfigurationInterceptingWriter. It <b>MAY</b>
     * also inherit from ConcordPersistenceStorage - this would give you access to the unity
     * container which you might need at this point:*/
    //{
    internal class ExampleStorageAdapter : ConcordPersistenceStorage, //can be inherited so that user could access unity container exposed in this base class
                                           IConfigurationInterceptingWriter //must inherit
    {
        //}
        /**Copy the following lines into your class. They define the XML structure used by MSDesktop and
         * the way how to retrieve layout data from it:*/
        //{
        private const string ConfigurationSectionXmlTemplate =
          @"<MSDesktopLayouts>
                <WindowManager type=""MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.ConcordWindowManager, MSDotNet.MSGui.Impl"">
                    <Layouts></Layouts>
                    <DefaultLayout></DefaultLayout>
                </WindowManager>
            </MSDesktopLayouts>";

        private const string LayoutXPath = "MSDesktopLayouts/WindowManager/Layouts/Layout";
        //}

        /**Define a read interceptor. The interceptor's role is to produce a ConfigurationSection
         * representing available profiles. You should provide your custom logic of retrieving the list
         * of profiles here.*/
        //{
        public ConfigurationSection InterceptRead(ConfigurationSection original)
        {
            //}

            var profileName = "Some profile";
            var profileXml = new XmlDocument();

            /**For each available profile you need to produce the following XML element and append it
             * to the original ConfigurationSection node:*/
            //{
            var node = new XmlDocument
            {
                InnerXml = ConfigurationSectionXmlTemplate
            };

            var attr = node.CreateAttribute("name");
            attr.InnerText = profileName;
            var element = node.CreateElement("Layout");
            element.Attributes.Append(attr);
            element.InnerXml = profileXml.InnerXml;
            node.DocumentElement["WindowManager"]["Layouts"].AppendChild(element);
           
            //}

            //**Also you can set up the default layout node as well:*/
            //{
            node.DocumentElement["WindowManager"]["DefaultLayout"].InnerText = "Default";
            //}
            /**Return the configuration section:*/
            //{
            return new ConfigurationSection(original.Name, original.Version, new XmlNode[] { }, node.DocumentElement);
        }
        //}

        /**Define a write interceptor. It's task is to reflect the profile data encoded in a configuration section
         * in your custom persistence provider. It will be called every time any of the profiles is modified and
         * saved.
         */
        //{
        public void InterceptWrite(string name, string version, XmlNode node, InterceptingConfigurationStore.DecoratedWriteConfiguration decoratedWriteConfiguration)
        {
            foreach (XmlElement element in node.OwnerDocument.SelectNodes(LayoutXPath))
            {
                var profileName = element.Attributes["name"].Value;
                var profileXml = XmlHelper.XmlElementToXElement((XmlElement) element.FirstChild);
                //}
                /**Here comes the logic for saving the profile in your custom storage.*/
                //{
            }
        }
    }
    //}
}
