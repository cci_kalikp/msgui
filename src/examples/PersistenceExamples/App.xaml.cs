﻿using System.Windows;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Themes;
//cw Persistence_Custom_Storage :2
//{
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
//}

namespace PersistenceExamples
{
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var boot = new Framework();
            boot.AddSimpleTheme();

            boot.AddModule<PersistenceDemoModule>();

            //cw Persistence_Custom_Storage :5
            /**<b>You only need to this if you are using unity inside your Adapter<b>,
             * You could enable the Concord Configuration based storage adapter inside your bootstrap code. Use the following framework extension. You need to provide
             * an instance of ConcordPersistenceStorage (which happens to be our Adapter).*/
            //{
            boot.SetupPersistenceStorage(new ExampleStorageAdapter());
            //}

            boot.Start();
        }
    }
}
