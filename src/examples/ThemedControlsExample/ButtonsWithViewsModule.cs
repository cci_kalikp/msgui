﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;

namespace ThemedControlsExample
{
    internal sealed class ButtonsWithViewsModule : IModule
    {
        private const string DockedViewFactory = "DockedWindowFactory";
        private const string FloatingViewFactory = "FloatingViewButtonFactory";

        private readonly IChromeRegistry _chromeRegistry;
        private readonly IChromeManager _chromeManager;

        public ButtonsWithViewsModule(IChromeRegistry chromeRegistry, IChromeManager chromeManager)
        {
            _chromeRegistry = chromeRegistry;
            _chromeManager = chromeManager;
        }

        public void Initialize()
        {
            // Register the buttons widgets
            _chromeRegistry.RegisterWidgetFactory(DockedViewFactory, _chromeManager.ShowWindowButtonFactory());
            _chromeRegistry.RegisterWidgetFactory(FloatingViewFactory, _chromeManager.ShowWindowButtonFactory());

            // Add buttons
            _chromeManager.PlaceWidget(
                DockedViewFactory,
                "Main/Views",
                new InitialShowWindowButtonParameters
                {
                    Text = "Show Docked View (Infragistics)",
                    WindowFactoryID = InfragisticsControlsModule.InfragisticsViewFactory,
                    Size=ButtonSize.Large,
                    InitialParameters = new InitialWindowParameters
                    {
                        InitialLocation = InitialLocation.DockRight,
                    }
                });

            _chromeManager.PlaceWidget(
                FloatingViewFactory,
                "Main/Views",
                new InitialShowWindowButtonParameters
                {
                    Text = "Show Floating View (WPF)",
                    WindowFactoryID = WpfControlsModule.WpfViewFactory,
                    Size = ButtonSize.Large,
                    InitialParameters = new InitialWindowParameters
                    {
                        InitialLocation = InitialLocation.Floating,
                    }
                });
        }
    }
}
