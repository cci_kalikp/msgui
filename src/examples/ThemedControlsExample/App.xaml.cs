﻿using System.Windows;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Impl; 
using MorganStanley.MSDotNet.MSGui.Themes;
using MSDesktop.Debugging;
using MorganStanley.MSDotNet.MSGui.Themes.MSDesktop2014;

namespace ThemedControlsExample
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
      

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var boot = new Framework(); 
            boot.AddConfigurationDependentTheme(ModernTheme.DarkThemeName, Colors.RoyalBlue);
            boot.AddModule<ButtonsWithViewsModule>();
            boot.AddModule<InfragisticsControlsModule>();
            boot.AddModule<WpfControlsModule>();
            boot.EnableDebugging();
            boot.Start();
        }
         
    }
}
 