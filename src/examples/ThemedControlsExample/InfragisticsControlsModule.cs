﻿using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using ThemedControlsExample.Views;

namespace ThemedControlsExample
{
    internal sealed class InfragisticsControlsModule : IModule
    {
        internal const string InfragisticsViewFactory = "InfragisticsViewFactory";

        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;

        public InfragisticsControlsModule(IChromeManager chromeManager, IChromeRegistry chromeRegistry)
        {
            _chromeRegistry = chromeRegistry;
            _chromeManager = chromeManager;
            _chromeManager.LayoutLoaded += _chromeManager_LayoutLoaded;
        }

        private void _chromeManager_LayoutLoaded(object sender, System.EventArgs e)
        {
            _chromeManager.CreateWindow(InfragisticsViewFactory, new InitialWindowParameters
                {
                    InitialLocation = InitialLocation.DockRight,
                });
        }

        public void Initialize()
        {
            _chromeRegistry.RegisterWindowFactory(InfragisticsViewFactory, InitHandler);
        }

        private static bool InitHandler(IWindowViewContainer emptyViewContainer, XDocument state)
        {
            emptyViewContainer.Title = "Infragistics Controls";
            emptyViewContainer.Content = new InfragisticsControlsView();
            return true;
        }
    }
}
