﻿using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using ThemedControlsExample.Views;

namespace ThemedControlsExample
{
    internal sealed class WpfControlsModule : IModule
    {
        internal const string WpfViewFactory = "WpfViewButtonFactory";

        private readonly IChromeRegistry _chromeRegistry;
        private readonly IChromeManager _chromeManager;

        public WpfControlsModule(IChromeManager chromeManager, IChromeRegistry chromeRegistry)
        {
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
            _chromeManager.LayoutLoaded += _chromeManager_LayoutLoaded;
        }

        private void _chromeManager_LayoutLoaded(object sender, System.EventArgs e)
        {
            _chromeManager.CreateWindow(WpfViewFactory, new InitialWindowParameters
                {
                    InitialLocation = InitialLocation.DockLeft,
                });
        }

        public void Initialize()
        {
            _chromeRegistry.RegisterWindowFactory(WpfViewFactory, InitHandler);
        }

        private static bool InitHandler(IWindowViewContainer emptyViewContainer, XDocument state)
        {
            emptyViewContainer.Title = "WPF Controls";
            emptyViewContainer.Content = new WpfControlsView();
            return true;
        }
    }
}
