﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ThemedControlsExample.Views
{
    public sealed class ListBoxItem
    {
        private readonly string _displayText;
        private readonly string _messageText;

        internal ListBoxItem(string displayText, string messageText)
        {
            _displayText = displayText;
            _messageText = messageText;
        }

        public string DisplayText
        {
            get { return _displayText; }
        }

        public string MessageText
        {
            get { return _messageText; }
        }
    }
}
