﻿using System.Collections.Generic;

namespace ThemedControlsExample.Views
{
    public partial class WpfControlsView
    {
        private readonly List<ListBoxItem> _listBoxItems;

        public WpfControlsView()
        {
            _listBoxItems = new List<ListBoxItem>
                {
                    new ListBoxItem("Item 1", "This was item 1"),
                    new ListBoxItem("Item 2", "This was item 2"),
                    new ListBoxItem("Item 3", "This was item 3"),
                };

            InitializeComponent();
        }

        public List<ListBoxItem> Items
        {
            get { return _listBoxItems; }
        }
    }
}
