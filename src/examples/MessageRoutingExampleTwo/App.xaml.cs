﻿using System;
using System.Windows;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.Message;
using MessageRoutingExampleLib.Messages;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Themes;
using DemoTwo.Modules;

namespace DemoTwo
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private const string APPLICATIONAME = "MSDesktop Secondary";

        protected override void OnStartup(StartupEventArgs e_)
        {
            base.OnStartup(e_);

            var msdesktop = new Framework();

            msdesktop.SetShellMode(ShellMode.RibbonWindow);
            msdesktop.AddSimpleTheme();
            msdesktop.EnableChromeReordering();

            //msdesktop.DisableSplashScreen();

            msdesktop.SetApplicationName(APPLICATIONAME);
            msdesktop.SetApplicationVersion("0.1.0.0");
            msdesktop.EnableExceptionHandler("msguiadministrators@ms.com");

            msdesktop.EnableLogging(APPLICATIONAME);

            msdesktop.EnableMessageRoutingCommunication(true);
            msdesktop.EnableMessageRoutingDefaultGui();
            msdesktop.EnableMessageRoutingHistory();
            msdesktop.EnableMessageRoutingHistoryGui();
            msdesktop.EnableMessageRoutingMonitor();

            //set up the message info separately
            MessageInfoExtensions.SetMessageInfo<Bar>("Bar Message");
            MessageInfoExtensions.SetMessageInfoGetter(new Func<Test, string>(t_ => t_.ToString()));

            msdesktop.AddModule<MSDesktopSecondaryModule>();
            //msdesktop.EnableParallelBasedEventing();
            msdesktop.AcquireFocusAfterStartup();

            msdesktop.Start();
        }
    }
}
