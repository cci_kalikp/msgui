﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using MSDesktop.MessageRouting;
using MessageRoutingExampleLib.Messages;
using MessageRoutingExampleOne.Modules;

namespace DemoTwo
{
    public partial class DemoWindowContent : UserControl
    {
        private readonly IRoutingCommunicator routingCommunicator;

        private readonly IRoutedPublisher<Bar> barPublisher;

        private int barCounter = 0;

        public DemoWindowContent(IRoutingCommunicator routingCommunicator, Color color)
        {
            InitializeComponent();
            this.routingCommunicator = routingCommunicator;

            this.routingCommunicator.GetSubscriber<Bar>().GetObservable().Subscribe(BarMessageArrived);
            this.routingCommunicator.GetSubscriber<Test>().GetObservable().Subscribe(TestMessageArrived);
            this.routingCommunicator.GetSubscriber<DemoTwoMessage>().GetObservable().Subscribe(DemoTowMessageArrived);

            this.barPublisher = this.routingCommunicator.GetPublisher<Bar>();
            this.barPublisher.MessageDelivered += BarPublisher_MessageDelivered;
            this.barPublisher.MessageFailed += BarPublisher_MessageFailed;

            Header.Background = new SolidColorBrush(color);
        }

        private void Bar_Click(object sender, RoutedEventArgs e)
        {
            var bar = new Bar
            {
                Action = "BarAction",
                Message = "Bar " + (++barCounter)
            };

            this.barPublisher.Publish(bar);
        }

        private void BarPublisher_MessageDelivered(object sender, MessageDeliveredEventArgs e)
        {
            Action invoke = () =>
            {
                ResultListBox.Items.Insert(0, "DELIVERED: " + ((Bar)e.Message).Message);
            };

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

        private void BarPublisher_MessageFailed(object sender, MessageFailedEventArgs e)
        {
            Action invoke = () =>
            {
                ResultListBox.Items.Insert(0, "FAILED: " + ((Bar)e.Message).Message);
            };

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

        private void BarMessageArrived(Bar bar)
        {
            Action invoke = () =>
            {
                ResultListBox.Items.Insert(0, "ARRIVED: " + bar.Message + " action: " + bar.Action);
            };

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

        private void TestMessageArrived(Test test)
        {
            Action invoke = () =>
            {
                ResultListBox.Items.Insert(0, "ARRIVED: " + test.Message + " action: " + test.Action);
            };

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }
        private void DemoTowMessageArrived(DemoTwoMessage message)
        {
            Action invoke = () => ResultListBox.Items.Insert(0, "ARRIVED: " + message);

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }
    }
}
