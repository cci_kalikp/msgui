﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Logging;
using MorganStanley.MSDotNet.My;

namespace Settings_ApplicationAndShells
{
    public class LoggingWithMprModule : IModule
    {
        //cw Settings_Howto_Enable_Logging_With_MPR :1
        /**##description*/
        /**The framework facilitates the way you can specify the MPR for logging messages by providing
         * a facade class over MSLoggerFactory.
         */
        //cw Settings_Howto_Enable_Logging_With_MPR :3
        /**##code*/
        //{
        private static IMSLogger m_logger = MSLoggerFactoryFacade.CreateLogger<LoggingWithMprModule>("sampleMeta",
                                                                                                     "sampleProject",
                                                                                                     "sampleRelease");
        //}

        public void Initialize()
        {
            throw new NotImplementedException();
        }
    }
}
