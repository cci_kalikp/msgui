﻿//cw Settings_Howto_Set_Ribbon_Icon :50
/**##Description
 * Below shows how to set the Ribbon Icon
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/**##namespaces*/
/** use the following namespace for*/
/**
 *IModule
 */
//{
using Microsoft.Practices.Composite.Modularity;
//}
/**
 *IChromeManager
 */
//{
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
//}
/**
 *RibbonChromeManager
*/
//{
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
//}
/**
*Uri.ToIcon
*/
//{
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
//}
using System.Windows.Media.Imaging;



/**##context*/
/**####prerequisite*/
/**######1. enable Ribbon Shell*/
/**You will need enable Ribbon Shell Mode,see [here][cw_ref Settings_Howto_Set_Shell_Modes]*/

/**######2. Configure module in this page */
/**Please see [Howto: Configure Module Via App Config][cw_ref Add_Module_Via_App_Config] or [Howto: Configure Module Via App Framework][cw_ref Add_Module_Via_AddModule_Call] */
namespace Settings_ApplicationAndShells.Shells
{
  
  /**##code*/
  /**####1. create a module */
  //{
  public class SetRibbonIconModule :  IModule
  //}
  {
      /**####2. define a `IChromeManager` field and initialize the field in the constructor */
    //{
    private IChromeManager m_chromeManager;
    public SetRibbonIconModule(IChromeManager chromeManager)
    {
        m_chromeManager = chromeManager;
    }
    //}
    /**####3. call setter of `IChromeManager.Ribbon.Icon` in`IModule.Initialize`   */
    //{
    public void Initialize()
    {
      var icon = new BitmapImage(new Uri(@"pack://application:,,,/Resources/icon.ico", UriKind.RelativeOrAbsolute));
        m_chromeManager.Ribbon.Icon = icon;
    }
    //}
    
  }
}
/**##results*/
/**the result is as follow  
 * ![Settings_Howto_SetRibbonIcon](images/Shells/Settings_Howto_SetRibbonIcon.png)
 */
/**##references*/
/**
 * + [Howto: set Shellmode][cw_ref Settings_Howto_Set_Shell_Modes]
 * + [Knowwhat: Shell mode][cw_ref Knowwhat_the_ShellModes]
 * + [Howto: Configure MOdule via Framework][cw_ref Add_Module_Via_AddModule_Call]
 * + [Howto: Configure Module via AppConfig][cw_ref Add_Module_Via_App_Config]
 */