﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
//cw Settings_Howto_Control_Window_Deactivation :10
/**##Description*/
/** Setting a handler for window deactivation which can cancel the process*/

namespace Settings_ApplicationAndShells
{
    public class ControlModule : IModule
    {
        private IChromeManager m_chromeManager;

        public ControlModule(IChromeManager chromeManager)
        {
            m_chromeManager = chromeManager;
        }

        //cw Settings_Howto_Control_Window_Deactivation : 35
        /**##Code*/
        /** Create an event handler */
        //{
        void OnWindowDeactivating(object sender, CancellableWindowEventArgs e)
        {
            // Cancel the event under certain condition
            e.Cancel = true;
        }
        //}

        public void Initialize()
        {
            //cw Settings_Howto_Control_Window_Deactivation :40
            /** Assign the event handler inside `IModule.Initialize()` method */
            //{
            m_chromeManager.WindowDeactivating += OnWindowDeactivating;
            //}
            //cw Settings_Howto_Enable_Drop_Target_Events_On_Shell :4
            /** Now you are able to access these events via IChromeManager interface: */
            //{
            m_chromeManager.DropOnShell += (sender_, args_) => { };
            m_chromeManager.DragEnterOnShell += (sender_, args_) => { };
            m_chromeManager.DragOverOnShell += (sender_, args_) => { };
            m_chromeManager.DragLeaveOnShell += (sender_, args_) => { };
            //}
            /**You might disable the individual events using the tunneled version of each event: */
            //{
            m_chromeManager.PreviewDropOnShell +=
                (o_, eventArgs_) =>
                    {
                        eventArgs_.DragEventArgs.Handled = true;
                    };
            //}
            /** You can also access the view container associated with the event. Null will be returned
             * if the event is associated with the docking space (only in TabbedDock shell mode): */
            //{
            m_chromeManager.DropOnShell +=
                (sender1_, windowEventArgs_) =>
                    {
                        if (windowEventArgs_.Window == null)
                        {
                            Console.WriteLine(@"Dropping on docking panel");
                        }
                    };
            //}
            /** You need to be aware that events my be further processed by the shell infrastructure. For example,
             * if you are setting the drag drop effect, you need to mark the event as handled. Otherwise,
             * the effect will be changed higher in the visual tree:*/
            //{
            m_chromeManager.DragOverOnShell +=
                (o1_, onWindowEventArgs_) =>
                    {
                        onWindowEventArgs_.DragEventArgs.Effects = DragDropEffects.Copy;
                        onWindowEventArgs_.DragEventArgs.Handled = true;
                    };
            //}
        }
    }
}