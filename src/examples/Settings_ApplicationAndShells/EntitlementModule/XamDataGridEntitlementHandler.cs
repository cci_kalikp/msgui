﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Infragistics.Windows.DataPresenter;
using MSDesktop.ComponentEntitlements;
using MorganStanley.MSDotNet.My;

namespace Settings_ApplicationAndShells.EntitlementModule
{
    public class XamDataGridEntitlementHandler:IComponentEntitlementHandler
    {
        internal static IMSLogger Logger = MSLoggerFactory.CreateLogger<XamDataGridEntitlementHandler>(); 
        public bool Handle(System.Windows.DependencyObject dependencyObject_, string resourceId_, IEnumerable<string> permittedActions_)
        {
            XamDataGrid grid = dependencyObject_ as XamDataGrid;
            if (grid == null) return false;
            bool viewable = permittedActions_.Contains(ComponentEntitlementsStrings.Actions.View);
            if (!viewable)
            {
                EntitlementHelper.HideElementAlways(grid);
                Logger.InfoWithFormat("Resource id: '{0}' has no '{1}' permission, hide grid: '{2}'", resourceId_, ComponentEntitlementsStrings.Actions.View, grid.Name);
            } 
            else
            {
                bool editable = permittedActions_.Contains(ComponentEntitlementsStrings.Actions.Modify);
                if (!editable)
                {
                    if (grid.FieldSettings == null)
                    {
                        grid.FieldSettings = new FieldSettings();
                    }
                    grid.FieldSettings.AllowEdit = 
                    grid.FieldLayoutSettings.AllowAddNew =  
                    grid.FieldLayoutSettings.AllowDelete = false;
                  
                    Logger.InfoWithFormat("Resource id: '{0}' has no '{1}' permission, make grid: '{2}' readonly", resourceId_, ComponentEntitlementsStrings.Actions.Modify, grid.Name);
                }
            }
            return true;
        }
    }
}
