﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infragistics.Windows.DataPresenter;
using MSDesktop.ComponentEntitlements;
using MorganStanley.MSDotNet.My;

namespace Settings_ApplicationAndShells.EntitlementModule
{
    public class FieldSettingsEntitlementHandler:IComponentEntitlementHandler
    {
        internal static IMSLogger Logger = MSLoggerFactory.CreateLogger<FieldSettingsEntitlementHandler>();
        public bool Handle(System.Windows.DependencyObject dependencyObject_, string resourceId_, IEnumerable<string> permittedActions_)
        {
            FieldSettings settings = dependencyObject_ as FieldSettings;
            if (settings == null) return false;
            bool editable = permittedActions_.Contains(ComponentEntitlementsStrings.Actions.Modify);
            if (!editable)
            {
                EntitlementHelper.ForcePropertyValue(settings, "AllowEdit", false); 
                Logger.InfoWithFormat("Resource id: '{0}' has no '{1}' permission, make field: '{2}' readonly", resourceId_, ComponentEntitlementsStrings.Actions.Modify, resourceId_);
            }
            return true;
        }
         
    }
}
