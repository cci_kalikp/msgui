﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Infragistics.Windows.DataPresenter;
using MSDesktop.ComponentEntitlements;

namespace Settings_ApplicationAndShells.EntitlementModule
{
    /// <summary>
    /// Interaction logic for View1.xaml
    /// </summary>
    public partial class View1 : UserControl
    {
        public View1()
        {
            InitializeComponent(); 
        }

        private void MenuItem_OnClick(object sender_, RoutedEventArgs e_)
        {
            txtReadOnlyField.IsEnabled = true; 
            gridData.FieldLayouts[0].Fields["ReadOnlyField"].Settings.AllowEdit = true;
        }
         
    }
}
