﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.ComponentEntitlements;

namespace Settings_ApplicationAndShells.EntitlementModule
{
    class ExceptionFieldHandler : IComponentEntitlementHandler
    {
        //cw Component_Entitlement_Custom_Handling :40
        /** Or sometimes if developer wants to neglect the entitlement check for a specific component resource, could code like this*/
        //{ 
        public bool Handle(System.Windows.DependencyObject dependencyObject_, string resourceId_, IEnumerable<string> permittedActions_)
        {
            if (resourceId_ == "EditableField")
             {
                 return true;
             }
            return false;
        }
        //}
    }
}
