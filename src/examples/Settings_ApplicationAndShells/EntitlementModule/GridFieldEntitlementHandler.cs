﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using Infragistics.Windows.DataPresenter;
using MSDesktop.ComponentEntitlements; 
using MorganStanley.MSDotNet.My;

namespace Settings_ApplicationAndShells.EntitlementModule
{
    //cw Component_Entitlement_Custom_Handling :30
    /**## Create customized EntitlementHandler */
    /** If "ComponentEntitlementsExtensions.CheckEntitlement" attached property is used to handle the control, customized handlers can be added to the handler pipe when calling the "EnableComponentEntitlements" Framework extension method*/
    /** If must implement IComponentEntitlementHandler which only has one method "Handle".*/
    /** First check if the dependencyObject_ transfered can be handled by this handler, if not, returns false, which would let handlers followed to handle it*/
    /** Then check the permittedActions to decide how to control the dependencyObject_ and returns true in the end, which suppress the handlers followed*/
    //{
    public class GridFieldEntitlementHandler:IComponentEntitlementHandler
    {
        internal static IMSLogger Logger = MSLoggerFactory.CreateLogger<GridFieldEntitlementHandler>();
        public bool Handle(DependencyObject dependencyObject_, string resourceId_, IEnumerable<string> permittedActions_)
        {
            Field field = dependencyObject_ as Field;
            if (field == null) return false; 
            bool viewable = permittedActions_.Contains(ComponentEntitlementsStrings.Actions.View);
            if (!viewable)
            { 
                EntitlementHelper.ForcePropertyValue(field, "Visibility", Visibility.Collapsed);  
                Logger.InfoWithFormat("Resource id: '{0}' has no '{1}' permission, hide field: '{2}'", resourceId_, ComponentEntitlementsStrings.Actions.View, field.Name);
            } 
            return true; 
        }   
         
    }
    //}
}
