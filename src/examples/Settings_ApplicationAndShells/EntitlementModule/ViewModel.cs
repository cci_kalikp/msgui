﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace Settings_ApplicationAndShells.EntitlementModule
{
    class ViewModel:ViewModelBase
    {

        public ViewModel()
        {
            Data = new ObservableCollection<ChildViewModel>
                {
                    new ChildViewModel() {ReadOnlyField = "Row1.Value1", EditableField = "Row1.Value2", HiddenField = "Row1.Value3"},
                    new ChildViewModel() {ReadOnlyField = "Row2.Value1", EditableField = "Row2.Value2", HiddenField = "Row2.Value3"},
                    new ChildViewModel() {ReadOnlyField = "Row3.Value1", EditableField = "Row3.Value2", HiddenField = "Row3.Value3"}
                };
            ReadOnlyField = "Value1";
            EditableField = "Value2";
            HiddenField = "Value3";
        }

        private string readonlyField;
        public string ReadOnlyField
        {
            get { return readonlyField; }
            set { readonlyField = value; OnPropertyChanged("ReadOnlyField"); }
        }

        private string editableField;
        public string EditableField
        {
            get { return editableField; }
            set { editableField = value; OnPropertyChanged("EditableField"); }
        }

        private string hiddenField;
        public string HiddenField
        {
            get { return hiddenField; }
            set { hiddenField = value; OnPropertyChanged("HiddenField"); }
        }

        private ObservableCollection<ChildViewModel> data;
        public ObservableCollection<ChildViewModel> Data
        {
            get { return data; }
            set { data = value; OnPropertyChanged("Data"); }
        }
    }

    public class ChildViewModel:ViewModelBase
    {
        private string readonlyField;
        public string ReadOnlyField
        {
            get { return readonlyField; }
            set { readonlyField = value; OnPropertyChanged("ReadOnlyField"); }
        }

        private string editableField;
        public string EditableField
        {
            get { return editableField; }
            set { editableField = value; OnPropertyChanged("EditableField"); }
        }

        private string hiddenField;
        public string HiddenField
        {
            get { return hiddenField; }
            set { hiddenField = value; OnPropertyChanged("HiddenField"); }
        }
    }
}
