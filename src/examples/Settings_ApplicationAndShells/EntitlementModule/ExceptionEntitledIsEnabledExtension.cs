﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using MSDesktop.ComponentEntitlements;
using MorganStanley.MSDotNet.MSGui.Core;

namespace Settings_ApplicationAndShells.EntitlementModule
{
    //cw Component_Entitlement_Custom_Handling :50
    /**## Create customized EntitledPropertyExtension */
    /** Specific property of the control can be bound to a entitlement based MarkupExtention, other than the default ones, developer could also create their own.*/
    /** Could either inherit from the root abstract "EntitledPropertyExtension" class or inherit from an existing one*/
    //{
    public class ExceptionEntitledVisibilityExtension : EntitledVisibilityExtension
    { 
        protected override object GetEntitledValue(DependencyObject dependencyObject_, DependencyProperty targetProperty_, IEnumerable<string> permittedActions_)
        {
            Visibility visibility = (Visibility)base.GetEntitledValue(dependencyObject_, targetProperty_, permittedActions_);
            if (visibility != Visibility.Visible)
            {
                //if the visibility is driven by entitlement of resource "EditableField", neglect it
                foreach (var binding in DependencyObjectHelper.GetBindingObjects(dependencyObject_))
                {
                    Binding binding2 = binding as Binding;
                    return (binding2 != null && (binding2.Path is PropertyPath) && binding2.Path.Path == "EditableField") ? Visibility.Visible : visibility;

                } 
            }
            return visibility;
        }
    }
    //}
}
