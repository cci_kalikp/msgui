﻿using System;
using System.Collections.Generic; 
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml.Linq;
//cw Component_Entitlement_Custom_Handling: 15
/**##Assembly*/
/**
 * You need to add a reference to the MSDesktop.ModuleEntitlements assembly.
 */
/**##Namespaces*/
/** Use the following namespaces*/
//{
using MSDesktop.ComponentEntitlements;
//}
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace Settings_ApplicationAndShells.EntitlementModule
{
    public class EntitlementModule:IModule
    {
        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;
        private const string EntitledView1ID = "EntitledView1ID";
        private const string EntitledView2ID = "EntitledView2ID";
        private const string EntitledView3ID = "EntitledView3ID";
        private const string EntitledView4ID = "EntitledView4ID";
        public EntitlementModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_)
        {
            chromeRegistry = chromeRegistry_;
            chromeManager = chromeManager_;
        }

        public void Initialize()
        {
            chromeRegistry.RegisterWindowFactory(EntitledView1ID, CreateView1);
            chromeRegistry.RegisterWindowFactory(EntitledView2ID, CreateView2);
            //cw Component_Entitlement :40
            /**##Entitled Window*/
            /**
             * "CheckEntitlement" extension method can be chained after the "IChromeManager.RegisterWindowFactory" method to if action <b>"Load"</b> is entitled for the resource of the window (resource name is either the window factory ID or specified by "Name" parameter in the "CheckEntitlement" method, resource type is <b>"MSDesktop.Component"</b>), 
             * if not entitled, the window would not be created when calling "IChromeManager.CreateWindow" method later on
             */
            //{
            chromeRegistry.RegisterWindowFactory(EntitledView3ID).SetInitHandler(CreateView3).CheckEntitlement();
            //}


            //cw Component_Entitlement_Custom_Handling :10
            /**## Window Entitlement Customization*/
            /**
             * "noEntitlementHandler" can be specified when calling the "CheckEntitlement" extension method, so if no entitlement for the window, this handler would be called; */
            /** Developer could either popup some message box and returns false to suppress the window creation; or provide an alternative content for the window and returns true. */
            //{
            chromeRegistry.RegisterWindowFactory(EntitledView4ID).SetInitHandler(CreateView4).CheckEntitlement((container_, state_) =>
            {
                container_.Title = "Window not entitled";
                container_.Content = new Label() { Content = "Window not entitled" };
                return true;
            });
            //}

            //cw Component_Entitlement :50
            /**##Entitled Widget*/
            /**
             * "CheckEntitlement" extension method can be chained after the "IChromeManager.RegisterWidgetFactory"  method to if action <b>"Load"</b> is entitled for the resource of the window (resource name is either the widget factory ID or specified by "Name" parameter in the "CheckEntitlement" method, resource type is <b>"MSDesktop.Component"</b>),
             * if not entitled, the widget would not be created when calling "IChromeManager.AddWidget" method later on
             */
            //{
            chromeRegistry.RegisterWidgetFactory("ButtonCreateEntitledView3").CheckEntitlement();
            //}

            //cw Component_Entitlement_Custom_Handling :20
            /**## Widget Entitlement Customization*/
            /**
             * "noEntitlementHandler" can be specified when calling the "CheckEntitlement" extension method, so if no entitlement for the widget, this handler would be called; */
            /** Developer could either add some log or popup some message and returns false to suppress the widget creation; or provide an alternative content for the widget and returns true. */
            //{
            chromeRegistry.RegisterWidgetFactory("ButtonCreateEntitledView4").CheckEntitlement((container_, state_) =>
                {
                    container_.Content = "Button Not Entitled";
                    return true;
                });
            //}

            var button = chromeManager.Ribbon["Entitlement"]["Views"].AddWidget("ButtonCreateEntitledView1",
                                                                                  new InitialShowWindowButtonParameters()
                                                                                      {
                                                                                          Text = "Create Entitled View using ComponentEntitlementsExtensions.CheckEntitlement attached property",
                                                                                          WindowFactoryID = EntitledView1ID,
                                                                                          InitialParameters =  new InitialWindowParameters() 
                                                                                      });


            button = chromeManager.Ribbon["Entitlement"]["Views"].AddWidget("ButtonCreateEntitledView2",
                                                                                  new InitialShowWindowButtonParameters()
                                                                                  {
                                                                                      Text = "Create Entitled View using EntitledPropertyExtension ",
                                                                                      WindowFactoryID = EntitledView2ID,
                                                                                      InitialParameters = new InitialWindowParameters() 
                                                                                  });

            button = chromeManager.Ribbon["Entitlement"]["Views"].AddWidget("ButtonCreateEntitledView3",
                                                                                   new InitialShowWindowButtonParameters()
                                                                                   {
                                                                                       Text = "Entitled Button: Create Entitled View 3",
                                                                                       WindowFactoryID = EntitledView3ID,
                                                                                       InitialParameters = new InitialWindowParameters()
                                                                                   });

            button = chromeManager.Ribbon["Entitlement"]["Views"].AddWidget("ButtonCreateEntitledView4",
                                                                       new InitialButtonParameters()
                                                                       {
                                                                           Text = "Entitled Button", 
                                                                       });

            button = chromeManager.Ribbon["Entitlement"]["Views"].AddWidget("ButtonCreateEntitledView5",
                                                           new InitialShowWindowButtonParameters()
                                                           {
                                                               Text = "Create Entitled View 4",
                                                               WindowFactoryID = EntitledView4ID,
                                                               InitialParameters = new InitialWindowParameters()
                                                           });


            button = chromeManager.Ribbon["Entitlement"]["Views"].AddWidget("GC", new InitialButtonParameters()
                {
                    Text = "GC",
                    Click = (sender_, args_) => GC.Collect()
                });
        }

        private bool CreateView1(IWindowViewContainer container_, XDocument state_)
        {
            container_.Title = "Use ComponentEntitlementsExtensions.CheckEntitlement";
            var model = new ViewModel();
            View1 v = new View1();
            v.DataContext = model;
            container_.Content = v;
            return true;
        }

        private bool CreateView2(IWindowViewContainer container_, XDocument state_)
        {
            container_.Title = "Use EntitledPropertyExtension";
            var model = new ViewModel();
            View2 v = new View2();
            v.DataContext = model;
            container_.Content = v;
            return true;
        }

        private bool CreateView3(IWindowViewContainer container_, XDocument state_)
        {
            container_.Content = new Rectangle() {Fill = new SolidColorBrush(Colors.Green)};
            return true;
        }

        private bool CreateView4(IWindowViewContainer container_, XDocument state_)
        {
            container_.Content = new Rectangle() { Fill = new SolidColorBrush(Colors.Red) };
            return true;
        }
    }
}
