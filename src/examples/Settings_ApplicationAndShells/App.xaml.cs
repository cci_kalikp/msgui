﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Security.AccessControl;
using System.Windows;
//cw Settings_Howto_Setup_Quit_Behavior :30 cw Settings_Howto_Acquire_Focus_After_Startup :30 cw Settings_Howto_Enable_Unhandled_Exception_Handler :30  cw Settings_Howto_Setup_Application_Information :30 cw Settings_Howto_Set_Shell_Modes：30 cw Settings_Howto_Set_Application_Icon :30  cw Settings_Howto_Start_Shell_In_Locked_Mode :30 cw Settings_Howto_Initialize_Log_Framework :30
/**##namespaces*/
/** use the following namespace*/
//{
using System.Windows.Controls; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
//} 

//cw Module_Entitlement :2 cw Module_Entitlement_Custom_Policies :2 
/**##Assembly*/
/**
 * You need to add a reference to the MSDesktop.Entitlement and MSDesktop.ModuleEntitlements assembly.
 */
/**##Namespaces*/
/** Use the following namespaces*/
//{
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.Desktop.Entitlement;
using MSDesktop.ModuleEntitlements;
//}

//cw Component_Entitlement :20
/**##Assembly*/
/**
 * You need to add a reference to the MSDesktop.Entitlement and MSDesktop.ModuleEntitlements assembly.
 */
/**##Namespaces*/
/** Use the following namespaces*/
//{
using MorganStanley.Desktop.Entitlement;
using MSDesktop.ComponentEntitlements;
//}

using System.Windows.Media.Imaging; 
using MorganStanley.MSDotNet.MSLog;
using System.Diagnostics; 
using MorganStanley.MSDotNet.MSGui.Themes;
using Settings_ApplicationAndShells.EntitlementModule;
using Settings_ApplicationAndShells.Shells; 
using Microsoft.Practices.Unity;

namespace Settings_ApplicationAndShells
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : Application
  {
      //cw Settings_Howto_Setup_Quit_Behavior :50 cw Settings_Howto_Acquire_Focus_After_Startup :50  cw Settings_Howto_Enable_Unhandled_Exception_Handler :50 cw Settings_Howto_Setup_Application_Information :50 cw Settings_Howto_Set_Shell_Modes :50 cw Settings_Howto_Set_Application_Icon :50  cw Settings_Howto_Start_Shell_In_Locked_Mode :50 cw Settings_Howto_Initialize_Log_Framework :50 cw Settings_Howto_Enable_Tabs_Drag_And_Drop :2 cw Settings_Howto_Set_Position_Grouped_Panes_Tab_Strip :2 cw Settings_Howto_Enable_Logging_Method_Name_Autoresolve : 2 cw Settings_Howto_Enable_Drop_Target_Events_On_Shell : 2
    /**##context*/
    /**put the code in the following method */
   //{
    protected override void OnStartup(StartupEventArgs e)
    {
      base.OnStartup(e);

      var boot = new Framework();
    //}

      boot.AddSimpleTheme();
      #region Settings_Howto_Setup_Quit_Behavior

      //cw Settings_Howto_Setup_Quit_Behavior :10

      /**##description*/
      /** you can set up the quit dialog, a quit dialog is the dialog which display when you try to close the application, normally it will warn you about unsaved profiles.
       */

      //cw Settings_Howto_Setup_Quit_Behavior :70
      /**##code*/
      //{
      
      boot.SetQuitPromptDialogBehaviour(showDontAskAgainOption: true, showSaveAndQuitOption: true, timeout: 10, warningprompt: "Are you sure to close the App?");
      //}

      /**##references*/
      /** [configuration pattern](http://where/it/links)
       */
      #endregion Settings_Howto_Setup_Quit_Behavior

      //boot.SetQuitDialogAutomaticBehaviour(10, true);

      #region Settings_Howto_Acquire_Focus_After_Startup
      //cw Settings_Howto_Acquire_Focus_After_Startup :10
      /**##description*/
      /** Whether or not to focus the newly started application
       */

      //cw Settings_Howto_Acquire_Focus_After_Startup :70

      /**##code*/
      //{
      boot.AcquireFocusAfterStartup();
      //}      
      /**##references*/
      /** [configuration pattern](http://where/it/links)
       */

      #endregion Settings_Howto_Acquire_Focus_After_Startup

      #region Settings_Howto_Enable_Unhandled_Exception_Handler
      //cw Settings_Howto_Enable_Unhandled_Exception_Handler :10
      /**##Description*/
      /** Allow yout specify the receiver in case of unhandled exception which is about to destroy the app */

      //cw Settings_Howto_Enable_Unhandled_Exception_Handler :70

      /**##code*/

      //{
      boot.EnableExceptionHandler(System.Environment.UserName + "@ms.com", "msguiadministrators@ms.com");
      //}
      /**##references*/
      /** [configuration pattern](http://where/it/links)
       */
      #endregion Settings_Howto_Enable_Unhandled_Exception_Handler

      #region Settings_Howto_Setup_Application_Information
      //cw Settings_Howto_Setup_Application_Information :10
      /**##Description*/
      /** You can set */
      /**
       * * 1.application name
       * * 2.application version
       * 
       * this can affect the text displayed on splash screen and the text in the logs
       */


      //cw Settings_Howto_Setup_Application_Information :70
      /**##code*/

      //{
      boot.SetApplicationName("Settings_ApplicationAndShells");
      boot.SetApplicationVersion(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());

      //}
      /**##references*/
      /**
       * + [configuration pattern](http://where/it/links)
       * + [splash screen](http://where/it/links) 
       * + [application logs][cw_ref Settings_Howto_Initialize_Log_Framework]
       */
      #endregion Settings_Howto_Setup_Application_Information

      #region Settings_Howto_Set_Shell_Modes
      //cw Settings_Howto_Set_Shell_Modes :10
      /**##Description*/
      /**Allow you to select the shell mode, by default RibbonShell will be assumed */
      //cw Settings_Howto_Set_Shell_Modes :70

      /**##code*/
      //{
      boot.SetShellMode(ShellMode.RibbonWindow);
      //}
      /**##references*/
      /** [configuration pattern](http://where/it/links)
       */
      #endregion Settings_Howto_Set_Shell_Modes

      #region Settings_Howto_Set_Application_Icon
      //cw Settings_Howto_Set_Application_Icon :10
      /**##Description*/
      /**set the icon of the app displayed in the task bar */
      //cw Settings_Howto_Set_Application_Icon :70

      /**##code*/
      //{
      boot.SetApplicationIcon(BitmapFrame.Create(new Uri(@"pack://application:,,,/Resources/icon.ico", UriKind.RelativeOrAbsolute)));
      //}
      /**##references*/
      /** [configuration pattern](http://where/it/links)
       */
      #endregion Settings_Howto_Set_Application_Icon

      

      #region Settings_Howto_Start_Shell_In_Locked_Mode
      //cw Settings_Howto_Start_Shell_In_Locked_Mode: 10
      /**##Description*/
      //**Lock the shell, do not allow user to modify the shell layout*/
      //cw Settings_Howto_Start_Shell_In_Locked_Mode :70

      if (false)
      {
          /**##code*/
          //{
          boot.HideOrLockShellElements(HideLockUIGranularity.LockedLayoutMode);
          //}
      }

            /**##references*/
      /** [configuration pattern](http://where/it/links)
       */
      #endregion Settings_Howto_Start_Shell_In_Locked_Mode

      #region Settings_Howto_Initialize_Log_Framework
      //cw Settings_Howto_Initialize_Log_Framework :10
      /**##Description*/
      /** initialize the app's log*/
      //cw Settings_Howto_Initialize_Log_Framework :70
      /** The easiest way to set up logging is to just call*/
      if (false)
      //{
        boot.EnableLogging();
        //}
            /** However, that would enable logging accross all log levels. To have more fine grained logging, look for the overrides hereby: */
      if (false)
      {
          //{
          boot.EnableLogging(
              // you can override the default Directory that stores the logs
              applicationName: "SuperApplication",
              //by default, the log file will be log{process_id}.log, you can override the log basename
              logbaseName: "dev" + Process.GetCurrentProcess().Id.ToString(),
              // default loggingLevels is null. Null or empty means "*/*/*:ALL"
              loggingLevels: new Dictionary<string, string>
                                 {
                                     // add your log levels here.
                                     {MSLogTarget.Root.Path, "*/*/*:ALL"},
                                     // and multiple rules for a policy
                                     {MSLogTarget.Default.Path, "FixATDL/*/*:Info,IED/CFM.Infrastructure/*:Info"}
                                 });
          //}
      }
        /**##references*/
        /** [configuration pattern](http://wiki-na.ms.com/twiki/cgi-bin/view/MSDotnet/DesignGuideMSLog80)
         */
      #endregion Settings_Howto_Initialize_Log_Framework

      #region Settings_Howto_Control_Window_Deactivation
      //cw Settings_Howto_Control_Window_Deactivation :20
      /**##Context*/
      /** First one must enable deactivation notifications inside `Application.OnStartup()` method */
      if (false)
          //{
          boot.SetWindowDeactivatingNotificationEnabled(true);
      //}
      #endregion Settings_Howto_Control_Window_Deactivation

      #region Settings_Howto_Enable_Tabs_Drag_And_Drop

      //cw Settings_Howto_Enable_Tabs_Drag_And_Drop :1
      /**##description*/
        /**This page describes how to enable drag and drop behaviour of tabs when using a shell with tabbed dock.
         * The behaviour is disabled by default because it requires you to introduce a new dependency.
         */
      //cw Settings_Howto_Enable_Tabs_Drag_And_Drop :4
      /**Add the dependency to InfragisticsWPF4.DragDrop in the same version as the rest of your Infragstics
        * references.
        */
        /**##code*/
        /**Enable the switch using framework extensions:*/
        if (false)
        {
            //{
            boot.EnableTabDragAndDrop();
            //}
        }

      #endregion

        #region Settings_Howto_Set_Position_Of_Subtabs
        //cw Settings_Howto_Set_Position_Grouped_Panes_Tab_Strip :1
        /**##description*/
        /**Panes can be docked together to form a group so that the user can switch between them with tabs.
         * The position of these tabs is by default at the bottom of the pane group. However, this can be changed
         * with the following setting.*/
        //cw Settings_Howto_Set_Position_Grouped_Panes_Tab_Strip :4
        /**##code*/
        if (false)
        {
            /**Set the tab strip positions to the left side of the pane group:*/
            //{
            boot.SetTabGroupTabStripPlacement(Dock.Left);
            //}
        }

        #endregion 
        
        #region Settings_Howto_Enable_Logging_Method_Name_Autoresolve
        //cw Settings_Howto_Enable_Logging_Method_Name_Autoresolve :1
        /**##description*/
        /**When using MSLog you have a possibilty to provide a method name. MSDesktop allows you
         * to automate this and can deduce the method name by analyzing the stacktrace. Bear
         * in mind this operation is costly so toggling autoresolve on may have impact on performance.
         * */
        //cw Settings_Howto_Enable_Logging_Method_Name_Autoresolve :4
        /**##code*/
        if (false)
        {
            //{
            boot.EnableAutoResolveMethodNameForLogging();
            //}
        }
        #endregion

        #region Settings_Howto_Enable_Drop_Target_Events_On_Shell
        //cw Settings_Howto_Enable_Drop_Target_Events_On_Shell :1
        /**##description*/
        /**It is possible to access drop target events such as Drop, DragEnter, etc on shell elements. 
         * This may come in handy for example if you want to handle drag and drop of items from external
         * applications. Depending on the shell mode, you might be able to handle drops on shell windows
         * and on the docking panel.
         * */
        //cw Settings_Howto_Enable_Drop_Target_Events_On_Shell :4
        /**##code*/
        /**Firstly, you need to enable these events via framework extension:*/
        #endregion

        if (false)
        {
            //{
            boot.EnableDragDropTargetEventsOnShell();
            //}
        }
            
        boot.AddModule<SetRibbonIconModule>();
        

        #region Module entitlement context

        //cw Module_Entitlement :3 cw Module_Entitlement_Custom_Policies :3 cw Component_Entitlement :30
        /**##Context*/
        /**
         * In order to use this feature you need to provide an instance of IEntitlementService to the framework. This
         * interface comes from MSDesktop.Entitlement assembly which is a common API for interacting with different 
         * entitlement providers. MSDesktop.Entitlement provides you with classes such as EclipseEntitlementService,
         * OfflineEntitlementService or LdapEntitlementService.
         */
        /**
         * An example configuration of an OfflineEntitlementService: 
         */
        //{
        var entitlementService = new OfflineEntitlementService();
        //}

        //cw Module_Entitlement :4 cw Module_Entitlement_Custom_Policies :4
        //{
        entitlementService.AddAuthorized(new ModuleResource
                                            {
                                                Type = typeof(ModuleA)
                                            },
                                            ModuleEntitlementsStrings.Actions.Load);
        entitlementService.AddAuthorized(new ModuleResource
                                            {
                                                Name = "moduleB"
                                            },
                                            ModuleEntitlementsStrings.Actions.Load);
        //}

        #endregion

        #region Module_Entitlement

        //cw Module_Entitlement :1
        /**##Description*/
        /**
         * The framework allows to control which modules are loaded based on queries to various entitlement services.
         */

        if (false)
        {
            //cw Module_Entitlement :5
            /**##Code*/
            /**
             * In order to enable entitlement-based module loading simply use the following framework
             * extension:
             */
            //{
            boot.EnableModuleEntitlements(entitlementService);
            //}
            /**
             * If you want to set the entitlement service after user logged in, usually to use impersonation for entitlement, you could use this 
             */
            //{
            boot.EnableModuleEntitlements(container_ => new EclipseEntitlementService("Domain_1", container_.Resolve<string>("CurrentConcordUser"), "e3farm-hkt:2232")); 
            //}
            /**
             * In order to simplify the transition to entitlement-based module loading, by default
             * modules have to opt-in for entitlement check. In order to indicate that a module
             * should be checked for entitlement, you need to change the way it's added.
             * */
            /**
             * Instead of Framework.AddModule call, please use the following call:
             */
            //{
            boot.AddModuleWithEntitlementCheck<ModuleA>();
            //}

            //cw Module_Entitlement :7
            /**
             * Alternatively, you can specify that module DO NOT have to opt-in for entitlement check. 
             * Bear in mind that this will mean that all of the modules will be checked for entitlements.
             */
            //{
            boot.EnableModuleEntitlements(entitlementService, modulesMustOptIn: false);
            //}
        }

        #endregion

        #region Module_Entitlement_Custom_Policies
        //cw Module_Entitlement_Custom_Policies :1
        /**##Description*/
        /**
         * The framework distinguishes three kinds of situations in which a module can be blocked:
         * <ul>
         *  <li>A user is not entitled to load a module (a "NO" situation)</li>
         *  <li>There is no information regarding the module in the entitlement service (a "DON'T KNOW" situation)</li>
         *  <li>At least one of the modules that the module is dependent on is blocked</li>
         * </ul>
         * 
         * By default, each of these situations will result in the module being blocked. However, you can 
         * implement custom logic that will decide whether to block a module or not.
         * 
         */
        //cw Module_Entitlement_Custom_Policies :6
        /**##Code*/
        /**
         * In order to implement a custom blocking policy, please provide the following parameter to 
         * the EnableModuleEntitilements call:
         */
        //{
        boot.EnableModuleEntitlements(entitlementService,
            blockingModuleCallback: (sender, args) =>
                                       {
                                           switch (args.Cause)
                                           {
                                                case ModuleStatus.BlockedByNoEntitlementsInfo:
                                                   args.Cancel = true;
                                                   break;
                                                case ModuleStatus.BlockedByEntitlements:
                                                case ModuleStatus.DependentModuleBlocked:
                                                   break;
                                           }
                                       });
        //}
        /**
         * The above call will cancel blocking in a DON'T KNOW situation (resulting in the module being loaded).
         * However, modules will still be blocked in the remaining situations.
         */
        #endregion

        #region Module_Entitlement_Eclipse_Specific_Setup
        //cw Module_Entitlement_Eclipse_Specific_Setup :1
        /**##Eclipse E3 custom module blocking policies*/
        /**
         * One of possible entitlement service providers compatible with the module loading entitlement feature
         * is Eclipse E3. There is an existing issue regarding this provider. Eclipse E3 does not distinguish between
         * the following situations:
         * <ul>
         *  <li>A user is not entitled to load a module (a "NO" situation)
         *  <li>There is no information regarding the module in the entitlement service (a "DON'T KNOW" situation)
         * </ul>
         * The ability to distinguish is important when you want to implement custom policy of allowing/blocking
         * modules which fall into the "DON'T KNOW" situation.
         * 
         * We solve the problem with the following approach. We introduce the <b>Ping</b> action which should 
         * be always granted to all modules.
         * 
         * Then:
         * <ul>
         *  <li>a module which does not have Load action but does have Ping action falls into "NO" situation
         *  <li>a module which does not have neither Load nor Ping action falls into "DON'T KNOW" situation
         * </ul>
         */
        #endregion

        #region Component_Entitlement
        //cw Component_Entitlement :10
        /**##Description*/
        /**
         * The framework allows to control if a specific window, widget or control can be loaded/enabled based on queries to various entitlement services.
         */
        //cw Component_Entitlement : 31
        /**
         * Usually resource of type <b>"MSDesktop.Component"</b> and resource name same as the component id found through the entitlement service is used to check for the actions allowed for a specific component
         */
        //cw Component_Entitlement :32
        //{ 
        entitlementService.AddAuthorized(new ComponentResource { ID = "Command1" }, new[] { ComponentEntitlementsStrings.Actions.Execute });
        entitlementService.AddAuthorized(new ComponentResource { ID = "ReadOnlyField" }, new[] { ComponentEntitlementsStrings.Actions.View });
        entitlementService.AddAuthorized(new ComponentResource { ID = "EditableField" }, new[] { ComponentEntitlementsStrings.Actions.View, ComponentEntitlementsStrings.Actions.Modify });
        entitlementService.AddAuthorized(new ComponentResource { ID = "Data" }, new[] { ComponentEntitlementsStrings.Actions.View, ComponentEntitlementsStrings.Actions.Modify });
        entitlementService.AddAuthorized(new ComponentResource { ID = "Data.ReadOnlyField" }, new[] { ComponentEntitlementsStrings.Actions.View });
        entitlementService.AddAuthorized(new ComponentResource { ID = "Data.EditableField" }, new[] { ComponentEntitlementsStrings.Actions.View, ComponentEntitlementsStrings.Actions.Modify });
        entitlementService.AddAuthorized(new ComponentResource { ID = "EntitledView3ID" }, new[] { ComponentEntitlementsStrings.Actions.Load });
        entitlementService.AddAuthorized(new ComponentResource { ID = "ButtonCreateEntitledView3" }, new[] { ComponentEntitlementsStrings.Actions.Load });
        //}

        //entitlementService.AddAuthorized(new ComponentResource { ID = "Command3" }, new string[0]); 
        //entitlementService.AddAuthorized(new ComponentResource { ID = "EntitledView4ID" }, new[] { ComponentEntitlementsStrings.Actions.Load });
        //entitlementService.AddAuthorized(new ComponentResource { ID = "ButtonCreateEntitledView4" }, new[] { ComponentEntitlementsStrings.Actions.Load });

        //cw Component_Entitlement :33
        /**##Code*/
        /**
         * In order to enable entitlement-based component enablement/visibility simply use the following framework extension*/
        if (1 == 2)
        {
            //{
            boot.EnableComponentEntitlements(entitlementService);
            //}
            /**
             * If you want to set the entitlement service after user logged in, usually to use impersonation for entitlement, you could use this 
             */
            //{
            boot.EnableModuleEntitlements(container_ => new EclipseEntitlementService("Domain_1", container_.Resolve<string>("CurrentConcordUser"), "e3farm-hkt:2232"));
            //}
        }
        /** Some customized component entitlement handlers can also be provided, see [Custom Entitlement Handling][cw_ref Component_Entitlement_Custom_Handling]*/ 
        //{
        boot.EnableComponentEntitlements(entitlementService, new XamDataGridEntitlementHandler(), new GridFieldEntitlementHandler(), new FieldSettingsEntitlementHandler(), new ExceptionFieldHandler());
        //}
        /**If the entitlement resource is not defined using the exact default resource type, resource id and resource action, the mapping can also be redefined */
        if (1 == 2)
        {
            //{
            boot.EnableComponentEntitlements(entitlementService, resourceFactory_: delegate(string componentId_)
            {
                if (componentId_.StartsWith("TradeView"))
                {
                    return new TradeTypeResource() { TradeTypeCode = "OTCOption" };
                }
                if (componentId_.StartsWith("BulkLoader"))
                {
                    return new TradeTypeResource() { TradeTypeCode = "Varswap" };
                }
                return null;
            }, customExecuteActionName_: "Execute", //action for entitle command buttons
customLoadActionName_: "Load", //action for loading windows and widgets through IChromeManager 
customModifyActionName_: "Edit", //action for modify editable controls
customViewActionName_: "View" //action for view contents
);
            //}
        } 

 
        boot.AddModule<EntitlementModule.EntitlementModule>();
        #endregion

        //cw Settings_Howto_Setup_Quit_Behavior :55 cw Settings_Howto_Acquire_Focus_After_Startup :55 cw Settings_Howto_Enable_Unhandled_Exception_Handler :55  cw Settings_Howto_Setup_Application_Information :55 cw Settings_Howto_Set_Shell_Modes：55 cw Settings_Howto_Set_Application_Icon :55  cw Settings_Howto_Start_Shell_In_Locked_Mode :55 cw Settings_Howto_Initialize_Log_Framework :55 cw Settings_Howto_Enable_Tabs_Drag_And_Drop :3 cw Settings_Howto_Set_Position_Grouped_Panes_Tab_Strip :3 cw Settings_Howto_Enable_Logging_Method_Name_Autoresolve :3 cw Settings_Howto_Enable_Drop_Target_Events_On_Shell :3
        //{
        //put your code here 
        boot.Start();

    }
    //}

  }
    //cw Component_Entitlement :34
    //{
    [EntitlementResource(ResourceType = "TradeType")]
    public class TradeTypeResource
    {
        [EntitlementResourceProperty("tradeTypeCode")]
        public string TradeTypeCode { get; set; }
    }
    //}

    internal class ModuleA : IModule
    {
        //cw Module_Entitlement :8
        /**
         * It is possible to get information about which modules were blocked and for what reason.
         * All is needed is to grab an instance of IModuleEntitlementsStatusInfo in one of your 
         * modules' constructors.
         */
        //{
        public ModuleA(IModuleEntitlementsStatusInfo info)
        {
            foreach (var moduleStatusInfo in info.BlockedModules)
            {
                Console.WriteLine(moduleStatusInfo.ModuleName);
            }
        }
        //}

        public void Initialize()
        {
        }
    }
}
