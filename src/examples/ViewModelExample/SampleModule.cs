﻿using System.Windows.Media;
using MSDesktop.ComponentEntitlements;
using Microsoft.Practices.Unity;
using MSDesktop.ViewModelAPI;
using MSDesktop.ViewModelAPI.Entitlements;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace ViewModelExample
{
    //cw ViewModelAPI_Entitlements :5
    /**Annotate a ViewModelBasedModule with the below annotation. It specifies that for the Dev
     * environment the entitlement service should be checked for "SampleModule" resource.
     * There will be no checks for qa and uat. In prod the user is never allowed to load this module.
     */
    //{
    [EntitlementResource(Dev = "SampleModule",
        Qa = EntitlementResourceAttribute.NoEclipsePermsRequired,
        Uat = EntitlementResourceAttribute.NoEclipsePermsRequired,
        Prod = EntitlementResourceAttribute.NotAllowed)]
    //}
    //cw ViewModelAPI_Intro :5
    /**Define a module. Inheriting from ViewModelBasedModule will allow you to use convenience methods.*/
    //cw ViewModelAPI_Intro :5 cw ViewModelAPI_Entitlements :7
    //{
    public class SampleModule : ViewModelBasedModule
    //}
    {
        private IChromeRegistry chromeRegistry;
        public SampleModule(IUnityContainer unityContainer_) : base(unityContainer_)
        {
            unityContainer_.RegisterType<ISampleService, SampleService>();
            chromeRegistry = unityContainer_.Resolve<IChromeRegistry>();
        }

        public override void Initialize()
        {
            //cw ViewModelAPI_Intro :6
            /**Use the AddRibbonButton convenience method. You need to specify the view type,
             * the viewmodel type and the parameters type. The call will result in adding a button to the
             * ribbon. The button will open a view bound to a new instance of the provided
             * view model type. When the user profile is saved, the view's state will be persisted
             * as an object of the provided parameters type. The persisted state would be used to load
             * the state of the view when the profile is loaded.
             *
             * Arguments specify respectively: view id, view title, ribbon tab, ribbon group, icon.
             * 
             * And you could also enable [component entitlement check][cw_ref Component_Entitlement] based on the view id 
             */
            //{
            string viewId = "{D780D18C-A0DE-4A88-8E65-3472F08CDD93}.sampleViewId";
            chromeRegistry.RegisterWidgetFactory(viewId).CheckEntitlement();

            AddRibbonButton<SampleView, SampleViewModel, SampleViewParameters>(
                viewId,
                "SampleView", "Home", "Views",null);
            //}
            if (false)
            {
                /**There is also a similar method in case you are using the launcher bar based shell: */
                //{
                AddLauncherButton<SampleView, SampleViewModel, SampleViewParameters>("{D780D18C-A0DE-4A88-8E65-3472F08CDD93}.sampleViewId",
                                                                                     "SampleView", null,  "Home");
                //}
            }
        }
    }
}
