﻿using System; 
using MSDesktop.ViewModelAPI;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace ViewModelExample
{
    //cw ViewModelAPI_Intro :4
    /** Define a viewmodel inheriting from IFrameworkViewModel<>. It has to be parametrized with the type
     * of viewmodel parameters. The parameters object represents the state of the view and will be automatically
     * persisted when a user profile is saved.*/
    //{
    public class SampleViewModel : ViewModelBase, IFrameworkViewModel<SampleViewParameters>
    //}
    {
        private readonly ISampleService m_sampleService;

        public SampleViewModel(ISampleService sampleService_)
        {
            m_sampleService = sampleService_;
        }

        private bool m_someValue;
        public bool SomeValue
        {
            get
            {
                return m_someValue;
            }
            set
            {
                m_someValue = value;
                OnPropertyChanged("SomeValue");
            }
        }

        public string Price
        {
            get
            {
                return m_sampleService.GetPrice().ToString();
            }
        }

        public void Dispose()
        {
        }

        public void SetState(SampleViewParameters state_)
        {
            m_someValue = state_.SavedInput;
        }

        public SampleViewParameters GetState()
        {
            return new SampleViewParameters {SavedInput = SomeValue};
        }

        public bool CanClose
        {
            get
            {
                return true;
            }
        }

        public void SetCloseAction(Action close_)
        {
        }

        public void Closed()
        {
        } 
    }
}
