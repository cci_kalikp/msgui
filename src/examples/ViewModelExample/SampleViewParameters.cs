﻿using System.Xml.Linq;
using MSDesktop.ViewModelAPI;

namespace ViewModelExample
{
    public class SampleViewParameters : IFrameworkViewParameters
    {
        public bool SavedInput { get; set; }

        public int Version { get; set; }

        public IFrameworkViewParameters GetDefault()
        {
            return new SampleViewParameters { SavedInput = false };
        }

        public XDocument Upgrade(int version_, XDocument xDocument_)
        {
            return xDocument_;
        }
    }
}
