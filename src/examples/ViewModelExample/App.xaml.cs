﻿//cw ViewModelAPI_Intro :1
/**##Description*/
/**The ViewModel API is the simplified API to MSDesktop that accomodates the most
 * common use cases. The API allows you to rapidly add a MVVM-based view along with
 * a button responsible for showing that view. The ViewModel API reduces complexity
 * and the amount of boilerplate code in the typical scenarios and also integrates
 * better with the MVVM pattern.*/

//cw ViewModelAPI_Entitlements :1
/**##Description*/
/**The ViewModel API integrates well with the entitlement-based module loading. It allows 
 * you to specify, for each ViewModel-based module, which entitlement resource should be associated
 * with it based on the current environment. It is for example very easy to express that a module doesn't need 
 * entitlement checks in the dev environment but it does in the qa environment.*/

//cw ViewModelAPI_Intro :2 cw ViewModelAPI_Entitlements :2
/**##Context*/
/**Reference MSDesktop.ViewModelAPI.*/
//cw ViewModelAPI_Entitlements :2
/**Reference MSDesktop.ModuleEntitlements.*/
/**Reference MSDesktop.Environment.*/

//cw ViewModelAPI_Intro :3 cw ViewModelAPI_Entitlements :3
/**##Code*/

using System.Windows;
using MorganStanley.Desktop.Entitlement;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Themes;
using MSDesktop.Environment;
using MSDesktop.ViewModelAPI.Entitlements;
using MSDesktop.ModuleEntitlements;

namespace ViewModelExample
{
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var boot = new Framework();
            boot.AddSimpleTheme();
            boot.EnableViewModelApiEntitlements();

            //cw ViewModelAPI_Entitlements :8
            /**When using entitlements you need to add your viewmodel-based module using the following
             * extension method:*/
            //{
            boot.AddViewModelBasedModule<SampleModule>();
            //}
            var service = new OfflineEntitlementService();
            service.AddAuthorized(new ModuleResource { Name = "SampleModule" }, new[] { ModuleEntitlementsStrings.Actions.Load });
            
            //cw ViewModelAPI_Entitlements :4
            /**Enable environment support and module entitlements.*/
            //{
            boot.EnableModuleEntitlements(service);
            boot.EnableEnvironment(EnvironmentExtensions.EnvironProviderId.CommandLine,
                EnvironmentExtensions.RegionProviderId.None,
                EnvironmentExtensions.UsernameProviderId.None);
            //}

            boot.HideOrLockShellElements(HideLockUIGranularity.HideMainTabwell);
            boot.Start();
        }
    }
}
