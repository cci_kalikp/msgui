﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ViewModelExample
{
    public interface ISampleService
    {
        double GetPrice();
    }
}
