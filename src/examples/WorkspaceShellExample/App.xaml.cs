﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using MSDesktop.WorkspaceShell;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace WorkspaceShellExample
{
    public partial class App : Application
    {
        public class TestModule : IModule
        {
            private readonly IChromeManager _manager;
            private readonly IChromeRegistry _registry;

            public TestModule(IChromeManager manager_, IChromeRegistry registry_)
            {
                _manager = manager_;
                _registry = registry_;
            }

            public void Initialize()
            {
                _registry.RegisterWindowFactory("aaa",
                                                (container_, state_) =>
                                                    {
                                                        container_.Content = new TextBox { MinHeight = 200 };
                                                        container_.Title = "Alerts";
                                                        return true;
                                                    });
                _registry.RegisterWindowFactory("bbb",
                                                (container_, state_) =>
                                                    {
                                                        container_.Content = new TextBox { MinHeight = 200 };
                                                        container_.Title = "Analytics Driver";
                                                        return true;
                                                    });
                _manager[ChromeArea.WorkspaceRootArea]["Group1"].AddWidget(
                    new InitialShowWindowButtonParameters
                        {
                            WindowFactoryID = "aaa",
                            Text = "hello",
                            Image =
                                new BitmapImage(
                        new Uri(
                            "pack://application:,,,/WorkspaceShellExample;component/SampleModulePreview.png",
                            UriKind.RelativeOrAbsolute)),
                            ToolTip = "Lorem ipsum 1"
                        });
                _manager[ChromeArea.WorkspaceRootArea]["Group1"].AddWidget(
                    new InitialShowWindowButtonParameters
                        {
                            WindowFactoryID = "bbb",
                            Text = "hello2",
                            Image =
                                new BitmapImage(
                        new Uri(
                            "pack://application:,,,/WorkspaceShellExample;component/SampleModulePreview.png",
                            UriKind.RelativeOrAbsolute)),
                            ToolTip = "Lorem ipsum 2"
                        });
            }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var boot = new Framework();
            boot.AddWorkspaceShellTheme();
            boot.AddModule<TestModule>();
            boot.HideOrLockShellElements(HideLockUIGranularity.HideNewTabButton | HideLockUIGranularity.HideStatusBar |
                                         HideLockUIGranularity.HideViewMaximizeButton |
                                         HideLockUIGranularity.HideViewMinimizeButton |
                                         HideLockUIGranularity.HideViewPinButton |
                                         HideLockUIGranularity.HideViewTearOffButton |
                                         HideLockUIGranularity.WorkspaceShellStyleDragDropLock);
            boot.EnableTabDragAndDrop();
            boot.SetWorkspaceShellMode();
            boot.AcquireFocusAfterStartup();
            boot.SetViewsSpacingAndShadowSize(5, 0);
            boot.SetDockedWindowPadding(0);
            boot.EnableLogging();
            boot.EnableExceptionHandler("msguiadministrators@ms.com");
            //boot.EnableWorkspaceSupport();
            //boot.EnableViewHistory(5);

            boot.Start();
        }

    }
}