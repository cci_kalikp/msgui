﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.Routing;

namespace MorganStanley.Msdotnet.Msgui.CefHtmlSlicing
{
    [Serializable]
    public class YokaiMessage : IExplicitTarget
    {
        /// <summary>
        /// Always set to the local endpoint.
        /// </summary>
        internal static Endpoint Target;
        Endpoint IExplicitTarget.Target
        {
            get
            {
                return YokaiMessage.Target;
            }
        }

        /// <summary>
        /// The contained Yokai message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Source application as used in Yokai. 
        /// </summary>
        public string SourceApp { get; set; }

        /// <summary>
        /// Target application as used in Yokai. 
        /// </summary>
        public string TargetApp { get; set; }
    }
}
