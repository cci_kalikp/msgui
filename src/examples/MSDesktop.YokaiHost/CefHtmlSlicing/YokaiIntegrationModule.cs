﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.Routing;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ControlParadigm;
using MorganStanley.MSDotNet.MSGui.Impl;

namespace MorganStanley.Msdotnet.Msgui.CefHtmlSlicing
{
    public class YokaiIntegrationModule : IEarlyInitializedModule
    {
        private readonly IRoutingCommunicator communicator;
        private readonly IEndpointSelector endpointSelector;

        public YokaiIntegrationModule(IRoutingCommunicator communicator, IEndpointSelector endpointSelector)
        {
            this.communicator = communicator;
            this.endpointSelector = endpointSelector;

            this.endpointSelector.AddEndpointSelectionStep(2999, YokaiEndpointSelectionStep); /* 2999 puts this right at the top of the chain */
        }

        public void Initialize()
        {
            //empty on purpose
        }

        private EndpointSelection YokaiEndpointSelectionStep(EndpointSelectorChain chain)
        {
            return chain.Message is YokaiMessage
                       ? new EndpointSelection { Endpoint = Endpoint.LocalEndpoint, Persist = false }
                       : chain.Next();
        }
    }

    public static class YokaiExtensions
    {
        public static void EnableYokaiIntegration(this Framework f)
        {
            f.AddModule<YokaiIntegrationModule>();
        }
    }
}
