﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using CefSharp.Wpf;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.Routing;
using MSDesktop.WebSupport;
using MSDesktop.WebSupport.CEF;
using Microsoft.Practices.Unity;

namespace MorganStanley.Msdotnet.Msgui.CefHtmlSlicing
{
    internal class YokaiApplicationsHost : CEFBasedURLModule
    {
        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;

        #region JS integration magic strings

        private const string YokaiAdapterJSObjectName = "MSDesktop_Yokai_Adapter_80FC55E092664504B7BA988AE54C154C";
        private const string MessageInjectionJS = @"
(function() {{
 var windows = document.getElementById('x').contentWindow.YokaiWindows;
 if (!windows) {{
  return;
 }}
 var targetWindow = windows['{0}'];
 if (!targetWindow) {{
  return;
 }}
 var Yokai;
 try {{
  Yokai = targetWindow.Yokai;
 }} catch (e) {{
  console.error('FATAL: Could not cross the domain boundary - '+e.message);
 }}
 if (Yokai) {{
  var connection = Yokai.messaging.MSDesktopConnection.mailbox.get({{source:'{0}', target: '{1}'}});
  if (connection) {{
   Yokai.Logger.debug('MSDesktop: s:' + '{0}' + ' t:' + '{1}' + ' m: ' + '{2}');
   connection.fireMessageReceived('{2}');
  }}
 }}
}})();
";
        internal const string YokaiRootContainerName = "rootContainerFakeApp";

        #endregion

        protected static YokaiApplicationsHost RootContainer;

        private readonly IRoutingCommunicator routingCommunicator;
        private readonly IRoutedPublisher<YokaiMessage> publisher;
        private WebView webControl;
        private readonly List<string> applications = new List<string>();
        
        public YokaiApplicationsHost(IUnityContainer container, IChromeRegistry chromeRegistry, IChromeManager chromeManager)
            : base(container)
        {
            this.chromeRegistry = chromeRegistry;
            this.chromeManager = chromeManager;
            this.routingCommunicator = container.Resolve<IRoutingCommunicator>();

            // we set this so Yokai messages always stay inside the app. This can be enhanced later on.
            YokaiMessage.Target = Endpoint.LocalEndpoint;

            // set up kraken communication
            publisher = this.routingCommunicator.GetPublisher<YokaiMessage>();
            this.routingCommunicator.GetSubscriber<YokaiMessage>().GetObservable().Subscribe(YokaiMessageReceived);

            this.RetainInitializerControl = true;
        }
        
        /// <summary>
        /// We override this to inject our Yokai adapter object
        /// </summary>
        public override void OnModuleInitialized()
        {
            base.OnModuleInitialized();

            this.webControl = _htmlInitializerControl.Control as WebView;
            this.webControl.RegisterJsObject(YokaiAdapterJSObjectName, this);

            if (!string.IsNullOrWhiteSpace(WebSupportExtensions.Registry[this].Title))
            {
                var factoryId = WebSupportExtensions.Registry[this].Url.ToString();
                this.chromeRegistry.RegisterWindowFactory(factoryId, (container, state) =>
                {
                    container.Parameters.Singleton = true; //todo farm ease this
                    (this.webControl.Parent as ContentControl).Content = null;
                    container.Content = this.webControl;
                    container.Title = WebSupportExtensions.Registry[this].Title;

                    return true;
                });

                this.chromeManager.PlaceWidget(Guid.NewGuid().ToString(), WebSupportExtensions.Registry[this].Location, new InitialButtonParameters
                {
                    Click = (sender, args) =>
                    {
                        this.chromeManager.CreateWindow(factoryId, new InitialWindowParameters { Singleton = true });
                        //webControl.ExecuteScript(File.ReadAllText("injection.js"));
                    },
                    Text = WebSupportExtensions.Registry[this].Title
                });
            }
        }

        /// <summary>
        /// Handles Yokai messages that arrive through Kraken
        /// </summary>
        /// <param name="message"></param>
        private void YokaiMessageReceived(YokaiMessage message)
        {
            if (!this.applications.Contains(message.TargetApp))
            {
                Debug.WriteLine(string.Format("YAH: ignoring message for {0}", message.TargetApp));
                return;
            }
            this.webControl.ExecuteScript(string.Format(MessageInjectionJS, message.TargetApp, message.SourceApp, message.Message));
        }

        #region Yokai JSVM - .net calls

        /// <summary>
        /// Register an application with the YokaiApplicationsHost
        /// </summary>
        /// <param name="appName"></param>
        public void RegisterApplication(string appName)
        {
            if (this.applications.Contains(appName))
                return;

            this.applications.Add(appName);

            if (appName == YokaiRootContainerName)
            {
                YokaiApplicationsHost.RootContainer = this;
            }
        }

        /// <summary>
        /// Publishes a message from Yokai onto Kraken
        /// </summary>
        /// <param name="sourceAppName">Source Yokai app</param>
        /// <param name="targetAppName">Target Yokai app</param>
        /// <param name="message">The payload</param>
        public void Publish(string sourceAppName, string targetAppName, string message)
        {
            this.publisher.Publish(new YokaiMessage { SourceApp = sourceAppName, TargetApp = targetAppName, Message = message });
        }

        /// <summary>
        /// Remove an application from the hosting YokaiApplicationsHost
        /// </summary>
        /// <param name="appName">The app to remove</param>
        public void RemoveApplication(string appName)
        {
            this.applications.Remove(appName);

            if (appName == YokaiApplicationsHost.YokaiRootContainerName)
            {
                throw new InvalidOperationException("Unregistering the root container is not allowed.");
            }
        }

        #endregion
    }
}
