﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq.Expressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;
using CefSharp.Wpf;
using MSDesktop.MessageRouting.Routing;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.DynamicType;
using MSDesktop.MessageRouting.Message;
using MSDesktop.WebSupport;
using MSDesktop.WebSupport.CEF;
using Expression = System.Linq.Expressions.Expression;

namespace MorganStanley.Msdotnet.Msgui.CefHtmlSlicing
{
    class SlicingWebIntegration : CEFBasedURLModule
    {
        internal const string YokaiRootContainerName = "rootContainerFakeApp";

        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;
        private readonly IRoutingCommunicator routingCommunicator;
        private const string EXT_WINDOW_PREFIX = "61AF9B94-AA41-4BC9-8482-2E1D2F041349:";
        private readonly Dictionary<string, IWindowViewContainer> extWindows = new Dictionary<string, IWindowViewContainer>();
        private Dispatcher _dispatcher;
        private readonly IRoutedPublisher<YokaiMessage> publisher;
        private WebView webControl;
        private readonly List<string> applications = new List<string>();
        protected static SlicingWebIntegration RootContainer;


        public SlicingWebIntegration(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            this.chromeRegistry = unityContainer.Resolve<IChromeRegistry>();
            this.chromeManager = unityContainer.Resolve<IChromeManager>();
            this.routingCommunicator = unityContainer.Resolve<IRoutingCommunicator>();

            this.routingCommunicator = unityContainer.Resolve<IRoutingCommunicator>();
            YokaiMessage.Target = Endpoint.LocalEndpoint;

            publisher = this.routingCommunicator.GetPublisher<YokaiMessage>();
            this.routingCommunicator.GetSubscriber<YokaiMessage>().GetObservable().Subscribe(YokaiMessageReceived);
        }

        private void YokaiMessageReceived(YokaiMessage message)
        {
            if (!this.applications.Contains(message.TargetApp))
            {
                Debug.WriteLine(string.Format("SWI: ignoring message for {0}", message.TargetApp));
                return;
            }

            this.webControl.ExecuteScript(string.Format(@"
(function() {{
 var windows = document.getElementById('x').contentWindow.YokaiWindows;
 if (!windows) {{
  return;
 }}
 var targetWindow = windows['{0}'];
 if (!targetWindow) {{
  return;
 }}
 var Yokai;
 try {{
  Yokai = targetWindow.Yokai;
 }} catch (e) {{
  console.error('FATAL: Could not cross the domain boundary - '+e.message);
 }}
 if (Yokai) {{
  var connection = Yokai.messaging.MSDesktopConnection.mailbox.get({{source:'{0}', target: '{1}'}});
  if (connection) {{
   Yokai.Logger.debug('MSDesktop: s:' + '{0}' + ' t:' + '{1}' + ' m: ' + '{2}');
   connection.fireMessageReceived('{2}');
  }}
 }}
}})();
", message.TargetApp, message.SourceApp, message.Message));

        }

        public override void OnModuleInitialized()
        {
            base.OnModuleInitialized();

            this.webControl = _htmlInitializerControl.Control as WebView;

            // register this as a js interop object
            webControl.RegisterJsObject("MSDesktop_Yokai_Adapter_80FC55E092664504B7BA988AE54C154C", this);

            if (!string.IsNullOrWhiteSpace(WebSupportExtensions.Registry[this].Title))
            {
                this.chromeManager.PlaceWidget(Guid.NewGuid().ToString(), WebSupportExtensions.Registry[this].Location, new InitialButtonParameters
                {
                    Click = (sender, args) => webControl.ExecuteScript(File.ReadAllText("injection.js")),
                    Text = WebSupportExtensions.Registry[this].Title
                });
            }
        }

        public void UpdateWindow(string windowId, double x, double y, double width, double height)
        {
            if (!extWindows.ContainsKey(windowId))
            {
                chromeRegistry.RegisterWindowFactory(EXT_WINDOW_PREFIX + windowId, (container, state) =>
                {
                    var webControl = _htmlInitializerControl.Control as WebView;
                    var vb = new VisualBrush(webControl) { Stretch = Stretch.None, Transform = new TranslateTransform(-x, -y), AlignmentX = AlignmentX.Left, AlignmentY = AlignmentY.Top };
                    var wrapper = new Canvas() { ClipToBounds = true };
                    var border = new Border
                    {
                        Background = vb,
                        Width = x + width,
                        Height = y + height,
                        Focusable = true
                    };

                    border.Tag = new Tuple<double, double>(x, y);
                    wrapper.SizeChanged += (s, e) =>
                    {
                        var t = border.Tag as Tuple<double, double>;
                        border.Width = t.Item1 + wrapper.ActualWidth;
                        border.Height = t.Item2 + wrapper.ActualHeight;
                        _htmlInitializerControl.CallJavascriptFunction("ResizeWindow", false, windowId, wrapper.ActualWidth, wrapper.ActualHeight);
                    };


                    wrapper.MouseDown += (sender, args) =>
                    {
                        var pos = args.MouseDevice.GetPosition(wrapper);

                        if (border == null)
                            return;

                        var t = (border.Background as VisualBrush).Transform as TranslateTransform;


                        webControl.SendMouseButton((int)(pos.X - t.X), (int)(pos.Y - t.Y), args.ChangedButton,
                                                    args.ButtonState, args.ClickCount);
                    };
                    wrapper.MouseMove += (sender, args) =>
                    {
                        var pos = args.MouseDevice.GetPosition(wrapper);

                        if (border == null)
                            return;

                        var t = (border.Background as VisualBrush).Transform as TranslateTransform;
                        webControl.SendMouseMove((int)(pos.X - t.X), (int)(pos.Y - t.Y));
                    };
                    wrapper.MouseUp += (sender, args) =>
                    {
                        var pos = args.MouseDevice.GetPosition(wrapper);

                        if (border == null)
                            return;

                        var t = (border.Background as VisualBrush).Transform as TranslateTransform;
                        webControl.SendMouseButton((int)(pos.X - t.X), (int)(pos.Y - t.Y), args.ChangedButton,
                                                    args.ButtonState, args.ClickCount);
                    };

                    border.KeyDown += (sender, e) =>
                    {
                        if (e.Key == System.Windows.Input.Key.Tab ||
                            e.Key >= System.Windows.Input.Key.Left && e.Key <= System.Windows.Input.Key.Down)
                        {

                            webControl.SendKey(e.Key, e.SystemKey, e.ImeProcessedKey, e.IsDown);
                        }
                    };
                    border.KeyUp += (sender, e) =>
                    {
                        if (e.Key == System.Windows.Input.Key.Tab ||
                            e.Key >= System.Windows.Input.Key.Left && e.Key <= System.Windows.Input.Key.Down)
                        {

                            webControl.SendKey(e.Key, e.SystemKey, e.ImeProcessedKey, e.IsDown);
                        }
                    };

                    wrapper.Children.Add(border);

                    container.Content = wrapper;
                    container.Created += (sender, args) => AddAutoRemoveKeyboardHook(wrapper, webControl, container);


                    return true;
                });
                var vc = chromeManager.CreateWindow(EXT_WINDOW_PREFIX + windowId, new InitialWindowParameters()
                {
                    SizingMethod = SizingMethod.Custom,
                    Width = width,
                    Height = height
                });

                extWindows.Add(windowId, vc);
            }
            else
            {
                var window = this.extWindows[windowId];
                window.Width = width;
                window.Height = height;

                var canvas = window.Content as Canvas;
                var border = canvas.Children[0] as Border;
                border.Width = x + width;
                border.Height = y + height;
                border.Tag = new Tuple<double, double>(x, y);
                (border.Background as VisualBrush).Transform = new TranslateTransform(-x, -y);
            }
        }

        private void AddAutoRemoveKeyboardHook(Visual visual, WebView webControl, IWindowViewContainer container)
        {
            HwndSource currentSource = null;
            var t = new Timer(state => visual.Dispatcher.BeginInvoke(((Action)(() =>
            {
                var source = (HwndSource)PresentationSource.FromVisual(visual);
                if (currentSource != null && currentSource != source)
                {
                    currentSource.RemoveHook(webControl.CallSourceHook);
                    //   currentSource.Dispose();
                }

                if (currentSource == source || source == null)
                    return;

                source.AddHook(webControl.CallSourceHook);
                currentSource = source;
            }))), null, 100, 200);

            EventHandler<WindowEventArgs> closeHandler = null;
            closeHandler = (sender, args) =>
            {
                t.Dispose();

                container.Closed -= closeHandler;
                container.ClosedQuietly -= closeHandler;
            };

            container.Closed += closeHandler;
            container.ClosedQuietly += closeHandler;
        }

        public void UpdateBuffer(double width, double height)
        {
            var webControl = _htmlInitializerControl.Control as WebView;
            var p = VisualTreeHelper.GetParent(webControl) as FrameworkElement;
            if (p != null)
            {
                p.Width = width;
                p.Height = height;
            }
        }

        #region Yokai JSVM - .net calls

        public void RegisterApplication(string appName)
        {
            if (this.applications.Contains(appName))
                return;

            this.applications.Add(appName);

            if (appName == YokaiRootContainerName)
            {
                RootContainer = this;
            }
        }

        public void Publish(string sourceAppName, string targetAppName, string message)
        {
            this.publisher.Publish(new YokaiMessage { SourceApp = sourceAppName, TargetApp = targetAppName, Message = message });
        }

        public void RemoveApplication(string appName)
        {
            this.applications.Remove(appName);

            if (appName == YokaiApplicationsHost.YokaiRootContainerName)
            {
                throw new InvalidOperationException(string.Format("Unknown application: {0}", appName));
            }
        }
        #endregion
    }

    [Serializable]
    public class YokaiDynamicBaseMessage : IProvideMessageInfo
    {
        public string AppName { get; set; }
        public string Content { get; set; }
        public string Action { get; set; }


        public string GetInfo()
        {
            return this.Action;
        }
    }
}