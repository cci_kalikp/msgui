﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MorganStanley.Desktop.ServiceDiscovery;
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
using MSDesktop.CrossMachineApplication.Extensions;
using MSDesktop.WebSupport.CEF;
using MSDesktop.Wormhole;
using MorganStanley.MSDesktop.IPC;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Themes;
using MSDesktop.WebSupport;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MSDesktop.Environment;
using MorganStanley.Msdotnet.Msgui.CefHtmlSlicing;

//using MorganStanley.Msdotnet.Msgui.WinRT;

namespace MSDesktop.Demo
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App
	{
		protected override void OnStartup(StartupEventArgs e_)
		{
			base.OnStartup(e_);
			var boot = new Framework();

            boot.AddModernTheme(Colors.MediumVioletRed, ModernThemeBase.Dark);
            boot.SetShellMode(ShellMode.RibbonWindow);
            boot.AcquireFocusAfterStartup();
            
            boot.HideOrLockShellElements(HideLockUIGranularity.HideViewPinButton | HideLockUIGranularity.HideEnvironmentLabellingForDockedWindows | HideLockUIGranularity.HideStatusBar);
            boot.SetApplicationIcon(BitmapFrame.Create(new Uri(@"pack://application:,,,/Resources/filed_48.png", UriKind.RelativeOrAbsolute)));
            boot.SetLayoutLoadStrategy(LayoutLoadStrategy.NeverAsk);

            // enable Kraken for Yokai hosted cross-container messaging
            boot.EnableMessageRoutingCommunication();
            boot.EnableMessageRoutingDefaultGui();
            boot.EnableMessageRoutingMonitor();
            boot.SetImcCommunicationStatus(CommunicationChannelStatus.Required);

            // enable web support and optional web debugger
            boot.EnableWebSupport();
            boot.DisableVirtualDesktopSupport();

            var cmdline = System.Environment.GetCommandLineArgs().FirstOrDefault(s => s.ToLower().Equals("enablechromedebugger"));
            if (!string.IsNullOrEmpty(cmdline))
            {
                boot.EnableDeveloperToolsForCEF();
            }
            
            cmdline = System.Environment.GetCommandLineArgs().FirstOrDefault(s => s.ToLower().StartsWith("exceptionddress="));
		    if (!string.IsNullOrEmpty(cmdline))
		    {
                boot.EnableExceptionHandler(cmdline.Substring(17));
		    }

            boot.EnableYokaiIntegration();

            //boot.SetUpProcessIsolatedWrapper<YAHProxy>(new ProcessIsolationOptions { RespawnEnabled = true, IsolatedPlatform = IsolatedProcessPlatform.X86, AdditionalCommandLine = "/debug" });
            //boot.SetUpProcessIsolatedWrapper<SWIProxy>(new ProcessIsolationOptions { RespawnEnabled = true, IsolatedPlatform = IsolatedProcessPlatform.X86, AdditionalCommandLine = "/debug" });
            
			boot.Start();
		}
	}
}
