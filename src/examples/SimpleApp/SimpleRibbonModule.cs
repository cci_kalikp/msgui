﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace SimpleApp
{
    internal class SimpleRibbonModule: IModule
    {
        private readonly IChromeRegistry _chromeRegistry;
        private readonly IChromeManager _chromeManager;

        public SimpleRibbonModule(IChromeRegistry chromeRegistry, IChromeManager chromeManager)
        {
            _chromeRegistry = chromeRegistry;
            _chromeManager = chromeManager;
        }
        public void Initialize()
        {

            _chromeRegistry.RegisterWidgetFactory(@"\\CommunicationExamples\EmptyWidget", CreateEmptyWidget);
            _chromeManager.PlaceWidget(@"\\CommunicationExamples\EmptyWidget", "Main/Empty", null);


            _chromeRegistry.RegisterWidgetFactory(@"\\CommunicationExamples\MoeduleToModule\PublishingWidget", CreateStrangeWidget);
            _chromeManager.PlaceWidget(@"\\CommunicationExamples\MoeduleToModule\PublishingWidget", "ModuleToModule/Sender", null);
        }

         private bool CreateEmptyWidget(IWidgetViewContainer widget, XDocument state)
         {
             return true;
         }

        private bool CreateStrangeWidget(IWidgetViewContainer widget, XDocument state)
        {
            var label = new Label() { Content = "MS" };
            var textBox = new TextBox() { Width = 300, AcceptsReturn = true };
            AutomationProperties.SetAutomationId(textBox, "ModulePublishingBox");


            DockPanel dockPanel = new DockPanel();

            dockPanel.Children.Add(label);
            dockPanel.Children.Add(textBox);

            widget.Content = dockPanel;
            //widget_.MinimizedView = textBox;

            return true;
        }
    }
}
