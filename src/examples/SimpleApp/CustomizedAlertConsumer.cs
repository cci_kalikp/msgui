﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace AlertExamples
{
    //cw Alert: 100
    /**
     * ### Create customized consumer
     * 
     */

    //{
    public class CustomizedAlertConsumer: IAlertConsumer  //implement interface
    {
        void IAlertConsumer.Consume(Alert alert) //Define how to handle alert
        {
            TaskDialog.ShowMessage(alert.Text, alert.AlertLevel.ToString(), TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
        }

        void IAlertConsumer.Initialize(IUnityContainer unityContainer) 
        {
            
        }
    }
    //}
}
