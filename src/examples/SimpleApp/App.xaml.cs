﻿using System.Collections.Generic;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;


//cw Alert: 0
/** 
 * ## Description
 * 
 *   * First, define and setup Alert consumers, which will respond to the alerts.
 * 
 *   * Then, send alerts.
 * 
 * 
 * ## Namespaces
 */
//cw Alert: 2 
//{
using MorganStanley.MSDotNet.MSGui.Impl.Alert;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
//}
using MorganStanley.MSDotNet.MSGui.Themes;
using SimpleApp;


namespace AlertExamples
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        //cw Alert: 10
        
        /** ## Context
         */
        //{ 
        protected override void OnStartup(StartupEventArgs e)
        //}
        {
       
            base.OnStartup(e);
            var boot = new Framework();
           
            boot.AddSimpleTheme();
            boot.EnableExceptionHandler("msguiadministrators@ms.com");

            boot.AddModule<SimpleRibbonModule>();

          

            boot.Start();
        }

      
    }
}
