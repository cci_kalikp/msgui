﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/examples/UsageAsLibrary/SimpleModule.cs#12 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;
using System.Windows.Controls;

namespace MorganStanley.MSDotNet.MSGui
{
  public class SimpleModule : IModule
  {
    readonly IViewManager m_viewManager;
    readonly IControlParadigmRegistrator m_ribbon;

    public SimpleModule(IViewManager viewManager_, IControlParadigmRegistrator ribbon_)
    {      
      m_viewManager = viewManager_;
      m_ribbon = ribbon_;
    }

    #region IModule Members

    public void Initialize()
    {
      //Register view builder.
      m_viewManager.AddViewCreator("Simple View", AddView);

      //Register button to create view
      var btn = new RibbonButton("Add Simple View", (s_, e_) => m_viewManager.CreateView("Simple View"));
      m_ribbon.AddRibbonItem("Tab", "Group", btn);            
    }

    private void AddView(IViewContainer viewContainer_)
    {
      var viewBtn = new Button();      
      viewBtn.Width = 100;
      viewBtn.Height = 100;
      viewBtn.Content = "Hello World";

      viewContainer_.Content = viewBtn;
      viewContainer_.Title = "New View";
    }

    #endregion
  }
}
