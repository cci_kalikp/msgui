﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/UsageAsLibrary/Program.cs#7 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Themes;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MorganStanley.MSDotNet.MSGui
{
  public class Program
  {    
    [STAThread]
    public static void Main(string[] args)
    {
      try
      {
        var fw = new Framework();
        fw.EnableReflectionModuleLoad();
        fw.AddBlackTheme();
        fw.SetShellMode(ShellMode.RibbonWindow);
		fw.SetupPersistenceStorage(new FilePersistenceStorage("UsageAsLibrary"));
        fw.EnableLegacyControlPlacement();
        fw.Start();
      }
      catch (Exception ex_)
      {
        TaskDialog.ShowMessage(ex_.Message + "\n" + ex_.StackTrace, ex_.Message, TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
      }
    }
  }
}
