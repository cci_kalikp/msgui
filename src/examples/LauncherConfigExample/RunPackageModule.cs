﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace LauncherConfigExample
{
    public class RunPackageModule:IModule
    {
        private readonly IChromeManager chromeManager;
        private readonly IChromeRegistry chromeRegistry;
        public RunPackageModule(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_)
        {
            this.chromeManager = chromeManager_;
            this.chromeRegistry = chromeRegistry_;
        }
        public void Initialize()
        {
            chromeManager[ChromeArea.LauncherBar]["Run Package"].AddWidget(new InitialButtonParameters()
            {
                Text = "Generate URL to run by package definition",
                Click = GenerateRunByPackageDefinitionURL
            });
            chromeManager[ChromeArea.LauncherBar]["Run Package"].AddWidget(new InitialButtonParameters()
            {
                Text = "Generate URL to run by package path",
                Click = GenerateRunByPackagePathURL
            });
            chromeManager[ChromeArea.LauncherBar]["Run Package"].AddWidget(new InitialButtonParameters()
            {
                Text = "Run by package definition directly",
                Click = RunByPackageDefinition
            });
            chromeManager[ChromeArea.LauncherBar]["Run Package"].AddWidget(new InitialButtonParameters()
            {
                Text = "Run by package path directly",
                Click = RunByPackagePath
            });
            
        }

        private void GenerateRunByPackageDefinitionURL(object sender_, EventArgs eventArgs_)
        {
            var link = DispatchableLauncherMessage.GeneratePackageStartLink("Carbon", "Daily", "Carbon.zip", null, "-profile=\"NA Carbon QA\" -username=bocca") ;
            MessageBox.Show(link, "URL Generated", MessageBoxButton.OK);
        }

        private void GenerateRunByPackagePathURL(object sender_, EventArgs eventArgs_)
        {
            var link = DispatchableLauncherMessage.GeneratePackagePathStartLink(@"\\san01b\DevAppsGML\dist\etari\PROJ\carbon\qa_workspace\carbon.zip", "Carbon.Workspace.exe", "-profile=\"NA Carbon QA\" -username=bocca");
            MessageBox.Show(link, "URL Generated", MessageBoxButton.OK);
        }

        
        private void RunByPackageDefinition(object sender_, EventArgs eventArgs_)
        {
             DispatchableLauncherMessage.StartPackage("Carbon", "Daily", "Carbon.zip", null, "-profile=\"NA Carbon QA\" -username=bocca") ;
        }

        
        private void RunByPackagePath(object sender_, EventArgs eventArgs_)
        {
            DispatchableLauncherMessage.StartPackageByPath(@"\\san01b\DevAppsGML\dist\etari\PROJ\carbon\qa_workspace\carbon.zip", "Carbon.Workspace.exe", "-profile=\"NA Carbon QA\" -username=bocca") ;
        }
    }
}
