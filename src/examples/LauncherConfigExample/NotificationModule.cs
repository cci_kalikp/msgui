﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;

namespace LauncherConfigExample
{
    public class NotificationModule:IModule
    {
        private DateTime startTime = DateTime.Now;
        private IPersistenceService persistenceService;
        public NotificationModule(IApplication application_, IPersistenceService persistenceService_)
        {
            persistenceService = persistenceService_;
            application_.ApplicationLoaded += new EventHandler(application__ApplicationLoaded); 
        }
         
        void application__ApplicationLoaded(object sender, EventArgs e)
        {
            TaskDialog.ShowMessage(stateStr ?? "Never started before", "Last Access State");
        }
         
        public void Initialize()
        { 
            persistenceService.AddGlobalPersistor("LastAccessState", LoadAccessState, SaveAccessState);
        }

        private XDocument SaveAccessState()
        {
            var stateDoc = new XDocument();
            var stateEl = new XElement("LastAccessState", "Start on " + startTime + " and end on " + DateTime.Now);
            stateDoc.Add(stateEl);
            return stateDoc;
        }

        private string stateStr;
        private void LoadAccessState(XDocument state)
        {
            if (state != null)

            {
                var stateEl = state.Descendants("LastAccessState").FirstOrDefault();
                if (stateEl != null)
                {
                    stateStr = stateEl.Value; 
                }
            } 
        }
    }
}
