﻿using System; 
using MSDesktop.Launcher.MessageDispatcher;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication; 

namespace LauncherConfigExample
{
    //cw Business_Specific_Launcher:50
    //{
    public class ApplicationMessageRouteModule : ModuleMessageHandlerBase
    { 
        private IModulePublisher<DispatchableLauncherMessage> messagePublisher; 
        public ApplicationMessageRouteModule(ICommunicator communicator_) : base(communicator_)
        { 
        }

        public override void Initialize()
        {
            messagePublisher = machineWideCommunicator.GetPublisher<DispatchableLauncherMessage>();
        }

        //handle the module message all the time
        protected override bool AcceptMessage(DispatchableLauncherModuleMessage message_)
        {
            return true;
        }

        protected override void HandleMessage(DispatchableLauncherModuleMessage message_)
        {
            if (message_.Payload == null) return;
            if (message_.Payload.Type == "SendingMessage")
            {

                messagePublisher.PublishRelativeToCurrent(new DispatchableLauncherMessage() { AppKey = "msdesktop.communicationexmples.prod", Payload = message_.Payload, Sequence = Guid.NewGuid().ToString(), HideProgress = true },
                    CommunicationTargetFilter.MachineInstance);
                messagePublisher.PublishRelativeToCurrent(new DispatchableLauncherMessage() { AppKey = "msdesktop.communicationexmples.prod2", Payload = message_.Payload, Sequence = Guid.NewGuid().ToString(), HideProgress = true },
                    CommunicationTargetFilter.MachineInstance);
                messagePublisher.PublishRelativeToCurrent(new DispatchableLauncherMessage() { AppKey = "msdesktop.communicationexmples.prod3", Payload = message_.Payload, Sequence = Guid.NewGuid().ToString(), HideProgress = true },
                    CommunicationTargetFilter.MachineInstance);

            }
        }
    }
    //}
}
