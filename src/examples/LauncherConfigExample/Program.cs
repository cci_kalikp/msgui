﻿#region Launcher Codewiki
//cw Business_Specific_Launcher: 10
/**##Description*/
/**A business specific launcher can be started by specifying a name along with a extended Root.xml different from \\san01b\DevAppsGML\dist\ied\PROJ\launcherconfig\prod\root.xml through command as following to start a different instance of launcher and with custom logics injected if needed*/
/**MSDesktop.Launcher.exe -n "PB Launcher" C:\Code\msdotnet\msgui\trunk\examples\LauncherConfigExample\bin\Debug\Root.xml */
/**User preference data and the log file for this launcher differentiated by name can then be found under %AppData%\Morgan Stanley\Launcher\[Launcher Name] */
/**##Custom settings*/
/**Other than the "Applications" element which contains a list of points LauncherConfig files, the new "LauncherSettings" element is supported by a business specific launcher */
/**Following are 2 examples */

//cw Business_Specific_Launcher:20
/**<b>Title</b>: specifies the title for the launcher, if missing the name specified through command line when starting launcher would be used. It would show as the main window/launcher bar title, and system tray tooltip.*/
/**<b>Theme</b>: overrides the default theme("White" or modern ui, and "Simple" for classic ui), valid values are "Simple", "Black", "Blue" and "White".*/
/**<b>IconFile</b>: specifies a different icon for the launcher.*/
/**<b>ForceModernUI</b>: forces to use the modern ui always, which disallows user to switch to classic ui.*/
/**<b>StayAlive</b>: if sets to false, the launcher can be closed and would always show at startup, and no system tray access; Otherwise it can only be hidden and activated through the system tray.*/
/**<b>HTTPServerPort</b>: specifies a different http server port for the [launcher message listener][cw_ref Launcher_Cross_Launcher_Message].*/
/**<b>CPSConfigFile</b>: sets the path of the cps config file if want to enable cross machine coorperation for launcher message.*/
/**<b>Modules</b>: defines custom modules to be loaded by the launcher, the definition is same as the definition for Modules in \\msad\root\NA\NY\lib\MSDotNet\Codewiki\MSDesktop\prod\examples\LoaderExample\MSDesktopConfigSchema.xsd.*/
/**<b>IsolatedSubsystems</b>: defines custom modules to be loaded by isolated process started by launcher, the definition is same as the definition for IsolatedSubsystems in \\msad\root\NA\NY\lib\MSDotNet\Codewiki\MSDesktop\prod\examples\LoaderExample\MSDesktopConfigSchema.xsd.*/
/**<b>Entitlement</b>: defines entitlement that can be used to check if a module/isolated module is entitled to be loaded, the definition is same as the definition for Entitlement in \\msad\root\NA\NY\lib\MSDotNet\Codewiki\MSDesktop\prod\examples\LoaderExample\MSDesktopConfigSchema.xsd.*/
 
/**##Module Message Handler*/
/**Custom handler can be added to handle messages sent from other application/http request using the DispatchableLauncherModuleMessage(as opposed to the [DispatchableLauncherMessage][cw_ref Launcher_Cross_Launcher_Message] used to send message to other applications managed by the launcher)*/
/**##Send message to launcher*/

//cw Business_Specific_Launcher:40
/**##Handle message at the launcher side*/
/**Create a custom module inheriting from ModuleMessageHandlerBase(need to reference MSDesktop.Launcher.exe, but no need to copy it to local), and override the AcceptMessage and HandleMessage*/
//cw Business_Specific_Launcher:60
/**Add this module to the Root.xml for the launcher */

#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;
using MorganStanley.MSDotNet.MSGui.Impl.SmartApi;

namespace LauncherConfigExample
{
    internal class Program
    {
        public static void Main(string[] args_)
        { 
            PathUtilities.PrepareExampleReference();
            string launcherPath = Extensions.GetMSDesktopApplicationLocation(MSDesktopApplication.Launcher);
            Environment.SetEnvironmentVariable("LauncherPath", launcherPath);
            string root = PathUtilities.GetAbsolutePath("Root.xml");
            string exe = Path.Combine(launcherPath, "MSDesktop.Launcher.exe");
            Process.Start(exe, @"-n ""PB Launcher"" " + root); 
 
        }
    }
}
