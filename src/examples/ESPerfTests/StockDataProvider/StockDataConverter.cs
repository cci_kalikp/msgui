﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.ESPerfTests.MarketDataService;
using ESPerfTests.StockDataProvider.Entities;
using System.Diagnostics;

namespace MorganStanley.Desktop.ESPerfTest.StockDataProvider
{
    class StockDataConverter<T> : IEntityServiceConverter<T>
        where T : AbstractStockEntity, IEntity, new()
    {
        private static T Convert(object data)
        {
            var stock = new T() { Price = (data as Stock).Price, Symbol = (data as Stock).Symbol };
            return stock;
        }

        private static Stock ConvertBack(IEntity<T, IKey<T>> entity)
        {
            var stockEntity = entity as AbstractStockEntity;
            return new Stock { Symbol = stockEntity.Symbol, Price = stockEntity.Price };
        }

        public IEnumerable<T> ConvertFromNativeFormatToEntities(IEnumerable<object> nativeEntities)
        {
            Stopwatch.Start();
            var result = from native in nativeEntities
                            where native != null
                            select Convert(native);
            Stopwatch.Stop();
            return result;
        }

        public IEnumerable<object> ConvertFromQueriesToNativeFormat(IEnumerable<IQuery<T>> queries, QueryConversionType conversionType)
        {
            return queries;
        }

        public IEnumerable<object> ConvertFromEntitiesToNativeFormat(IEnumerable<IEntity<T, IKey<T>>> entities)
        {
            Stopwatch.Start();
            var result = from entity in entities
                           where entity != null
                           select ConvertBack(entity);
            Stopwatch.Stop();
            return result;
        }

        #region Time metrics

        public static Stopwatch Stopwatch = new Stopwatch();

        #endregion
    }
}
