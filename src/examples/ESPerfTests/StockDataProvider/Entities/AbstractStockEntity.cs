﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESPerfTests.StockDataProvider.Entities
{
    [Serializable]
    public abstract class AbstractStockEntity
    {
        public string Symbol { get; internal set; }
        public double Price { get; internal set; }
    }
}
