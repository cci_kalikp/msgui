﻿using System;
using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using ESPerfTests.StockDataProvider.Entities;

namespace MorganStanley.Desktop.ESPerfTest.StockDataProvider
{
    [Serializable]
    [EntityService(ServiceMode.Caching)]
    public class CachingStockEntity : AbstractStockEntity, IEntity<CachingStockEntity, CachingStockEntity>, IKey<CachingStockEntity>, ISubscribable, IFetchable, IPublishable
    {
        public CachingStockEntity()
        {
            _key = this;
        }

        private CachingStockEntity _key;

        CachingStockEntity IEntity<CachingStockEntity, CachingStockEntity>.Key
        {
            get { return _key; }
        }

        IKey IEntity.Key
        {
            get { return _key; }
        }

        public bool Equals(IQuery other)
        {
            var otherKey = other as CachingStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol;
        }

        public bool Equals(IKey other)
        {
            var otherKey = other as CachingStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol && Price == otherKey.Price;
        }

        public bool Equals(IQuery<CachingStockEntity> other)
        {
            var otherKey = other as CachingStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol;
        }
    }
}
