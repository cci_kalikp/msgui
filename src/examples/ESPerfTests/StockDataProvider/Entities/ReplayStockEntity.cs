﻿using System;
using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using ESPerfTests.StockDataProvider.Entities;

namespace MorganStanley.Desktop.ESPerfTest.StockDataProvider
{
    [Serializable]
    [EntityService(ServiceMode.Replay)]
    public class ReplayStockEntity : AbstractStockEntity, IEntity<ReplayStockEntity, ReplayStockEntity>, IKey<ReplayStockEntity>, ISubscribable, IFetchable, IPublishable
    {
        public ReplayStockEntity()
        {
            _key = this;
        }

        private ReplayStockEntity _key;

        ReplayStockEntity IEntity<ReplayStockEntity, ReplayStockEntity>.Key
        {
            get { return _key; }
        }

        IKey IEntity.Key
        {
            get { return _key; }
        }

        public bool Equals(IQuery other)
        {
            var otherKey = other as ReplayStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol;
        }

        public bool Equals(IKey other)
        {
            var otherKey = other as ReplayStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol && Price == otherKey.Price;
        }

        public bool Equals(IQuery<ReplayStockEntity> other)
        {
            var otherKey = other as ReplayStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol;
        }
    }
}
