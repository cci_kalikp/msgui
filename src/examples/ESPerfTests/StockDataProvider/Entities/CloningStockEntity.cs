﻿using System;
using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using ESPerfTests.StockDataProvider.Entities;

namespace MorganStanley.Desktop.ESPerfTest.StockDataProvider
{
    [Serializable]
    [EntityService(ServiceMode.Cloning)]
    public class CloningStockEntity : AbstractStockEntity, IEntity<CloningStockEntity, CloningStockEntity>, IKey<CloningStockEntity>, ISubscribable, IFetchable, IPublishable
    {
        public CloningStockEntity()
        {
            _key = this;
        }

        private CloningStockEntity _key;

        CloningStockEntity IEntity<CloningStockEntity, CloningStockEntity>.Key
        {
            get { return _key; }
        }
        
        IKey IEntity.Key
        {
            get { return _key; }
        }

        public new string Symbol { get; set; }
        public new double Price { get; set; }

        public bool Equals(IQuery other)
        {
            var otherKey = other as CloningStockEntity;
            if(otherKey == null)
                return false;

            return Symbol == otherKey.Symbol;
        }

        public bool Equals(IKey other)
        {
            var otherKey = other as CloningStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol && Price == otherKey.Price;
        }

        public bool Equals(IQuery<CloningStockEntity> other)
        {
            var otherKey = other as CloningStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol;
        }
    }
}
