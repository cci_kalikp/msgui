﻿using System;
using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using ESPerfTests.StockDataProvider.Entities;

namespace MorganStanley.Desktop.ESPerfTest.StockDataProvider
{
    [Serializable]
    [EntityService(ServiceMode.None)]
    public class StockEntity : AbstractStockEntity, IEntity<StockEntity, StockEntity>, IKey<StockEntity>, ISubscribable, IFetchable, IPublishable
    {
        public StockEntity()
        {
            _key = this;
        }

        private StockEntity _key;

        StockEntity IEntity<StockEntity, StockEntity>.Key
        {
            get { return _key; }
        }

        IKey IEntity.Key
        {
            get { return _key; }
        }

        public bool Equals(IQuery other)
        {
            var otherKey = other as StockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol;
        }

        public bool Equals(IKey other)
        {
            var otherKey = other as StockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol && Price == otherKey.Price;
        }

        public bool Equals(IQuery<StockEntity> other)
        {
            var otherKey = other as StockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol;
        }
    }
}
