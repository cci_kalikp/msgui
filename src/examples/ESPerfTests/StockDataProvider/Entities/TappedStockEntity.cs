﻿using System;
using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using ESPerfTests.StockDataProvider.Entities;

namespace MorganStanley.Desktop.ESPerfTest.StockDataProvider
{
    [Serializable]
    [EntityService(ServiceMode.Tapped)]
    public class TappedStockEntity : AbstractStockEntity, IEntity<TappedStockEntity, TappedStockEntity>, IKey<TappedStockEntity>, ISubscribable, IFetchable, IPublishable
    {
        public TappedStockEntity()
        {
            _key = this;
        }

        private TappedStockEntity _key;

        TappedStockEntity IEntity<TappedStockEntity, TappedStockEntity>.Key
        {
            get { return _key; }
        }

        IKey IEntity.Key
        {
            get { return _key; }
        }

        public bool Equals(IQuery other)
        {
            var otherKey = other as TappedStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol;
        }

        public bool Equals(IKey other)
        {
            var otherKey = other as TappedStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol && Price == otherKey.Price;
        }

        public bool Equals(IQuery<TappedStockEntity> other)
        {
            var otherKey = other as TappedStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol;
        }
    }
}
