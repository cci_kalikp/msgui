﻿using System;
using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using ESPerfTests.StockDataProvider.Entities;

namespace MorganStanley.Desktop.ESPerfTest.StockDataProvider
{
    [Serializable]
    [EntityService(ServiceMode.Local)]
    public class LocalStockEntity : AbstractStockEntity, IEntity<LocalStockEntity, LocalStockEntity>, IKey<LocalStockEntity>, ISubscribable, IFetchable, IPublishable
    {
        public LocalStockEntity()
        {
            _key = this;
        }

        private LocalStockEntity _key;

        LocalStockEntity IEntity<LocalStockEntity, LocalStockEntity>.Key
        {
            get { return _key; }
        }

        IKey IEntity.Key
        {
            get { return _key; }
        }
        
        public bool Equals(IQuery other)
        {
            var otherKey = other as LocalStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol;
        }

        public bool Equals(IKey other)
        {
            var otherKey = other as LocalStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol && Price == otherKey.Price;
        }

        public bool Equals(IQuery<LocalStockEntity> other)
        {
            var otherKey = other as LocalStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol;
        }
    }
}
