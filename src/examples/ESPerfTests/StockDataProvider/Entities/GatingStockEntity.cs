﻿using System;
using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using ESPerfTests.StockDataProvider.Entities;

namespace MorganStanley.Desktop.ESPerfTest.StockDataProvider
{
    [Serializable]
    [EntityService(ServiceMode.Gating)]
    public class GatingStockEntity : AbstractStockEntity, IEntity<GatingStockEntity, GatingStockEntity>, IKey<GatingStockEntity>, ISubscribable, IFetchable, ITimestamped, IPublishable
    {
        public GatingStockEntity()
        {
            _key = this;
        }

        private GatingStockEntity _key;

        GatingStockEntity IEntity<GatingStockEntity, GatingStockEntity>.Key
        {
            get { return _key; }
        }

        IKey IEntity.Key
        {
            get { return _key; }
        }

        public bool Equals(IQuery other)
        {
            var otherKey = other as GatingStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol;
        }

        public bool Equals(IKey other)
        {
            var otherKey = other as GatingStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol && Price == otherKey.Price;
        }

        public bool Equals(IQuery<GatingStockEntity> other)
        {
            var otherKey = other as GatingStockEntity;
            if (otherKey == null)
                return false;

            return Symbol == otherKey.Symbol;
        }

        public DateTime Timestamp
        {
            get { return new DateTime(); }
        }
    }
}
