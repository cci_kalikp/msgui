﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESPerfTests.StockDataProvider.Entities
{
    interface IStockEntity
    {
        string Symbol { get; }
        double Price { get; }
        byte[] Data { get; }
    }
}
