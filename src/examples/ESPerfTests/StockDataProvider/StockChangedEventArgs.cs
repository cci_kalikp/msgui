﻿using System;
using MorganStanley.Desktop.ESPerfTests.MarketDataService;

namespace MorganStanley.Desktop.ESPerfTest.StockDataProvider
{
    class StockChangedEventArgs: EventArgs
    {
        public Stock StockEntity;

        public StockChangedEventArgs(Stock stock)
        {
            StockEntity = stock;
        }
    }
}