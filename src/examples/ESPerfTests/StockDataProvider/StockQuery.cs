﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.EntityServices.Queries;

namespace ESPerfTests.StockDataProvider
{
    public class StockQuery<T> : StringBasedQuery, IQuery<T>
    {
        public StockQuery(QuerySeperator separator, params string[] fields) : base(separator, fields)
        {

        }

        public bool Equals(IQuery<T> other)
        {
            return this.ToString().Equals(other.ToString());
        }
    }
}
