﻿using System;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;

namespace MorganStanley.Desktop.ESPerfTest.StockDataProvider
{
    [Serializable]
    public class StockKey<T> : StringBasedKey, IKey<T>
    {
        public StockKey(KeySeperator seperator, params string[] fields) : base(seperator, fields)
        {
        }

        public StockKey(KeySeperator seperator, int numberOfFields)
            : base(seperator, numberOfFields)
        {
        }

        bool IEquatable<IQuery<T>>.Equals(IQuery<T> other)
        {
            return this.ToString().Equals(other.ToString());
        }
    }
    
}
