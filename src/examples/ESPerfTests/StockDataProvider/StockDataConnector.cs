﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.ESPerfTests.MarketDataService;
using System.Diagnostics;

namespace MorganStanley.Desktop.ESPerfTest.StockDataProvider
{
    class StockDataConnector: IEntityServiceConnector
    {
        IStockService m_stockService;

        private event EventHandler<StockChangedEventArgs> StockChanged;
        
        public StockDataConnector(IStockService service)
        {
            m_stockService = service;
            m_stockService.StockPriceChanged += (s, e) =>
            {
                if (e.Stock != null)
                {
                    var handler = StockChanged;
                    if (handler != null)
                    {
                        handler(this, new StockChangedEventArgs(e.Stock));
                    }
                }
            };
        }

        IEnumerable<object> IEntityServiceConnector.Get(IEnumerable<object> queries)
        {
            Stopwatch.Start();
            var res = from query in queries
                      select m_stockService.GetStock(query.ToString()) as Stock;
            Stopwatch.Stop();
            return res;
        }

        IObservable<IEnumerable<object>> IEntityServiceConnector.GetObservable(IEnumerable<object> queries)
        {
            var observable = Observable.FromEventPattern<EventHandler<StockChangedEventArgs>, StockChangedEventArgs>(ev => StockChanged += ev, ev => StockChanged -= ev);
            if (queries.Count() > 0 && queries.First().ToString() == "")
                return from stockChangedEvent in observable
                       select new List<object> { stockChangedEvent.EventArgs.StockEntity };
            var filter = from stockChangedEvent in observable
                         from q in queries
                         where stockChangedEvent.EventArgs.StockEntity.Symbol == q.ToString()
                         select new List<object> { stockChangedEvent.EventArgs.StockEntity };
        	return filter;
        }
        
        void IEntityServiceConnector.Publish(IEnumerable<object> objectsToPublish)
        {
            Stopwatch.Start();
            foreach (var obj in objectsToPublish)
            {
                m_stockService.ConsumeStock(obj as Stock);
            }
            Stopwatch.Stop();
        }

        void IEntityServiceConnector.Delete(IEnumerable<object> key)
        {
            throw new NotImplementedException();
        }

        #region Time metrics

        public static Stopwatch Stopwatch = new Stopwatch();

        #endregion
    }
}
