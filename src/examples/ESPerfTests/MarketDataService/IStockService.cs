﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.ESPerfTests.MarketDataService
{
    interface IStockService
    {
        Stock GetStock(string symbol);
        event EventHandler<StockPriceChangedEventArgs> StockPriceChanged;
        void ConsumeStock(Stock stock);
    }
}
