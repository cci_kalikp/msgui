﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using MorganStanley.Desktop.ESPerfTest;

namespace MorganStanley.Desktop.ESPerfTests.MarketDataService
{
    public class StockService : IStockService
    {
        public StockService()
        {
            m_random = new Random();
        }

        static StockService()
        {
            m_staticRandom = new Random();
        }

        public void SendRandomMessages(int howMany, int? interval=null)
        {
            if (interval.HasValue)
            {
                var timer = new Timer(interval.Value);
                timer.Elapsed += (s, e) =>
                {
                    var stock = new Stock { Price = m_random.NextDouble(), Symbol = GetRandomSymbol() };
                    OnStockPriceChanged(stock);
                    if (--howMany == 0)
                        timer.Dispose();
                };
                timer.Enabled = true;
            }
            else
            {
                for (int i = 0; i < howMany; i++)
                {
                    var stock = new Stock { Price = m_random.NextDouble(), Symbol = GetRandomSymbol() };
                    OnStockPriceChanged(stock);
                }
            }
        }

        public Stock GetStock(string symbol)
        {
            return new Stock { Price = m_random.NextDouble(), Symbol = symbol };
        }

        public void ConsumeStock(Stock stock)
        {
        }

        public event EventHandler<StockPriceChangedEventArgs> StockPriceChanged;

        public static string GetRandomSymbol()
        {
            return String.Format("{0}{1}{2}",
                (char)(m_staticRandom.Next(25) + 65),
                (char)(m_staticRandom.Next(25) + 65),
                (char)(m_staticRandom.Next(25) + 65));
    //        return String.Format("{0}{1}{2}{3}",
    //(char)(m_staticRandom.Next(25) + 65),
    //(char)(m_staticRandom.Next(25) + 65),
    //(char)(m_staticRandom.Next(25) + 65),
    //(char)(m_staticRandom.Next(5) + 65));
        }

        void OnStockPriceChanged(Stock stock)
        {
            var handler = StockPriceChanged;
            if (handler != null)
            {
                handler(this, new StockPriceChangedEventArgs { Stock = stock });
            }
        }

        Random m_random;
        static Random m_staticRandom;
    }
}
