﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.ESPerfTests.MarketDataService
{
    public class Stock
    {
        public string Symbol { get; set; }
        public double Price { get; set; }
    }
}
