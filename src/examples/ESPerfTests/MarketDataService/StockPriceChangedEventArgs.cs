﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.ESPerfTests.MarketDataService
{
    public class StockPriceChangedEventArgs : EventArgs
    {
        public Stock Stock { get; set; }
    }
}
