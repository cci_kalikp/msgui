﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;

using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.ESPerfTest.StockDataProvider;
using MorganStanley.Desktop.EntityServices.Keys;
using System.Threading;
using MorganStanley.Desktop.ESPerfTests.MarketDataService;
using System.Diagnostics;
using MorganStanley.Desktop.EntityServices.Entities;
using ESPerfTests.StockDataProvider.Entities;
using System.Reflection;
using MorganStanley.Desktop.EntityServices.Queries;
using ESPerfTests.StockDataProvider;

namespace MorganStanley.Desktop.ESPerfTests
{
    class Program
    {
        static void Main(string[] args)
        {
            StockService stockService = new StockService();

            TestSubscribe(stockService);
            TestPublish(stockService);
            TestGet(stockService);

            Console.ReadLine();
        }

        #region Fetching

        private static void TestGet(StockService stockService)
        {
            Console.WriteLine("Fetching by key");
            TestGetKeys(stockService);
            Console.WriteLine("Fetching by query");
            TestGetQueries(stockService);
        }

        private static void TestGetQueries(StockService stockService)
        {
            Console.WriteLine("N,Events,None,None Converter,None Connector,Cloning,Replay,Caching,Gating,Tapped,Local");
            var loads = new int[] { 1000, 10000, 100000, 1000000 };

            foreach (var howMany in loads)
            {
                Console.Write("{0},", howMany);
                TestCallGet(stockService, howMany);
                TestEntityServiceGetQueries<StockEntity>(stockService, howMany);
                TestEntityServiceGetQueries<CloningStockEntity>(stockService, howMany);
                TestEntityServiceGetQueries<ReplayStockEntity>(stockService, howMany);
                TestEntityServiceGetQueries<CachingStockEntity>(stockService, howMany);
                TestEntityServiceGetQueries<GatingStockEntity>(stockService, howMany);
                TestEntityServiceGetQueries<TappedStockEntity>(stockService, howMany);
                TestEntityServiceGetQueries<LocalStockEntity>(stockService, howMany);
                Console.WriteLine();
            }
        }

        private static void TestEntityServiceGetQueries<T>(StockService stockService, int howMany)
            where T : AbstractStockEntity, IEntity<T, IKey<T>>, IFetchable, new()
        {
            IUnityContainer container = new UnityContainer();

            container.RegisterInstance<IStockService>(stockService);
            container.RegisterEntityService(typeof(T), typeof(StockDataConnector), typeof(StockDataConverter<T>));

            var entityService = container.Resolve<IEntityService<T>>();
            var queries = new List<IQuery<T>>();

            for (int i = 0; i < howMany; i++)
            {
                queries.Add(new StockQuery<T>(KeySeperator.Default, StockService.GetRandomSymbol()));
            }

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            StockDataConverter<T>.Stopwatch.Reset();
            StockDataConnector.Stopwatch.Reset();

            foreach (var query in queries)
            {
                entityService.Get(query);
            }

            stopwatch.Stop();
            if (typeof(T) == typeof(StockEntity))
                Console.Write("{0},{1},{2},", stopwatch.ElapsedMilliseconds, StockDataConverter<T>.Stopwatch.ElapsedMilliseconds, StockDataConnector.Stopwatch.ElapsedMilliseconds);
            else
                Console.Write("{0},", stopwatch.ElapsedMilliseconds);
        }

        private static void TestGetKeys(StockService stockService)
        {
            Console.WriteLine("N,Events,None,None Converter,None Connector,Cloning,Replay,Caching,Gating,Tapped,Local");
            var loads = new int[] { 1000, 10000, 100000, 1000000 };

            foreach (var howMany in loads)
            {
                Console.Write("{0},", howMany);
                TestCallGet(stockService, howMany);
                TestEntityServiceGetKeys<StockEntity>(stockService, howMany);
                TestEntityServiceGetKeys<CloningStockEntity>(stockService, howMany);
                TestEntityServiceGetKeys<ReplayStockEntity>(stockService, howMany);
                TestEntityServiceGetKeys<CachingStockEntity>(stockService, howMany);
                TestEntityServiceGetKeys<GatingStockEntity>(stockService, howMany);
                TestEntityServiceGetKeys<TappedStockEntity>(stockService, howMany);
                TestEntityServiceGetKeys<LocalStockEntity>(stockService, howMany);
                Console.WriteLine();
            }
        }

        private static void TestEntityServiceGetKeys<T>(StockService stockService, int howMany)
            where T : AbstractStockEntity, IEntity<T, IKey<T>>, IKey<T>, IFetchable, new()
        {
            IUnityContainer container = new UnityContainer();

            container.RegisterInstance<IStockService>(stockService);
            container.RegisterEntityService(typeof(T), typeof(StockDataConnector), typeof(StockDataConverter<T>));

            var entityService = container.Resolve<IEntityService<T>>();
            var keys = new List<IKey<T>>();

            for (int i = 0; i < howMany; i++)
            {
                keys.Add(new StockKey<T>(KeySeperator.Default, new[] { StockService.GetRandomSymbol() }));
                //keys.Add(new T() { Symbol = StockService.GetRandomSymbol() });
            }

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            StockDataConverter<T>.Stopwatch.Reset();
            StockDataConnector.Stopwatch.Reset();

            foreach (var key in keys)
            {
                entityService.Get(key);
            }

            stopwatch.Stop();
            if (typeof(T) == typeof(StockEntity))
                Console.Write("{0},{1},{2},", stopwatch.ElapsedMilliseconds, StockDataConverter<T>.Stopwatch.ElapsedMilliseconds, StockDataConnector.Stopwatch.ElapsedMilliseconds);
            else
                Console.Write("{0},", stopwatch.ElapsedMilliseconds);
        }

        private static void TestCallGet(StockService stockService, int howMany)
        {
            var stopwatch = new Stopwatch();

            string[] keys = new string[howMany];
            for (int i = 0; i < howMany; i++)
                keys[i] = StockService.GetRandomSymbol();

            stopwatch.Start();

            foreach (var key in keys)
                stockService.GetStock(key);

            stopwatch.Stop();
            Console.Write("{0},", stopwatch.ElapsedMilliseconds);
        }

        #endregion

        #region Publication

        private static void TestPublish(StockService stockService)
        {
            Console.WriteLine("Publishing");

            Console.WriteLine("N,Events,None,None Converter,None Connector,Cloning,Replay,Caching,Gating,Tapped,Local");
            var loads = new int[] { 1000, 10000, 100000, 1000000 };

            foreach (var howMany in loads)
            {
                Console.Write("{0},", howMany);
                TestCallPublish(stockService, howMany);
                TestEntityServicePublish<StockEntity>(stockService, howMany);
                TestEntityServicePublish<CloningStockEntity>(stockService, howMany);
                TestEntityServicePublish<ReplayStockEntity>(stockService, howMany);
                TestEntityServicePublish<CachingStockEntity>(stockService, howMany);
                TestEntityServicePublish<GatingStockEntity>(stockService, howMany);
                TestEntityServicePublish<TappedStockEntity>(stockService, howMany);
                TestEntityServicePublish<LocalStockEntity>(stockService, howMany);
                Console.WriteLine();
            }
        }

        private static void TestEntityServicePublish<T>(StockService stockService, int howMany)
                        where T : AbstractStockEntity, IEntity<T, IKey<T>>, IPublishable, ISubscribable, new()
        {
            IUnityContainer container = new UnityContainer();

            container.RegisterInstance<IStockService>(stockService);
            container.RegisterEntityService(typeof(T), typeof(StockDataConnector), typeof(StockDataConverter<T>));

            var entityService = container.Resolve<IEntityService<T>>();
            var entities = new List<T>();

            for (int i = 0; i < howMany; i++)
            {
                entities.Add(new T() { Symbol = StockService.GetRandomSymbol(), Price = m_random.NextDouble() });
            }

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            StockDataConverter<T>.Stopwatch.Reset();
            StockDataConnector.Stopwatch.Reset();

            foreach (var entity in entities)
                entityService.Publish(entity);

            stopwatch.Stop();
            if (typeof(T) == typeof(StockEntity))
                Console.Write("{0},{1},{2},", stopwatch.ElapsedMilliseconds, StockDataConverter<T>.Stopwatch.ElapsedMilliseconds, StockDataConnector.Stopwatch.ElapsedMilliseconds);
            else
                Console.Write("{0},", stopwatch.ElapsedMilliseconds);
        }

        private static void TestCallPublish(StockService stockService, int howMany)
        {
            var stocks = new List<Stock>();

            for (int i = 0; i < howMany; i++)
                stocks.Add(new Stock { Symbol = StockService.GetRandomSymbol(), Price = m_random.NextDouble() });

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            foreach (var stock in stocks)
                stockService.ConsumeStock(stock);

            stopwatch.Stop();
            Console.Write("{0},", stopwatch.ElapsedMilliseconds);
        }

        #endregion

        #region Subscription

        private static void TestSubscribe(StockService stockService)
        {
            Console.WriteLine("Subscribing");
            Console.WriteLine("N,Events,None,Cloning,Replay,Caching,Gating,Tapped,Local");
            TestAllServicesSubscribe(stockService, null, new int[] { 1000, 10000, 100000, 1000000 });
            Console.WriteLine("Subscribing with interval");
            Console.WriteLine("N,Events,None,Cloning,Replay,Caching,Gating,Tapped,Local");
            TestAllServicesSubscribe(stockService, 5, new int[] { 1000 });
        }

        private static void TestAllServicesSubscribe(StockService stockService, int? interval, int[] loads)
        {
            foreach (var howMany in loads)
            {
                Console.Write("{0},", howMany);
                TestEventSubscribe(stockService, howMany, interval);
                TestEntityServiceSubscribe<StockEntity>(stockService, howMany, interval);
                TestEntityServiceSubscribe<CloningStockEntity>(stockService, howMany, interval);
                TestEntityServiceSubscribe<ReplayStockEntity>(stockService, howMany, interval);
                TestEntityServiceSubscribe<CachingStockEntity>(stockService, howMany, interval);
                TestEntityServiceSubscribe<GatingStockEntity>(stockService, howMany, interval);
                TestEntityServiceSubscribe<TappedStockEntity>(stockService, howMany, interval);
                TestEntityServiceSubscribe<LocalStockEntity>(stockService, howMany, interval);
                Console.WriteLine();
            }
        }

        private static void TestEntityServiceSubscribe<T>(StockService stockService, int howMany, int? interval = null)
            where T : AbstractStockEntity, IEntity, ISubscribable, new()
        {
            IUnityContainer container = new UnityContainer();

            container.RegisterInstance<IStockService>(stockService);
            container.RegisterEntityService(typeof(T), typeof(StockDataConnector), typeof(StockDataConverter<T>));

            var entityService = container.Resolve<IEntityService<T>>();
            var query = new StockKey<T>(KeySeperator.Default, 0); // will subscribe to all stocks
            var all = entityService.GetObservable(query);

            var stopwatch = new Stopwatch();
            var hitCount = 0;
            var auto = new AutoResetEvent(false);

            using (all.Subscribe(s =>
            {
                Interlocked.Increment(ref hitCount);
                if (hitCount == howMany) auto.Set();
            }))
            {
                stopwatch.Start();
                stockService.SendRandomMessages(howMany, interval);
                auto.WaitOne();
                stopwatch.Stop();
            }

            //System.Console.WriteLine("Received {0} messages in {1} miliseconds", howMany, stopwatch.ElapsedMilliseconds);
            System.Console.Write("{0},", stopwatch.ElapsedMilliseconds);
        }

        private static void TestEventSubscribe(StockService stockService, int howMany, int? interval = null)
        {
            var stopwatch = new Stopwatch();
            var hitCount = 0;
            var auto = new AutoResetEvent(false);

            stockService.StockPriceChanged += (s, e) =>
            {
                Interlocked.Increment(ref hitCount);
                if (hitCount == howMany) auto.Set();
            };
            stopwatch.Start();
            stockService.SendRandomMessages(howMany, interval);
            auto.WaitOne();
            stopwatch.Stop();

            //System.Console.WriteLine("Received {0} messages in {1} miliseconds", howMany, stopwatch.ElapsedMilliseconds);
            System.Console.Write("{0},", stopwatch.ElapsedMilliseconds);
        }

        #endregion

        static Random m_random = new Random();
    }
}
