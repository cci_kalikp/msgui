﻿#region cw page

//cw Exceptions : 10

/**
 * ## Description
 * Set up exception dialog and hebaviours
 */
//cw Exceptions: 20
/**
 * ## Namespace
 */

//cw Exceptions: 30
/**
 * ## Context
 * Set up when initializing the application
 */
//cw Exceptions: 40
/**
 * ## Code
 * 
 */

//cw Exceptions: 50
/**
 * ## Run example 
 * ![dialog](images/Exceptions/Exception_dialog.png)
 */

#endregion

using System.Windows;
using System.Windows.Controls;
//cw Exceptions: 21
//{
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
//}
using MorganStanley.MSDotNet.MSGui.Themes;
using MorganStanley.MSDotNet.My;

namespace ExceptionExamples
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>


    public partial class App : Application
    {
        private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<App>();

        //cw Exceptions: 31
        //{
        protected override void OnStartup(StartupEventArgs e)
            //}
        {
            base.OnStartup(e);
            //{
            var boot = new Framework(); //variable needed
            //}

            boot.EnableLegacyDialog = false;

            boot.AddSimpleTheme();
            //cw Exceptions: 41
            /** Config the email address to receive the exceptions
             */
            //{
            boot.EnableMailServiceExceptionHandler("Jing.Cai@morganstanley.com"); //Change the email address
            //}

            /** Multiple email addresses can be added
             */
            if (false)
            {
                //{
                boot.EnableExceptionHandler("email1@ms.com", "email2@ms.com");
                //}
            }

            /** Config the dialog behaviour */
            //{
            boot.SetExceptionDialogBehaviour(copyButtonVisible: true /* visibility of the 'copy' button */,
                                             emailButtonCloses: true /* should the 'email' button also close the dialog */,
                                             suppressDialog: false /* should the dialog be replaced with an automatic email send */,
                                             attachScreenshot: true /*attach the screenshot*/,
                                             attachLog: true /*attach the log*/,
                                             attachFullLog: true, /*attach full log, if enabled, should reference \\san01b\DevAppsGML\dist\msdotnet\PROJ\sharpziplib\0.86.0\assemblies\ICSharpCode.SharpZipLib.dll and \\san01b\DevAppsGML\dist\deskapps\PROJ\attachlink-api\1.4.1\assemblies\DeskApps.attachlink-api.dll in the project */
                                             exceptionMessage: "Custom exception text" /* the custom exception text  */);
            //}


            /** Config the mail attachment service, this is needed when attachFullLog is enabled for exception dialog*/
            /** Set the large file storage directory to a shared folder*/
            /** Set the maximum size of the file in bytes to be attached directly in email, default is 19MB */
            /** Set the maximum size of the file in bytes to be attached as attachlink in email, default is 70MB */
            //{
            boot.ConfigMailAttachmentService(@"U:\tmp", 10 * 1024 * 1024, 50 * 1024 * 1024);
            //}

            /** Exception dialogs' upper part can be customized with custom UI elements
             */

            if (false)
            {
                //{
                boot.SetExceptionDialogBehaviour(exceptionMessageFrameworkElement: new Button {Content = "Test Button"});
                //}
            }

            /** Can force the automatic email sending when exception happens
             */
            if (false)
            {
                //{
                boot.SendExceptionReportAutomatically(); 
                //}
            }
            /**
             * The continue button can also be hidden to ensure user terminate the application when unhandled exception happened, to avoid running application in an unstable state
             */
            if (false)
            {
                //{
                boot.DisallowContinueInExceptionDialog();
                //}
            }
             
            boot.Start();

            var a = 1;
            var b = 0;
            var c = a/b;
        }
    }
}
