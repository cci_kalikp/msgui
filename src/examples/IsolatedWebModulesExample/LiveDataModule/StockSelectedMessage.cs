﻿using System;

namespace IsolatedWebModulesExample.LiveDataModule
{
    [Serializable]
    public class StockSelectedMessage
    {
        public string Symbol { get; set; }
    }
}
