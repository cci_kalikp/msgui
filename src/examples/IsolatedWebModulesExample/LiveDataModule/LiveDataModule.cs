﻿using System;
using MSDesktop.WebSupport.MessageRouting;
using Microsoft.Practices.Unity;
using MSDesktop.WebSupport.CEF;

namespace IsolatedWebModulesExample.LiveDataModule
{
    //class LiveDataModule : AwesomiumBasedHTMLModule
    class LiveDataModule : CEFBasedHTMLModule
    {
        public LiveDataModule(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            this.EnableMessageRouting();
        }

        protected override string Name
        {
            get { return "LiveDataModule"; }
        }

        protected override Uri Url
        {
            get 
            { 
                //return new Uri("http://google.pl");
                return new Uri(String.Format("{0}\\LiveDataModule\\WebModule.html", AppDomain.CurrentDomain.BaseDirectory),
                              UriKind.RelativeOrAbsolute); 
            }
        }
    }
}
