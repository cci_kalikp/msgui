﻿using System.Windows;
using IsolatedWebModulesExample.MultiviewTest;
using IsolatedWebModulesExample.Stocks;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.MSGui.Themes;
using MSDesktop.WebSupport;

namespace IsolatedWebModulesExample
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);
			var boot = new Framework();
            boot.AddSimpleTheme();
            boot.SetShellMode(ShellMode.RibbonWindow);
            boot.SetupPersistenceStorage(new FilePersistenceStorage("IsolatedWebModulesExample", "Profiles"));

            boot.AddModule<StocksModule>();
            boot.AddModule<MultiviewWebModule>();
            boot.AddModule<V2VConfigModule>();

             boot.AddModule<FlexTradeHistoryModule>();
            // boot.AddModule<LiveDataModule.LiveDataModule>();

            boot.AcquireFocusAfterStartup();
            
            boot.EnableWebSupport();

            boot.EnableMessageRoutingCommunication();

            boot.EnableLogging();

			boot.Start();
            boot.SetUpProcessIsolatedWrapper<LiveDataModule.LiveDataModule>(IsolatedProcessPlatform.X86
                                                                            ,additionalCommandLine: @"/debug"
                                                                            );  
		}
	}
}
