package com.ms.msdotnet.msgui
{
	public class InitialShowWindowButtonParameters implements InitialWidgetParameters
	{
		public var Text:String;
		public var WindowFactoryID:String;
		
		public function InitialShowWindowButtonParameters(text:String, windowFactory:String)
		{
			Text = text;
			WindowFactoryID = windowFactory;
		}
		
		public function getDotNetType():String
		{
			return "MorganStanley.MSDotNet.MSGui.Core.ChromeManager.InitialShowWindowButtonParameters, MSDotNet.MSGui.Core";
		}
	}
}