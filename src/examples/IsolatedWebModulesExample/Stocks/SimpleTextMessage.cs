﻿using System;

namespace IsolatedWebModulesExample.Stocks
{
    [Serializable]
    public class SimpleTextMessage
    {
        public string Text { get; set; }
    }
}
