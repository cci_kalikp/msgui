﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading;
using System.Xml.Linq;
using IsolatedWebModulesExample.LiveDataModule;
using MSDesktop.MessageRouting;
using Microsoft.Practices.Composite.Modularity; 
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace IsolatedWebModulesExample.Stocks
{
    public class StocksModule : IModule
    {
        private const string ViewId = "TradesViewID";
        private const string WidgetButtonId = "TradesButtonID";
        private const string WidgetButtonId2 = "TradesButtonID2";
        private static readonly Random Rnd = new Random();
        private readonly IChromeManager m_chromeManager;
        private readonly IChromeRegistry m_chromeRegistry;
        private readonly ICommunicator m_communicator;
        private readonly IRoutingCommunicator routingCommunicator;
        private readonly IEventRegistrator m_eventRegistrator;
        private readonly IList<Stock> m_stocks;
        private int m_interval = 500;
        private IModulePublisher<Stock> m_publisher;
        private IModulePublisher<SimpleTextMessage> m_messagePublisher; 
        private DispatcherTimer m_timer;

        public StocksModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_,
                            IEventRegistrator eventRegistrator_, ICommunicator communicator_,
                            IRoutingCommunicator routingCommunicator)
        {
            m_chromeRegistry = chromeRegistry_;
            m_chromeManager = chromeManager_;
            m_eventRegistrator = eventRegistrator_;
            m_communicator = communicator_;
            this.routingCommunicator = routingCommunicator;
            m_stocks = new ObservableCollection<Stock>();
            PopulateStocks();
            SimulateTicks();
        }

        public int Interval
        {
            get { return m_interval; }
            set
            {
                m_timer.Stop();
                m_interval = value;
                m_timer.Interval = new TimeSpan(0, 0, 0, 0, m_interval);
                m_timer.Start();
            }
        }

        public ICollectionView Stocks
        {
            get { return CollectionViewSource.GetDefaultView(m_stocks); }
        }

        #region IModule Members

        public void Initialize()
        {
            m_chromeRegistry.RegisterWindowFactory(ViewId, CreateView);
            m_chromeRegistry.RegisterWidgetFactory(WidgetButtonId, m_chromeManager.ShowWindowButtonFactory());

            m_chromeManager.PlaceWidget(WidgetButtonId, "Trade/Stocks",
                                        new InitialShowWindowButtonParameters
                                            {
                                                Text = "View stocks",
                                                ToolTip = "View all stocks",
                                                WindowFactoryID = ViewId,
                                                InitialParameters = new InitialWindowParameters(),
                                            });

            m_publisher = m_communicator.GetPublisher<Stock>();

            m_chromeRegistry.RegisterWidgetFactory(WidgetButtonId2, CreateWidget);
            m_chromeManager.PlaceWidget(WidgetButtonId2, "Trade/Stocks", new InitialWidgetParameters());

            m_messagePublisher = m_communicator.GetPublisher<SimpleTextMessage>();
            m_communicator.GetSubscriber<Trade>().GetObservable().Subscribe(t_ => 
                TaskDialog.ShowMessage("Quantity: " + t_.Quantity + ", Price: " + t_.Price, t_.Symbol));

            routingCommunicator.GetSubscriber<SimpleRoutedMessage>().GetObservable().Subscribe(message => TaskDialog.ShowMessage("Received message: " + message.Content, "Message received"));
        }

        #endregion

        private bool CreateWidget(IWidgetViewContainer emptyViewContainer_, XDocument state_)
        {
            var panel = new StackPanel() {Orientation = Orientation.Horizontal};
            panel.Children.Add(new Label {Content = "Message:"});
            var text = new TextBox() {Width = 300};
            panel.Children.Add(text);
            var button = new Button() {Content = "Send Message"};
            panel.Children.Add(button);
            button.Click += (sender_, args_) => m_messagePublisher.Publish(new SimpleTextMessage(){Text = text.Text});
            emptyViewContainer_.Content = panel;
            return true;
        }

        private bool CreateView(IWindowViewContainer viewcontainer_, XDocument state_)
        {
            var tradesGrid = new TradesGrid() {DataContext = this};
            viewcontainer_.Content = tradesGrid;
            viewcontainer_.Title = "Trading view";
            tradesGrid.DataContext = this;

            IModuleSubscriber<StockSelectedMessage> subscriber = m_communicator.GetSubscriber<StockSelectedMessage>();
            subscriber.GetObservable().Subscribe(m_ =>
                {
                    tradesGrid.Dispatcher.Invoke(
                        new Action(() => Stocks.MoveCurrentTo(
                            m_stocks.First(s_ => s_.Symbol.Equals(m_.Symbol))
                                             )));
                });


            //get the v2v subscriber/publisher until view created to make it work when isolated
            EventHandler<WindowEventArgs> created = null;
            viewcontainer_.Created += created = (sender_, args_) =>
                {
                    IPublisher<Trade> publisher = m_eventRegistrator.RegisterPublisher<Trade>(viewcontainer_);
                    tradesGrid.Publisher = publisher;
                    var subscriber2 = m_eventRegistrator.RegisterSubscriber<SimpleTextMessage>(viewcontainer_);
                    subscriber2.GetObservable().Subscribe(
                        message_ =>
                        TaskDialog.ShowMessage(message_.Text)
                        );
                    viewcontainer_.Created -= created;
                };

                
            return true;
        }

        private void PopulateStocks()
        {
            m_stocks.Add(new Stock {Symbol = "APL", Price = 123.4});
            m_stocks.Add(new Stock {Symbol = "GOOG", Price = 43.1});
            m_stocks.Add(new Stock {Symbol = "MS", Price = 182.6});
            m_stocks.Add(new Stock {Symbol = "XYZ", Price = 56.2});
            m_stocks.Add(new Stock {Symbol = "WPF", Price = 234.5});
        }

        private void SimulateTicks()
        {
            m_timer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 0, 0, m_interval)};
            m_timer.Tick += (s, e) => OnTick();
            m_timer.Start();
        }

        private void OnTick()
        {
            //int r = rnd.Next(m_stocks.Count);
            foreach (var t in m_stocks)
            {
                t.Price += Rnd.NextDouble() * 20 - 10;
                if (m_publisher != null)
                {
                    m_publisher.Publish(t);
                }
            }

            m_timer.Start();
        }
    }
}
