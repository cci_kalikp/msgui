﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Threading;
using System.Windows;
using Newtonsoft.Json;

namespace IsolatedWebModulesExample.Stocks
{
    [JsonObject(MemberSerialization.OptIn), Serializable]
    public class Stock : DependencyObject, ISerializable
    {
        // volatile variables to keep the copies of values for serialziation
        private volatile string _symbol;
        private double _price;

        public Stock()
        {
        }

        protected Stock(SerializationInfo info, StreamingContext context)
        {
            Symbol = (string) info.GetValue("Symbol", typeof (string));
            Price = (double) info.GetValue("Price", typeof (double));
        }

        [JsonProperty]
        public string Symbol
        {
            get { return _symbol; }
            set
            {
                SetValue(SymbolProperty, value);
                _symbol = value;
            }
        }

        [JsonProperty]
        public double Price
        {
            get { return Interlocked.Exchange(ref _price, _price); }
            set
            {
                SetValue(PriceProperty, value);
                Interlocked.Exchange(ref _price, value);
            }
        }

        public static readonly DependencyProperty PriceProperty =
            DependencyProperty.Register("Price", typeof (double), typeof (Stock), new UIPropertyMetadata(0.0));

        public static readonly DependencyProperty SymbolProperty =
            DependencyProperty.Register("Symbol", typeof (string), typeof (Stock), new UIPropertyMetadata(null));

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Symbol", _symbol);
            info.AddValue("Price", _price);
        }
    }
}
