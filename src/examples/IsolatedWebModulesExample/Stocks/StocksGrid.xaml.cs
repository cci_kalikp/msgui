﻿using System.Windows;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace IsolatedWebModulesExample.Stocks
{
    /// <summary>
    /// Interaction logic for TradesGrid.xaml
    /// </summary>
    public partial class TradesGrid
    {
        private IPublisher<Trade> _publisher;

        internal IPublisher<Trade> Publisher { get { return _publisher; } set { _publisher = value; } }

        public TradesGrid()
        {
            InitializeComponent();
        }

        public TradesGrid(IPublisher<Trade> publisher) : this()
        {
            _publisher = publisher;
        }

        
        private bool ProcessUserInput(out Stock stock, out int quantity)
        {
            stock = Grid.SelectedItem as Stock;
            if (!int.TryParse(QuantityTextBox.Text, out quantity))
            {
                TaskDialog.ShowMessage("Quantity must be a number");
                return false;
            }
            if (stock == null)
            {
                TaskDialog.ShowMessage("Please select a stock");
                return false;
            }
            return true;
        }

        public void BuyClick(object sender, RoutedEventArgs e)
        {
            int quantity;
            Stock stock;
            if (ProcessUserInput(out stock, out quantity))
            {
                _publisher.Publish(new Trade
                {
                    Symbol = stock.Symbol,
                    Price = stock.Price,
                    Quantity = quantity
                });
            }
        }

        public void SellClick(object sender, RoutedEventArgs e)
        {
            int quantity;
            Stock stock;
            if (ProcessUserInput(out stock, out quantity))
            {
                var trade = new Trade
                {
                    Symbol = stock.Symbol,
                    Price = stock.Price,
                    Quantity = -quantity
                };
                _publisher.Publish(trade);
            }
        }

        public void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            QuantityTextBox.Clear();
            QuantityTextBox.Focus();
        }
    }
}
