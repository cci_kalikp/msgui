﻿namespace IsolatedWebModulesExample.MultiviewTest
{
    public class DummyMessage
    {
        public string SomeString { get; set; }
        public int SomeInt { get; set; }
    }
}
