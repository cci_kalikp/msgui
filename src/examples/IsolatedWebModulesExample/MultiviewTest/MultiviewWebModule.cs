﻿using System;
using Microsoft.Practices.Unity;
using MSDesktop.WebSupport.CEF;

namespace IsolatedWebModulesExample.MultiviewTest
{
    public class MultiviewWebModule : CEFBasedHTMLModule
    {
        public MultiviewWebModule(IUnityContainer unityContainer) : base(unityContainer)
        {
        }

        protected override string Name
        {
            get { return "MultiviewWebModule"; }
        }

        protected override Uri Url
        {
            get
            {
                //return new Uri("http://google.pl");
                return new Uri(String.Format("{0}\\MultiviewTest\\MultiviewWebModule.html", AppDomain.CurrentDomain.BaseDirectory),
                              UriKind.RelativeOrAbsolute);
            }
        }
    }
}
