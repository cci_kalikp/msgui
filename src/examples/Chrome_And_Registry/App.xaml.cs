﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using System.Windows.Media.Imaging;
using System.IO;
using MorganStanley.MSDotNet.MSLog;
using System.Diagnostics;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Themes;
using Chrome_And_Registry.Modules.Chrome;


namespace Chrome_And_Regsitry
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : Application
  {


    protected override void OnStartup(StartupEventArgs e)
    {
      base.OnStartup(e);
      var boot = new Framework();
      boot.AddSimpleTheme();
      // ...
      //boot.AddModule<Chrome_And_Registry.Modules.Chrome.ChromeModule>();
      //boot.AddModule<Chrome_And_Registry.Modules.Chrome.ControlParadigmModule>();
      //boot.AddModule<Chrome_And_Registry.Modules.Chrome.BindCommandToWidgetModule>();

      //boot.SetShellMode(ShellMode.LauncherBarAndWindow);
      //boot.AddModule<InitialCommandParametersModule>();
      //boot.AddModule<HierarchyMenuModule>();
        boot.AddModule<WidgetChrome>();
        boot.AddModule<WindowChromeModule>();
      boot.Start();

    }

  }

  
}
