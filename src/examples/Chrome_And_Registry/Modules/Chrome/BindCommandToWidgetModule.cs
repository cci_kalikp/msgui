﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;
using System.Xml.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows;
using Infragistics.Windows.Ribbon;
//cw Chrome_Bind_Command_To_Widget :50
/**##description*/
/** this is a example showing you how to bind Command to a widget */
 
/**##namespaces*/
/**use the following namespace*/
/**IChromeManager, IChromeRegistry*/
//{
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
//}
/**RibbonChromeManager*/
//{
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
//}


namespace Chrome_And_Registry.Modules.Chrome
{
  /**##context*/
  /** Command Propoperty is not exposed yet.*/

  public class BindCommandToWidgetModule : IModule
  {
    private IChromeManager manager;
    private IChromeRegistry register;

    public BindCommandToWidgetModule(
      IChromeManager manager,
      IChromeRegistry register
      )
    {
      this.manager = manager;
      this.register = register;

      MyCommand = new RoutedCommand("MyCommand", typeof(BindCommandToWidgetModule));
    }

    
    public void Initialize()
    {

      register.RegisterWidgetFactory("WidgetFactory_BindCommandToWidgetModule");

      /**##code*/
      /**#### 1. cache the Widget container */
      /** the cached widget below is `widget`, which is returned by the `PlaceWidget` call. */
      //{
      var widget = manager.PlaceWidget("WidgetFactory_BindCommandToWidgetModule", 
                  RibbonChromeManager.RIBBON_COMMANDAREA_NAME,
                  "Main/Command",
                  new InitialButtonParameters {
                    Text = "Bind2Command",
                  }
                  );
      //}
      /**#### 2. bind the Command in ContentReady handler */
      /** you can use reflection if you wish. */
      //{
      widget.ContentReady += (s1, e1) =>
      {
        var content = widget.Content;
        var name = content.GetType().Name;
        Type t = content.GetType();

        var b = content as ButtonTool;
        // DO NOT KNOW HOW, but if you do 
        //  b.Command = MyCommand;
        // before 
        //  b.CommandBindings.Add(new CommandBinding(MyCommand, MyCommandExecuted));
        // widget won't show
        b.CommandBindings.Add(new CommandBinding(MyCommand, MyCommandExecuted));
        b.Command = MyCommand;
      };
      //}
    }


    public ICommand MyCommand
    {
      get;
      set;
    }

    private void MyCommandExecuted(object sender_, ExecutedRoutedEventArgs e)
    {
      MessageBox.Show("MyCommandExecuted");
    }
  }
}

/**[raw]</div></td></tr></table>*/
/**##references*/
/**
 * + [Chrome Manager and Chrome Registry][cw_ref Chrome_Management_Introduction] 
 */


