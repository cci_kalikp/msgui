﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;
//cw Migrate_to_Chrome :50
/**##description*/
/** Below shows the migration guide from IControlParadigmxxx to IChromxxx interfaces.
 */
/**##namespaces*/
/**use the following namespace*/
/**IChromeManager, IChromeRegistry*/
//{
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
//}
/**IControlParadigmRegistrator, IControlParadigmImplementor*/
//{
using MorganStanley.MSDotNet.MSGui.Core;
//}
/**RibbonButton*/
//{
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;
//}
using System.Xml.Linq;
using System.Drawing;
/**RibbonChromeManager*/
//{
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
//}
/**ToIamgeSource*/
//{
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
//}
namespace Chrome_And_Registry.Modules.Chrome
{


    /**##context*/
  /**In the code section, left is the code with recommended `IChromexxx` pattern, whilst the right is the obsolte `IControlParadigmxxxx` pattern*/

  public class ChromeModule : IModule
  {
    /**##code*/
    /**[raw] <table border='0'><tr><td><div align="center">*/
    /**IChromeManager and IChromeRegistry*/
    /**#### declare fields*/
    /** declare internal fields hold interface through DI*/
    //{
    private IChromeManager m_chromeManager;
    private IChromeRegistry m_chromeRegistry;
    //}
    /**#### ctor*/
    /** declare control that accepts interface object through DI and intialize the fields*/
    //{
    public ChromeModule(IChromeManager manager_, IChromeRegistry registry_)
    {
      m_chromeManager = manager_;
      m_chromeRegistry = registry_;
    }
    //}
    /**#### initialize the module*/

    #region IModule
    //{
    public void Initialize()
    {
      // Register a window factory with only the initialize handler
      m_chromeRegistry.RegisterWindowFactory(VIEWID,  WindowFactory);
      // register a widget with the dafault Widget factory 
      m_chromeRegistry.RegisterWidgetFactory(WIDGETID);

      // Create the Widget/Button, the widget will create a View identified by the 
      // Window ID - `VIEWID` , and it will create the view with initial parameters to contorl how the view is created.
      // you may pass in other dervied class to InitialWidgetParameters.
      m_chromeManager.PlaceWidget(WIDGETID,
        RibbonChromeManager.RIBBON_COMMANDAREA_NAME,
        "Tab/Group", // location
        new InitialShowWindowButtonParameters
        {
          Image = SystemIcons.Shield.ToImageSource(),
          Text = "Create View",
          ToolTip = "Create A View with IChromeRegistry and IChromeManager",
          WindowFactoryID = VIEWID,
          InitialParameters = new InitialWindowParameters // additional Window/View control parameters
          {
            InitialLocation = InitialLocation.Floating, // where it starts?
            Singleton = true,                           // whether it is singleton
            Width = 800,                                // width
            Height = 100,                               // Height,
          }
        });
    }
    //}

    /**#### the Window factory */
    //{
    private bool WindowFactory(IWindowViewContainer container_, XDocument state_)
    {
      // add your logic here to create the view
      // return true to indicate success
      return true;
    }
    //}

    private const string VIEWID = "ChromeModule_VIEWID";
    private const string WIDGETID = "ChromeModule_WIDGETID";

    #endregion IModule

  }

  /**[raw]</div></td><td>&nbsp;&nbsp;</td><td><div align="center">*/
  public class ControlParadigmModule : IModule
  {
    /**IControlParadigm and ViewManager*/
    /**#### declare fields*/
    /** declare internal fields hold interface through DI*/
    //{
    private IControlParadigmRegistrator m_registor;
    private IViewManager m_viewManager;
    //}

    /**#### ctor*/
    /** declare control that accepts interface object through DI and intialize the fields*/
    //{
    public ControlParadigmModule(IControlParadigmRegistrator registrator_, IViewManager viewManager_)
    {
      m_registor = registrator_;
      m_viewManager = viewManager_;
    }
    //}

    #region ControlParadigmModule
    /**#### initialize the module*/
    /** initialize the modules, using IChromeRegistry and the IChromeManager interface to
     */
    //{
    public void Initialize()
    {
      // Register the View creator for `VIEWID`
      m_viewManager.AddViewCreator(VIEWID, CreateView);

      // Create a Widget by creating RibbonButton, and associate the OnClickCreateView handler to the button
      var button = new RibbonButton(
        new Icon(SystemIcons.Shield, 32, 32), // Icon
        "Create View",      // Text
        "Create a New View via ViewManager and IContorlParadigmRegistry", // tips
        OnClickCreateView // handler
        );

      // add the button to the Ribbon, you can add the button to Menu via the 
      // m_registor.AddMenuItem(...)
      m_registor.AddRibbonItem("Tab", // tab
                                "Group", // group
                                new WrapperPannelRibbonItem(RibbonItemOrientation.Horizontal, button) // Orientation and etc..
                                );

    }
    //}
    #endregion ControlParadigmModule

    /**#### the View create factory/callback*/
    //{
    private void CreateView(IViewContainer viewContainer_)
    {
      // add your logic here to create the views
    }
    //}

    /**#### code the handler to Ribbon Button*/
    //{
    private void OnClickCreateView(object sender_, EventArgs e)
    {
      // request creating the View by ViewManager.CreateView(VIEWID) call.
      m_viewManager.CreateView(VIEWID);
    }
    //}
    #region Fields
    private const string VIEWID = "ControlParadigmModule_VIEWID";
    #endregion Fields
  }
  /**[raw]</div></td></tr></table>*/
  /**##references*/
  /**
   * + [Chrome Manager and Chrome Registry][cw_ref Chrome_Management_Introduction] 
   */
}  


