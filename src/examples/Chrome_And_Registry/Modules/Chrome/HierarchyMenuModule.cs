﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;
using System.Drawing;
using System.Xml.Linq;
using Infragistics.Windows.Ribbon;
//cw Hierarchical_Menu :50
/**##description*/
/** This page will show you how to create hierarchical menus to Application Menu area
 * 
 * *note*: if you have looking to add hierarchical menus to the LauncherBarWindow shell mode, you might change the 
 * `MenuTool` to `MenuItem` or `Menu` depends on the situation.
 */
/**##namespaces*/
/**use the following namespace*/
/**IChromeManager, IChromeRegistry*/
//{
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
//}
/**ToIamgeSource*/
//{
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
//}

namespace Chrome_And_Registry.Modules.Chrome
{
  public  class HierarchyMenuModule : IModule
  {
    /**##context*/
    /**You may need to DI inject the interface of `IChromeManager` and `IChromeManager`; ignore for spcae... */
    private IChromeManager manager;
    private IChromeRegistry registry;

    public HierarchyMenuModule(
      IChromeManager manager, IChromeRegistry registry
      )
    {
      this.manager = manager;
      this.registry = registry;
    }

    /**##code*/
    /** ####1. Create your own custom factory and custom factory's initialXXXXParameter*/
    //{

    class MyInitialMenuParameters : InitialWidgetParameters
    {
      public string Header { get; set; }
    }


    InitializeWidgetHandler MyMenuFactory = (IWidgetViewContainer container_, XDocument state) =>
    {
      var parameters = container_.Parameters as MyInitialMenuParameters;
      if (parameters != null)
      {

        container_.Content = new MenuTool
        {
          Caption = parameters.Header,         // set the captoin to display
          LargeImage =  SystemIcons.Shield.ToImageSource(), // set Image if you want to have images
        };
        return true; // return true to indicate success
      }
      else
      {
        return false; // return true to indicate FAILURE
      } 
    };
    //}

    public void Initialize()
    {
      /** ####2. Register the Custom Menu Factory */
      //{
      registry.RegisterWidgetFactory("menu_level1", MyMenuFactory);
      //registry.RegisterWidgetFactory("menu_level2"); -- you won't plcae widget with lower in the hierarchy 
      //}

      /** ####3. Place the widget to the Menu Area and store the WidgetViewContainer into a vairable*/
      //{
      var menu_level1 = manager.PlaceWidget("menu_level1", RibbonChromeManager.MENU_COMMANDAREA_NAME, new MyInitialMenuParameters
      {
        Header = "Menu1",
      });
      //}

      /** ####4. in the contentReady handler, try to set its children and establish the hierarchy*/
      //{
      menu_level1.ContentReady += (o, e) =>
      {
        ((MenuTool)menu_level1.Content).Items.Add(new ButtonTool() // do the proper cast...
        {
          Caption = "menu_level2",
        });
      };
      //}
      
    }

   
  }
}

/**##references*/
/**
 * + [Chrome Manager and Chrome Registry][cw_ref Chrome_Management_Introduction] 
 */
