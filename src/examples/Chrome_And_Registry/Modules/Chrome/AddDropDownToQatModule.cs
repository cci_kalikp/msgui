﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;
//cw Chrome_Dropdown_Widget_To_Qat :50
/**##description*/
/** Shows how to add dropdown Widget to QAT or Launcher area.
 */
/**##namespaces*/
/**use the following namespace*/
//{
//IChromeManager, IChromeRegistry
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
// DropdownButtonFactory
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
//RibbonChromeManager
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
//IDropdownButtonViewContainer
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;
//}

namespace Chrome_And_Registry.Modules.Chrome
{
  /**##context*/
  /** the code should goes to the `IModule.Initialize()` method
   */
  /**[placeholder]*/
  public class AddDropDownToQatModule :IModule
  {


    private IChromeManager manager;
    private IChromeRegistry registry;
    public AddDropDownToQatModule(
        IChromeManager manager, IChromeRegistry registry)
    {

      this.manager = manager;
      this.registry = registry;
    }

    public void Initialize()
    {
      /**##code*/
      /**#### 1. register and place dropdown with InitialDropdownButtonParameters*/
      /** do the following. */
      //{
      registry.RegisterWidgetFactory("dropdown_test", manager.DropdownButtonFactory());

      IDropdownButtonViewContainer dropdown = manager.PlaceWidget("dropdown_test", "", new InitialDropdownButtonParameters()
      {
        Text = "Lorem ipsum",
        Enabled = true,
        ToolTip = "Open Me!"
      }) as IDropdownButtonViewContainer;
      //}

      /**#### 2. add sub-widgets to the dropdown widget */
      /** an e.g. register sub-widgets. */
      //{
      registry.RegisterWidgetFactory("dropdown_test_content1", manager.ButtonFactory());
      registry.RegisterWidgetFactory("dropdown_test_content2", manager.ButtonFactory());
      //}
      /** the key is the .AddWidget call */
      //{
      dropdown.AddWidget("dropdown_test_content1", new InitialButtonParameters()
      {
        Text = "1",
      });

      dropdown.AddWidget("dropdown_test_content2", new InitialButtonParameters()
      {
        Text = "2",
      });
      //}
    }
  }
}
/**##references*/
/**
 * + [Chrome Manager and Chrome Registry][cw_ref Chrome_Management_Introduction] 
 */

