﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Core;
using Microsoft.Practices.Composite.Presentation.Commands;
using Infragistics.Windows.Ribbon;
using System.Windows.Controls.Primitives;

namespace Chrome_And_Registry.Modules.Chrome
{
  public class InitialCommandParametersModule  : IModule
  {
    private IChromeManager manager;
    private IChromeRegistry registry;
    private IApplication application;


    public InitialCommandParametersModule(
      IChromeManager manager,
      IChromeRegistry registry,
      IApplication application
      )
    {
      this.manager = manager;
      this.registry = registry;

      MyCommand = new RoutedCommand("MyCommand", typeof(InitialCommandParametersModule));
      MyCommand2 = new DelegateCommand<object>(MyCommand2_Executed, MyCommand2_CanExecute);
    }

    void MyCommand_CanExecuteChanged(object sender, EventArgs e)
    {
      MessageBox.Show("MyCommand_CanExecuteChanged");
    }  

    public void Initialize()
    {
      registry.RegisterWidgetFactory("InitialCommandParametersModule_Widget");
      registry.RegisterWidgetFactory("InitialCommandParametersModule_Widget2");

      var widget = manager.PlaceWidget("InitialCommandParametersModule_Widget", "", "Command", new InitialCommandButtonParameters
      {
        Text = "Click me",
        Command = MyCommand,
        CommandBinding = new CommandBinding(MyCommand, MyCommandExecuted, delegate(object o, CanExecuteRoutedEventArgs e_)
        {
          if (count == 2)
          {
            e_.CanExecute = false;
            e_.ContinueRouting = false;
            e_.Handled = true;
          }
          else
          {
            e_.CanExecute = true;
            e_.ContinueRouting = false;
            e_.Handled = true;
          }
        }),
      }
      
      );



      var widget2 = manager.PlaceWidget("InitialCommandParametersModule_Widget2", "", "Command", new InitialCommandButtonParameters
      {
        Text = "Click me (ICommand)",
        Command = MyCommand2,
      });
     
    }


    private int count { get; set; } 


    public ICommand MyCommand
    {
      get;
      set;
    }


    public ICommand MyCommand2
    {
      get;
      set;
    }

    public CommandBinding MyCommandBindings
    {
      get;
      set;
    }

    private void MyCommandExecuted(object sender_, ExecutedRoutedEventArgs e)
    {
      ++count;
      MessageBox.Show("MyCommandExecuted");
    }

    private void MyCommand2_Executed(object parameter)
    {
      MessageBox.Show("MyCommand2_Executed");
    }


    private bool MyCommand2_CanExecute(object parameter)
    {
      if (count == 2)
      {
        return false;
      }
      return true;
    }
  }
}
