﻿//cw Howto_Use_Window_Actions :1
/**##Description*/
/**This page describes how to obtain an instance of a window and how to perform the following actions:
 * activate, flash or close.
 */

//cw Howto_Persist_Restore_Window_State :1
/**##Description*/
/**This page describes how to allow windows to persist their state to the user's profile so that they can
 * be restored when the profile is loaded.*/

//cw Howto_Use_Window_Actions :2 cw Howto_Persist_Restore_Window_State :2
/**##Namespaces*/
using System;
using System.Linq;
using System.Windows.Controls;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
//{
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
//}

namespace Chrome_And_Registry.Modules.Chrome
{
    //cw Howto_Use_Window_Actions :3 cw Howto_Persist_Restore_Window_State :3
    /**##Context*/
    /** Obtain IChromeManager and IChromeRegistry from the container: */
    //{
    public class WindowChromeModule : IModule
    {
        public WindowChromeModule(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_)
        {
            m_chromeManager = chromeManager_;
            m_chromeRegistry = chromeRegistry_;
        }
        //}

        public void Initialize()
        {
            //cw Howto_Use_Window_Actions :5
            /**##Code*/
            /** Register widget and window factories:*/
            //{
            m_chromeRegistry.RegisterWidgetFactory(CreateViewWidgetFactoryId);
            m_chromeRegistry.RegisterWidgetFactory(ActivateViewWidgetFactoryId);
            m_chromeRegistry.RegisterWindowFactory(WindowFactoryID, InitHandler);
            //}

            /** Add a button responsible for window creation to the ribbon: */
            //{
            m_chromeManager.Ribbon["Window actions"]["Create"]
                .AddWidget(CreateViewWidgetFactoryId,
                           new InitialShowWindowButtonParameters
                               {
                                   Text = "Create view",
                                   WindowFactoryID = WindowFactoryID
                               });
            //}

            /** Add another button to the ribbon: */
            //{
            m_chromeManager.Ribbon["Window actions"]["Actions"]
                .AddWidget(ActivateViewWidgetFactoryId,
                           new InitialButtonParameters
                               {
                                   Text = "Activate",
                                   Click = OnActivateButtonClicked
                               }
                );
            //}

            //cw Howto_Persist_Restore_Window_State :5
            /** Register factories for a button and for a window. Notice that we pass the third parameter
             * to the RegisterWindowFactory function. This parameter is a function which is able to serialize
             * the window's state to a XDocument instance.*/
            //{
            m_chromeRegistry.RegisterWindowFactory(PersistRestoreWindowFactory, CreateWindow, PersistWindow);
            m_chromeRegistry.RegisterWidgetFactory(ShowPersistRestoreWindowWidgetFactory);
            //}
            //cw Howto_Persist_Restore_Window_State :8
            /** Place the window creation button on the ribbon: */
            //{
            m_chromeManager.Ribbon["Other"]["Window with state"].AddWidget(
                ShowPersistRestoreWindowWidgetFactory,
                new InitialShowWindowButtonParameters()
                    {
                        Text = "Show window",
                        WindowFactoryID = PersistRestoreWindowFactory
                    });
            //}
        }

        //cw Howto_Persist_Restore_Window_State :6
        /** This is the function used to persist the window's state. It assumes that the window's content
         * is a TextBox. It creates a simple XDocument which simply holds the TextBox's value.*/
        //{
        private static XDocument PersistWindow(IWindowViewContainer viewContainer_)
        {
            var textBox = viewContainer_.Content as TextBox;
            var text = textBox.Text;
            return new XDocument(new XElement("MyCustomState", text));
        }
        //}

        //cw Howto_Persist_Restore_Window_State :7
        /** This is the function used for window creation. Normally, when you don't want to persist state,
         * it simple fills the view container's content. However, this time it should be more generic - it has
         * to handle both the first-time creation of the window and also the situation in which a profile is
         * loaded and the window is re-created. In the latter situation, the state_ parameter will not be null
         * but it will contain the persisted XDocument.
         */
        //{
        private static bool CreateWindow(IWindowViewContainer viewContainer_, XDocument state_)
        {
            var textBox = new TextBox();
            viewContainer_.Content = textBox;
            if (state_ != null)
            {
                var text = state_.Root.Value;
                textBox.Text = text;
            }
            return true;
        }
        //}

        //cw Howto_Use_Window_Actions :6
        /** Implement the window activation logic. Use ChromeManager's Windows collection in order
         * to obtain a reference to the relevant window. In this example we use window factory ID but 
         * any other search criterion can be used (in particular you can hold any information you like
         * inside window_.Content).*/
        //{
        private void OnActivateButtonClicked(object sender_, EventArgs args_)
        {
            var theWindow = m_chromeManager
                .Windows
                .FirstOrDefault(window_ => window_.FactoryID == WindowFactoryID);
            //}
            //cw Howto_Use_Window_Actions :7
            //{
            theWindow.Activate();
            //}
            if (false)
            {
                //cw Howto_Use_Window_Actions :9
                /** Similarly, you can invoke the Close or Flash actions on the window: */
                //{
                theWindow.Close();
                //}
                //cw Howto_Use_Window_Actions :10
                //{
                theWindow.Flash();
                //}
            }
            //cw Howto_Use_Window_Actions :8
            //{
        }
        //}

        private static bool InitHandler(IWindowViewContainer emptyViewContainer_, XDocument state_)
        {
            emptyViewContainer_.Content = new TextBox();
            return true;
        }

        private const string WindowFactoryID = "MyWindow";
        private const string ActivateViewWidgetFactoryId = "MyActivateWidget";
        private const string CreateViewWidgetFactoryId = "MyWidget";
        private const string PersistRestoreWindowFactory = "MyWindow2";
        private const string ShowPersistRestoreWindowWidgetFactory = "ShowPersistRestoreWindowWidgetFactory";
        private readonly IChromeManager m_chromeManager;
        private readonly IChromeRegistry m_chromeRegistry;

    //cw Howto_Use_Window_Actions :4 cw Howto_Persist_Restore_Window_State :4
    //{
    }
    //}
}