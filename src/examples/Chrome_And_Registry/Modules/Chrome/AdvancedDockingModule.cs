﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Xml.Linq;
using System.Windows.Controls;
//cw Chrome_Advanced_Docking :50
/**##description*/
/** Shows some advanced use of Docking of views. you can 
 * 
 * + dock a view as tabbed to the Tab that an existing View is docking to (Tabs in tab)
 * + dock relative to an existing view.
 * 
 * This page will show  you how to dock relative to an existing view to show the necessary knowhow.
 */
/**##namespaces*/
/**use the following namespace*/
/**IChromeManager, IChromeRegistry*/
//{
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
//}



namespace Chrome_And_Registry.Modules.Chrome
{

  /**##context*/
  /**[placeholder]*/

  public class AdvancedDockingModule : IModule
  {
    /**##code*/
    /**#### 1. declare fields*/
    /** declare internal fields hold interface through DI*/
    //{

    private IChromeManager manager;
    private IChromeRegistry registry;
    //}

    /**#### 2. declare a field to cache the fist view*/
    //{
    public IWindowViewContainer FirstTab { get; set; }
    //}

    public bool Singleton { get; set; }

    public double DefaultHeight { get; set; }

    public double DefaultWidth { get; set; }

    /**#### 3. ctor*/
    /** declare control that accepts interface object through DI and intialize the fields.*/
    //{

    public AdvancedDockingModule(
      IChromeManager manager, IChromeRegistry registry)
    {

      this.manager = manager;
      this.registry = registry;
    }
    //}

    /**#### 4. two view's widgets/views factory. */
    //{
    public void Initialize()
    {
      registry.RegisterWidgetFactory("FirstWidget");
      registry.RegisterWidgetFactory("SecondWidget");
      registry.RegisterWindowFactory("FirstView", FirstView);
      registry.RegisterWindowFactory("SecondView", SecondView);


      manager.PlaceWidget("SecondWidget", RibbonChromeManager.RIBBON_COMMANDAREA_NAME, "Main/Subgroup",
        new InitialShowWindowButtonParameters
        {
          Text = "Second Tab",
          WindowFactoryID = "SecondView",
          InitialParameters = new InitialWindowParameters
          {
            InitialLocation = MorganStanley.MSDotNet.MSGui.Core.InitialLocation.DockBottom, // Dock bottom relative to the FirstTab, use InitialLocation.DockBottom if you want Tabs in tab
          }
        }
        );

      manager.PlaceWidget("FirstWidget", RibbonChromeManager.RIBBON_COMMANDAREA_NAME, "Main/Subgroup",
              new InitialShowWindowButtonParameters
              {
                Text = "First Tab",
                WindowFactoryID = "FirstView",
                InitialParameters = new InitialWindowParameters
                {
                  InitialLocation = InitialLocation.DockInNewTab,
                }
              }
       );

    }
    //}

    /**#### 5. first View's Window Factory */
    /** Cache the First view's WindowViewContainer so second tab can use in its `InitialLocationTarget` property.*/
    //{
    private bool FirstView(IWindowViewContainer emptyViewContainer, XDocument state)
    {
      emptyViewContainer.Title = "First Tab";
      if (FirstTab == null) 
      {
        FirstTab = emptyViewContainer;
      }
      return true;
    }
    //}

    /**#### 6. Second View's Window Factory*/
    /** Set `InitialLocationTarget` so second view know where to dock againt. */
    //{
    private bool SecondView(IWindowViewContainer emptyViewContainer, XDocument state)
    {
      var parameter = emptyViewContainer.Parameters;
      if (parameter != null)
      {
        parameter.InitialLocationTarget = FirstTab;
      }
       //...
    //}
      emptyViewContainer.Title = "Second Tab";

      var stack = new StackPanel()
      {
        Children =
        {
          new TextBlock() { Text = "Hello " },
        }
      };

      emptyViewContainer.Content = stack;
   //{
      return true;
    }
    //}

  }
}

/**[raw]</div></td></tr></table>*/
/**##references*/
/**
 * + [Chrome Manager and Chrome Registry][cw_ref Chrome_Management_Introduction] 
 */

