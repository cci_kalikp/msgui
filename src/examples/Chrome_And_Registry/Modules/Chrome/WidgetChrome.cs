﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
//cw WidgetChrome_centralised_placement :1 
/**##Description*/
/** This page describes how to add widgets using the new way. The old PlaceWidget method has been deprecated.
 */

//cw WidgetChrome_key_gestures :1
/**##Description*/
/** This page describes how to assign key gestures to widgets.
 */

//cw WidgetChrome_removing_widgets :1
/**##Description*/
/** This page describes how to remove a widget. The example creates button which disappears when clicked. */

//cw WidgetChrome_create_window :1
/**##Description*/
/** The most common type of widget is one that creates a window. However, sometimes you may wany to provide some 
 * custom logic for window creation. This page explains how to achieve that. It utilises an example in which the window
 * is created conditionally - only when there are no other windows open (<i>not to be confused with Singleton
 * window behaviour</i>).
 */

//cw WidgetChrome_widget_with_state :1
/**##Description*/
/** Widgets, similarly to views, can also have persistable state. MSDesktop allows you to persist
 * widget state in two ways: globally or on per layout basis. The global persistence means that there is only
 * one widget state for all layouts. Depending on the situation you may find either mode useful.
 */

//cw WidgetChrome_centralised_placement :2 cw WidgetChrome_key_gestures :2 cw WidgetChrome_removing_widgets :2 cw WidgetChrome_create_window :2 cw WidgetChrome_widget_with_state :2
/**##Namespaces*/
/**Use the following namespace:*/
//{
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
//}
namespace Chrome_And_Registry.Modules.Chrome
{
    public class WidgetChrome : IModule
    {
        private const string MyWidgetFactoryID = "MyWidgetFactory";
        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;
        private const string WidgetWithGlobalStateFactoryId = "WidgetWithGlobalState";
        private const string MyWidgetWithKeyFactoryID = "MyWidgetWithKeyFactory";
        private const string SelfRemoveWidgetFactoryId = "RemoveSelfButton";
        private const string WidgetWithStateFactoryId = "WidgetWithState";
        private const string CreateViewManuallyFactoryId = "CreateViewManually";
        private const string ManuallyCreatedWindowFactoryId = "ManuallyCreatedWindow";

        public WidgetChrome(IChromeManager chromeManager, IChromeRegistry chromeRegistry)
        {
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
        }

        public void Initialize()
        {
            //cw WidgetChrome_centralised_placement :3
            /**##Context*/
            /**Register a widget factory: */
            //{
            _chromeRegistry.RegisterWidgetFactory(MyWidgetFactoryID,
                (container, state) =>
                    {
                        container.Content = new Button {Content = "Custom button"};
                        return true;
                    });
            //}
            //cw WidgetChrome_centralised_placement :4
            /**##Code*/
            /**Place the widget on the ribbon, on tab "Hello tab" and in the ribbon group "Hello group":*/
            //{
            _chromeManager.Ribbon["Widgets"]["Widget creation"].AddWidget(MyWidgetFactoryID,
                new InitialWidgetParameters());
            //}
            if (false)
            {
                /**Place the widget in the middle area of the titlebar:*/
                //{
                _chromeManager.TitleBar.Middle.AddWidget(MyWidgetFactoryID, new InitialWidgetParameters());
                //}
                /**Place the widget on the left edge of the tabwell:*/
                //{
                _chromeManager.Tabwell.LeftEdge.AddWidget(MyWidgetFactoryID, new InitialWidgetParameters());
                //}
                _chromeManager.RemoveWidget(MyWidgetFactoryID);
            }

            //cw WidgetChrome_key_gestures :3
            /**##Context*/
            /**Register a widget factory: */
            //{
            _chromeRegistry.RegisterWidgetFactory(MyWidgetWithKeyFactoryID);
            //}
            //cw WidgetChrome_key_gestures :4
            /**##Code*/
            /**Place the widget setting the Gesture initial parameter. 
             * Gestures will work with CommandButton, Button or ShowWindowButton:*/
            //{
            _chromeManager.Ribbon["Widgets"]["Key bindings"].AddWidget(MyWidgetWithKeyFactoryID,
                new InitialButtonParameters
                    {
                        Gesture = new KeyGesture(Key.W, ModifierKeys.Alt),
                        Text = "Press Alt+W"
                    });
            //}

            //cw WidgetChrome_removing_widgets :3
            /**##Code*/
            /** Register a widget factory: */
            //{
            _chromeRegistry.RegisterWidgetFactory(SelfRemoveWidgetFactoryId);
            //}
            /** Place a button and provide a click handler. Inside the handler, use the IChromeManager's
             * RemoveWidget method providing the widget's Id to it: */
            //{
            _chromeManager.Ribbon["Examples"]["Remove"].AddWidget(
                SelfRemoveWidgetFactoryId,
                new InitialButtonParameters
                    {
                        Text = "Disappear",
                        Click = (sender_, args_) => _chromeManager.RemoveWidget(SelfRemoveWidgetFactoryId)
                    });
            //}

            //cw WidgetChrome_create_window :3
            /**##Context*/
            /** Register factories for a widget and for a window: */
            //{
            _chromeRegistry.RegisterWidgetFactory(CreateViewManuallyFactoryId);
            _chromeRegistry.RegisterWindowFactory(ManuallyCreatedWindowFactoryId,
                (emptyViewContainer_, document_) => true);
            //}

            /**##Code*/
            /** Place a Button widget. Inside its click handler check for the aforementioned condition.
             * When it's satisfied, use the IChromeManager to create the window.*/
            //{
            _chromeManager.Ribbon["Examples"]["Create view"].AddWidget(
                new InitialButtonParameters
                    {
                        Text = "Create view conditionally",
                        Click = (o_, eventArgs_) =>
                                    {
                                        if (_chromeManager.Windows.Count == 0)
                                        {
                                            _chromeManager.CreateWindow(ManuallyCreatedWindowFactoryId,
                                                                        new InitialWindowParameters());
                                        }
                                    }
                    });
            //}
            /** An important thing to note here is that you are not allowed to call CreateWindow
             * inside Initialize method. If you are willing to do so, it is likely that something is wrong
             * with the design you are trying to enforce. If you want a window to be initially open, the
             * layout persistance is the what you are looking for. You can also set a layout as a default, 
             * pre-loaded one for all users.
             * */

            // cw WidgetChrome_widget_with_state :3
            /**##Code*/
            /**The below code registers a custom widget presenting a TextBox. The TextBox's content is
             * persisted into the widget's state:*/
            //{
            _chromeRegistry.RegisterWidgetFactory(WidgetWithStateFactoryId,
                    (container, state) =>
                        {
                            var textBox = new TextBox();
                            container.Content = textBox;
                            if (state != null)
                            {
                                textBox.Text = state.Root.Value;
                            }
                            return true;
                        },
                    viewContainer =>
                        {
                            var textBox = viewContainer.Content as TextBox;
                            return new XDocument(new XElement("MyWidgetState", textBox.Text));
                        });
            //}
            /**Place the widget on the ribbon:*/
            //{
            _chromeManager.Ribbon["Examples"]["State"].AddWidget(WidgetWithStateFactoryId,
                new InitialWidgetParameters());
            //}
            /**This is an example of how to persist the widget's state globally. The only difference
             * is that one more parameter is provided to the RegisterWidgetFactory function:*/
            //{
            _chromeRegistry.RegisterGlobalStoredWidgetFactory(WidgetWithGlobalStateFactoryId,
                    (container, state) =>
                    {
                        var textBox = container.Content as TextBox?? new TextBox();
                        container.Content = textBox;
                        if (state != null)
                        {
                            textBox.Text = state.Root.Value;
                        }
                        return true;
                    },
                    viewContainer =>
                    {
                        var textBox = viewContainer.Content as TextBox;
                        return new XDocument(new XElement("MyWidgetGlobalState", textBox.Text));
                    });
            //}
            _chromeManager.Ribbon["Examples"]["GlobalPersistor State"].AddWidget(WidgetWithGlobalStateFactoryId,
                new InitialWidgetParameters());
        }
    }
}
