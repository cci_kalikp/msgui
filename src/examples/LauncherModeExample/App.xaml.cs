﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/LauncherModeExample/App.xaml.cs#27 $
// $Change: 884929 $
// $DateTime: 2014/06/16 10:55:05 $
// $Author: istvanf $

//cw Settings_Howto_Set_Application_Bar_Title: 20
/**
 * ## Namespaces
 */
/** Use the following namespaces*/
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

//{
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
//}
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;
using MorganStanley.MSDotNet.MSGui.Themes;
using MorganStanley.Msdotnet.Msgui.Modules.MorganStanley.MSDotNet.MSGui;
using MorganStanley.MSDotNet.MSGui.Themes.MSDesktop2014;

namespace MorganStanley.Msdotnet.Msgui
{
    //cw Settings_Howto_Set_Application_Bar_Title: 50
    /**##context*/
    /**put the code in the following method */
    
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
        //{
		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);
			Framework boot = new Framework();
        //}
			boot.EnableExceptionHandler("msguiadministrators@ms.com");
            boot.SetLoginBehavior(true, false); 
			boot.SetShellMode(ShellMode.LauncherBarAndFloatingWindows);
			boot.EnableLegacyControlPlacement();
			boot.AddSimpleTheme(); 
            //boot.AddModernTheme(Colors.OrangeRed, ModernThemeBase.Dark);
			boot.SetApplicationVersion("1.2.3.4");
			boot.SetApplicationName("Sample Application foo");
            boot.EnableReflectionModuleLoad();
			//msguiFramework.AddBlueTheme();
			//msguiFramework.SetTheme("Blue");
			boot.SetApplicationIcon(BitmapFrame.Create(new Uri(@"pack://application:,,,/Images/sphere.png", UriKind.RelativeOrAbsolute)));
            boot.AddModule<SimpleModule>();
			boot.EnableShutdownTimer(10);
			//msguiFramework.ForceLockerRegistrations(true);

            boot.SetLauncherBarHeight(90, 150);
		    boot.EnableTabDragAndDrop();

            #region Settings_Howto_Set_Application_Bar_Title
            //cw Settings_Howto_Set_Application_Bar_Title :10
            /**##Description*/
            /**Set the title on the Launcher bar */
            //cw Settings_Howto_Set_Application_Bar_Title :70 
            /**##code*/
            //{
            boot.SetAppBarTitle("Welcome to MSDesktop Demo Application");
            //}
            /**##result*/
            /**See the application bar below 
             * ![Application bar](images/Shells/Settings_ApplicationAndShells_AppBar.png)
             */
         
         
            #endregion Settings_Howto_Set_Application_Bar_Title
        //cw Settings_Howto_Set_Application_Bar_Title: 50
        //{
			boot.Start();
		}
        //}
	}

}