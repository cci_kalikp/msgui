﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/examples/LauncherModeExample/Modules/SimpleModule.cs#18 $
// $Change: 893457 $
// $DateTime: 2014/08/19 04:08:01 $
// $Author: caijin $

using System;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.LauncherBar;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.Msdotnet.Msgui.Modules
{
	namespace MorganStanley.MSDotNet.MSGui
	{
		public class SimpleModule : IModule
		{
			readonly IChromeRegistry m_chromeRegistry;
			readonly IChromeManager m_chromeManager;
			readonly IAlert m_alert;

			private const string m_simpleViewId = @"\\LauncherModeExample\SimpleModule\SimpleView";
			private const string m_simpleViewId_DockToHost = @"\\LauncherModeExample\SimpleModule\SimpleView-DockToHost";  
			private const string m_simpleViewId_SizeToContent = @"\\LauncherModeExample\SimpleModule\SimpleView-SizeToContent";
			private const string m_simpleViewId_PlaceAtCursor = @"\\LauncherModeExample\SimpleModule\SimpleView-PlaceAtCursor";

			public SimpleModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, IAlert alert_)
			{
				m_chromeRegistry = chromeRegistry_;
				m_chromeManager = chromeManager_;
				m_alert = alert_;
			}

			public void Initialize()
			{
				//Register view builder
				m_chromeRegistry.RegisterWindowFactory(m_simpleViewId, AddView);
                m_chromeRegistry.RegisterWindowFactory(m_simpleViewId_DockToHost, AddView_DockLeft); 
				m_chromeRegistry.RegisterWindowFactory(m_simpleViewId_SizeToContent, AddView_SizeToContent);
				m_chromeRegistry.RegisterWindowFactory(m_simpleViewId_PlaceAtCursor, AddView_PlaceAtCursor);

				m_chromeManager.Windows.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(ViewsObservable_CollectionChanged);

				//Register button to create view
				m_chromeRegistry.RegisterWidgetFactory(@"\\LauncherModeExample\SimpleModule\Button1");
				m_chromeRegistry.RegisterWidgetFactory(@"\\LauncherModeExample\SimpleModule\Button2");
				m_chromeRegistry.RegisterWidgetFactory(@"\\LauncherModeExample\SimpleModule\Button3");
                m_chromeRegistry.RegisterWidgetFactory(@"\\LauncherModeExample\SimpleModule\Button4");
                m_chromeRegistry.RegisterWidgetFactory(@"\\LauncherModeExample\SimpleModule\Button5");

				m_chromeManager.PlaceWidget(@"\\LauncherModeExample\SimpleModule\Button1", "Group One", new InitialShowWindowButtonParameters()
				{
					Image = BitmapFrame.Create(new Uri(@"pack://application:,,,/Images/sphere.png", UriKind.RelativeOrAbsolute)),
					ToolTip = "Create new window",

					WindowFactoryID = m_simpleViewId,
					InitialParameters = new InitialWindowParameters() 
					{ 
						InitialLocation = InitialLocation.FloatingOnly
					}
				});

				m_chromeManager.PlaceWidget(@"\\LauncherModeExample\SimpleModule\Button2", "Group One", new InitialButtonParameters()
				{
					Image = BitmapFrame.Create(new Uri(@"pack://application:,,,/Images/sphere.png", UriKind.RelativeOrAbsolute)),
					ToolTip = "New window docked in left tab",
					Click = (s, e) =>
					{

						m_chromeManager.CreateWindow(m_simpleViewId_DockToHost, new InitialWindowParameters()
						{
							InitialLocation = InitialLocation.DockLeft
						});
					}
				});



				m_chromeManager.PlaceWidget(@"\\LauncherModeExample\SimpleModule\Button3", "Group One", new InitialShowWindowButtonParameters()
				{
					Image = BitmapFrame.Create(new Uri(@"pack://application:,,,/Images/sphere.png", UriKind.RelativeOrAbsolute)),
					ToolTip = "New size-to-content window",

					WindowFactoryID = m_simpleViewId_SizeToContent,
					InitialParameters = new InitialWindowParameters()
				});

				m_chromeManager.PlaceWidget(@"\\LauncherModeExample\SimpleModule\Button4", "Group One", new InitialShowWindowButtonParameters()
				{
					Image = BitmapFrame.Create(new Uri(@"pack://application:,,,/Images/sphere.png", UriKind.RelativeOrAbsolute)),
					ToolTip = "New window @ cursor",

					WindowFactoryID = m_simpleViewId_PlaceAtCursor,
					InitialParameters = new InitialWindowParameters()
				});

                m_chromeManager.PlaceWidget(@"\\LauncherModeExample\SimpleModule\Button5", "Group One", new InitialButtonParameters()
                {
                    Image = BitmapFrame.Create(new Uri(@"pack://application:,,,/Images/sphere.png", UriKind.RelativeOrAbsolute)),
                    ToolTip = "New window docked tabbed",
                    Click = (s, e) =>
                    {

                        m_chromeManager.CreateWindow(m_simpleViewId_DockToHost, new InitialWindowParameters()
                        {
                            InitialLocation = InitialLocation.DockTabbed
                        });
                    }
                });


			    m_chromeRegistry.RegisterWidgetFactory("separator", (container_, state_) =>
			        {
                        //container_.Content = new Separator()
                        //    {
                        //        LayoutTransform = new RotateTransform(90),
                        //        Margin = new Thickness(3, 0, 3, 0), 
                        //    };
                        container_.Content = new Border()
                            {
                                VerticalAlignment = VerticalAlignment.Stretch,
                                Width = 3,
                                Background = System.Windows.SystemColors.ControlDarkBrush,
                                Margin = new Thickness(3, 0, 3, 0)
                            };
                        return true;
			        });
			    m_chromeManager[ChromeArea.LauncherBar].AddWidget("separator", new InitialWidgetParameters());
                 

				m_chromeRegistry.RegisterWidgetFactory(@"\\LauncherModeExample\SimpleModule\StrangeWidget", CreateStrangeWidget, DehydrateStrangeWidget);

				m_chromeManager.PlaceWidget(@"\\LauncherModeExample\SimpleModule\StrangeWidget", "", new InitialWidgetParameters());
				
				m_chromeManager.AddButton("FooButton", "Group Two", new InitialButtonParameters()
				{
					Text = "Foo"
				});

              var separatorWidget =  m_chromeManager[ChromeArea.LauncherBar].AddWidget("seperator2", new InitialLauncherBarSeparatorParameters()
                  {
                      //override the default margin 3,0,3,0
                      Margin = new Thickness(6,0,6,0)
                  });
                
                //other customization
                VerticalSeparator separator = separatorWidget.Content as VerticalSeparator;
                if (separator != null)
                {
                    //override the default foreground
                    separator.Background = System.Windows.SystemColors.ControlDarkDarkBrush;
                }
                else
                {
                    EventHandler contentReady = null;
                    separatorWidget.ContentReady += contentReady = (sender_, args_) =>
                        {
                            separator = separatorWidget.Content as VerticalSeparator;
                            //override the default foreground
                            separator.Background = System.Windows.SystemColors.ControlDarkDarkBrush;
                            separatorWidget.ContentReady -= contentReady;
                        };
                }
                m_chromeRegistry.RegisterDialogFactory("xx", (d) => {
                    d.Content = "foobar";
                    d.Title = "ohai dialog";
                    return true;
                });

                m_chromeManager.Launcherbar().AddWidget("xx_btn", new InitialButtonParameters()
                    {
                        Text = "Show dialog",
                        Click = (s, e) =>
                            {
                                var d = m_chromeManager.CreateDialog("xx");
                                d.ShowDialog();
                            }
                    });

				//m_chromeManager.WindowDeactivating += (s, e) => 
				//{
				//  var windowContainer = e.ViewContainer;
				//  if (windowContainer != null)
				//  {
				//    Console.WriteLine("***Deactivating view...");
				//    var owner = e.ViewContainer.Content != null ? Window.GetWindow(e.ViewContainer.Content as DependencyObject) : null;
				//    var result = TaskDialog.ShowMessage("Deactivate view?", "Dactivating view", TaskDialogCommonButtons.OKCancel, VistaTaskDialogIcon.Warning, owner);
				//    if (result == TaskDialogSimpleResult.Cancel)
				//      e.Cancel = true;
				//  }
				//};
			}

		    void ViewsObservable_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
			{
				if (e.NewItems != null)
				{
					foreach (IWindowViewContainer vc in e.NewItems)
					{
						System.Diagnostics.Trace.WriteLine(String.Format("You added: {0}", vc.Title));
					}
				}

				if (e.OldItems != null)
				{
					foreach (IWindowViewContainer vc in e.OldItems)
					{
						System.Diagnostics.Trace.WriteLine(String.Format("You removed: {0}", vc.Title));
					}
				}
			}

			private bool CreateStrangeWidget(IWidgetViewContainer widget_, XDocument state)
			{
                if (widget_.Content == null)
                {
                    TextBox textBox = new TextBox() { Text = "Text", Width = 200, AcceptsReturn = true };

                    Label letterCountLabel = new Label();
                    Binding lettersCountBinding =
                        new Binding("Text.Count")
                            {
                                Source = textBox,
                                Mode = BindingMode.OneWay
                            };
                    letterCountLabel.SetBinding(ContentControl.ContentProperty, lettersCountBinding);

                    DockPanel dockPanel = new DockPanel();
                    dockPanel.Children.Add(letterCountLabel);
                    var letterCountPostfixLabel = new Label { Content = "characters" };
                    dockPanel.Children.Add(letterCountPostfixLabel);
                    dockPanel.Children.Add(textBox);

                    widget_.Content = dockPanel;

                    letterCountLabel.SetBinding(UIElement.VisibilityProperty, new Binding()
                        {
                            Path = new PropertyPath(LauncherBarChromeManager.LauncherBarStateProperty),
                            Source = dockPanel,
                            Mode = BindingMode.OneWay,
                            Converter = new VisibleWhenExpandedConverter()
                        });

                    letterCountPostfixLabel.SetBinding(UIElement.VisibilityProperty, new Binding()
                        {
                            Path = new PropertyPath(LauncherBarChromeManager.LauncherBarStateProperty),
                            Source = dockPanel,
                            Mode = BindingMode.OneWay,
                            Converter = new VisibleWhenExpandedConverter()
                        });
                }

                if (state != null)
                {
                    var textBox = (TextBox)((DockPanel)widget_.Content).Children[2];                
                    textBox.Text = state.Root.Value;
                }

				return true;
			}

            private XDocument DehydrateStrangeWidget(IWidgetViewContainer widgetViewContainer_)
            {
                var content = ((TextBox) ((DockPanel) widgetViewContainer_.Content).Children[2]).Text;
                return new XDocument(new XElement("content", content));
            }

			private bool AddView(IWindowViewContainer viewContainer_, XDocument state)
			{
				var viewBtn = new Button();
				viewBtn.Width = 100;
				viewBtn.Height = 100;
				viewBtn.Content = "Hello World" + viewContainer_.ID;
				viewBtn.Click +=
				delegate
				{
					viewContainer_.Width += 200;
					//MessageBox.Show(viewContainer_.ID);
					/*foreach (IViewContainer viewContainer in m_chromeManager)
					{
						viewContainer.Flash();
					}*/
				};


				viewContainer_.Content = viewBtn;
				viewContainer_.Title = "New View";
				viewContainer_.Parameters.Width = 400;
				viewContainer_.Parameters.Height = 300;
				viewContainer_.Parameters.SizingMethod = SizingMethod.Custom;

				return true;
			}

			private bool AddView_DockLeft(IWindowViewContainer viewContainer_, XDocument state)
			{
				AddView(viewContainer_, state);
			    viewContainer_.Parameters.InitialLocationTarget = m_chromeManager.Windows.FirstOrDefault();
				return true;
			}

			private bool AddView_SizeToContent(IWindowViewContainer viewContainer_, XDocument state)
			{
				var viewBtn = new Button();
				viewBtn.Width = 100;
				viewBtn.Height = 100;
				viewBtn.Content = "Hello World" + viewContainer_.ID;
				viewBtn.Click +=
				delegate
				{
					viewBtn.Width += 200;
				};
                viewContainer_.HeaderItems.Add(new Button() { Content = "hello" });

				viewContainer_.Content = viewBtn;
				viewContainer_.Title = "New View";
				viewContainer_.Parameters.Width = 400;
				viewContainer_.Parameters.Height = 300;
				viewContainer_.Parameters.SizingMethod = SizingMethod.SizeToContent;
				viewContainer_.Parameters.EnforceSizeRestrictions = false;
				return true;
			}

			private bool AddView_PlaceAtCursor(IWindowViewContainer viewContainer_, XDocument state)
			{
				AddView(viewContainer_, state);
                viewContainer_.Parameters.InitialLocation = InitialLocation.Floating | InitialLocation.PlaceAtCursor;
                viewContainer_.Header.AddWidget(new InitialButtonParameters() { Text = "OHAI CM2" });
                viewContainer_.Header.AddWidget(new InitialButtonParameters() { Text = "OHAI CM2" });
			    viewContainer_.Icon = SystemIcons.Question;
                return true;
			}
		}
	}

}