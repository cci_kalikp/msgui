﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using MSDesktop.MessageRouting;
using MessageRoutingExampleLib.Messages;
using MessageRoutingExampleOne.Modules;

namespace DemoThree
{
    public partial class DemoWindowContent : UserControl
    {
        private readonly IRoutingCommunicator routingCommunicator;

        private readonly IRoutedPublisher<Foo> fooPublisher;

        private int fooCounter = 0;

        public DemoWindowContent(IRoutingCommunicator routingCommunicator, Color color)
        {
            InitializeComponent();
            this.routingCommunicator = routingCommunicator;

            this.routingCommunicator.GetSubscriber<Foo>().GetObservable().Subscribe(FooMessageArrived);
            this.routingCommunicator.GetSubscriber<Test>().GetObservable().Subscribe(TestMessageArrived);
            this.routingCommunicator.GetSubscriber<DemoThreeMessage>().GetObservable().Subscribe(DemoThreeMessageArrived);

            this.fooPublisher = this.routingCommunicator.GetPublisher<Foo>();
            this.fooPublisher.MessageDelivered += FooPublisherMessageDelivered;
            this.fooPublisher.MessageFailed += FooPublisherMessageFailed;

            Header.Background = new SolidColorBrush(color);
        }

        private void Bar_Click(object sender, RoutedEventArgs e)
        {
            var foo = new Foo
            {
                Action = "FooAction",
                Message = "Foo " + (++fooCounter)
            };

            this.fooPublisher.Publish(foo);
        }

        private void FooPublisherMessageDelivered(object sender, MessageDeliveredEventArgs e)
        {
            Action invoke = () =>
            {
                ResultListBox.Items.Insert(0, "DELIVERED: " + ((Foo)e.Message).Message);
            };

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

        private void FooPublisherMessageFailed(object sender, MessageFailedEventArgs e)
        {
            Action invoke = () =>
            {
                ResultListBox.Items.Insert(0, "FAILED: " + ((Foo)e.Message).Message);
            };

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

        private void FooMessageArrived(Foo foo)
        {
            Action invoke = () =>
            {
                ResultListBox.Items.Insert(0, "ARRIVED: " + foo.Message + " action: " + foo.Action);
            };

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

        private void TestMessageArrived(Test test)
        {
            Action invoke = () =>
            {
                ResultListBox.Items.Insert(0, "ARRIVED: " + test.Message + " action: " + test.Action);
            };

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

        private void DemoThreeMessageArrived(DemoThreeMessage message)
        {
            Action invoke = () => ResultListBox.Items.Insert(0, "ARRIVED: " + message);

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

    }
}
