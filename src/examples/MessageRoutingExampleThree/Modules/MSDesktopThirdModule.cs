﻿using System;
using System.Windows.Media;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MSDesktop.MessageRouting;

namespace DemoThree.Modules
{
    public class MSDesktopThirdModule : IModule
    {
        public const string BlueWindowFactoryID = "BlueWindow";
        public const string YellowWindowFactoryID = "YellowWindow";
        public const string RedWindowFactoryID = "RedWindow";
        public const string GreenWindowFactoryID = "GreenWindow";

        private readonly IChromeManager chromeManager;
        private readonly IChromeRegistry chromeRegistry;
        private readonly IRoutingCommunicator routingCommunicator;

        public MSDesktopThirdModule(IChromeRegistry chromeRegistry, IChromeManager chromeManager, IRoutingCommunicator routingCommunicator)
        {
            this.chromeRegistry = chromeRegistry;
            this.chromeManager = chromeManager;
            this.routingCommunicator = routingCommunicator;
        }

        public void Initialize()
        {
            this.chromeRegistry.RegisterWindowFactory(BlueWindowFactoryID, this.BlueWindowFactory);
            this.chromeRegistry.RegisterWindowFactory(YellowWindowFactoryID, this.YellowWindowFactory);
            this.chromeRegistry.RegisterWindowFactory(RedWindowFactoryID, this.RedWindowFactory);
            this.chromeRegistry.RegisterWindowFactory(GreenWindowFactoryID, this.GreenWindowFactory);

            this.chromeManager[ChromeArea.Ribbon]["Demo"]["Demo window"].AddWidget(new InitialShowWindowButtonParameters
            {
                Text = "Open blue window",
                WindowFactoryID = BlueWindowFactoryID,
                InitialParameters = new InitialWindowParameters()
                {
                    InitialLocation = InitialLocation.DockInNewTab
                }
            });

            this.chromeManager[ChromeArea.Ribbon]["Demo"]["Demo window"].AddWidget(new InitialShowWindowButtonParameters
            {
                Text = "Open yellow window",
                WindowFactoryID = YellowWindowFactoryID,
                InitialParameters = new InitialWindowParameters()
                {
                    InitialLocation = InitialLocation.DockInNewTab
                }
            });

            this.chromeManager[ChromeArea.Ribbon]["Demo"]["Demo window"].AddWidget(new InitialShowWindowButtonParameters
            {
                Text = "Open red window",
                WindowFactoryID = RedWindowFactoryID,
                InitialParameters = new InitialWindowParameters()
                {
                    InitialLocation = InitialLocation.DockInNewTab
                }
            });

            this.chromeManager[ChromeArea.Ribbon]["Demo"]["Demo window"].AddWidget(new InitialShowWindowButtonParameters
            {
                Text = "Open green window",
                WindowFactoryID = GreenWindowFactoryID,
                InitialParameters = new InitialWindowParameters()
                {
                    InitialLocation = InitialLocation.DockInNewTab
                }
            });
        }

        private bool BlueWindowFactory(IWindowViewContainer viewContainer, XDocument state)
        {
            viewContainer.Title = "Blue";
            viewContainer.Content = new DemoWindowContent(routingCommunicator, Colors.Blue);
            return true;
        }

        private bool YellowWindowFactory(IWindowViewContainer viewContainer, XDocument state)
        {
            viewContainer.Title = "Yellow";
            viewContainer.Content = new DemoWindowContent(routingCommunicator, Colors.Yellow);
            return true;
        }

        private bool RedWindowFactory(IWindowViewContainer viewContainer, XDocument state)
        {
            viewContainer.Title = "Red";
            viewContainer.Content = new DemoWindowContent(routingCommunicator, Colors.Red);
            return true;
        }

        private bool GreenWindowFactory(IWindowViewContainer viewContainer, XDocument state)
        {
            viewContainer.Title = "Green";
            viewContainer.Content = new DemoWindowContent(routingCommunicator, Colors.Green);
            return true;
        }
    }

    [Serializable]
    public class Base
    {

    }

    [Serializable]
    public class Derived : Base
    {

    }
}
