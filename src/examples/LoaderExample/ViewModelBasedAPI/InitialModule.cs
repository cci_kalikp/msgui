﻿using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;

namespace LoaderExample.ViewModelBasedAPI
{
    public class InitialModule: IModule
    {
        public InitialModule(IUnityContainer container_)
        {
            container_.RegisterType<ISampleService, SampleService>();
            container_.RegisterType<ISampleService2, SampleService2>();
        }

        public void Initialize()
        {
            
        }
    }
}
