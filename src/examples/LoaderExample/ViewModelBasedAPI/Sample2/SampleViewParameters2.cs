﻿using System.ComponentModel;
using System.Xml.Linq;
using MSDesktop.ViewModelAPI;

namespace LoaderExample.ViewModelBasedAPI
{
    public class SampleViewParameters2 : IFrameworkViewParameters
    {
  
        public string SavedInput { get; set; }

        public int Version { get; set; }

        public IFrameworkViewParameters GetDefault()
        {
            return new SampleViewParameters2();
        }

        public XDocument Upgrade(int version_, XDocument xDocument_)
        {
            return xDocument_;
        }
    }
}
