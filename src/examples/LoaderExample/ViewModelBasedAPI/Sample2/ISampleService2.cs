﻿using System;

namespace LoaderExample.ViewModelBasedAPI
{
    public interface ISampleService2
    {
        DateTime GetDate();
    }
}
