﻿using System;

namespace LoaderExample.ViewModelBasedAPI
{
    class SampleService2 : ISampleService2
    {
        public DateTime GetDate()
        {
            return DateTime.Now;
        }
    }
}
