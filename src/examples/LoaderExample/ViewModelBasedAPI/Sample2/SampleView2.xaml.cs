﻿using System.Windows.Controls;

namespace LoaderExample.ViewModelBasedAPI
{
    /// <summary>
    /// Interaction logic for SampleView2.xaml
    /// </summary>
    public partial class SampleView2 : UserControl
    {
        public SampleView2()
        {
            InitializeComponent();
        }
    }
}
