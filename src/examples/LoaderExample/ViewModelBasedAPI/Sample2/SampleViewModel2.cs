﻿using System;
using System.ComponentModel;
using MSDesktop.ViewModelAPI;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace LoaderExample.ViewModelBasedAPI
{

    public class SampleViewModel2 : ViewModelBase, IFrameworkViewModel<SampleViewParameters2> 
    {
        private readonly ISampleService2 sampleService;

        public SampleViewModel2(ISampleService2 sampleService_)
        {
            sampleService = sampleService_;
        }

        private string someValue;
        public string SomeValue
        {
            get
            {
                return someValue;
            }
            set
            {
                someValue = value;
                OnPropertyChanged("SomeValue");
            }
        }

        public DateTime Date
        {
            get { return sampleService.GetDate(); }
        }

        public void Dispose()
        {
        }

        public void SetState(SampleViewParameters2 state_)
        {
            someValue = state_.SavedInput;
        }

        public SampleViewParameters2 GetState()
        {
            return new SampleViewParameters2 { SavedInput = SomeValue };
        }

        public bool CanClose
        {
            get
            {
                if (string.IsNullOrWhiteSpace(SomeValue))
                {
                    TaskDialog.ShowMessage("Some Value cannot be empty", "Error", TaskDialogCommonButtons.Close,
                                           VistaTaskDialogIcon.Error);
                    return false;
                }
                return true;
            }
        }

        public void SetCloseAction(Action close_)
        {
        }

        public void Closed()
        {
        }
         
    }
}
