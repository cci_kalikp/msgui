﻿namespace LoaderExample.ViewModelBasedAPI
{
    public interface ISampleService
    {
        double GetPrice();
    }
}
