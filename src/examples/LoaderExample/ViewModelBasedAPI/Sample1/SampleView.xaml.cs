﻿using System.Windows.Controls;

namespace LoaderExample.ViewModelBasedAPI
{
    /// <summary>
    /// Interaction logic for SampleView.xaml
    /// </summary>
    public partial class SampleView : UserControl
    {
        public SampleView()
        {
            InitializeComponent();
        }
    }
}
