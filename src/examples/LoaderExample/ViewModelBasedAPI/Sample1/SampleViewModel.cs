﻿using System;
using System.ComponentModel;
using MSDesktop.ViewModelAPI;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace LoaderExample.ViewModelBasedAPI
{
 
    public class SampleViewModel : ViewModelBase, IFrameworkViewModel<SampleViewParameters> 
    {
        private readonly ISampleService sampleService;

        public SampleViewModel(ISampleService sampleService_)
        {
            sampleService = sampleService_;
        }

        private bool someValue;
        public bool SomeValue
        {
            get
            {
                return someValue;
            }
            set
            {
                someValue = value;
                OnPropertyChanged("SomeValue");
            }
        }

        public string Price
        {
            get
            {
                return sampleService.GetPrice().ToString();
            }
        }

        public void Dispose()
        {
        }

        public void SetState(SampleViewParameters state_)
        {
            someValue = state_.SavedInput;
        }

        public SampleViewParameters GetState()
        {
            return new SampleViewParameters {SavedInput = SomeValue};
        }

        public bool CanClose
        {
            get
            {
                return true;
            }
        }

        public void SetCloseAction(Action close_)
        {
        }

        public void Closed()
        {
        }
         
    }
}
