﻿using System.Xml.Linq;
using MSDesktop.ViewModelAPI;

namespace LoaderExample.ViewModelBasedAPI
{
    public class SampleViewParameters : IFrameworkViewParameters
    {
        public SampleViewParameters()
        {
            Version = 2;
        }
        public bool SavedInput { get; set; }

        public int Version { get; set; }

        public IFrameworkViewParameters GetDefault()
        {
            return new SampleViewParameters();
        }

        public XDocument Upgrade(int version_, XDocument xDocument_)
        {
            if (version_ < 2)
            {
                //in older version, we use attribute instead of element to persist "SavedInput"
               var savedInput = xDocument_.Root.Attribute("SavedInput");
                if (savedInput != null)
                {
                    xDocument_.Root.SetElementValue("SavedInput", savedInput.Value);
                }
            }
            return xDocument_;
        }
    }
}
