﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;

namespace LoaderExample.Modules
{
    public class URLInvokerRedefineModule:IModule
    {
        private readonly IChromeManager chromeManager;
        private readonly IChromeRegistry chromeRegistry;
        
        public URLInvokerRedefineModule(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_)
        {
            chromeManager = chromeManager_;
            chromeRegistry = chromeRegistry_;
        }
        public void Initialize()
        {
            (chromeRegistry.WindowFactoryIDs as INotifyCollectionChanged).CollectionChanged += WindowFactoryIDs_CollectionChanged;
        }

        void WindowFactoryIDs_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (var newItem in e.NewItems)
                {
                    if (newItem as string == "MyURLInvokerID")
                    {
                        chromeManager[ChromeArea.QAT].AddWidget(new InitialShowWindowButtonParameters()
                            {
                                WindowFactoryID = "MyURLInvokerID",
                                Text = "Open Web Page",
                                InitialParameters = new InitialWindowParameters()
                                    {
                                        InitialLocation = InitialLocation.DockInNewTab
                                    }
                            });
                        (chromeRegistry.WindowFactoryIDs as INotifyCollectionChanged).CollectionChanged -= WindowFactoryIDs_CollectionChanged;

                        break;
                    }
                }
            }
        }
    }
}
