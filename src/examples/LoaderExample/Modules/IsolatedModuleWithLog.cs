﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using MSDesktop.Isolation.Interfaces;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDotNet.My;

namespace LoaderExample.Modules
{
    public class IsolatedModuleWithLog:IModule
    {
        private readonly IUnityContainer container;
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<IsolatedModuleWithLog>();
        public IsolatedModuleWithLog(IUnityContainer container_)
        {
            container = container_;
        }
        public void Initialize()
        {
            if (container.IsRegistered<IMSDesktopSubsystemSite>())
            {
                var site = container.Resolve<IMSDesktopSubsystemSite>();
                string directory = Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    @"Morgan Stanley"), "Isolated Logs"); 
                
                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);
                }
                string logFileName = Path.Combine(directory,
                                              "IsolatedModuleWithLog_" + Process.GetCurrentProcess().Id +
                                              DateTime.Now.ToString("yyyyMMdd") + ".log");
                var logFile = new MSLogFileDestination(logFileName);
                MSLog.AddDestination("/default/fileDestination", logFile);
                MSLog.SetThresholdPolicy("/", "*/*/*:All"); 

                site.SetLogFilePath(logFileName);
            }
            Logger.Debug("Test Log", "Initialize");
        }
    }
}
