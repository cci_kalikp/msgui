﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace LoaderExample.Modules
{
    static class MessageSender
    {
        public static bool CreateSenderWidget(IWidgetViewContainer widget, XDocument state)
        {
            var label = new Label { Content = "Message:" };
            var textBox = new TextBox { Width = 150, AcceptsReturn = true };
              
            WrapPanel panel = new WrapPanel();
            panel.Orientation = Orientation.Vertical;  
            var button = new Button() { Content = "Send Email", Margin = new Thickness(5)};
            button.Click += (sender_, args_) =>
                {
                    var msg = new MailMessage();
                    msg.From = new MailAddress(Environment.UserName + "@ms.com");
                    msg.To.Add(new MailAddress(Environment.UserName + "@ms.com"));
                    msg.Subject = "Publish message to Launcher";
                    msg.IsBodyHtml = true;
                     
                    string url = DispatchableLauncherModuleMessage.GenerateLink(
                         "SendingMessage",
                         new Dictionary<string, object>() 
                      {  
                          { "Description", textBox.Text }, 
                          { "Date", DateTime.Now},  
                      });

                    msg.Body = string.Format("Click <a href=\"{0}\">here</a> {1}</br></br>If it doesn't open in your web browser, try copy following url: {0} and open it manually in your web browser</br>",
                        url, "to publish SendingMessage to Launcher"); 
                    using (var client = new SmtpClient("mta-hub.ms.com"))
                    {
                        client.Send(msg);  
                    } 
                };
            panel.Children.Add(button);

            var button2 = new Button() { Content = "Publish Directly", Margin = new Thickness(5) };
            button2.Click += (sender_, args_) => DispatchableLauncherModuleMessage.Publish("SendingMessage",
                new Dictionary<string, object>() 
                    {  
                        { "Description", textBox.Text }, 
                        { "Date", DateTime.Now},  
                    });

            panel.Children.Add(button2);

            var dockPanel = new DockPanel(); 
            dockPanel.Children.Add(label);
            dockPanel.Children.Add(textBox);
            dockPanel.Children.Add(panel); 
            widget.Content = dockPanel;
            return true;
        }
 
    }
}
