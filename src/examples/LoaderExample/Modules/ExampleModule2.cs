﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using MSDesktop.ComponentEntitlements;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;

namespace LoaderExample.Modules
{ 
    public class ExampleModule2:IModule
    {
        private readonly IChromeManager chromeManager;
        private const string TestButton1ID = "ModuleTest1Button"; 
        private const string TestButton2ID = "ModuleTest2Button";
        private const string TestButton3ID = "ModuleTest3Button";
        private readonly IChromeRegistry chromeRegistry;
        private const string AboutWindowID = "AboutWindow";
        private readonly IAlert alert;
        public ExampleModule2(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_, IAlert alert_)
        {
            chromeManager = chromeManager_;
            chromeRegistry = chromeRegistry_;
            alert = alert_;
        }

        public void Initialize()
        {
            chromeRegistry.RegisterWidgetFactory(TestButton1ID).CheckEntitlement();
            chromeManager.AddWidget(TestButton1ID, new InitialButtonParameters()
                {
                    Text = "Test Button 1",
                    Click = Click1,
                    Size = ButtonSize.Small
                }, chromeManager[ChromeArea.Ribbon]["Tools"]["Modules"]);
            chromeRegistry.RegisterWidgetFactory(TestButton2ID).CheckEntitlement();
            chromeManager.AddWidget(TestButton2ID, new InitialButtonParameters()
            {
                Text = "Test Button 2",
                Click = Click2,
                Size = ButtonSize.Small
            }, chromeManager[ChromeArea.Ribbon]["Tools"]["Modules"]);

            chromeRegistry.RegisterWidgetFactory(TestButton3ID, MessageSender.CreateSenderWidget);
            chromeManager.AddWidget(TestButton3ID, new InitialWidgetParameters()
            {
                Text = "Send Message"
            }, chromeManager[ChromeArea.Ribbon]["Tools"]["Modules"]);

            chromeManager.AddWidget("AboutButton", new InitialShowWindowButtonParameters()
            {
                Text = "About", 
                Size = ButtonSize.Small,
                WindowFactoryID = AboutWindowID,
                InitialParameters = new InitialWindowParameters()
                    {
                        InitialLocation = InitialLocation.FloatingOnly, 
                    }
            }, chromeManager[ChromeArea.Ribbon]["Help"]["Info"]);
             
            chromeRegistry.RegisterWindowFactory(AboutWindowID, (container_, state_) =>
                {
                    container_.Title = "About";
                    var dropdown = container_.Header.AddWidget(Guid.NewGuid().ToString(), new InitialDropdownButtonParameters()
                    {
                        Text = "Items", 
                    });
                    dropdown.AddWidget(new InitialButtonParameters()
                    {
                        Text = "Item 1", 
                    });
                    container_.Content = new TextBlock() {Text = "MSDesktop Loader Example", HorizontalAlignment = HorizontalAlignment.Center, VerticalAlignment =  VerticalAlignment.Center};
                    container_.Tooltip = "About Loader Example";
                    return true;
                });
        }

        private void Click1(object sender_, EventArgs eventArgs_)
        {
             alert.SubmitAlert(new Alert(){ AlertLevel = AlertLevel.Critical, Text = "Critical Alert"});
        }

        private void Click2(object sender_, EventArgs eventArgs_)
        {
            alert.SubmitAlert(new Alert() { AlertLevel = AlertLevel.Minor, Text = "Minor Alert" });

        }

    }
}
