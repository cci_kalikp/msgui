﻿
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using MSDesktop.ComponentEntitlements;
using MSDesktop.ModuleEntitlements;
using MorganStanley.Desktop.Entitlement;
using MorganStanley.MSDotNet.MSGui.Core; 

namespace LoaderExample
{
    public static class EntitlementHelper
    {
        public static object GetResourceForModule(string moduleName_)
        {
           if (moduleName_ == "TradeViewModule")
           {
               return new TradeTypeResource() {TradeTypeCode = "OTCOption"};
           }
           if (moduleName_ == "BulkLoaderModule")
           {
               return new TradeTypeResource() { TradeTypeCode = "Varswap" }; 
           } 
            return null;
        }

        public static object GetResourceForComponent(string componentID_)
        {
             if (componentID_.StartsWith("TradeView"))
             {
                 return new TradeTypeResource() { TradeTypeCode = "OTCOption" };
             }
             if (componentID_.StartsWith("BulkLoader"))
             {
                 return new TradeTypeResource() { TradeTypeCode = "Varswap" };
             }
             return null;
        }

        public static void ModuleBlockedCallback(object sender, BlockingModuleEventArgs args_)
        {
            //if no entitlement defined instead of blocked explicitly, allow it
            if (args_.Cause == ModuleStatus.BlockedByNoEntitlementsInfo) args_.Cancel = true;
        }
    }

    [EntitlementResource(ResourceType = "TradeType")]
    public class TradeTypeResource
    {
        [EntitlementResourceProperty("tradeTypeCode")]
        public string TradeTypeCode { get; set; }
    }
    public class ExceptionFieldHandler : IComponentEntitlementHandler
    {
        public bool Handle(System.Windows.DependencyObject dependencyObject_, string resourceId_, IEnumerable<string> permittedActions_)
        {
            if (resourceId_ == "EditableField")
            {
                return true;
            }
            return false;
        }
    }
    
}
