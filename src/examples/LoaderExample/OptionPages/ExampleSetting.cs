﻿using System;
using System.ComponentModel;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.MSGui.Impl.Options;

namespace LoaderExample.OptionPages
{
    [Serializable]
    public class ExampleSetting : ViewModelBase, IOptionViewViewModel
    { 
        private bool booleanSetting;
        [DisplayName("Boolean Setting")]
        [SearchTags("Advanced,Test")]
        public bool BooleanSetting
        {
            get { return booleanSetting; }
            set
            {
                booleanSetting = value;
                base.OnPropertyChanged("BooleanSetting");
            }
        }

        private string stringSetting;
        [DisplayName("String Setting")]
        [SearchTags("Advanced,Test")]
        public string StringSetting
        {
            get { return stringSetting; }
            set
            {
                stringSetting = value;
                base.OnPropertyChanged("StringSetting");
            }
        }


        public object Clone()
        {
            var copy = new ExampleSetting();
            copy.AcceptChanges(this);
            return copy;
        }
        public void AcceptChanges(IOptionViewViewModel newViewModel_)
        {
            var vm = newViewModel_ as ExampleSetting;
            BooleanSetting = vm.BooleanSetting;
            StringSetting = vm.StringSetting;
        }
        public bool Validate(out string errorString_, out string invalidControlName_, out string invalidPropertyName_)
        {
            errorString_ = invalidControlName_ = invalidPropertyName_ = null;
            if (!BooleanSetting)
            {
                errorString_ = StringSetting;
                invalidPropertyName_ = "StringSetting";
            }
            return BooleanSetting;
        }
        public XDocument Persist()
        {
            return PersistenceExtensions.Store(this);
        }

        public void LoadState(XDocument state_)
        {
            var loaded = PersistenceExtensions.Restore<ExampleSetting>(state_);
            AcceptChanges(loaded);
        }

        public bool HasCustomTemplate
        {
            get { return true; }
        }
    }
}
