﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace LoaderExample.Applets
{
    public partial class ExampleForm : Form
    {
        public ExampleForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var vc = this.GetWindowViewContainer();
            vc.AccentColour = Colors.DarkOrange;
        }
    }
}
