﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSNet;

namespace LoaderExample.Applets
{
    public class TestApplet : ConcordAppletBase
    {
        private const string Test1ButtonID = "AppletTest1Button";
        private const string Test1ButtonCaption = "Test Button 1";
        private const string Test2ButtonID = "AppletTest2Button";
        private const string Test2ButtonCaption = "Test Button 2";
        private const string Test3ButtonID = "AppletTest3Button";
        private const string Test3ButtonCaption = "Test Button 3"; 
        private const string Test4ButtonID = "AppletTest4Button";
        private const string Test4ButtonCaption = "Test Button 4"; 
        private const string  OPTIONS = "Loader Example";
        private IConcordApplication application;
        public TestApplet(IMSNetLoop uiLoop_)
            : base(uiLoop_)
        {
        }

        #region ConcordAppletBase Members

        public override void OnConnect(IConcordApplication application_)
        {
            base.OnConnect(application_);
            application = application_;

            //General Options for all users
            _application.AddOptionPage(OPTIONS, "General", new ExampleOptionsPage());

            var button1 =
                (ICommandBarButton)
                application_.CommandBars.AddNamedCommand(Test1ButtonID, Test1ButtonCaption, null,
                                                         CommandBarControlType.Button, EventHandler1, _uiLoop);
            button1.Style = CommandBarControlStyle.Caption;
            _application.CommandBars["Tools"].AddCommand(button1);

            var button2 =
                (ICommandBarDropDownButton)
                application_.CommandBars.AddNamedCommand(Test2ButtonID, Test2ButtonCaption, null,
                                                         CommandBarControlType.DropDown, null, _uiLoop);
            _application.CommandBars["Tools"].AddCommand(button2);

            var button3 =
                (ICommandBarDropDownButton)
                application_.CommandBars.AddNamedCommand(Test3ButtonID, Test3ButtonCaption, null,
                                                         CommandBarControlType.DropDown, null, _uiLoop); 
            button2.CommandBar.AddCommand(button3);

            var button4 =
    (ICommandBarButton)
    application_.CommandBars.AddNamedCommand(Test4ButtonID, Test4ButtonCaption, null,
                                             CommandBarControlType.Button, null, _uiLoop);
            button4.Style = CommandBarControlStyle.Caption;
            button3.CommandBar.AddCommand(button4);
        }

        private void EventHandler1(object sender_, ItemClickEventArgs e_)
        {
            application.WindowManager.CreateWindow("Test Window", new ExampleForm()); 
        }

        private void EventHandler2(object sender_, ItemClickEventArgs e_)
        {
             
        }
        public override string Description
        {
            get { return "Test Applet"; }
        }

        public override string Title
        {
            get { return "Test Applet"; }
        }
        #endregion
    }
}
