﻿using System.Collections.Generic;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Alert;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace LoaderExample.BootModules
{
    //cw Loader: 20
    //{
    public class BootModule : IBootModule
    { 
        public void Initialize(Framework framework_)
        {
            framework_.SetDefaultWindowParameters(new InitialWindowParameters() { Singleton = true });
            framework_.SetAlertConsumer(
      new MultipleAlertConsumer(AlertChooser,
                                new AlertConsumerStatusBar(),
                                new AlertConsumerDialogBox()));
        }

        private IEnumerable<IAlertConsumer> AlertChooser(Alert alert_, IList<IAlertConsumer> consumers_)
        {
            yield return alert_.AlertLevel >= AlertLevel.Critical ? consumers_[0] : consumers_[1]; 
        } 
    }
    //}
}
