﻿#region Loader Codewiki
//cw Loader: 10
/**##Description*/
/**MSDesktop loader is a configuration based general purpose MSDesktop application starter*/
/**Configuration can be created and saved through the rich config editor, it can then be referenced by the msdesktop loader.*/

/**##Benefit */
/**Different from using code to customize the MSDesktop application, there are some benefits of using MSDesktop loader as the start up */
/** <ul>
 *    <li>Provides super rich configuration for almost all customizable appearance, layout, behavior, logging, entitlement, etc without coding, thus allows easy start and easy change
 *    <li>Defines which, where and how modules, isolated modules to be loaded through
 *    <li>Much less knowledge to MSDesktop is needed to create different kinds of MSDesktop application utilizing configuration editor
 *  </ul>
 */

/**##Editor*/
/**Although we provided MSDesktopConfig.xsd to ease the creation of msdesktop config through xml directly, config editor makes it much more intuitive and easy to manage*/
/**You could get the configuration editor from \\san01b\DevAppsGML\dist\msdotnet\PROJ\msgui\[latest version]\bin\MSDesktop.LoaderConfigEditor*/
/**###Convenient value editor*/
/**Different editors, take, path editor, font editor, color editor, centralized control placement editor are provided to ease input*/
/**![Centralized control placement editor](images/Loader/ControlPlacementEditor.png)*/
/**###Sub configuration*/
/**Several complex configuration items(Entitlement, ViewModelBasedButtons, BootModules, Modules and IsolatedSubsystems) can not only be configured in side the main config file, but also configured in an external file, which can then replaced/reused by the main configuration easily*/
/**![Turn onf external editor](images/Loader/ExternalEditor1.png)*/
/**![External editor](images/Loader/ExternalEditor2.png)*/
/**###Rich description and visualized configuration*/
/**When a specific configuration item is selected in the editor, in the bottom of the editor, it would show detailed explanation of its usage, default value, example value, codewiki path(if there is one), and a snapshot showing the effect(if it affects display) */
/**![Rich description](images/Loader/RichDescription.png)*/
/**###Restore/Reset*/
/**The value of the configuration item can be rolled back to its original value(rolling back the value of the parent configuration would rollback the value of its children configuration item automatically) if changed, or reset to the default value*/
/**![Restore/Reset](images/Loader/RestoreReset.png)*/
/**###Search*/
/**Type ahead search can be done upon the name and description of all the configuration items, selecting the name would set focus on the configuration item or bring up its editor if it's hosted in some sub configuration. */
/**This functionality can also be used as a quick way to study MSDesktop, due to the rich description provided.*/
/**![Search](images/Loader/Search.png)*/

/**##Advanced Configuration*/
/** If there is any msdesktop feature that is not supported by configuration or the logic is more complex than a static value, a class that implements "IBootModule" can be created and added to the "BootModules" collection. This class would accept a Framework object in the initialize method and do some initialization there as following */

/**##Where to specify the config */
/**There are several ways to specify which config to use ordered by check priority*/
/** 1. Through command line /msdesktopconfig:[configname]. here the config name is usually the path of the config file name without file extension(<b>*.msdesktopconfig</b>) */
/** 2. Through "msdesktopconfig" key in appSettings on app.config as following, and the definition of config name is same as above */
//cw Loader: 40
/** 3. Through "MSDesktop" section in app.config as following */
//cw Loader: 50
/** 4. Call the StartUp.Start method that accepts a Stream object as parameter, thus makes the configuration storage more flexible*/

/**##Startup code using the Loader */
/** 1. User could create a clean WPF application, then reference MSDotNet.MSGui.Core.dll, MSDotNet.MSGui.Impl.dll, MSDotNet.MSGui.Themes.dll, MSDotNet.MSGui.Controls.dll and MorganStanley.Desktop.Loader.dll*/
/**Then the start up code would look like this */ 
//cw Loader: 70
/** 2. Or could download the source code of this example, remove all unneeded modules, resources and use the App.xaml as the start up */



#endregion
using System; 
using System.IO;  
using System.Windows; 
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.Desktop.Loader; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi; 
using MorganStanley.MSDotNet.MSGui.Impl.SmartApi;
using Environment = System.Environment;

namespace LoaderExample
{
    public partial class App : Application
    {
        //cw Loader: 60
        //{
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
        //}
 
            if (1 == 2)
            {        
                //cw Loader: 61
                //{
                StartUp.Start();
                //}
            }
            try
            {
                //comment out following line if used for production code
                this.PrepareExampleReference(); 
                string loaderConfigEditorPath = Extensions.GetMSDesktopApplicationLocation(MSDesktopApplication.LoaderConfigEditor);
                Environment.SetEnvironmentVariable("LoaderConfigEditorPath", loaderConfigEditorPath);
                string msdesktopAFS = Extensions.MSDesktopAFS;
                Environment.SetEnvironmentVariable("SubsystemARFile",
                                                   !string.IsNullOrEmpty(msdesktopAFS)
                                                       ? Path.Combine(msdesktopAFS,@"assemblies\MSDesktop.Isolation.Loader.msde.config")
                                                       : "subsystem.dev.msde.config");
                if (!StartUp.Start())
                { 
                    var options = new TaskDialogOptions
                    {
                        Title = "MSDesktop Loader",
                        MainInstruction = "Failed to read msdesktop configuration", 
                        MainIcon = VistaTaskDialogIcon.Error, 
                        Owner = null,
                        ForceNullOwner = true,
                        AllowDialogCancellation = false,
                    };
                    TaskDialog.Show(options);
                    try
                    {
                        Current.Shutdown(0);
                    }
                    catch
                    {

                    }
                }
            }
            catch (Exception ex_)
            {
                var options = new TaskDialogOptions
                {
                    Title = "MSDesktop Loader",
                    MainInstruction = "Failed to start application",
                    Content = ex_.Message,
                    MainIcon = VistaTaskDialogIcon.Error,
                    ExpandedInfo = ExceptionUtil.GetDetailedMessage(ex_),
                    Owner = null,
                    ForceNullOwner = true,
                    AllowDialogCancellation = false,
                };
                TaskDialog.Show(options);

            }
            //cw Loader: 62
            //{
        }
        //}


    }
}