﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Xaml;

namespace LoaderExample
{
    internal static class ExceptionUtil
    {
        public static string GetDetailedMessage( Exception exception)
        {
            var sb = new StringBuilder();
            if (exception is TargetInvocationException)
            {
                sb.AppendLine("Exception was wrapped into a TargetInvocationException");
                exception = exception.InnerException;
            }

            var xamlParseException = exception as XamlParseException;
            if (xamlParseException != null)
            {
                try
                {
                    sb.AppendFormat("LN: {0}, LP: {1}{2}{3}{2}", xamlParseException.LineNumber,
                                    xamlParseException.LinePosition, Environment.NewLine,
                                    xamlParseException.Message);
                }
                catch
                {
                }
            }

            sb.AppendLine(exception.ToString());

            sb.AppendLine(Environment.NewLine + "------------------" + Environment.NewLine);
            try
            {
                sb.AppendFormat("Username: {0} Machine: {1} OS: {4} CLR: {5}{6} CmdLine: {2}{6} CD: {3}{6}",
                                Environment.UserName, Environment.MachineName, Environment.CommandLine,
                                Environment.CurrentDirectory,
                                Environment.OSVersion, Environment.Version, Environment.NewLine);
                sb.AppendLine(Environment.NewLine + "------------------" + Environment.NewLine);
                var envs = Environment.GetEnvironmentVariables();
                var sortedenv = new SortedDictionary<string, string>();
                foreach (DictionaryEntry dictionaryEntry in envs)
                {
                    sortedenv.Add(dictionaryEntry.Key.ToString(), dictionaryEntry.Value.ToString());
                }

                foreach (KeyValuePair<string, string> dictionaryEntry in sortedenv)
                {
                    sb.AppendFormat("{0} : {1} {2}", dictionaryEntry.Key, dictionaryEntry.Value, Environment.NewLine);
                }
                sb.AppendLine(Environment.NewLine + "------------------" + Environment.NewLine);
            }
            catch (Exception exc)
            {
                // This would just mean a part of information would not be contained in the mail
            }
            sb.AppendLine("This is the list of the currently loaded assemblies: ");
            sb.AppendLine(
                "Please note that this listing only includes assemblies already loaded thus some referenced assemblies may be missing");
            sb.AppendLine(
                "Please also take into consideration that it lists assemblies only from the current Application Domain");
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                sb.AppendLine("Name: " + assembly.GetName().Name);
                sb.AppendLine("Version: " + assembly.GetName().Version);
                sb.AppendLine("Global cache: " + assembly.GlobalAssemblyCache);
                string location = GetLocation(assembly);
                sb.AppendLine("Location: " + location);
            }
            return sb.ToString();
        }

        private static string GetLocation(Assembly assembly)
        {
            string location = "Cannot be retrieved.";
            try
            {
                if (assembly != null && !(assembly is AssemblyBuilder) &&
                    assembly.GetType().FullName != "System.Reflection.Emit.InternalAssemblyBuilder")
                    location = assembly.Location;
            }
            catch (NotSupportedException)
            {
                // in case of dynamic assemblies the Location property cannot be retrieved,
                // so let's ignore it.
            }
            return location;
        }
    }
}
