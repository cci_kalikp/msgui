﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;

namespace ConcordIntegrationExample
{
    class AppletMapModule:IModule
    {
        private IChromeManager chromeManager;
        private IChromeRegistry chromeRegistry;
        public AppletMapModule(IConcordTypeToViewCreator mapper_, IChromeManager chromeManager_,
                               IChromeRegistry chromeRegistry_)
        {
            chromeManager = chromeManager_;
            chromeRegistry = chromeRegistry_;
            mapper_.Map("ConcordIntegrationExample.SampleForm, ConcordIntegrationExample", "SampleFormCreator", Converter);
        }



        public void Initialize()
        {
            chromeRegistry.RegisterWidgetFactory("ButtonSampleFormCreator");
            chromeManager.AddWidget("ButtonSampleFormCreator", new InitialShowWindowButtonParameters()
                {
                    WindowFactoryID = "SampleFormCreator",
                    Text = "Open"
                }, chromeManager[ChromeArea.Ribbon]["open"]["Tools"]);
            chromeRegistry.RegisterWindowFactory("SampleFormCreator", InitHandler, DehydrateHandler);
        }

        private XElement Converter(XElement xElement_)
        {
            return new XElement("State", xElement_);
        }

        private XDocument DehydrateHandler(IWindowViewContainer windowViewContainer_)
        {
            WindowsFormsHost host = windowViewContainer_.Content as WindowsFormsHost;
            if (host != null)
            {
                var persistable = host.Child as IPersistentWindow;
                if (persistable != null)
                {
                    bool cancel = false;
                    XmlDocument data = new XmlDocument();
                   var root = data.CreateElement("State");
                    data.AppendChild(root);
                    persistable.SaveState(root, ref cancel);
                    return !cancel ? XDocument.Parse(root.OuterXml) : null;
                }

            }
            return null;
        }

        private bool InitHandler(IWindowViewContainer emptyViewContainer_, XDocument state_)
        {
            var form = new SampleForm();
            form.TopLevel = false;
            form.FormBorderStyle = FormBorderStyle.None;
            var host = new WindowsFormsHost();
            host.Child = form;
            emptyViewContainer_.Content = host;
            bool cancel = false;
            if (state_ != null)
            {
                form.LoadState(new XmlDocument().ReadNode(state_.CreateReader()), ref cancel);
            }
            return true;
        }
    }
}
