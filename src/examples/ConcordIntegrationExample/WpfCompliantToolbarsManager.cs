﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Windows.Forms;
using Infragistics.Win.UltraWinToolbars;

namespace ConcordIntegrationExample
{
    /// <summary>
    /// This is an extension of the UltraToolbarsManager that allows the user to avoid ceratin issues related to hosting the control
    /// in WPF. In particular, this implementation solves the following issues:
    /// 
    /// * disappearing context menu
    /// * hotkeys not working
    /// 
    /// This implementation is based on the code provided by Infragistics:
    /// http://www.infragistics.com/community/cfs-file.ashx/__key/CommunityServer.Components.PostAttachments/00.00.31.03.31/WpfApplication11.zip
    /// 
    /// The IG support case for the above issues is CAS-131698-Q5G6X0.
    /// </summary>
    public class WpfCompliantToolbarsManager : UltraToolbarsManager
    {
        private bool isFormActive;
        private Timer timer;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (this.timer != null)
            {
                this.timer.Enabled = false;
                this.timer.Dispose();
                this.timer = null;
            }
        }

        protected override void OnEndInit()
        {
            base.OnEndInit();

            this.timer = new Timer();
            this.timer.Interval = 100;
            this.timer.Tick += new EventHandler(timer_Tick);
            this.timer.Enabled = true;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            Control dockWithinContainer = this.DockWithinContainer;

            if (dockWithinContainer == null)
                return;

            bool isFormNowActive = IsControlOnActiveForm(dockWithinContainer);

            if (isFormNowActive != this.isFormActive)
            {
                this.isFormActive = isFormNowActive;

                if (this.isFormActive)
                    this.OnFormActivated();
                else
                    this.OnFormDeactivate();
            }
        }

        protected override bool ActivateForm()
        {
            bool succeeded = base.ActivateForm();

            if (succeeded)
                return true;

            Control dockWithinContainer = this.DockWithinContainer;

            if (dockWithinContainer == null || dockWithinContainer.IsHandleCreated == false)
                return false;

            IntPtr parentHwnd = NativeWindowMethods.FindTopLevelWindow(dockWithinContainer.Handle);

            if (parentHwnd == IntPtr.Zero)
                return false;

            NativeWindowMethods.SetActiveWindow(parentHwnd);
            return true;
        }

        protected override Control ActiveControlOfActiveForm
        {
            get
            {
                Control control = base.ActiveControlOfActiveForm;

                if (control != null)
                    return control;

                Console.WriteLine(Control.FromChildHandle(NativeWindowMethods.GetFocus()));
                return Control.FromChildHandle(NativeWindowMethods.GetFocus());
            }
        }

        protected override FormWindowState FormWindowState
        {
            get
            {
                FormWindowState windowState = base.FormWindowState;

                if (windowState != FormWindowState.Normal)
                    return windowState;

                // TODO: Get the window state and return it
                return windowState;
            }
        }

        protected override bool IsControlOnActiveForm(Control control)
        {
            bool isOnActiveForm = base.IsControlOnActiveForm(control);

            if (isOnActiveForm)
                return true;

            IntPtr activeForm = NativeWindowMethods.GetForegroundWindow();

            IntPtr controlHandle = control.Handle;

            while (controlHandle != IntPtr.Zero)
            {
                if (controlHandle == activeForm)
                    return true;

                controlHandle = NativeWindowMethods.GetParent(controlHandle);
            }

            return false;
        }

        protected override bool IsFormActive
        {
            get
            {
                bool isFormActive = base.IsFormActive;

                if (isFormActive)
                    return true;

                Control dockWithinContainer = this.DockWithinContainer;

                if (dockWithinContainer == null || dockWithinContainer.IsHandleCreated == false)
                    return false;

                IntPtr activeForm = NativeWindowMethods.GetForegroundWindow();
                IntPtr form = NativeWindowMethods.FindTopLevelWindow(dockWithinContainer.Handle);

                while (activeForm != IntPtr.Zero)
                {
                    if (activeForm == form)
                        return true;

                    activeForm = NativeWindowMethods.GetWindowLong(activeForm, NativeWindowMethods.GWL_HWNDPARENT);
                }

                return false;
            }
        }

        protected override void OnFloatingToolbarWindowShown(FloatingToolbarWindowBase floatingToolbarWindow)
        {
            base.OnFloatingToolbarWindowShown(floatingToolbarWindow);

            if (floatingToolbarWindow.Owner != null)
                return;

            Control dockWithinContainer = this.DockWithinContainer;

            if (dockWithinContainer != null && dockWithinContainer.IsHandleCreated)
            {
                try
                {
                    IntPtr ownerHandle = NativeWindowMethods.FindTopLevelWindow(dockWithinContainer.Handle);

                    if (ownerHandle != IntPtr.Zero)
                    {
                        NativeWindowMethods.SetWindowLong(floatingToolbarWindow.Handle, NativeWindowMethods.GWL_HWNDPARENT, ownerHandle);
                        // TODO: If the owner is TopMost, make the floating toolbar window TopMost
                    }
                }
                catch (SecurityException) { }
            }
        }
    }

    [SuppressUnmanagedCodeSecurity]
    internal class NativeWindowMethods
    {
        internal const int GWL_HWNDPARENT = -8;

        [DllImport("user32")]
        internal static extern IntPtr GetFocus();

        [DllImport("user32")]
        internal static extern IntPtr GetForegroundWindow();

        [DllImport("user32")]
        internal static extern IntPtr GetParent(IntPtr childHwnd);

        [DllImport("user32")]
        internal static extern IntPtr GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32")]
        internal static extern IntPtr SetActiveWindow(IntPtr hWnd);

        [DllImport("user32")]
        internal static extern IntPtr SetWindowLong(IntPtr hWnd, int nIndex, IntPtr newLong);

        internal static IntPtr FindTopLevelWindow(IntPtr childWindow)
        {
            IntPtr control = childWindow;

            while (true)
            {
                IntPtr nextControl = GetParent(control);

                if (nextControl == IntPtr.Zero)
                    break;

                control = nextControl;
            }

            return control;
        }
    }
}
