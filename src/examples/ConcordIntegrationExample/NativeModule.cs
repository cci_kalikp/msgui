﻿using System;
using System.IO;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Linq;
using MSDesktop.Omnibox.Grammar;
using MSDesktop.Omnibox.Interfaces;
using MSDesktop.Omnibox.Message;
using MSDesktop.Omnibox.UI;
using MSDesktop.Omnibox.UI.Grammar;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace ConcordIntegrationExample
{
    public class NativeModule : IModule
    {
        private readonly IChromeRegistry _registry;
        private readonly IChromeManager _manager;
        private readonly IMSDesktopEnvironment _environment;
        private readonly IOmniboxRegistry omniboxRegistry;
        private readonly ICommunicator communicator;

        public NativeModule(IChromeRegistry registry_, 
            IChromeManager manager_, 
            IMSDesktopEnvironment environment_, 
            IOmniboxRegistry omniboxRegistry_,
            ICommunicator communicator)
        {
            _registry = registry_;
            _manager = manager_;
            _environment = environment_;
            omniboxRegistry = omniboxRegistry_;
            this.communicator = communicator;
        }

        public void Initialize()
        {
            _registry.RegisterWidgetFactory("label", (container_, state_) =>
                {
                    container_.Content = new TextBox
                        {
                            Text = _environment.Environment + ", " + _environment.User + ", " + _environment.Region
                        };
                    return true;
                });
            _manager.Ribbon["open"]["Env"].AddWidget("label", new InitialWidgetParameters());
            var omniboxWidget = _manager.Ribbon["open"]["Omnibox"].AddWidget(OmniboxModule.WidgetFactoryName, new InitialWidgetParameters());
            _manager.Ribbon.QAT.AddWidget(omniboxWidget, 0);

            var document = new XmlDocument();
            document.Load("TestGrammar.xml");
            var grammar = XMLGrammar.BuildXMLGrammar(document);
            omniboxRegistry.RegisterGrammar(grammar);

            communicator.GetSubscriber<OmniboxMessage>().GetObservable().Subscribe(m => TaskDialog.ShowMessage("Running Omnibox command from module: " + m.ModuleName));
        }
    }
}