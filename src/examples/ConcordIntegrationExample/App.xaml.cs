﻿using System.Windows;
using MSDesktop.Environment;
using MSDesktop.Omnibox.UI;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.SmartApi;
using MorganStanley.MSDotNet.MSGui.Themes;

namespace ConcordIntegrationExample
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var boot = new Framework();  
            boot.EnableSmartApi();            
            boot.AddSimpleTheme(); 
            boot.EnableEnvironment(EnvironmentExtensions.EnvironProviderId.ConcordProfile, EnvironmentExtensions.RegionProviderId.ConcordProfile, EnvironmentExtensions.UsernameProviderId.ConcordProfile);
            boot.EnableConcord();
            boot.EnableAutosizeConcordForms();
            //if use the AppletMapModule, we don't need to call EnableConcord, but all Concord specific configuration would be skipped in this case
            //boot.AddModule<AppletMapModule>();
            boot.SetViewHeaderHeight(30);
            boot.AcquireFocusAfterStartup();
            boot.EnableAggregatedWindowGroupTitle(", ");
            boot.ShowSeparateHeaderItemsInFloatingWindow();
            boot.PreventDoubleWindowCreation();
            boot.EnableSequencialViewLoad(); 
            boot.EnableLayoutLoadProgressTracking();  
            boot.AddModule<OmniboxModule>();
            boot.AddModule<NativeModule>();
            boot.Start();
        }
    }
}
