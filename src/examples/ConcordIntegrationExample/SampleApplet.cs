﻿using System;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Media;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSNet;
using Button = System.Windows.Controls.Button;

namespace ConcordIntegrationExample
{
    public class SampleApplet : ConcordAppletBase, IWindowFactory
    {
        private int counter = 0;
        private IConcordApplication application;
        public SampleApplet(IMSNetLoop uiLoop_)
            : base(uiLoop_)
        {
        }

        public override void OnConnect(IConcordApplication application_)
        {
            base.OnConnect(application_);
            this.application = application_;
            var bar = application_.CommandBars.AddCommandBar("open", CommandBarType.Normal);
            var control = application_.CommandBars.AddNamedCommand("openWindow", "Open", null, CommandBarControlType.Button,
                (sender, e) => Task.Factory.StartNew(() => CreateWindow(application_)), _uiLoop);

            bar.AddCommand(control);
        }

        public override string Title
        {
            get
            {
                return "SampleApplet";
            }
        }
         
        private void CreateWindow(IConcordApplication application_)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                var form = GetForm(null, null);
                application_.WindowManager.CreateWindow("someWindow", form);
            }));
        }


        public System.Windows.Forms.Form GetForm(string type_, System.Xml.XmlNode state_)
        {
            var form = new SampleForm();
            var initializer = new ConcordExtension.WindowViewContainerInitializer();
            EventHandler handler = null;
            initializer.WindowViewContainerCreated += handler = (sender_, args_) =>
            { 
                initializer.WindowViewContainer.Title = form.Text + ++counter;
                initializer.WindowViewContainer.FloatingWindowMinimumWidth = 250;
                initializer.WindowViewContainer.FloatingWindowMinimumHeight = 50;
                initializer.WindowViewContainer.FloatingWindowMaximumHeight = 500;
                initializer.WindowViewContainer.FloatingWindowMaximumWidth = 500;
                initializer.WindowViewContainer.HeaderItems.Add(new Button()
                {
                    Content = "Test Button" + counter
                });
                var root = new DockPanel() {LastChildFill = true};
                var control = new WinFormQATControl();
                var host = new WindowsFormsHost();  
                host.Child = control;
                host.Margin = new System.Windows.Thickness(1.0, 0.0, 1.0, 0.0);
                host.Height = control.Height + 2;
                host.Width = control.Width + 2;
                host.Background = System.Windows.Media.Brushes.Transparent;
                root.Children.Add(host);
                initializer.WindowViewContainer.HeaderItems.Add(root);
                initializer.WindowViewContainer.HeaderStructure = HeaderStructure.WindowsClassic;
                initializer.WindowViewContainerCreated -= handler;
            };

            form.SetWindowViewContainerInitializer(initializer);
            return form;
        }
         
    }
}
