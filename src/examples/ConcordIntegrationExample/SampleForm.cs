﻿using System;
using System.Threading;
using System.Threading.Tasks; 
using System.Windows.Forms;
using System.Windows.Media; 
using System.Xml;
using Infragistics.Win.UltraWinToolbars;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MessageBox = System.Windows.Forms.MessageBox;

namespace ConcordIntegrationExample
{
    [WindowFactory("SampleApplet")]
    public partial class SampleForm : Form, IFeedbackWindow, IPersistentWindow
    {
        public SampleForm()
        {
            InitializeComponent(); 

            var manager = new WpfCompliantToolbarsManager {DockWithinContainer = this};

            var contextMenu = new PopupMenuTool("context");
            manager.Tools.Add(contextMenu);
            manager.SetContextMenuUltra(this, "context");

            var findButton = new ButtonTool("find");
            findButton.SharedProps.Caption = @"Find";
            findButton.SharedProps.Shortcut = Shortcut.CtrlF;
            findButton.SharedProps.Visible = true;
            manager.Tools.Add(findButton);
            contextMenu.Tools.AddTool("find");

            manager.ToolClick += (sender, args) => MessageBox.Show(@"Find!");
            button1.Click += (sender, args) =>
                {
                    var vc = this.GetWindowViewContainer();
                    vc.AccentColour = Colors.DarkOrange; 
                };
            Task.Factory.StartNew(new Action(() =>
            {
                Thread.Sleep(1000);
                var copy = FullyLoaded;
                if (copy != null)
                {
                    copy(this, EventArgs.Empty);
                }
            })); 
        }
         
        public void LoadState(System.Xml.XmlNode state_, ref bool cancel_)
        {
            foreach (XmlElement node in state_.ChildNodes)
            {
                textBox1.Text += node.InnerText + Environment.NewLine;
            }
        }

        public void SaveState(System.Xml.XmlNode state_, ref bool cancel_)
        {
            string[] texts = textBox1.Text.Split(new string[] { Environment.NewLine },
                                                 StringSplitOptions.RemoveEmptyEntries);
            foreach (var text in texts)
            {
                var element = state_.OwnerDocument.CreateElement("Line");
                element.InnerText = text;
                state_.AppendChild(element);
            }
        }

 
 
        public event System.EventHandler<EventArgs> FullyLoaded; 

        public bool IsLayoutLoadActive { get; set; }
    }
}
