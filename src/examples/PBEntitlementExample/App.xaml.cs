﻿//cw PB_Entitlement :10
/**##Description*/
/**PBEntitlementService inherits from the OnlineEntitlementService, which support query resource entitlement through PBE web service. It is used as all other [entitlement services](http://codewiki.webfarm.ms.com/msdotnet/#!/DNParts/Entitlements/QuickStart/) msdotnet supports */

/**##Assembly*/
/**
 * You need to add a reference to the MSDesktop.Entitlement and MSDesktop.PrimeBrokerage assembly.
 */

using System.Windows;
using MSDesktop.ComponentEntitlements;
using MSDesktop.PrimeBrokerage; 
using MorganStanley.MSDotNet.MSGui.Impl; 
using MorganStanley.MSDotNet.MSGui.Themes; 
using MSDesktop.ModuleEntitlements; 

namespace PBEntitlementExample
{
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var boot = new Framework();
            boot.AddSimpleTheme();

            //cw PB_Entitlement :30
            /**##Construct Entitlement Service*/
            /**Create an instance of PBEntitlementService by specifying the environment type, the domain name(application name), the firm id or use tag (only one is needed) */
            /** [Cache](http://codewiki.webfarm.ms.com/msdotnet/#!/DNParts/Entitlements/How_to_use_resilient_cache/) can be enabled to improve performance, and the default request timeout can be overridden depending on the network performance */
            //{
            var service = new PBEntitlementService(PBEnvironment.Uat, "CANS", null, "579008");
            service.RequestTimeout = 12000; //default is also 12 sec
            service.EnableCache = true;
            service.CacheTimeout = 300000; //cache timeout in 5 min
            //}
            
            /**##Query Entitlement Service*/
            /**There are 2 ways to query it */
            /**One is to define a strong typed resource as following, which would return list of such strong typed instances */
            //cw PB_Entitlement :45
            //{
            var accounts = service.GetPermittedResources<AccountResource>("view");
            AccountResource account = new AccountResource() 
                                        {
                                            Name = "098580MS9"
                                        };
            bool allowed = service.IsUserAllowed("view", account);
            //}

            //cw PB_Entitlement :50
            /**The other is to provide the resource type explicitly and return a list of IEntitlementResource */
            //{
            var accounts2 = service.GetPermittedResources("view", "Account");
            foreach (var account2 in accounts2)
            {
                allowed = service.IsUserAllowed("view", account2);
                var actions = service.GetPermittedActions(account2);
            }
             //} 

            /**##Work with other MSDesktop features*/
            /**This entitlement service can be used for [entitling module][cw_ref Module_Entitlement], [entitling component][cw_ref Component_Entitlement] and [entitling ViewModelAPI] [cw_ref ViewModelAPI_Entitlements] if resources are define appropriately*/
            //{
            boot.EnableModuleEntitlements(service);
            boot.EnableComponentEntitlements(service);
            //}
            boot.Start();
        }
    }
}
