﻿//cw PB_Entitlement :20
/**##Namespaces*/
//{
using MSDesktop.PrimeBrokerage;
using MorganStanley.Desktop.Entitlement;
//}

namespace PBEntitlementExample
{
    //cw PB_Entitlement :40
    //{
    //The resource type should be the same as the value defined in PBE service
    [EntitlementResource(ResourceType = "Account")]
    public class AccountResource
    { 
        public AccountResource()
        {
            //default string properties to empty string to avoid exception when resilient cache is used
            Client = FSAttribute = TradingName = Description = string.Empty;
        }

        //map each property to the resource attribute, if the property name is different from the attribute name returned from PBE service, override it in EntitlementResourcePropertyAttribute
        [EntitlementResourceProperty(PBEntitlementService.ResourceName)]
        public string Name { get; set; }
         
        [EntitlementResourceProperty]
        public string Client { get; set; }

        [EntitlementResourceProperty("FS Attribute")]
        public string FSAttribute { get; set; }

        [EntitlementResourceProperty]
        public string TradingName { get; set; }

        [EntitlementResourceProperty(PBEntitlementService.ResourceDescription)]
        public string Description { get; set; }

    }
    //}
}
