﻿using System;
using System.Diagnostics;
using System.Management;
using System.Reflection;
using System.Threading;
using System.Windows;
using MSDesktop.Wormhole;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace JavaIntegrationExample
{
    class JavaModule : JavaModuleBase
    {
        public JavaModule(IChromeRegistry chromeRegistry, IChromeManager chromeManager, IApplication application, IUnityContainer container) : base(chromeRegistry, chromeManager, application, container)
        {
        }

        protected override int PortNumber
        {
            get
            {
                return 10090;
            }
        }

        protected override string Path
        {
            get
            {
                var path = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                return System.IO.Path.Combine(path,"bin\\run-prod.cmd");
            }
        }

        protected override int TimeoutInSeconds
        {
            get
            {
                return 30;
            }
        }

        protected override bool EnableKrakenCommunication
        {
            get
            {
                return true;
            }
        }
    }
}
