﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.MessageRouting.Message;

namespace JavaIntegrationExample
{
    [Serializable]
    class FooMessage: IProvideMessageInfo
    {
        public int SomeNumber { get; set; }

        public string GetInfo()
        {
            return "Foo message";
        }
    }
}
