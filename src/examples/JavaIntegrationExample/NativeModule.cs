﻿using System;
using MSDesktop.MessageRouting;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace JavaIntegrationExample
{
    class NativeModule : IModule
    {
        private readonly IChromeRegistry registry;
        private readonly IChromeManager manager;
        private readonly IRoutingCommunicator communicator;

        public NativeModule(IChromeRegistry registry, IChromeManager manager, IRoutingCommunicator communicator)
        {
            this.registry = registry;
            this.manager = manager;
            this.communicator = communicator;
        }

        public void Initialize()
        {
            communicator.GetSubscriber<TestMessage>().GetObservable().Subscribe(ProcessMessage);
            var publisher = communicator.GetPublisher<FooMessage>();
            int number = 0;
            manager.Ribbon["NativeTab"]["NativeGroup"].AddWidget(new InitialButtonParameters
                {
                    Text = "Send message",
                    Click = (sender, args) => publisher.Publish(new FooMessage { SomeNumber = number++ })
                });
        }

        private void ProcessMessage(TestMessage testMessage)
        {
            TaskDialog.ShowMessage(testMessage.Content, "Received message from Java");
        }
    }
}
