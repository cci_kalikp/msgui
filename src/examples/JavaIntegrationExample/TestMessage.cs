﻿using System;
using MSDesktop.MessageRouting.Message;

namespace JavaIntegrationExample
{
    [Serializable]
    class TestMessage : IProvideMessageInfo
    {
        public String Content { get; set; }

        public string GetInfo()
        {
            return "Test message";
        }
    }
}
