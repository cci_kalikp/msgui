@echo off
set CLASSPATH=
call \\ms\dist\winaurora\PROJ\compat\incr\bin\useafs.cmd


call module load msjava/oraclejdk/1.7.0_25



call module load webinfra/smsdk/6.0-prod
call module load kerberos
call module load msjava/tools/2014.04.22

set RUNTIME_CLASSPATH=%~dp0\..\lib\msdesktop_wormhole_client.jar;%~dp0\..\lib\msdesktop_wormhole_client_examples.jar;
set RUNTIME_CLASSPATH=%RUNTIME_CLASSPATH%;%~dp0\..\lib\patching.jar
set RUNTIME_CLASSPATH="%RUNTIME_CLASSPATH%"

set JVM_ARGS= -Djava.util.logging.config.class=msjava.base.slf4j.jul.LogConfig

set MAIN_CLASS=com.ms.msdotnet.msdesktop.examples.module.JavaModuleExample

set MAIN_CLASS_ARGS=

set MSDE_IVY_ENDORSED_DIRS=

set MSDE_JNI_PATH=///ms/dist/msjava/PROJ/tools_jni/4.1/exec/lib/;///ms/dist/webinfra/PROJ/smsdk/6.0-prod/exec/lib/;

set MSDE_LAUNCH_WRAPPER=

:: Run the program
echo %MSDE_LAUNCH_WRAPPER% java %JVM_ARGS% -Djava.endorsed.dirs=%MSDE_IVY_ENDORSED_DIRS% -Djava.library.path=%MSDE_JNI_PATH% -cp %RUNTIME_CLASSPATH% %MAIN_CLASS% %MAIN_CLASS_ARGS%
%MSDE_LAUNCH_WRAPPER% java %JVM_ARGS% -Djava.endorsed.dirs=%MSDE_IVY_ENDORSED_DIRS% -Djava.library.path=%MSDE_JNI_PATH% -cp %RUNTIME_CLASSPATH% %MAIN_CLASS% %MAIN_CLASS_ARGS%
