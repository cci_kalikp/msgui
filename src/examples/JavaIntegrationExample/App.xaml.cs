﻿using System.Windows;
using System.Windows.Controls;
using MSDesktop.Environment;
using MSDesktop.Wormhole;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.SmartApi;
using MorganStanley.MSDotNet.MSGui.Themes;


namespace JavaIntegrationExample
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var boot = new Framework();
            boot.EnableSmartApi();
            boot.AddSimpleTheme();
            boot.AddModule<JavaModule>();
            boot.AddModule<NativeModule>();
            boot.AddModule<NotepadModule>();
            boot.EnableContentHostMode();
            boot.SetTabGroupTabStripPlacement(Dock.Top);
            boot.AcquireFocusAfterStartup();
            boot.EnableCompactTabGroups(true);
            boot.EnableWormhole();
            boot.HideOrLockShellElements(HideLockUIGranularity.HideEnvironmentLabellingForDockedWindows);
            boot.EnableEnvironment(EnvironmentExtensions.EnvironProviderId.CommandLine, EnvironmentExtensions.RegionProviderId.None, EnvironmentExtensions.UsernameProviderId.None);
            boot.EnableMessageRoutingCommunication();
            boot.EnableMessageRoutingDefaultGui();
            boot.EnableMessageRoutingHistory();
            boot.EnableMessageRoutingHistoryGui();
            boot.EnableMessageRoutingMonitor();
            boot.Start();
        }
    }
}