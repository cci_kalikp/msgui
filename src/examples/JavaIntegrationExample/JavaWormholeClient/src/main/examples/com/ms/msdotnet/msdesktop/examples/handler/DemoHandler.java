package com.ms.msdotnet.msdesktop.examples.handler;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingUtilities;

import org.w3c.dom.Node;

import com.ms.msdotnet.msdesktop.examples.DemoWindow;
import com.ms.msdotnet.msdesktop.wormhole.MSDesktopShellHandler;
import com.ms.msdotnet.msdesktop.wormhole.WormholeClient;
import com.ms.msdotnet.msdesktop.wormhole.environment.MSDesktopEnvironment;

public class DemoHandler implements MSDesktopShellHandler {

    private MSDesktopEnvironment environment;
    private Map<String, DemoWindow> windows;
    private WormholeClient client;
        
    public DemoHandler() {
        windows = new HashMap<String, DemoWindow>();
    }
        
    @Override
    public void setClient(WormholeClient client) {
        this.client = client;
    }
    
    @Override
    public void createWindow(final String windowId, final String factoryId, final Node state) {
        if (windows.containsKey(windowId)) return;
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                DemoWindow windowInstance = new DemoWindow();
                windowInstance.setTitle(windowId);
                windowInstance.addWindowListener(new WindowAdapter() {
                    public void windowOpened(WindowEvent e) {
                        client.sendCaptureWindow(windowId, "Demo Java window");
                    }
                });
                windowInstance.setVisible(true);
                if (state != null) {
                    windowInstance.deserialize(state);
                }
                if (environment != null) {
                    windowInstance.setEnvironmentText(environment);
                }
                windows.put(windowId, windowInstance);
            }
        });  
    }

    @Override
    public void closeWindow(String windowId) {
        if (!windows.containsKey(windowId)) return;
        DemoWindow windowInstance = windows.get(windowId); 
        windowInstance.dispose();
        windowInstance = null;

    }

    @Override
    public void getWindowState(String windowId, String requestId) {
        if (!windows.containsKey(windowId)) return;
        DemoWindow windowInstance = windows.get(windowId); 
        client.sendWindowState(windowId, requestId, windowInstance.serialize());
    }

    @Override
    public void setEnvironment(MSDesktopEnvironment msDesktopEnvironmentImpl) {
        this.environment = msDesktopEnvironmentImpl;
    }

    @Override
    public void initialize() { 
        /* ignore */
    }

}
