package com.ms.msdotnet.msdesktop.examples;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class Utils {
    public static Document getDocumentFromString(String text) {
        try {
            DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
            DocumentBuilder db1 = dbf1.newDocumentBuilder(); 
            return db1.parse(new ByteArrayInputStream(text.getBytes()));
        } catch (SAXException e) {
            throw new RuntimeException( e );
        } catch (IOException e) {
            throw new RuntimeException( e );
        } catch (ParserConfigurationException e) {
            throw new RuntimeException( e );
        }
    }
}
