package com.ms.msdotnet.msdesktop.examples.handler;

import com.ms.msdotnet.msdesktop.wormhole.MSDesktopShellHandler;
import com.ms.msdotnet.msdesktop.wormhole.WormholeClient;

public class JavaHandlerExample {

    /**
     * @param args
     */
    public static void main(String[] args) {
        MSDesktopShellHandler handler = new DemoHandler();
        int portNumber = 10090;
		String portNumberStr = System.getenv(WormholeClient.MSDesktopPortNumberVariable);
		if(portNumberStr != null && !portNumberStr.isEmpty()) {
		    portNumber = Integer.parseInt(portNumberStr);
			System.out.println("PortNumber:" + portNumberStr); 
		} 
        WormholeClient client = new WormholeClient(portNumber, handler); 
        client.Start();
    }

}
