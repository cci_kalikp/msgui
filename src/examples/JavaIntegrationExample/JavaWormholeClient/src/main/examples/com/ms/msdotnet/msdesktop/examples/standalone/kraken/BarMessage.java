package com.ms.msdotnet.msdesktop.examples.standalone.kraken;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ms.msdotnet.msdesktop.interfaces.kraken.Broadcast;
import com.ms.msdotnet.msdesktop.interfaces.kraken.MapsToDotNetType;

@JsonIgnoreProperties({"Target"})
@MapsToDotNetType(type="MessageRoutingExampleLib.Messages.Bar, MessageRoutingExampleLib")
@Broadcast
public class BarMessage {
    private String message;
    private String action;

    public BarMessage() {}
    
    public BarMessage(String message, String action) {
        this.message = message;
        this.action = action;
    }
    
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
