package com.ms.msdotnet.msdesktop.examples.module;

import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingUtilities;

import org.w3c.dom.Node;

import com.ms.msdotnet.msdesktop.examples.DemoWindow;
import com.ms.msdotnet.msdesktop.interfaces.kraken.MessageListener;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutedSubscriber;
import com.ms.msdotnet.msdesktop.wormhole.MSDesktopHostedModule;
import com.ms.msdotnet.msdesktop.wormhole.WindowFactory;
import com.ms.msdotnet.msdesktop.wormhole.chrome.InitialLocation;
import com.ms.msdotnet.msdesktop.wormhole.chrome.InitialShowWindowButtonParameters;
import com.ms.msdotnet.msdesktop.wormhole.chrome.InitialWindowParameters;

public class DemoModule extends MSDesktopHostedModule {

    private Map<String, DemoWindow> windows;

    public DemoModule() {
        windows = new HashMap<String, DemoWindow>();
    }
    
    @Override
    public void initialize() {
        registerWindowFactory("redWindowFactory", new WindowFactory() {
            @Override
            public boolean createWindow(final String windowId, final Node state) {
                DemoWindow window = new DemoWindow();
                window.setTextBackground(Color.red);
                if (environment != null) {
                    window.setEnvironmentText(environment);
                }
                window.enablePublishing(communicator.getPublisher(TestMessage.class));
                return createDemoWindow(windowId, state, window);
            }
            @Override
            public String dehydrateWindow(String windowId) {
                DemoWindow windowInstance = windows.get(windowId);
				if (windowInstance == null) return null;
                return windowInstance.serialize();
            }            
        });
        
        registerWindowFactory("blueWindowFactory", new WindowFactory() {
            @Override
            public boolean createWindow(final String windowId, final Node state) {
                final DemoWindow window = new DemoWindow();
                window.setTextBackground(Color.blue);
                RoutedSubscriber<FooMessage> subscriber = communicator.getSubscriber(FooMessage.class);
                subscriber.subscribe(new MessageListener<FooMessage>() {                    
                    @Override
                    public void receive(FooMessage message) {
                        window.setText("Received message with number: " + message.getSomeNumber());
                    }
                });
                return createDemoWindow(windowId, state, window);
            }
            @Override
            public String dehydrateWindow(String windowId) {
                DemoWindow windowInstance = windows.get(windowId);
				if (windowInstance == null) return null;
                return windowInstance.serialize();
            }            
        });
        
        InitialWindowParameters windowParams = new InitialWindowParameters();
        windowParams.setInitialLocation(InitialLocation.DockTabbed | InitialLocation.DockInActiveTab);
                
        InitialShowWindowButtonParameters params = new InitialShowWindowButtonParameters();
        params.setText("Show red");
        params.setWindowFactoryId("redWindowFactory");
        params.setImagePath("\\\\ms\\dev\\msdotnet\\msgui\\2014.04.02\\src\\examples\\ExampleFrameworkApp\\Resources\\icon.ico");
        //params.setInitialParameters(windowParams);
        placeWidget("JavaTab/JavaGroup", "RedJavaWindow", params);
        
        InitialShowWindowButtonParameters params2 = new InitialShowWindowButtonParameters();
        params2.setText("Show blue");
        params2.setWindowFactoryId("blueWindowFactory");
        params2.setInitialParameters(windowParams);
        placeWidget("JavaTab/JavaGroup", "BlueJavaWindow",params2);
    }

    @Override
    public void closeWindow(String windowId) {
        if (!windows.containsKey(windowId)) return;
        DemoWindow windowInstance = windows.get(windowId);
        windowInstance.dispose();
        windowInstance = null;
    }

    private boolean createDemoWindow(final String windowId, final Node state, final DemoWindow windowInstance) {
        if (windows.containsKey(windowId)) return false;
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                windowInstance.setTitle(windowId);
                windowInstance.addWindowListener(new WindowAdapter() {
                    public void windowOpened(WindowEvent e) {
                        client.sendCaptureWindow(windowId, "Demo Java window");
                    }
                });
                windowInstance.setVisible(true);
                if (state != null) {
                    windowInstance.deserialize(state);
                }
                windows.put(windowId, windowInstance);
            }
        });
        return true;
    }
}
