package com.ms.msdotnet.msdesktop.examples.standalone.kraken;

import com.ms.msdotnet.msdesktop.interfaces.kraken.MapsToDotNetType;

@MapsToDotNetType(type="MessageRoutingExampleLib.Messages.Foo, MessageRoutingExampleLib")
public class FooMessage {
    private String action;
    private String message;
    
    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}
