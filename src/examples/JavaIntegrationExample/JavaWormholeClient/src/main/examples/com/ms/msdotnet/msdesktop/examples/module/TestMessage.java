package com.ms.msdotnet.msdesktop.examples.module;

import com.ms.msdotnet.msdesktop.interfaces.kraken.MapsToDotNetType;

@MapsToDotNetType(type="JavaIntegrationExample.TestMessage, JavaIntegrationExample")
public class TestMessage {
    private String content;

    public TestMessage(String string) {
        content = string;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
