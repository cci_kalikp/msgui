package com.ms.msdotnet.msdesktop.examples.standalone.kraken;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.IMC.CPS.IMCMessages.CpsModuleMessage.Environment;

import com.ms.msdotnet.msdesktop.interfaces.kraken.DeliveryInfo;
import com.ms.msdotnet.msdesktop.interfaces.kraken.MessageListener;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutedPublisher;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutedSubscriber;
import com.ms.msdotnet.msdesktop.standalone.kraken.RoutingCommunicatorImpl;
import com.ms.msdotnet.msdesktop.standalone.m2m.TypeRegistry;

public class StandaloneExampleApp {    
    /**
     * @param args
     */
    public static void main(String[] args) {
        final TypeRegistry typeRegistry = new TypeRegistry();
        typeRegistry.registerDotNetType(FooMessage.class);
        typeRegistry.registerDotNetType(BarMessage.class);
        final ExecutorService pool = Executors.newFixedThreadPool(1);
        try (RoutingCommunicatorImpl communicator = new RoutingCommunicatorImpl("icompile1.croydon.ms.com:9995",
                "StandaloneExampleApp", Environment.Dev, typeRegistry)) {
            final RoutedPublisher<BarMessage> publisher = communicator.getPublisher(BarMessage.class);
            try (RoutedSubscriber<FooMessage> subscriber = communicator.getSubscriber(FooMessage.class)) {
                subscriber.subscribe(new MessageListener<FooMessage>() {

                    @Override
                    public void receive(FooMessage message) {
                        System.err.println("Received message Foo: " + message.getMessage());
                        pool.execute(new Runnable() {

                            @Override
                            public void run() {
                                try {
                                    DeliveryInfo info = publisher.publishAsync(new BarMessage("Hello", "BUY")).get();
                                    System.err.println("Message with id: " + info.getId() + " status: "
                                            + info.getStatus());
                                } catch (InterruptedException | ExecutionException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
                Thread.sleep(300000);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

}
