package com.ms.msdotnet.msdesktop.examples;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.peer.ComponentPeer;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;

import org.w3c.dom.Node;

import com.ms.msdotnet.msdesktop.examples.module.TestMessage;
import com.ms.msdotnet.msdesktop.interfaces.kraken.DeliveryInfo;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutedPublisher;
import com.ms.msdotnet.msdesktop.wormhole.environment.MSDesktopEnvironment;


public class DemoWindow extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private final JTextField textField;
    private JButton button;
    private final JPanel listPane;

    private final JPopupMenu popup = new JPopupMenu();
                
    public DemoWindow() {
        popup.add(new JMenuItem("I am a menu"));
        
        setTitle("Simple example");
        setSize(300, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setUndecorated(true);
                        
        listPane = new JPanel();
        listPane.setLayout(new BoxLayout(listPane, BoxLayout.PAGE_AXIS));
        
        textField = new JTextField(20);
        textField.setText("Hello from Java test!");
        textField.setForeground(Color.white);
        listPane.add(textField);
        
        textField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                popup.show(e.getComponent(), e.getX(), e.getY());
            }
        });
        
        add(listPane);
    }
        
    public String serialize() {
        return "<DemoWindowState>" + textField.getText() + "</DemoWindowState>";
    }
    
    public void deserialize(Node state) {
        String text = state.getTextContent();
        textField.setText(text);
    }
    
    public void setEnvironmentText(MSDesktopEnvironment env) {
        textField.setText(textField.getText() + " " + env.getEnvironment());
    }
    
    public void setText(String text) {
        textField.setText(text);
    }
    
    public void setTextBackground(Color color) {
        textField.setBackground(color);
    }

    public void enablePublishing(final RoutedPublisher<TestMessage> publisher) {
        final JTextField input = new JTextField();        
        button = new JButton("Publish");
        button.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                DeliveryInfo info = publisher.publish(new TestMessage(input.getText()));
                setText("Message published to " + info.getTarget().getHostname());
            }
        });
        listPane.add(input);
        listPane.add(button);
    }
}
