package com.ms.msdotnet.msdesktop.examples.module;

import com.ms.msdotnet.msdesktop.wormhole.MSDesktopShellHandler;
import com.ms.msdotnet.msdesktop.wormhole.WormholeClient;

public class JavaModuleExample {
    
    public static void main(String[] args) {
        MSDesktopShellHandler handler = new DemoModule();
		int portNumber = 10090;
		String portNumberStr = System.getenv(WormholeClient.MSDesktopPortNumberVariable);
		if(portNumberStr != null && !portNumberStr.isEmpty()) {
		    portNumber = Integer.parseInt(portNumberStr);
			System.out.println("PortNumber:" + portNumberStr); 
		} 
        WormholeClient client = new WormholeClient(portNumber, handler);
        client.Start();
    }
}