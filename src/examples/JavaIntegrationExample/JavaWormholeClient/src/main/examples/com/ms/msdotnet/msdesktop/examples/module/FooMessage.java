package com.ms.msdotnet.msdesktop.examples.module;

import com.ms.msdotnet.msdesktop.interfaces.kraken.MapsToDotNetType;

@MapsToDotNetType(type="JavaIntegrationExample.FooMessage, JavaIntegrationExample")
public class FooMessage {
    private int someNumber;

    public int getSomeNumber() {
        return someNumber;
    }

    public void setSomeNumber(int someNumber) {
        this.someNumber = someNumber;
    } 
}
