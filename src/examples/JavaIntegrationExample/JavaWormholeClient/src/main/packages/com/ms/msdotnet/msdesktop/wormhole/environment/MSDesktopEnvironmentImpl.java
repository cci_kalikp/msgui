package com.ms.msdotnet.msdesktop.wormhole.environment;

public class MSDesktopEnvironmentImpl implements MSDesktopEnvironment {

    private Environment environment;
    private Region region;
    private String user;
    
    @Override
    public Environment getEnvironment() {
        return environment;
    }

    @Override
    public Region getRegion() {
        return region;
    }

    @Override
    public String getUser() {
        return user;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public MSDesktopEnvironmentImpl(Environment environment, Region region, String user) {
        super();
        this.environment = environment;
        this.region = region;
        this.user = user;
    }
}
