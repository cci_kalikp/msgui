package com.ms.msdotnet.msdesktop.wormhole.kraken.impl;

import com.ms.msdotnet.msdesktop.interfaces.kraken.MessageListener;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutedSubscriber;
import com.ms.msdotnet.msdesktop.wormhole.WormholeClient;

public class RoutedSubscriberProxy<T> implements RoutedSubscriber<T> {

    private MessageListener<T> listener;
    
    public RoutedSubscriberProxy(String subscriberId, WormholeClient client, String type) { /* empty */}

    @Override
    public void subscribe(MessageListener<T> listener) {
        this.listener = listener;
    }

    @SuppressWarnings("unchecked")
    public void receive(Object message) {
        if (listener != null) {
            listener.receive((T) message);
        }
    }

    @Override
    public void close() throws Exception {
        // TODO Auto-generated method stub
        
    }

}
