package com.ms.msdotnet.msdesktop.interfaces.kraken;

public interface BypassRouting {
    Endpoint getTarget();
}
