package com.ms.msdotnet.msdesktop.standalone.kraken;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import com.ms.msdotnet.msdesktop.interfaces.kraken.DeliveryInfo;
import com.ms.msdotnet.msdesktop.interfaces.kraken.DeliveryStatus;
import com.ms.msdotnet.msdesktop.interfaces.kraken.DeliveryStrategy;
import com.ms.msdotnet.msdesktop.interfaces.kraken.DeliveryStrategyEnum;
import com.ms.msdotnet.msdesktop.interfaces.kraken.MessageListener;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutedSubscriber;
import com.ms.msdotnet.msdesktop.interfaces.m2m.ModulePublisher;
import com.ms.msdotnet.msdesktop.standalone.kraken.messages.Ack;
import com.ms.msdotnet.msdesktop.standalone.kraken.messages.RoutedMessage;

@SuppressWarnings("rawtypes")
public class AckTrackingPublisher {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AckTrackingPublisher.class);
    
    private final ModulePublisher<RoutedMessage> underlyingPublisher;
    private final ConcurrentHashMap<Integer, SettableFuture<DeliveryInfo>> results;
    private final AtomicInteger seq = new AtomicInteger(0);
    
    public AckTrackingPublisher(ModulePublisher<RoutedMessage> publisher, RoutedSubscriber<Ack> ackSubscriber) {
        underlyingPublisher = publisher;
        results = new ConcurrentHashMap<>();
        ackSubscriber.subscribe(new MessageListener<Ack>() {
            @Override
            public void receive(Ack message) {
                SettableFuture<DeliveryInfo> future = results.remove(message.getId());
                if (future != null) {
                    LOGGER.info("Received Ack for message with id: " + message.getId());
                    DeliveryInfo info = new DeliveryInfo();
                    info.setId(String.valueOf(message.getId()));
                    info.setTarget(message.getTarget());
                    if (message.getIsAcceptable()) {
                        info.setStatus(DeliveryStatus.Delivered);
                    } else {
                        info.setStatus(DeliveryStatus.Rejected);
                    }
                    future.set(info);
                }
            }
        });
    }
    
    public ListenableFuture<DeliveryInfo> publish(final RoutedMessage message) {
        message.setId(seq.incrementAndGet());
        DeliveryInfo info = new DeliveryInfo();
        info.setId(String.valueOf(message.getId()));
        info.setTarget(message.getTarget());
        
        boolean shouldWaitForAck = shouldWaitForAck(message);
        SettableFuture<DeliveryInfo> result = SettableFuture.create();
        
        if (shouldWaitForAck) {
            results.put(message.getId(), result);
        }
        else {
            info.setStatus(DeliveryStatus.Delivered);
            result.set(info);
        }
        
        LOGGER.info("Publishing routed message with id: " + message.getId() + ", will wait for Ack: " + shouldWaitForAck);
        underlyingPublisher.publish(message);        
        return result;
    }
    
    private static boolean shouldWaitForAck(RoutedMessage message) {
        DeliveryStrategy deliveryStrategy = message.getMessage().getClass().getAnnotation(DeliveryStrategy.class);
        return message.getTarget().getApplication() != null && 
               message.getTarget().getHostname() != null &&
               !(message.getMessage() instanceof Ack) &&
               !(deliveryStrategy != null && deliveryStrategy.deliveryStrategy() == DeliveryStrategyEnum.FireAndForget);
    }    
}
