package com.ms.msdotnet.msdesktop.wormhole.kraken.impl;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.ms.msdotnet.msdesktop.interfaces.kraken.DeliveryInfo;
import com.ms.msdotnet.msdesktop.interfaces.kraken.Endpoint;
import com.ms.msdotnet.msdesktop.wormhole.MSDesktopMessageProcessor;

public class KrakenDeliveryInfoProcessor implements MSDesktopMessageProcessor {

    private RoutingCommunicatorProxy communicator;

    public KrakenDeliveryInfoProcessor(RoutingCommunicatorProxy communicator) {
        this.communicator = communicator;
    }
    
    @Override
    public boolean processMessage(Document message) {
        Node krakenMessageNode = message.getElementsByTagName("KrakenDeliveryInfo").item(0);        
        if (krakenMessageNode != null) {
            String id = krakenMessageNode.getAttributes().getNamedItem("id").getTextContent();
            String hostname = krakenMessageNode.getAttributes().getNamedItem("hostname").getTextContent();
            String application = krakenMessageNode.getAttributes().getNamedItem("application").getTextContent();
            String username = krakenMessageNode.getAttributes().getNamedItem("username").getTextContent();
            String requestId = krakenMessageNode.getAttributes().getNamedItem("requestId").getTextContent();
            String publisherId = krakenMessageNode.getAttributes().getNamedItem("publisherId").getTextContent();
            
            DeliveryInfo result = new DeliveryInfo();
            Endpoint endpoint = new Endpoint();
            endpoint.setApplication(application);
            endpoint.setHostname(hostname);
            endpoint.setUsername(username);
            result.setTarget(endpoint);
            result.setId(id);
            
            communicator.receiveDeliveryInfo(result, publisherId, requestId);
            return true;
        }
        return false;
    }

}
