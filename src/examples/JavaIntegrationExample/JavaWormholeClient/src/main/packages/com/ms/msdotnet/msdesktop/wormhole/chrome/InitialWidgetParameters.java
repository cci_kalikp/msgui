package com.ms.msdotnet.msdesktop.wormhole.chrome;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"dotNetType"})
public abstract class InitialWidgetParameters {

    private String text;
        
    public abstract String getDotNetType();

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    
}
