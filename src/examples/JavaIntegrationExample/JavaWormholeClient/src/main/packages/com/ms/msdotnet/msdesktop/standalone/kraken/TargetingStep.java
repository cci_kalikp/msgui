package com.ms.msdotnet.msdesktop.standalone.kraken;

import com.ms.msdotnet.msdesktop.interfaces.kraken.Endpoint;

public interface TargetingStep {
    public Endpoint execute(EndpointSelectorChain chain);
}
