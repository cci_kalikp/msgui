package com.ms.msdotnet.msdesktop.standalone.kraken;

import java.util.Stack;

import com.ms.msdotnet.msdesktop.interfaces.kraken.Endpoint;

public class EndpointSelectorChain {
    private Stack<TargetingStep> chain;
    private Object message;
    
    public EndpointSelectorChain(Stack<TargetingStep> chain, Object message) {
        this.chain = chain;
        this.setMessage(message);
    }
    
    public Endpoint next() {
        TargetingStep step = chain.pop();
        if (step == null) return null;
        return step.execute(this);
    }

    public Object getMessage() {
        return message;
    }

    private void setMessage(Object message) {
        this.message = message;
    }
}
