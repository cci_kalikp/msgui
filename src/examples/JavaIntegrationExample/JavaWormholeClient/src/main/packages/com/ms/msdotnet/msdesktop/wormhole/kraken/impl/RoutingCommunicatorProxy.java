package com.ms.msdotnet.msdesktop.wormhole.kraken.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ms.msdotnet.msdesktop.interfaces.kraken.DeliveryInfo;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutedPublisher;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutedSubscriber;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutingCommunicator;
import com.ms.msdotnet.msdesktop.interfaces.kraken.MapsToDotNetType;
import com.ms.msdotnet.msdesktop.standalone.m2m.PascalCasePropertyNamingStrategy;
import com.ms.msdotnet.msdesktop.wormhole.WormholeClient;

public class RoutingCommunicatorProxy implements RoutingCommunicator {

    private final WormholeClient client;
    private final Map<String, RoutedSubscriberProxy<?>> subscribers;
    private final Map<String, RoutedPublisherProxy<?>> publishers;
    private final Map<String, Class<?>> reverseTypeMapping;
    private final ObjectMapper mapper;
    
    public RoutingCommunicatorProxy(WormholeClient client) {
        this.client = client;
        this.subscribers = new HashMap<String,RoutedSubscriberProxy<?>>();
        this.publishers = new HashMap<String,RoutedPublisherProxy<?>>();
        this.reverseTypeMapping = new HashMap<String, Class<?>>();
        this.mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(new PascalCasePropertyNamingStrategy());
        
        this.client.registerMessageProcessor(new KrakenForwardedMessageProcessor(this));
        this.client.registerMessageProcessor(new KrakenDeliveryInfoProcessor(this));
    }
    
    @Override
    public <T> RoutedPublisher<T> getPublisher(Class<T> type) {
        String publisherId = java.util.UUID.randomUUID().toString();
        if (!type.isAnnotationPresent(MapsToDotNetType.class)) {
            throw new RuntimeException("The message type must be annotated with MapsToDotNetType");
        }
        MapsToDotNetType annotation = (MapsToDotNetType)type.getAnnotation(MapsToDotNetType.class);
        client.sendCommand("<KrakenGetPublisher id=\"" + publisherId + "\" type=\"" + annotation.type() + "\" />");
        RoutedPublisherProxy<T> publisher = new RoutedPublisherProxy<T>(publisherId, client, annotation.type());
        publishers.put(publisherId, publisher);
        return publisher;
    }
    
    @Override
    public <T> RoutedSubscriber<T> getSubscriber(Class<T> type) {
        String subscriberId = java.util.UUID.randomUUID().toString();
        if (!type.isAnnotationPresent(MapsToDotNetType.class)) {
            throw new RuntimeException("The message type must be annotated with MapsToDotNetType");
        }
        MapsToDotNetType annotation = (MapsToDotNetType)type.getAnnotation(MapsToDotNetType.class);
        client.sendCommand("<KrakenGetSubscriber id=\"" + subscriberId + "\" type=\"" + annotation.type() + "\" />");
        RoutedSubscriberProxy<T> subscriber = new RoutedSubscriberProxy<T>(subscriberId, client, annotation.type());
        reverseTypeMapping.put(annotation.type(), type);
        subscribers.put(subscriberId, subscriber);
        return subscriber;
    }

    public void forwardSubscribedMessage(String subscriberId, String content, String type) {
        RoutedSubscriberProxy<?> subscriber = subscribers.get(subscriberId);
        Class<?> javaType = reverseTypeMapping.get(type);
        Object message = null;
        try {
            message = mapper.readValue(content, javaType);
        } catch (JsonParseException e) {
            throw new RuntimeException( e );
        } catch (JsonMappingException e) {
            throw new RuntimeException( e );
        } catch (IOException e) {
            throw new RuntimeException( e );
        }
        subscriber.receive(message);
    }

    public void receiveDeliveryInfo(DeliveryInfo result, String publisherId, String requestId) {
        publishers.get(publisherId).receiveDeliveryInfo(result, requestId);
    }

    @Override
    public void close() throws Exception {
        // TODO Auto-generated method stub        
    }

}
