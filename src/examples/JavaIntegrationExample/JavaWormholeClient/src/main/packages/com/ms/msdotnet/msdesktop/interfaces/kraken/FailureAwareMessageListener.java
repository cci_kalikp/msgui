package com.ms.msdotnet.msdesktop.interfaces.kraken;

public interface FailureAwareMessageListener<T> extends MessageListener<T> {
    void onFailure(Throwable e);
}
