package com.ms.msdotnet.msdesktop.standalone.m2m;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ms.msdotnet.msdesktop.interfaces.kraken.FailureAwareMessageListener;
import com.ms.msdotnet.msdesktop.interfaces.kraken.MessageListener;

public class LogOnFailureMessageListener<T> implements FailureAwareMessageListener<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogOnFailureMessageListener.class);
    
    private MessageListener<T> listener;
    
    public LogOnFailureMessageListener(MessageListener<T> listener) {
        this.listener = listener;
    }
    
    @Override
    public void receive(T message) {
        listener.receive(message);
    }

    @Override
    public void onFailure(Throwable e) {
        LOGGER.error("Exception while receiving message", e);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof LogOnFailureMessageListener)) return false;
        if (obj == this) return true;

        LogOnFailureMessageListener<?> other = (LogOnFailureMessageListener<?>) obj;
        return new EqualsBuilder()
            .append(listener, other.listener)
            .isEquals();
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(51, 67)
            .append(listener)
            .toHashCode();
    }
}
