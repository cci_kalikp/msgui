package com.ms.msdotnet.msdesktop.wormhole.processors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.ms.msdotnet.msdesktop.wormhole.MSDesktopMessageProcessor;
import com.ms.msdotnet.msdesktop.wormhole.MSDesktopShellHandler;
import com.ms.msdotnet.msdesktop.wormhole.environment.Environment;
import com.ms.msdotnet.msdesktop.wormhole.environment.MSDesktopEnvironmentImpl;
import com.ms.msdotnet.msdesktop.wormhole.environment.Region;

public class EnvironmentProcessor implements MSDesktopMessageProcessor {

private static Logger logger = LoggerFactory.getLogger(EnvironmentProcessor.class); 
    
    private MSDesktopShellHandler client;
    
    public EnvironmentProcessor(MSDesktopShellHandler client) {
        super();
        this.client = client;
    }
    
    @Override
    public boolean processMessage(Document message) {
        Node environmentNode = message.getElementsByTagName("Environment").item(0);
        if (environmentNode != null) {
            logger.info("Received GetEnvironment");
            if (environmentNode.getChildNodes().getLength() < 3) {
                client.setEnvironment(null);
				return true;
            }
            Environment env = Environment.valueOf(environmentNode.getChildNodes().item(0).getTextContent());
            Region region = Region.valueOf(environmentNode.getChildNodes().item(1).getTextContent());
            String username = environmentNode.getChildNodes().item(2).getTextContent();

            client.setEnvironment(new MSDesktopEnvironmentImpl(env, region, username));
            
            return true;
        }
        return false;
    }

}
