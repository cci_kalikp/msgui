package com.ms.msdotnet.msdesktop.standalone.m2m;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.ms.msdotnet.msdesktop.interfaces.kraken.MapsToDotNetType;

public class TypeRegistry {
    private final Map<String, Class<?>> registeredTypes;
    private final Map<Class<?>, String> registeredTypesReversed;
    private final ObjectMapper mapper;
    
    public TypeRegistry() {
        registeredTypes = new HashMap<String, Class<?>>();
        registeredTypesReversed = new HashMap<Class<?>, String>();
        mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(new PascalCasePropertyNamingStrategy());
    }
    
    public void registerDotNetType(Class<?> type) {
        if (!type.isAnnotationPresent(MapsToDotNetType.class)) {
            throw new RuntimeException("The message type must be annotated with MapsToDotNetType");
        }
        MapsToDotNetType annotation = (MapsToDotNetType)type.getAnnotation(MapsToDotNetType.class);
        String dotNetType = annotation.type();
        registeredTypes.put(dotNetType, type);
        registeredTypesReversed.put(type, dotNetType);
    }
    
    public String getDotNetType(Class<?> javaType) {
        return registeredTypesReversed.get(javaType);
    }
    
    public JavaType getJavaType(String type) {
      if (type.contains("`")) {
          final String pattern = "([a-zA-Z.]+)`([0-9]+)\\[(.+)\\]";
          final String typePattern = "\\[([a-zA-Z.]+), ([a-zA-Z.]+).*\\]";
          final Matcher matcher = Pattern.compile(pattern).matcher(type); 
          if (matcher.find()) {
            String mainTypeName = matcher.group(1);
            Class<?> mainType = registeredTypes.get(mainTypeName);
            
            List<Class<?>> argumentTypes = new LinkedList<Class<?>>();
            String part = matcher.group(3); // TODO handle multiple parameters
            
            final Matcher typeMatcher = Pattern.compile(typePattern).matcher(part);
            if (typeMatcher.find()) {
                final String argumentTypeName = typeMatcher.group(1) + ", " + typeMatcher.group(2);
                Class<?> argumentType = registeredTypes.get(argumentTypeName);
                if (argumentType == null) {
                    throw new RuntimeException("Message type not registered: " + argumentTypeName);
                }
                argumentTypes.add(argumentType);
            } else {
                throw new RuntimeException("Pattern matching failed for type name: " + part);
            }
                
            return mapper.getTypeFactory().constructParametricType(mainType, argumentTypes.toArray(new Class<?>[0]));
          }
          return TypeFactory.unknownType();
      } else {
          return mapper.getTypeFactory().constructType(registeredTypes.get(type));
      }
  }
}
