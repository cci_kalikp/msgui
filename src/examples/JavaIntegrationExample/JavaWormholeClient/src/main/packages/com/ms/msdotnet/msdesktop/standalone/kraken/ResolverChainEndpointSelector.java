package com.ms.msdotnet.msdesktop.standalone.kraken;

import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ms.msdotnet.msdesktop.interfaces.kraken.Broadcast;
import com.ms.msdotnet.msdesktop.interfaces.kraken.Endpoint;
import com.ms.msdotnet.msdesktop.interfaces.kraken.BypassRouting;

public class ResolverChainEndpointSelector {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResolverChainEndpointSelector.class);
    
    private Stack<TargetingStep> masterChain;
    private final Map<Integer, TargetingStep> targetingSteps;
    private final Endpoint localEndpoint;
    
    public ResolverChainEndpointSelector(Endpoint localEndpoint) {
        this.localEndpoint = localEndpoint;
        targetingSteps = new TreeMap<Integer, TargetingStep>();
        targetingSteps.put(0, new TargetingFailedStep());
        targetingSteps.put(3000, new CheckIfBypassRouting());
        targetingSteps.put(3001, new CheckIfBroadcastStep());
        rebuildMasterChain();
    }
    
    public Endpoint resolveTarget(Object message) {
        @SuppressWarnings("unchecked")
        EndpointSelectorChain chain = new EndpointSelectorChain((Stack<TargetingStep>) masterChain.clone(), message);
        return chain.next();
    }
    
    private void rebuildMasterChain() {
        masterChain = new Stack<TargetingStep>();
        for (Integer key : targetingSteps.keySet()) {
            masterChain.push(targetingSteps.get(key));
        }
    }
    
    private class TargetingFailedStep implements TargetingStep {
        @Override
        public Endpoint execute(EndpointSelectorChain chain) {
            LOGGER.error("Could not resolve target for message: " + chain.getMessage());
            return null;
        }        
    }
    
    private class CheckIfBroadcastStep implements TargetingStep {
        @Override
        public Endpoint execute(EndpointSelectorChain chain) {
            if (chain.getMessage().getClass().getAnnotation(Broadcast.class) != null) {
                Endpoint result = new Endpoint();
                result.setUsername(localEndpoint.getUsername());
                return result;
            }
            return chain.next();
        }        
    }
    
    private class CheckIfBypassRouting implements TargetingStep {
        @Override
        public Endpoint execute(EndpointSelectorChain chain) {
            if (chain.getMessage() instanceof BypassRouting) {
                return ((BypassRouting) chain.getMessage()).getTarget();
            }
            return chain.next();
        }
        
    }
}
