package com.ms.msdotnet.msdesktop.interfaces.kraken;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Endpoint {
    private String hostname;
    private String application;
    private String username;
    private String instanceName;
    private String cpsServer;
    private boolean kerberosed;
    
    public String getHostname() {
        return hostname;
    }
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }
    public String getApplication() {
        return application;
    }
    public void setApplication(String application) {
        this.application = application;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getInstanceName() {
        return instanceName;
    }
    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }
    public String getCpsServer() {
        return cpsServer;
    }
    public void setCpsServer(String cpsServer) {
        this.cpsServer = cpsServer;
    }
    public boolean getKerberosed() {
        return kerberosed;
    }
    public void setKerberosed(boolean kerberosed) {
        this.kerberosed = kerberosed;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Endpoint)) return false;
        if (obj == this) return true;

        Endpoint other = (Endpoint) obj;
        return new EqualsBuilder()
            .append(application, other.application)
            .append(username, other.username)
            .append(hostname, other.hostname)
            .isEquals();
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(51, 67)
            .append(getApplication())
            .append(getHostname())
            .append(getUsername())
            .toHashCode();
    }
}