package com.ms.msdotnet.msdesktop.standalone.kraken.messages;

import com.ms.msdotnet.msdesktop.interfaces.kraken.Broadcast;
import com.ms.msdotnet.msdesktop.interfaces.kraken.Endpoint;
import com.ms.msdotnet.msdesktop.interfaces.kraken.MapsToDotNetType;
import com.ms.msdotnet.msdesktop.standalone.kraken.InternalMessage;

@InternalMessage
@Broadcast
@MapsToDotNetType(type="MSDesktop.MessageRouting.ZeroConfig.SubscriptionAvailableMessage, MSDesktop.MessageRouting")
public class SubscriptionAvailableMessage {
    private String type;
    private Endpoint subscriberIdentity;
    
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public Endpoint getSubscriberIdentity() {
        return subscriberIdentity;
    }
    public void setSubscriberIdentity(Endpoint subscriberIdentity) {
        this.subscriberIdentity = subscriberIdentity;
    }
}
