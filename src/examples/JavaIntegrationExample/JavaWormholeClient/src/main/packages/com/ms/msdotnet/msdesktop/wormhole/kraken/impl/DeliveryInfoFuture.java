package com.ms.msdotnet.msdesktop.wormhole.kraken.impl;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.ms.msdotnet.msdesktop.interfaces.kraken.DeliveryInfo;

public class DeliveryInfoFuture implements Future<DeliveryInfo> {

    private final BlockingQueue<DeliveryInfo> reply = new ArrayBlockingQueue<DeliveryInfo>(1);
    
    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return this.reply.size() > 0;
    }

    @Override
    public DeliveryInfo get() throws InterruptedException, ExecutionException {
        return this.reply.take();
    }

    @Override
    public DeliveryInfo get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException,
            TimeoutException {
        final DeliveryInfo replyOrNull = reply.poll(timeout, unit);        
        if (replyOrNull == null) {
            throw new TimeoutException();
        }
        return replyOrNull;
    }

    public void receiveReply(DeliveryInfo info) {
        reply.add(info);
    }
}
