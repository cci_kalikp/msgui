package com.ms.msdotnet.msdesktop.standalone.kraken.messages;

import com.ms.msdotnet.msdesktop.interfaces.kraken.Broadcast;
import com.ms.msdotnet.msdesktop.interfaces.kraken.Endpoint;
import com.ms.msdotnet.msdesktop.interfaces.kraken.MapsToDotNetType;
import com.ms.msdotnet.msdesktop.standalone.kraken.InternalMessage;

@InternalMessage
@Broadcast
@MapsToDotNetType(type="MSDesktop.MessageRouting.ZeroConfig.SubscriptionQueryMessage, MSDesktop.MessageRouting")
public class SubscriptionQueryMessage {
    private Endpoint origin;
    private int queryId;
    private String messageType;
    public Endpoint getOrigin() {
        return origin;
    }
    public void setOrigin(Endpoint origin) {
        this.origin = origin;
    }
    public int getQueryId() {
        return queryId;
    }
    public void setQueryId(int queryId) {
        this.queryId = queryId;
    }
    public String getMessageType() {
        return messageType;
    }
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }    
}
