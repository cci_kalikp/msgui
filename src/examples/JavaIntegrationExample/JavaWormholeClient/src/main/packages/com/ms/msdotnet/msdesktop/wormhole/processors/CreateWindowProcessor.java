package com.ms.msdotnet.msdesktop.wormhole.processors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.ms.msdotnet.msdesktop.wormhole.MSDesktopMessageProcessor;
import com.ms.msdotnet.msdesktop.wormhole.MSDesktopShellHandler;

public class CreateWindowProcessor implements MSDesktopMessageProcessor {
    
    private static Logger logger = LoggerFactory.getLogger(CreateWindowProcessor.class); 
    
    private MSDesktopShellHandler client;
    
    public CreateWindowProcessor(MSDesktopShellHandler client) {
        super();
        this.client = client;
    }

    @Override
    public boolean processMessage(Document message) {
        Node createWindowNode = message.getElementsByTagName("CreateWindow").item(0);        
        if (createWindowNode != null) {
            logger.info("Received CreateWindow");
            String windowId = createWindowNode.getAttributes().getNamedItem("windowID").getTextContent();
            String factoryId = createWindowNode.getAttributes().getNamedItem("factoryID").getTextContent();
            Node stateNode = message.getElementsByTagName("State").item(0);
            Node state = stateNode != null ? stateNode.getChildNodes().item(1) : null;
            client.createWindow(windowId, factoryId, state);
            return true;
        }        
        return false;
    }

}
