package com.ms.msdotnet.msdesktop.wormhole.environment;

public enum Environment {
    N_A, Prod, UAT, QA, Dev
}
