package com.ms.msdotnet.msdesktop.standalone.kraken.messages;

import com.ms.msdotnet.msdesktop.interfaces.kraken.Endpoint;
import com.ms.msdotnet.msdesktop.interfaces.kraken.BypassRouting;
import com.ms.msdotnet.msdesktop.interfaces.kraken.MapsToDotNetType;
import com.ms.msdotnet.msdesktop.standalone.kraken.InternalMessage;

@InternalMessage
@MapsToDotNetType(type="MSDesktop.MessageRouting.Ack, MSDesktop.MessageRouting")
public class Ack implements BypassRouting {
    
    private int id;
    private Endpoint target;
    private boolean isAcceptable;
    private String extraInformation;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public Endpoint getTarget() {
        return target;
    }
    public void setTarget(Endpoint target) {
        this.target = target;
    }
    public boolean getIsAcceptable() {
        return isAcceptable;
    }
    public void setIsAcceptable(boolean acceptable) {
        this.isAcceptable = acceptable;
    }
    public String getExtraInformation() {
        return extraInformation;
    }
    public void setExtraInformation(String extraInformation) {
        this.extraInformation = extraInformation;
    }
}
