package com.ms.msdotnet.msdesktop.wormhole;

import org.w3c.dom.Node;

import com.ms.msdotnet.msdesktop.wormhole.environment.MSDesktopEnvironment;

public interface MSDesktopShellHandler {

    void initialize();
    
    void setClient(WormholeClient client);
    
    void createWindow(String windowId, String factoryId, Node state);

    void closeWindow(String windowId);

    void getWindowState(String windowId, String requestId);

    void setEnvironment(MSDesktopEnvironment msDesktopEnvironmentImpl);
    
}
