package com.ms.msdotnet.msdesktop.standalone.kraken.messages;

import com.ms.msdotnet.msdesktop.interfaces.kraken.Endpoint;
import com.ms.msdotnet.msdesktop.interfaces.kraken.BypassRouting;
import com.ms.msdotnet.msdesktop.interfaces.kraken.MapsToDotNetType;
import com.ms.msdotnet.msdesktop.standalone.kraken.InternalMessage;

@InternalMessage
@MapsToDotNetType(type="MSDesktop.MessageRouting.ZeroConfig.SubscriptionQueryReplyMessage, MSDesktop.MessageRouting")
public class SubscriptionQueryReplyMessage implements BypassRouting {
    private Endpoint subscriberIdentity;
    private Endpoint target;
    private int queryId;
    public Endpoint getSubscriberIdentity() {
        return subscriberIdentity;
    }
    public void setSubscriberIdentity(Endpoint subscriberIdentity) {
        this.subscriberIdentity = subscriberIdentity;
    }
    public Endpoint getTarget() {
        return target;
    }
    public void setTarget(Endpoint target) {
        this.target = target;
    }
    public int getQueryId() {
        return queryId;
    }
    public void setQueryId(int queryId) {
        this.queryId = queryId;
    }
    
}
