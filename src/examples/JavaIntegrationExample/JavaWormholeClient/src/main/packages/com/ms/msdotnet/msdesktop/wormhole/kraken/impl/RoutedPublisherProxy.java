package com.ms.msdotnet.msdesktop.wormhole.kraken.impl;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ms.msdotnet.msdesktop.interfaces.kraken.DeliveryInfo;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutedPublisher;
import com.ms.msdotnet.msdesktop.wormhole.WormholeClient;

public class RoutedPublisherProxy<T> implements RoutedPublisher<T> {

    private final String publisherId;
    private final WormholeClient client;
    private final ObjectMapper mapper;
    private final String type;
    private final ConcurrentHashMap<String, DeliveryInfoFuture> deliveryInfos;
    
    public RoutedPublisherProxy(String publisherId, WormholeClient client, String type) {
        this.client = client;
        this.publisherId = publisherId;
        this.mapper = new ObjectMapper();
        this.type = type;
        this.deliveryInfos = new ConcurrentHashMap<String, DeliveryInfoFuture>();
    }

    @Override
    public DeliveryInfo publish(T message) {        
        try {
            return publishAsync(message).get();
        } catch (InterruptedException e) {
            throw new RuntimeException( e );
        } catch (ExecutionException e) {
            throw new RuntimeException( e );
        }
    }
    
    @Override
    public Future<DeliveryInfo> publishAsync(T message) {        
        try {
            String requestId = java.util.UUID.randomUUID().toString();
            String messageSerialized = mapper.writeValueAsString(message);
            DeliveryInfoFuture result = new DeliveryInfoFuture();
            deliveryInfos.put(requestId, result);
            client.sendCommand("<KrakenPublish publisherId=\"" + publisherId + "\" " +
                    "type=\"" + type + "\" requestId=\"" + requestId + "\">" + messageSerialized + "</KrakenPublish>");
            return result;
        } catch (JsonGenerationException e) {
            throw new RuntimeException( e );
        } catch (JsonMappingException e) {
            throw new RuntimeException( e );
        } catch (IOException e) {
            throw new RuntimeException( e );
        }
    }

    public void receiveDeliveryInfo(DeliveryInfo result, String requestId) {
        if (deliveryInfos.containsKey(requestId)) {
            DeliveryInfoFuture future = deliveryInfos.get(requestId);
            future.receiveReply(result);
            deliveryInfos.remove(future);
        }
    }

}
