package com.ms.msdotnet.msdesktop.interfaces.kraken;

public @interface DeliveryStrategy {
    public DeliveryStrategyEnum deliveryStrategy();
}
