package com.ms.msdotnet.msdesktop.wormhole.environment;

public interface MSDesktopEnvironment {
    public Environment getEnvironment();
    public Region getRegion();
    public String getUser();    
}
