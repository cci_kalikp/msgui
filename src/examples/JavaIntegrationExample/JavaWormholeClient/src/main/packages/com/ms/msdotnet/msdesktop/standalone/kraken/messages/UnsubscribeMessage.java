package com.ms.msdotnet.msdesktop.standalone.kraken.messages;

import com.ms.msdotnet.msdesktop.interfaces.kraken.Broadcast;
import com.ms.msdotnet.msdesktop.interfaces.kraken.Endpoint;
import com.ms.msdotnet.msdesktop.interfaces.kraken.MapsToDotNetType;
import com.ms.msdotnet.msdesktop.standalone.kraken.InternalMessage;

@InternalMessage
@Broadcast
@MapsToDotNetType(type="MSDesktop.MessageRouting.ZeroConfig.UnsubscribeMessage, MSDesktop.MessageRouting")
public class UnsubscribeMessage {
    private Endpoint origin;

    public Endpoint getOrigin() {
        return origin;
    }

    public void setOrigin(Endpoint origin) {
        this.origin = origin;
    }
}
