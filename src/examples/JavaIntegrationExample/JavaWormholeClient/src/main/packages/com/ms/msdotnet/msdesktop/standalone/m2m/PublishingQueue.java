package com.ms.msdotnet.msdesktop.standalone.m2m;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.IMC.CPS.IMCMessages.CpsApplicationInfo;
import MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.IMC.CPS.IMCMessages.CpsModuleMessage;
import MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.IMC.CPS.IMCMessages.CpsModuleMessage.Environment;
import appmw.cpsjava.client.publisher.impl.CPSProtoBufPublisher;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.ByteString;
import com.ms.msdotnet.msdesktop.interfaces.kraken.MapsToDotNetType;
import com.ms.msdotnet.msdesktop.interfaces.kraken.TypeArgumentAccessor;

public class PublishingQueue extends Thread {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(PublishingQueue.class);
    
    private final CPSProtoBufPublisher publisher;
    private final BlockingQueue<Object> queue;
    private final String applicationName;
    private final Environment applicationEnvironment;
    private final ObjectMapper mapper;
        
    private long seq = 0;

    public PublishingQueue(CPSProtoBufPublisher publisher, String applicationName, Environment applicationEnvironment) {
        this.publisher = publisher;
        this.applicationName = applicationName;
        this.applicationEnvironment = applicationEnvironment;
        this.queue = new LinkedBlockingQueue<Object>();
        this.mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(new PascalCasePropertyNamingStrategy());
        setName("Publishing Queue Thread");
    }
    
    public void publish(Object message) {
        LOGGER.info("New item on the queue");
        queue.add(message);
    }
    
    public void run() {
        try {
            while (true) {
                LOGGER.info("Waiting for a new item to publish...");
                Object item = queue.take();
                LOGGER.info("Publishing message of type {}", item.getClass());
                doPublish(item);
                LOGGER.info("Item successfully published");
            }
        } catch (InterruptedException e) { 
            LOGGER.warn("PublishingQueue thread interrupted"); 
        }
    }
    
    private void doPublish(Object message) {
        try {
            final CpsModuleMessage.Builder builder = CpsModuleMessage.newBuilder();        
            builder.setContent(ByteString.copyFrom(mapper.writeValueAsBytes(message)));
            builder.setAppEnvironment(applicationEnvironment);
            builder.setApplicationName(applicationName);
            
            MapsToDotNetType annotation = (MapsToDotNetType)message.getClass().getAnnotation(MapsToDotNetType.class);
            String messageType = annotation.type();
            String[] parts = messageType.split(",");
            if (parts.length > 1) {
                builder.setMessageAssembly(parts[1].trim());
            } else {
                builder.setMessageAssembly("MSDesktop.MessageRouting");
            }
            
            for (Method method : message.getClass().getDeclaredMethods()) {                
                if (method.isAnnotationPresent(TypeArgumentAccessor.class)) {         
                    Class<?> typeArgument = (Class<?>) method.invoke(message);
                    MapsToDotNetType innerTypeAnnotation = (MapsToDotNetType)typeArgument.getAnnotation(MapsToDotNetType.class);                    
                    messageType = messageType + "`1[[" + innerTypeAnnotation.type() + "]]";
                }
            }   
            
            builder.setSequence(seq++);
            builder.setMachineName(InetAddress.getLocalHost().getHostName());
            builder.setUserName(System.getProperty("user.name"));
            builder.setMessageType(messageType);
            
            builder.setSender(CpsApplicationInfo.newBuilder()
                    .setApplication(applicationName)
                    .setHost(InetAddress.getLocalHost().getHostName())
                    .setUser(System.getProperty("user.name"))
                    .setDesktop("")
                    .setInstance(applicationName)
                    .build());
            
            publisher.publish(builder.build(), 30, TimeUnit.SECONDS);
        } catch (JsonProcessingException | UnknownHostException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            LOGGER.error("Exception occured when trying to publish", e);
        }
    }
    
}
