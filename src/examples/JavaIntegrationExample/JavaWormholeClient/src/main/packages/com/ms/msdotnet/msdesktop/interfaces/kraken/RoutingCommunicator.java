package com.ms.msdotnet.msdesktop.interfaces.kraken;

public interface RoutingCommunicator extends AutoCloseable {
    <T> RoutedPublisher<T> getPublisher(Class<T> type);
    <T> RoutedSubscriber<T> getSubscriber(Class<T> type);
}