package com.ms.msdotnet.msdesktop.standalone.kraken.messages;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ms.msdotnet.msdesktop.interfaces.kraken.Endpoint;
import com.ms.msdotnet.msdesktop.interfaces.kraken.MapsToDotNetType;
import com.ms.msdotnet.msdesktop.interfaces.kraken.TypeArgumentAccessor;

@MapsToDotNetType(type="MSDesktop.MessageRouting.RoutedMessage")
public class RoutedMessage<T> {
    
    private T message;
    private int id;
    private Endpoint origin;
    private Endpoint target;
    private int attempts;
    //2014-05-02T13:24:12.4078718+01:00
    //@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSSSSSXXX")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss")
    private Date lastAttempt;
    
    public T getMessage() {
        return message;
    }
    public void setMessage(T message) {
        this.message = message;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public Endpoint getOrigin() {
        return origin;
    }
    public void setOrigin(Endpoint origin) {
        this.origin = origin;
    }
    public Endpoint getTarget() {
        return target;
    }
    public void setTarget(Endpoint target) {
        this.target = target;
    }
    public int getAttempts() {
        return attempts;
    }
    public void setAttempts(int attepmpts) {
        this.attempts = attepmpts;
    }
    public Date getLastAttempt() {
        return lastAttempt;
    }
    public void setLastAttempt(Date lastAttempt) {
        this.lastAttempt = lastAttempt;
    }   
    
    @TypeArgumentAccessor
    public Class<?> innerType() {
        return getMessage().getClass();
    }
}
