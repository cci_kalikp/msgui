package com.ms.msdotnet.msdesktop.wormhole;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Node;

import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutingCommunicator;
import com.ms.msdotnet.msdesktop.wormhole.chrome.InitialWidgetParameters;
import com.ms.msdotnet.msdesktop.wormhole.environment.MSDesktopEnvironment;
import com.ms.msdotnet.msdesktop.wormhole.kraken.impl.RoutingCommunicatorProxy;

public abstract class MSDesktopHostedModule implements MSDesktopShellHandler {

    protected WormholeClient client;
    protected MSDesktopEnvironment environment;
    protected RoutingCommunicator communicator;

    private Map<String, WindowFactory> factories;
    private Map<String, String> factoryIds;
    
    public MSDesktopHostedModule() {
        this.factories = new HashMap<String, WindowFactory>();
        this.factoryIds = new HashMap<String, String>();
    }
    
    @Override
    public void setClient(WormholeClient client) {
        this.client = client;
        this.communicator = new RoutingCommunicatorProxy(client);
    };
    
    @Override
    public void setEnvironment(MSDesktopEnvironment msDesktopEnvironmentImpl) {
        this.environment = msDesktopEnvironmentImpl;
    };
    
    @Override
    public void createWindow(String windowId, String factoryId, Node state) {
        WindowFactory factory = factories.get(factoryId);
        factory.createWindow(windowId, state);
        factoryIds.put(windowId, factoryId);
    }
    
    @Override
    public void getWindowState(String windowId, String requestId) {
        String factoryId = factoryIds.get(windowId);
        String state = factories.get(factoryId).dehydrateWindow(windowId);
        if (state == null) {
            state = "<EmptyState />";
        }
        client.sendWindowState(windowId, requestId, state);
    }
    
    protected void registerWindowFactory(String factoryId, WindowFactory factory) {
        client.sendRegisterWindowFactory(factoryId);
        factories.put(factoryId, factory);
    }
    
    protected void placeWidget(String location, InitialWidgetParameters parameters) {
        client.sendPlaceWidget(location, null, parameters);
    }

	protected void placeWidget(String location, String widgetId, InitialWidgetParameters parameters) {
        client.sendPlaceWidget(location, widgetId, parameters);
    }

}
