package com.ms.msdotnet.msdesktop.standalone.kraken;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ms.msdotnet.msdesktop.interfaces.kraken.Endpoint;
import com.ms.msdotnet.msdesktop.interfaces.kraken.MessageListener;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutedPublisher;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutedSubscriber;
import com.ms.msdotnet.msdesktop.standalone.kraken.messages.SubscriptionQueryMessage;
import com.ms.msdotnet.msdesktop.standalone.kraken.messages.SubscriptionQueryReplyMessage;
import com.ms.msdotnet.msdesktop.standalone.kraken.messages.UnsubscribeMessage;
import com.ms.msdotnet.msdesktop.standalone.m2m.TypeRegistry;

public class RouteExplorer implements AutoCloseable {

    private static final Logger LOGGER = LoggerFactory.getLogger(RouteExplorer.class);
    
    private final Endpoint localEndpoint;
    private final CopyOnWriteArraySet<Endpoint> availableEndpoints;
    private final RoutedPublisher<SubscriptionQueryReplyMessage> subscriptionQueryReplyMessagePublisher;
    private final RoutedPublisher<SubscriptionQueryMessage> subscriptionQueryMessagePublisher;
    private final TypeRegistry typeRegistry;
    private final ScheduledThreadPoolExecutor executorService;
    private final RoutedSubscriber<SubscriptionQueryMessage> subscriptionQueryMessageSubscriber;
    private final RoutedSubscriber<SubscriptionQueryReplyMessage> subscriptionQueryReplyMessageSubscriber;
    private final RoutedSubscriber<UnsubscribeMessage> unsubscribeMessageSubscriber;
    
    private static int seq = 0;
    private static int requestAvailableEndpointsTimeout = 3;
    
    public RouteExplorer(RoutingCommunicatorImpl communicator, Endpoint localEndpoint, TypeRegistry registry) {
        this.localEndpoint = localEndpoint;
        this.availableEndpoints = new CopyOnWriteArraySet<Endpoint>();
        this.typeRegistry = registry;
        executorService = new ScheduledThreadPoolExecutor(1);
        subscriptionQueryReplyMessagePublisher = communicator.getPublisher(SubscriptionQueryReplyMessage.class);
        subscriptionQueryMessagePublisher = communicator.getPublisher(SubscriptionQueryMessage.class);
        
        subscriptionQueryMessageSubscriber = communicator.getSubscriber(SubscriptionQueryMessage.class);
        subscriptionQueryReplyMessageSubscriber = communicator.getSubscriber(SubscriptionQueryReplyMessage.class);
        unsubscribeMessageSubscriber = communicator.getSubscriber(UnsubscribeMessage.class);
        
        subscriptionQueryMessageSubscriber.subscribe(new SubscriptionQueryMessageListener());
        subscriptionQueryReplyMessageSubscriber.subscribe(new SubscriptionQueryReplyMessageListener());
        unsubscribeMessageSubscriber.subscribe(new UnsubscribeMessageListener());
    }
    
    public Future<Set<Endpoint>> requestAvailableEndpoints(Class<?> type) {
        SubscriptionQueryMessage message = new SubscriptionQueryMessage();
        message.setMessageType(typeRegistry.getDotNetType(type));
        message.setOrigin(localEndpoint);
        message.setQueryId(seq++);
        subscriptionQueryMessagePublisher.publish(message);
        return executorService.schedule(new Callable<Set<Endpoint>>() {
            @Override
            public Set<Endpoint> call() throws Exception {
                Set<Endpoint> result = new HashSet<Endpoint>();
                result.addAll(availableEndpoints);
                return result;
            }
        }, requestAvailableEndpointsTimeout, TimeUnit.SECONDS);
    }
    
    private class SubscriptionQueryMessageListener implements MessageListener<SubscriptionQueryMessage> {
        @Override
        public void receive(SubscriptionQueryMessage message) {
            SubscriptionQueryReplyMessage reply = new SubscriptionQueryReplyMessage();
            reply.setQueryId(message.getQueryId());
            reply.setSubscriberIdentity(localEndpoint);
            reply.setTarget(message.getOrigin());
            subscriptionQueryReplyMessagePublisher.publish(reply);            
        }        
    }
    
    private class SubscriptionQueryReplyMessageListener implements MessageListener<SubscriptionQueryReplyMessage> {
        @Override
        public void receive(SubscriptionQueryReplyMessage message) {
            if (!availableEndpoints.contains(message.getSubscriberIdentity())) {
                availableEndpoints.add(message.getSubscriberIdentity());
            }
        }
    }  
    
    private class UnsubscribeMessageListener implements MessageListener<UnsubscribeMessage> {
        @Override
        public void receive(UnsubscribeMessage message) {
            availableEndpoints.remove(message.getOrigin());
        }
        
    }

    @Override
    public void close() throws RuntimeException {
        LOGGER.info("Closing RouteExplorer");       
        try {
            subscriptionQueryMessageSubscriber.close();
            subscriptionQueryReplyMessageSubscriber.close();
            unsubscribeMessageSubscriber.close();
        } catch (Exception e) {
            throw new RuntimeException( e );
        }
    }
    
}
