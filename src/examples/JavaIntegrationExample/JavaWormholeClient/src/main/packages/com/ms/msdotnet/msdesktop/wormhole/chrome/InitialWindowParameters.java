package com.ms.msdotnet.msdesktop.wormhole.chrome;

public class InitialWindowParameters {

    private int initialLocation = InitialLocation.Floating;

    public int getInitialLocation() {
        return initialLocation;
    }

    public void setInitialLocation(int initialLocation) {
        this.initialLocation = initialLocation;
    }
}
