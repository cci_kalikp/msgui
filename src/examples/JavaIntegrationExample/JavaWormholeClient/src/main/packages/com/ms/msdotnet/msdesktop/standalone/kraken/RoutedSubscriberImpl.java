package com.ms.msdotnet.msdesktop.standalone.kraken;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.ms.msdotnet.msdesktop.interfaces.kraken.MessageListener;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutedSubscriber;

public class RoutedSubscriberImpl<T> implements RoutedSubscriber<T> {
    private final ConcurrentLinkedQueue<MessageListener<T>> listeners;
    private final RoutingCommunicatorImpl communicator;
    private final Class<T> messageType;
    
    @Override
    public void subscribe(MessageListener<T> listener) {
        addListener(listener);
    }
    
    public RoutedSubscriberImpl(RoutingCommunicatorImpl routingCommunicatorImpl, Class<T> type) {
        listeners = new ConcurrentLinkedQueue<MessageListener<T>>();
        communicator = routingCommunicatorImpl;
        messageType = type;
    }
    
    public void addListener(MessageListener<T> listener) {
        listeners.add(listener);
    }
    
    public void removeListener(MessageListener<T> listener) {
        listeners.remove(listener);
    }
    
    @SuppressWarnings("unchecked")
    void receive(Object result) {
        for (MessageListener<T> listener : listeners) {
            listener.receive((T)result);
        }
    }

    @Override
    public void close() {
        communicator.removeSubscriber(this, messageType);
    }
}
