package com.ms.msdotnet.msdesktop.standalone.m2m;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import msjava.cxfutils.transport.msnet.TCPClientTransport;
import msjava.cxfutils.transport.msnet.experimental.SingleConnectionTCPClientTransport;
import msjava.msnet.MSNetLoop;
import msjava.msnet.MSNetLoopThread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.IMC.CPS.IMCMessages.CpsModuleMessage;
import MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.IMC.CPS.IMCMessages.CpsModuleMessage.Environment;
import appmw.cpsjava.client.publisher.impl.CPSProtoBufPublisher;
import appmw.cpsjava.client.subscriber.CPSSubscriptionListenerAdapter;
import appmw.cpsjava.client.subscriber.impl.CPSProtoBufSubscriber;
import appmw.cpsjava.integrations.msnet.MSNetCPS0Protocol;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ms.msdotnet.msdesktop.interfaces.kraken.SubscriptionFailureHandler;
import com.ms.msdotnet.msdesktop.interfaces.m2m.Communicator;
import com.ms.msdotnet.msdesktop.interfaces.m2m.ModulePublisher;
import com.ms.msdotnet.msdesktop.interfaces.m2m.ModuleSubscriber;

@SuppressWarnings("restriction")
public class CommunicatorImpl implements Communicator {

    private static final Logger LOGGER= LoggerFactory.getLogger(CommunicatorImpl.class);

    private final static String CpsModuleMessageType = "MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.IMC.CPS.CpsModuleMessage";
    private final static String CpsModuleTopicName = "ModuleMessage";
    
    private MSNetLoop loop;
    private CPSProtoBufSubscriber<CpsModuleMessage> subscriber;
    private CPSProtoBufPublisher publisher;
    private PublishingQueue publishingQueue;
    
    private final SubscriptionFailureHandler subscriptionFailureHandler;    
    private final ObjectMapper mapper;
    private final Map<Class<?>, Set<ModuleSubscriberImpl<?>>> subscribers;
    private final String hostport;
    private final TypeRegistry typeRegistry;
    private final String subscriptionId;
            
    public CommunicatorImpl(String address, String applicationName, Environment applicationEnvironment, TypeRegistry registry) {
        this(address, applicationName, applicationEnvironment, registry, null);
    }
    
    public CommunicatorImpl(String address, String applicationName, Environment applicationEnvironment, TypeRegistry registry, SubscriptionFailureHandler handler) {
        subscribers = new HashMap<Class<?>, Set<ModuleSubscriberImpl<?>>>();
        mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(new PascalCasePropertyNamingStrategy());
        hostport = address;        
        typeRegistry = registry;
        subscriptionFailureHandler = handler;
        
        try {
            loop = new MSNetLoopThread("communicatorLoop").startLoop();
            TCPClientTransport tcpTransport = new TCPClientTransport();
            tcpTransport.setHostPort(hostport);
            tcpTransport.setLoop(loop);
            tcpTransport.setProtocol(new MSNetCPS0Protocol());
            tcpTransport.setKerberos(true);
            tcpTransport.afterPropertiesSet();
            
            SingleConnectionTCPClientTransport pubTransport = new SingleConnectionTCPClientTransport();
            pubTransport.setHostPort(hostport);
            pubTransport.setLoop(loop);
            pubTransport.setProtocol(new MSNetCPS0Protocol());
            pubTransport.setKerberos(true);
            pubTransport.afterPropertiesSet();
            pubTransport.start();
            
            publisher = new CPSProtoBufPublisher();
            publisher.setClientTransport(pubTransport);
            publisher.setTopic(CpsModuleTopicName);
            publisher.setId("pub1");
            publisher.afterPropertiesSet();

            subscriber = new CPSProtoBufSubscriber<CpsModuleMessage>();
            subscriber.setClientTransport(tcpTransport);
            subscriber.setTopic(CpsModuleTopicName);
            subscriber.setType(CpsModuleMessage.class);
            subscriber.setProtocolName(CpsModuleMessageType);
            subscriber.setSubscriptionId("sub1");
            subscriber.afterPropertiesSet();
            
            subscriptionId = subscriber.subscribe(new CPSSubscriptionListenerAdapter<CpsModuleMessage>() {
                @Override
                public void publicationCallback(CpsModuleMessage message) {
                    LOGGER.info("Received CpsModuleMessage of type " + message.getMessageType() + " from application " + message.getApplicationName());
                    try {
                        String dotNetType = message.getMessageType();
                        JavaType javaType = typeRegistry.getJavaType(dotNetType);
                        Class<?> clazz = javaType.getRawClass();
                        
                        try {
                            String messageContent = message.getContent().toStringUtf8();
                            Object result = mapper.readValue(messageContent, javaType);
                            for (ModuleSubscriberImpl<?> subscriber : subscribers.get(clazz)) {
                                subscriber.receive(result);
                            }
                        } catch (IOException|RuntimeException e) {
                            for (ModuleSubscriberImpl<?> subscriber : subscribers.get(clazz)) {
                                subscriber.onFailure(e);
                            }
                        }
                    } catch (RuntimeException e) {
                        if (subscriptionFailureHandler != null) {
                            subscriptionFailureHandler.onFailure(e);
                        }
                    }
                }
            }, 10, TimeUnit.SECONDS);
            
            publishingQueue = new PublishingQueue(publisher, applicationName, applicationEnvironment);
            publishingQueue.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public <T> ModulePublisher<T> getPublisher(Class<T> clazz) {
        return new ModulePublisherImpl<T>(this);
    }

    @Override
    public <T> ModuleSubscriber<T> getSubscriber(Class<T> clazz) {
        ModuleSubscriberImpl<T> subscriber = new ModuleSubscriberImpl<T>(this, clazz);
        if (!subscribers.containsKey(clazz)) {
            subscribers.put(clazz, new HashSet<ModuleSubscriberImpl<?>>());
        }
        subscribers.get(clazz).add(subscriber);
        return subscriber;
    }
    
    void removeSubscriber(ModuleSubscriber<?> subscriber, Class<?> clazz) {
        subscribers.get(clazz).remove(subscriber);
    }
    
    public <T> void publish(T message) {
        publishingQueue.publish(message);
    }

    @Override
    public void close() throws RuntimeException {
        LOGGER.info("Closing CommunicatorImpl");
        subscriber.unsubscribeAsync(subscriptionId);
        publishingQueue.interrupt();
    }

}
