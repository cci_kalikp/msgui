package com.ms.msdotnet.msdesktop.interfaces.kraken;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Broadcast {
    /* marker annotation */
}
