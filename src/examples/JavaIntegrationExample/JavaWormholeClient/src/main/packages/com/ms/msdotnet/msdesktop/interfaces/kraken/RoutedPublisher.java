package com.ms.msdotnet.msdesktop.interfaces.kraken;

import java.util.concurrent.Future;

public interface RoutedPublisher<T> {
    DeliveryInfo publish(T message);
    Future<DeliveryInfo> publishAsync(T message);
}
