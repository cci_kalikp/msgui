package com.ms.msdotnet.msdesktop.standalone.kraken;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.IMC.CPS.IMCMessages.CpsModuleMessage.Environment;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import com.ms.msdotnet.msdesktop.interfaces.kraken.DeliveryInfo;
import com.ms.msdotnet.msdesktop.interfaces.kraken.DeliveryStatus;
import com.ms.msdotnet.msdesktop.interfaces.kraken.Endpoint;
import com.ms.msdotnet.msdesktop.interfaces.kraken.FailureAwareMessageListener;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutedPublisher;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutedSubscriber;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutingCommunicator;
import com.ms.msdotnet.msdesktop.interfaces.kraken.SubscriptionFailureHandler;
import com.ms.msdotnet.msdesktop.interfaces.m2m.ModulePublisher;
import com.ms.msdotnet.msdesktop.interfaces.m2m.ModuleSubscriber;
import com.ms.msdotnet.msdesktop.standalone.kraken.messages.Ack;
import com.ms.msdotnet.msdesktop.standalone.kraken.messages.RoutedMessage;
import com.ms.msdotnet.msdesktop.standalone.kraken.messages.SubscriptionAvailableMessage;
import com.ms.msdotnet.msdesktop.standalone.kraken.messages.SubscriptionQueryMessage;
import com.ms.msdotnet.msdesktop.standalone.kraken.messages.SubscriptionQueryReplyMessage;
import com.ms.msdotnet.msdesktop.standalone.kraken.messages.UnsubscribeMessage;
import com.ms.msdotnet.msdesktop.standalone.m2m.CommunicatorImpl;
import com.ms.msdotnet.msdesktop.standalone.m2m.TypeRegistry;

@SuppressWarnings("rawtypes")
public class RoutingCommunicatorImpl implements RoutingCommunicator {
    private static final Logger LOGGER = LoggerFactory.getLogger(RoutingCommunicatorImpl.class);

    private final CommunicatorImpl communicator;
    private ModuleSubscriber<RoutedMessage> underlyingSubscriber;
    private ModulePublisher<RoutedMessage> underlyingPublisher;
    private AckTrackingPublisher ackTrackingPublisher;
    
    private final SubscriptionFailureHandler subscriptionFailureHandler;
    private final Map<Class<?>, Set<RoutedSubscriberImpl<?>>> subscribers;
    private final Endpoint localEndpoint;
    private final TypeRegistry typeRegistry;
    private final RouteExplorer explorer;
    private final ResolverChainEndpointSelector endpointSelector;
    private final FailureAwareMessageListener<RoutedMessage> routedMessageListener;

    public RoutingCommunicatorImpl(String hostport, String applicationName, Environment appEnvironment, TypeRegistry registry) {
        this(hostport, applicationName, appEnvironment, registry, new SubscriptionFailureHandler() {            
            @Override
            public void onFailure(Throwable t) {
                LOGGER.error("Error when receiving a message: " + t);
            }
        });
    }
    
    public RoutingCommunicatorImpl(String hostport, String applicationName, Environment appEnvironment, TypeRegistry registry, SubscriptionFailureHandler handler) {
        try {
            registry.registerDotNetType(SubscriptionAvailableMessage.class);
            registry.registerDotNetType(Ack.class);
            registry.registerDotNetType(RoutedMessage.class);
            registry.registerDotNetType(SubscriptionQueryMessage.class);
            registry.registerDotNetType(SubscriptionQueryReplyMessage.class);
            registry.registerDotNetType(UnsubscribeMessage.class);
            typeRegistry = registry;
            subscriptionFailureHandler = handler;
            
            communicator = new CommunicatorImpl(hostport, applicationName, appEnvironment, registry);            
            
            subscribers = new HashMap<Class<?>, Set<RoutedSubscriberImpl<?>>>();
            localEndpoint = new Endpoint();
            localEndpoint.setApplication(applicationName);
            localEndpoint.setHostname(InetAddress.getLocalHost().getHostName());
            localEndpoint.setUsername(System.getProperty("user.name"));
            
            explorer = new RouteExplorer(this, localEndpoint, typeRegistry);
            endpointSelector = new ResolverChainEndpointSelector(localEndpoint);
            
            underlyingSubscriber = communicator.getSubscriber(RoutedMessage.class);
            underlyingPublisher = communicator.getPublisher(RoutedMessage.class);
            ackTrackingPublisher = new AckTrackingPublisher(underlyingPublisher, getSubscriber(Ack.class));
            
            final RoutedPublisher<Ack> ackPublisher = getPublisher(Ack.class);            
            routedMessageListener = new FailureAwareMessageListener<RoutedMessage>() {
                @Override
                public void receive(RoutedMessage message) {
                    try {
                        if (!((message.getTarget().getApplication() == null && message.getTarget().getHostname() == null) || message
                                .getTarget().equals(localEndpoint))) {
                            return;
                        }
                        LOGGER.info("Received message of type {}", message.getMessage().getClass());

                        if (!(message.getMessage() instanceof Ack)) {
                            Ack ack = new Ack();
                            ack.setId(message.getId());
                            ack.setIsAcceptable(true);
                            ack.setTarget(message.getOrigin());
                            ackPublisher.publish(ack);
                        }

                        if (subscribers.containsKey(message.getMessage().getClass())) {
                            for (RoutedSubscriberImpl<?> subscriber : subscribers.get(message.getMessage().getClass())) {
                                subscriber.receive(message.getMessage());
                            }
                        }

                    } catch (RuntimeException e) {
                        subscriptionFailureHandler.onFailure(e);
                    }
                }

                @Override
                public void onFailure(Throwable e) {
                    if (subscriptionFailureHandler != null) {
                        subscriptionFailureHandler.onFailure(e);
                    }                    
                }
            };
            underlyingSubscriber.addListener(routedMessageListener);
        } catch (UnknownHostException e) {
            throw new RuntimeException( e );
        }
    }
        
    @Override
    public <T> RoutedPublisher<T> getPublisher(Class<T> type) {
        return new RoutedPublisherImpl<T>(this);
    }

    @Override
    public <T> RoutedSubscriber<T> getSubscriber(Class<T> type) {
        RoutedSubscriberImpl<T> subscriber = new RoutedSubscriberImpl<T>(this, type);
        if (!subscribers.containsKey(type)) {
            subscribers.put(type, new HashSet<RoutedSubscriberImpl<?>>());
        }
        subscribers.get(type).add(subscriber);
        return subscriber;
    }
    
    void removeSubscriber(RoutedSubscriber<?> subscriber, Class<?> type) {
        subscribers.get(type).remove(subscriber);
    }
    
    <T> DeliveryInfo publish(T message) {
        publishAsync(message);
        return null;
    }
    
    <T> ListenableFuture<DeliveryInfo> publishAsync(T message) {
        LOGGER.info("Publishing message of type {}", message.getClass());
        
        RoutedMessage<T> routedMessage = new RoutedMessage<T>();
        routedMessage.setMessage(message);
        routedMessage.setOrigin(localEndpoint);
        routedMessage.setAttempts(0);
        routedMessage.setLastAttempt(new Date());
        
        Endpoint target = endpointSelector.resolveTarget(message);
        if (target == null) {
            DeliveryInfo info = new DeliveryInfo();
            info.setStatus(DeliveryStatus.EndpointNotResolved);
            SettableFuture<DeliveryInfo> result = SettableFuture.create();
            result.set(info);
            return result;
        }
        
        routedMessage.setTarget(target);
        
        return ackTrackingPublisher.publish(routedMessage);
    }

    @Override
    public void close() throws RuntimeException {
        LOGGER.info("Closing RoutingCommunicatorImpl");
        try {
            underlyingSubscriber.close();
        } catch (Exception e) {
            /* never happens */
        }
        explorer.close();
        communicator.close();
    }
}
