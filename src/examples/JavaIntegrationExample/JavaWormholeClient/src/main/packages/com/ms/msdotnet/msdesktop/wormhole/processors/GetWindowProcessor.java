package com.ms.msdotnet.msdesktop.wormhole.processors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.ms.msdotnet.msdesktop.wormhole.MSDesktopMessageProcessor;
import com.ms.msdotnet.msdesktop.wormhole.MSDesktopShellHandler;

public class GetWindowProcessor implements MSDesktopMessageProcessor {

private static Logger logger = LoggerFactory.getLogger(GetWindowProcessor.class); 
    
    private MSDesktopShellHandler client;
    
    public GetWindowProcessor(MSDesktopShellHandler client) {
        super();
        this.client = client;
    }
    
    @Override
    public boolean processMessage(Document message) {
        Node getWindowStateNode = message.getElementsByTagName("GetWindowState").item(0);
        if (getWindowStateNode != null) {
            logger.info("Received GetWindowState");
            String windowId = getWindowStateNode.getAttributes().getNamedItem("windowID").getTextContent();
            String requestId = getWindowStateNode.getAttributes().getNamedItem("requestID").getTextContent();
            client.getWindowState(windowId, requestId);
            return true;
        }
        return false;
    }

}
