package com.ms.msdotnet.msdesktop.interfaces.kraken;

public interface SubscriptionFailureHandler {
    void onFailure(Throwable t);
}
