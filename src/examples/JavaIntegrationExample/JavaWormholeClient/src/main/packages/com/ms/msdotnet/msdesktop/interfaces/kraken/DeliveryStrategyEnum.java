package com.ms.msdotnet.msdesktop.interfaces.kraken;

public enum DeliveryStrategyEnum {
    FireAndForget, Tracked
}
