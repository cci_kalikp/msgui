package com.ms.msdotnet.msdesktop.wormhole.chrome;

public class InitialLocation {
    /// <summary>
    /// The new view will be created floating; and can be docked by the user. 
    /// </summary>
    public static int Floating = 1;

    /// <summary>
    /// The new view will be created floating; and can NOT be docked by the user. 
    /// </summary>
    public static int FloatingOnly = 2;

    /// <summary>
    /// The new view will be created docked to a new empty tab in the main docking area.
    /// </summary>
    public static int DockInNewTab = 4;

    /// <summary>
    /// The new view will be created floating centered under the mouse pointer. Can only be 
    /// used in conjunction with InitialLocation.Floating or InitialLocation.FloatingOnly.
    /// </summary>
    public static int PlaceAtCursor = 8;

    /// <summary>
    /// Custom placement logic via the InitialLocationCallback; look at <see cref="InitialLocationHelper"/> for 
    /// common scenarios. Can only be used in conjunction with InitialLocation.Floating or 
    /// InitialLocation.FloatingOnly.
    /// </summary>
    public static int Custom = 16;

    /// <summary>
    /// The new view will be created docked in a tab group with DockTarget in <see cref="InitialWindowParameters"/>.
    /// 
    /// If InitialLocationTarget is null and DocumentContentHost mode is enabled; pane will be docked
    /// inside the DocumentContentHost.
    /// </summary>
    public static int DockTabbed = 32;

    /// <summary>
    /// The new view will be created docked to the left of DockTarget in <see cref="InitialWindowParameters"/>.
    /// </summary>
    public static int DockLeft = 64;

    /// <summary>
    /// The new view will be created docked to the top of DockTarget in <see cref="InitialWindowParameters"/>.
    /// </summary>
    public static int DockTop = 128;

    /// <summary>
    /// The new view will be created docked to the right of DockTarget in <see cref="InitialWindowParameters"/>.
    /// </summary>
    public static int DockRight = 256;

    /// <summary>
    /// The new view will be created docked to the bottom of DockTarget in <see cref="InitialWindowParameters"/>.
    /// </summary>
    public static int DockBottom = 512;

    /// <summary>
    /// Thew new view will be docked in the currently active tab. Has to be used in conjuction with
    /// DockTabbed; DockLeft; DockTop; DockRight or DockBottom.
    /// 
    /// The option should be used with InitialLocationTarget == null.
    /// </summary>
    public static int DockInActiveTab = 1024;
}
