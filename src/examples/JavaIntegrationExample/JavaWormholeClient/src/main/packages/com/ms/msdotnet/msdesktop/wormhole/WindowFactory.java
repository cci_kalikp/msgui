package com.ms.msdotnet.msdesktop.wormhole;

import org.w3c.dom.Node;

public interface WindowFactory {

    boolean createWindow(String windowId, Node state);
    
    String dehydrateWindow(String windowId);
    
}
