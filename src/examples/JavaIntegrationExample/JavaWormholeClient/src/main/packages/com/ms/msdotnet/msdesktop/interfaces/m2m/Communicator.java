package com.ms.msdotnet.msdesktop.interfaces.m2m;

public interface Communicator extends AutoCloseable {
    <T> ModulePublisher<T> getPublisher(Class<T> clazz);
    <T> ModuleSubscriber<T> getSubscriber(Class<T> clazz);
}
