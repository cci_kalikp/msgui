package com.ms.msdotnet.msdesktop.standalone.m2m;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.PropertyNamingStrategyBase;

public class PascalCasePropertyNamingStrategy extends PropertyNamingStrategyBase {

    /**
     * 
     */
    private static final long serialVersionUID = 1512784478749167620L;

    @Override
    public String translate(String propertyName) {
        return Character.toUpperCase(propertyName.charAt(0)) + propertyName.substring(1);
    }

}
