package com.ms.msdotnet.msdesktop.interfaces.kraken;

public enum DeliveryStatus {
    Pending, WaitingForAck, Delivered, Failed, Rejected, EndpointNotResolved
}
