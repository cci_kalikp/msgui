package com.ms.msdotnet.msdesktop.wormhole.processors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.ms.msdotnet.msdesktop.wormhole.MSDesktopMessageProcessor;
import com.ms.msdotnet.msdesktop.wormhole.MSDesktopShellHandler;

public class CloseWindowProcessor implements MSDesktopMessageProcessor {

private static Logger logger = LoggerFactory.getLogger(CloseWindowProcessor.class); 
    
    private MSDesktopShellHandler handler;
    
    public CloseWindowProcessor(MSDesktopShellHandler handler) {
        super();
        this.handler = handler;
    }
    
    @Override
    public boolean processMessage(Document message) {
        Node closeWindowNode = message.getElementsByTagName("CloseWindow").item(0);        
        if (closeWindowNode != null) {
            logger.info("Received CloseWindow");
            String windowId = closeWindowNode.getAttributes().getNamedItem("windowID").getTextContent();
            handler.closeWindow(windowId);            
            return true;
        }
        return false;
    }

}
