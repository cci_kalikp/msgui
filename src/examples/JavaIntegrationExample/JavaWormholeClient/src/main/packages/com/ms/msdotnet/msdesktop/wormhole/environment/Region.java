package com.ms.msdotnet.msdesktop.wormhole.environment;

public enum Region {
    N_A, LN, EU, NY, HK, TK
}
