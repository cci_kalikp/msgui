package com.ms.msdotnet.msdesktop.interfaces.m2m;

public interface ModulePublisher<T> {
    void publish(T message);
}
