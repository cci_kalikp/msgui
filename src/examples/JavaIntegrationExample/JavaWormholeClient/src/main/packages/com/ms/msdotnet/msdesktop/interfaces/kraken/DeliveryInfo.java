package com.ms.msdotnet.msdesktop.interfaces.kraken;

public class DeliveryInfo {
    private Endpoint target;
    private String id;
    private DeliveryStatus status;
    public Endpoint getTarget() {
        return target;
    }
    public void setTarget(Endpoint target) {
        this.target = target;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public DeliveryStatus getStatus() {
        return status;
    }
    public void setStatus(DeliveryStatus status) {
        this.status = status;
    }
}
