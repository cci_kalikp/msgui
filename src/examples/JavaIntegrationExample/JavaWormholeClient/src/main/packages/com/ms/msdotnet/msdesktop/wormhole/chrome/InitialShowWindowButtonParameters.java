package com.ms.msdotnet.msdesktop.wormhole.chrome;

public class InitialShowWindowButtonParameters extends InitialWidgetParameters {

    private String windowFactoryId;
    private String imagePath;
    private InitialWindowParameters initialParameters;
    
    public InitialShowWindowButtonParameters() {
        initialParameters = new InitialWindowParameters();
    }
    
    @Override
    public String getDotNetType() {
        return "MorganStanley.MSDotNet.MSGui.Core.ChromeManager.InitialShowWindowButtonParameters, MSDotNet.MSGui.Core";
    }
    
    public String getWindowFactoryId() {
        return windowFactoryId;
    }

    public void setWindowFactoryId(String windowFactoryId) {
        this.windowFactoryId = windowFactoryId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public InitialWindowParameters getInitialParameters() {
        return initialParameters;
    }

    public void setInitialParameters(InitialWindowParameters initialParameters) {
        this.initialParameters = initialParameters;
    }

}
