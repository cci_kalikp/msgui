package com.ms.msdotnet.msdesktop.wormhole;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ms.msdotnet.msdesktop.wormhole.chrome.InitialWidgetParameters;
import com.ms.msdotnet.msdesktop.wormhole.processors.CloseWindowProcessor;
import com.ms.msdotnet.msdesktop.wormhole.processors.CreateWindowProcessor;
import com.ms.msdotnet.msdesktop.wormhole.processors.EnvironmentProcessor;
import com.ms.msdotnet.msdesktop.wormhole.processors.GetWindowProcessor;
import com.ms.msdotnet.msdesktop.wormhole.processors.ShutdownProcessor;

public class WormholeClient {
    
    private static Logger logger = LoggerFactory.getLogger(WormholeClient.class); 
	public static final String MSDesktopPortNumberVariable = "MSDesktopPortNumber";
    private Socket msDesktopSocket;
    private List<MSDesktopMessageProcessor> messageProcessors;
    private MSDesktopShellHandler handler;
    private ObjectMapper mapper;
       
    final private int portNumber;
    
    public WormholeClient(int portNumber, MSDesktopShellHandler handler) {
        this.portNumber = portNumber;
        this.handler = handler;
        this.mapper = new ObjectMapper();
        this.messageProcessors = new LinkedList<MSDesktopMessageProcessor>();        
        this.messageProcessors.add(new CreateWindowProcessor(handler));
        this.messageProcessors.add(new CloseWindowProcessor(handler));
        this.messageProcessors.add(new EnvironmentProcessor(handler));
        this.messageProcessors.add(new GetWindowProcessor(handler));
        this.messageProcessors.add(new ShutdownProcessor(this));
        handler.setClient(this);
    }

    public void Start() {
        try {
            
            while (true) {
                try {
                    msDesktopSocket = new Socket("127.0.0.1", portNumber);
                    break;
                } catch (SocketException e) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e1) { /* swallow */}
                }
            }
            
            BufferedReader in =
                    new BufferedReader(
                        new InputStreamReader(msDesktopSocket.getInputStream()));
            char[] buff = new char[1024];
            int num = 0;
            StringBuilder sb = new StringBuilder();
                        
            handler.initialize();
            
            sendGetEnvironment();
                        
            while ((num = in.read(buff)) > 0) {           
                String messageText = new String(buff, 0, num);
                String[] parts = messageText.split("\0", -1);
                sb.append(parts[0]);
                
                if (parts.length > 1) {
                    processMessage(sb.toString());
                    for (int i = 1; i < parts.length - 1; i++) {
                        processMessage(parts[i]);
                    }
                    sb = new StringBuilder(parts[parts.length - 1]);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException( e );
        } 
    }
            
    public void shutdown() {
        try {
            msDesktopSocket.close();
            System.exit(0);
        } catch (IOException e) {
            // swallow
        }
    }
    
    public void sendCaptureWindow(String windowId, String desiredTitle) {
        sendCommand("<CaptureWindow windowID=\""+windowId+"\" desiredTitle=\""+desiredTitle+"\" />");
    }
    
    public void sendWindowState(String windowId, String requestId, String state) {
        sendCommand("<WindowState windowID=\""+windowId+"\" requestID=\""+requestId+"\">"+state+"</WindowState>");                           
    }
    
    public void sendGetEnvironment() {
        sendCommand("<GetEnvironment/>");
    }

    public void sendRegisterWindowFactory(String factoryId) {
        sendCommand("<RegisterWindowFactory factoryID=\""+factoryId+"\" />");
    }
    
    public void sendPlaceWidget(String location, String widgetId, InitialWidgetParameters parameters) {
        try {
            sendCommand("<PlaceWidget location=\""+location+"\"" +  
			((widgetId == null || widgetId.length() == 0) ? "" : (" widgetId=\"" + widgetId + "\"")) +
			" type=\""+parameters.getDotNetType()+"\">"+mapper.writeValueAsString(parameters)+"</PlaceWidget>");
        } catch (JsonGenerationException e) {
            throw new RuntimeException( e );
        } catch (JsonMappingException e) {
            throw new RuntimeException( e );
        } catch (IOException e) {
            throw new RuntimeException( e );
        }
    }

    public void sendCommand(String text) {
        OutputStreamWriter writer;
        try {
            writer = new OutputStreamWriter(msDesktopSocket.getOutputStream());
            writer.write(text);
            writer.write('\0');
            writer.flush();         
        } catch (IOException e) {
            throw new RuntimeException( e );
        } 
    }
    
    public void registerMessageProcessor(MSDesktopMessageProcessor processor) {
        messageProcessors.add(processor);
    }
    
    private void processMessage(String messageText) {
        try {
            DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
            DocumentBuilder db1 = dbf1.newDocumentBuilder();
            Document doc1 = db1.parse(new ByteArrayInputStream(messageText.getBytes()));
            
            boolean processed = false;
            for (MSDesktopMessageProcessor processor : messageProcessors) {
                if (processor.processMessage(doc1)) {
                    processed = true;
                    break;
                }
            }
            
            if (!processed) {
                logger.warn("Unrecognized message: " + messageText);
            }
            
        } catch (SAXException e) {
            throw new RuntimeException(e);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
