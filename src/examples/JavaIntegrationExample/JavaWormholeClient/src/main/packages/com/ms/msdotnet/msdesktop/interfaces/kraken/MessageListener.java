package com.ms.msdotnet.msdesktop.interfaces.kraken;

public interface MessageListener<T> {
    void receive(T message);
}
