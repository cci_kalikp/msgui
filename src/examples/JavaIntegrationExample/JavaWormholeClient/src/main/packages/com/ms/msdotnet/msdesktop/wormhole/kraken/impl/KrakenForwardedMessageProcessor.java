package com.ms.msdotnet.msdesktop.wormhole.kraken.impl;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.ms.msdotnet.msdesktop.wormhole.MSDesktopMessageProcessor;

public class KrakenForwardedMessageProcessor implements MSDesktopMessageProcessor {

    private RoutingCommunicatorProxy communicator;
    
    public KrakenForwardedMessageProcessor(RoutingCommunicatorProxy communicator) {
        this.communicator = communicator;
    }
    
    @Override
    public boolean processMessage(Document message) {
        Node krakenMessageNode = message.getElementsByTagName("KrakenForwardedMessage").item(0);        
        if (krakenMessageNode != null) {
            String subscriberId = krakenMessageNode.getAttributes().getNamedItem("subscriberId").getTextContent();
            String type = krakenMessageNode.getAttributes().getNamedItem("type").getTextContent();
            String content = krakenMessageNode.getChildNodes().item(0).getTextContent();
            communicator.forwardSubscribedMessage(subscriberId, content, type);
            return true;
        }
        return false;
    }

}
