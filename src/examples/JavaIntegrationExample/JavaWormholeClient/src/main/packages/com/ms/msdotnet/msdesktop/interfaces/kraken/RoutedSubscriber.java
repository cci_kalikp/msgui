package com.ms.msdotnet.msdesktop.interfaces.kraken;

public interface RoutedSubscriber<T> extends AutoCloseable {
    void subscribe(MessageListener<T> listener);
}
