package com.ms.msdotnet.msdesktop.wormhole;

import org.w3c.dom.Document;

public interface MSDesktopMessageProcessor {
    boolean processMessage(Document message);
}
