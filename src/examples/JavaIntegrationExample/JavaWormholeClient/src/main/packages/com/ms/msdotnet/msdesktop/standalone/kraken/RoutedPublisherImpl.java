package com.ms.msdotnet.msdesktop.standalone.kraken;

import java.util.concurrent.Future;

import com.ms.msdotnet.msdesktop.interfaces.kraken.DeliveryInfo;
import com.ms.msdotnet.msdesktop.interfaces.kraken.RoutedPublisher;

public class RoutedPublisherImpl<T> implements RoutedPublisher<T> {

    private RoutingCommunicatorImpl communicator;
    
    public RoutedPublisherImpl(RoutingCommunicatorImpl communicator) {
        this.communicator = communicator;
    }
    
    @Override
    public DeliveryInfo publish(T message) {
        return communicator.publish(message);
    }

    @Override
    public Future<DeliveryInfo> publishAsync(T message) {
        return communicator.publishAsync(message);
    }

}
