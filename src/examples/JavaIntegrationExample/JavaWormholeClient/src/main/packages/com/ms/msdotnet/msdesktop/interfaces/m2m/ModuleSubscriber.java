package com.ms.msdotnet.msdesktop.interfaces.m2m;

import com.ms.msdotnet.msdesktop.interfaces.kraken.MessageListener;

public interface ModuleSubscriber<T> extends AutoCloseable {
    public void addListener(MessageListener<T> listener);
    
    public void removeListener(MessageListener<T> listener);
}
