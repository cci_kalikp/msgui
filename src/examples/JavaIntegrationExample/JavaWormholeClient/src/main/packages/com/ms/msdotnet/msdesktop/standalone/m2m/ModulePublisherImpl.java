package com.ms.msdotnet.msdesktop.standalone.m2m;

import com.ms.msdotnet.msdesktop.interfaces.m2m.ModulePublisher;

public class ModulePublisherImpl<T> implements ModulePublisher<T> {

    private CommunicatorImpl communicator;
    
    public ModulePublisherImpl(CommunicatorImpl communicator) {
        this.communicator = communicator;
    }
    
    @Override
    public void publish(T message) {
        communicator.publish(message);
    }

}
