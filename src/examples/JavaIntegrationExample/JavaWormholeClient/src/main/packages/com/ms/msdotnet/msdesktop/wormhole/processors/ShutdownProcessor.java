package com.ms.msdotnet.msdesktop.wormhole.processors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.ms.msdotnet.msdesktop.wormhole.MSDesktopMessageProcessor;
import com.ms.msdotnet.msdesktop.wormhole.WormholeClient;

public class ShutdownProcessor implements MSDesktopMessageProcessor {

    private static Logger logger = LoggerFactory.getLogger(ShutdownProcessor.class); 
    
    private WormholeClient client;
    
    public ShutdownProcessor(WormholeClient client) {
        super();
        this.client = client;
    }
    
    @Override
    public boolean processMessage(Document message) {
        Node shutdownNode = message.getElementsByTagName("Shutdown").item(0);
        if (shutdownNode != null) {
            logger.info("Received Shutdown");
            client.shutdown();
            return true;
        }
        return false;
    }

}
