package com.ms.msdotnet.msdesktop.standalone.m2m;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.ms.msdotnet.msdesktop.interfaces.kraken.FailureAwareMessageListener;
import com.ms.msdotnet.msdesktop.interfaces.kraken.MessageListener;
import com.ms.msdotnet.msdesktop.interfaces.m2m.ModuleSubscriber;

public class ModuleSubscriberImpl<T> implements ModuleSubscriber<T> {
    private final ConcurrentLinkedQueue<FailureAwareMessageListener<T>> listeners;
    private final Class<T> type;
    private final CommunicatorImpl communicator;
    
    public ModuleSubscriberImpl(CommunicatorImpl communicator, Class<T> type) {
        listeners = new ConcurrentLinkedQueue<FailureAwareMessageListener<T>>();
        this.type = type;
        this.communicator = communicator;
    }
    
    public void addListener(MessageListener<T> listener) {
        listeners.add(new LogOnFailureMessageListener<>(listener));
    }
    
    public void removeListener(MessageListener<T> listener) {
        listeners.remove(new LogOnFailureMessageListener<>(listener));
    }
    
    public void addListener(FailureAwareMessageListener<T> listener) {
        listeners.add(listener);
    }
    
    public void removeListener(FailureAwareMessageListener<T> listener) {
        listeners.remove(listener);
    }
    
    void onFailure(Throwable t) {
        for (FailureAwareMessageListener<T> listener : listeners) {
            listener.onFailure(t);
        }
    }
    
    @SuppressWarnings("unchecked")
    void receive(Object result) {
        for (FailureAwareMessageListener<T> listener : listeners) {
            listener.receive((T)result);
        }
    }

    @Override
    public void close() throws Exception {
        communicator.removeSubscriber(this, type);
    }
}
