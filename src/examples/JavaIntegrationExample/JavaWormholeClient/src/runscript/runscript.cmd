@echo off
set CLASSPATH=
call \\ms\dist\winaurora\PROJ\compat\incr\bin\useafs.cmd

@SETENV_USE64BITJVM_WINDOWS@
call module load @jvm.module@
@MSJAVA_PATCHMODULELOAD_WIN@

@MODULE_LOADS_WIN@
set RUNTIME_CLASSPATH=@ivy.artifacts@
set RUNTIME_CLASSPATH=%RUNTIME_CLASSPATH%;%~dp0\..\lib\patching.jar
set RUNTIME_CLASSPATH="%RUNTIME_CLASSPATH%"

set JVM_ARGS=@jvm.options@

set MAIN_CLASS=@jvm.mainclass@

set MAIN_CLASS_ARGS=@jvm.mainclass.args@

set MSDE_IVY_ENDORSED_DIRS=@endorsed.dirs@

set MSDE_JNI_PATH=@MSDE_JNI_PATH_WIN@

set MSDE_LAUNCH_WRAPPER=@launchwrapper@

:: Run the program
echo %MSDE_LAUNCH_WRAPPER% java %JVM_ARGS% -Djava.endorsed.dirs=%MSDE_IVY_ENDORSED_DIRS% -Djava.library.path=%MSDE_JNI_PATH% -cp %RUNTIME_CLASSPATH% %MAIN_CLASS% %MAIN_CLASS_ARGS%
%MSDE_LAUNCH_WRAPPER% java %JVM_ARGS% -Djava.endorsed.dirs=%MSDE_IVY_ENDORSED_DIRS% -Djava.library.path=%MSDE_JNI_PATH% -cp %RUNTIME_CLASSPATH% %MAIN_CLASS% %MAIN_CLASS_ARGS%
