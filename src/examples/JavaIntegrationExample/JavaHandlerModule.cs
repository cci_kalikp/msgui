﻿using System.Diagnostics;
using MSDesktop.Wormhole;
using MSDesktop.Wormhole.Communication.Tcp;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace JavaIntegrationExample
{
    class JavaHandlerModule : IModule
    {
        private readonly IChromeRegistry _chromeRegistry;
        private readonly IChromeManager _chromeManager;
        private readonly IApplication _application;
        private readonly IUnityContainer _container;

        //private const string JvmDebugArgs = "-Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=4000,suspend=y";
        private const string JavaWindowFactoryId = "wormholeTest";
        private const int PortNumber = 10090;

        public JavaHandlerModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, IApplication application_, IUnityContainer container_)
        {
            _chromeRegistry = chromeRegistry_;
            _chromeManager = chromeManager_;
            _application = application_;
            _container = container_;
        }

        public void Initialize()
        {
            var transport = new WormholeTcpServer(PortNumber);
            var javaApplication = new WormholeBridgedApplication(
                () => Process.Start("bin\\run-prod.cmd"),
                transport,
                _chromeRegistry,
                _chromeManager,
                _container);
            javaApplication.RegisterWindowFactory(JavaWindowFactoryId);
            javaApplication.Start();

            _chromeManager.Ribbon["Java"]["Java"].AddWidget(new InitialButtonParameters
            {
                Text = "Open Java window",
                Click = (sender1_, args1_) => javaApplication.CreateWindow(JavaWindowFactoryId, null)
            });

            _application.ApplicationClosed += (sender_, args_) => transport.Stop();
        }
    }
}
