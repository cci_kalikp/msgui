﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using MSDesktop.Wormhole;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Win32;

namespace JavaIntegrationExample
{
    class NotepadModule : IModule
    {
        private readonly IChromeManager manager;
        private readonly IChromeRegistry registry;
        private readonly IUnityContainer container;

        public NotepadModule(IChromeManager manager, IChromeRegistry registry, IUnityContainer container)
        {
            this.manager = manager;
            this.registry = registry;
            this.container = container;
        }

        public void Initialize()
        {
            var application = new WormholeBridgedApplication(registry, manager, container);
            application.RegisterWindowFactory("notepad");
            
            manager.Ribbon["Wormhole"]["Applications"].AddWidget(
                new InitialShowWindowButtonParameters
                    {
                        Text = "Notepad",
                        WindowFactoryID = "notepad",
                        InitialParameters = new InitialProcessBoundWindowParameters
                            {
                                ProcessExe = @"notepad.exe"
                            }
                    });
            manager.Ribbon["Wormhole"]["Applications"].AddWidget(
                new InitialShowWindowButtonParameters
                {
                    Text = "Calculator",
                    WindowFactoryID = "notepad",
                    InitialParameters = new InitialProcessBoundWindowParameters
                    {
                        ProcessExe = @"calc.exe"
                    }
                });
            manager.Ribbon["Wormhole"]["Applications"].AddWidget(
                new InitialShowWindowButtonParameters
                {
                    Text = "D1GUI",
                    WindowFactoryID = "notepad",
                    InitialParameters = new InitialProcessBoundWindowParameters
                    {
                        ProcessExe = @"\\san01b\DevAppsGML\dist\amg\PROJ\V1\2014.06.27-555\bin\run-v1gui.cmd",
                        ProcessArgs = @"config/gui/panthera/tk/qa/panthera-mktintel.properties",
                        WindowTitleSubstring = "D1 RESEARCH",
                        StartupTimeoutInSeconds = 1000,
                    }
                });
        }
    }
}
