﻿using System;
using System.Windows;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;
using MorganStanley.MSDotNet.MSGui.Themes.MSDesktop2014;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.Message;
using MessageRoutingExampleLib.Messages;
using MessageRoutingExampleOne.Modules;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Themes;

namespace MessageRoutingExampleOne
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private const string APPLICATIONAME = "MSDesktop Application";

        protected override void OnStartup(StartupEventArgs e_)
        {
            base.OnStartup(e_);

            var msdesktop = new Framework();

            msdesktop.SetShellMode(ShellMode.RibbonWindow);
            msdesktop.AddModernTheme(Colors.RoyalBlue, ModernThemeBase.Dark, new ModernThemeOptions
            {
                UseAccentColorOnStatusBar = false
            });

            msdesktop.EnableChromeReordering();

            //msdesktop.DisableSplashScreen();

            msdesktop.SetApplicationName(APPLICATIONAME);
            msdesktop.SetApplicationVersion("0.1.0.0");
            msdesktop.EnableExceptionHandler("msguiadministrators@ms.com");

            msdesktop.EnableLogging(APPLICATIONAME);

            //set trackInternalMessageEvents_ to true to allow tracking the internal message in kraken history and kraken monitor 
            msdesktop.EnableMessageRoutingCommunication(true, trackInternalMessageEvents_: true);
            msdesktop.EnableMessageRoutingDefaultGui(null, 5);
            msdesktop.EnableMessageRoutingHistory();
            msdesktop.EnableMessageRoutingHistoryGui();
            msdesktop.EnableMessageRoutingMonitor();

            //set up the message info separately
            MessageInfoExtensions.SetMessageInfo<Bar>("Bar Message");
            MessageInfoExtensions.SetMessageInfoGetter(new Func<Test,string>(t_=> t_.ToString()));

            msdesktop.EnableTypeaheadDropdownOptionsSearch();
            msdesktop.AddModule<MSDesktopApplicationModule>();

            msdesktop.EnablePrintSupport();
            //msdesktop.EnableParallelBasedEventing();
            msdesktop.AcquireFocusAfterStartup();
            
            msdesktop.Start();
        }
    }
}
