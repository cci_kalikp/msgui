﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;
using DemoOne; 
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.Routing;

namespace MessageRoutingExampleOne.Modules
{
    public class MSDesktopApplicationModule : IModule
    {
        public const string BlueWindowFactoryID = "BlueWindow";
        public const string YellowWindowFactoryID = "YellowWindow";
        public const string RedWindowFactoryID = "RedWindow";
        public const string GreenWindowFactoryID = "GreenWindow";
        public const string ExplicitCPSWindowFactoryID = "ExplicitCPSWindowFactoryID";

        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;
        private readonly IRoutingCommunicator _routingCommunicator;
        private readonly LayoutScreenshotProvider screenshotProvider;

        public MSDesktopApplicationModule(IChromeRegistry chromeRegistry, IChromeManager chromeManager, IRoutingCommunicator routingCommunicator, LayoutScreenshotProvider screenshotProvider)
        {
            _chromeRegistry = chromeRegistry;
            _chromeManager = chromeManager;
            _routingCommunicator = routingCommunicator;
            this.screenshotProvider = screenshotProvider;
        }

        public void Initialize()
        {
            _chromeRegistry.RegisterWindowFactory(BlueWindowFactoryID, BlueWindowFactory);
            _chromeRegistry.RegisterWindowFactory(YellowWindowFactoryID, YellowWindowFactory);
            _chromeRegistry.RegisterWindowFactory(RedWindowFactoryID, RedWindowFactory);
            _chromeRegistry.RegisterWindowFactory(GreenWindowFactoryID, GreenWindowFactory);
            _chromeRegistry.RegisterWindowFactory(ExplicitCPSWindowFactoryID, ExplicitCpsWindowFactory);
            var orr = _routingCommunicator.GetPublisher<Fika>(); 

            //_chromeManager.Ribbon["fuckup"]["exception"].AddWidget("sdfargvaefaA3efc", new InitialButtonParameters
            //    {
            //        Text = "Die.",
            //        Click = (sender, args) => orr.Publish(new Fika {I = 32})
            //    });

            _chromeManager.Ribbon["Demo"]["explicit cps"].AddWidget("ExplicitCps",
                                                                      new InitialShowWindowButtonParameters()
                                                                          {
                                                                              Text =  "Send Message to target cps server specified",
                                                                              WindowFactoryID = ExplicitCPSWindowFactoryID,
                                                                              InitialParameters = new InitialWindowParameters()
                                                                                  {
                                                                                      InitialLocation = InitialLocation.DockInNewTab
                                                                                  }
                                                                          });


            _chromeManager[ChromeArea.Ribbon]["Demo"]["Demo window"].AddWidget(new InitialShowWindowButtonParameters
                {
                    Text = "Open blue window",
                    WindowFactoryID = BlueWindowFactoryID,
                    InitialParameters = new InitialWindowParameters()
                        {
                            InitialLocation = InitialLocation.DockInNewTab
                        }
                });

            _chromeManager[ChromeArea.Ribbon]["Demo"]["Demo window"].AddWidget(new InitialShowWindowButtonParameters
                {
                    Text = "Open yellow window",
                    WindowFactoryID = YellowWindowFactoryID,
                    InitialParameters = new InitialWindowParameters()
                        {
                            InitialLocation = InitialLocation.DockInNewTab
                        }
                });

            _chromeManager[ChromeArea.Ribbon]["Demo"]["Demo window"].AddWidget(new InitialShowWindowButtonParameters
                {
                    Text = "Open red window",
                    WindowFactoryID = RedWindowFactoryID,
                    InitialParameters = new InitialWindowParameters()
                        {
                            InitialLocation = InitialLocation.DockInNewTab
                        }
                });

            _chromeManager[ChromeArea.Ribbon]["Demo"]["Demo window"].AddWidget(new InitialShowWindowButtonParameters
                {
                    Text = "Open green window",
                    WindowFactoryID = GreenWindowFactoryID,
                    InitialParameters = new InitialWindowParameters()
                        {
                            InitialLocation = InitialLocation.DockInNewTab
                        }
                });


            _chromeManager[ChromeArea.Ribbon]["Screenshot"]["Test"].AddWidget(new InitialButtonParameters
            {
                Text = "Capture",
                Click = (sender, args) =>
                {
                    this.screenshotProvider.CaptureCurrentLayout();
                    MessageBox.Show("Capture completed.");
                }
            });
        }

        private bool ExplicitCpsWindowFactory(IWindowViewContainer viewContainer, XDocument state)
        {
            viewContainer.Title = "Send Message to target cps server specified";
            viewContainer.Content = new DemoWindow2Content(_routingCommunicator);
            return true;
        }

        private bool BlueWindowFactory(IWindowViewContainer viewContainer, XDocument state)
        {
            viewContainer.Title = "Blue";
            viewContainer.Content = new DemoWindowContent(_routingCommunicator, Colors.Blue);
            return true;
        }

        private bool YellowWindowFactory(IWindowViewContainer viewContainer, XDocument state)
        {
            viewContainer.Title = "Yellow";
            viewContainer.Content = new DemoWindowContent(_routingCommunicator, Colors.Yellow);
            return true;
        }

        private bool RedWindowFactory(IWindowViewContainer viewContainer, XDocument state)
        {
            viewContainer.Title = "Red";
            viewContainer.Content = new DemoWindowContent(_routingCommunicator, Colors.Red);
            return true;
        }

        private bool GreenWindowFactory(IWindowViewContainer viewContainer, XDocument state)
        {
            viewContainer.Title = "Green";
            viewContainer.Content = new DemoWindowContent(_routingCommunicator, Colors.Green);
            return true;
        }
    }

    [Serializable]
    [DeliveryStrategy(DeliveryStrategy = DeliveryStrategy.FireAndForget)]
    public class Fika : IExplicitTarget
    {
        public int I { get; set; }

        public Endpoint Target
        {
            get
            {
                return new Endpoint
                {
                    Application = "MSDesktop Application",
                    Hostname = "YO074036",
                    Username = "istvanf"
                };
            }
        }
    }
}
