﻿using System;
using System.Windows;
using System.Windows.Media;
using MessageRoutingExampleLib.Messages;
using MSDesktop.MessageRouting;

namespace DemoOne
{
    public partial class DemoWindowContent
    {
        private readonly IRoutingCommunicator _routingCommunicator;
        private readonly IRoutedPublisher<Bar> _barPublisher;
        private readonly IRoutedPublisher<Foo> _fooPublisher;
        private readonly IRoutedPublisher<Test> _testPublisher;
        private int _barCounter;
        private int _fooCounter;
        private int _testCounter;

        public DemoWindowContent(IRoutingCommunicator routingCommunicator, Color color)
        {
            InitializeComponent();
            _routingCommunicator = routingCommunicator;

            _routingCommunicator.GetSubscriber<Bar>().GetObservable().Subscribe(BarMessageArrived);
            _routingCommunicator.GetSubscriber<Foo>().GetObservable().Subscribe(FooMessageArrived);

            _barPublisher = _routingCommunicator.GetPublisher<Bar>();
            _barPublisher.MessageDelivered += BarPublisher_MessageDelivered;
            _barPublisher.MessageFailed += BarPublisher_MessageFailed;

            _fooPublisher = _routingCommunicator.GetPublisher<Foo>();
            _fooPublisher.MessageDelivered += FooPublisher_MessageDelivered;
            _fooPublisher.MessageFailed += FooPublisher_MessageFailed;

            _testPublisher = _routingCommunicator.GetPublisher<Test>();
            _testPublisher.MessageDelivered += TestPublisher_MessageDelivered;
            _testPublisher.MessageFailed += TestPublisher_MessageFailed;

            Header.Background = new SolidColorBrush(color);
        }

        private void Bar_Click(object sender, RoutedEventArgs e)
        {
            var bar = new Bar
                {
                    Action = "BarAction",
                    Message = "Bar " + (++_barCounter)
                };

            _barPublisher.Publish(bar);
        }

        private void Foo_Click(object sender, RoutedEventArgs e)
        {
            //for (int i = 0; i < 1000; i++)
            //{
            var foo = new Foo
                {
                    Action = "FooAction",
                    Message = "Foo " + (++_fooCounter)
                };

            _fooPublisher.Publish(foo);
            //}
        }

        private void Test_Click(object sender, RoutedEventArgs e)
        {
            var test = new Test
                {
                    Action = "TestAction",
                    Message = "Test " + (++_testCounter)
                };

            _testPublisher.Publish(test);
        }

        private void BarPublisher_MessageDelivered(object sender, MessageDeliveredEventArgs e)
        {
            Action invoke = () =>
                {
                    ResultListBox.Items.Insert(0, "DELIVERED: " + ((Bar) e.Message).Message);
                };

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

        private void FooPublisher_MessageDelivered(object sender, MessageDeliveredEventArgs e)
        {
            Action invoke = () =>
                {
                    ResultListBox.Items.Insert(0, "DELIVERED: " + ((Foo) e.Message).Message);
                };

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

        private void TestPublisher_MessageDelivered(object sender, MessageDeliveredEventArgs e)
        {
            Action invoke = () =>
                {
                    ResultListBox.Items.Insert(0, "DELIVERED: " + ((Test) e.Message).Message);
                };

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

        private void BarPublisher_MessageFailed(object sender, MessageFailedEventArgs e)
        {
            Action invoke = () =>
                {
                    ResultListBox.Items.Insert(0, "FAILED: " + ((Bar) e.Message).Message);
                };

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

        private void FooPublisher_MessageFailed(object sender, MessageFailedEventArgs e)
        {
            Action invoke = () =>
                {
                    ResultListBox.Items.Insert(0, "FAILED: " + ((Foo) e.Message).Message);
                };

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

        private void TestPublisher_MessageFailed(object sender, MessageFailedEventArgs e)
        {
            Action invoke = () =>
                {
                    ResultListBox.Items.Insert(0, "FAILED: " + ((Test) e.Message).Message);
                };

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

        private void BarMessageArrived(Bar bar)
        {
            Action invoke = () =>
                {
                    ResultListBox.Items.Insert(0, "ARRIVED: " + bar.Message + " action: " + bar.Action);
                };

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

        private void FooMessageArrived(Foo foo)
        {
            Action invoke = () =>
                {
                    ResultListBox.Items.Insert(0, "ARRIVED: " + foo.Message + " action: " + foo.Action);
                };

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }
    }
}
