﻿using System;
using System.Windows;
using System.Windows.Media;
using MessageRoutingExampleLib.Messages;
using MSDesktop.MessageRouting;
using MessageRoutingExampleOne.Modules;

namespace DemoOne
{
    public partial class DemoWindow2Content
    {
        private readonly IRoutingCommunicator _routingCommunicator;
        private readonly IRoutedPublisher<DemoTwoMessage> _publisher1;
        private readonly IRoutedPublisher<DemoThreeMessage> _publisher2; 
        private int _counter1;
        private int _counter2; 

        public DemoWindow2Content(IRoutingCommunicator routingCommunicator)
        {
            InitializeComponent();
            _routingCommunicator = routingCommunicator;
             

            _publisher1 = _routingCommunicator.GetPublisher<DemoTwoMessage>();
            _publisher1.MessageDelivered += Publisher_MessageDelivered;
            _publisher1.MessageFailed += Publisher_MessageFailed;

            _publisher2 = _routingCommunicator.GetPublisher<DemoThreeMessage>();
            _publisher2.MessageDelivered += Publisher_MessageDelivered;
            _publisher2.MessageFailed += Publisher_MessageFailed;
             
        }

        private void DemoTwoMessage_Click(object sender, RoutedEventArgs e)
        {
            var message = new DemoTwoMessage()
                {
                    HostName = txtHostName1.Text.Trim(),
                    CPSServer = txtCpsServer1.Text.Trim(),
                    I = ++_counter1
                };
            _publisher1.Publish(message);
        }

        private void DemoThreeMessage_Click(object sender, RoutedEventArgs e)
        {
            var message = new DemoThreeMessage()
            {
                HostName = txtHostName2.Text.Trim(),
                CPSServer = txtCpsServer2.Text.Trim(),
                I = ++_counter2
            };
            _publisher2.Publish(message);
        }

         

        private void Publisher_MessageDelivered(object sender, MessageDeliveredEventArgs e)
        {
            Action invoke = () => ResultListBox.Items.Insert(0, "DELIVERED: " + e.Message);

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

   
        private void Publisher_MessageFailed(object sender, MessageFailedEventArgs e)
        {
            Action invoke = () => ResultListBox.Items.Insert(0, "FAILED: " + e.Message);

            Application.Current.Dispatcher.BeginInvoke(invoke);
        }

       
    }
}
