﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Xml.Linq;
using MSDesktop.Wormhole;
using MSDesktop.Wormhole.Communication.Tcp;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Modules.Air;

namespace MorganStanley.Msdotnet.Msgui.Air
{
    public class AirIntegrationExampleModule : IModule
    {
        public static Dispatcher StaticDispatcher;
        private const string m_viewKey = "AirIntegrationExampleModule";
        private const string m_socketKey = "AirSocketViewer";
        private readonly IChromeRegistry chromeRegistry;
        private IChromeManager chromeManager;
        private Process process;
        private WormholeBridgedApplication matrix;
        private String[] args;
        private IViewManager m_viewManager;
        private IModuleSubscriber<XDocument> m_xmlSubscriber;
        private readonly ICommunicator m_communicator;
        private readonly IUnityContainer _container;
        private readonly IEventRegistrator m_eventRegistrator;
        private readonly string appParams;

        public AirIntegrationExampleModule(IChromeRegistry chromeRegistry, IChromeManager chromeManager, IViewManager viewManager_, IEventRegistrator eventRegistrator_, ICommunicator communicator_, IUnityContainer container_)
        {
            this.m_eventRegistrator = eventRegistrator_;
            this.m_communicator = communicator_;
            _container = container_;
            this.m_viewManager = viewManager_;
            this.chromeRegistry = chromeRegistry;
            this.chromeManager = chromeManager;
            int portNumber = 25761;
            args = Environment.GetCommandLineArgs();

            string executable = @"\\san01b\DevAppsGML\dist\ria\PROJ\flexsdk\4.5-ga\bin\adl.exe";

            Console.WriteLine("Running Xenon - will ignore the first arg as an environment flag");
            if (args.Length > 2)
            {
                executable = args[2];
                if (args.Length > 3)
                {
                    args = (string[])args.Skip(3).ToArray();
                }
                else args = new string[] { };
            }
            else
            {
                args = new string[] { Environment.CurrentDirectory + @"\AirTestTwo-app.xml" };
            }

            appParams = String.Join(" ", args);
            Console.WriteLine("Args to run  {0}", appParams);


            string port = Environment.GetEnvironmentVariable("AIRPORT");
            if (port != null)
            {
                Console.WriteLine("WARNING: You've specified AIRPORT so that's the port I'm going to use!");
                try
                {
                    portNumber = int.Parse(port);
                }
                catch
                {
                    Console.WriteLine("ERROR: %AIRPORT% is not a valid integer for port(" + port + ") using default: " + portNumber);
                }
            }



            Console.WriteLine("Using executable: {0}", executable);


            //@"\\ms\dev\f16\client-deploy\Core\install\common\air\MatrixAir.exe"
            matrix = new WormholeBridgedApplication(() => Process.Start(executable, appParams), new WormholeTcpServer(portNumber), chromeRegistry, chromeManager, container_);


            string factories = Environment.GetEnvironmentVariable("AIRFACTORIES");
            if (factories != null && factories.Trim().Length > 0)
            {
                Console.WriteLine("Registering supplied AIRFACTORIES env variable: " + factories);
                foreach (String fact in factories.Split(','))
                {
                    matrix.RegisterWindowFactory(fact);
                }
            }
            else
            {
                Console.WriteLine("Only Registering default factories since AIRFACTORIES env var not specified");

            }
            InitializeSubscription();
            matrix.RegisterWindowFactory("FirstAirWindow");
            matrix.RegisterWindowFactory("SecondAirWindow");
            chromeManager.LayoutLoaded += (x, y) => { StaticDispatcher = Application.Current.MainWindow.Dispatcher; };

            var publisher = communicator_.GetPublisher<XDocument>();

            this.chromeManager.Ribbon["Flex"]["Msg"].AddWidget(new InitialButtonParameters
            {
                Click = (sender, eventArgs) =>
                {
                    var ccy = "USD/GBP";
                    var elem = new XElement("OpenTradeTicket");
                    elem.SetAttributeValue("pair", ccy.Replace("/", ""));
                    publisher.Publish(new XDocument(elem));
                },
                Text = "Open USDGBP"
            });

        }

        private void InitializeSubscription()
        {
            m_xmlSubscriber = m_communicator.GetSubscriber<XDocument>();
            m_xmlSubscriber.GetObservable().Subscribe(
                m_ =>
                {
                    if (m_.Root.Name.LocalName.Equals("OpenTradeTicket"))
                    {
                        XElement entry = new XElement("entry");
                        entry.SetAttributeValue("name", m_.Root.Attributes("pair").FirstOrDefault().Value);
                        XDocument doc = new XDocument(entry);
                        StaticDispatcher.Invoke(
                       new Action(() =>
                          chromeManager.CreateWindow("quotePanel", new InitialAirWindowParameters { InitialState = doc, InitialLocation = InitialLocation.Floating | InitialLocation.PlaceAtCursor })
                           ));
                    }
                    else if (m_.Root.Name.LocalName.Equals("CurrencyPairSelected"))
                    {
                        matrix.IpcBasedApplicationProxy.SendSocketText(m_.Root.ToString());
                    }
                    else if (m_.Root.Name.LocalName.Equals("OpenChart"))
                    {
                        XElement entry = new XElement("entry");
                        entry.SetAttributeValue("name", m_.Root.Attributes("series").FirstOrDefault().Value);
                        XDocument doc = new XDocument(entry);

                        StaticDispatcher.Invoke(
                       new Action(() =>
                          chromeManager.CreateWindow("chartingWidget", new InitialAirWindowParameters { InitialState = doc, InitialLocation = InitialLocation.Floating | InitialLocation.PlaceAtCursor })
                           ));
                    }
                }
                );
        }


        private bool CreateWindowView(IWindowViewContainer viewContainer_, XDocument state_)
        {
            m_viewManager.CreateView(m_viewKey);
            viewContainer_.Title = "Air Console";
            Frame f = new Frame();
            f.Content = new AirPage(matrix, chromeManager, chromeRegistry); ;
            viewContainer_.Content = f;

            return true;

        }

        private bool CreateSocketWindowView(IWindowViewContainer viewContainer_, XDocument state_)
        {
            m_viewManager.CreateView(m_socketKey);
            viewContainer_.Title = "Socket View";
            Frame f = new Frame();
            f.Content = new SocketDebugWindow(matrix); ;
            viewContainer_.Content = f;

            return true;

        }
        ResourceManager resourceMgr = MorganStanley.Msdotnet.Msgui.Properties.Resources.ResourceManager;
        TextBox quickBox;
        private void quickPriceLaunch(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                String ccypair = quickBox.Text;
                XElement elem = new XElement("entry");
                elem.SetAttributeValue("name", ccypair.ToUpper());
                matrix.CreateWindow("quotePanel", new XDocument(elem));
                quickBox.Text = "";
            }
            if (e.Key == Key.Escape)
            {
                quickBox.Text = "";
            }

        }
        public void Initialize()
        {
            quickBox = new TextBox();
            quickBox.Width = 60;
            quickBox.MaxLines = 1;
            quickBox.MaxLength = 6;
            quickBox.KeyDown += this.quickPriceLaunch;

            this.chromeRegistry.RegisterWidgetFactory("quickBox",
                (widgetContainer, state) => { widgetContainer.Content = quickBox; return true; });
            this.chromeRegistry.RegisterWindowFactory(m_viewKey, CreateWindowView);

            this.chromeRegistry.RegisterWindowFactory(m_socketKey, CreateSocketWindowView);
            IWidgetViewContainer container = null;
            if (Environment.GetCommandLineArgs()[0].Contains("Xenon"))
            {
                container = this.chromeManager[ChromeArea.LauncherBar]; ;
            }
            else container = this.chromeManager[ChromeArea.Ribbon]["Flex"]["HostedProcess"];


            container.AddWidget(new InitialShowWindowButtonParameters()
            {
                Text = "Open AIR Console",
                Enabled = true,
                ToolTip = "Open Browser Based Flex Window",
                WindowFactoryID = m_viewKey,
                Image = GetImage("AdobeAir32"),
                InitialParameters = new InitialWindowParameters() { Singleton = true },
            });


            container.AddWidget(new InitialShowWindowButtonParameters()
            {
                Text = "Open Socket Viewer",
                Enabled = true,
                ToolTip = "Open SocketViewer",
                Image = GetImage("NetTraffic32"),
                WindowFactoryID = m_socketKey,
                InitialParameters = new InitialWindowParameters() { Singleton = true },
            });



            container.AddWidget(new InitialButtonParameters
            {
                Text = "Open Matrix",
                Image = GetIcon("Matrix"),
                Click = (sender, args) => { this.process = matrix.Start(); }
            });
            container.AddWidget("quickBox", new InitialWidgetParameters());

            /*

            container.AddWidget(new InitialButtonParameters
            {
                Text = "AIR Launching Window (Socket)",
                Click = (sender, args) => matrix.CreateWindow("SecondAirWindow", null)
            });

            container.AddWidget(new InitialButtonParameters
            {
                Text = "MSDesktop Launching Window (ChromeMgr)",
                Click = (sender, args) => this.chromeManager.CreateWindow("SecondAirWindow", new InitialWindowParameters())
            });
             * */
        }

        private ImageSource GetImage(String name)
        {
            return ((Bitmap) resourceMgr.GetObject(name)).ToImageSource();
        }
        private System.Windows.Media.ImageSource GetIcon(String name)
        {
            return ((Icon)resourceMgr.GetObject(name)).ToImageSource();
        }
    }
}
