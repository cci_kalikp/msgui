﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.FlexModule;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MSDesktop.Demo.Stocks;

namespace MSDesktop.Demo.TradeHistory
{
    public class TradeHistoryModule : FlexModuleFlashPlayerBaseImpl, IModule
    {
        private const string BrowserViewId = "TradeHistoryViewID";
        private const string BrowserViewButtonId = "TradeHistoryButtonID";
        private readonly IEventRegistrator m_eventRegistrator;
        private readonly IList<Trade> m_trades;

        public TradeHistoryModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_,
                                  IEventRegistrator eventRegistrator_) :
                                      base(chromeRegistry_, chromeManager_,
                                           new Uri(
                                               Path.Combine(
                                                   Path.Combine(
                                                       Path.GetDirectoryName(
                                                           Assembly.GetExecutingAssembly().Location), "Lib"),
                                                   "tradehistory.swf")))
        {
            m_eventRegistrator = eventRegistrator_;
            m_trades = new List<Trade>();
        }

        public override void Initialize()
        {
            m_chromeRegistry.RegisterWindowFactory(BrowserViewId, CreateView);
            m_chromeRegistry.RegisterWidgetFactory(BrowserViewButtonId, m_chromeManager.ShowWindowButtonFactory());

            m_chromeManager.PlaceWidget(BrowserViewButtonId, "Trade/Stocks", new InitialShowWindowButtonParameters
                                                                                 {
                                                                                     Text = "Trade history",
                                                                                     ToolTip = "See history of trades",
                                                                                     WindowFactoryID = BrowserViewId,
                                                                                     Image = BitmapFrame.Create(new Uri(@"pack://application:,,,/Resources/filed_fav_48.png", UriKind.RelativeOrAbsolute)),
                                                                                     InitialParameters =
                                                                                         new InitialWindowParameters()
                                                                                 });
        }

        #region View Creation

        protected override bool CreateView(IWindowViewContainer viewcontainer_, XDocument state_)
        {
            if (base.CreateView(viewcontainer_, state_))
            {
                viewcontainer_.Title = "Trade history";

                ISubscriber<Trade> subscriber = m_eventRegistrator.RegisterSubscriber<Trade>(viewcontainer_);
                subscriber.GetObservable().Subscribe(m_ =>
                                                         {
                                                             CallToFlex(viewcontainer_, "handleNewTrade", m_.Price, m_.Quantity, m_.Symbol);
                                                             lock (m_trades)
                                                             {
                                                                 m_trades.Add(m_);
                                                                 var result = from t in m_trades
                                                                              group t by t.Symbol
                                                                                  into g
                                                                                  select
                                                                                      new
                                                                                      {
                                                                                          Symbol = g.Key,
                                                                                          Volume = g.Sum(s_ => s_.Quantity)
                                                                                      };
                                                                 foreach (var item in result.ToList())
                                                                     CallToFlex(viewcontainer_, "handleGrouping", item.Symbol, item.Volume);
                                                             }
                                                         });

                return true;
            }
            return false;
        }

        #endregion

        #region Communication With Flex

        protected override object CallFromFlex(string functionName_, object[] arguments_)
        {
            return null;
        }

        #endregion
    }
}