﻿namespace MSDesktop.Demo.LiveDataModule
{
    public class StockSelectedMessage
    {
        public string Symbol { get; set; }
    }
}
