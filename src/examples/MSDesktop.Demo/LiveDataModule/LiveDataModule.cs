﻿using System;
using Microsoft.Practices.Composite.Modularity;
using MSDesktop.WebSupport.CEF;
using Microsoft.Practices.Unity;
using MSDesktop.WebSupport;

namespace MSDesktop.Demo.LiveDataModule
{
    //class LiveDataModule : AwesomiumBasedHTMLModule
    class LiveDataModule : CEFBasedHTMLModule, IModule
    {
        public LiveDataModule(IUnityContainer unityContainer_)
            : base(unityContainer_)
        {
        }

        protected override string Name
        {
            get { return "LiveDataModule"; }
        }

        protected override Uri Url
        {
            get 
            { 
                //return new Uri("http://google.pl");
                return new Uri(String.Format("{0}\\LiveDataModule\\WebModule.html", System.Environment.CurrentDirectory),
                              UriKind.RelativeOrAbsolute); 
            }
        }
    }
}
