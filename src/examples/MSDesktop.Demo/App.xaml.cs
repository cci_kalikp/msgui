﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;
using MSDesktop.CrossMachineApplication.Extensions;
using MSDesktop.Wormhole;
using MorganStanley.MSDesktop.IPC;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Themes;
using MSDesktop.WebSupport;
using MSDesktop.Demo.Stocks;
using MSDesktop.Demo.TradeHistory;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MSDesktop.Environment;
using MorganStanley.Msdotnet.Msgui.Air;
//using MorganStanley.Msdotnet.Msgui.WinRT;

namespace MSDesktop.Demo
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App
	{
		protected override void OnStartup(StartupEventArgs e_)
		{
			base.OnStartup(e_);
			var boot = new Framework();

            //boot.AddSimpleTheme();
            boot.AddModernTheme(Colors.DeepPink, ModernThemeBase.Dark);
            boot.SetShellMode(ShellMode.RibbonWindow);
		    //boot.SetupPersistenceStorage(new FilePersistenceStorage("MSDesktop.Demo", "Profiles"));

            boot.EnableWormhole();

		    boot.AddModule<LiveDataModule.LiveDataModule>();

            boot.AcquireFocusAfterStartup();
            
            boot.EnableWebSupport();
            boot.EnableCrossMachineDragAndDrop(new IPCOptions
            {
                Host = "vmias6680",
                Port = "64291",
                Kerberos = true,
                GlobalTargeting = new IPCTargeting(new IPCTarget { Key = "application", Value = "MSDesktop.Demo___" })
            });

            boot.SetViewHeaderHeight(30);
            boot.HideOrLockShellElements(HideLockUIGranularity.HideViewPinButton | HideLockUIGranularity.HideEnvironmentLabellingForDockedWindows);

		    boot.EnableEnvironment(EnvironmentExtensions.EnvironProviderId.CommandLine, EnvironmentExtensions.RegionProviderId.None, EnvironmentExtensions.UsernameProviderId.None);
            //boot.EnableFlatMode();
            boot.SetApplicationIcon(BitmapFrame.Create(new Uri(@"pack://application:,,,/Resources/filed_48.png", UriKind.RelativeOrAbsolute)));

            boot.AddModule<StocksModule>();
            boot.AddModule<TradeHistoryModule>();
            boot.AddModule<V2VConfigModule>();
            boot.AddModule<AirIntegrationExampleModule>();
//            boot.AddModule<WinRtEyeCandyModule>();
//            boot.SetLayoutLoadStrategy(LayoutLoadStrategy.NeverAsk); 
			boot.Start();

            

		}
	}
}
