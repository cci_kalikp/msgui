﻿using System;
using System.ComponentModel;
using MSDesktop.Demo.Annotations;
using Newtonsoft.Json;

namespace MSDesktop.Demo.Stocks
{
    [Serializable]
    [JsonObject(MemberSerialization.OptIn)]
    public class Stock : INotifyPropertyChanged
    {
        private string _symbol;
        private double _price;
        private int _quantity;

        [JsonProperty]
        public string Symbol
        {
            get
            {
                return _symbol;
            }
            set
            {
                if (value == _symbol) return;
                _symbol = value;
                OnPropertyChanged("Symbol");
            }
        }

        [JsonProperty]
        public double Price
        {
            get
            {
                return _price;
            }
            set
            {
                if (value.Equals(_price)) return;
                _price = value;
                OnPropertyChanged("Price");
            }
        }

        [JsonProperty]
        public int Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                if (value == _quantity) return;
                _quantity = value;
                OnPropertyChanged("Quantity");
            }
        }

        public override string ToString()
        {
            return Symbol;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName_)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName_));
        }
    }

    [Serializable]
    public class SimpleStock
    {
        public string Symbol { get; set; }
        public double Price { get; set; }
        public DateTime Time { get; set; }

        public SimpleStock(Stock stock_)
        {
            Symbol = stock_.Symbol;
            Price = stock_.Price;
            Time = DateTime.Now;
        }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class StockSelectionChanged
    {
        [JsonProperty]
        public Stock Value
        {
            get;
            set;
        }

        [JsonProperty]
        public string NS
        {
            get
            {
                return "MSDesktop.Demo.Stocks.StockSelectionChanged, MSDesktop.Demo";
            }

        }
    }
}
