﻿using System;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MSDesktop.Demo.Stocks
{
    /// <summary>
    /// Interaction logic for PriceHistoryView.xaml
    /// </summary>
    public partial class PriceHistoryView : UserControl
    {
        public PriceHistoryView(ISubscriber<SimpleStock> subscriber_)
        {
            InitializeComponent();
            subscriber_.GetObservable().Subscribe(stock_ => Dispatcher.Invoke(new Action(() => Grid.Items.Insert(0, stock_))));
        }
    }
}
