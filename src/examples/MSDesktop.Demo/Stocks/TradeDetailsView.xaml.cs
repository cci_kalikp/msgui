﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using MSDesktop.CrossMachineApplication;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MSDesktop.Demo.Stocks
{
    /// <summary>
    /// Interaction logic for TradeDetailsView.xaml
    /// </summary>
    public partial class TradeDetailsView : UserControl
    {
        private readonly IPublisher<Trade> _publisher;
        private readonly IWindowTransferManager _transferManager;
        private readonly IWindowViewContainer _emptyviewcontainer;

        public TradeDetailsView(IPublisher<Trade> publisher_, IWindowViewContainer emptyviewcontainer_)
        {
            _publisher = publisher_;
            _emptyviewcontainer = emptyviewcontainer_;
            InitializeComponent();
        }

        private void BuyClick(object sender_, RoutedEventArgs e_)
        {
            PublishTrade();
        }

        private void PublishTrade(bool sell_=false)
        {
            var stock = (Stock) TradeSymbol.SelectedItem;
            int qty;
            if (int.TryParse(TradeQuantity.Text, out qty))
            {
                _publisher.Publish(new Trade
                    {
                        Quantity = sell_ ? -qty : qty,
                        Symbol = stock.Symbol,
                        Price = stock.Price
                    });
            }
            else
            {
                TaskDialog.ShowMessage("Incorrect quantity", "Incorrect input", TaskDialogCommonButtons.Close,
                                       VistaTaskDialogIcon.Error);
            }
            TradeQuantity.Clear();
        }

        private void SellClick(object sender_, RoutedEventArgs e_)
        {
            PublishTrade(true);
        }

        internal XDocument GetState()
        {
            var result = new XDocument();
            var root = new XElement("root");
            var qty = new XElement("qty", TradeQuantity.Text);
            var symbol = new XElement("symbol", TradeSymbol.SelectedIndex);
            root.Add(qty, symbol);
            result.Add(root);
            return result;
        }

        internal void SetState(XDocument state)
        {
            if (state == null) return;
            TradeQuantity.Text = state.Descendants("qty").First().Value;
            TradeSymbol.SelectedIndex = int.Parse(state.Descendants("symbol").First().Value);
        }
    }
}
