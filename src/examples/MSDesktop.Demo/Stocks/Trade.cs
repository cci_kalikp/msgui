﻿using System.Linq;
using System.Xml.Linq;
using MSDesktop.CrossMachineApplication.Messages;

namespace MSDesktop.Demo.Stocks
{
    public class Trade : ITransferableV2VMessage
    {
        public string Symbol { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }

        public XDocument SaveState()
        {
            var doc = new XDocument();
            var root = new XElement("root");
            root.Add(new XElement("Symbol", Symbol));
            root.Add(new XElement("Price", Price));
            root.Add(new XElement("Quantity", Quantity));
            doc.Add(root);
            return doc;
        }

        public void LoadState(XDocument state)
        {
            Symbol = state.Descendants("Symbol").First().Value;
            Price = double.Parse(state.Descendants("Price").First().Value);
            Quantity = int.Parse(state.Descendants("Quantity").First().Value);
        }
    }
}
