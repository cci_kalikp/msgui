﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Xml.Linq;
using MSDesktop.CrossMachineApplication;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MSDesktop.Demo.LiveDataModule;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;

namespace MSDesktop.Demo.Stocks
{
    public class StocksModule : IModule
    {
        private const string ViewId = "TradesViewID";
        private const string WidgetButtonId = "TradesButtonID";
        private const string ExecutionViewId = "ExecutionViewId";
        private static readonly Random Rnd = new Random();
        private readonly IChromeManager m_chromeManager;
        private readonly IChromeRegistry m_chromeRegistry;
        private readonly ICommunicator m_communicator;
        private readonly IHidingLockingManager _hidingLockingManager;
        private readonly IWindowTransferManager _transferManager;
        private readonly IEventRegistrator m_eventRegistrator;
        private readonly IList<Stock> m_stocks;
        private IModulePublisher<StockSelectionChanged> _stocksPublisher;
        private const int Interval = 1000;
        private IModulePublisher<Stock> m_publisher;
        private DispatcherTimer m_timer;
        private WeakReference grid;

        public StocksModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_,
                            IEventRegistrator eventRegistrator_, ICommunicator communicator_,
                            IHidingLockingManager hidingLockingManager_, IWindowTransferManager transferManager_)
        {
            m_chromeRegistry = chromeRegistry_;
            m_chromeManager = chromeManager_;
            m_eventRegistrator = eventRegistrator_;
            m_communicator = communicator_;
            _hidingLockingManager = hidingLockingManager_;
            _transferManager = transferManager_;
            m_stocks = new ObservableCollection<Stock>();
            PopulateStocks();
            SimulateTicks();

        }

        public ICollectionView Stocks
        {
            get { return CollectionViewSource.GetDefaultView(m_stocks); }
        }

        #region IModule Members

        public void Initialize()
        {
            m_chromeRegistry.RegisterWindowFactory(ViewId, CreateView);
            m_chromeRegistry.RegisterWidgetFactory(WidgetButtonId, m_chromeManager.ShowWindowButtonFactory());

            m_chromeManager.PlaceWidget(WidgetButtonId, "Trade/Stocks",
                                        new InitialShowWindowButtonParameters
                                            {
                                                Text = "View stocks",
                                                ToolTip = "View all stocks",
                                                WindowFactoryID = ViewId,
                                                InitialParameters = new InitialWindowParameters(),
                                                Image = BitmapFrame.Create(new Uri(@"pack://application:,,,/Resources/book_48.png", UriKind.RelativeOrAbsolute))
                                            });

            m_chromeRegistry.RegisterWindowFactory(ExecutionViewId, CreateExecutionView, PersistExecutionView);
            m_chromeManager.Ribbon["Trade"]["Stocks"].AddWidget(
                new InitialShowWindowButtonParameters
                    {
                        Text = "Trade execution",
                        WindowFactoryID = ExecutionViewId,
                        Image = BitmapFrame.Create(new Uri(@"pack://application:,,,/Resources/shopping_cart_48.png", UriKind.RelativeOrAbsolute)),
                        InitialParameters = new InitialWindowParameters
                            {
                                InitialLocation = InitialLocation.DockInNewTab,
                                Singleton = true
                            }
                    });

            m_publisher = m_communicator.GetPublisher<Stock>();

            m_chromeRegistry.RegisterWindowFactory("PriceLoggerId", CreatePriceLogger);
            m_chromeManager.Ribbon["Trade"]["Stocks"].AddWidget(
                new InitialShowWindowButtonParameters
                    {
                        Text = "Price history",
                        WindowFactoryID = "PriceLoggerId",
                        Image = BitmapFrame.Create(new Uri(@"pack://application:,,,/Resources/filed_48.png", UriKind.RelativeOrAbsolute))
                    });

            m_chromeManager.Tabwell.RightEdge.AddWidget(new InitialButtonParameters
                {
                    Text = "Lock layout",
                    Click = (sender_, args_) => _hidingLockingManager.ToggleFlag(
                        HideLockUIGranularity.LockedLayoutMode)
                });
            _stocksPublisher = m_communicator.GetPublisher<StockSelectionChanged>();

            ((ChromeManagerBase)m_chromeManager).SetRibbonIcon(new Uri(@"pack://application:,,,/Resources/filed_48.png", UriKind.RelativeOrAbsolute).ToIcon(new PngBitmapEncoder()));
        }

        private bool CreatePriceLogger(IWindowViewContainer emptyViewContainer_, XDocument state_)
        {
            var addIn = emptyViewContainer_.SetUp32BitProcessIsolatedWrapperFor<PriceHistoryView>(null, typeof(SimpleStock));
            m_eventRegistrator.RegisterMarshalingSubscriber<SimpleStock>(emptyViewContainer_, addIn);
            emptyViewContainer_.Title = "Price history";
            return true;
        }

        private static XDocument PersistExecutionView(IWindowViewContainer windowviewcontainer_)
        {
            var view = (TradeDetailsView) windowviewcontainer_.Content;
            return view.GetState();
        }

        private bool CreateExecutionView(IWindowViewContainer emptyviewcontainer_, XDocument state_)
        {
            var publisher = m_eventRegistrator.RegisterPublisher<Trade>(emptyviewcontainer_);            
            var tradeDetailsView = new TradeDetailsView(publisher, emptyviewcontainer_) {DataContext = this};
            emptyviewcontainer_.Content = tradeDetailsView;
            emptyviewcontainer_.Title = "Trade execution";
            tradeDetailsView.SetState(state_);
            return true;
        }

        #endregion

        private bool CreateView(IWindowViewContainer viewcontainer_, XDocument state_)
        {
            var tradesGrid = new TradesGrid() {DataContext = this};
            viewcontainer_.Content = tradesGrid;
            viewcontainer_.Title = "Trading view";
            tradesGrid.DataContext = this;

            grid = new WeakReference(tradesGrid);

            IModuleSubscriber<StockSelectedMessage> subscriber = m_communicator.GetSubscriber<StockSelectedMessage>();
            subscriber.GetObservable().Subscribe(m_ =>
                                                     {
                                                         Console.WriteLine(m_.Symbol);
                                                         tradesGrid.Dispatcher.Invoke(
                                                             new Action(() => Stocks.MoveCurrentTo(
                                                                 m_stocks.First(s_ => s_.Symbol.Equals(m_.Symbol))
                                                                                  )));
                                                     });

            var trackSymbol = new ComboBox {ItemsSource = m_stocks, SelectedIndex = 0, DisplayMemberPath = "Symbol"};
            var limit = new TextBox {Text = "10"};

            string headerItemId1 = "trackSymbolPicker" + Guid.NewGuid().ToString();
            string headerItemId2 = "trackLimitPicker" + Guid.NewGuid().ToString();
            m_chromeRegistry.RegisterWidgetFactory(headerItemId1,
                (container_, document_) => { container_.Content = trackSymbol; return true;} );
            m_chromeRegistry.RegisterWidgetFactory(headerItemId2,
                (container_, document_) => { container_.Content = limit; return true;});

            viewcontainer_.Header.AddWidget(headerItemId1, new InitialWidgetParameters());
            viewcontainer_.Header.AddWidget(headerItemId2, new InitialWidgetParameters());

            var v2vPublisher = m_eventRegistrator.RegisterPublisher<SimpleStock>(viewcontainer_);

            EventHandler handler = null;
            handler = (sender_, args_) =>
                {
                    viewcontainer_.AccentColour = Colors.Blue;
                    if (trackSymbol.SelectedItem == null) return;
                    if (((Stock) trackSymbol.SelectedItem).Price > int.Parse(limit.Text))
                    {
                        viewcontainer_.AccentColour = Colors.Red;
                    }

                    foreach (var stock in m_stocks)
                    {
                        v2vPublisher.Publish(new SimpleStock(stock));
                    }
                };
            m_timer.Tick += handler;
            EventHandler<WindowEventArgs> handler2 = null;
            handler2 = (sender_, args_) =>
                {
                    m_timer.Tick -= handler;
                    viewcontainer_.Closed -= handler2;
                    viewcontainer_.ClosedQuietly -= handler2;
                };
            viewcontainer_.Closed += handler2;
            viewcontainer_.ClosedQuietly += handler2;
            return true;
        }

        private void PopulateStocks()
        {
            m_stocks.Add(new Stock {Symbol = "APL", Price = 123.4, Quantity = 100});
            m_stocks.Add(new Stock {Symbol = "GOOG", Price = 43.1, Quantity = 125});
            m_stocks.Add(new Stock {Symbol = "MS", Price = 182.6, Quantity = 230});
            m_stocks.Add(new Stock {Symbol = "XYZ", Price = 56.2, Quantity = 500});
            m_stocks.Add(new Stock {Symbol = "WPF", Price = 234.5, Quantity = 233});
        }

        private void SimulateTicks()
        {
            m_timer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 0, 0, Interval)};
            m_timer.Tick += (s_, e_) =>
                                {
                                    //int r = rnd.Next(m_stocks.Count);
                                    foreach (Stock t in m_stocks)
                                    {
                                        t.Price += Rnd.NextDouble()*20 - 10;
                                        if (t.Price < 0) t.Price = 0;
                                        if (m_publisher != null)
                                        {
                                            m_publisher.Publish(t);
                                        }
                                        if (grid != null && grid.IsAlive && _stocksPublisher != null)
                                        {
                                            _stocksPublisher.Publish(new StockSelectionChanged
                                                {
                                                    Value = ((Stock)((TradesGrid)grid.Target).Grid.SelectedItem)
                                                });
                                        }
                                    }
                                    m_timer.Start();
                                };
            m_timer.Start();
        }
    }
}