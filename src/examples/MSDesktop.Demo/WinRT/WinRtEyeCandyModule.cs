﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Windows;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.Msdotnet.Msgui.WinRT
{
    class WinRtEyeCandyModule : IModule
    {
        private readonly IChromeManager chromeManager;
        private readonly IPersistenceService persistenceService;

        public WinRtEyeCandyModule(IChromeManager chromeManager, IPersistenceService persistenceService)
        {
            this.chromeManager = chromeManager;
            this.persistenceService = persistenceService;
        }

        public void Initialize()
        {
            chromeManager.Ribbon["Windows RT"]["Companion app"].AddWidget(new InitialButtonParameters
            {
                Text = "List",
                Click = (sender, args) =>
                    {
                        MessageBox.Show(persistenceService.GetProfileNames().Aggregate((a, b) => a + Environment.NewLine + b));
                    }
            });

            var t = new Thread(() =>
            {
                var wc = new WebClient
                {
                    Proxy = new WebProxy("webproxy-winhttp.ms.com:8080")
                };

                while (true)
                {
                    var layoutName = wc.DownloadString(new Uri("http://rainbow.farmo.si/ms/?action=get"));
                    if (layoutName != "")
                    {
                        wc.DownloadString(new Uri("http://rainbow.farmo.si/ms/?action=set&layoutName="));
                        persistenceService.LoadProfile(layoutName);
                    }

                    Thread.Sleep(100);
                }
            });

            t.Start();
        }
    }
}
