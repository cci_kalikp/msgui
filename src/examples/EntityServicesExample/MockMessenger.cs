﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace MorganStanley.Desktop.EntityServicesExample
{
	internal class MockMessenger
	{
		private Timer timer;
		private bool busy; 
		public MockMessenger()
		{
			MySigninName = Environment.UserName;
			MyServiceId = "FakeCommunicator";
			timer = new Timer(300) {AutoReset = true};
			timer.Elapsed += timer_Elapsed;
			timer.Enabled = true;

		}

		void timer_Elapsed(object sender, ElapsedEventArgs e)
		{
			if (OnContactStatusChange != null)
			{
				var contact = GetContact(MySigninName, MyServiceId);
				contact.Status = busy ? "BUSY" : "ONLINE";
				busy = !busy;
				OnContactStatusChange(contact, new object());
			}
		}
		public event Action<IMockMessengerContact, object> OnContactStatusChange;

		public object MyServiceId { get; set; }

		public string MySigninName { get; set; }

		public IMockMessengerContact GetContact(string toString, object myServiceId)
		{
			return new MockMessengerContact { FriendlyName = toString, SigninName = toString, Status = "BUSY" };
		}
	}
	internal interface IMockMessengerContact
	{

		string SigninName { get; set; }

		string FriendlyName { get; set; }
		string Status { get; set; }
	}

	internal class MockMessengerContact : IMockMessengerContact
	{
		public string SigninName { get; set; }

		public string FriendlyName { get; set; }

		public string Status { get; set; }
	}
}
