﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.MSGui.Themes;
using MorganStanley.MSDotNet.MSGui.Themes.Fx;


namespace MorganStanley.Desktop.EntityServicesExample
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>

    public partial class App : Application
    {


        static App()
        {
            
        }
        [System.STAThread]
        protected override void OnStartup(StartupEventArgs e_)
        {

            base.OnStartup(e_);
            var msguiFramework = new Framework();
           
            msguiFramework.SetShellMode(ShellMode.RibbonWindow);
           
            msguiFramework.EnableLegacyControlPlacement();
            msguiFramework.EnableExceptionHandler("msguiadministrators@ms.com");


            msguiFramework.SetApplicationName("Sample EntityServices");
            msguiFramework.AddWhiteTheme();
            msguiFramework.EnableNonQuietCloseOfViews();


            
            msguiFramework.AddModule<CommunicatorStatusModule>();
            
            msguiFramework.EnableNonOwnedWindows();
            msguiFramework.Start();
        }


    }
}
