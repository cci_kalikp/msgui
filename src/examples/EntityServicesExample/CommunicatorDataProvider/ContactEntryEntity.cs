﻿using System;
using MorganStanley.Desktop.EntityServices;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;

namespace MorganStanley.Desktop.EntityServicesExample.CommunicatorDataProvider
{

    //cw SetupEntityServices : 100

    /**
     * ## Description
     * Setup and use the entity service
     * 
     * ## Define the entity service layers
     * the [service layers][cw_ref EntityServices] are defined as the attribute of the entity
     */


    //cw DefEntity: 100
    /**
     * ## Define the entity
     * Indicate the calss define a entity with the key type, and indicate the allowed operations (fetch, subscribe, etc)
     * 
     */
    //cw SetupEntityServices:100  cw DefEntity: 100
    //{
    [Serializable]
    [EntityService(ServiceMode.Tapped | ServiceMode.Gating | ServiceMode.Replay | ServiceMode.Cloning)]
    
    public class ContactEntryEntity : IEntity<ContactEntryEntity, ContactEntryEntity>, IKey<ContactEntryEntity>,
        ISubscribable, IFetchable, ITimestamped //, IDeletable, IPublishable
    //}

    {
        public ContactEntryEntity()
        {
            _key = this;
        }
        internal ContactEntryEntity(string contactName): this()
        {
            Contact = contactName;
        }

        private ContactEntryEntity _key;
        //cw DefEntity: 100
        /**
         * #### The enetity is identified by its Key         
         */
        //{
        ContactEntryEntity IEntity<ContactEntryEntity, ContactEntryEntity>.Key
        {
            get { return _key; }
        }
        
        IKey IEntity.Key
        {
            get { return _key; }
        }
        //}
        //cw DefEntity :100
        /**
         * ####  The properties of the entity to hold the contact info 
         */
        //{
        public string Contact { get; internal set; }
        public string Status { get; internal set; }
        //}

    
     /*   public ContactEntryEntity(string key)
        {
            _key = new ContactEntryKey(KeySeperator.Default, new string[]{key});
        }

        internal ContactEntryEntity(ContactEntryKey key)
        {
            _key = key;
        }*/

        public DateTime Timestamp
        {
            get; set; 
		}

        public bool Equals(IQuery other)
        {
            var otherKey = other as ContactEntryEntity;
            if(otherKey == null)
                return false;

            return Contact == otherKey.Contact;
        }

        public bool Equals(IKey other)
        {
            var otherKey = other as ContactEntryEntity;
            if (otherKey == null)
                return false;

            return Contact == otherKey.Contact && Status == otherKey.Status;
        }

        public bool Equals(IQuery<ContactEntryEntity> other)
        {
            var otherKey = other as ContactEntryEntity;
            if (otherKey == null)
                return false;

            return Contact == otherKey.Contact;
        }
    }
}
