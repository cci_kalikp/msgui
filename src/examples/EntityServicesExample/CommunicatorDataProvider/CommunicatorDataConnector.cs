﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using MorganStanley.Desktop.EntityServices;


namespace MorganStanley.Desktop.EntityServicesExample.CommunicatorDataProvider
{

    //cw DataConnector : 110
    /**
     * ## Description
     * Retrieve from, send to, and delete data from external data source
     * 
     * ## Define the data connector    
     *
     */
    //{
    class CommunicatorDataConnector: IEntityServiceConnector
    //}
    {
        /**
         * ### In this example, we utilize Office Communicator to provide the IM status data
         */
        //{
        private readonly MockMessenger _communicator;
        //}

        //cw DataConnector : 121
        //{
        private event EventHandler<ContactStatusChangedEventArgs> ContactStatusChanged;
        //}
       
      

        public CommunicatorDataConnector()
        {
            _communicator = new MockMessenger();
            var login = _communicator.MySigninName;

            //cw DataConnector  : 120
            /**
             *   * #### 1. change the non standard IM event into .Net event pattern
             */
            //cw DataConnector: 122
            //{
            _communicator.OnContactStatusChange += (s, e) =>
                                                       {
                                                           var contact = s as IMockMessengerContact;
                                                           if(contact == null)
                                                               return;

                                                           var copy = ContactStatusChanged;
                                                           if (copy != null)
                                                           {
                                                               copy(this, new ContactStatusChangedEventArgs(contact));
                                                           }
                                                           
                                                       };
            //}
        }

        //cw DataConnector  : 110
        /**
         * #### Retrieve external data
         * 
         * Query on contact status from the external source, the result will be sent to converter by entity service
         * 
         */

        //{
        IEnumerable<object> IEntityServiceConnector.Get(IEnumerable<object> queries)
        {
            var res = from query in queries
                      select _communicator.GetContact(query.ToString(),
                      _communicator.MyServiceId) as IMockMessengerContact;

            return res;
        }
        //}

        //cw DataConnector: 110
        /**
         * #### Subscribe external data
         * 
         * In this example, we subscribe the change of the communicator status
         * 
         */

        //cw DataConnector : 125
        /**
         *  * #### 2. subscribe the status changed event
         */
        //{
        IObservable<IEnumerable<object>> IEntityServiceConnector.GetObservable(IEnumerable<object> queries)
        {
            var observable =  Observable.FromEventPattern<EventHandler<ContactStatusChangedEventArgs>, ContactStatusChangedEventArgs>(ev => ContactStatusChanged += ev, ev => ContactStatusChanged -= ev);

            var filter = from contact in observable
                         from q in queries
                         where contact.EventArgs.ContactEntity.SigninName == q.ToString()
                         select new List<object>{contact.EventArgs.ContactEntity};
        	return filter;
        }
        //}



        //cw DataConnector : 125
        /**
         * ### send data to the external source
         */
        //{
        void IEntityServiceConnector.Publish(IEnumerable<object> objectsToPublish)
        //}
        {
            throw new NotImplementedException();  
        }
        

        //cw DataConnector : 125
        /**
         * ### delete data from the external source
         */
        //{
        void IEntityServiceConnector.Delete(IEnumerable<object> key)
        //}
        {
            throw new NotImplementedException();
        }
        
    }
}
