﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MorganStanley.Desktop.EntityServices;
//cw EntityServiceRefs: 101
//{
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
//}

namespace MorganStanley.Desktop.EntityServicesExample.CommunicatorDataProvider
{

    //cw DataConverter  : 105

	/**
     * ## Description
     * Convert external data into entities and convert entities back
     * 
     * ## Define Converter
	 * 
	 */

	//{
	class CommunicatorDataConverter : IEntityServiceConverter<ContactEntryEntity>
    //}
	{
        //cw DataConverter :106
        /**
         * ## Convert raw data to entity
         * 
         * In this example, we need to convert IMessengerContact to our ContactEntryEntity
         */
        //{
        private static ContactEntryEntity Convert(object data)
        {
            var contact = new ContactEntryEntity(((IMockMessengerContact)data).SigninName)
            {
                Contact = ((IMockMessengerContact)data).FriendlyName
            };

            var status = ((IMockMessengerContact)data).Status.ToString();

            status = status.Replace("MISTATUS", "");
            status = status.Replace("_", " ").Trim();

            contact.Status = status;

            return contact;
        }
        //}
       
        //{
        public IEnumerable<ContactEntryEntity> ConvertFromNativeFormatToEntities(IEnumerable<object> nativeEntities)
        {
            return from native in nativeEntities
                   where native != null
                   select Convert(native);  //convert every native entities
        }
        //}

        //cw DataConverter :106
        /**
         * ## Convert internal query to external query
         * 
         */
        //{
        public IEnumerable<object> ConvertFromQueriesToNativeFormat(IEnumerable<IQuery<ContactEntryEntity>> queries, QueryConversionType conversionType)
        {
            return queries; //Here we assume the connector understands entity service queries
        }
        //}


        //cw DataConverter: 107
        /**
         * ## Convert enetity back to raw data format
         * 
         */
        //{
        public IEnumerable<object> ConvertFromEntitiesToNativeFormat(IEnumerable<IEntity<ContactEntryEntity, IKey<ContactEntryEntity>>> entities)
        //}
        {
            throw new NotImplementedException(); //implement this method to convert entities back to native format
        }

		

		

	}
	//}
}
