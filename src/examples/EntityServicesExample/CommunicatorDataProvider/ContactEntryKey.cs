﻿using System;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;

namespace MorganStanley.Desktop.EntityServicesExample.CommunicatorDataProvider
{
    //cw DefEntity : 10 

    /**
     * ## Description     
     * Define Entity and EntityKey 
     * 
     * ## Define the entity key
     * 
     * 
     * The IDataConnector will decide how to query on the key.
     */

    //{
    [Serializable]
    public class ContactEntryKey: StringBasedKey, IKey<ContactEntryEntity>
    //}
    {
        public ContactEntryKey(KeySeperator seperator, params string[] fields) : base(seperator, fields)
        {
        }

        bool IEquatable<IQuery<ContactEntryEntity>>.Equals(IQuery<ContactEntryEntity> other)
        {
            return this.ToString().Equals(other.ToString());
        }
    }
    
}
