﻿using System;

namespace MorganStanley.Desktop.EntityServicesExample.CommunicatorDataProvider
{
    class ContactStatusChangedEventArgs: EventArgs
    {
        public IMockMessengerContact ContactEntity { get; private set; }

        public ContactStatusChangedEventArgs(IMockMessengerContact contact)
        {
            ContactEntity = contact;
        }
        
    }
}