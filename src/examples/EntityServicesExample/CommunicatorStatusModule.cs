﻿using System;
using System.Reactive.Linq;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;

//cw EntityServiceRefs: 100
//{
using MorganStanley.Desktop.EntityServices;
//}
using MorganStanley.Desktop.EntityServices.Keys;

using MorganStanley.Desktop.EntityServicesExample.CommunicatorDataProvider;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.StatusBar;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;

#region  codewiki pages
//cw EntityServiceRefs: 0
/**
 * ## Description
 *   The example shows how to setup up a entity service that utialize Microsoft Office Communicator as its underlying data source.
 *   
 *   It will extract the communicator status and show in the appliation statusbar.
 * 
 * ## Assembly
 * 
 * To use Entity Services API, you need refer to MSDesktop.EntityServices.dll in your project
 * 
 * ## Namespaces
 */

//cw EntityServiceDownload
/**
 *  
 * #### At the begining,  then initial communicator status is obtained and disbplayed in the status bar:
 *  ![Fetch status](images/EntityServices/entity_service_1.jpg)
 *  
 * #### When change the communicator status to busy, the change is picked up 
 *  ![Fetch status](images/EntityServices/entity_service_2.jpg)
 */

#endregion


namespace MorganStanley.Desktop.EntityServicesExample
{
    
    public class CommunicatorStatusModule : IModule
    {
        private IEntityService<ContactEntryEntity> _entityService;
        private IStatusLabelViewContainer _statusbar;
        private readonly IChromeManager _chromeManager;
        private readonly IUnityContainer _container;

        

        public CommunicatorStatusModule(IUnityContainer container, IChromeManager chromeManager)
        {
            _chromeManager = chromeManager;
            _container = container;
        }

        public void Initialize()
        {
            //cw  SetupEntityServices :201
            /**
            * ## Register a new entity service  with the NUnity container          
            */

            //{
            _container.RegisterEntityService(typeof(ContactEntryEntity), typeof(CommunicatorDataConnector), typeof(CommunicatorDataConverter));
            //}

          

            //cw SetupEntityServices :202

            /**
             * ## Obtain the instance of IEntityService from NUnity container
             */
            //{
            _entityService = _container.Resolve<IEntityService<ContactEntryEntity>>();
            //}

            /**
             * ## Query on the entity services
             * 
             * Need to claim the entity to implement IFetchable
             */
            //{
            var communicator = new MockMessenger();
            var query = new ContactEntryKey(KeySeperator.Default, new[] { communicator.MySigninName });

            
            foreach(var status in _entityService.Get(new[] { query }))
            {

                var wvc
                    = _chromeManager.Statusbar().LeftSide.AddWidget("Communicator_status",
                                                                           new InitialStatusLabelParameters()
                                                                               {
                                                                                   Level = StatusLevel.Information,
                                                                                   Text =
                                                                                       "Communicator status: " +
                                                                                       status.Status

                                                                               });
                _statusbar = wvc as IStatusLabelViewContainer;
                break;
            }
            //}

            /**
             * ## Subscribe updates
             * 
             * Need to claim the entity to implement ISubscribable
             */

            //{[csharp]
            var all = _entityService.GetObservable(query);
            var filter = from sub in all
                         //where  clause
                         select sub;

            filter.Subscribe((x) => {
                _statusbar.Text = "Communicator status: " + x.Status;  
                //Display in the status bar.  Note: there might be delays to pick up the office communicator status
            });

          
            //}

           

            //cw_end
            
            var contacts = _entityService.Get(new[] {query});
            foreach (var contact in contacts)
            {
                Console.WriteLine(contact.Contact);
                Console.WriteLine(contact.Status);
            }

        }
    }
/*
    class ExceptionService : IEntityServiceExceptionService<MSDotNet.MSLog.MSLog>
    {
        public void LogException(string message, Exception exception)
        {
            var log = MSDotNet.MSLog.MSLog.Error();
            log.Message = message;
            log.AppendException(exception);
            log.Send();
            throw new ApplicationException(message, exception);
        }

        public void LogException(Exception exception)
        {
            var log = MSDotNet.MSLog.MSLog.Error();
            log.AppendException(exception);
            log.Send();

            throw exception;
        }
    }*/

   
}
