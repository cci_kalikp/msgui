﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/InitialSplash/StartForm.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace MorganStanley.MSDotNet.MSGui.InitialSplash
{
  internal class StartForm : Form
  {
    public StartForm()
    {
      InitializeComponent();
    }
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    private Point m_mouseDistance;
    private bool m_dragging;
    private void Form1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
    {
      switch (e.Button)
      {
        case System.Windows.Forms.MouseButtons.Left:
          m_mouseDistance = new Point(MousePosition.X - this.Location.X, MousePosition.Y - this.Location.Y);
          m_dragging = true;
          break;
      }
    }

    private void Form1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
    {
      switch (e.Button)
      {
        case System.Windows.Forms.MouseButtons.Left:
          m_dragging = false;
          break;
      }
    }

    private void Form1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
    {
      if (e.Button == System.Windows.Forms.MouseButtons.Left && m_dragging)
      {
        this.Location = new Point(MousePosition.X - m_mouseDistance.X, MousePosition.Y - m_mouseDistance.Y);
      }
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartForm));
      this.SuspendLayout();
      // 
      // StartForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImage = MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Properties.Resources.msgui_splash;
      this.ClientSize = new System.Drawing.Size(this.BackgroundImage.Width, this.BackgroundImage.Height);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      //this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "StartForm";
      this.Opacity = 0.9;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "MSGui";
      this.TopMost = true;
      this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
      this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
      this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
      this.ResumeLayout(false);
    }

    #endregion
  }
}
