﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/InitialSplash/InitialWindowHandler.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace MorganStanley.MSDotNet.MSGui.InitialSplash
{
	internal class InitialWindowHandler
	{
		private Thread m_thread;
		private StartForm m_sf;

		private void StartThread()
		{
			m_sf = new StartForm();
			Application.Run(m_sf);
		}

		public void Start()
		{
			m_thread = new Thread(StartThread);
			m_thread.Start();
		}

		private Point m_lastLocation = Point.Empty;

		public void Stop()
		{
			if (Application.AllowQuit)
			{
				m_lastLocation = m_sf.Location;
				Application.Exit();
				m_sf = null;
			}
			else
			{
				try
				{
					m_thread.Abort();
				}
				catch (ThreadStateException exc)
				{
					//we couldn't close the initial splash screen.
				}
			}
		}

		public Point GetLocation()
		{
			return m_sf == null ? m_lastLocation : m_sf.Location;
		}

		private delegate void SetBoolDelegate(bool value_);

		private delegate bool GetBoolDelegate();

		public bool IsHidden
		{
			get
			{
				return m_sf.InvokeRequired ? (bool)m_sf.Invoke(new GetBoolDelegate(GetHidden), null) : GetHidden();
			}
			set
			{
				if (m_sf != null)
				{
					if (m_sf.InvokeRequired)
					{
						m_sf.Invoke(new SetBoolDelegate(SetHidden), new object[] { value });
					}
					else
					{
						SetHidden(value);
					}
				}
			}
		}

		private bool GetHidden()
		{
			return !m_sf.Visible;
		}

		private void SetHidden(bool isHidden_)
		{
			m_sf.Visible = !isHidden_;
		}
	}
}