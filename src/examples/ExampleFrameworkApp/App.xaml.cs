﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/App.xaml.cs#81 $
// $Change: 894857 $
// $DateTime: 2014/08/29 07:00:19 $
// $Author: milosp $

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MSDesktop.Extensions;
using MSDesktop.Omnibox.UI;
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;
using MSDesktop.Debugging;
using MorganStanley.MSDotNet.MSGui.Themes.MSDesktop2014;
using MSDesktop.ModuleEntitlements;
using MSDesktop.Workspaces;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.Desktop.Entitlement;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Modules;
using MorganStanley.MSDotNet.MSGui.Impl.Alert;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.MSGui.InitialSplash;
using MorganStanley.MSDotNet.MSGui.Themes;
using MorganStanley.MSDotNet.MSGui.Themes.Fx;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDotNet.MSGui.Impl.Printing; 

namespace MorganStanley.MSDotNet.MSGui
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private static readonly InitialWindowHandler m_splash;

		static App()
		{
			m_splash = new InitialWindowHandler();
			m_splash.Start();
		}
		protected override void OnStartup(StartupEventArgs e_)
		{
			base.OnStartup(e_);
			var boot = new Framework();
			boot.RegisterInitialSplash(m_splash);
            //boot.AddBlueTheme();
            //boot.AddModernTheme(new Color {A = 255, R = 149, G = 0, B = 81 }, ModernThemeBase.Dark);
            boot.AddModernTheme(new Color { A = 255, R = 77, G = 69, B = 154 }, ModernThemeBase.Dark);
            //cw options_overview : 40
            /**##Style of Options Window*/
            /**By default options window would be themed as other MSDesktop Dialog, but it could be forced to still use the old classic style if needed */
            if (1==0)
            //{
		    boot.UseClassicOptionsWindow();
            //}
            /**This has the equivalent effect */
            if (1 == 0)
            //{
                boot.UseClassicTheme(PopupDialogType.OptionsWindow);
            //}
          

            //cw options_overview: 50
            /**Default Style*/
            /**![Default Style](images/Options/OptionsWindowDefaultStyle.png)*/
            /**Classic Style*/
            /**![Classic Style](images/Options/OptionsWindowClassicStyle.png)*/

            //cw options_overview: 60
            /**##Search Option Pages 
             *Options window now support searching option pages upon the path, title, label, tags and etc*/
            /**![Search Option Window](images/Options/OptionsWindowSearch.png)*/

            //cw options_overview: 65
            /**User could also choose to use the type ahead drop down to show the searched result 
            */
            if (1 == 0)
            //{
            boot.EnableTypeaheadDropdownOptionsSearch();
            //}
            /**![Search Option Window](images/Options/OptionsWindowSearch2.png)*/

            //cw options_overview: 70
            /**##Highlight invalid field
             * Validation can now be supported by the Option Windows, and if invalid, the page would be navigated to, and highlight the invalid field if specified using a flashing label*/

            //cw options_overview: 80
            /**##Adjust option page position
             * A priority can be specified for the option page path or the path and title of the option page to adjust the position of the options page */
            /** The higher the priority the former the page would be added. A positive priority would move the page above normal order, a negative one would move it below normal order */
            //{
            //move "Custom" node above
            boot.SetOptionGroupPriority("Custom", 1000);
            //move page "Custom/Sub Settings/Example custom settings1" above other nodes under parent group "Custom"
            boot.SetOptionPagePriority("Custom/Sub Settings", "Example custom settings1", 2000);
            //}

            //boot.AddSimpleTheme();
			//boot.SetTheme("Simple");
			boot.EnableExceptionHandler("msguiadministrators@ms.com");
			//boot.EnableNonOwnedWindows();

			//boot.AddModule<ViewToViewConfigModule>();
			boot.AddModule<V2VConfigModule>();

		    boot.EnableWorkspaceSaveLoad();
			
            // to have the PrintSupport, you can either 
			// boot.AddModule<PrintModule>();
			// or call  
			//boot.EnablePrintSupport() the extension methods
			//
			// the order you put PrintModule or V2VConfigModule will decide the order of the items
			// Menu that appears on Application Menu
			boot.AddModule<PrintModule>();
			boot.EnablePrintSupport();

            //example code of enable workspace with multiple workspace category
            var workspaceTitles = new Dictionary<string, string>();
            workspaceTitles.Add(WorkspacesExampleModule.DockImageWorkspaceCategory, "New " + WorkspacesExampleModule.DockImageWorkspaceCategory);
            workspaceTitles.Add(WorkspacesExampleModule.DockTextWorkspaceCategory, "New " + WorkspacesExampleModule.DockTextWorkspaceCategory);
            workspaceTitles.Add(WorkspacesExampleModule.TileImageWorkspaceCategory, "New " + WorkspacesExampleModule.TileImageWorkspaceCategory);
            workspaceTitles.Add(WorkspacesExampleModule.TileTextWorkspaceCategory, "New " + WorkspacesExampleModule.TileTextWorkspaceCategory);
            boot.EnableWorkspaceSupport(workspaceTitles);
            boot.EnableWorkspaceMixingMode();
            boot.AddModule<WorkspacesExampleModule>();

			boot.SetApplicationIcon(BitmapFrame.Create(new Uri(@"pack://application:,,,/Resources/icon.ico", UriKind.RelativeOrAbsolute)));

          
			string currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			boot.SetupPersistenceStorage(
			  new MultipleFilePersistenceStorage(boot, Path.Combine(currentDirectory, "Profiles")));
			string directory = Path.Combine(
			  Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
			  "Morgan Stanley",
			  "ExampleFrameworkApp",
			  "Log");
			if (!(System.IO.Directory.Exists(directory)))
			{
                System.IO.Directory.CreateDirectory(directory);
			} 
            
			MSLog.MSLog.Init(Path.Combine(directory, string.Format("log{0}.log", Process.GetCurrentProcess().Id)),
							 MSLogPriority.All);
			boot.SetLayoutLoadStrategy(LayoutLoadStrategy.Default);
			//boot.SetLayoutLoadStrategy(new LayoutLoadStrategyCallback(() => true));

			//boot.DisableThemeChanger();
			boot.SetViewRenameKeyGesture(new KeyGesture(Key.F2, ModifierKeys.Alt));

			//boot.AddSimpleTheme();
			//boot.AddBlackTheme();
			//boot.AddBlueTheme();
			//boot.AddWhiteTheme();
			//boot.SetAlertConsumer(new AlertConsumerStatusBar());
			//boot.SetAlertConsumer(new AlertConsumerDialogBox());
			boot.SetAlertConsumer(
			  new MultipleAlertConsumer(AlertChooser,
										new AlertConsumerStatusBar(),
										new AlertConsumerDialogBox()));

            //boot.EnableCentralizedPlacement("C:\\msde\\dev\\sample.xml", true);

            boot.SetWindowDeactivatingNotificationEnabled(true);

            boot.SetTabGroupTabStripPlacement(Dock.Right);
            boot.EnableTabDragAndDrop();
		    boot.EnableDebugging();
            Resources.MergedDictionaries.Add(
                new ResourceDictionary
                    {
                        Source = new Uri("/ExampleFrameworkApp;component/Resources/ResourcesDict.xaml",
                            UriKind.RelativeOrAbsolute)
                    });
		    //boot.EnableTopmostFloatingPanes();

            boot.AddModule<OmniboxModule>();
            boot.AddModule<OmniboxBasedViewSelectorModule>();

			boot.Start();
		}

		private IEnumerable<IAlertConsumer> AlertChooser(Alert alert, IList<IAlertConsumer> consumers)
		{
			yield return alert.AlertLevel == AlertLevel.Critical ? consumers[0] : consumers[1];
			//if (alert.ViewContainer != null)
			//  yield return consumers[2];
		}
	}
}
