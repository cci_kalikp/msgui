﻿#pragma checksum "..\..\..\..\Modules\Air\AirPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "F9123DCB73ED78C39CC5DDF6BE9F2CC1"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Modules.Air {
    
    
    /// <summary>
    /// AirPage
    /// </summary>
    public partial class AirPage : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label3;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox WindowList;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox InputText;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button GetStateButton;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button LaunchWithStateButton;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CloseButton;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ShutdownButton;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox FactoryName;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label1;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label2;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label100;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox AirFactories;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addFactoryButton;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button button1;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button button2;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox setParentParam1;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox setParentParam2;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\..\Modules\Air\AirPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button setParentButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ExampleFrameworkApp;component/modules/air/airpage.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Modules\Air\AirPage.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.label3 = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.WindowList = ((System.Windows.Controls.ListBox)(target));
            return;
            case 3:
            this.InputText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.GetStateButton = ((System.Windows.Controls.Button)(target));
            
            #line 24 "..\..\..\..\Modules\Air\AirPage.xaml"
            this.GetStateButton.Click += new System.Windows.RoutedEventHandler(this.GetStateButton_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.LaunchWithStateButton = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\..\..\Modules\Air\AirPage.xaml"
            this.LaunchWithStateButton.Click += new System.Windows.RoutedEventHandler(this.LaunchWithStateButton_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.CloseButton = ((System.Windows.Controls.Button)(target));
            
            #line 26 "..\..\..\..\Modules\Air\AirPage.xaml"
            this.CloseButton.Click += new System.Windows.RoutedEventHandler(this.CloseButton_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.ShutdownButton = ((System.Windows.Controls.Button)(target));
            
            #line 27 "..\..\..\..\Modules\Air\AirPage.xaml"
            this.ShutdownButton.Click += new System.Windows.RoutedEventHandler(this.ShutdownButton_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.FactoryName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.label1 = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.label2 = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.label100 = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.AirFactories = ((System.Windows.Controls.ListBox)(target));
            
            #line 33 "..\..\..\..\Modules\Air\AirPage.xaml"
            this.AirFactories.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.AirFactories_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 13:
            this.addFactoryButton = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\..\..\Modules\Air\AirPage.xaml"
            this.addFactoryButton.Click += new System.Windows.RoutedEventHandler(this.addFactoryButton_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.button1 = ((System.Windows.Controls.Button)(target));
            
            #line 35 "..\..\..\..\Modules\Air\AirPage.xaml"
            this.button1.Click += new System.Windows.RoutedEventHandler(this.cmLaunch_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.button2 = ((System.Windows.Controls.Button)(target));
            
            #line 36 "..\..\..\..\Modules\Air\AirPage.xaml"
            this.button2.Click += new System.Windows.RoutedEventHandler(this.sendEvent_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.setParentParam1 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.setParentParam2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.setParentButton = ((System.Windows.Controls.Button)(target));
            
            #line 39 "..\..\..\..\Modules\Air\AirPage.xaml"
            this.setParentButton.Click += new System.Windows.RoutedEventHandler(this.setParentButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

