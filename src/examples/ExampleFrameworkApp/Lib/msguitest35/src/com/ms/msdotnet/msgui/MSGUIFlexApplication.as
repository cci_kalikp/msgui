package com.ms.msdotnet.msgui
{
	import flash.events.TimerEvent;
	import flash.external.ExternalInterface;
	import flash.utils.Timer;
	
	import mx.core.Application;
	import mx.events.FlexEvent;
	
	public class MSGUIFlexApplication extends Application
	{
		
		private var m_useDirectCalls:Boolean = true;
		
		public function MSGUIFlexApplication()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		/**
		 * Initializes communication infrastructure between the ActiveX container and the Flex application
		 * */
		private function creationCompleteHandler(event:FlexEvent):void
		{
			// check if the container is able to use the External API
			if(ExternalInterface.available)
			{
				try
				{
					// This calls the isContainerReady() method, which in turn calls
					// the container to see if Flash Player has loaded and the container
					// is ready to receive calls from the SWF.
					var containerReady:Boolean = isContainerReady();
					if(containerReady)
					{
						// if the container is ready, register the SWF's functions
						initCommunication();
					}
					else
					{
						// If the container is not ready, set up a Timer to call the
						// container at 100ms intervals. Once the container responds that
						// it's ready, the timer will be stopped.
						var readyTimer:Timer = new Timer(100);
						readyTimer.addEventListener(TimerEvent.TIMER, timerHandler);
						readyTimer.start();
					}
				}
				catch(error:SecurityError)
				{
					trace("A SecurityError occurred: " + error.message + "\n");
					throw error;
				}
				catch(error:Error)
				{
					trace("An Error occurred: " + error.message + "\n");
					throw error;
				}
			}
			else
			{
				trace("External interface is not available for this container.");
			}
		}
		
		/**
		 * Calls the container's isReady() function, to check if the container is loaded
		 * and ready to communicate with the SWF file.
		 * @return 	Whether the container is ready to communicate with ActionScript.
		 */
		private function isContainerReady():Boolean
		{
			return ExternalInterface.call("MSGUI_isReady");
		}
				
		/**
		 * Registers the appropriate ActionScript functions with the container, so that
		 * they can be called, and calls the "setSWFIsReady()" function in the container
		 * which tells the container that the SWF file is ready to receive function calls.
		 */
		private function initCommunication():void
		{
			m_useDirectCalls = ExternalInterface.call("MSGUI_useDirectCalls");
			setupCallbacks();
			// notify the container that the SWF is ready to be called.
			callFromFlex("MSGUI_setSWFIsReady");
		}
		
		/**
		 * Registers the appropriate ActionScript functions with the container, so that
		 * they can be called.
		 * 
		 * A registration could look like this:
		 * <code>ExternalInterface.addCallback("C# func", flexFunc);</code>
		 * 
		 */
		protected function setupCallbacks():void
		{
			// add callback registrations here
		}
		
		/**
		 * Calls host function
		 * @param func function name
		 * */
		protected function callFromFlex(func:String, ...arguments):*
		{
			if(m_useDirectCalls)
			{
				arguments.unshift(func);
				return ExternalInterface.call.apply(null, arguments);
			}
			return ExternalInterface.call("MSGUI_callFromFlex", func, arguments);
		}
		
		/**
		 * Handles the timer event; this function is called by the timer each
		 * time the elapsed time has been reached.
		 * The net effect is that on regular intervals this function checks
		 * to see if the container is ready to receive communication.
		 * @param event		The event object for the Timer event.
		 */
		private function timerHandler(event:TimerEvent):void
		{
			// check if the container is now ready
			var isReady:Boolean = isContainerReady();
			if (isReady)
			{
				// If the container has become ready, we don't need to check anymore,
				// so stop the timer.
				Timer(event.target).stop();
				// Set up the ActionScript methods that will be available to be
				// called by the container.
				initCommunication();
			}
		}
	}
}