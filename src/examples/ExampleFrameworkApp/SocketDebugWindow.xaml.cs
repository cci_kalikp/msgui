﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MSDesktop.Wormhole;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Xml.Linq;
using System.Net.Sockets;
using Microsoft.Practices.Composite.Events;

namespace MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Modules.Air
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class SocketDebugWindow : Page
    {

        BridgedAirApplication bridge;
        public SocketDebugWindow(BridgedAirApplication air)
        {
            bridge=air;
            
       
            InitializeComponent();
            setupBindings();
            
        }

        public void setupBindings() {

            bridge.SentText += SentText;
            bridge.ReceiveText += RecvText;

        }

        private void clearTextButton_Click(object sender, RoutedEventArgs e)
        {
            this.SocketText.Text = "";
        }

        private void SentText(object sender, DataEventArgs<String> str) { 
             Dispatcher.Invoke(new Action(() =>{
                 this.SocketText.Text += "\nSENT: " + str.Value;
             
             }));
        }


        private void RecvText(object sender, DataEventArgs<String> str) { 
            Dispatcher.Invoke(new Action(() =>{  this.SocketText.Text += "\nRECV: " + str.Value;}));

        }
        
        
       

       
    }

   
}
