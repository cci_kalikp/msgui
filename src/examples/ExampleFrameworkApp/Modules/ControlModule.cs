﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/ControlModule.cs#12 $
// $Change: 858188 $
// $DateTime: 2013/12/10 01:31:58 $
// $Author: caijin $

using System.Drawing;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows.Data;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Modules
{
	public class ControlModule : IControlParadigmImplementatorModule
	{
		private readonly IControlParadigmImplementator m_controlParadigmImplementator;
		private readonly IChromeManager m_chromeManager;

		private const string ConfirmDeactivateToggleID = "ConfirmDeactivateToggleID";

		public ControlModule(IChromeManager chromeManager_, IControlParadigmImplementator controlParadigmImplementator, IChromeRegistry chromeRegistry_)
		{
			this.m_chromeManager = chromeManager_;
			m_controlParadigmImplementator = controlParadigmImplementator;

      var viewManager = chromeManager_ as IViewManager;
      if (viewManager != null)
      {
        viewManager.Activated += new System.EventHandler<ViewEventArgs>(ViewActivated) ;
        viewManager.ViewAdded += new EventHandler<ViewEventArgs>(ViewAdded);
      }
			

			chromeRegistry_.RegisterWidgetFactory(ConfirmDeactivateToggleID);
			chromeManager_.PlaceWidget(ConfirmDeactivateToggleID, "Dialog/Options", new InitialButtonParameters
			{
				Text = "Confirmations on",
				Click = (s, e) =>
				{
					if ((s as ButtonBase).Content.Equals("Confirmations on"))
					{
						chromeManager_.WindowDeactivating += ChromeManagerViewDeactivating;
						(s as ButtonBase).Content = "Confirmations off";
					}
					else
					{
						chromeManager_.WindowDeactivating -= ChromeManagerViewDeactivating;
						(s as ButtonBase).Content = "Confirmations on";
					}
				}
			});

      chromeManager_.WindowActivated += new EventHandler<WindowEventArgs>(ChromeManagerViewActivated);
      chromeManager_.WindowDeactivated +=new EventHandler<WindowEventArgs>(ChromeManagerViewDeactivated);

          var colorSet = false;
          chromeManager_.Ribbon["Chrome"]["Titlebar"]
              .AddWidget(new InitialButtonParameters
              {
                  Text = "Change color",
                  Click = (sender_, args_) =>
                  {
                      if (colorSet = !colorSet)
                      {
                          chromeManager_.SetMainWindowTitleBarColour(System.Windows.Media.Colors.Red);
                      }
                      else
                      {
                          chromeManager_.SetMainWindowTitleBarColour(null);
                      }
                  }
              });

		    var overlayOn = false;
		    chromeManager_.Ribbon["Chrome"]["Overlay"]
		        .AddWidget(new InitialButtonParameters
		            {
		                Text = "Toggle",
		                Click = (sender_, args_) =>
		                    {
		                        if (overlayOn = !overlayOn)
		                        {
		                            var brush = new SolidColorBrush(Colors.Red);
		                            var animation = new ColorAnimation
		                                {
		                                    From = System.Windows.Media.Color.FromArgb(100, 255, 0, 0),
		                                    To = System.Windows.Media.Color.FromArgb(200, 255, 0, 0),
		                                    Duration = TimeSpan.FromSeconds(1),
		                                    RepeatBehavior = RepeatBehavior.Forever,
		                                    AutoReverse = true
		                                };
		                            chromeManager_.ToggleOverlayMode(true, brush, 
                                        () => new TextBlock
                                        {
                                            Text = @"PANIC!!!",
                                            FontSize = 50,
                                            Foreground = System.Windows.Media.Brushes.White,
                                            HorizontalAlignment = HorizontalAlignment.Center,
                                            VerticalAlignment = VerticalAlignment.Center
                                        });
                                    brush.BeginAnimation(SolidColorBrush.ColorProperty, animation);
		                        }
		                        else
		                        {
                                    chromeManager_.ToggleOverlayMode(false);
		                        }
		                    }
		            });


		    //chromeManager_.WindowAdded += new EventHandler<WindowEventArgs>(ChromeManagerWindowAdded);
		}

		void ChromeManagerViewDeactivating(object sender, CancellableWindowEventArgs e)
		{
			var windowContainer = e.ViewContainer;
			if (windowContainer != null)
			{
				Console.WriteLine("***Deactivating view...");
				var owner = e.ViewContainer.Content != null ? Window.GetWindow(e.ViewContainer.Content as DependencyObject) : null;
				var result = TaskDialog.ShowMessage("Deactivate view?", "Deactivating view", TaskDialogCommonButtons.OKCancel, VistaTaskDialogIcon.Warning, owner);
				if (result == TaskDialogSimpleResult.Cancel)
					e.Cancel = true;
			}
		}

    void ChromeManagerViewDeactivated(object sender, WindowEventArgs e)
    {
      var windowContainer = e.ViewContainer;
      if (windowContainer != null)
      {
        Console.WriteLine("***Deactivated view.");
      }

    }

    void ChromeManagerWindowAdded(object sender, WindowEventArgs e)
    {
      var windowContainer = e.ViewContainer;
      if (windowContainer != null)
      {
        Console.Write("hello");
      }
    }

    void ChromeManagerViewActivated(object sender, WindowEventArgs e)
    {
      var windowContainer = e.ViewContainer;
      if (windowContainer != null)
      {
        Console.Write("hello");
      }
    }


    // this is the new way to use the Activate and Deactivate 


    void ViewActivated(object sender_, ViewEventArgs e)
    {
      var viewContainer = e.ViewContainer as IWindowViewContainer;
      if (viewContainer != null)
      {
        // do something here
        Console.WriteLine("hello world");
      }
    }


    void ViewAdded(object sender, ViewEventArgs e)
    {
      var viewContainer = e.ViewContainer as IWindowViewContainer;
      if (viewContainer != null)
      {
        // do something here. 
      }
    }



		#region IControlParadigmImplementatorModule Members

		public void Initialize()
		{
			//m_controlParadigmImplementator.PlaceAllControls();
			//m_chromeManager.CreateUI();
      //m_controlParadigmImplementator.SetRibbonIcon(SystemIcons.Shield);
			m_controlParadigmImplementator.SetOnClosingAction(() => TaskDialog.ShowMessage("blabla"));
		}

		#endregion
	}
}