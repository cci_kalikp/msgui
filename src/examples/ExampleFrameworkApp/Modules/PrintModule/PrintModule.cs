﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Modules.PrintModule.Views;
using MorganStanley.MSDotNet.MSGui.Core.Printing;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Printing;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using System.Windows.Forms.Integration;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ShellMenu;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using Size = System.Drawing.Size;

namespace MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Modules.PrintModule
{
  public class PrintDemoModule : IModule
  {
    public PrintDemoModule(IChromeManager chrome, IChromeRegistry registry)
    {
      this.Chrome = chrome;
      this.Register = registry;
    }


    public void Initialize()
    {
      Register.RegisterWindowFactory(PrintModuleViewId, CreateView);
      Register.RegisterWindowFactory(PrintModuleWin32ViewId, CreateWin32View);
      Register.RegisterWindowFactory(PrintModulePrintTextBlockViewId, PrintTextBlockOnlyView);
      Register.RegisterWidgetFactory(PrintModuleWidgetId, Chrome.ShowWindowButtonFactory());
      // this widget will create a button, whose click handler shall print the View
      Register.RegisterWidgetFactory(PrintModulePrintWidgetId);
      Register.RegisterWidgetFactory(PrintModulePrintPreviewWidgetId);
      Register.RegisterWidgetFactory(PrintModulePrintPreviewAllWidgetId);
      Register.RegisterWidgetFactory(PrintModulePrintAllWidgetId);
      Register.RegisterWidgetFactory(PrintModuleWin32WidgetId);

      Register.RegisterWidgetFactory(PrintModulePrintTextBlockWidgetId);


      Chrome.PlaceWidget(PrintModuleWidgetId, RibbonChromeManager.RIBBON_COMMANDAREA_NAME, "Print/View", new InitialShowWindowButtonParameters
      {
        Text = "Create View",
        WindowFactoryID = PrintModuleViewId,
        InitialParameters = new InitialWindowParameters
        { 
          InitialLocation = Core.InitialLocation.Floating
        },
        ToolTip = "Create the View to print",
        Enabled = true
      }
      );


      Chrome.PlaceWidget(PrintModulePrintWidgetId, RibbonChromeManager.RIBBON_COMMANDAREA_NAME, "Print/Print", new InitialButtonParameters
      {
        Text = "Print",
        Click = PrintView,
        Enabled =true, 
        ToolTip = "Print the view",
      }
      );

      Chrome.PlaceWidget(PrintModulePrintAllWidgetId, RibbonChromeManager.RIBBON_COMMANDAREA_NAME, "Print/Print", new InitialButtonParameters
      {
        Text = "Print all",
        Click = PrintViewAll,
        Enabled = true,
        ToolTip = "Print all View",
      });

      Chrome.PlaceWidget(PrintModulePrintPreviewWidgetId, RibbonChromeManager.RIBBON_COMMANDAREA_NAME, "Print/Print", new InitialButtonParameters
      {
        Text = "Print Preview",
        Click = PrintPreview,
        Enabled = true,
        ToolTip = "Preview Print the view",
      }
      );


      Chrome.PlaceWidget(PrintModulePrintPreviewAllWidgetId, RibbonChromeManager.RIBBON_COMMANDAREA_NAME, "Print/Print", new InitialButtonParameters
      {
        Text = "Print Preview All",
        Click = PrintPreviewAll,
        Enabled = true, 
        ToolTip= "Preview Print All the view",
      });

      Chrome.PlaceWidget(PrintModulePrintTextBlockWidgetId, RibbonChromeManager.RIBBON_COMMANDAREA_NAME, "Print/Print", new InitialShowWindowButtonParameters
      {
        Text = "TextBlock only View",
        WindowFactoryID = PrintModulePrintTextBlockViewId,
        ToolTip = "Create Textblock only view for printing",
        InitialParameters = new InitialWindowParameters
        {
          InitialLocation = Core.InitialLocation.Floating,
          Singleton = true, 
          Width = 800,
          Height = 100,
          ShowFlashBorder = true,
        }
      });

      Register.RegisterWindowFactory(DemoViewWindowId, CreateDemoView);

      Chrome.PlaceWidget(DemoViewId, RibbonChromeManager.RIBBON_COMMANDAREA_NAME, "Print/Demo", new InitialShowWindowButtonParameters
      {
        Text = "Click me to trap the exception",
        WindowFactoryID = DemoViewWindowId,
        InitialParameters = new InitialWindowParameters
        {
          //InitialLocation = Core.InitialLocation.Floating, 
          //InitialLocation = Core.InitialLocation.DockInNewTab
          InitialLocation = Core.InitialLocation.DockInNewTab
          
        },
        ToolTip = "Click me to trap the execution",
        Enabled = true,
      }
      );


      Chrome.PlaceWidget(PrintModuleWin32WidgetId, RibbonChromeManager.RIBBON_COMMANDAREA_NAME, "Print/Demo", new InitialShowWindowButtonParameters
      {
        Text = "Win32 View",
        WindowFactoryID = PrintModuleWin32ViewId,
        InitialParameters = new InitialWindowParameters
        {
        },
        ToolTip = "Click me to create a win32 View",
        Enabled = true,
      });


      var printWidgetId = Chrome.PrintButtonFactory();
      Chrome.PlaceWidget(printWidgetId, RibbonChromeManager.RIBBON_COMMANDAREA_NAME, "Print/Menu", new InitialWidgetParameters());

      Chrome.PlaceWidget(Chrome.PrintPreviewFactory(), RibbonChromeManager.RIBBON_COMMANDAREA_NAME,  "Print/Menu", new InitialWidgetParameters());
    }


    private bool PrintTextBlockOnlyView(IWindowViewContainer container_, XDocument state_)
    {
      container_.Title = "TextBlock only view Demo";

      var textBlock = new TextBlock
      {
        Text = @" Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit amet, consectetur, adipisci[ng] velit, sed quia non numquam [do] eius modi tempora inci[di]dunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur?",
        TextWrapping = TextWrapping.Wrap,
        Height = 100, 
        Width = 800,
        Foreground = new SolidColorBrush(Colors.Red),
        //Background = new SolidColorBrush(Colors.Blue),
      };

      container_.Content = textBlock;
      return true;
    }

    private bool CreateDemoView(IWindowViewContainer container_, XDocument state_)
    {
      container_.Title = null;
      container_.Content = new TextBlock { Text = "This is a demo view" };
      return true;
    }

    private void PrintView(object sender_, EventArgs e)
    {
      if (ViewContainer == null)
      {
        TaskDialog.ShowMessage("Please create the view first");
        return;
      }
      var printManager = Chrome as IPrintManager;
      printManager.Print(ViewContainer);
    }


    private void PrintViewAll(object sender, EventArgs e)
    {
      var printManager = Chrome as IPrintManager;
      printManager.PrintAll();
    }

    private void PrintPreview(object sender_, EventArgs e)
    {
      if (ViewContainer == null)
      {
        TaskDialog.ShowMessage("Please create the view first");
        return;
      }
      var printManager = Chrome as IPrintManager;
      printManager.PreviewPrint(ViewContainer);
    }

    private void PrintPreviewAll(object sender_, EventArgs e)
    {
      var printManager = Chrome as IPrintManager;
      printManager.PreviewPrintAll();
    }

    public IWindowViewContainer ViewContainer { get; private set; }
    
    private bool CreateView(IWindowViewContainer container, XDocument state)
    {
      PrintDemoView printDemoView = new PrintDemoView();
      container.Content = printDemoView;
      ViewContainer = container;

      ViewContainer.PrintHandler = OnPrint;
      ViewContainer.PreviewPrintHandler = OnPreviewPrint;

      return true;
    }

    private bool CreateWin32View(IWindowViewContainer container, XDocument state)
    {


      WindowsFormsHost host = new WindowsFormsHost();
      System.Windows.Forms.Form form = new System.Windows.Forms.Form();
      form.Controls.AddRange(
        new System.Windows.Forms.Control[] { 
          new System.Windows.Forms.TextBox  { Text = "New Romes", Height = 100, Width =50 },
          new System.Windows.Forms.Button { Text = "Click me"}
        });
        form.MinimumSize = new Size(120, 120);
        form.MaximumSize = new Size(300, 300);
        form.TopLevel = false;
        form.FormBorderStyle = FormBorderStyle.None; 
        host.Child = form;

        DockPanel p = new DockPanel();
        p.LastChildFill = true;
        p.Children.Add(host);
        container.Content = p;
      return true;
    }

    // Is the InitialPrintParamter necessary?
    private IPrintImage OnPrint(IWindowViewContainer container, InitialPrintParameters printParamter)
    {
      var content = ViewContainer.Content as FrameworkElement;

      //return new PrintImage(container.Content as Visual);

      return PrintControlFactory.CreatePrintImage(container, printParamter);
    }

    private IPrintImage OnPreviewPrint(IWindowViewContainer container, InitialPrintParameters printParameter)
    {
      return OnPrint(container, printParameter);
    }




    public IChromeManager Chrome { get; internal set; }

    public IChromeRegistry Register { get; internal set; }

    public static readonly string PrintModuleViewId = "PrintModuleViewId";
    public static readonly string PrintModuleWidgetId = "PrintModuleWidgetId";
    public static readonly string PrintModulePrintWidgetId = "PrintModulePrintWidgetId";
    public static readonly string PrintModulePrintPreviewWidgetId = "PrintModulePrintPreviewWidgetId";
    public static readonly string PrintModulePrintPreviewAllWidgetId = "PrintModulePrintPrevieAllWidgetId";
    public static readonly string PrintModulePrintAllWidgetId = "PrintModulePrintAllWidgetId";


    public static readonly string PrintModuleWin32WidgetId = "PrintModuleWin32WidgetId";
    public static readonly string PrintModuleWin32ViewId = "PrintModuleWin32ViewId";

    public static readonly string DemoViewWindowId = "DemoViewWindowId";
    public static readonly string DemoViewId = "DemoViewId";

    // this is for the test of the TextBlocks controls which has
    public static readonly string PrintModulePrintTextBlockWidgetId = "PrintModulePrintTextBlockWidgetId";
    public static readonly string PrintModulePrintTextBlockViewId = "PrintModulePrintTextBlockViewId";
  }
}
