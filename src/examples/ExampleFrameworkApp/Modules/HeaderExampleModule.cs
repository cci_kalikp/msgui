﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/HeaderExampleModule.cs#8 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Drawing;
using System.Windows.Controls;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using Brushes = System.Windows.Media.Brushes;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Modules
{
	public class HeaderExampleModule : IModule
	{
		private readonly IChromeRegistry m_chromeRegistry;
		private readonly IChromeManager m_chromeManager;
		private const string m_viewKey = "HeaderExampleModule-Example View";

		public HeaderExampleModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_)
		{
			m_chromeRegistry = chromeRegistry_;
			m_chromeManager = chromeManager_;
		}

		public void Initialize()
		{
			this.m_chromeRegistry.RegisterWidgetFactory("HeaderIconsView");
			this.m_chromeRegistry.RegisterWindowFactory(m_viewKey, CreateView);

			this.m_chromeManager.PlaceWidget("HeaderIconsView", "Views/Header Icons", new InitialShowWindowButtonParameters()
			{
				Image = SystemIcons.Shield.ToImageSource(),
				Text = "Header Items View",
				ToolTip = "Add a new view",

				WindowFactoryID = m_viewKey,
				InitialParameters = new InitialWindowParameters()
			}).AddToQAT();
		}

		bool CreateView(IWindowViewContainer viewContainer_, XDocument state_)
		{
			viewContainer_.Parameters.Width = 300;
			viewContainer_.Parameters.Height = 50;
			viewContainer_.HeaderItems.Add(new TextBlock()
			{
				Text = "text"
			});
		  viewContainer_.Title = "Header Demo View " + DateTime.UtcNow.ToString();
			Button orangeButton = new Button
			{
				Height = 16,
				Width = 16,
				Background = Brushes.DarkOrange
			};
			viewContainer_.HeaderItems.Add(orangeButton);
			var infolabel = new TextBlock();
			viewContainer_.HeaderItems.Add(infolabel);
			viewContainer_.Background = Brushes.Orange;

			bool flag = true;
			orangeButton.Click += (sender_, e_) =>
			{
				infolabel.Text = flag ? "Yes" : "No";
				infolabel.Background = flag ? Brushes.Green : Brushes.IndianRed;
				flag = !flag;
			};

			return true;
		}
	}
}