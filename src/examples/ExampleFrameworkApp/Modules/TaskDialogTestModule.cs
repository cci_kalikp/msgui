﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.Win7;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using TaskDialog = MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialog;

namespace MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Modules
{
    public class TaskDialogTestModule : IModule
    {
        private readonly IChromeManager m_chromeManager;
        private IChromeRegistry m_chromeRegistry;

        public TaskDialogTestModule(IChromeRegistry chromeRegistry, IChromeManager chromeManager)
        {
            m_chromeManager = chromeManager;
            m_chromeRegistry = chromeRegistry;
        }

        public void Initialize()
        {
            //m_chromeRegistry.RegisterWidgetFactory("TaskDialog1");
            //m_chromeRegistry.RegisterWidgetFactory("TaskDialog2");
            //m_chromeRegistry.RegisterWidgetFactory("TaskDialog3");
            //m_chromeRegistry.RegisterWidgetFactory("TaskDialog4");
            //m_chromeRegistry.RegisterWidgetFactory("TaskDialog5");

            m_chromeManager.PlaceWidget("TaskDialog1", "TaskDialog/TaskDialogTests",
                                        new InitialButtonParameters
                                            {
                                                Text = "Task Dialog #1",
                                                Click = delegate
                                                    {
                                                        TaskDialog.ShowMessage("message", "caption",
                                                                               TaskDialogCommonButtons.YesNoCancel,
                                                                               VistaTaskDialogIcon.Information);
                                                    }
                                            }
                );

            m_chromeManager.PlaceWidget("TaskDialog2", "TaskDialog/TaskDialogTests",
                                        new InitialButtonParameters
                                            {
                                                Text = "Task Dialog #2",
                                                Click = delegate
                                                    {
                                                        TaskDialog.ShowMessage("title", "main instruction", "content",
                                                                               "expanded info", "verification text",
                                                                               "footer text",
                                                                               TaskDialogCommonButtons.RetryCancel,
                                                                               VistaTaskDialogIcon.Shield,
                                                                               VistaTaskDialogIcon.Warning);
                                                    }
                                            }
                );

            m_chromeManager.PlaceWidget("TaskDialog3", "TaskDialog/TaskDialogTests",
                                        new InitialButtonParameters
                                            {
                                                Text = "Task Dialog #3",
                                                Click = delegate
                                                    {
                                                        TaskDialog.Show(new TaskDialogOptions
                                                            {
                                                                CommandButtons = new[] {"CB1", "CB2\nDT1"},
                                                                Content = "Expansion",
                                                                CommonButtons = TaskDialogCommonButtons.YesNoCancel,
                                                                CustomButtons = new[] {"Whatever"},
                                                                ExpandedInfo = "Expanded",
                                                                FooterIcon = VistaTaskDialogIcon.Information,
                                                                FooterText = "Footer",
                                                                MainIcon = VistaTaskDialogIcon.Error,
                                                                MainInstruction = "Instruction",
                                                                Title = "Title",
                                                                VerificationText = "Do not show this again"
                                                            });
                                                    }
                                            }
                );

            m_chromeManager.PlaceWidget("TaskDialog4", "TaskDialog/TaskDialogTests",
                                        new InitialButtonParameters
                                            {
                                                Text = "Task Dialog #4",
                                                Click = delegate
                                                    {
                                                        TaskDialog.Show(new TaskDialogOptions
                                                            {
                                                                Content = "Expansion",
                                                                CommonButtons = TaskDialogCommonButtons.YesNoCancel,
                                                                CustomButtons = new[] {"Whatever"},
                                                                ExpandedInfo = "Expanded",
                                                                FooterIcon = VistaTaskDialogIcon.Information,
                                                                FooterText = "Footer",
                                                                MainIcon = VistaTaskDialogIcon.Error,
                                                                MainInstruction = "Instruction",
                                                                RadioButtons = new[] {"RB1", "RB2\nDT2"},
                                                                Title = "Title",
                                                                VerificationText = "Do not show this again"
                                                            });
                                                    }
                                            }
                );

            m_chromeManager.PlaceWidget("TaskDialog5", "TaskDialog/TaskDialogTests",
                                        new InitialButtonParameters
                                            {
                                                Text = "Task Dialog #5",
                                                Click = delegate
                                                    {
                                                        TaskDialog.Show(new TaskDialogOptions
                                                            {
                                                                Content = "Expansion",
                                                                CommonButtons = TaskDialogCommonButtons.YesNoCancel,
                                                                CustomButtons = new[] {"Whatever"},
                                                                ExpandedInfo = "Expanded",
                                                                MainIcon = VistaTaskDialogIcon.Error,
                                                                MainInstruction = "Instruction",
                                                                Title = "Title",
                                                                VerificationText = "Do not show this again",
                                                                UserInputEnabled = true,
                                                                InitialUserInput = "initial user input",
                                                            });
                                                    }
                                            }
                );

            var handle = new TaskDialogHandle();
            var actions = new Dictionary<string, Action>();
            actions.Add("More", () => { handle.ProgressBarCurrentValue += 1; });
            actions.Add("Less", () => { handle.ProgressBarCurrentValue -= 1; });
            m_chromeManager.PlaceWidget("TaskDialog6", "TaskDialog/TaskDialogTests",
                                        new InitialButtonParameters
                                            {
                                                Text = "Task Dialog #6",
                                                Click = delegate
                                                    {
                                                        TaskDialog.Show(new TaskDialogOptions
                                                            {
                                                                Content = "Expansion",
                                                                CustomButtons = new[] {"More", "Less", "Close"},
                                                                ProgressBarInitialValue = 5,
                                                                ProgressBarMinimum = 0,
                                                                ProgressBarMaximum = 10,
                                                                CustomButtonActions = actions,
                                                                AllowDialogCancellation = true,
                                                            }, handle);
                                                    }
                                            }
                );

            if (Desktop.GUI.Controls.TaskDialogInterop.Win7.TaskDialog.Supported)
                m_chromeManager.PlaceWidget("TaskDialog7", "TaskDialog/TaskDialogTests",
                                            new InitialButtonParameters
                                                {
                                                    Text = "Task Dialog #7",
                                                    Click = delegate
                                                        {
                                                            new Desktop.GUI.Controls.TaskDialogInterop.Win7.TaskDialog
                                                                {
                                                                    Caption = "caption",
                                                                    CheckBoxText = "Checkbox text",
                                                                    CollapsedControlText = "Collapsed",
                                                                    Content = "Content <a href=\"xxx\">aaa</a>",
                                                                    ExpandedControlText = "Expanded control",
                                                                    ExpandedText = "Expanded",
                                                                    ExpansionMode =
                                                                        TaskDialogExpandedInformationLocation
                                                                            .ExpandContent,
                                                                    FooterIcon = TaskDialogStandardIcon.Information,
                                                                    FooterText = "Footertext",
                                                                    HyperlinksEnabled = true,
                                                                    Instruction = "instrcution",
                                                                    Expanded = false,
                                                                    Cancelable = true,
                                                                    CheckBoxChecked = false,
                                                                    MainIcon = TaskDialogStandardIcon.Warning,
                                                                    Marquee =
                                                                        new TaskDialogMarquee
                                                                            {
                                                                                Name = "marquee",
                                                                                State = TaskDialogProgressBarState.Normal
                                                                            },
                                                                    //ProgressBar = new TaskDialogProgressBar { },
                                                                    StandardButtons =
                                                                        TaskDialogStandardButtons.YesNoCancel,
                                                                    StartupLocation =
                                                                        TaskDialogStartupLocation.CenterScreen
                                                                }.Show();
                                                        }
                                                }
                    );
        }
    }
}