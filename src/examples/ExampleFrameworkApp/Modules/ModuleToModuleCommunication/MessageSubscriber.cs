﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/ModuleToModuleCommunication/MessageSubscriber.cs#7 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Drawing;
using System.Reactive.Linq;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Data;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Presentation.Events;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace MorganStanley.MSDotNet.MSGui.Modules.ModuleToModuleCommunication
{
    /// <summary>
    /// Module that registers an int->double and a double->string adapter and then subscribes to and shows published string messages
    /// </summary>
    public class MessageSubscriber : IModule
    {
        private const string m_viewKey = "Subscriber-Example View";
        private readonly IControlParadigmRegistrator m_ribbon;
        private readonly IViewManager m_viewManager;
        private readonly ICommunicator m_communicator;
        private readonly IEventAggregator m_eventAggregator;
        
        public MessageSubscriber(IControlParadigmRegistrator ribbon_, IViewManager viewManager_, 
            ICommunicator communicator_, IEventAggregator eventAggregator_,
            IAdapterService adpaterService)
        {
            m_ribbon = ribbon_;
            m_viewManager = viewManager_;
            m_communicator = communicator_;
            m_eventAggregator = eventAggregator_;

            

            adpaterService.AddAdapter<int, string>( s => s.ToString());
        }

        #region IModule Members

        public void Initialize()
        {
            m_viewManager.AddViewCreator(m_viewKey, CreateView);

            var rb = new RibbonButton(
              new Icon(SystemIcons.Shield, 32, 32),
              "Message Subscription",
              "Message Subscription",
              (a_, b_) => m_viewManager.CreateView(m_viewKey));

            m_ribbon.AddRibbonItem(
              "Messages",
              "Subscription",
              new WrapperPannelRibbonItem(RibbonItemOrientation.Horizontal, rb));
        }

        #endregion

        private void CreateView(IViewContainer viewContainer_)
        {
          //var subscriberViewContainer_ = viewContainer_;
          //var publisherViewContainer_ = viewContainer_;
          ////subscriber:
          //// registering
          //ISubscriber<SubscribedMessage> subscriber = m_eventRegistrator.RegisterSubscriber<SubscribedMessage>(subscriberViewContainer_);
          //// subscription message handler prints message to console
          //subscriber.Subscribe(msg_ => msg_.Process());
          //// adapter to adapt int messages to string
          //m_adapterService.AddAdapter<PublishedMessage, SubscribedMessage>(published_ => new SubscribedMessage(published_));

          ////publisher:
          //// registering
          //IPublisher<PublishedMessage> publisher = m_eventRegistrator.RegisterPublisher<PublishedMessage>(publisherViewContainer_);

          ////link editor:
          //// check if subscriber can be connected to publisher
          //if (m_eventRegistrator.GetCompatibleSubscribers<PublishedMessage>().Contains(subscriberViewContainer_))
          //{
          //    //connecting publisher and subscriber 
          //    //(do not have to be the same type, only need adapter chains between them)
          //    m_eventRouter.Connect(publisher, subscriber);
          //}

          ////publisher:
          //// publishing
          //publisher.Publish(new PublishedMessage());

          //ISubscriber<string> subscriber = m_eventRegistrator.RegisterSubscriber<string>(viewContainer_);
            var text = new TextBlock();
            viewContainer_.Content = text;
            
            viewContainer_.Title = "Module Subscriber";

            var subscriber = m_communicator.GetSubscriber<string>();

            var filter = from message in subscriber.GetObservable()
                         select message;


            filter.Subscribe((str) => text.Dispatcher.BeginInvoke((UpdateContent)((box, msg) => box.Text +=msg), new object[] {text, str }) );   
            //m_communicator.GetObservable<string>().Subscribe();
            //subscriber.Subscribe(str_ => text.Text += str_);
            m_eventAggregator.GetEvent<ModuleToModuleMessageEvent<string>>().Subscribe(str_ =>  text.Text += str_, ThreadOption.UIThread, true);

            var evt = m_eventAggregator.GetEvent<CompositePresentationEvent<string>>();
            evt.Subscribe(str_ => text.Text += str_, ThreadOption.UIThread, true);

            var e2 = m_eventAggregator.GetEvent<MessagePublisher.MyEvent>();
            Console.WriteLine(e2.Content);
        }

        private delegate void UpdateContent(TextBlock text, string message);
    }
}
