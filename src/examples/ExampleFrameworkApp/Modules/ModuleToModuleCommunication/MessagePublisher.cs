﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/ModuleToModuleCommunication/MessagePublisher.cs#10 $
// $Change: 859787 $
// $DateTime: 2013/12/22 01:10:19 $
// $Author: hrechkin $

using System;
using System.Drawing;
using System.Windows.Controls;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Presentation.Events;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace MorganStanley.MSDotNet.MSGui.Modules.ModuleToModuleCommunication
{
    /// <summary>
    /// Module publishing the message 3 (type int) when its view is created
    /// </summary>
    public class MessagePublisher : IModule
    {
        private const string m_viewKey = "Publisher-Example View";
        private readonly IControlParadigmRegistrator m_ribbon;
        private readonly IViewManager m_viewManager;
        private readonly ICommunicator m_communicator;
        private readonly IEventAggregator m_eventAggregator;
        private readonly ApplicationInfo _application;

        public MessagePublisher(IControlParadigmRegistrator ribbon, IViewManager viewManager, ICommunicator communicator,
                                IEventAggregator eventAggregator,
                                IApplication app)
        {
            m_ribbon = ribbon;
            m_viewManager = viewManager;
            m_communicator = communicator;
            m_eventAggregator = eventAggregator;
            var instance = app.GetApplicationInstance();
            _application = new ApplicationInfo(Environment.MachineName, Environment.UserName, "MSDesktop", app.Name,
                                               instance);
        }

        #region IModule Members

        public void Initialize()
        {
            m_viewManager.AddViewCreator(m_viewKey, CreateView);

            var rb = new RibbonButton(
                new Icon(SystemIcons.Shield, 32, 32),
                "Message Publishing",
                "Message Publishing",
                (a_, b_) => m_viewManager.CreateView(m_viewKey));

            m_ribbon.AddRibbonItem(
                "Messages",
                "Publish",
                new WrapperPannelRibbonItem(RibbonItemOrientation.Horizontal, rb));
        }

        #endregion



        private void CreateView(IViewContainer viewContainer_)
        {
            //IPublisher<int> publisher = m_eventRegistrator.RegisterPublisher<int>(viewContainer_);
            viewContainer_.Title = "Module Publisher";
            var text = new TextBlock();
            viewContainer_.Content = text;
            //publisher.Publish(3);
            var publihser = m_communicator.GetPublisher<int>();
            publihser.Publish(1); //to all
            // publihser.PublishRelativeToCurrent(2, CommunicationTargetFilter.Machine);
            // publihser.Publish(3, CommunicationTargetFilter.Application, "ExampleFrameworkApp");
            // publihser.Publish(4, CommunicationTargetFilter.Machine, "OZWVM27005");
            // publihser.PublishRelativeToCurrent(5, CommunicationTargetFilter.Application);
            // publihser.Publish(6, CommunicationTargetFilter.MachineApplication, "OZWVM27005", "ExampleFrameworkApp");

            m_eventAggregator.GetEvent<ModuleToModuleMessageEvent<int>>().Publish(7);

            m_eventAggregator.GetEvent<ModuleToModuleMessageEvent<double>>().Publish(3.2);

            var evt = m_eventAggregator.GetEvent<CompositePresentationEvent<string>>();
            evt.Publish("This is out of M2M");

            var e2 = m_eventAggregator.GetEvent<MyEvent>();
            e2.Content = "This is not for publishing";

            var e3 = m_eventAggregator.GetEvent<MyEvent>();
            Console.WriteLine(e3.Content);
        }

        public class MyEvent : EventBase
        {
            public string Content { get; set; }
        }
    }
}
