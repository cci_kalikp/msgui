﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using Infragistics.Windows.Ribbon;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Controls.SplitButton;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 

namespace MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Modules
{
    public class DropDownExampleModule:IModule
    { 
        private readonly IChromeManager chromeManager;
        private readonly IChromeRegistry chromeRegistry;
        private const string PopupWindowID = "PopupWindowID";
        public DropDownExampleModule(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_)
        {
            this.chromeManager = chromeManager_;
            this.chromeRegistry = chromeRegistry_;
        }

        public void Initialize()
        {
            chromeRegistry.RegisterWindowFactory(PopupWindowID, CreateView);

            IWidgetViewContainer dropdown = null;
            dropdown = chromeManager.Ribbon["Drop Down"]["Small Buttons"].AddWidget("Dropdown Button Small", new InitialDropdownButtonParameters()
            {
                Text = "DropDown Button",
                Image = new BitmapImage(new Uri(@"pack://application:,,,/ExampleFrameworkApp;component/Resources/app1.png")),
                Size = ButtonSize.Small,
                Opening = (sender_, args_) =>
                {
                    MenuTool menuTool = sender_ as MenuTool;
                    if (menuTool != null)
                    {
                        menuTool.ItemsSource = GetDropDownItemsSource();
                    }

                },
            });
            dropdown.AddWidget(
                    new InitialButtonParameters
                    {
                        Text = "Item1",
                        Image = new BitmapImage(new Uri(@"pack://application:,,,/ExampleFrameworkApp;component/Resources/app2.png")),
                        Click = (sender_, args_) => MessageBox.Show("Item Clicked")

                    });
              

            dropdown = chromeManager.Ribbon["Drop Down"]["Small Buttons"].AddWidget("Split Button Small", new InitialSplitButtonParameters()
            {
                Text = "Split Button",
                Image = new BitmapImage(new Uri(@"pack://application:,,,/ExampleFrameworkApp;component/Resources/app1.png")),
                //IsDropDownEnabled = false,
                Opening = (sender_, args_) =>
                {
                    SplitButton splitButton = sender_ as SplitButton;
                    if (splitButton != null)
                    {
                        splitButton.ItemsSource = GetDropDownItemsSource();
                    }

                },
                Size = ButtonSize.Small
            });



            dropdown = chromeManager.Ribbon["Drop Down"]["Large Buttons"].AddWidget("Dropdown Button Large", new InitialDropdownButtonParameters()
            {
                Text = "DropDown Button",
                Image = new BitmapImage(new Uri(@"pack://application:,,,/ExampleFrameworkApp;component/Resources/app1.png")),
            });
            FillDropDown(dropdown);


            dropdown = chromeManager.Ribbon["Drop Down"]["Large Buttons"].AddWidget("Split Button Large", new InitialSplitButtonParameters()
            {
                Text = "Split Button",
                Image = new BitmapImage(new Uri(@"pack://application:,,,/ExampleFrameworkApp;component/Resources/app1.png")),
                Click = (sender_, args_) => MessageBox.Show("Header Clicked"),
            });
            FillDropDown(dropdown);
        }

        private void FillDropDown(IWidgetViewContainer dropdown_)
        {
            dropdown_.Clear();
            dropdown_.AddWidget(
                                new InitialButtonParameters
                                {
                                    Text = "Item1",
                                    Image = new BitmapImage(new Uri(@"pack://application:,,,/ExampleFrameworkApp;component/Resources/app2.png")),
                                    Click = (sender_, args_) =>
                                        {
                                            MessageBox.Show("Item Clicked");
                                            chromeManager.CreateWindow(PopupWindowID, new InitialWindowParameters());
                                        }

                                });
            dropdown_.AddWidget(
                    new InitialButtonParameters
                    {
                        Text = "Item2",
                        Click = (sender_, args_) => chromeManager.CreateWindow(PopupWindowID, new InitialWindowParameters())
                    });
        }

        private IEnumerable GetDropDownItemsSource()
        {
            return new object[]
                                {
                                    new ToolMenuItem
                                        {
                                            Header = "Item1", 
                                            Icon =new Image() {Source =new BitmapImage(new Uri(@"pack://application:,,,/ExampleFrameworkApp;component/Resources/app2.png"))},
                                        },
                                    new ToolMenuItem
                                        {
                                            Header = "Item2"
                                        }
                                };
        }

        private IEnumerable GetDropDownItemsSource2()
        {
            BitmapImage source = new BitmapImage();
            source.BeginInit();
            source.UriSource = new Uri(@"pack://application:,,,/ExampleFrameworkApp;component/Resources/app2.png");
            source.DecodePixelHeight = 16;
            source.DecodePixelWidth = 16;
            source.EndInit();

            return new object[]
                                {
                                    new MenuItem
                                        {
                                            Header = "Item1", 
                                            Icon =new Image() {Source = source}
                                        },
                                    new MenuItem
                                        {
                                            Header = "Item2"
                                        }
                                };
        }
        private bool CreateView(IWindowViewContainer container_, XDocument state_)
        {
            Grid grid = new Grid();
            grid.RowDefinitions.Add(new RowDefinition(){Height = new GridLength(30)});
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(30) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(30) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(30) });

            var button = new SplitButton();
            button.Caption = "Button1";
            button.Image = new BitmapImage(new Uri(@"pack://application:,,,/ExampleFrameworkApp;component/Resources/app1.png"));
            button.ItemsSource = GetDropDownItemsSource2();
            Grid.SetRow(button, 0);
            grid.Children.Add(button);

            button = new SplitButton();
            button.Caption = "Button2";
            button.Image = new BitmapImage(new Uri(@"pack://application:,,,/ExampleFrameworkApp;component/Resources/app1.png"));
            button.Mode = SplitButtonMode.Button;
            button.ItemsSource = GetDropDownItemsSource2();
            Grid.SetRow(button, 1);
            grid.Children.Add(button);

            button = new SplitButton();
            button.Caption = "Button3";
            button.Image = new BitmapImage(new Uri(@"pack://application:,,,/ExampleFrameworkApp;component/Resources/app1.png"));
            button.Mode = SplitButtonMode.Dropdown;
            button.ItemsSource = GetDropDownItemsSource2();
            Grid.SetRow(button, 2);
            grid.Children.Add(button);

            button = new SplitButton();
            button.Caption = "Button4";
            button.Image = new BitmapImage(new Uri(@"pack://application:,,,/ExampleFrameworkApp;component/Resources/app1.png"));
            button.ItemsSource = GetDropDownItemsSource2();
            button.Opened += (s, e) => button.IsDropDownEnabled = false;
            Grid.SetRow(button, 3);
            grid.Children.Add(button);

            container_.Header.AddWidget(new InitialSplitButtonParameters()
                {
                    Text = "Split",
                    Click = (sender_, args_) => MessageBox.Show("Header Clicked")
                }).AddWidget(new InitialButtonParameters()
                    {
                        Text = "Test",
                        Click = (sender_, args_) => MessageBox.Show("Item Clicked")
                    });

            container_.Header.AddWidget(new InitialDropdownButtonParameters()
            {
                Text = "DropDown" 
            }).AddWidget(new InitialButtonParameters()
            {
                Text = "Test",
                Click = (sender_, args_) => MessageBox.Show("Item Clicked")
            });

            container_.Content = grid;

            return true;
        }
         
    }
}
