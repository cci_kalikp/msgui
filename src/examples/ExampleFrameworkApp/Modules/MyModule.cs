/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/MyModule.cs#45 $
// $Change: 894857 $
// $DateTime: 2014/08/29 07:00:19 $
// $Author: milosp $

using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Infragistics.Windows.Editors;
using Infragistics.Windows.Ribbon;
using MSDesktop.Omnibox.UI;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Windows.Media.Imaging;
using System.IO;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using System.Windows.Data;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Modules
{
    public class MyModule : IModule
    {
        private const string m_viewKey = "MyModule-Example View";
        private readonly IAlert m_alert;
        private readonly IControlParadigmRegistrator m_ribbon;
        private readonly ISplashScreen m_splashScreen;
        private readonly IViewManager m_viewManager;
        private readonly IShellWindowMenuFacade m_shellWindowMenuFacade;
        private readonly IChromeManager m_chrom;
        private readonly IChromeRegistry m_registry;

        public MyModule(IControlParadigmRegistrator ribbon_, ISplashScreen splashScreen_, IViewManager viewManager_,
                        IAlert alert, IShellWindowMenuFacade shellWindowMenuFacade, IChromeManager chrome_,
                        IChromeRegistry registry_)
        {
            m_ribbon = ribbon_;
            m_splashScreen = splashScreen_;
            m_viewManager = viewManager_;
            m_alert = alert;
            m_shellWindowMenuFacade = shellWindowMenuFacade;

            this.m_chrom = chrome_;
            this.m_registry = registry_;


            this.m_chrom.AddButton("sublayouttest_load", RibbonChromeManager.MENU_COMMANDAREA_NAME, "",
                                   new InitialButtonParameters()
                                   {
                                       Text = "Load workspace...",
                                       Click = OnLoadWorkspaceClick
                                   });

            this.m_chrom.AddButton("sublayouttest_save", RibbonChromeManager.MENU_COMMANDAREA_NAME, "",
                                   new InitialButtonParameters()
                                   {
                                       Text = "Save workspace...",
                                       Click = OnSaveWorkspaceClick
                                   });
            m_chrom.Ribbon["Help"]["Support"].AddWidget("MSGui Colors", new InitialShowWindowButtonParameters()
            {
                WindowFactoryID = ThemeExtensions.MSGuiColorPaletteViewId,
                Text = "MSGui Colors",
                InitialParameters = new InitialWindowParameters()
                {
                    InitialLocation = InitialLocation.FloatingOnly,
                    IsModal = true
                }
            });
            m_chrom.Ribbon["Help"]["Omnibox"].AddWidget(OmniboxModule.WidgetFactoryName, new InitialWidgetParameters());
        }

        private void OnLoadWorkspaceClick(object sender, EventArgs e)
        {
            var tdResult = TaskDialog.Show(new TaskDialogOptions()
            {
                UserInputEnabled = true,
                Content = "Enter a name for the saved workspace:",
                Title = "Load workspace",
                CommonButtons = TaskDialogCommonButtons.OKCancel
            });

            if (tdResult.Result == TaskDialogSimpleResult.Ok)
            {
                if (tdResult.UserInput.Trim() != string.Empty)
                {
                    (m_chrom as ChromeManagerBase).LoadViews(tdResult.UserInput);
                }
                else
                {
                    TaskDialog.ShowMessage("Please enter a name.");
                }
            }
        }

        private void OnSaveWorkspaceClick(object sender, EventArgs e)
        {
            var tdResult = TaskDialog.Show(new TaskDialogOptions()
            {
                UserInputEnabled = true,
                Content = "Enter a name for the workspace tp save:",
                Title = "Save workspace",
                CommonButtons = TaskDialogCommonButtons.OKCancel
            });

            if (tdResult.Result == TaskDialogSimpleResult.Ok)
            {
                if (tdResult.UserInput.Trim() != string.Empty)
                {
                    (m_chrom as ChromeManagerBase).SaveViews(null, tdResult.UserInput);
                }
                else
                {
                    TaskDialog.ShowMessage("Please enter a name.");
                }
            }
        }

        #region IModule Members
        public void Initialize()
        {
            m_splashScreen.SetText("Initializing MyModule");

            m_viewManager.AddViewCreator(m_viewKey, CreateView);

            var rb = new RibbonButton(
              new Icon(SystemIcons.Shield, 32, 32),
              "Add View",
              "Add a new view",
              OnClickAddView);

            var rb2 = new RibbonButton(
              new Icon(SystemIcons.Shield, 32, 32),
              "Show dialog",
              "Show a modal dialog",
              OnClickShowDialog);

            m_ribbon.AddRibbonItem(
              "Views",
              "Creation",
              new WrapperPannelRibbonItem(RibbonItemOrientation.Horizontal, rb));
            m_ribbon.AddRibbonItem(
              "Views",
              "Creation",
              new WrapperPannelRibbonItem(RibbonItemOrientation.Horizontal, rb2));


            m_ribbon.AddRibbonItem("Test", "test",
                                   new RibbonButton("text",
                                                    (sender, args) =>
                                                          ((ButtonTool)m_shellWindowMenuFacade.SaveLayoutButton).RaiseEvent(new RoutedEventArgs(ButtonTool.ClickEvent))));
            //m_ribbon.Icon = new Icon(@"\\msad\root\eu\eubo\yq\users\smulovic\Xenon.ico");


            m_viewManager.Activated += new EventHandler<ViewEventArgs>(ViewActivated);


            // below show how you can change the ribbon's icon
            //  Uri.ToIcon is a extension method in MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
            var icon = new Uri(@"pack://application:,,,/Resources/icon.ico", UriKind.RelativeOrAbsolute).ToIcon(new PngBitmapEncoder());
            ((RibbonChromeManager)m_chrom).SetRibbonIcon(icon);

            //((RibbonChromeManager)m_chrom).SetRibbonIcon(SystemIcons.Shield);
            m_registry.RegisterWidgetFactory(Lock_Widget_Id);
            // Place the widget to the Lock Command Area,
            // you may need to call the 
            //  boot.ForceLockerRegistrations(true);
            // in order to enforce the Locker command area
            // it is like the Widget sinkhole, while the user can push the widget to the Lock Area,
            // and then rendered all at once. 
            //
            m_chrom.PlaceWidget(Lock_Widget_Id, RibbonChromeManager.LOCKER_COMMANDAREA_NAME, "locker", new InitialShowWindowButtonParameters
            {
                Text = "test",
                Enabled = true,
            });

            // the tabCell area is the aread where you can push widget to the 
            // bar that is on the header of the dock Manager.
            m_chrom.PlaceWidget(Lock_Widget_Id, ChromeArea.TabWell, "left", null);


            m_registry.RegisterDialogFactory(Model_Dialog_Window_Id, dw =>
            {
                dw.Content = new Button()
                {
                    Width = 140,
                    Height = 80,
                    Content = "foo"
                };
                dw.Title = "Foo";
                dw.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                dw.ResizeMode = ResizeMode.NoResize;
                (dw).SizeToContent = SizeToContent.WidthAndHeight;
                return true;
            });

            m_chrom.PlaceWidget(Model_Dialog_Window_Id, "Dialog/Test", new InitialButtonParameters
            {
                Text = "Dialog",
                Click = (s, e) =>
                {
                    var d = m_chrom.CreateDialog(Model_Dialog_Window_Id);
                    d.ShowDialog();
                }
            });
        }

        private static readonly string Model_Dialog_Window_Id = "Model_Dialog_Window_Id";


        public void ViewActivated(object sender, ViewEventArgs e)
        {
            Console.WriteLine("new view activated is ", e.ViewContainer);
        }


        #endregion

        private void CreateView(IViewContainer viewContainer_)
        {
            var tb = new TextBlock
            {
                Text = "Hello world",
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = HorizontalAlignment.Left
            };

            var list = new ObservableCollection<string>();
            for (int i = 0; i < 10; i++)
            {
                list.Add(i + " item");
            }

            var combo = new XamComboEditor
            {
                Height = 25,
                ItemsSource = list,
                IsEditable = true
            };

            var alertbutton = new Button { Content = "Send alert!" };
            alertbutton.Click += delegate { m_alert.SubmitAlert(new Alert { Text = "Alert!" }); };
            var criticalalertbutton = new Button { Content = "Send critical alert!" };
            criticalalertbutton.Click +=
                    delegate { m_alert.SubmitAlert(new Alert { Text = "Alert!", AlertLevel = AlertLevel.Critical }); };

            var flashbutton = new Button { Content = "Flash the window!" };
            flashbutton.Click += delegate
            {
                foreach (IViewContainer viewContainer in m_viewManager.Views)
                {
                    viewContainer.Flash();
                }



            };

            var activatebutton = new Button { Content = "Activate" };

            activatebutton.Click += delegate
            {
                foreach (IViewContainer viewContainer in
                  m_viewManager.Views.Where(viewContainer => viewContainer.Title == "Third example"))
                {
                    viewContainer.Activate();
                }
            };

            var resizebutton = new Button { Content = "Resize me!" };
            resizebutton.Click += delegate { viewContainer_.Width += 100; };

            var sp = new StackPanel();
            sp.Children.Add(tb);
            sp.Children.Add(combo);
            sp.Children.Add(alertbutton);
            sp.Children.Add(criticalalertbutton);
            sp.Children.Add(flashbutton);
            sp.Children.Add(resizebutton);
            sp.Children.Add(activatebutton);
            sp.Height = 300;

            viewContainer_.Content = sp;
            viewContainer_.Title = "New View";
            viewContainer_.Icon = new Icon(SystemIcons.Exclamation, 16, 16);

            var saveButton = new Button { Content = "Save" };
            saveButton.Click += delegate { m_viewManager.SaveViewContainer(viewContainer_); };
            viewContainer_.HeaderItems.Add(new Viewbox { Child = saveButton });




        }
        private const string Lock_Widget_Id = "Lock_Widget_Id";



        void OnClickShowDialog(object sender, EventArgs e)
        {
            IDialogWindow d = m_viewManager.CreateDialog();

            var s = new StackPanel() { Orientation = System.Windows.Controls.Orientation.Vertical };

            var c1 = new CheckBox() { Content = "Minimize visible" };
            c1.SetBinding(CheckBox.IsCheckedProperty, new Binding("MinimizeVisible")
            {
                Source = d,
                Mode = BindingMode.TwoWay,
            }
            );

            var c2 = new CheckBox() { Content = "Maximize visible" };
            c2.SetBinding(CheckBox.IsCheckedProperty, new Binding("MaximizeVisible")
            {
                Source = d,
                Mode = BindingMode.TwoWay,
            }
            );

            var c3 = new CheckBox() { Content = "Close visible" };
            c3.SetBinding(CheckBox.IsCheckedProperty, new Binding("CloseVisible")
            {
                Source = d,
                Mode = BindingMode.TwoWay,
            }
            );

            s.Children.Add(c1);
            s.Children.Add(c2);
            s.Children.Add(c3);

            d.Width = 400;
            d.Height = 300;
            d.Content = s;

            d.MinimizeVisible = false;
            d.MaximizeVisible = false;
            d.CloseVisible = true;

            d.ShowDialog();
        }

        private void OnClickAddView(object sender, EventArgs e)
        {
            m_viewManager.CreateView(m_viewKey);
        }
    }
}
