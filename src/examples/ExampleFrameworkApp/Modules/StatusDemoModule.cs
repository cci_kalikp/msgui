﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/StatusDemoModule.cs#24 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Windows.Controls;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;

namespace MorganStanley.MSDotNet.MSGui.Modules
{
  public class StatusDemoModule : IModule
  {
    private readonly IViewManager m_viewManager;
    private readonly IControlParadigmRegistrator m_ribbon;
    private readonly IStatusBar m_statusBar;

    public StatusDemoModule(
      IControlParadigmRegistrator ribbon_,
      IViewManager viewManager_,
      IStatusBar statusBar_)
    {
      m_ribbon = ribbon_;
      m_viewManager = viewManager_;
      m_statusBar = statusBar_;
    }

    public void Initialize()
    {
      //m_statusBar.MainItem.StatusUpdated += MainStatusUpdated;
      RibbonButton rbChgStatus = new RibbonButton(new Icon(SystemIcons.WinLogo, 32, 32), "Change Status", RibbonButtonSize.Large, "Change status of the status bar.", new EventHandler(OnChangeStatusClick));
      m_ribbon.AddRibbonItem("Views", "Status", rbChgStatus);      

      string progBarKey = "StatusDemoModuleProgressBar";
      if (!m_statusBar.ContainsItem(progBarKey))
      {
        m_statusBar.AddProgressBarStatusItem(progBarKey, 10, 20, StatusBarItemAlignment.Right);
      }

      m_statusBar.AddSeparator(StatusBarItemAlignment.Right);
      string textItemKey = m_statusBar.AddTextStatusItem(StatusBarItemAlignment.Right);
      //m_statusBar[textItemKey].UpdateStatus(StatusLevel.Information, "Hello!");

      EventHandler rhAdd = null;
      rhAdd += delegate
      {
        if (value < 20)
        {
          value++;
        }
        m_statusBar[progBarKey].UpdateStatus(StatusLevel.Information, value.ToString());
      };

      RibbonButton btnAdd = new RibbonButton("+", "Add to status bar", rhAdd);

      EventHandler rhMinus = null;
      rhMinus += delegate
      {
        if (value > 10)
        {
          value--;
        }
        m_statusBar[progBarKey].UpdateStatus(StatusLevel.Information, value.ToString());
      };
      RibbonButton btnMinus = new RibbonButton("- ", "Minus the status bar", rhMinus);

      string id = "StatusDemoModuleHistoryView";
      m_viewManager.AddViewCreator(id, OnCreateView);

      m_ribbon.AddRibbonItem("Views", "Status",
        new WrapperPannelRibbonItem(RibbonItemOrientation.Vertical,          
          new RibbonButton(
            new Icon(SystemIcons.Information, 16, 16), 
            "Show", 
            RibbonButtonSize.Small, 
            "Show the status bar.", 
            (x,y) => m_viewManager.CreateView(id)),
          new RibbonButtonGroup(btnAdd, btnMinus)));
    }

    private double value = 10;
    private bool m_flag = true;
    void OnChangeStatusClick(object sender, System.EventArgs e)
    {
      if (m_flag)
      {
        m_statusBar.MainItem.UpdateStatus(StatusLevel.Information, "Hello!");
      }
      else
      {
        m_statusBar.MainItem.UpdateStatus(StatusLevel.Critical, "Alert!");
      }
      m_flag = !m_flag;
    }

    void OnCreateView(IViewContainer viewContainer_)
    {
      ListBox list = new ListBox();
      list.ItemsSource = m_statuses;
      viewContainer_.Content = list;
      viewContainer_.Title = "Status history";
      viewContainer_.Icon = new Icon(SystemIcons.Information, 16, 16);
    }
    
    private ObservableCollection<string> m_statuses = new ObservableCollection<string>();
    void MainStatusUpdated(object sender_, StatusUpdatedEventArgs e_)
    {
      m_statuses.Add("Text: " + e_.Text + "; Time: " + e_.Time);
    }
  }
}
