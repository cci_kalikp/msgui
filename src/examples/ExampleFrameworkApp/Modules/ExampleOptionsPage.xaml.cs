﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/ExampleOptionsPage.xaml.cs#7 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Windows;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MorganStanley.MSDotNet.MSGui.Modules
{
  /// <summary>
  /// Interaction logic for UserControl1.xaml
  /// </summary>
  public partial class ExampleOptionsPage : UserControl, IOptionView
  {
    public ExampleOptionsPage()
    {
      InitializeComponent();
    }

    public void OnOK()
    {
      //Done!
    }

    public void OnCancel()
    {
      
    }

    public void OnDisplay()
    {
      //assign values
    }

    public void OnHelp()
    {
      TaskDialog.ShowMessage("Helping...");
    }

    public UIElement Content
    {
      get
      {
        return this;
      }
    }
  }
}
