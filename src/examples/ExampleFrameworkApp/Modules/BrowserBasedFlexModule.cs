﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/BrowserBasedFlexModule.cs#9 $
// $Change: 861162 $
// $DateTime: 2014/01/08 12:20:01 $
// $Author: anbuy $

using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Controls;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.FlexModule;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Impl.View;
using System.Windows.Media.Imaging;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MorganStanley.MSDotNet.MSGui.Modules
{
    /// <summary>
    /// Browser based Flex module
    /// </summary>
    class BrowserBasedFlexModule : FlexModuleWebBaseImpl, IModule
    {
        #region Private Fields

		private TextBox m_headerText;
		private const string BrowserViewID = "BrowserBasedFlexView";
		private const string BrowserViewButtonID = "BrowserBasedFlexViewWidget";

        #endregion

        #region Constructor

        public BrowserBasedFlexModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_) :
            base(chromeRegistry_, chromeManager_,
                new Uri(
                    Path.Combine(
                        Path.Combine(
                            Path.GetDirectoryName(
                                Assembly.GetExecutingAssembly().Location), "Lib"), "msguitest35.swf")),
                false)
        {
		}

        #endregion

        #region IModule Members

        public override void Initialize()
        {
			m_chromeRegistry.RegisterWindowFactory(BrowserViewID, CreateView);
			m_chromeRegistry.RegisterWidgetFactory(BrowserViewButtonID, m_chromeManager.ShowWindowButtonFactory());

			m_chromeManager.PlaceWidget(BrowserViewButtonID, "Views/Flex", new InitialShowWindowButtonParameters()
			{
        Image = new Icon(MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Properties.Resources.Chart, 32, 32).ToImageSource(),
				Text = "Flex Browser",
				Enabled = true,
				ToolTip = "Open Browser Based Flex Window",

				//below two specify the window to create
				WindowFactoryID = BrowserViewID,
		InitialParameters = new InitialWindowParameters() { Singleton = true },
			});

			//put this on the QAT is well
			m_chromeManager.PlaceWidget(BrowserViewButtonID, ChromeArea.QAT, "", null);

			m_chromeRegistry.RegisterWidgetFactory("lorem ipsum", m_chromeManager.ButtonFactory());
			m_chromeManager.PlaceWidget("lorem ipsum", "Lorem/Ipsum", new InitialButtonParameters()
			{
        Image = new Icon(MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Properties.Resources.Chart, 32, 32).ToImageSource(),
				Text = "Flex Browser",
				Enabled = true,
				ToolTip = "Open Browser Based Flex Window",
				Click = (a, b) => { TaskDialog.ShowMessage("Lorem ipsum dolor sit amet"); }
			});

			m_chromeRegistry.RegisterWidgetFactory("dropdown_test", m_chromeManager.DropdownButtonFactory());
			m_chromeManager.PlaceWidget("dropdown_test", "ChromeManager tests/Dropdown", new InitialDropdownButtonParameters()
			{
        Image = new Icon(MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Properties.Resources.Chart, 32, 32).ToImageSource(),
				Text = "Lorem ipsum",
				Enabled = true,
				ToolTip = "Open Me!"
			});

			m_chromeRegistry.RegisterWidgetFactory("dropdown_test_content1", m_chromeManager.ButtonFactory());
			m_chromeRegistry.RegisterWidgetFactory("dropdown_test_content2", m_chromeManager.ButtonFactory());

			m_chromeManager.PlaceWidget("dropdown_test_content1", "ChromeManager tests/Dropdown/dropdown_test", new InitialButtonParameters()
			{
				Text = "dolor sit amet"
			});
			m_chromeManager.PlaceWidget("dropdown_test_content2", "ChromeManager tests/Dropdown/dropdown_test", new InitialButtonParameters()
			{
				Text = "consectetur adipiscing elit"
			});
		}

        #endregion

        #region View Creation

        protected override bool CreateView(IWindowViewContainer viewcontainer_, XDocument state)
        {
			if (base.CreateView(viewcontainer_, state))
			{
				if (!UseExternalWindow)
				{
					viewcontainer_.Title = "Browser Based Flex View";

					m_headerText = new TextBox { Text = "Text to be sent to SWF" };
					viewcontainer_.HeaderItems.Add(m_headerText);
					var sendButton = new Button { Content = "Send" };
					sendButton.Click += (a_, b_) => CallToFlex("newMessage", m_headerText.Text);
					viewcontainer_.HeaderItems.Add(sendButton);
				}

				return true;
			}

			return false;
        }

        #endregion

        #region Communication With Flex

        protected override object CallFromFlex(string functionName_, object[] arguments_)
        {
            if(functionName_.Equals("newMessage")  && arguments_ != null && arguments_.Length > 0)
            {
                NewMessage(arguments_[0] as string);
            }
            return null;
        }

        /// <summary>
        /// Called to notify C# of a new message from the SWF IM client
        /// </summary>
        /// <param name="message_"></param>
        private void NewMessage(string message_)
        {
            m_headerText.Text = message_;
        }

        #endregion

    }
}
