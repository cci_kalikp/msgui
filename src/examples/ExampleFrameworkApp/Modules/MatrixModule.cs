﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2012 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/MatrixModule.cs#4 $
// $Change: 861162 $
// $DateTime: 2014/01/08 12:20:01 $
// $Author: anbuy $

using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Controls;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.FlexModule;
using MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Properties;

namespace MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Modules
{
	public class MatrixModule : FlexModuleFlashPlayerBaseImpl, IModule
	{
		#region Private Fields

		private const string BrowserViewId = "MatrixModuleView";
		private const string BrowserViewButtonId = "MatrixModuleWidget";

		#endregion Private Fields

		#region Constructor

		public MatrixModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_) :
			base(chromeRegistry_, chromeManager_,
			new Uri(
					Path.Combine(
						Path.Combine(
							Path.GetDirectoryName(
								Assembly.GetExecutingAssembly().Location), "Lib"), "WidgetRunner.swf"))
			
					)
		{
		}

		#endregion Constructor

		public override void Initialize()
		{
			m_chromeRegistry.RegisterWindowFactory(BrowserViewId, CreateView);
			m_chromeRegistry.RegisterWidgetFactory(BrowserViewButtonId, m_chromeManager.ShowWindowButtonFactory());

			m_chromeManager.PlaceWidget(BrowserViewButtonId, "Views/Flex", new InitialShowWindowButtonParameters
																			{
																				Image =
																					new Icon(Resources.Chart, 32, 32).ToImageSource(),
																				Text = "Matrix Test",
																				Enabled = true,
																				WindowFactoryID = BrowserViewId,
																				InitialParameters = new InitialWindowParameters(),
																				/*Size = ButtonSize.Dynamic,*/
																			});
		}

		#region View Creation

		protected override bool CreateView(IWindowViewContainer viewcontainer_, XDocument state)
		{
			if (base.CreateView(viewcontainer_, state))
			{
				viewcontainer_.Title = "Matrix test";

				return true;
			}
			return false;
		}

		#endregion View Creation

		#region Communication With Flex

		protected override object CallFromFlex(string functionName_, object[] arguments_)
		{
			return null;
		}

		#endregion Communication With Flex
	}
}