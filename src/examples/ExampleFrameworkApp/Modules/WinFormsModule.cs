﻿using System.Windows.Forms;
using System.Windows.Forms.Integration;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;

namespace MorganStanley.MSDotNet.MSGui.Modules
{
	public class WinFormsModule : IModule
	{
		private readonly IControlParadigmRegistrator m_ribbon;
		private readonly IViewManager m_viewManager;
		private const string m_viewKey = "HeaderExampleModule-WinForms View";

		public WinFormsModule(IControlParadigmRegistrator ribbon_, IViewManager viewManager_)
		{
			this.m_ribbon = ribbon_;
			this.m_viewManager = viewManager_;
		}

		#region IModule Members

		public void Initialize()
		{
			this.m_ribbon.AddRibbonItem("Main", "WinForms", new RibbonButton(null, "WinForms window", "Create new Windows forms window", 
                (s, e) =>
			    {
			        m_viewManager.CreateView(m_viewKey);
			    }));

			this.m_viewManager.AddViewCreator(m_viewKey, CreateWinFormsWindow);
		}

		public void CreateWinFormsWindow(IViewContainer vc)
		{
			WindowsFormsHost wfh = new WindowsFormsHost();
			Form f = new Form()
			{
				TopLevel = false
			};

			var b = new System.Windows.Forms.Button()
			{
				Text = "OH HAI WINFORMS",
				Width = 100,
				Height = 30
			};
			b.Click += (s, e) =>
			{
				vc.Height = 100;
			};

			var tb = new TextBox()
			{
				Width = 100,
				Left = 110,
				Multiline = true,
				Height = 200
			};

			f.Controls.Add(b);
			f.Controls.Add(tb);

			f.FormBorderStyle = FormBorderStyle.None;
			wfh.Child = f;
			vc.Content = wfh;
			vc.InitialViewSettings.SizingMethod = SizingMethod.Custom;
            

			//f.SizeChanged += (s, e) =>
			//{
			//    vc.Width = f.Width;
			//    vc.Height = f.Height;
			//};
		}

		#endregion
	}
}