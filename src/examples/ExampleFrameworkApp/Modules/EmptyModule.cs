﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/EmptyModule.cs#8 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Modules
{
  public class EmptyModule : IModule
  {
    private readonly IUnityContainer m_unityContainer;
    private readonly IApplication m_application;
    private readonly IViewManager m_viewManager;
    private readonly IControlParadigmRegistrator m_ribbon;
    private readonly IStatusBar m_statusBar;
    private readonly IPersistenceService m_persistenceService;
    private readonly ISplashScreen m_splashScreen;
    private readonly IApplicationOptions m_applicationOptions;
    private readonly IAlert m_alert;

    public EmptyModule(
      IUnityContainer unityContainer_,
      IApplication application_,      
      IViewManager viewManager_,
      IControlParadigmRegistrator ribbon_,
      IStatusBar statusBar_,
      IPersistenceService persistenceService_,
      ISplashScreen splashScreen_,
      IApplicationOptions applicationOptions_,
      IAlert alert_
      )
    {
      m_unityContainer = unityContainer_;
      m_application = application_;
      m_viewManager = viewManager_;
      m_ribbon = ribbon_;
      m_statusBar = statusBar_;
      m_persistenceService = persistenceService_;
      m_splashScreen = splashScreen_;
      m_applicationOptions = applicationOptions_;
      m_alert = alert_;
    }
    public void Initialize()
    {
      
    }
  }
}
