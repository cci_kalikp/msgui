﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/ViewCreatorDemoModule.cs#15 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.ComponentModel;
using System.Drawing;
using System.Windows.Controls;
using System.Windows.Data;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;
using Brushes = System.Windows.Media.Brushes;

namespace MorganStanley.MSDotNet.MSGui.Modules
{
  public class ViewCreatorDemoModule : IModule, INotifyPropertyChanged
  {
    private readonly IPersistenceService m_persistenceService;
    private readonly IControlParadigmRegistrator m_ribbon;
    private readonly IViewManager m_viewManager;
    private string m_tb3;

    public ViewCreatorDemoModule(
      IControlParadigmRegistrator ribbon_,
      IViewManager viewManager_,
      IPersistenceService persistenceService_)
    {
      m_ribbon = ribbon_;
      m_viewManager = viewManager_;
      m_persistenceService = persistenceService_;
    }

    //property used to bind value resored by registered persistor
    public string CommonText
    {
      get { return m_tb3; }
      set
      {
        m_tb3 = value;
        PropertyChangedEventHandler copy = PropertyChanged;

        if (copy != null)
        {
          copy(this, new PropertyChangedEventArgs("CommonText"));
        }
      }
    }

    #region IModule Members

    public void Initialize()
    {
      //In this example we register a delegate that assigns IViewContainer properties including the main one: Content
      //This window is not persistent.
      m_viewManager.AddViewCreator(
        "ViewCreatorDemoModule-TextBox1",
        delegate(IViewContainer view_)
          {
            view_.Content = new TextBox
                              {
                                Background = Brushes.Yellow,
                                Text = "Unsavable text"
                              };
            view_.Title = "Hello!";
          });

      // In this example we are registering two functions: one to create a window 
      // (non-zero XmlNode is passed when we are restoring a window from saved state),
      // and another to save window contents
      m_viewManager.AddViewCreator(
        "ViewCreatorDemoModule-TextBox2",
        delegate(XDocument state_, IViewContainer view_) //restore
          {
            string text = "Persistent text";
            if (state_ != null)
            {
              XElement stateElement = state_.Element("MyTextBoxState");
              if (stateElement != null && stateElement.Attribute("Text") != null)
              {
                text = stateElement.Attribute("Text").Value;
              }
            }
            view_.Content = new TextBox
                              {
                                Background = Brushes.Green,
                                Text = text
                              };
            view_.Title = "Example";
          },
        delegate(IViewContainer view_) //store
          {
            var tb = view_.Content as TextBox;
            if (tb != null)
            {
              var doc = new XDocument(
                new XElement("MyTextBoxState",
                             new XAttribute("Text", tb.Text ?? string.Empty))
                );
              return doc;
            }
            return null;
          });

      m_viewManager.AddViewCreator(
        "ViewCreatorDemoModule-TextBox2a",
        delegate(XDocument state_, IViewContainer view_) //restore
          {
            var foo = PersistenceExtensions.Restore<Foo>(state_);

            view_.Content = new TextBox
                              {
                                Background = Brushes.Green,
                                Text = foo.PersistentText ?? "Persistent text"
                              };
            view_.Title = "Example";
          },
        delegate(IViewContainer view_) //store
          {
            return
              PersistenceExtensions.Store(
              new Foo
                {
                  PersistentText = (view_.Content as TextBox).Text ?? string.Empty
                });
          });


      m_persistenceService.AddPersistor("ViewCreatorDemoModule-TextBox3Contents",
                                        delegate(XDocument state_)
                                          {
                                            if (state_ != null)
                                            {
                                              XElement stateElem = state_.Element("MyTextState");
                                              if (stateElem != null && stateElem.Attribute("Text") != null)
                                              {
                                                CommonText = stateElem.Attribute("Text").Value;
                                              }
                                            }
                                            else
                                            {
                                              CommonText = "Persistent text in non-persistent view";
                                            }
                                          },
                                        delegate
                                          {
                                            var doc = new XDocument(
                                              new XElement("MyTextState",
                                                           new XAttribute("Text", CommonText ?? string.Empty)));
                                            return doc;
                                          }
        );

      // In this example we are using non-persistent window and a registered persistor (above) - a couple of 
      // functions that are called when you save or load application state
      m_viewManager.AddViewCreator(
        "ViewCreatorDemoModule-TextBox3",
        delegate(IViewContainer view_)
          {
            var textBox = new TextBox();
            textBox.Background = Brushes.SeaGreen;
            var binding = new Binding("CommonText");
            binding.Mode = BindingMode.TwoWay;
            binding.Source = this;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            textBox.SetBinding(TextBox.TextProperty, binding);

            view_.Content = textBox;
            view_.Title = "Third example";
          });

      var rbChgStatus1 = new RibbonButton(
        new Icon(SystemIcons.Asterisk, 32, 32),
        "TextBox #1",
        RibbonButtonSize.Large,
        "Non-persistent text box.",
        //be careful here and pass exactly the id that you used when you registered ViewCreator
        (x_, y_) => m_viewManager.CreateView("ViewCreatorDemoModule-TextBox1"));
      m_ribbon.AddRibbonItem("Views", "Persistence Demo", rbChgStatus1);

      var rbChgStatus2 = new RibbonButton(
        new Icon(SystemIcons.Asterisk, 32, 32),
        "TextBox #2",
        RibbonButtonSize.Large,
        "Persistent text box.",
        (x_, y_) => m_viewManager.CreateView("ViewCreatorDemoModule-TextBox2"));
      m_ribbon.AddRibbonItem("Views", "Persistence Demo", rbChgStatus2);

      var rbChgStatus2a = new RibbonButton(
        new Icon(SystemIcons.Information, 32, 32),
        "TextBox #2 a",
        RibbonButtonSize.Large,
        "Persistent text box.",
        (x_, y_) => m_viewManager.CreateView("ViewCreatorDemoModule-TextBox2a"));
      m_ribbon.AddRibbonItem("Views", "Persistence Demo", rbChgStatus2a);

      var rbChgStatus3 = new RibbonButton(
        new Icon(SystemIcons.Asterisk, 32, 32),
        "TextBox #3",
        RibbonButtonSize.Large,
        "Persistent text box.",
        (x_, y_) => m_viewManager.CreateView("ViewCreatorDemoModule-TextBox3"));
      m_ribbon.AddRibbonItem("Views", "Persistence Demo", rbChgStatus3);

      _updateButton = new RibbonButton("Update Document", (s, a) =>
                                                            { _updateButton.IsEnabled = !_updateButton.IsEnabled; });
      m_ribbon.AddRibbonItem("Main", "Actions", _updateButton);

    }

    private RibbonButton _updateButton;

    #endregion

    #region INotifyPropertyChanged Members

    public event PropertyChangedEventHandler PropertyChanged;

    #endregion
  }


  public class TextBoxState
  {
    public TextBoxState()
    {
    }

    public TextBoxState(string text_)
    {
      Text = text_;
    }

    public string Text { get; set; }
  }

  public class Foo
  {
    public string PersistentText { get; set; }
  }
}