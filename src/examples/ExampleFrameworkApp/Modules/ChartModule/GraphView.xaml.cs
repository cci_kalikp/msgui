﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/ChartModule/GraphView.xaml.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.ObjectModel;
using System.Windows.Controls;

namespace MorganStanley.MSDotNet.MSGui.Modules.ChartModule
{
  /// <summary>
  /// Interaction logic for GraphView.xaml
  /// </summary>
  public partial class GraphView : UserControl
  {
    public GraphView()
    {
      InitializeComponent();
    }
  }

  //TEMP
  //the following will be moved to different files
  public class FuncValue
  {
    private DateTime time;
    private double value;

    public FuncValue(DateTime time, double value)
    {
      this.time = time;
      this.value = value;
    }

    public DateTime Time
    {
      get { return time; }
      set { time = value; }
    }

    public double Value
    {
      get { return this.value; }
      set { this.value = value; }
    }
  }

  public class FuncCollection : ObservableCollection<FuncValue>
  {
    public FuncCollection()
    {
      for (int i = 1; i < 50; i++)
      {
        Add(new FuncValue(DateTime.Now + TimeSpan.FromSeconds(i * 10.0), i * i));
      }
    }
  }
}
