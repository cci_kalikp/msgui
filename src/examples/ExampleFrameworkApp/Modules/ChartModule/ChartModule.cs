﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/ChartModule/ChartModule.cs#11 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Drawing;
using System.Windows.Controls;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;

namespace MorganStanley.MSDotNet.MSGui.Modules.ChartModule
{
  public class ChartModule : IModule
  {
    public const string CHART_VIEWID = "ChartModule-ChartView";
    public const string GRAPH_VIEWID = "ChartModule-GraphView";
    private IViewManager m_viewManager;
    private IApplication m_application;
    private IControlParadigmRegistrator m_ribbon;

    public ChartModule(IViewManager viewManager_, IApplication application_, IControlParadigmRegistrator ribbon_)
    {
      m_viewManager = viewManager_;
      m_application = application_;
      m_ribbon = ribbon_;
    }
    public void Initialize()
    {
      m_application.ApplicationLoaded += OnApplicationLoaded;

      //a chart with persistent data
      m_viewManager.AddViewCreator(CHART_VIEWID, CreateChart, PersistenceExtensions.SerializeDataContext<ChartViewModel>);
      var chartButton = new RibbonButton(
         new Icon(MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Properties.Resources.Chart, 32, 32),
         "Open Chart Window",
         "Chart",
         OnClickAddView);
      m_ribbon.AddRibbonItem(
          "Views",
          "Chart",
          new WrapperPannelRibbonItem(RibbonItemOrientation.Horizontal, chartButton));

      //a non-persistent graph 
      m_viewManager.AddViewCreator(GRAPH_VIEWID, CreateGraphView);
      var graphButton = new RibbonButton(
         new Icon(MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Properties.Resources.Graph, 32, 32),
         "Open Graph Window",
         "Graph",
         OnClickAddView2);
      m_ribbon.AddRibbonItem(
          "Views",
          "Graph",
          new WrapperPannelRibbonItem(RibbonItemOrientation.Horizontal, graphButton));
    }    

    private void CreateChart(XDocument savedstate_, IViewContainer viewcontainer_)
    {      
      ChartViewModel viewModel = savedstate_ == null ? ChartViewModel.CreateDefaultInitialized() : PersistenceExtensions.Restore<ChartViewModel>(savedstate_);
      
      viewcontainer_.Content = new ChartView {DataContext = viewModel};

      //adding buttons to the header
      var plusButton = new Button()
                         {
                           Content = "+", Width = 20
                         };
      plusButton.Click += (x, y) => viewModel.IncrementData();

      var minusButton = new Button()
                          {
                            Content = "-", Width = 20
                          };
      minusButton.Click += (x, y) => viewModel.DecrementData();

      viewcontainer_.HeaderItems.Add(new Viewbox { Child = plusButton });
      viewcontainer_.HeaderItems.Add(new Viewbox { Child = minusButton });
    }    

    private void CreateGraphView(IViewContainer viewcontainer_)
    {
      var view = new GraphView();
      viewcontainer_.Content = view;
    } 

    private void OnClickAddView(object sender_, EventArgs e_)
    {
      m_viewManager.CreateView(CHART_VIEWID);
    }

    private void OnClickAddView2(object sender_, EventArgs e_)
    {
      m_viewManager.CreateView(GRAPH_VIEWID);
    }

    private void OnApplicationLoaded(object sender_, EventArgs e_)
    {
      //load data
    }    
  }
}
