﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/ChartModule/ChartViewModel.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace MorganStanley.MSDotNet.MSGui.Modules.ChartModule
{
  public class ChartViewModel
  {
    private BindingList<ArrayList> m_data;

    public static ChartViewModel CreateDefaultInitialized()
    {
      ChartViewModel result = new ChartViewModel();
      var copyOfDefault = new List<ArrayList>(ChartData.DefaultData);
      result.m_data = new BindingList<ArrayList>(copyOfDefault);
      return result;
    }
        
    public BindingList<ArrayList> Data
    {
      get
      {
        return m_data;
      }
      set // for serialization
      {
        m_data = value;
      }
    }    

    public void IncrementData()
    {      
      var newList = new ArrayList(m_data[3]);
      for (int i = 0; i < newList.Count; i++)
      {
        newList[i] = ((int) newList[i]) + 10;
      }
      m_data[3] = newList;
    }
    public void DecrementData()
    {
      var newList = new ArrayList(m_data[3]);
      for (int i = 0; i < newList.Count; i++)
      {
        newList[i] = ((int)newList[i]) - 10;
      }
      m_data[3] = newList;
    }    
  }
}
