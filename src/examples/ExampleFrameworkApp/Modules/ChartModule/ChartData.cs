﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/ChartModule/ChartData.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Collections;
using System.ComponentModel;

namespace MorganStanley.MSDotNet.MSGui.Modules.ChartModule
{
  static class ChartData
  {
    private static readonly BindingList<ArrayList> m_data = new BindingList<ArrayList>
                                 {
                                   new ArrayList {45, 42, 48, 49, 59, 69, 63, 62, 79, 73, 72},
                                   new ArrayList {12, 17, 18, 13, 23, 32, 33, 33, 42, 43, 43},
                                   new ArrayList {25, 38, 25, 34, 44, 45, 44, 54, 55, 54, 64},
                                   new ArrayList {38, 33, 37, 31, 38, 47, 51, 48, 57, 61, 58},
                                   new ArrayList
                                     {
                                       "Aug 2",
                                       "Aug 3",
                                       "Aug 4",
                                       "Aug 5",
                                       "Aug 6",
                                       "Aug 9",
                                       "Aug 10",
                                       "Aug 11",
                                       "Aug 12",
                                       "Aug 13",
                                       "Aug 16"
                                     }
                                 };
    public static BindingList<ArrayList> DefaultData
    {
      get
      {
        return m_data;
      }
    }
  }
}
