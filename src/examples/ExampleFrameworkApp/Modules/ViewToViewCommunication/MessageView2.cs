﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/ViewToViewCommunication/MessageView2.cs#6 $
// $Change: 884834 $
// $DateTime: 2014/06/16 03:24:14 $
// $Author: caijin $

using System;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

namespace MorganStanley.MSDotNet.MSGui.Modules.ViewToViewCommunication
{
    /// <summary>
    /// Module publishing and subscribing messages
    /// </summary>
    public class MessageView2 : IModule
    {
        private const string m_viewKey = "V2V-Example View 2";
        private readonly IEventRegistrator m_eventRegistrator;
        private readonly IEventRouter m_eventRouter;
        private readonly IChromeManager m_chromeManager;
        private readonly IChromeRegistry m_chromeRegistry;
        private readonly V2VConfigModule m_v2vConfigModule;

        public MessageView2(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_, IEventRegistrator eventRegistrator_, V2VConfigModule v2vConfigModule)
        {
            m_chromeManager = chromeManager_;
            m_chromeRegistry = chromeRegistry_;
            m_v2vConfigModule = v2vConfigModule;


            
         
          m_eventRegistrator = eventRegistrator_;
        }

        #region IModule Members

        public void Initialize()
        {
            m_chromeRegistry.RegisterWindowFactory(m_viewKey, CreateView);

            m_chromeRegistry.RegisterWidgetFactory("SubscriberFactory"); 
            m_chromeManager.PlaceWidget("SubscriberFactory", RibbonChromeManager.RIBBON_COMMANDAREA_NAME, "View-to-View Messaging/View-to-View Messaging",
                                        new InitialShowWindowButtonParameters()
                                            {
                                                Image = SystemIcons.Shield.ToBitmap().ToImageSource(true),
                                                Text = "Messenger2",
                                                ToolTip = "Message Publishing",
                                                WindowFactoryID = m_viewKey
                                            });
             
        }

        #endregion

        private bool CreateView(IWindowViewContainer viewContainer_, XDocument state_)
        {
          var publisher = m_eventRegistrator.RegisterPublisher<string>(viewContainer_);
          var subscriber = m_eventRegistrator.RegisterSubscriber<Tuple<string>>(viewContainer_);
          var panel = new StackPanel();
          var text = new TextBox { Text = "text1" };
          var button = new Button { Content = "Send" };
          button.Click += (a_, b_) => publisher.Publish(text.Text);
          subscriber.Subscribe(
            str_ => text.Dispatcher.BeginInvoke(
              (Action) (() => text.Text = str_.Item1)));
          panel.Children.Add(text);
          panel.Children.Add(button);
          viewContainer_.Content = panel;
          viewContainer_.Title = m_viewKey;

            m_v2vConfigModule.SignUpAutoHook(viewContainer_, null, null);
            return true;

        }
    }
}
