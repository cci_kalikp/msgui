﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/FlashPlayerBasedFlexModule.cs#12 $
// $Change: 890851 $
// $DateTime: 2014/07/31 06:46:01 $
// $Author: caijin $

using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.FlexModule;

namespace MorganStanley.MSDotNet.MSGui.Modules
{
    /// <summary>
    /// Flash player ActiveX control hosting Flex module
    /// </summary>
    class FlashPlayerBasedFlexModule : FlexModuleFlashPlayerBaseImpl, IModule
    {
        #region Private Fields


        private TextBox m_headerText;
		private const string BrowserViewId = "FlashPlayerBasedFlexView";
		private const string BrowserViewButtonId = "FlashPlayerBasedFlexViewWidget";

		private const string asd = "asd";
		private const string qwe = "qwe";
		#endregion

        #region Constructor

		public FlashPlayerBasedFlexModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_) :
            base(chromeRegistry_, chromeManager_, 
                new Uri(
                    Path.Combine(
                        Path.Combine(
                            Path.GetDirectoryName(
                                Assembly.GetExecutingAssembly().Location), "Lib"), "msguitest35.swf")))
        {
        }

        #endregion

        #region IModule Members

        public override void Initialize()
        {
			m_chromeRegistry.RegisterWindowFactory(BrowserViewId, CreateView);
			m_chromeRegistry.RegisterWidgetFactory(BrowserViewButtonId, m_chromeManager.ShowWindowButtonFactory());

            m_chromeManager.PlaceWidget(BrowserViewButtonId, "Views/Flex", new InitialShowWindowButtonParameters()
                {
                    Image =
                        new Icon(MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Properties.Resources.Chart,
                                 32, 32).ToImageSource(),
                    Text = "Flex Player",
                    ToolTip = "Open Flash Player Based Flex Window",
                    Enabled = true,

                    //window settings
                    WindowFactoryID = BrowserViewId,
                    InitialParameters = new InitialWindowParameters()
                    {
                        
                    }
                });

            m_chromeManager.PlaceWidget(BrowserViewButtonId, ChromeArea.QAT, "", new InitialWidgetParameters());

			
			m_chromeRegistry.RegisterWindowFactory(asd, ASDView);
			m_chromeRegistry.RegisterWidgetFactory(qwe, QWEInit);
			
			m_chromeManager.PlaceWidget(qwe, "Hello/world", new InitialWidgetParameters());

            /*
            m_viewManager.AddViewCreator(BrowserViewId, CreateView);
            var flexButton = new RibbonButton(
               new Icon(Properties.Resources.Chart, 32, 32),    //TODO change icon
               "Open Flash Player Based Flex Window",
               "Flex Player",
               (a_, b_) => m_viewManager.CreateView(BrowserViewId));
            m_ribbon.AddRibbonItem(
                "Views",
                "Flex Player",
                new WrapperPannelRibbonItem(RibbonItemOrientation.Horizontal, flexButton));
			*/
        }

		public bool ASDView(IWindowViewContainer wvc, XDocument state)
		{
			return true;
		}

		public bool QWEInit(IWidgetViewContainer wvc, XDocument state)
		{
			wvc.Content = new Infragistics.Windows.Ribbon.ButtonTool() { Content = "LOL", Id = qwe };

			return true;
		}

        #endregion

        #region View Creation

        protected override bool CreateView(IWindowViewContainer viewcontainer_, XDocument state)
        {
			if (base.CreateView(viewcontainer_, state))
			{
				viewcontainer_.Title = "Flash Player Based Flex View";

				m_headerText = new TextBox {Text = "Text to be sent to SWF"};
				viewcontainer_.HeaderItems.Add(m_headerText);
				var sendButton = new Button {Content = "Send"};
				sendButton.Click +=sendButton_Click;
				viewcontainer_.HeaderItems.Add(sendButton);

                viewcontainer_.Closed += viewcontainer__Closed;
                viewcontainer_.ClosedQuietly += viewcontainer__Closed;

				return true;
			}
			return false;
        }

        void viewcontainer__Closed(object sender, WindowEventArgs e)
        {
            e.ViewContainer.Closed -= viewcontainer__Closed;
            e.ViewContainer.ClosedQuietly -= viewcontainer__Closed;
            ((Button) e.ViewContainer.HeaderItems[1]).Click -= sendButton_Click;
        }

        void sendButton_Click(object sender, RoutedEventArgs e)
        {
            CallToFlex("newMessage", m_headerText.Text);
        }

        #endregion

        #region Communication With Flex

        protected override object CallFromFlex(string functionName_, object[] arguments_)
        {
            if(functionName_.Equals("newMessage")  && arguments_ != null && arguments_.Length > 0)
            {
                NewMessage(arguments_[0] as string);
            }
            return null;
        }

        /// <summary>
        /// Called to notify C# of a new message from the SWF IM client
        /// </summary>
        /// <param name="message_"></param>
        private void NewMessage(string message_)
        {
            m_headerText.Text = message_;
        }

        #endregion

    }
}
