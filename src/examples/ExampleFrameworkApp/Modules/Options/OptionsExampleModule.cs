﻿//cw options_overview : 10
/**##Description*/
/**
 * MSDesktop allows you to easily create option pages using different ways and inject them into the "Options" window
 * */
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp
{
    //cw options_overview : 20
    /**##First Step */
    /**The module which you want to bring in your own option page should have IApplicationOptions injected */
    //{
    public class OptionsExampleModule : IModule
    //}
    {
        private readonly IApplicationOptions options;
        private readonly IPersistenceService service;
        private readonly IUnityContainer container;

        //{
        public OptionsExampleModule(IApplicationOptions options_, IPersistenceService service_, IUnityContainer container_)
        {
            options = options_;
            service = service_;
            container = container_;
        }
        //}

        //cw options_overview : 30
        /**##Second Step */
        /**use IApplicationOptions.AddOptionPage or use IApplicationOptions.AddViewModelBasedOptionPage to add your own option page */
        /**For both methods, the first parameter is the Path of the option page in the navigation tree of Options window */
        /**If setting to null, it means it's added directly under the root node */
        /**If setting to a path delimited by "/", take, "Node1/Node2", the page would be put under Node2 whose parent is Node1(which is one of the first level node)*/
        /**A group node could have its own option page, if the path and title are specified appropriately */
        /**For AddViewModelBasedOptionPage method, needs to specify whether the associated data needs to be persisted globally or layout specific*/
        //{
        public void Initialize()
        //}
        {
            //cw options_overview: 31 cw Option_page_implemented_as_UserControl: 31
            //{
            options.AddOptionPage(null, "General Settings", new ExampleSettingWithView(service));
            //}
            //cw options_overview: 32
            //{
            options.AddOptionPage("General Settings", "Other Settings1", new ExampleSettingWithView2(service));
            //}
            options.AddOptionPage("General Settings", "Other Settings2", new ExampleSettingWithView3(service));

            //cw Option_page_implemented_as_data_object_visualized_by_DataTemplate: 31
            //{
            options.AddViewModelBasedOptionPage<ExampleSettingWithDataTemplate>("Custom", "Example custom settings2", true);
            //}
            //cw Option_page_implemented_as_data_object_visualized_by_DataTemplate: 32
            /**The View Model registered can be get through the container later on as this */
            //{
            var settings1 = container.Resolve<ExampleSettingWithDataTemplate>();
            //}
            //cw options_overview: 33 cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 31
            //{
            options.AddViewModelBasedOptionPage<ExampleSettingWithoutDataTemplate>("Custom",
                                                                                   "Example custom settings1", true);
            //}
            //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 32
            /**The View Model registered can be get through the container later on as this */
            //{
            var settings2 = container.Resolve<ExampleSettingWithDataTemplate>();
            //}


       //cw options_overview : 34
       //{
        }
        //}
        
    }
}
