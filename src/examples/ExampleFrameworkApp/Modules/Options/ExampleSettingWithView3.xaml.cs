﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.MSGui.Impl.Options;
namespace MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp
{
    /// <summary>
    /// Interaction logic for ExampleSettingWithView3.xaml
    /// </summary>
    public partial class ExampleSettingWithView3 : UserControl, IOptionViewWithModel
    {
        public ExampleSettingWithView3(IPersistenceService service_)
        {
            InitializeComponent();
            service_.AddGlobalPersistor("OptionsExample.Settings3", LoadState, SaveState);
            this.DataContext = Settings;
        }

        private Settings3 oldSettings;
        private Settings3 settings;
        public Settings3 Settings
        {
            get
            {
                if (settings == null)
                {
                    settings = new Settings3();
                    settings.Value1 = "Test1";
                    settings.Value2 = false;
                    oldSettings = settings.Clone();
                }
                return settings;
            }
            set { settings = value; }
        }

         

        public XDocument SaveState()
        {
            return PersistenceExtensions.Store(settings);
        }

        public void LoadState(XDocument state_)
        {
            var loaded = PersistenceExtensions.Restore<Settings3>(state_);
            this.DataContext = Settings = loaded;
            oldSettings = settings.Clone();
        }
         

        public void OnOK()
        {
            PersistenceExtensions.Store(settings);
        }

        public void OnCancel()
        {
            if (oldSettings != null)
            {
                this.DataContext = Settings = oldSettings.Clone();
            }
            else
            {
                settings = null;
                this.DataContext = Settings;
            }
        }


        public void OnDisplay()
        {
             
        }

        public void OnHelp()
        { 
        }

        public new UIElement Content
        {
            get { return this; }
        }

        public object ViewModel
        {
            get { return Settings; }
        }


        public bool CanNavigateSubItem
        {
            get { return true; }
        }
         
    }

    [Serializable]
    public class Settings3:ViewModelBase, ICloneable
    {
        private string value1;
        [DisplayName("Value 1")]
        public string Value1
        {
            get { return value1; }
            set { value1 = value; OnPropertyChanged("Value1"); }
        }

        private bool value2;
        [DisplayName("Value 2")]
        public bool Value2
        {
            get { return value2; }
            set { value2 = value; OnPropertyChanged("Value2"); }
        }

        public Settings3 Clone()
        {
            Settings3 s = new Settings3();
            s.value1 = this.value1;
            s.value2 = this.value2;
            return s;
        }
        object ICloneable.Clone()
        {
            return this.Clone();
        }
    }
}
