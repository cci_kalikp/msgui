﻿//cw Option_page_implemented_as_UserControl: 10
/**##Description*/
/**Custom option page can be created using a UserControl and registered through IApplicationOptions.AddOptionPage method
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
//cw Option_page_implemented_as_UserControl: 20
/**##Namespace*/
//{
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Options;
//}
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp
{
    /// <summary>
    /// Interaction logic for ExampleSettingWithView.xaml
    /// </summary>
    //cw Option_page_implemented_as_UserControl: 30
    /**##Code*/
    //cw Option_page_implemented_as_UserControl: 32
    /**IOptionView or IValidatableOptionView(if validation is needed) must be implemented */
    /**ISearchablePage or ISubItemAwareSeachablePage can be also implemented if developer want to support the search function in his own logic */
    //{
    public partial class ExampleSettingWithView : UserControl, IValidatableOptionView, ISearchablePage, ISubItemAwareSeachablePage
    //}
    {
        //cw Option_page_implemented_as_UserControl: 33
        /**Usually IPersistenceService is needed for persisting the data into the global/profile specific storage*/
        //{
        public ExampleSettingWithView(IPersistenceService service_)
        {
            InitializeComponent();
            service_.AddGlobalPersistor("OptionsExample.Settings", LoadState, SaveState);
            //}

            //cw Option_page_implemented_as_UserControl: 36
            //{
            this.DataContext = Settings; 
            //}
        //cw Option_page_implemented_as_UserControl: 34
        //{
        }
        //}

        private Settings oldSettings;
        //cw Option_page_implemented_as_UserControl: 35
        /**Set the DataContext to the user control to allow end user to view and edit it utilizing data binding */
        //{
        private Settings settings;
        public Settings Settings
        {
            get
            {
                if (settings == null)
                {
                    settings = new Settings();
                    settings.Add(new Pair("Setting1", "Value1"));
                    oldSettings = settings.Clone();
                }
                return settings;
            }
            set { settings = value; }
        }
        //}

        public XDocument SaveState()
        {
            return PersistenceExtensions.Store(Settings);
        }

        public void LoadState(XDocument state_)
        {
            var loaded = PersistenceExtensions.Restore<Settings>(state_);
            this.DataContext = Settings = loaded;
            oldSettings = settings.Clone();
        }

        //cw Option_page_implemented_as_UserControl: 50
        /**##Validation*/
        /**If IValidatableOptionView instead of IOptionView is implemented, it gives a chance for the option page to validate user input before it's persisted.*/
        /**OnValidate should return false if validation failed, and pop up appropriate error message to hint user about the reason */
        //{
        public bool OnValidate()
        {
            if (Settings.Count == 0)
            {
                TaskDialog.ShowMessage("Must specify some settings",
                       "Invalid settings", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
                return false;
            }
            else if (Settings.Count > 5)
            {
                TaskDialog.ShowMessage("Too many some settings",
       "Invalid settings", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
                return false;
            }
            return true;
        }
        //}


        /**OnInvalid should also be implemented to highlight the error control, we could use extension method "Highlight" defined in OptionsExtensions to give an effect of flashing border to be consistent*/
        //{
        public void OnInvalid()
        {
            this.grid.Highlight();
        }
        //}

        //cw Option_page_implemented_as_UserControl: 39
        /** OnOK can be handled to persist the changes done*/
        //{
        public void OnOK()
        {
            PersistenceExtensions.Store(settings);
        }
        //}

        //cw Option_page_implemented_as_UserControl: 40
        //**OnCancel can be used to rollback to the original setting*/
        //{
        public void OnCancel()
        {
            if (oldSettings != null)
            {
                this.DataContext = Settings = oldSettings.Clone();         
            }
            else
            {
                settings = null;
                this.DataContext = Settings;
            }
        }
        //}

        //cw Option_page_implemented_as_UserControl: 38
        /** OnDisplay can be handled to add some initializing logic before the option page is displayed*/
        //{
        public void OnDisplay()
        { 
        }
        //}


        //cw Option_page_implemented_as_UserControl: 41
        /** OnHelp can be handled to the helping request from end user*/
        //{
        public void OnHelp()
        { 
        }
         //}

        //cw Option_page_implemented_as_UserControl: 37
        /**return the UserControl itself as the Content property exposed*/
        //{
        public new UIElement Content
        {
            get { return this; }
        }
        //}

        //cw Option_page_implemented_as_UserControl: 60
        /**##Search Support*/
        /**Search can be supported in this case, by implement the ISearchablePage interface, which has only one method "GetMatchCount"*/
        //{
        public int GetMatchCount(string textToSearch_)
        {
            return Settings.Count(setting_ => (setting_.Key ?? string.Empty).ToLower().Contains(textToSearch_.ToLower()) || 
                (setting_.Value ?? string.Empty).ToLower().Contains(textToSearch_.ToLower()));
        }
        //}

        //cw Option_page_implemented_as_UserControl: 65 
        /**Search can be also supported in this case in finer grain, by implement the ISubItemAwareSeachablePage interface, which has two methods "GetMatchedItems" and "LocateItem", they would provide smarter search navigation when EnableTypeaheadDropdownOptionsSearch is set*/
        //{
        public IList<KeyValuePair<string, object>> GetMatchedItems(string textToSearch_)
        {
            var matchedItems = new List<KeyValuePair<string, object>>();
            foreach (var pair2 in Settings)
            {
                if ((pair2.Key ?? string.Empty).ToLower().Contains(textToSearch_.ToLower()) ||
                (pair2.Value ?? string.Empty).ToLower().Contains(textToSearch_.ToLower()))
                {
                    matchedItems.Add(new KeyValuePair<string, object>(pair2.Key, pair2.Key));
                }
            }
            return matchedItems;

        }

        public void LocateItem(object item_)
        {
            string key = item_ as string;
            if (key != null)
            {
                foreach (var pair2 in Settings)
                {
                    if (pair2.Key == key)
                    {
                        grid.ActiveDataItem = pair2;
                        break;
                    }
                }
            }
        }
        //}


    }

    [Serializable]
    public class Settings : ObservableCollection<Pair>, ICloneable
    {

        public Settings Clone()
        {
            Settings s = new Settings();
            foreach (var setting in this)
            {
                s.Add(new Pair(setting.Key, setting.Value));
            }
            return s;
        }



        object ICloneable.Clone()
        {
            return Clone();
        }
    }

    [Serializable]
    public class Pair : ViewModelBase
    {
        public Pair (){}
        public Pair(string key_, string value_)
        {
            key = key_;
            _value = value_;
        }

        private string key;
        public string Key
        {
            get { return key; }
            set { key = value; OnPropertyChanged("Key"); }
        }

        private string _value;
        public string Value
        {
            get { return _value; }
            set { _value = value; OnPropertyChanged("Value"); }
        }
    }
}
