﻿//cw Option_page_implemented_as_UserControl_with_ViewModel_specified: 10
/**##Description*/
/**If using UserControl as Option page, user could also specify a ViewModel so that the Search can be supported more automatically*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using System.Collections.ObjectModel;
//cw Option_page_implemented_as_UserControl_with_ViewModel_specified: 20
/**##Namespace*/
//{
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Options;
//}

namespace MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp
{
    /// <summary>
    /// Interaction logic for ExampleSettingWithView2.xaml
    /// </summary>
    //cw Option_page_implemented_as_UserControl_with_ViewModel_specified: 30
    /**##Code*/
    /** If is implemented IOptionViewWithModel, the ViewModel returned through this interface can be analyzed by Options Window during searching as if done when an IOptionViewViewModel is registered as option page*/
    //{
    public partial class ExampleSettingWithView2 : UserControl, IValidatableOptionView, IOptionViewWithModel
    //}
    {
        public ExampleSettingWithView2(IPersistenceService service_)
        {
            InitializeComponent();
            service_.AddGlobalPersistor("OptionsExample.Settings2", LoadState, SaveState);
            //cw Option_page_implemented_as_UserControl_with_ViewModel_specified: 32
            //{
            this.DataContext = Settings; 
            //}
        }
          
        private Settings2 oldSettings;
        private Settings2 settings;
        public Settings2 Settings
        {
            get
            {
                if (settings == null)
                {
                    settings = new Settings2();
                    settings.Add(new Pair2("SettingA", "Value1"));
                    settings.Add(new Pair2("SettingB", "Value2"));
                    settings.Add(new Pair2("SettingC", "Value3"));
                    oldSettings = settings.Clone();
                }
                return settings;
            }
            set { settings = value; }
        }

        public XDocument SaveState()
        {
            return PersistenceExtensions.Store(Settings);
        }

        public void LoadState(XDocument state_)
        {
            var loaded = PersistenceExtensions.Restore<Settings2>(state_);
            this.DataContext = Settings = loaded;
            oldSettings = settings.Clone();
        }

        public bool OnValidate()
        {
            if (Settings.Count == 0)
            {
                TaskDialog.ShowMessage("Must specify some settings",
                       "Invalid settings", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
                return false;
            }
            else if (Settings.Count > 5)
            {
                TaskDialog.ShowMessage("Too many some settings",
       "Invalid settings", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
                return false;
            }
            return true;
        }
        public void OnOK()
        {
            PersistenceExtensions.Store(settings);
        }

        public void OnInvalid()
        {
            this.grid.Highlight();
        }
        public void OnCancel()
        {
            if (oldSettings != null)
            {
                this.DataContext = Settings = oldSettings.Clone();         
            }
            else
            {
                settings = null;
                this.DataContext = Settings;
            }
        }

        public void OnDisplay()
        {
             
        }

        public void OnHelp()
        {
             
        }

        public new UIElement Content
        {
            get { return this; }
        }

        //cw Option_page_implemented_as_UserControl_with_ViewModel_specified: 31
        /**return DataContext as ViewModel property exposed*/
        //cw Option_page_implemented_as_UserControl_with_ViewModel_specified: 33
        //{
        public object ViewModel
        {
            get { return Settings; }
        }
        //}

    }

    [Serializable]
    public class Settings2 : ObservableCollection<Pair2>, ICloneable, ISubItemAwareSeachablePage, INotifyPropertyChanged
    {

        public Settings2 Clone()
        {
            Settings2 s = new Settings2();
            foreach (var setting in this)
            {
                s.Add(new Pair2(setting.Key, setting.Value));
            }
            return s;
        }

        private Pair2 currentPair;
        public Pair2 CurrentPair
        {
            get { return currentPair; }
            set { currentPair = value; OnPropertyChanged("CurrentPair"); }
        }
        object ICloneable.Clone()
        {
            return Clone();
        } 

        public IList<KeyValuePair<string, object>> GetMatchedItems(string textToSearch_)
        {
            var  matchedItems = new List<KeyValuePair<string, object>>();
            foreach (var pair2 in Items)
            {
                if((pair2.Key ?? string.Empty).ToLower().Contains(textToSearch_.ToLower()) ||
                (pair2.Value ?? string.Empty).ToLower().Contains(textToSearch_.ToLower()))
                {
                    matchedItems.Add(new KeyValuePair<string, object>(pair2.Key, pair2.Key));
                }
            }
            return matchedItems;
        }

        public void LocateItem(object item_)
        {
            string key = item_ as string;
            if (key != null)
            {
                foreach (var pair2 in Items)
                {
                    if (pair2.Key == key)
                    {
                        CurrentPair = pair2;
                        break; 
                    }
                }
            }
        }

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName_">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName_)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName_);
                handler(this, e);
            }
        }
    }

    [Serializable]
    public class Pair2 : ViewModelBase
    {
        public Pair2 (){}
        public Pair2(string key_, string value_)
        {
            key = key_;
            _value = value_;
        }

        private string key;
        public string Key
        {
            get { return key; }
            set { key = value; OnPropertyChanged("Key"); }
        }

        private string _value;
        public string Value
        {
            get { return _value; }
            set { _value = value; OnPropertyChanged("Value"); }
        }
    }
}
