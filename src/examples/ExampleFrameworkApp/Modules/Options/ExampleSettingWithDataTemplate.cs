﻿//cw Option_page_implemented_as_data_object_visualized_by_DataTemplate: 10
/**##Description*/
/**Custom option page can be created as a data object and visualized using a DataTemplate*/
using System;
using System.ComponentModel;
using System.Xml.Linq;
//cw Option_page_implemented_as_data_object_visualized_by_DataTemplate: 20
/**##Namespace */
//{
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.MSGui.Impl.Options;
//}

namespace MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp
{
    //cw Option_page_implemented_as_data_object_visualized_by_DataTemplate: 30
    /**##Code*/
    /**The data object should be marked as Serializable to be able to be persisted by the persister*/
    /**It should also implement IOptionViewViewModel, and INotifyPropertyChanged(which has a basic implementation in MorganStanley.MSDotNet.MSGui.Core.ViewModel.ViewModelBase)*/
    //{
    [Serializable]
    public class ExampleSettingWithDataTemplate : ViewModelBase, IOptionViewViewModel
    //}
    {

        private bool booleanSetting;

        //cw Option_page_implemented_as_data_object_visualized_by_DataTemplate: 40
        /**For each public property, "DisplayNameAttribute" could be specified, whose value would be utilized by Options window to search upon; if not specified, the name of the property would be used instead */
        /**"SearchTagsAttribute" could be specified for the Options window to search upon as well */
        //{
        [DisplayName("Boolean Setting")]
        [SearchTags("Advanced,Test")]
        public bool BooleanSetting
        {
            get { return booleanSetting; }
            set
            {
                booleanSetting = value;
                base.OnPropertyChanged("BooleanSetting");
            }
        }
        //}

        private string stringSetting;
        [DisplayName("String Setting")]
        [SearchTags("Advanced,Test")]
        public string StringSetting
        {
            get { return stringSetting; }
            set
            {
                stringSetting = value;
                base.OnPropertyChanged("StringSetting");
            }
        }


        //cw Option_page_implemented_as_data_object_visualized_by_DataTemplate: 41
        /**This method will be called when the options screen for this object is displayed. The end user will modify the copy and later the modified copy will be passed to the original options object. */
        //{
        public object Clone()
        {
            var copy = new ExampleSettingWithDataTemplate();
            copy.AcceptChanges(this);
            return copy;
        }
        //}

        //cw Option_page_implemented_as_data_object_visualized_by_DataTemplate: 42
        /**This method will be called on the original object when the user confirms the changes they made.The parameter contains the modified copy of the original object. The method should propagate the changes to the original object. */
        //{
        public void AcceptChanges(IOptionViewViewModel newViewModel_)
        {
            var vm = newViewModel_ as ExampleSettingWithDataTemplate;
            BooleanSetting = vm.BooleanSetting;
            StringSetting = vm.StringSetting;
        }
        //}

        //cw Option_page_implemented_as_data_object_visualized_by_DataTemplate: 43
        /**This method is called before changes are applied. It's purpose is to validate the data and return true if the data is correct. */
        /**If data is invalid, errorString_ should be specified for popping up the message box, and either invalidControlName_(which is specified in the DataTemplate) or invalidPropertyName_ should be returned for highlighting the invalid field later on */
        //{
        public bool Validate(out string errorString_, out string invalidControlName_, out string invalidPropertyName_)
        {
            errorString_ = invalidControlName_ = invalidPropertyName_ = null;
            if (!BooleanSetting)
            {
                errorString_ = StringSetting;
                invalidPropertyName_ = "StringSetting";
            }
            return BooleanSetting;
        }
        //}


        //cw Option_page_implemented_as_data_object_visualized_by_DataTemplate: 44
        /**These methods ensure that the settings object's state is persisted between application instantiations. PersistenceExtensions facilitate serialization to/from XDocument. */
        //{
        public XDocument Persist()
        {
            return PersistenceExtensions.Store(this);
        }

        public void LoadState(XDocument state_)
        {
            var loaded = PersistenceExtensions.Restore<ExampleSettingWithDataTemplate>(state_);
            AcceptChanges(loaded);
        }
        //}


        //cw Option_page_implemented_as_data_object_visualized_by_DataTemplate: 45
        /** "HasCustomTemplate" property must return true to indicate that there is a DataTemplate available in Application Resources for visualizing this data object */
        //{
        public bool HasCustomTemplate
        {
            get { return true; }
        }
        //}
    }
}
