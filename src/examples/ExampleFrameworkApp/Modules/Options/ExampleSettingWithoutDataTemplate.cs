﻿//cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 10
/**##Description*/
/**Custom option page can be created as a data object without DataTemplate, in this case, it would be visualized by the PropertyGrid control*/
using System;
using System.ComponentModel;
using System.Xml.Linq;
using System.Windows;

//cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid:20
/**##Assembly*/
/**
 * You need to add a reference to the Xceed.Wpf.Toolkit assembly. (files:////ms/dist/dotnet3rd/PROJ/extendedWPFToolkit/2.0.0.0/assemblies/Xceed.Wpf.Toolkit.dll)
 */
/**##Namespace*/
//{
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.MSGui.Impl.Options;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
//}

namespace MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp
{
    //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 30
    /**##Code*/
    /**The data object should be marked as Serializable to be able to be persisted by the persister*/
    /**It should also implement IOptionViewViewModel, and INotifyPropertyChanged(which has a basic implementation in MorganStanley.MSDotNet.MSGui.Core.ViewModel.ViewModelBase)*/
    //{
    [Serializable]
    public class ExampleSettingWithoutDataTemplate : ViewModelBase, IOptionViewViewModel
     //}
    {

        private int numericSetting;


        [DisplayName("Numeric Setting")]
        [Category("General 1")]
        [PropertyOrder(0)]
        public int NumericSetting
        {
            get { return numericSetting; }
            set
            {
                numericSetting = value;
                base.OnPropertyChanged("NumericSetting");
            }
        }

        //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 32
        /**"DisplayNameAttribute"(if missing the name of the property would be used) and "CategoryAttribute"(if missing "Misc" would become the category) should be specified to be displayed appropriately in the PropertyGrid*/
        /**"PropertyOrderAttribute" could also be specified to make the order more explicit, if not specified, properties would be ordered by Alphanumeric order */
        //{
        private bool booleanSetting;
        [DisplayName("Boolean Setting")]
        [Category("General 1")]
        [PropertyOrder(1)]
        public bool BooleanSetting
        {
            get { return booleanSetting; }
            set
            {
                if (booleanSetting != value)
                {
                    booleanSetting = value; 
                    base.OnPropertyChanged("BooleanSetting");
                }
                
            }
        }
        //}

        private HorizontalAlignment enumSetting;
        [DisplayName("Enum Setting")]
        [Category("General 2")]
        [PropertyOrder(3)]
        //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 33
        /**Visibility of the property can be decided by other property, using "BindableVisibilityAttribute", the first parameter is the name of the property to be dependent upon, the second property, if specified indicate if the value of the property specified by the first parameter should be used reversely */
        //{
        [BindableVisibility("BooleanSetting", true)]
        public HorizontalAlignment EnumSetting
        {
            get { return enumSetting; }
            set
            {
                enumSetting = value;
                base.OnPropertyChanged("EnumSetting");
            }
        }
        //}

        private string currencySetting;
        [DisplayName("Currency Setting")]
        //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 40
        /** "ItemsSourceAttribute" can be used to specify a naming list for the drop down editor */
        //{
        [ItemsSource(typeof(BaseCurrencyTypes))]
        //}
        [Category("General 2")]
        [PropertyOrder(4)]
        //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 34
        //{
        [BindableVisibility("BooleanSetting")]
        public string CurrencySetting
        {
            get { return currencySetting; }
            set { currencySetting = value; base.OnPropertyChanged("CurrencySetting"); }
        }
        //}

        
        private SubSetting part = new SubSetting();
        [DisplayName("Sub Setting")] 
        [Category("General 2")] 
        [PropertyOrder(5)]
        //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 37
        /** A DataTemplate can be associated with the property using "CustomTemplateAttribute", the parameter is the name of the DataTemplate in the Application Resources. Note, DataContext of the Root element of the DataTemplate is set to the value of this property */
        /** If without "ShowInPopup" specified, or its value is set to false, this property would be rendered using the DataTemplate specified as an embedded control in the PropertyGrid*/
        //{
        [CustomTemplate("SubSettingTemplate")]
        public SubSetting Part
        {
            get { return part; }
            set { part = value; base.OnPropertyChanged("Part"); }
        }
        //}

        private SubSetting2 part2 = new SubSetting2();
        [DisplayName("Sub Setting2")]
        [Category("General 2")]
        [PropertyOrder(6)]
        //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 39
        /**Property with complex type could also be marked using "ExpandableObjectAttribute", to ask "PropertyGrid" to expand on sub properties of this property */
        //{
        [ExpandableObject]
        public SubSetting2 Part2
        {
            get { return part2; }
            set { part2 = value; base.OnPropertyChanged("Part2"); }
        }
        //}

        private SubSetting part3 = new SubSetting();
        [DisplayName("Sub Setting3")]
        [Category("General 2")]
        [PropertyOrder(7)]
        //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 38
        /** If "ShowInPopup" is set to true, this property would be rendered as a hyper link button in the PropertyGrid, and clicking it would invoke the dialog rendering using the DataTemplate specified*/
        //{
        [CustomTemplate("SubSettingTemplate", true)]
        public SubSetting Part3
        {
            get { return part3; }
            set { part3 = value; base.OnPropertyChanged("Part3"); }
        }
        //}

        //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 43
        /**This method will be called when the options screen for this object is displayed. The end user will modify the copy and later the modified copy will be passed to the original options object. */
        //{
        public object Clone()
        {
            var copy = new ExampleSettingWithoutDataTemplate();
            copy.AcceptChanges(this);
            return copy;
        }
        //}

        //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 44
        /**This method will be called on the original object when the user confirms the changes they made.The parameter contains the modified copy of the original object. The method should propagate the changes to the original object. */
        //{
        public void AcceptChanges(IOptionViewViewModel newViewModel_)
        {
            var vm = newViewModel_ as ExampleSettingWithoutDataTemplate;
            NumericSetting = vm.NumericSetting;
            EnumSetting = vm.EnumSetting;
            BooleanSetting = vm.booleanSetting;
            CurrencySetting = vm.currencySetting;
            if (vm.part != null)
            {
                Part = vm.part.Clone();
            }
            if (vm.part2 != null)
            {
                Part2 = vm.part2.Clone();           
            }
            if (vm.part3 != null)
            {
                Part3 = vm.part3.Clone();
            }
        }
        //}


        //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 42
        /**This method is called before changes are applied. It's purpose is to validate the data and return true if the data is correct. */
        /**If data is invalid, errorString_ should be specified for popping up the message box, invalidPropertyName_(embedded property can be specified using "PropertyName.SubPropertyName") should be returned for highlighting the invalid field later on */
        //{
        public bool Validate(out string errorString_, out string invalidControlName_, out string invalidPropertyName_)
        {
            errorString_ = invalidControlName_ = invalidPropertyName_ = null;
            if (string.IsNullOrEmpty(CurrencySetting))
            {
                invalidPropertyName_ = "CurrencySetting";
                errorString_ = "Currency Setting must be specified";
                return false; 
            }
            if (string.IsNullOrEmpty(part.Part1))
            {
                invalidPropertyName_ = "Part.Part1";
                errorString_ = "SubSetting 1 must have all parts filled in";
                return false;
            }
            if (string.IsNullOrEmpty(part2.PartY))
            {
                invalidPropertyName_ = "Part2.PartY";
                errorString_ = "SubSetting 2 must have PartY filled in";
                return false;
            }
            if (string.IsNullOrEmpty(part3.Part1))
            {
                invalidPropertyName_ = "Part3.Part1";
                errorString_ = "SubSetting 3 must have Part1 filled in";
                return false;
            }
            return true;
        }
        //}

        //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 35
        /**And usually if dynamic visibility is introduced, we need to blank out the value of the invisible property as following*/
        //{
        protected override void OnPropertyChanged(string propertyName_)
        {
            if (propertyName_ == "BooleanSetting")
            {
                if (!booleanSetting)
                {
                    CurrencySetting = null;
                }
                else
                {
                    EnumSetting = HorizontalAlignment.Left; 
                }
            }
            
            base.OnPropertyChanged(propertyName_);
        }
        //}

        //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 45
        /**These methods ensure that the settings object's state is persisted between application instantiations. PersistenceExtensions facilitate serialization to/from XDocument. */
        //{
        public XDocument Persist()
        {
            return PersistenceExtensions.Store(this);
        }

        public void LoadState(XDocument state_)
        {
            var loaded = PersistenceExtensions.Restore<ExampleSettingWithoutDataTemplate>(state_);
            AcceptChanges(loaded);
        }
        //}


        //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 46
        /** "HasCustomTemplate" property must return false to indicate that there is no DataTemplate associated with this data object, so the PropertyGrid would be used to give out the default layout according to the attributes specified on each property */
        //{
        public bool HasCustomTemplate
        {
            get { return false; }
        }
        //}

        //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 41
        //{
        public class BaseCurrencyTypes : IItemsSource
        {
            public ItemCollection GetValues()
            {
                ItemCollection items = new ItemCollection(); 
                items.Add("USD");
                items.Add("GBP");
                items.Add("CAD");
                items.Add("EUR");
                items.Add("JPY");
                items.Add("RMB");
                return items;
            } 
        } 
        //}
    }

    [Serializable]
    public class SubSetting : ViewModelBase, ICloneable
    {
        private string part1;
        public String Part1
        {
            get { return part1; }
            set { part1 = value; OnPropertyChanged("Part1"); }
        }


        private string part2;
        public String Part2
        {
            get { return part2; }
            set { part2 = value; OnPropertyChanged("Part2"); }
        }


        private string part3;
        public String Part3
        {
            get { return part3; }
            set { part3 = value; OnPropertyChanged("Part3"); }
        }

        public string DisplayText
        {
            get { return "Edit Sub Setting"; }
        }
        public SubSetting Clone()
        {
            var copy = new SubSetting();
            copy.part1 = this.part1;
            copy.part2 = this.part2;
            copy.part3 = this.part3;
            return copy;
        }
        object ICloneable.Clone()
        {
            return this.Clone();
        } 
    }

    [Serializable]
    public class SubSetting2 : ViewModelBase, ICloneable
    {
        private bool partX;
        public bool PartX
        {
            get { return partX; }
            set
            {
                if (partX != value)
                {
                    partX = value; 
                    OnPropertyChanged("PartX");
                }
            }
        }


        //cw Option_page_implemented_as_data_object_visualized_by_PropertyGrid: 36
        /**Enablement of the property can be decided by other property, using "BindableEnablementAttribute", the first parameter is the name of the property to be dependent upon, the second property, if specified indicate if the value of the property specified by the first parameter should be used reversely */
        //{
        private string partY;
        [BindableEnablement("PartX")]
        public String PartY
        {
            get { return partY; }
            set { partY = value; OnPropertyChanged("PartY"); }
        }


        private string partZ;
        [BindableEnablement("PartX", true)] 
        public String PartZ
        {
            get { return partZ; }
            set { partZ = value; OnPropertyChanged("PartZ"); }
        }
        //}

        public SubSetting2 Clone()
        {
            var copy = new SubSetting2();
            copy.partX = this.partX;
            copy.partY = this.partY;
            copy.partZ = this.partZ;
            return copy;
        }
        object ICloneable.Clone()
        {
            return this.Clone();
        }

        public override string ToString()
        {
            return "Settings";
        }
    }
}
