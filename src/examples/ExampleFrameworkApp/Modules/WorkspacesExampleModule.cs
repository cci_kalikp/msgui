﻿using System.Drawing;
using System.Windows.Controls;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MSDesktop.Workspaces;
using MSDesktop.Workspaces.Interfaces;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using Image = System.Windows.Controls.Image;

namespace MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Modules
{
    public class WorkspacesExampleModule : IModule
    {
        private const string ImageWindowOne = "ImageWindowOne";
        private const string ImageWindowTwo = "ImageWindowTwo";
        private const string ImageWindowThree = "ImageWindowThree";
        private const string TextWindowOne = "TextWindowOne";
        private const string TextWindowTwo = "TextWindowTwo";
        private const string TextWindowThree = "TextWindowThree";
        private const string ImageWindowOne2 = "ImageWindowOne2";
        private const string ImageWindowTwo2 = "ImageWindowTwo2";
        private const string ImageWindowThree2 = "ImageWindowThree2";
        private const string TextWindowOne2 = "TextWindowOne2";
        private const string TextWindowTwo2 = "TextWindowTwo2";
        private const string TextWindowThree2 = "TextWindowThree2";


        internal const string DockImageWorkspaceCategory = "Image Dock";
        internal const string DockTextWorkspaceCategory = "Text Dock";

        internal const string TileImageWorkspaceCategory = "Image Tile";
        internal const string TileTextWorkspaceCategory = "Text Tile";


        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;

        public WorkspacesExampleModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_)
        {
            this.chromeRegistry = chromeRegistry_;
            this.chromeManager = chromeManager_;
        }

        public void Initialize()
        {
            this.RegisterWindowFactories();
            this.RegisterWorkspaces();
        }

        private void RegisterWindowFactories()
        {
            var groupContainer = chromeManager.Ribbon["Workspaces"]["x"];
            this.chromeManager.AddWorkspaceDropdowns(groupContainer);

            chromeManager.AddLoadWorkspacesButton("Load Workspace", null, groupContainer);

            chromeManager.AddWidget("CreateEmpty" + DockImageWorkspaceCategory, new InitialButtonParameters()
            {
                Text = "Create " + DockImageWorkspaceCategory + " Workspace",
                Click = (sender_, args_) => chromeManager.CreateEmptyWorkspace(new InitialWindowParameters()
                {
                    WorkspaceCategory = DockImageWorkspaceCategory, 
                    SizingMethod = SizingMethod.Custom,
                    Width = 1000,
                    Height = 800
                })
            }, groupContainer);

            chromeManager.AddWidget("CreateEmpty" + DockTextWorkspaceCategory, new InitialButtonParameters()
            {
                Text = "Create " + DockTextWorkspaceCategory + " Workspace",
                Click = (sender_, args_) => chromeManager.CreateEmptyWorkspace(new InitialWindowParameters()
                {
                    WorkspaceCategory = DockTextWorkspaceCategory, 
                    SizingMethod = SizingMethod.Custom,
                    Width = 1000,
                    Height = 800
                })
            }, groupContainer);

            chromeManager.AddWidget("CreateEmpty" + TileImageWorkspaceCategory, new InitialButtonParameters()
            {
                Text = "Create " + TileImageWorkspaceCategory + " Workspace",
                Click = (sender_, args_) => chromeManager.CreateEmptyWorkspace(new InitialTileParameters()
                {
                    WorkspaceCategory = TileImageWorkspaceCategory, 
                    SizingMethod = SizingMethod.Custom,
                    Width = 1000,
                    Height = 800
                })
            }, groupContainer);

            chromeManager.AddWidget("CreateEmpty" + TileTextWorkspaceCategory, new InitialButtonParameters()
            {
                Text = "Create " + TileTextWorkspaceCategory + " Workspace",
                Click = (sender_, args_) => chromeManager.CreateEmptyWorkspace(new InitialTileParameters()
                {
                    WorkspaceCategory = TileTextWorkspaceCategory, 
                    SizingMethod = SizingMethod.Custom,
                    Width = 1000,
                    Height = 800
                })
            }, groupContainer);



            //dock view workspaces
            this.chromeRegistry.RegisterWindowFactory(ImageWindowOne).SetInitHandler((container_, state_) =>
            {
                container_.Content = new Image
                {
                    Source = SystemIcons.Application.ToImageSource()
                };
                container_.Parameters.SizingMethod = SizingMethod.Custom;
                container_.Parameters.Width = 100;
                container_.Parameters.Height = 100;
                container_.Title = ImageWindowOne;

                return true;
            }).AddToWorkspace(DockImageWorkspaceCategory, SystemIcons.Application.ToImageSource());

            this.chromeRegistry.RegisterWindowFactory(ImageWindowTwo).SetInitHandler((container_, state_) =>
            {
                container_.Content = new Image
                {
                    Source = SystemIcons.Error.ToImageSource()
                };
                container_.Parameters.SizingMethod = SizingMethod.Custom;
                container_.Parameters.Width = 100;
                container_.Parameters.Height = 100;
                container_.Title = ImageWindowTwo;

                return true;
            }).AddToWorkspace(DockImageWorkspaceCategory, SystemIcons.Error.ToImageSource());

            this.chromeRegistry.RegisterWindowFactory(ImageWindowThree).SetInitHandler((container_, state_) =>
            {
                container_.Content = new Image
                {
                    Source = SystemIcons.Information.ToImageSource()
                };
                container_.Parameters.SizingMethod = SizingMethod.Custom;
                container_.Parameters.Width = 100;
                container_.Parameters.Height = 100;
                container_.Title = ImageWindowThree;

                return true;
            }).AddToWorkspace(DockImageWorkspaceCategory, SystemIcons.Information.ToImageSource());


            this.chromeRegistry.RegisterWindowFactory(TextWindowOne).SetInitHandler((container_, state_) =>
            {
                container_.Content = new TextBox
                {
                    Text = "Hello!"
                };
                container_.Title = TextWindowOne;
                return true;
            }).AddToWorkspace(DockTextWorkspaceCategory);

            this.chromeRegistry.RegisterWindowFactory(TextWindowTwo).SetInitHandler((container_, state_) =>
            {
                container_.Content = new TextBox
                {
                    Text = "Bonjour!"
                };

                container_.Title = TextWindowTwo;
                return true;
            }).AddToWorkspace(DockTextWorkspaceCategory);

            this.chromeRegistry.RegisterWindowFactory(TextWindowThree).SetInitHandler((container_, state_) =>
            {
                container_.Content = new TextBox
                {
                    Text = "Szevasz!"
                };
                container_.Title = TextWindowThree;
                return true;
            }).AddToWorkspace(DockTextWorkspaceCategory);


            //tile view workspaces
            this.chromeRegistry.RegisterWindowFactory(ImageWindowOne2).SetInitHandler((container_, state_) =>
            {
                container_.Content = new Image
                {
                    Source = SystemIcons.Application.ToImageSource()
                };
                container_.Parameters.SizingMethod = SizingMethod.Custom;
                container_.Parameters.Width = 100;
                container_.Parameters.Height = 100;
                container_.Title = ImageWindowOne2;

                return true;
            }).AddToWorkspace(TileImageWorkspaceCategory, SystemIcons.Application.ToImageSource(), LayoutEngine.Tile);

            this.chromeRegistry.RegisterWindowFactory(ImageWindowTwo2).SetInitHandler((container_, state_) =>
            {
                container_.Content = new Image
                {
                    Source = SystemIcons.Error.ToImageSource()
                };
                container_.Parameters.SizingMethod = SizingMethod.Custom;
                container_.Parameters.Width = 100;
                container_.Parameters.Height = 100;
                container_.Title = ImageWindowTwo2;

                return true;
            }).AddToWorkspace(TileImageWorkspaceCategory, SystemIcons.Error.ToImageSource(), LayoutEngine.Tile);

            this.chromeRegistry.RegisterWindowFactory(ImageWindowThree2).SetInitHandler((container_, state_) =>
            {
                container_.Content = new Image
                {
                    Source = SystemIcons.Information.ToImageSource()
                };
                container_.Parameters.SizingMethod = SizingMethod.Custom;
                container_.Parameters.Width = 100;
                container_.Parameters.Height = 100;
                container_.Title = ImageWindowThree2;

                return true;
            }).AddToWorkspace(TileImageWorkspaceCategory, SystemIcons.Information.ToImageSource(), LayoutEngine.Tile);


            this.chromeRegistry.RegisterWindowFactory(TextWindowOne2).SetInitHandler((container_, state_) =>
            {
                container_.Content = new TextBox
                {
                    Text = "Hello!"
                };
                container_.Parameters.SizingMethod = SizingMethod.SizeToContent;
                container_.Title = TextWindowOne2;
                return true;
            }).AddToWorkspace(TileTextWorkspaceCategory, null, LayoutEngine.Tile);

            this.chromeRegistry.RegisterWindowFactory(TextWindowTwo2).SetInitHandler((container_, state_) =>
            {
                container_.Content = new TextBox
                {
                    Text = "Bonjour!"
                };
                container_.Parameters.SizingMethod = SizingMethod.SizeToContent;
                container_.Title = TextWindowTwo2;

                return true;
            }).AddToWorkspace(TileTextWorkspaceCategory, null, LayoutEngine.Tile);

            this.chromeRegistry.RegisterWindowFactory(TextWindowThree2).SetInitHandler((container_, state_) =>
            {
                container_.Content = new TextBox
                {
                    Text = "Szevasz!"
                };
                container_.Parameters.SizingMethod = SizingMethod.SizeToContent;
                container_.Title = TextWindowThree2;

                return true;
            }).AddToWorkspace(TileTextWorkspaceCategory, null, LayoutEngine.Tile);

        }

        private void RegisterWorkspaces()
        {
            //this.workspaceManager.RegisterWorkspace(imageWorkspaceId, this.imageWorkspaceFactories, false);
            //this.workspaceManager.RegisterWorkspace(textWorkspaceId, this.textWorkspaceFactories, false);
            //this.workspaceManager.RegisterWorkspace(imageWorkspaceId2, this.imageWorkspaceFactories2, true);
            //this.workspaceManager.RegisterWorkspace(textWorkspaceId2, this.textWorkspaceFactories2, true);
        }

    }
}
