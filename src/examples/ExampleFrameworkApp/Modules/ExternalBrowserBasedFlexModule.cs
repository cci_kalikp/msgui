﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/ExternalBrowserBasedFlexModule.cs#8 $
// $Change: 861162 $
// $DateTime: 2014/01/08 12:20:01 $
// $Author: anbuy $

using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.FlexModule;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Modules
{
    /// <summary>
    /// Flex module using an external window and process
    /// </summary>
    class ExternalBrowserBasedFlexModule : FlexModuleWebBaseImpl, IModule
    {
		private const string BrowserViewId = "BrowserViewId";
		private const string BrowserViewButtonId = "BrowserViewButtonId";

        #region Constructor

		public ExternalBrowserBasedFlexModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_) :
            base(chromeRegistry_, chromeManager_,
                new Uri(
                    Path.Combine(
                        Path.Combine(
                            Path.GetDirectoryName(
                                Assembly.GetExecutingAssembly().Location), "Lib"), "msguitest35.swf")),
                true)
        {
        }

        #endregion

        #region IModule Members

        public override void Initialize()
        {
			m_chromeRegistry.RegisterWindowFactory(BrowserViewId, CreateView);
			m_chromeRegistry.RegisterWidgetFactory(BrowserViewButtonId, m_chromeManager.ShowWindowButtonFactory());

			m_chromeManager.PlaceWidget(BrowserViewButtonId, "Views/Flex", new InitialShowWindowButtonParameters()
			{
        Image = new Icon(MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Properties.Resources.Chart, 32, 32).ToImageSource(),
				Text = "External Flex Browser",
				ToolTip = "Open External Browser Based Flex Window",
				Enabled = true,

				//window settings
				WindowFactoryID = BrowserViewId,
				InitialParameters = new InitialWindowParameters()
			});
			
			/* var flexButton = new RibbonButton(
				new Icon(Properties.Resources.Chart, 32, 32),    //TODO change icon
				"Open External Browser Based Flex Window",
				"External Flex Browser",
				(a_, b_) => CreateView(null));
			 m_ribbon.AddRibbonItem(
				 "Views",
				 "Flex External Browser",
				 new WrapperPannelRibbonItem(RibbonItemOrientation.Horizontal, flexButton));*/
        }

        #endregion

        protected override bool CreateView(IWindowViewContainer viewcontainer_, XDocument state)
		{
			base.CreateView(viewcontainer_, state);
			return false;
		}


        protected override object CallFromFlex(string functionName_, object[] arguments_)
        {
            return null;
        }
    }
}
