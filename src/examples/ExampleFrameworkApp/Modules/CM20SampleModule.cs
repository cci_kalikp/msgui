﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Windows.Controls;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;


namespace MorganStanley.MSDotNet.MSDesktop.Examples.ChromeManager
{
    public class CM20SampleModule : IModule
    {
        private readonly IChromeRegistry _chromeRegistry;
        private readonly IChromeManager _chromeManager;
        private readonly IApplication _application;
        private readonly IPersistenceService _persistenceService;

        public CM20SampleModule(IChromeRegistry chromeRegistry, IChromeManager chromeManager, IApplication application,
                                IPersistenceService persistenceService)
        {
            _chromeRegistry = chromeRegistry;
            _chromeManager = chromeManager;
            _application = application;
            _persistenceService = persistenceService;

            chromeRegistry.RegisterWidgetFactory("mySeparator", (container, state) =>
                {
                    container.Content = new Separator();
                    return true;
                });


        }

        #region IModule Members

        public void Initialize()
        {
            var cm20Tab = _chromeManager.Ribbon().AddTab("ChromeManager 2");
            var sampleGroup = cm20Tab.AddGroup("Sample");
            var reorderingGroup = cm20Tab.AddGroup("Reordering");
            var removalGroup = cm20Tab.AddGroup("Removal");

            sampleGroup.AddWidget("cm20_button", new InitialButtonParameters
                {
                    Text = "Button"
                });

            reorderingGroup.AddWidget("cm20_button_2", new InitialButtonParameters
                {
                    Text = "First registered"
                });

            reorderingGroup.AddWidget("cm20_button_3", new InitialButtonParameters
                {
                    Text = "Second registered"
                });

            reorderingGroup.AddWidget("cm20_button_3", null);
            reorderingGroup.AddWidget("cm20_button_2", null);

            IWidgetViewContainer button4 = null;
            button4 = removalGroup.AddWidget("cm20_button_4", new InitialButtonParameters
                {
                    Text = "Button",
                    Click = (s, e) =>
                        {
                            removalGroup.RemoveWidget(button4.FactoryID);
                        }
                });

            var button5 = removalGroup.AddWidget("cm20_button_5", new InitialButtonParameters
                {
                    Text = "Button"
                });
            removalGroup.RemoveWidget(button5);




            var tabs = _chromeManager.Tabs();

            foreach (var tab in tabs.Tabs)
            {
                AddWindowCounter(tab);
            }

            (tabs.Tabs as INotifyCollectionChanged).CollectionChanged += (s, e) =>
                {
                    if (e.NewItems != null)
                    {
                        foreach (IDockTab tab in e.NewItems)
                        {
                            AddWindowCounter(tab);
                        }
                    }
                    if (e.OldItems != null)
                    {
                        foreach (IDockTab tab in e.OldItems)
                        {
                            RemoveWindowCounter(tab);
                        }
                    }
                };

            _chromeManager.Tabs().LeftEdge.AddWidget("activateMain", new InitialButtonParameters
                {
                    Text = "Activate Main tab",
                    Click = (s, e) =>
                        {
                            var tab = _chromeManager.Tabs("Main");
                            if (tab != null)
                            {
                                tab.Activate();
                            }
                        }
                });

            var x = _chromeManager.Statusbar().RightSide.AddWidget("sbrfoo", new InitialStatusLabelParameters {Text = "Foo"}) as IStatusLabelViewContainer;
            x.Text = "mukoggye baszadek";

            _chromeManager.StatusBar.AddTextStatusItem("test_statusbar_item", StatusBarItemAlignment.Right);
            _chromeManager.StatusBar["test_statusbar_item"].UpdateStatus(StatusLevel.Information, "update.");
            _chromeManager.StatusBar["test_statusbar_item"].UpdateStatus(StatusLevel.Information, "another update.");



            _chromeRegistry.RegisterWindowFactory("specificDockingTest", (vc, s) =>
                {
                    vc.Content = new Button
                        {
                            Content = "foo"
                        };

                    return true;
                });

            _chromeManager.AddButton("sDT_1", "Docking/Specific", new InitialButtonParameters
                {
                    Text = "floating",
                    Click = (s, e) =>
                        {
                            var dockTarget = _chromeManager.CreateWindow(
                                "specificDockingTest",
                                new InitialWindowParameters
                                    {
                                        InitialLocation = InitialLocation.FloatingOnly
                                    });
                            _chromeManager.AddShowWindowButton(
                                "sDT_2",
                                "Docking/Specific",
                                new InitialShowWindowButtonParameters
                                    {
                                        Text = "left",
                                        WindowFactoryID = "specificDockingTest",
                                        InitialParameters = new InitialWindowParameters
                                            {
                                                InitialLocation = InitialLocation.DockLeft,
                                                InitialLocationTarget = dockTarget
                                            }
                                    });
                        }
                });

            _chromeManager.Ribbon["Profiles"]["Custom profile management"].AddWidget(
                new InitialButtonParameters
                    {
                        Text = "Save profile",
                        Click = (sender, args) => _persistenceService.SaveCurrentProfile()
                    });
            _chromeManager.Ribbon["Profiles"]["Custom profile management"].AddWidget(
                new InitialButtonParameters
                    {
                        Text = "Delete profile",
                        Click = (sender, args) => _persistenceService.DeleteCurrentProfile()
                    });
            _application.ApplicationLoaded +=
                (sender1, args1) =>
                    {
                        var loadDropdown = _chromeManager.Ribbon["Profiles"]["Custom profile management"].AddWidget(
                            new InitialDropdownButtonParameters
                                {
                                    Text = "Load profile"
                                }
                            );
                        PopulateDropdown(loadDropdown);
                        _persistenceService.GetProfileNames().CollectionChanged +=
                            (o, eventArgs) =>
                                {
                                    loadDropdown.Clear();
                                    PopulateDropdown(loadDropdown);
                                };
                    };
            _chromeManager.Ribbon["Profiles"]["Custom profile management"].AddWidget(
                new InitialButtonParameters
                    {
                        Text = "Save profile as",
                        Click = (sender, args) => _persistenceService.SaveProfileAs()
                    });
            _chromeManager.Ribbon["Profiles"]["Custom profile management"].AddWidget(
                new InitialButtonParameters
                    {
                        Text = "Make current default",
                        Click = (sender, args) => _persistenceService.MakeProfileDefault(
                            _persistenceService.GetCurrentProfileName())
                    });
            _chromeManager.Ribbon["Profiles"]["Custom profile management"].AddWidget(
                new InitialButtonParameters
                    {
                        Text = "Save profile with random name",
                        Click = (sender, args) => _persistenceService.SaveProfileAs(Guid.NewGuid().ToString("N"))
                    });
        }

        private void PopulateDropdown(IWidgetViewContainer loadDropdown)
        {
            foreach (var profileName in _persistenceService.GetProfileNames())
            {
                string name = profileName;
                loadDropdown.AddWidget(
                    new InitialButtonParameters
                        {
                            Text = name,
                            Click = (sender, args) => _persistenceService.LoadProfile(name)
                        });
            }
        }

        private void RemoveWindowCounter(IDockTab tab)
        {
            tab.HeaderPostItems.Clear();
        }

        private void AddWindowCounter(IDockTab tab)
        {
            var tabButton = tab.HeaderPostItems.AddWidget(Guid.NewGuid().ToString(), new InitialButtonParameters
                {
                    Text = tab.Windows.Count.ToString(CultureInfo.InvariantCulture),
                    Click = (s, e) =>
                        {
                            var msg = string.Empty;
                            foreach (var w in tab.Windows)
                            {
                                msg += w.Title + Environment.NewLine;
                            }
                            TaskDialog.ShowMessage(msg);
                        }
                }) as IButtonViewContainer;

            (tab.Windows as INotifyCollectionChanged).CollectionChanged += (s, e) =>
                {
                    tabButton.Text = tab.Windows.Count.ToString(CultureInfo.InvariantCulture);
                };
        }

        #endregion
    }
}
