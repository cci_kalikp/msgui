﻿using System;
using System.Windows;
using System.Windows.Controls;
using MSDesktop.Wormhole;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Runtime.InteropServices;

namespace MorganStanley.MSDotNet.MSGui.Examples.ExampleFrameworkApp.Modules.Air
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class AirPage : Page
    {
        IChromeManager chromeManager;
        WormholeBridgedApplication bridge;
        IChromeRegistry registry;

        public AirPage(WormholeBridgedApplication air, IChromeManager chrome, IChromeRegistry reg)
        {
            bridge = air;
            registry = reg;
            chromeManager = chrome;
            InitializeComponent();
            setupBindings();
        }

        public void setupBindings()
        {
            WindowList.ItemsSource = chromeManager.Windows;
            AirFactories.ItemsSource = bridge.WindowFactories;

        }

        private void GetStateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                XDocument doc = bridge.TestGetWindowXml(WindowList.SelectedItem as IWindowViewContainer);
                InputText.Text = doc.ToString();
            }
            catch (Exception x)
            {
                MessageBox.Show("Error getting window XML via AIR bridge. Are you sure this is an AIR window? " + x);
            }
        }

        private void LaunchWithStateButton_Click(object sender, RoutedEventArgs e)
        {
            XDocument doc = null;
            if (InputText.Text.Length > 0)
            {
                try
                {
                    doc = XDocument.Parse(InputText.Text);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to parse input text. Clear text box and re-try, or fix xml: " + ex);
                    return;
                }
            }

            bridge.CreateWindow(FactoryName.Text, doc);
        }

        private void ShutdownButton_Click(object sender, RoutedEventArgs e)
        {
            bridge.IpcBasedApplicationProxy.TestShutdownMessage();
        }
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Not Implemented. Just close the .NET window!");
        }

        private void AirFactories_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FactoryName.Text = AirFactories.SelectedItem.ToString();
        }

        private void addFactoryButton_Click(object sender, RoutedEventArgs e)
        {
            this.bridge.RegisterWindowFactory(FactoryName.Text);
        }

        private void cmLaunch_Click(object sender, RoutedEventArgs e)
        {
            XDocument doc = null;
            if (InputText.Text.Length > 0)
            {
                try
                {
                    doc = XDocument.Parse(InputText.Text);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to parse input text. Clear text box and re-try, or fix xml: " + ex);
                    return;
                }
            }
            chromeManager.CreateWindow(FactoryName.Text, new InitialAirWindowParameters { InitialState = doc, InitialLocation = InitialLocation.Floating | InitialLocation.PlaceAtCursor });
        }

        private void sendEvent_Click(object sender, RoutedEventArgs e)
        {
            XDocument doc = parseXmlDoc();
            if (doc == null) return;
            bridge.IpcBasedApplicationProxy.SendSocketText(doc.ToString());
        }

        

        private XDocument parseXmlDoc()
        {
            XDocument doc = null;
            if (InputText.Text.Length > 0)
            {
                try
                {
                    doc = XDocument.Parse(InputText.Text);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to parse input text. Clear text box and re-try, or fix xml: " + ex);
                    return null;
                }
            }
            return doc;

        }
        [DllImport("user32.dll", SetLastError = true)]
        public static extern int SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        private void setParentButton_Click(object sender, RoutedEventArgs e)
        {
            int arg1 = 0, arg2 = 0;
            try
            {
                arg1 = Int32.Parse(setParentParam1.Text);
                arg2 = Int32.Parse(setParentParam2.Text);
            }
            catch (Exception x)
            {
                MessageBox.Show("Sorry you need 2 numeric params!"); return;
            }
           SetParent(new IntPtr(arg1), new IntPtr(arg2));
        }

    }
}