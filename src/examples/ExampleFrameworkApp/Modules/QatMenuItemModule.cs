﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ExampleFrameworkApp/Modules/QatMenuItemModule.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Modules
{
  class QatMenuItemModule : IModule
  {
    private readonly IShellWindowMenuFacade m_menuFacade;
    private readonly IShellWindowMenuFacade m_menuFacade2;
    private readonly IControlParadigmRegistrator m_controlParadigmRegistrator;
    private readonly IControlParadigmImplementator m_controlParadigmImplementator;

    public QatMenuItemModule(IShellWindowMenuFacade menuFacade_, IControlParadigmRegistrator controlParadigmRegistrator_, IControlParadigmImplementator controlParadigmImplementator_)
    {
      m_menuFacade = menuFacade_;
      m_controlParadigmRegistrator = controlParadigmRegistrator_;
      m_controlParadigmImplementator = controlParadigmImplementator_;
    }

    #region Implementation of IModule

    public void Initialize()
    {
      // add all menu items to the QAT (implicitly adds them to ribbon as per the XamRibbon's normal operation)
      m_controlParadigmRegistrator.Register("MsGuiLoadLayoutMenuItem", m_menuFacade.LoadLayoutMenu);
      m_controlParadigmImplementator["QAT"].AddItem("MsGuiLoadLayoutMenuItem");
      m_controlParadigmRegistrator.Register("MsGuiSaveLayoutButtonItem", m_menuFacade.SaveLayoutButton);
      m_controlParadigmImplementator["QAT"].AddItem("MsGuiSaveLayoutButtonItem");
      m_controlParadigmRegistrator.Register("MsGuiSaveLayoutAsMenuItem", m_menuFacade.SaveLayoutAsMenu);
      m_controlParadigmImplementator["QAT"].AddItem("MsGuiSaveLayoutAsMenuItem");
      m_controlParadigmRegistrator.Register("MsGuiDeleteCurrentLayoutButtonItem", m_menuFacade.DeleteCurrentLayoutButton);
      m_controlParadigmImplementator["QAT"].AddItem("MsGuiDeleteCurrentLayoutButtonItem");
      m_controlParadigmRegistrator.Register("MsGuiExitButtonItem", m_menuFacade.ExitButton);
      m_controlParadigmImplementator["QAT"].AddItem("MsGuiExitButtonItem");

      m_controlParadigmRegistrator.Register("MSGuiPrintButtonItem", m_menuFacade.PrintButton);
      m_controlParadigmImplementator["QAT"].AddItem("MSGuiPrintButtonItem");
      m_controlParadigmRegistrator.Register("MSGuiPrintPreviewButtonItem", m_menuFacade.PrintPreviewButton);
      m_controlParadigmImplementator["QAT"].AddItem("MSGuiPrintPreviewButtonItem");
    }

    #endregion
  }
}
