﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using MorganStanley.MSDotNet.My;
using log4net.Appender;
using log4net.Config;
using System.IO;
using log4net.Core;

namespace DebuggingExample
{
    public class MsLog4NetLoggerProvider : IMSLoggerProvider
    {
        /// <summary>
        /// Constructs an MSLog4NetLoggerProvider instance.
        /// This will get the configuration from the Application Configuration Settings
        /// </summary>
        public MsLog4NetLoggerProvider(string logFile, IEnumerable<KeyValuePair<string, string>> additionalLogHeaders = null)
        {
            //note that 1.2.11 of XmlConfigurator returns an ICollection where as 1.2.10 does not.  Therefore 1.2.11 must be used.
            XmlConfigurator.Configure();

            var layout = new PatternLayout(additionalLogHeaders)
            {
                IgnoresException = false,
                ConversionPattern =
                    "%date{yyyy-MM-dd HH:mm:ss} %-5level [\"%-3thread\"] [\"%logger\"] %message%newline-exceptionǁ%newline"
            };
            layout.ActivateOptions();

            var appender = new FileAppender
            {
                Encoding = Encoding.UTF8,
                File = logFile,
                AppendToFile = true,
                Layout = layout
            };
            appender.ActivateOptions();

            BasicConfigurator.Configure(appender);
        }


        /// <summary>
        /// Creates a named MSLog4NetLogger instance.
        /// </summary>
        /// <param name="name">The logger's name.</param>
        /// <returns>The MSLog4NetLogger instance that the provider created.</returns>
        public IMSLogger CreateLogger(string name)
        {
            return new MsLog4NetLogger(name);
        }

        /// <summary>
        /// Creates a named MSLog4NetLogger for the type T.
        /// </summary>
        /// <typeparam name="T">Type of the class that the logger instance is bound to.</typeparam>
        /// <param name="name">The logger's name.</param>
        /// <returns>The MSLog4NetLogger instance that the provider created.</returns>
        public IMSLogger CreateLogger<T>(string name)
        {
            return new MsLog4NetLogger(typeof(T));
        }

        /// <summary>
        /// Releasing resources associated with this Provider.
        /// </summary>
        public void Dispose()
        {
        }

        private class PatternLayout : log4net.Layout.PatternLayout
        {
            private readonly IEnumerable<KeyValuePair<string, string>> _additionalLogHeaders;

            public PatternLayout(IEnumerable<KeyValuePair<string, string>> additionalLogHeaders)
            {
                _additionalLogHeaders = additionalLogHeaders;
            }

            public override string Header
            {
                get
                {
                    var header = "# Log4Net Log File" + Environment.NewLine +
                                 "#" + Environment.NewLine +
                                 "# Log Version:     1.0" + Environment.NewLine +
                                 "# Created:         " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss zzz") + Environment.NewLine +
                                 "# Username:        " + Environment.UserName + Environment.NewLine +
                                 "# Machine:         " + Environment.MachineName + Environment.NewLine +
                                 "# Process Name:    " + Process.GetCurrentProcess().ProcessName + Environment.NewLine +
                                 "# Process Id:      " + Process.GetCurrentProcess().Id + Environment.NewLine +
                                 "# Process Version: " + Assembly.GetEntryAssembly().GetName().Version + Environment.NewLine +
                                 "# Commandline:     " + Environment.GetCommandLineArgs().First() + " " + string.Join(" ", Environment.GetCommandLineArgs().Skip(1).Select(arg => arg.Contains(" ") ? "\"" + arg + "\"" : arg)) + Environment.NewLine +
                                 "# OS Version:      " + Environment.OSVersion + Environment.NewLine +
                                 "# DotNet Version:  " + Environment.Version + Environment.NewLine;


                    if (_additionalLogHeaders != null)
                    {
                        header = _additionalLogHeaders.Aggregate(header, (current, pair) => current + string.Format("# {0,-17}{1}{2}", pair.Key + ":", pair.Value, Environment.NewLine));
                    }

                    header += "# Format:         \"<DateTime> <Log Level> [\"<Managed Thread ID>\"] [\"<Logger/Class Name>\"] [\"<Member Name (optional)>\"] [\"<Source File (optional)>\"] [\"<Source Line Number (optional)>\"] <message>ǁ\"" + Environment.NewLine +
                              "#" + Environment.NewLine +
                              "#" + Environment.NewLine;

                    return header;
                }
            }

            public override void ActivateOptions()
            {
                base.ActivateOptions();
                AddConverter("newline-exception", typeof(ExceptionPatternConverter));
            }

            private sealed class ExceptionPatternConverter : log4net.Layout.Pattern.PatternLayoutConverter
            {
                // Methods
                public ExceptionPatternConverter()
                {
                    IgnoresException = false;
                }

                protected override void Convert(TextWriter writer, LoggingEvent loggingEvent)
                {
                    string exceptionString = loggingEvent.GetExceptionString();
                    if (!string.IsNullOrEmpty(exceptionString))
                    {
                        writer.WriteLine(Environment.NewLine + exceptionString);
                    }
                }
            }


        }
    }
}

