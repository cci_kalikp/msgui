﻿using System;
using MorganStanley.MSDotNet.Runtime; 


namespace DebuggingExample
{
  
  public class Program
  {
 
    [STAThread]
    public static void Main(string[] args)
    {
      AssemblyResolver.RunMain(args);
    } 
    [MSDEMain]
    public static void RealMain(string[] args)
    {
      var app = new App();
      app.Run();
    }
     
  } 
}
