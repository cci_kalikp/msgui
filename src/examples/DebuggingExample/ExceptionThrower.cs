﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DebuggingExample
{
    public static class ExceptionThrower
    {
        public static void ThrowException()
        {
            if (Environment.UserName == "pjenkbld" || Environment.GetCommandLineArgs().Contains("-noex")) return;
             throw new Exception("Exception thrown from " + ResolveMethodName());
        }

        private static string ResolveMethodName()
        {
            MethodBase m = new StackTrace().GetFrame(2).GetMethod();
            return (m.DeclaringType == null ? "No class" : m.DeclaringType.Name) + "." + m.Name; 
        }
    }
}
