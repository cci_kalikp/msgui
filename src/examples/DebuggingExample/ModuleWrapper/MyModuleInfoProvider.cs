﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;

namespace DebuggingExample.ModuleWrapper
{
    //cw Provide_Module_Info_When_Using_Module_Wrapper: 31 
    //{
    public class MyModuleInfoProvider:IModuleInfoProvider
    {
        public Type GetModuleType(Type wrapperType_)
        {
            if (typeof (MyModuleWrapper<>) == wrapperType_.GetGenericTypeDefinition())
            {
                var types = wrapperType_.GetGenericArguments();
                return types.FirstOrDefault();
            }
            return null;
        }
    }
    //}
}
