﻿using System;
using System.Collections.Generic; 
using Microsoft.Practices.Unity;

namespace DebuggingExample.ModuleWrapper
{
    public class MyModuleRepository: IMyModuleRepository
    {
        private Dictionary<string, IMyModule> modules; 
        public MyModuleRepository(IUnityContainer container_)
        {
            container_.RegisterInstance<IMyModuleRepository>(this);
        }
        public void Initialize()
        {
           modules = new Dictionary<string, IMyModule>();
        }
         
        public void RegisterModule(IMyModule module_)
        {
            modules[module_.Id] = module_;
        }


        public IMyModule GetModule(string id_)
        {
            IMyModule module;
            modules.TryGetValue(id_, out module);
            return module;
        }
    }
}
