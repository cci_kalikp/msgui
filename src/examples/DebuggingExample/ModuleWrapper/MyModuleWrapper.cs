﻿using System;

using Microsoft.Practices.Composite.Modularity;
//cw Provide_Module_Info_When_Using_Module_Wrapper: 20
/** ## Namespace */
/**Use the following namespace*/
//{
using MorganStanley.MSDotNet.MSGui.Core;
//}

namespace DebuggingExample.ModuleWrapper
{
    //cw Provide_Module_Info_When_Using_Module_Wrapper: 10 
    /**
     * ## Description
     * If developer uses some kind of module wrapper to wrap the real module, we provide a way to show the real module type in [Module Viewer][cw_ref Debug_Module] for such module.
     */
    /** We provide the ModuleInfoProviderAttribute to allow a module type information provider(implement IModuleInfoProvider) to tell us the real module type providing the type of module wrapper as following*/
    /** This attribute can be defined either at the parent class module wrapper or at the sub class module wrapper, following are 2 examples of how it's used*/
    
    //cw Provide_Module_Info_When_Using_Module_Wrapper: 30 
    /** ## Example 1 */
    //{
    [ModuleInfoProvider(Provider = typeof(MyModuleInfoProvider))]
    public class MyModuleWrapper<T>:IModule where T:IMyModule, new()
    {
        private readonly T module;
        private readonly IMyModuleRepository repository; 
        public MyModuleWrapper(IMyModuleRepository repository_)
        {
            module = new T();
            this.repository = repository_;
        }
         
        public void Initialize()
        {
            module.Initialize(repository);
        }
    } 
    //}
}
