﻿namespace DebuggingExample.ModuleWrapper
{
    public interface IMyModule
    {
        void Initialize(IMyModuleRepository repository_);
        string Id { get; }
    }
}
