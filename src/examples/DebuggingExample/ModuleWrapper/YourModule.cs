﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebuggingExample.ModuleWrapper
{
    class YourModule:IYourModule
    {
        public void Initialize()
        {
             
        }

        public string Name
        {
            get { return "YourModule"; }
        }
    }
}
