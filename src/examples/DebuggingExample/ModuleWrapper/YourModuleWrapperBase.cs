﻿using System;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;

namespace DebuggingExample.ModuleWrapper
{
    //cw Provide_Module_Info_When_Using_Module_Wrapper: 40
    /** ## Example 2 */
    //{
    [ModuleInfoProvider(Provider = typeof(YourModuleInfoProvider))]
    public abstract class YourModuleWrapperBase:IModule
    {
        protected abstract IYourModule CreateModule();
        public void Initialize()
        {
            var module = CreateModule();
            module.Initialize();
        }
    }
    //}
     
}
