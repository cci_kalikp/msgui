﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;

namespace DebuggingExample.ModuleWrapper
{
    public interface IMyModuleRepository:IModule
    {
        void RegisterModule(IMyModule module_);

        IMyModule GetModule(string id_);
    }
}
