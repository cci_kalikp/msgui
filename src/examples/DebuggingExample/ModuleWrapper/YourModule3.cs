﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;

namespace DebuggingExample.ModuleWrapper
{
    class YourModule3:IYourModule
    {
        public YourModule3(IUnityContainer container_)
        {
            
        }
        public void Initialize()
        { 
        } 
        public string Name
        {
            get { return "YourModule3"; }
        }
    }
}
