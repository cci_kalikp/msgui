﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;

namespace DebuggingExample.ModuleWrapper
{
    //cw Provide_Module_Info_When_Using_Module_Wrapper: 42
    //{
    public class YourModuleInfoProvider:IModuleInfoProvider
    {
        public Type GetModuleType(Type wrapperType_)
        {
            if (wrapperType_ == typeof (YourModuleWrapper)) return typeof (YourModule);
            if (wrapperType_ == typeof (YourModuleWrapper2)) return typeof (YourModule2);
            if (wrapperType_ == typeof (YourModuleWrapper3)) return typeof (YourModule3);
            return wrapperType_;
        }
    }
    //}
}
