﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;

namespace DebuggingExample.ModuleWrapper
{
    class YourModule2:IYourModule
    {
        public YourModule2(IUnityContainer container_)
        {
            
        }
        public void Initialize()
        {
            ExceptionThrower.ThrowException(); 
        } 
        public string Name
        {
            get { return "YourModule2"; }
        }
    }
}
