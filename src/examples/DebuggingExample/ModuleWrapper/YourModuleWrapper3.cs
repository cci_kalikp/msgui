﻿
using Microsoft.Practices.Unity;

namespace DebuggingExample.ModuleWrapper
{
    public class YourModuleWrapper3:YourModuleWrapperBase
    {
        private IUnityContainer container;
        public YourModuleWrapper3(IUnityContainer container_)
        {
            container = container_;
        }

        protected override IYourModule CreateModule()
        {
            return new YourModule3(container);
        } 
    }
}
