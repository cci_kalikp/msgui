﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebuggingExample.ModuleWrapper
{
    class MyModule2 : IMyModule
    {
        public MyModule2()
        {
            Id = "MyModule2";
        }
        public string Id { get; private set; }

        public void Initialize(ModuleWrapper.IMyModuleRepository repository_)
        {
            //to some initialization work here
            repository_.RegisterModule(this);
        }
    }  
}
