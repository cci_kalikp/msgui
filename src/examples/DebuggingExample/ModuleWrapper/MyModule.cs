﻿using System;

namespace DebuggingExample.ModuleWrapper
{
    class MyModule:IMyModule
    {
        public MyModule()
        {
            Id = "MyModule";
            ExceptionThrower.ThrowException(); 
        }
        public string Id { get; private set; }

        public void Initialize(ModuleWrapper.IMyModuleRepository repository_)
        {
            //to some initialization work here
            repository_.RegisterModule(this);
        }
    }  
}
