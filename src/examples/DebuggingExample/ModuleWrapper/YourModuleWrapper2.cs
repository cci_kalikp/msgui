﻿
using Microsoft.Practices.Unity;

namespace DebuggingExample.ModuleWrapper
{
    public class YourModuleWrapper2:YourModuleWrapperBase
    {
        private IUnityContainer container;
        public YourModuleWrapper2(IUnityContainer container_)
        {
            container = container_;
        }

        protected override IYourModule CreateModule()
        {
            return new YourModule2(container);
        } 
    }
}
