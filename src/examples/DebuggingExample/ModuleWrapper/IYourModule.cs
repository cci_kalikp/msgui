﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebuggingExample.ModuleWrapper
{
    public interface IYourModule
    {
        void Initialize();
        string Name { get; }
    }
}
