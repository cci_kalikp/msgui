﻿
namespace DebuggingExample.ModuleWrapper
{
    //cw Provide_Module_Info_When_Using_Module_Wrapper: 41
    //{
    public class YourModuleWrapper:YourModuleWrapperBase
    {
        protected override IYourModule CreateModule()
        {
            return new YourModule();
        } 
    }
    //}
}
