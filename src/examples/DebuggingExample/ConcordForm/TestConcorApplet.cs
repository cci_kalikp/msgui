﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MorganStanley.Ecdev.Desktop.Utils;
using MorganStanley.IED.Concord.Application; 
using MorganStanley.MSDotNet.MSNet;

namespace DebuggingExample
{
    public class TestConcorApplet : ConcordAppletBase, IWindowFactory
    {
        protected const string TOP_MENU = "Tools";    
        protected bool _hideMenu;

        public string[] TOPIC_CASH_EVENT_VIEWER = new string[] { "CashEventViewer" }; 

        /// <summary>
        /// Construct the applet and set the title, The title of this 
        /// applet ***MUST*** match the WindowFactory attribute in the 
        /// corresponding form we are creating from this applet.
        /// </summary>
        /// <param name="uiLoop_"></param>
        public TestConcorApplet(IMSNetLoop uiLoop_)
            : base(uiLoop_)
        {
            // This title ***MUST*** match the WindowFactory attribute on the 
            // form that is created by this applet.
            _title = "Test Concord Applet"; 
        }

        /// <summary>
        /// This method invoke the base OnConnect method and then 
        /// adds a menuitme onto the Tools menu in the concord container
        /// that will invoked our OnCommandButtonClick when it is invoked.
        /// </summary>
        /// <param name="application_"></param>
        public override void OnConnect(IConcordApplication application_)
        {
            base.OnConnect(application_);

            var button =
                (ICommandBarButton)
                application_.CommandBars.AddNamedCommand(_title, "Test Concord Form", null, CommandBarControlType.Button,
                                                         new ItemClickEventHandler(menuClick), _uiLoop);
            button.Style = CommandBarControlStyle.IconAndCaption;
            _application.CommandBars[TOP_MENU].AddCommand(button);
        }

        /// <summary>
        /// In response to a users selection of our button in the Tools menu  
        /// ( registered in above OnConnect method ) we create a new form and
        /// add it to the window mangager.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e_"></param>
        public virtual void menuClick(object sender, ItemClickEventArgs e_)
        {
            try
            {
                //Form form = CreateForm() ;
                IWindow iw = _application.WindowManager.CreateWindow(_title, GetForm(_title, null));
            }
            catch (System.Exception ex_)
            {
                 
            }
        }

        public Form GetForm(string type_, System.Xml.XmlNode xmlNode_)
        {
            try
            {
                return new TestConcordForm(_uiLoop, _application);
                //form = CreateForm(_uiLoop, _application);
            }
            catch (System.Exception ex_)
            {
                 
            }
            return null;
        }

        /*
        /// <summary>
        /// Create an instance of the TestLinkingApplet.
        /// Dont forget the line to set up the applet context.
        /// </summary>
        /// <returns></returns>
        private Form CreateForm()
        {
          CashEventForm form = new CashEventForm(_uiLoop, _application);
          form.SetAppContext( _uiLoop, _application ) ;

          return ( form ) ;
        }
        */
 

    }
}
