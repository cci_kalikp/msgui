﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MorganStanley.Ecdev.ConcordUtils;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSNet;

namespace DebuggingExample
{
    public partial class TestConcordForm : EcdevConcordForm
    {
        IMSNetLoop _loop;

        public TestConcordForm()
        {
            InitializeComponent();
        }

        public TestConcordForm(IMSNetLoop loop_, IConcordApplication application_)
            : base(loop_, application_)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

        }

        public void SetAppContext(IMSNetLoop loop_, IConcordApplication application_)
        {
            _loop = loop_;
            _application = application_;
        }

        public override void SubscribeToMediator(System.Xml.XmlNode state_) { }
    }
}
