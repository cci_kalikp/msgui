﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSLog.Log4Net;
using log4net;
using MorganStanley.MSDotNet.My;

namespace DebuggingExample
{
    /// <summary>
    /// This class implements the IMSLogger interface.
    /// Users don't have to call the IsDebugLoggable etc log guards, the Debug(), Info() Error() etc methods will check the loggable guard for you. 
    /// </summary>
    internal class MsLog4NetLogger : IMSLogger
    {
        private readonly ILog _logger;

        ///["{MEMBER}"] ["{PATH}"] ["{LINENUMBER}"] {MESSAGE}
        private const string _methodInfFormat = "[\"{0}\"] [\"{1}\"] [\"{2}\"] {3}";

        internal protected MsLog4NetLogger(Type type)
        {
            _logger = LogManager.GetLogger(type);
        }

        internal protected MsLog4NetLogger(string name)
        {
            _logger = LogManager.GetLogger(name);
        }

        /// <summary>
        ///Answers with the name of this logger. 
        ///Should be the same as the name given to LoggerFactory.CreateLogger(String), when the logger was created. 
        /// </summary>
        /// <returns>Name of this logger</returns>
        public string GetName()
        {
            return _logger.Logger.Name;
        }


        //////////////////////////////////////// IMSLogger Interface ////////////////////////////////////////

        /// <summary>
        /// The class name to be used to generate the log entry
        /// </summary>
        public string ClassName
        {
            get
            {
                return GetName();
            }
        }

        /// <summary>
        /// Use to guard Debug() logging where there is cost in building the log message
        /// object before Debug() is called.
        /// </summary>
        /// <returns>return true if there is a destination to accept the Debug() message.</returns>
        public bool IsDebugLoggable()
        {
            return _logger.IsDebugEnabled;
        }

        public void Debug(string message)
        {
            DebugCore(message);
        }

        public void Debug(string message, Exception ex)
        {
            DebugCore(message, ex);
        }

        public void Debug(string message, string methodName)
        {
            DebugCore(message, null, methodName);
        }

        public void Debug(string message, Exception ex, string methodName)
        {
            DebugCore(message, ex, methodName);
        }

        public void DebugWithFormat(string format, params object[] parameters)
        {
            DebugCore(string.Format(format, parameters));
        }

        public void Debug(Func<string> messageFunc)
        {
            DebugCore(messageFunc);
        }

        public void Debug(Func<string> messageFunc, Exception ex)
        {
            DebugCore(messageFunc, ex);
        }

        public void Debug(Func<string> messageFunc, string methodName)
        {
            DebugCore(messageFunc, null, methodName);
        }

        public void Debug(Func<string> messageFunc, Exception ex, string methodName)
        {
            DebugCore(messageFunc, ex, methodName);
        }

        [Obsolete]
        public void Debug(string message, string className, string methodName)
        {
            Debug(message, methodName);
        }

        [Obsolete]
        public void Debug(string message, Exception ex, string className, string methodName)
        {
            Debug(message, ex, methodName);
        }

        [Obsolete]
        public void Debug(Func<string> messageFunc, Exception ex, string className, string methodName)
        {
            DebugCore(messageFunc, ex, methodName);
        }

        [Obsolete]
        public void Debug(Func<string> messageFunc, string className, string methodName)
        {
            DebugCore(messageFunc, null, methodName);
        }


        private void DebugCore(Func<string> messageFunc, Exception ex = null, string methodName = "")
        {
            if (_logger.IsDebugEnabled)
            {
                if (ex == null)
                {
                    _logger.Debug(string.Format(_methodInfFormat, methodName, "", 0, messageFunc()));
                }
                else
                {
                    _logger.Debug(string.Format(_methodInfFormat, methodName, "", 0, messageFunc()), ex);
                }
            }
        }

        private void DebugCore(string message, Exception ex = null, string methodName = "")
        {
            if (_logger.IsDebugEnabled)
            {
                if (ex == null)
                {
                    _logger.Debug(string.Format(_methodInfFormat, methodName, "", 0, message));
                }
                else
                {
                    _logger.Debug(string.Format(_methodInfFormat, methodName, "", 0, message), ex);
                }
            }
        }

        /// <summary>
        /// Use to guard Info() logging where there is cost in building the log message
        /// object before Info() is called.
        /// </summary>
        /// <returns>return true if there is a destination to accept the Info() message.</returns>
        public bool IsInfoLoggable()
        {
            return _logger.IsInfoEnabled;
        }

        public void Info(string message)
        {
            InfoCore(message);
        }

        public void Info(string message, Exception ex)
        {
            InfoCore(message, ex);
        }

        public void Info(string message, string methodName)
        {
            InfoCore(message, null, methodName);
        }

        public void Info(string message, Exception ex, string methodName)
        {
            InfoCore(message, ex, methodName);
        }

        public void Info(Func<string> messageFunc, Exception ex, string methodName)
        {
            InfoCore(messageFunc, ex, methodName);
        }

        public void InfoWithFormat(string format, params object[] parameters)
        {
            InfoCore(string.Format(format, parameters));
        }

        public void Info(Func<string> messageFunc)
        {
            InfoCore(messageFunc);
        }

        public void Info(Func<string> messageFunc, Exception ex)
        {
            InfoCore(messageFunc, ex);
        }

        public void Info(Func<string> messageFunc, string methodName)
        {
            InfoCore(messageFunc, null, methodName);
        }

        [Obsolete]
        public void Info(string message, string className, string methodName)
        {
            InfoCore(message, null, methodName);
        }

        [Obsolete]
        public void Info(string message, Exception ex, string className, string methodName)
        {
            InfoCore(message, ex, methodName);
        }

        [Obsolete]
        public void Info(Func<string> messageFunc, Exception ex, string className, string methodName)
        {
            InfoCore(messageFunc, ex, methodName);
        }

        [Obsolete]
        public void Info(Func<string> messageFunc, string className, string methodName)
        {
            InfoCore(messageFunc, null, methodName);
        }

        private void InfoCore(Func<string> messageFunc, Exception ex = null, string methodName = "")
        {
            if (_logger.IsInfoEnabled)
            {
                if (ex == null)
                {
                    _logger.Info(string.Format(_methodInfFormat, methodName, "", 0, messageFunc()));
                }
                else
                {
                    _logger.Info(string.Format(_methodInfFormat, methodName, "", 0, messageFunc()), ex);
                }
            }
        }

        private void InfoCore(string message, Exception ex = null, string methodName = "")
        {
            if (_logger.IsInfoEnabled)
            {
                if (ex == null)
                {
                    _logger.Info(string.Format(_methodInfFormat, methodName, "", 0, message));
                }
                else
                {
                    _logger.Info(string.Format(_methodInfFormat, methodName, "", 0, message), ex);
                }
            }
        }


        /// <summary>
        /// Use to guard Notice() logging where there is cost in building the log message
        /// object before Notice() is called.
        /// </summary>
        /// <returns>return true if there is a destination to accept the Notice() message.</returns>
        public bool IsNoticeLoggable()
        {
            return _logger.IsInfoEnabled;
        }

        public void Notice(string message)
        {
            InfoCore(message);
        }

        public void Notice(string message, Exception ex)
        {
            InfoCore(message, ex);
        }

        public void Notice(string message, string methodName)
        {
            InfoCore(message, null, methodName);
        }

        public void Notice(string message, Exception ex, string methodName)
        {
            InfoCore(message, ex, methodName);
        }

        public void NoticeWithFormat(string format, params object[] parameters)
        {
            InfoCore(string.Format(format, parameters));
        }

        public void Notice(Func<string> messageFunc)
        {
            InfoCore(messageFunc);
        }

        public void Notice(Func<string> messageFunc, Exception ex)
        {
            InfoCore(messageFunc, ex);
        }

        public void Notice(Func<string> messageFunc, string methodName)
        {
            InfoCore(messageFunc, null, methodName);
        }

        public void Notice(Func<string> messageFunc, Exception ex, string methodName)
        {
            InfoCore(messageFunc, ex, methodName);
        }

        [Obsolete]
        public void Notice(string message, string className, string methodName)
        {
            InfoCore(message, null, methodName);
        }

        [Obsolete]
        public void Notice(string message, Exception ex, string className, string methodName)
        {
            InfoCore(message, ex, methodName);
        }

        [Obsolete]
        public void Notice(Func<string> messageFunc, Exception ex, string className, string methodName)
        {
            InfoCore(messageFunc, ex, methodName);
        }

        [Obsolete]
        public void Notice(Func<string> messageFunc, string className, string methodName)
        {
            InfoCore(messageFunc, null, methodName);
        }


        /// <summary>
        /// Use to guard Warning() logging where there is cost in building the log message
        /// object before Warning() is called.
        /// </summary>
        /// <returns>return true if there is a destination to accept the Warning() message.</returns>
        public bool IsWarningLoggable()
        {
            return _logger.IsWarnEnabled;
        }

        public void Warning(string message)
        {
            WarningCore(message);
        }

        public void Warning(string message, Exception ex)
        {
            WarningCore(message, ex);
        }

        public void Warning(string message, string methodName)
        {
            WarningCore(message, null, methodName);
        }

        public void Warning(Func<string> messageFunc)
        {
            WarningCore(messageFunc);
        }

        public void Warning(Func<string> messageFunc, Exception ex)
        {
            WarningCore(messageFunc, ex);
        }

        public void Warning(Func<string> messageFunc, string methodName)
        {
            WarningCore(messageFunc, null, methodName);
        }

        public void Warning(Func<string> messageFunc, Exception ex, string methodName)
        {
            WarningCore(messageFunc, ex, methodName);
        }

        public void Warning(string message, Exception ex, string methodName)
        {
            WarningCore(message, ex, methodName);
        }

        public void WarningWithFormat(string format, params object[] parameters)
        {
            WarningCore(string.Format(format, parameters));
        }

        [Obsolete]
        public void Warning(string message, string className, string methodName)
        {
            WarningCore(message, null, methodName);
        }

        [Obsolete]
        public void Warning(string message, Exception ex, string className, string methodName)
        {
            WarningCore(message, ex, methodName);
        }

        [Obsolete]
        public void Warning(Func<string> messageFunc, string className, string methodName)
        {
            WarningCore(messageFunc, null, methodName);
        }

        [Obsolete]
        public void Warning(Func<string> messageFunc, Exception ex, string className, string methodName)
        {
            WarningCore(messageFunc, ex, methodName);
        }

        private void WarningCore(Func<string> messageFunc, Exception ex = null, string methodName = "")
        {
            if (_logger.IsWarnEnabled)
            {
                if (ex == null)
                {
                    _logger.Debug(string.Format(_methodInfFormat, methodName, "", 0, messageFunc()));
                }
                else
                {
                    _logger.Debug(string.Format(_methodInfFormat, methodName, "", 0, messageFunc()), ex);
                }
            }
        }

        private void WarningCore(string message, Exception ex = null, string methodName = "")
        {
            if (_logger.IsWarnEnabled)
            {
                if (ex == null)
                {
                    _logger.Warn(string.Format(_methodInfFormat, methodName, "", 0, message));
                }
                else
                {
                    _logger.Warn(string.Format(_methodInfFormat, methodName, "", 0, message), ex);
                }
            }
        }

        /// <summary>
        /// Use to guard Error() logging where there is cost in building the log message
        /// object before Error() is called.
        /// </summary>
        /// <returns>return true if there is a destination to accept the Error() message.</returns>
        public bool IsErrorLoggable()
        {
            return _logger.IsErrorEnabled;
        }

        public void Error(string message)
        {
            ErrorCore(message);
        }

        public void Error(string message, Exception ex)
        {
            ErrorCore(message, ex);
        }

        public void Error(string message, string methodName)
        {
            ErrorCore(message, null, methodName);
        }

        public void Error(string message, Exception ex, string methodName)
        {
            ErrorCore(message, ex, methodName);
        }

        public void ErrorWithFormat(string format, params object[] parameters)
        {
            ErrorCore(string.Format(format, parameters));
        }

        public void Error(Func<string> messageFunc)
        {
            ErrorCore(messageFunc);
        }

        public void Error(Func<string> messageFunc, Exception ex)
        {
            ErrorCore(messageFunc, ex);
        }

        public void Error(Func<string> messageFunc, string methodName)
        {
            ErrorCore(messageFunc, null, methodName);
        }

        public void Error(Func<string> messageFunc, Exception ex, string methodName)
        {
            ErrorCore(messageFunc, ex, methodName);
        }

        [Obsolete]
        public void Error(string message, string className, string methodName)
        {
            ErrorCore(message, null, methodName);
        }

        [Obsolete]
        public void Error(string message, Exception ex, string className, string methodName)
        {
            ErrorCore(message, ex, methodName);
        }

        [Obsolete]
        public void Error(Func<string> messageFunc, string className, string methodName)
        {
            ErrorCore(messageFunc, null, methodName);
        }

        [Obsolete]
        public void Error(Func<string> messageFunc, Exception ex, string className, string methodName)
        {
            ErrorCore(messageFunc, ex, methodName);
        }


        private void ErrorCore(Func<string> messageFunc, Exception ex = null, string methodName = "")
        {
            if (_logger.IsErrorEnabled)
            {
                if (ex == null)
                {
                    _logger.Error(string.Format(_methodInfFormat, methodName, "", 0, messageFunc()));
                }
                else
                {
                    _logger.Error(string.Format(_methodInfFormat, methodName, "", 0, messageFunc()), ex);
                }
            }
        }

        private void ErrorCore(string message, Exception ex = null, string methodName = "")
        {
            if (_logger.IsErrorEnabled)
            {
                if (ex == null)
                {
                    _logger.Error(string.Format(_methodInfFormat, methodName, "", 0, message));
                }
                else
                {
                    _logger.Error(string.Format(_methodInfFormat, methodName, "", 0, message), ex);
                }
            }
        }

        /// <summary>
        /// Use to guard Critical() logging where there is cost in building the log message
        /// object before Critical() is called.
        /// </summary>
        /// <returns>return true if there is a destination to accept the Critical() message.</returns>
        public bool IsCriticalLoggable()
        {
            return _logger.IsFatalEnabled;
        }

        public void Critical(string message)
        {
            CriticalCore(message);
        }

        public void Critical(string message, Exception ex)
        {
            CriticalCore(message, ex);
        }

        public void Critical(string message, string methodName)
        {
            CriticalCore(message, null, methodName);
        }

        public void Critical(string message, Exception ex, string methodName)
        {
            CriticalCore(message, ex, methodName);
        }

        public void CriticalWithFormat(string format, params object[] parameters)
        {
            CriticalCore(string.Format(format, parameters));
        }

        public void Critical(Func<string> messageFunc)
        {
            CriticalCore(messageFunc);
        }

        public void Critical(Func<string> messageFunc, Exception ex)
        {
            CriticalCore(messageFunc, ex);
        }

        public void Critical(Func<string> messageFunc, string methodName)
        {
            CriticalCore(messageFunc, null, methodName);
        }

        public void Critical(Func<string> messageFunc, Exception ex, string methodName)
        {
            CriticalCore(messageFunc, ex, methodName);
        }

        [Obsolete]
        public void Critical(string message, string className, string methodName)
        {
            Critical(message, methodName);
        }

        [Obsolete]
        public void Critical(string message, Exception ex, string className, string methodName)
        {
            Critical(message, ex, methodName);
        }

        [Obsolete]
        public void Critical(Func<string> messageFunc, string className, string methodName)
        {
            CriticalCore(messageFunc, null, methodName);
        }

        public void Critical(Func<string> messageFunc, Exception ex, string className, string methodName)
        {
            CriticalCore(messageFunc, ex, methodName);
        }



        private void CriticalCore(Func<string> messageFunc, Exception ex = null, string methodName = "")
        {
            if (_logger.IsFatalEnabled)
            {
                if (ex == null)
                {
                    _logger.Fatal(string.Format(_methodInfFormat, methodName, "", 0, messageFunc()));
                }
                else
                {
                    _logger.Fatal(string.Format(_methodInfFormat, methodName, "", 0, messageFunc()), ex);
                }
            }
        }

        private void CriticalCore(string message, Exception ex = null, string methodName = "")
        {
            if (_logger.IsFatalEnabled)
            {
                if (ex == null)
                {
                    _logger.Fatal(string.Format(_methodInfFormat, methodName, "", 0, message));
                }
                else
                {
                    _logger.Fatal(string.Format(_methodInfFormat, methodName, "", 0, message), ex);
                }
            }
        }


        /// <summary>
        /// Use to guard Alert() logging where there is cost in building the log message
        /// object before Alert() is called.
        /// </summary>
        /// <returns>return true if there is a destination to accept the Alert() message.</returns>
        public bool IsAlertLoggable()
        {
            return _logger.IsFatalEnabled;
        }

        public void Alert(string message)
        {
            CriticalCore(message);
        }

        public void Alert(string message, Exception ex)
        {
            CriticalCore(message, ex);
        }

        public void Alert(string message, string methodName)
        {
            CriticalCore(message, null, methodName);
        }

        public void Alert(string message, Exception ex, string methodName)
        {
            CriticalCore(message, ex, methodName);
        }

        public void AlertWithFormat(string format, params object[] parameters)
        {
            CriticalCore(string.Format(format, parameters));
        }

        public void Alert(Func<string> messageFunc)
        {
            CriticalCore(messageFunc);
        }

        public void Alert(Func<string> messageFunc, Exception ex)
        {
            CriticalCore(messageFunc, ex);
        }

        public void Alert(Func<string> messageFunc, string methodName)
        {
            CriticalCore(messageFunc, null, methodName);
        }

        public void Alert(Func<string> messageFunc, Exception ex, string methodName)
        {
            CriticalCore(messageFunc, ex, methodName);
        }

        [Obsolete]
        public void Alert(string message, string className, string methodName)
        {
            CriticalCore(message, null, methodName);
        }

        [Obsolete]
        public void Alert(string message, Exception ex, string className, string methodName)
        {
            CriticalCore(message, ex, methodName);
        }

        [Obsolete]
        public void Alert(Func<string> messageFunc, Exception ex, string className, string methodName)
        {
            CriticalCore(messageFunc, ex, methodName);
        }

        [Obsolete]
        public void Alert(Func<string> messageFunc, string className, string methodName)
        {
            CriticalCore(messageFunc, null, methodName);
        }



        /// <summary>
        /// Use to guard Emergency() logging where there is cost in building the log message
        /// object before Emergency() is called.
        /// </summary>
        /// <returns>return true if there is a destination to accept the Emergency() message.</returns>
        public bool IsEmergencyLoggable()
        {
            return _logger.IsFatalEnabled;
        }

        public void Emergency(string message)
        {
            CriticalCore(message);
        }

        public void Emergency(string message, Exception ex)
        {
            CriticalCore(message, ex);
        }

        public void Emergency(string message, string methodName)
        {
            CriticalCore(message);
        }

        public void Emergency(string message, Exception ex, string methodName)
        {
            CriticalCore(message, ex, methodName);
        }

        public void EmergencyWithFormat(string format, params object[] parameters)
        {
            CriticalCore(string.Format(format, parameters));
        }

        public void Emergency(Func<string> messageFunc)
        {
            CriticalCore(messageFunc);
        }

        public void Emergency(Func<string> messageFunc, Exception ex)
        {
            CriticalCore(messageFunc, ex);
        }

        public void Emergency(Func<string> messageFunc, string methodName)
        {
            CriticalCore(messageFunc, null, methodName);
        }

        public void Emergency(Func<string> messageFunc, Exception ex, string methodName)
        {
            CriticalCore(messageFunc, ex, methodName);
        }

        [Obsolete]
        public void Emergency(string message, string className, string methodName)
        {
            Critical(message, className, methodName);
        }

        [Obsolete]
        public void Emergency(string message, Exception ex, string className, string methodName)
        {
            Critical(message, ex, className, methodName);
        }

        [Obsolete]
        public void Emergency(Func<string> messageFunc, Exception ex, string className, string methodName)
        {
            CriticalCore(messageFunc, ex, methodName);
        }

        [Obsolete]
        public void Emergency(Func<string> messageFunc, string className, string methodName)
        {
            CriticalCore(messageFunc, null, methodName);
        }


        public static void Flush()
        {
            foreach (var asyncForwardAppender in LogManager.GetRepository().GetAppenders().OfType<AsyncForwardingAppender>())
            {
                asyncForwardAppender.Flush();
            }
        }
    }
}
