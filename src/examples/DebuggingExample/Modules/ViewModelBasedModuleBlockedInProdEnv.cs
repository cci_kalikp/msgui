﻿using MSDesktop.ViewModelAPI;
using MSDesktop.ViewModelAPI.Entitlements; 
using Microsoft.Practices.Unity; 

namespace DebuggingExample
{
     [EntitlementResource(Dev = "Dev_ViewModelBasedModuleBlockedInProdEnv",
        Qa = EntitlementResourceAttribute.NoEclipsePermsRequired,
        Uat = EntitlementResourceAttribute.NoEclipsePermsRequired,
        Prod = "Prod_ViewModelBasedModuleBlockedInProdEnv")]
    public class ViewModelBasedModuleBlockedInProdEnv : ViewModelBasedModule
    {
         public ViewModelBasedModuleBlockedInProdEnv(IUnityContainer unityContainer_)
             : base(unityContainer_)
        {
             
        }
         public override void Initialize()
         {
              
         }
    }
}
