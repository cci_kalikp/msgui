﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace DebuggingExample
{
    public class IsolatedModuleSingle : IModule
    {
        private readonly IChromeManager chromeManager;
        private readonly IChromeRegistry chromeRegistry;

        public IsolatedModuleSingle(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_)
        {
            chromeManager = chromeManager_;
            chromeRegistry = chromeRegistry_;
        }

        public void Initialize()
        {
             
        }
    }
}
