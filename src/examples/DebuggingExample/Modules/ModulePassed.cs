﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace DebuggingExample
{
 
    public class ModulePassed : IModule
    {
        private readonly IChromeManager chromeManager;
        private readonly IChromeRegistry chromeRegistry;   
        public ModulePassed(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_)
        {
            this.chromeManager = chromeManager_;
            this.chromeRegistry = chromeRegistry_;
        }

        public void Initialize()
        {
            chromeManager.Ribbon["Help"]["Support"].AddWidget("MSGui Colors", new InitialShowWindowButtonParameters()
                {
                    WindowFactoryID = ThemeExtensions.MSGuiColorPaletteViewId,
                    Text = "MSGui Colors",
                    InitialParameters = new InitialWindowParameters()
                        {
                            InitialLocation = InitialLocation.FloatingOnly, 
                        }
                }); 
             
        }

        
    }
}
