﻿using System;
using System.Collections.Generic;
using MSDesktop.Isolation;
using MSDesktop.Isolation.Interfaces;

namespace DebuggingExample
{
    public class IsolatedModulesCollection : ISubsystemModuleCollection
    {
        public IEnumerable<Type> GetModules()
        {
            yield return typeof (IsolatedModuleCollection1);
            yield return typeof (IsolatedModuleCollection2);
        }
    }
}
