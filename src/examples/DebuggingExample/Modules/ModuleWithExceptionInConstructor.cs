﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;

namespace DebuggingExample
{
    public class ModuleWithExceptionInConstructor:IModule
    {
        public ModuleWithExceptionInConstructor()
        {
            ExceptionThrower.ThrowException(); 
        }

        public void Initialize()
        {
             
        }
    }
}
