﻿using MSDesktop.ViewModelAPI;
using MSDesktop.ViewModelAPI.Entitlements;
using Microsoft.Practices.Unity; 

namespace DebuggingExample
{
     [EntitlementResource(Dev = "Dev_ModulePassedEntitlementInProdEnv",
        Qa = EntitlementResourceAttribute.NoEclipsePermsRequired,
        Uat = EntitlementResourceAttribute.NoEclipsePermsRequired,
        Prod = "Prod_ViewModelBasedModulePassedInProdEnv")]
    public class ViewModelBasedModulePassedInProdEnv : ViewModelBasedModule
    {
         public ViewModelBasedModulePassedInProdEnv(IUnityContainer unityContainer_)
             : base(unityContainer_)
        {
             
        }
         public override void Initialize()
         {
              
         }
    }
}
