﻿using MSDesktop.ViewModelAPI;
using MSDesktop.ViewModelAPI.Entitlements; 
using Microsoft.Practices.Unity;

namespace DebuggingExample
{
     [EntitlementResource(Dev = "ViewModelBasedModuleAlwaysPassedInProdEnv",
        Qa = EntitlementResourceAttribute.NoEclipsePermsRequired,
        Uat = EntitlementResourceAttribute.NoEclipsePermsRequired,
        Prod = EntitlementResourceAttribute.NoEclipsePermsRequired)]
    public class ViewModelBasedModuleAlwaysPassedInProdEnv: ViewModelBasedModule
    {
         public ViewModelBasedModuleAlwaysPassedInProdEnv(IUnityContainer unityContainer_)
             : base(unityContainer_)
        {
             
        }
         public override void Initialize()
         {
              
         }
    }
}
