﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace DebuggingExample
{
    public class IsolatedModuleCollection1 : IModule
    {
        private readonly IChromeManager chromeManager;
        private readonly IChromeRegistry chromeRegistry;

        public IsolatedModuleCollection1(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_)
        {
            chromeManager = chromeManager_;
            chromeRegistry = chromeRegistry_;
        }

        public void Initialize()
        {
            chromeRegistry.RegisterWindowFactory("IsolatedModuleCollection1", (container_, state_) =>
                {
                    container_.Content = new TextBox() { Text = "IsolatedModuleCollection1" };
                return true;
            });

            var fakeQatArea = new WidgetViewContainer(ChromeArea.QAT);
            var initialParameters = new InitialShowWindowButtonParameters
            {
                WindowFactoryID = "IsolatedModuleCollection1",
                Text = "I'm from isolated subsystem"
            };
            chromeManager.AddWidget("IsolatedModuleCollection1", initialParameters, fakeQatArea); 
        }
    }
}
