﻿using MSDesktop.ViewModelAPI;
using MSDesktop.ViewModelAPI.Entitlements; 
using Microsoft.Practices.Unity; 

namespace DebuggingExample
{
     [EntitlementResource(Dev = "ViewModelBasedModuleAlwaysBlockedInProdEnv",
        Qa = EntitlementResourceAttribute.NoEclipsePermsRequired,
        Uat = EntitlementResourceAttribute.NoEclipsePermsRequired,
        Prod = EntitlementResourceAttribute.NotAllowed)]
    public class ViewModelBasedModuleAlwaysBlockedInProdEnv : ViewModelBasedModule
    {
         public ViewModelBasedModuleAlwaysBlockedInProdEnv(IUnityContainer unityContainer_)
             : base(unityContainer_)
        {
             
        }
         public override void Initialize()
         {
              
         }
    }
}
