﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace DebuggingExample
{
    public class IsolatedModuleCollection2 : IModule
    {
        private readonly IChromeManager chromeManager;
        private readonly IChromeRegistry chromeRegistry;

        public IsolatedModuleCollection2(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_)
        {
            chromeManager = chromeManager_;
            chromeRegistry = chromeRegistry_;
        }

        public void Initialize()
        {
            chromeRegistry.RegisterWindowFactory("IsolatedModuleCollection2", (container_, state_) =>
            {
                container_.Content = new TextBox() { Text = "IsolatedModuleCollection2" };
                return true;
            });


            var statusBarArea = new WidgetViewContainer(ChromeArea.StatusBar);
            var initialParameters = new InitialShowWindowButtonParameters
            {
                WindowFactoryID = "IsolatedModuleCollection2",
                Text = "I'm also from isolated subsystem"
            };
            chromeManager.AddWidget("IsolatedModuleCollection2", initialParameters, statusBarArea);  
        }
    }
}
