﻿#region cw page

//cw Debug_Module: 10

/**
 * ## Description
 */
//cw Debug_Module: 11 
/**DebugViewModule can be added to support viewing all currently loaded assembly and sending message to be logged. */
//cw Debug_Module: 12
/**LogViewModule can be added support viewing log files both in the old MSLog format and the log4net format. */
//cw Debug_Module: 15
/**
 * ## Assembly
 * 
 * You need to add a reference to the MSDesktop.Debugging assembly.
 */
//cw Debug_Module: 20
/**
 * ## Namespace
 */

//cw Debug_Module: 30
/**
 * ## Context 
 */

#endregion
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows;
using DebuggingExample.ModuleWrapper;
using MSDesktop.Environment;
using MSDesktop.ModuleEntitlements;
using MSDesktop.ViewModelAPI.Entitlements;
using MSDesktop.Workspaces;
using MorganStanley.Desktop.Entitlement;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using log4net;
using log4net.Repository;
using log4net.Repository.Hierarchy;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Themes;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDotNet.My;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
//cw Debug_Module: 21
//{
using MSDesktop.Debugging; 
//} 

namespace DebuggingExample
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
 
        //cw Debug_Module: 31
        /**put the code in the following method */
        //{
        protected override void OnStartup(StartupEventArgs e)
        {

            base.OnStartup(e);
            var boot = new Framework(); 
             //}
            boot.AddBlueTheme();
            boot.EnableExceptionHandler("msguiadministrators@morganstanley.com"); //Change the email address

            boot.SetExceptionDialogBehaviour(copyButtonVisible: true /* visibility of the 'copy' button */,
                                             emailButtonCloses: true /* should the 'email' button also close the dialog */,
                                             suppressDialog: false /* should the dialog be replaced with an automatic email send */,
                                             attachScreenshot: true /*attach the screenshot*/,
                                             attachLog: true /*attach the log*/,
                                             attachFullLog: true, /*attach full log, if enabled, should reference \\san01b\DevAppsGML\dist\msdotnet\PROJ\sharpziplib\0.86.0\assemblies\ICSharpCode.SharpZipLib.dll and \\san01b\DevAppsGML\dist\deskapps\PROJ\attachlink-api\1.4.1\assemblies\DeskApps.attachlink-api.dll in the project */
                                             exceptionMessage: "Custom exception text" /* the custom exception text  */);
 
            boot.ConfigMailAttachmentService(@"U:\tmp", 10 * 1024 * 1024, 50 * 1024 * 1024);  


            //cw Debug_Module: 40
            /**
             * ## Code
             *Enable the DebugViewModule and LogViewModule
             */
            //{
            boot.EnableDebugging();
            //}

            boot.SetImcCommunicationStatus(CommunicationChannelStatus.Optional);

            //configure the mslog for testing the log sending using
            //you should be able to locate the log files under the output path of this example
            //that is, "..\..\..\build\common\ms\csc\Debug\examples\DebuggingExample\" in this case
            //MSLogConfig.Init("MSLogConfig.xml"); 
            //uncomment this line and comment out MSLogConfig.Init("MSLogConfig.xml") to test the new log4net log 

            /**Specify the initial directory for the log viewer, if not specified, it would use the directory of the latest log file found for the current application*/
            //{
            boot.SetInitialLogFilePath(InitializeLogging());
            //}
            boot.EnableConcord();
            //boot.EnableReflectionModuleLoad(); 
            boot.SkipSaveProfileOnExitWhen(SkipSaveProfileOnExitCondition.ModuleTypeResolutionFailed | SkipSaveProfileOnExitCondition.ModuleLoadExceptioned | SkipSaveProfileOnExitCondition.ModuleBlockedByEntitlement);
            boot.EnableWorkspaceSupport();
            var service = new OfflineEntitlementService();
            service.AddAuthorized(new ModuleResource { Name = "ModulePassed" }, new[] { ModuleEntitlementsStrings.Actions.Load });
            service.AddAuthorized(new ModuleResource { Name = "ViewModelBasedModuleAlwaysBlockedInProdEnv" }, new string[] { ModuleEntitlementsStrings.Actions.Load });
            service.AddAuthorized(new ModuleResource { Name = "Dev_ViewModelBasedModuleBlockedInProdEnv" }, new string[] { ModuleEntitlementsStrings.Actions.Load });
            service.AddAuthorized(new ModuleResource { Name = "Prod_ViewModelBasedModulePassedInProdEnv" }, new string[] { ModuleEntitlementsStrings.Actions.Load });
            boot.EnableModuleEntitlements(service); 
            boot.EnableViewModelApiEntitlements(); 
            boot.EnableEnvironment(EnvironmentExtensions.EnvironProviderId.CommandLine,
    EnvironmentExtensions.RegionProviderId.None,
    EnvironmentExtensions.UsernameProviderId.None);


            boot.AddViewModelBasedModule<ViewModelBasedModuleAlwaysBlockedInProdEnv>();
            boot.AddViewModelBasedModule<ViewModelBasedModuleAlwaysPassedInProdEnv>();
            boot.AddViewModelBasedModule<ViewModelBasedModuleBlockedInProdEnv>();
            boot.AddViewModelBasedModule<ViewModelBasedModulePassedInProdEnv>();

            boot.AddModuleWithEntitlementCheck<ModulePassed>();
            boot.AddModuleWithEntitlementCheck<ModuleBlocked>();
            boot.AddModule<ModuleWithExceptionInInitialize>();
            boot.AddModule<ModuleWithExceptionInConstructor>();

            boot.AddModule<MyModuleRepository>();
            //cw Provide_Module_Info_When_Using_Module_Wrapper: 32 
            //{
            boot.AddModule<MyModuleWrapper<MyModule>>();
            boot.AddModule<MyModuleWrapper<MyModule2>>();
            //}

            //cw Provide_Module_Info_When_Using_Module_Wrapper: 43
            //{
            boot.AddModule<YourModuleWrapper>();
            boot.AddModule<YourModuleWrapper2>();
            //}
            try
            {
                //cw Debug_Module :32 
                //{
                //put your code here 
                boot.Start();
                //}
            }
            finally
            {
                boot.SetUpProcessIsolatedWrapper<IsolatedModuleSingle>(IsolatedProcessPlatform.X86);
                boot.SetUpProcessIsolatedWrapper<IsolatedModuleSingleWithException>(IsolatedProcessPlatform.X86);
                boot.SetUpProcessIsolatedWrapper<IsolatedModulesCollection>(IsolatedProcessPlatform.X86);
                //cw Provide_Module_Info_When_Using_Module_Wrapper: 44
                //{
                boot.SetUpProcessIsolatedWrapper<YourModuleWrapper3>(IsolatedProcessPlatform.X86);
                //}
            } 

            //cw Debug_Module :33 
            //{
        }
        //}

        //cw Debug_Module: 50
        /**
         * ##Buttons
         */
        /** Button ID for Assembly Viewer: "DebugViewerButton" */
        /** Button ID for Log Viewer: "LogViewerButton" */
        /** If EnableCentralizedPlacement is called upon Framework, in the ribbon definition file, developer could add widget using these ids and they would be automatically placed to the location specified */

        //cw Debug_Module: 60
        /**
         * ##Screenshots
         */
        /**Assembly Viewer - Assembly Tab*/
        /**![Assembly Tab](images/Debugging/AssemblyTab.png)*/
        /**Assembly Viewer - Module Tab*/
        /**![Module Tab](images/Debugging/ModuleTab.png)*/
        /**Log Viewer*/
        /**![Log Viewer](images/Debugging/LogViewer.png)*/


        

        private string InitializeLogging()
        { 
            string logFile = "NewLog" + Environment.TickCount + ".log"; 
            if (File.Exists(logFile))
            {
                File.Delete(logFile);
            }
            MSLoggerFactory.Provider = new MsLog4NetLoggerProvider(logFile, new[] { new KeyValuePair<string, string>("Example Version", "1.0.0.0") });
            //since MSLog doesn't support configure the new MsLog4NetMsLogDestination through configuration file
            //we write the code here to mimic the setting in MSLogConfig.xml, except that the destination are to the same MsLog4NetMsLogDestination
            var dest = new MsLog4NetMsLogDestination();

            MSLog.Mode = MSLog.MSLogMode.Sync; 
            MSLog.AddDestinationToTarget(MSLogTarget.Root, dest);
            MSLog.SetThresholdPolicy(MSLogTarget.Root, "*/*/*:All");
            var target = new MSLogTarget("root");
            MSLog.AddDestinationToTarget(target, dest);
            MSLog.SetThresholdPolicy(target, "*/*/*:All");
            target = new MSLogTarget("root/msdotnet");
            MSLog.AddDestinationToTarget(target, dest);
            MSLog.SetThresholdPolicy(target, "msdotnet/*/*:All");
            target = new MSLogTarget("root/msdotnet/msgui");
            MSLog.AddDestinationToTarget(target, dest);
            MSLog.SetThresholdPolicy(target, "msdotnet/msgui/*:Notice,msdotnet/*/*:Critical,*/*/*:Emergency");
            target = new MSLogTarget("root/msdotnet/dnparts");
            MSLog.AddDestinationToTarget(target, dest);
            MSLog.SetThresholdPolicy(target, "*/*/*:All"); 
            MSLog.Start();

            var repository = (Hierarchy)LogManager.GetRepository();

            if (repository != null)
            {
                SetLoggerLevel(repository, repository.Root, "INFO");
            }
            return logFile;
        }

        private static void SetLoggerLevel(ILoggerRepository repository, Logger logger, string level)
        {
            if (repository == null || logger == null || string.IsNullOrEmpty(level))
            {
                return;
            }

            switch (level.ToLower())
            {
                case "warning":
                    logger.Level = repository.LevelMap["warn"];
                    break;
                case "none":
                    logger.Level = repository.LevelMap["off"];
                    break;
                case "clear":
                    logger.Level = null;
                    break;
                default:
                    logger.Level = repository.LevelMap[level];
                    break;
            }
        }
    }
}
