﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSLog;
using System.Collections.Concurrent;
using MorganStanley.MSDotNet.My;

namespace DebuggingExample
{
    public class MsLog4NetMsLogDestination : MSLogDestination
    {
        private readonly ConcurrentDictionary<string, IMSLogger> _loggers = new ConcurrentDictionary<string, IMSLogger>();

        /// <summary>
        /// a flag representing the state of the destination.
        /// </summary>
        private bool _started;

        /// <summary>
        /// a lock for starting / stopping the destination.
        /// </summary>
        private readonly object _startedLock;

        public MsLog4NetMsLogDestination()
            : base("log4net")
        {
            _startedLock = new object();
        }

        public static void Redirectlog4NetDestination()
        {
            MSLog.Mode = MSLog.MSLogMode.Sync;
            MSLog.AddDestinationToTarget(MSLogTarget.Root, new MsLog4NetMsLogDestination());
            MSLog.Start();
        }

        /// <summary>
        /// Starts the destination
        /// </summary>
        public override void Start()
        {
            lock (_startedLock)
            {
                _started = true;
            }
        }

        /// <summary>
        /// Stops the destination
        /// </summary>
        public override void Stop()
        {
            lock (_startedLock)
            {
                _started = false;
            }
        }

        /// <summary>
        /// Gets a value representing whether the destination is started or not
        /// </summary>
        public override bool Started
        {
            get
            {
                lock (_startedLock)
                {
                    return _started;
                }
            }
        }

        /// <summary>
        /// Sends an MSLogMessage to the destination
        /// </summary>
        /// <param name="message">The message to send</param>
        public override void Send(MSLogMessage message)
        {
            var name = message.Layer.Name + "." + message.ClassName;

            var logger = _loggers.GetOrAdd(name, key => MSLoggerFactory.Provider.CreateLogger(name));

            switch (message.Priority)
            {
                case (MSLogPriority.Debug):
                    logger.Debug(message.Message, name, message.Method);
                    break;
                case (MSLogPriority.Alert):
                    logger.Alert(message.Message, name, message.Method);
                    break;
                case (MSLogPriority.Critical):
                    logger.Critical(message.Message, name, message.Method);
                    break;
                case (MSLogPriority.Emergency):
                    logger.Emergency(message.Message, name, message.Method);
                    break;
                case (MSLogPriority.Error):
                    logger.Error(message.Message, name, message.Method);
                    break;
                case (MSLogPriority.Info):
                    logger.Info(message.Message, name, message.Method);
                    break;
                case (MSLogPriority.Notice):
                    logger.Notice(message.Message, name, message.Method);
                    break;
                case (MSLogPriority.Warning):
                    logger.Warning(message.Message, name, message.Method);
                    break;
            }
        }
    }

}
