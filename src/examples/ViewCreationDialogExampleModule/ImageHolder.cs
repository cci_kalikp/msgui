﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Image = System.Windows.Controls.Image;

namespace MorganStanley.MSDotNet.MSGui
{
  class ImageHolder: UserControl
  {
    public ImageHolder(Bitmap image_)
    {
      MinWidth = 200;
      MinHeight = 200;      
      // Winforms Image we want to get the WPF Image from...
      BitmapImage bitmap = new BitmapImage();
      bitmap.BeginInit();
      MemoryStream memoryStream = new MemoryStream();
      // Save to a memory stream...
      image_.Save(memoryStream, ImageFormat.Bmp);
      // Rewind the stream...
      memoryStream.Seek(0, SeekOrigin.Begin);
      bitmap.StreamSource = memoryStream;

      bitmap.EndInit();

      var img = new Image();
      img.Stretch = Stretch.Uniform;
      img.Source = bitmap;
      this.Content = img;
    }

    
  }
}
