﻿using System;
using System.Windows.Controls;
using Microsoft.Practices.Composite.Modularity;

namespace MorganStanley.MSDotNet.MSGui
{
  public class ExampleModule : IModule
  {
    private ExtendedViewManager m_evm;
    public ExampleModule(ExtendedViewManager evm_)
    {
      m_evm = evm_;
    }
    public void Initialize()
    {
      m_evm.AddViewCreator("Market Position",
                           view_ =>
                             {
                               view_.Content = new ImageHolder(Resources.fake_trae);
                               view_.Title = "Market Position";
                             },
                           new ViewCreationToken()
                             {
                               Name = "Market Position",
                               Description = "View a set of trades graphs and corresponding volatility.",
                               Group = "Trading",
                               Image = Resources.fake_trae
                             });


      m_evm.AddViewCreator("Arbitrage",
                           view_ =>
                           {
                             view_.Content = new ImageHolder(Resources.arb);
                             view_.Title = "Arbitrage";
                           },
                           new ViewCreationToken()
                           {
                             Name = "Arbitrage",
                             Description = "The matrix representing the calendar spread arbitrage tests.",
                             Group = "Trading",
                             Image = Resources.arb
                           });
      m_evm.AddViewCreator("Trade Table",
                           view_ =>
                           {
                             view_.Content = new ImageHolder(Resources.grid_view);
                             view_.Title = "Trade Table";
                           },
                           new ViewCreationToken()
                           {
                             Name = "Trade Table",
                             Description = "Grid displays the current trade set and allows pivoting of positions.",
                             Group = "Trading",
                             Image = Resources.grid_view
                           });
      m_evm.AddViewCreator("Trade Notify",
                           view_ =>
                           {
                             view_.Content = new ImageHolder(Resources.alert_bar);
                             view_.Title = "Market Position";
                           },
                           new ViewCreationToken()
                           {
                             Name = "Trade Notify",
                             Description = "Notifications of new trade information",
                             Group = "Trading",
                             Image = Resources.alert_bar
                           });
      m_evm.AddViewCreator("Computation Graph",
                           view_ =>
                           {
                             view_.Content = new ImageHolder(Resources.graph);
                             view_.Title = "Computation Graph";
                           },
                           new ViewCreationToken()
                           {
                             Name = "Computation Graph",
                             Description = "Visualize the computation graph.",
                             Group = "Visualisation",
                             Image = Resources.graph
                           });

    }
  }
}
