﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace CommunicationExamples.StarterExecuter
{
    class Program
    {
        static void Main(string[] args_)
        {

            ProcessStartInfo startInfo = new ProcessStartInfo(@"CommunicationExamples.Starter.exe")
            {
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false, 
                WorkingDirectory = Environment.CurrentDirectory,  
                Arguments = string.Join(" ", args_)
            };
            var process = Process.Start(startInfo);
            process.WaitForExit(6000);
        }
    }
}
