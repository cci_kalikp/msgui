package com.ms.msdotnet.msdesktop.examples;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import com.ms.msdotnet.msdesktop.launchermessageapi.DispatchableLauncherMessage;

public class LauncherMessageExample {

    /**
     * @param args
     */
    public static void main(String[] args) {
        HashMap<String, Object> message = new HashMap<String, Object>();
        message.put("StockName", "MS"); 
        message.put("Description", "Ada");
        message.put("Price", 999.99); 
        message.put("OrderType", OrderType.Buy);
        message.put("Date", new Date(114,0,1,10,9,8));
        List<Point2D> points = new LinkedList<Point2D>();
        points.add(new Point2D.Double(1.1, 2.2));
        points.add(new Point2D.Double(3.3, 4.4));
        message.put("Points", points);

		DispatchableLauncherMessage.addSerializer(Point2D.class, new Point2DSerializer());
        
		try {
            String url = DispatchableLauncherMessage.generateLink("SendingMessage", message, "msdesktop.communicationexmples.prod");
			System.out.println("url generated:");
            System.out.println(url);
			
			StringBuilder longMessage = new StringBuilder("This is a super l"); 
			for(int i = 0; i < 3000; i++)
			{  
			   longMessage.append("o");
			}
			longMessage.append(" message");
			message.put("Description",  longMessage.toString());
			url = DispatchableLauncherMessage.generateLink("SendingMessage", message, "msdesktop.communicationexmples.prod");
			System.out.println("another url generated:");
            System.out.println(url);
        } 
		catch (JsonProcessingException e) {
            throw new RuntimeException( e );
        }
		catch (IOException e) {
            throw new RuntimeException( e );
        }
    }

}
