package com.ms.msdotnet.msdesktop.launchermessageapi.serialization;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class IntValueEnumSerializer extends JsonSerializer<IntValueEnum> {

    @Override
    public void serialize(IntValueEnum value, JsonGenerator jgen, SerializerProvider provider) throws IOException,
            JsonProcessingException {
        jgen.writeNumber(value.getValue());
    }

}
