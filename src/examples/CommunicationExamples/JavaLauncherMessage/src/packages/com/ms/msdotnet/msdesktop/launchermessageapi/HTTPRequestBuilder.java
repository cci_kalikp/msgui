package com.ms.msdotnet.msdesktop.launchermessageapi;

import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPOutputStream; 

public class HTTPRequestBuilder {
 
    String SessionIdParameter = "id"; 
    String MessageParameter = "data"; 
    String HashParameter = "hash";
	String ZippedParameter = "zip";
    int CompressThreshold = 1500;
	 
    private String host;
    private int port;
    private boolean enableHash;
    private String hashSeed;
    
    public HTTPRequestBuilder(String host, int port, boolean enableHash, String hashSeed) {
        super();
        this.host = host;
        this.port = port;
        this.enableHash = enableHash;
        this.hashSeed = hashSeed;
    }
    
    public String getBaseUrl()
    {
        return "http://" + host + ":" + port + "/";
    }

    public String buildPublishUrl(String message_, String publisherId_) throws IOException
    {
        StringBuilder url = new StringBuilder(getBaseUrl());
        url.append("pub.json");
        url.append("?");  
	    boolean zipped = false;
        if (message_.length() > CompressThreshold) //needs to be zipped
        {
            message_ = compress(message_);
            zipped = true;
        }
        url.append(MessageParameter).append("=").append(URLEncoder.encode(message_));
        if (zipped)
        { 
			url.append("&").append(ZippedParameter).append("=1");
        } 
        url.append("&").append(SessionIdParameter).append("=").append(publisherId_);
        return protectUrl(url.toString());
    }
    
    public String protectUrl(String url_)
    {
        String hashResult = SHA256Hash(url_);
        if (url_.contains("?"))
        {
            url_ += "&" + HashParameter + "=" + hashResult;
        }
        else
        {
            url_ += "?" + HashParameter + "=" + hashResult;
        }
        return url_;
    }
    
    private String SHA256Hash(String plainText) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");

            md.update((hashSeed + plainText).getBytes());
            byte[] hashedBytes = md.digest();

            StringBuilder hashString = new StringBuilder();
            for (byte hashedByte : hashedBytes) {
                hashString.append(Integer.toString((hashedByte & 0xff) + 0x100, 16).substring(1));
            }
            return hashString.toString();
        } catch (NoSuchAlgorithmException e) { /* ignore */
            return null;
        }
    } 
	
	
	private static String compress(String str) throws IOException {
        if (str == null || str.length() == 0) {
            return str;
        } 
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip = new GZIPOutputStream(out);
        gzip.write(str.getBytes());
        gzip.close();  
		
		byte[] data = out.toByteArray();  
		char[] outChars = Base64Coder.encode(data);
 
        return String.valueOf(outChars);
     }

}
