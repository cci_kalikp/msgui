package com.ms.msdotnet.msdesktop.launchermessageapi;


import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.ms.msdotnet.msdesktop.launchermessageapi.serialization.IntValueEnum;
import com.ms.msdotnet.msdesktop.launchermessageapi.serialization.IntValueEnumSerializer; 

public class LauncherMessage {

    private String type;
    private String content;
    private int format;
    
    public LauncherMessage(String type, Map<String, Object> dictionary, SimpleModule module) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper(); 
        mapper.registerModule(module);
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ"));
        
        this.format = 1; //json
        this.type = type;
        this.content = mapper.writeValueAsString(dictionary);
    } 
	
    @JsonProperty("Type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("Content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @JsonProperty("Format")
    public int getFormat() {
        return format;
    }

    public void setFormat(int format) {
        this.format = format;
    }
}
