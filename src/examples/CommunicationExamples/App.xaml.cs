﻿#region Launcher Codewiki
//cw Launcher_Introduction: 10
/**##Description*/
/**MSDesktop provide a more powerful [Launcher](http://wiki.ms.com/IEDLauncher/WebHome).*/
/**It can be turned on in main menu of IED Launcher by clicking "Switch to cross machine and restart" or by starting it with a "/f" argument*/
/**IED Launcher can be turned back using the main menu of MSDesktop Launcher by clicking "Switch to single machine and restart". */
/**It not only supports all existing functions of IED Launcher, but also */
/** <ul>
 *    <li> Support launching and sending message to an application from email link
 *    <li> Provide another modern look UI using MSDesktop LauncherBar 
 *  </ul>
 */

//cw Launcher_Deployment: 10
/**##Specify the HttpServerPort*/
/**Launcher need to run an embedded http server to accept http request invoked from email link, so an http port is needed, it can be configured in the application config of Launcher as following */
/* 
//{  
  <appSettings>
    <add key="HTTPServerPort" value="8080"/>
  </appSettings>
 //}
*/
/** This http server port should then be used when generating the url link embedded in email*/

/**##Config the CPS Server*/
/**To enable launchers running on different machines under the same user to communicate with each other, we need to configure the [cps server](http://codewiki.webfarm.ms.com/msdotnet/#!/MSDesktop/Communications/IPC_Communication/How_to_Setup_a_CPS_Server_for_IPC/) for imc communication, after the cps server is started, CPSConfig.xml should be configured with the correct cps server address and if kerberos is enabled or not.*/
/* 
//{  
<?xml version="1.0" encoding="utf-8" ?>
<ServiceConfig>
   <System>
    <Name>msdesktop/Communication</Name>
     <Service>
      <Name>CPS</Name>
      <address>ms.tcp://icompile1.croydon.ms.com:9992</address>
       <Kerberos>true</Kerberos>
     </Service>
  </System>
</ServiceConfig>
 //}
*/
/**If not configured, there would be no cross launcher communication*/

//cw Launcher_Cross_Launcher_Message: 10
/**##Description */
/**Launcher now could listen to any http request and delegate it to the appropriate application, and if cps is configured correctly, not only the application running on the current machine might be the candidate for the message receiver, application running on other machines with the same user account might also be used for the message receiver.*/
/**##Message sending workflow */
/**
 <ul>
   <li>User receives an email with a link in it which specified the message content, application, whether new application can be started, whether the route can be saved for later use</li>
   <li>User clicks the link, Launcher running on the local machine detected this http request</li>
   <li>If the application specified is already running, and this is the only application running(which means this application is not running on other machine with user signed in), message would be send to that application</li>
   <li>If there are multiple instance of applications running on different machine, and user has not persisted the choice previously, he will be prompt to pick up one, and the message would be send to the chosen application, and his choice would be persisted if route saving is enabled in the http request</li>
   <li>If there are multiple instance of applications running on different machine, and previous choice has been saved, the application chosen previously would be used</li>
   <li>If there is no application specified running, and we allow starting new application. If Launcher is only running on the local machine, that application would be started and sent message to</li>
   <li>If there is no application specified running, and we allow starting new application. If launchers are running on other machines user signed in as well, user would be prompt to pick up the machine he want the application to run, application on that machine would be started and sent message to, and his choice would be persisted if route saving is enabled in the http request</li>
 </ul>
 */
/**##Url generation*/
/**###Message content */
/** We need to use an existing or create a message type which contains the message content received by the application.*/
/** To shorten the url, we could use "MessageAttribute" to specify the message id of it; Then when generating url we could specify the message type using the short message id instead of the full message type name**/
//cw Launcher_Cross_Launcher_Message: 30
/**###Specify application*/
/**A unique id should be used to set the target application when sending message, the recommended way is to specify an "iconId" explicitly in LauncherConfig.xml as following, or else, the unique id is calculated by {meta}.{project}.{env group}.{env name} */

//cw Launcher_Cross_Launcher_Message: 40
/**##Decide message sending behavior*/
/**
 * <ul>
 *   <li>If the application we want to send message to is not opened, can it be started automatically? Default is true.</li>
 *   <li>If user need to make some choice of application running on which machine to use, should this choice be reused next time? Default is true.</li>
 *   <li>Do we always open a new instance of application when sending the message, if true, route is never reused? Default is false.</li>
 * </ul>
 */

//cw Launcher_Cross_Launcher_Message: 50
/**##Generate the url using MSDesktop library*/
/**We provide 2 versions of overloads to generate the url, one is to accept a strong type, can be used when url generator know the message type; one is to accept a dictionary, used when the generator has no knowledge of the message type*/

//cw Launcher_Cross_Launcher_Message: 54
/**Note, the Configuration\LauncherConfig.config for this generator needs to be changed to use the same port as the http server as following*/

//cw Launcher_Cross_Launcher_Message: 60
/**##Send message directly to the target application*/


//cw Launcher_Cross_Launcher_Message: 65
/**##We also support simply start an application using email or directly */



//cw Launcher_Cross_Launcher_Message: 70
/**##Generate the url using Java API */
/**We also provide a Java package(\\san01b\DevAppsGML\dist\msdotnet\PROJ\msgui\[version]\lib\launcherMessageAPI.jar) and the source code is under JavaLauncherMessage folder of the example source code of this article) which can be referenced to generate the url */
/*
//{
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import com.ms.msdotnet.msdesktop.launchermessageapi.DispatchableLauncherMessage;
public class LauncherMessageExample {
 
   public static void main(String[] args) {
        HashMap<String, Object> message = new HashMap<String, Object>();
        message.put("StockName", "MS"); 
        message.put("Description", "Ada");
        message.put("Price", 999.99); 
        message.put("OrderType", OrderType.Buy);
        message.put("Date", new Date(114,0,1,10,9,8));
        List<Point2D> points = new LinkedList<Point2D>();
        points.add(new Point2D.Double(1.1, 2.2));
        points.add(new Point2D.Double(3.3, 4.4));
        message.put("Points", points);
       
        //register special serializer for custom type to match .net version
		DispatchableLauncherMessage.addSerializer(Point2D.class, new Point2DSerializer());
		try 
        {
            //use DispatchableLauncherMessage provided in launcherMessageAPI.jar to generate url
            String url = DispatchableLauncherMessage.generateLink("SendingMessage", message, "msdesktop.communicationexmples.prod"); 
        } 
		catch (JsonProcessingException e) {
            throw new RuntimeException( e );
        }
		catch (IOException e) {
            throw new RuntimeException( e );
        }
    } 
}

//special handling for enum type to match .net type
import com.ms.msdotnet.msdesktop.launchermessageapi.serialization.IntValueEnum;
public enum OrderType implements IntValueEnum {
    Buy(1), Sell(0);

    private int value;
    
    OrderType(int i) {
        this.value = i;
    }
    
    @Override
    public int getValue() {
        return value;
    } 
}
 
//custom serializer 
import java.awt.geom.Point2D;
import java.io.IOException; 
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
public class Point2DSerializer extends JsonSerializer<Point2D> {

    @Override
    public void serialize(Point2D value, JsonGenerator jgen, SerializerProvider provider) throws IOException,
            JsonProcessingException {
        jgen.writeString(value.getX() + "," + value.getY());
    } 
}
//}
 */
//cw Launcher_Cross_Launcher_Message: 71
/**Note, the LauncherConsts.HttpServerPort might needs to be changed to the correct number and recompile launcherMessageAPI*/

//cw Launcher_Cross_Launcher_Message: 80
/**##Receiver application change */
/** Firstly, the receiver application must be upgraded to use the latest MSDesktop and add a reference to \\san01b\DevAppsGML\dist\dotnet3rd\PROJ\jsondotnet\5.0.6\assemblies\Net40\Newtonsoft.Json.dll */
/** Secondly, other than the extra message type might needed, if the message is not handled using [M2M message subscriber][cw_ref ModuleToModuleSubscriber] of this message type (take, in this example, IModuleSubscriber<SendingMessage>), the receiver application needs to write a bridge to subscribe this message as following */
/*
//{
 var subscriber = communicator.GetSubscriber<SendingMessage>(); //communicator is an instance of ICommunicator
 subscriber.GetObservable().Subscribe(msg =>
 {
      //re-dispatch this message to an internal message bus
       //or call some local method to handle this message 
 });  
//}
*/

//cw Launcher_New_UI
/**##Overview */
/** The new ui can be turned on as following in the old ui
/**![Turn on Modern UI](images/Launcher/SwitchToNewUI.png)*/
/**All the applications are populated as a group box in the launcher bar as below, and it can be docked to the bottom of the screen for easy access. If there are too many applications, firstly icons would be grouped by environment group as drop down items, if there are still more the scrollbar at the right side of the launcher bar can be clicked to scroll the view*/
/**![Modern UI](images/Launcher/NewUIBar.png)*/
/**Right click on the button or the drop down icon would both bring the general context menu for that specific environment*/

/**##Filter By Environment*/
/**Other than the existing location filter, a new environment filter is supported in the new ui, which can be used to clean up the ui a bit, and this setting would be saved as user preference.*/
/**![Filter By Environment](images/Launcher/FilteredByEnvironment.png)*/
/**If the application is totally filtered out, it would appear in the "Applications filtered out" dropdown in the right side of the bar*/
/**![Quick access hidden applications](images/Launcher/HiddenApplications.png)*/
/**If some environments are filtered out for a specific application, user could access them quickly using "Ctrl+Right Click"*/
/**![Quick access hidden environments](images/Launcher/HiddenEnvironments.png)*/

/**##Show as favorite*/
/**A new menu item "Show as favorite on this machine" is added to the context menu of the an environment icon, which allows toggling the "favorite" setting of this environmnent icon*/
/**<ul>
 * <li>The favorite environment can be utilizing as a filter option, so that only favorite environments are visible when "Filter by favorites" is set in the application menu</li>
 * <li>It can also be utilizing as a virtual "Favorites" group, so that it would be added as the first group with favorite environments populated in it. It can be turned on with the "Favorite as filter" set to FALSE in the application menu</li>
 *</ul>
 */


/**##Delete saved route*/
/**Message routes to application saved previously can be deleted/viewed through the options window*/
/**![View saved routes](images/Launcher/DeleteSavedRoute.png)*/

//cw Launcher_New_LaunchConfig_Items:5
/**##Description*/
/**Some new xml elements are introduced to [LauncherConfig](http://wiki.ms.com/IEDLauncher/HOWTO), mostly for rendering the new ui better or for advanced features of cross launcher message */
/**##Environment Group properties*/
/**IconPath: to set an icon for the environment group, can be used when populating the environment drop down. If not set, the first environment icon under this group would be used*/
/**##Application startup time estimation*/
/**To avoid starting the application in duplicate, we introduced a delay for the next startup for the same application, the default is very short(4 sec), however, more precise configuration can be done at application and application release level as following, by providing "EstimatedStartupSeconds" */
/** Take for following example configuration, if the "EstimatedStartupSeconds" is missing for the active release, it would inherit the configuration on the application if there is any */
/*
 //{ 
<Application appID="CommunicationExamples" usingLDAP="true">
  <Meta>msdotnet</Meta>
  <Project>msdesktop</Project>
  <Description>Communication Example</Description>
  <EstimatedStartupSeconds>30</EstimatedStartupSeconds>
  <ActiveReleases>
    <ActiveRelease>
      <Version>PROD</Version>
      <Executable>%ExampleRoot%\CommunicationExamples\CommunicationExamples.exe</Executable>
      <FileSystem>DIRECT</FileSystem>
      <IconFile></IconFile>
      <EstimatedStartupSeconds>40</EstimatedStartupSeconds>
    </ActiveRelease>
    <ActiveRelease>
      <Version>PROD2</Version>
      <Executable>%ExampleRoot%\CommunicationExamples\CommunicationExamples.Starter.exe</Executable>
      <FileSystem>DIRECT</FileSystem>
      <IconFile></IconFile>
      <EstimatedStartupSeconds>50</EstimatedStartupSeconds>
    </ActiveRelease>
    <ActiveRelease>
      <Version>PROD2</Version>
      <Executable>%ExampleRoot%\CommunicationExamples\Start.cmd</Executable>
      <FileSystem>DIRECT</FileSystem>
      <IconFile></IconFile>
      <EstimatedStartupSeconds>50</EstimatedStartupSeconds>
    </ActiveRelease>
    <ActiveRelease>
      <Version>DEBUG</Version>
      <Executable>%ExampleRoot%\CommunicationExamples%ExeRelative%\CommunicationExamples.exe</Executable>
      <FileSystem>DIRECT</FileSystem>
      <IconFile></IconFile>
    </ActiveRelease>
  </ActiveReleases>
</Application>
 //}
 */

//cw Launcher_New_LaunchConfig_Items:10
/**##Cross Launcher Message properties*/
/**
 * <ul> 
 *   <li>MessageDispatchFailMessageOverride: used to override the default message output to the web page when failed to send message to the application</li>
 *   <li>MessageDispatchFailHtml: provide an extra web page link under the error message, when failed to send message to the application</li>
 *   <li>MessageDispatchSucceedMessageOverride: used to override the default message output to the web page, when send message to the application successfully</li>
 *   <li>MessageDispatchSucceedHtml: provide an extra web page link under the error message, when send message to the application successfully</li>
 * </ul>
 */

//cw Launcher_New_LaunchConfig_Items: 20
/**If failed to send message the output would like this*/
/**![Override fail message](images/Launcher/OverrideMessage.png)*/
#endregion

using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows;
using CommunicationExamples.ModuleToModule;
using CommunicationExamples.ViewToView;
using MSDesktop.WebSupport;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
//cw ViewToView: 33
//{
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
//}
using MorganStanley.MSDotNet.MSGui.Themes;

namespace CommunicationExamples
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        //cw ViewToViewConnection: 11
        //{
        protected override void OnStartup(StartupEventArgs e)
        //}
        {
            string id = null;
            foreach (var arg in e.Args)
            {
                if (arg.StartsWith("/id=") ||
                    arg.StartsWith("-id="))
                {
                    id = arg.Substring(4);
                    break;
                } 
            }

            base.OnStartup(e);
            var boot = new Framework();
            boot.AddSimpleTheme();
            boot.EnableExceptionHandler("msguiadministrators@ms.com");
            if (!string.IsNullOrEmpty(id))
            {
                boot.SetApplicationName(id); 
            }
             
            //boot.SetShellMode(ShellMode.RibbonAndFloatingWindows);

            boot.EnableCompactTabGroups(true);

           boot.SetupPersistenceStorage(new FilePersistenceStorage(string.IsNullOrEmpty(id) ? "CommunicationExamples": id, 
                Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Profile")));


            boot.EnableMessageRoutingCommunication();
            boot.EnableMessageRoutingDefaultGui();
            boot.EnableMessageRoutingHistory();
            boot.EnableMessageRoutingHistoryGui();
            boot.EnableMessageRoutingMonitor();
            boot.SetImcCommunicationStatus(CommunicationChannelStatus.Disabled);

            boot.AddModule<V2VPublisherModule>();
            boot.AddModule<V2VSubscriberModule>();
            boot.AddModule<V2VIsolatedSubscriberModule>();
            boot.AddModule<V2VIsolatedPublisherModule>();

            boot.EnableWebSupport();
            boot.AddModule<WebModule>();

            boot.AddModule<ManagingModule>();

            boot.AddModule<PublishingModule>();
            boot.AddModule<SubscribingModule>();
            boot.EnableWidgetUserConfig();

            boot.ShowSeparateHeaderItemsInFloatingWindow();

            //cw ViewToViewConnection: 15
            //{
            boot.AddModule<V2VConfigModule>();
            //}

            boot.EnableScreenGroupSupport();

            boot.Start();
        }
    }
}
