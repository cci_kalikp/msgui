﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;
//cw MessageAdapter :5
/**##namespace*/
/**IAdapterServicee*/
//{
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
//}

//cw MessageAdapter :5
/**##context*/
/**You may create a Managing Module to help setup the Message Adapter, e.g*/


namespace CommunicationExamples.ViewToView
{
   //{
    public class ManagingModule: IModule
    {
      // put the code in the Code section here..
  //}
        //cw MessageAdapter: 11
        //{
        private readonly IAdapterService _adapterService;
        //}

        //{
        public ManagingModule(IAdapterService adapterService)
        {
            _adapterService = adapterService;
        }
        //}

        //cw MessageAdapter: 21
        //{[csharp]
        public void Initialize() //Module initializer
        {
            _adapterService.AddAdapter<SendingMessage, ReceivingMessage>(
               (m) => new ReceivingMessage() { Stock = m.StockName, Message = m.Description, OrderType = m.OrderType, Price = m.Price, Points = m.Points, Date = m.Date });
        }
        //}
   //cw MessageAdapter :5
   //{
    }
  //}
}
