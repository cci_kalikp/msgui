﻿using System;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace CommunicationExamples.ViewToView
{
  //cw ViewToViewPublisher: 31
  //{[csharp]
    public partial class PublisherView : UserControl
    {
        private readonly IPublisher<SendingMessage> _publisher;
        
        public PublisherView(IPublisher<SendingMessage> publisher)
        {   
            _publisher = publisher;
            InitializeComponent();
        }
        
        //Invoked when a button is clicked
        private void SubmitNewPrice(object sender, RoutedEventArgs e)
        {
            var sp = new SendingMessage
                         {
                             StockName = cb_stockNames.SelectionBoxItem as String,
                             Description = tb_price.Text
                         };
            _publisher.Publish(sp);
        }
    }
    //}
}
