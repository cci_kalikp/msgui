﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
//cw ViewToViewPublisher: 11
/**##namepace*/
//{
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.ScreenGroup;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ScreenGroup;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

//}

using MorganStanley.MSDotNet.MSGui.Impl.ScreenGroup;
namespace CommunicationExamples.ViewToView
{
    public class V2VPublisherModule: IModule
    {

        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;
        private readonly V2VConfigModule _v2vConfigModule;

        //cw ViewToViewPublisher : 20
        //{
        private readonly IEventRegistrator _eventRegistrator;
        //}

        private const string ViewId = "Publisher";
        private const string WidgetButtonId = "CreatePublisher";
        private readonly IScreenGroupManager _screenGroupManager;
        
        //{
        public V2VPublisherModule(IChromeManager chromeManager, 
            IChromeRegistry chromeRegistry, 
            IEventRegistrator eventRegistrator, V2VConfigModule v2vConfigModule,
            IScreenGroupManager screenGroupManager
            )
        {
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
            _eventRegistrator = eventRegistrator;
            _v2vConfigModule = v2vConfigModule;
            _screenGroupManager = screenGroupManager;
        }
        //}


        public void Initialize()
        {
           
            _chromeRegistry.RegisterWindowFactory(ViewId, CreateView);
            _chromeRegistry.RegisterWidgetFactory(WidgetButtonId, _chromeManager.ShowWindowButtonFactory());

            _chromeManager.PlaceWidget(WidgetButtonId, "How/Where",
            new InitialShowWindowButtonParameters()
            {
                Text = "Publisher",
                ToolTip = "Demo Button",
                WindowFactoryID = ViewId,
                InitialParameters = new InitialWindowParameters()

            });

            _v2vConfigModule.AutoHookMode = AutoHookMode.IslandMode;

            _chromeManager.Tabwell.ActiveTabChanged += (s, e) =>
                                                           {
                                                               var activeTab = _chromeManager.Tabwell.ActiveTab;
                                                               if(activeTab != null)
                                                                   activeTab.AccentBrush = new SolidColorBrush(Colors.Red);

                                                           };

            _screenGroupManager.CreateGroup("Group 1", Brushes.AliceBlue);
            _screenGroupManager.CreateGroup("Group 2", Brushes.IndianRed);
            _screenGroupManager.CreateGroup("Group 3", Brushes.LightGoldenrodYellow);
            
        }


        //cw ViewToViewPublisher : 28
        //{
        private bool CreateView(IWindowViewContainer viewcontainer, XDocument state)
        {
            var publisher = _eventRegistrator.RegisterPublisher<SendingMessage>(viewcontainer);
            viewcontainer.Content = new PublisherView(publisher);
            viewcontainer.Title = "Publisher";

            viewcontainer.AccentColour = Colors.YellowGreen;
            viewcontainer.TabColour = new SolidColorBrush(Colors.RoyalBlue);

            viewcontainer.AddScreenGroupSelector();
            
            
            //viewcontainer.Content = new complexView(0);))
            return true;
        }
        //}
    }
}
