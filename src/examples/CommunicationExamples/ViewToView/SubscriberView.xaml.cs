﻿using System;
using System.Reactive.Linq;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace CommunicationExamples.ViewToView
{
    /// <summary>
    /// Interaction logic for SubscriberView.xaml
    /// </summary>
    public partial class SubscriberView : UserControl
    {
        public string BookingName
        {
            get
            {
                return (string)FilterBox.Dispatcher.Invoke((Func<string>)(() => (string)((ComboBoxItem)FilterBox.SelectedItem).Content));
            }
        }

        //cw ViewToViewSubscriber: 31
        //{[csharp]
        public SubscriberView(ISubscriber<ReceivingMessage> subscriber)
        {
            var filter = subscriber
                .GetObservable()
                .Where(msg => msg.Stock == BookingName);

            //BookingName value comes from the combobox of the view
            filter.Subscribe(
                m => MessageHistory.Dispatcher.Invoke(
                    (Action)(() => MessageHistory.Items.Add(m)  //Display the message in the MessageHistory
                    )) );
           
            InitializeComponent();
        }
        //}
    }
}
