﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
//cw ViewToViewSubscriber :15
/**namespace*/
//{
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
//}

//cw ViewToView: 31
//{
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ScreenGroup;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

//}

namespace CommunicationExamples.ViewToView
{
    public class V2VSubscriberModule: IModule
    {
        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;

        private const string ViewId = "Subscriber";
        private const string WidgetButtonId = "CreateSubscriber";

        private readonly V2VConfigModule _v2vConfigModule;


        //cw ViewToViewSubscriber: 21
        //{
        private readonly IEventRegistrator _eventRegistrator;
        //}

        //{
        public V2VSubscriberModule(IChromeManager chromeManager,
            IChromeRegistry chromeRegistry,
            IEventRegistrator eventRegistrator, V2VConfigModule v2vConfigModule)
        {
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
            _eventRegistrator = eventRegistrator;

            _v2vConfigModule = v2vConfigModule;
        }
        //}

        public void Initialize()
        {
            _chromeRegistry.RegisterWindowFactory(ViewId, CreateView);
            _chromeRegistry.RegisterWidgetFactory(WidgetButtonId, _chromeManager.ShowWindowButtonFactory());

            _chromeManager.PlaceWidget(WidgetButtonId, "How/Where",
            new InitialShowWindowButtonParameters()
            {
                Text = "Sbuscriber",
                ToolTip = "Demo Button",
                WindowFactoryID = ViewId,
                InitialParameters = new InitialWindowParameters()
                                       
            });

            

        }

        //cw ViewToViewSubscriber: 21
        //{
        private bool CreateView(IWindowViewContainer viewcontainer, XDocument state)
        {
            var sub = _eventRegistrator.RegisterSubscriber<ReceivingMessage>(viewcontainer);
            viewcontainer.Content = new SubscriberView(sub);
            viewcontainer.Title = "Subscriber";

            viewcontainer.AddScreenGroupSelector();
            

            return true;
        }
        //}
    }
}
