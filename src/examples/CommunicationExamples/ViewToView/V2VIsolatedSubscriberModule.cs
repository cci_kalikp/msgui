﻿using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;

namespace CommunicationExamples.ViewToView
{
    internal class V2VIsolatedSubscriberModule : IModule
    {
        private const string ViewId = "IsolatedSubscriber";
        private const string WidgetButtonId = "CreateIsolatedSubscriber";

        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;
        private readonly IEventRegistrator _eventRegistrator;

        public V2VIsolatedSubscriberModule(IChromeManager chromeManager,
                                           IChromeRegistry chromeRegistry,
                                           IEventRegistrator eventRegistrator)
        {
            _eventRegistrator = eventRegistrator;
            _chromeRegistry = chromeRegistry;
            _chromeManager = chromeManager;
        }

        public void Initialize()
        {

            _chromeRegistry.RegisterWindowFactory(ViewId, CreateView);
            _chromeRegistry.RegisterWidgetFactory(WidgetButtonId, _chromeManager.ShowWindowButtonFactory());

            var initialWindowParameters = new InitialWindowParameters
                {
                    InitialLocation = InitialLocation.DockRight
                };

            var initialShowWindowButtonParameters = new InitialShowWindowButtonParameters()
                {
                    Text = "Isolated Sbuscriber",
                    ToolTip = "Isolated Subscriber",
                    WindowFactoryID = ViewId,
                    InitialParameters = initialWindowParameters,
                };

            _chromeManager.PlaceWidget(WidgetButtonId, "How/Where", initialShowWindowButtonParameters);
        }

        private bool CreateView(IWindowViewContainer viewcontainer, XDocument state)
        {
            var addInHost =
                viewcontainer.SetUp32BitProcessIsolatedWrapperFor<SubscriberView>(state, typeof (ReceivingMessage));
            var sub = _eventRegistrator.RegisterMarshalingSubscriber<ReceivingMessage>(viewcontainer, addInHost);

            // viewcontainer.Content = new SubscriberView(sub);
            viewcontainer.Title = "Isolated Subscriber";
            return true;
        }
    }
}
