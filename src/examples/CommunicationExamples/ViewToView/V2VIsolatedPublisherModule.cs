﻿using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;

namespace CommunicationExamples.ViewToView
{
    internal class V2VIsolatedPublisherModule : IModule
    {
        private const string ViewId = "IsolatedPublisher";
        private const string WidgetButtonId = "CreateIsolatedPublisher";

        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;
        private readonly IEventRegistrator _eventRegistrator;

        public V2VIsolatedPublisherModule(IChromeManager chromeManager,
                                           IChromeRegistry chromeRegistry,
                                           IEventRegistrator eventRegistrator)
        {
            _eventRegistrator = eventRegistrator;
            _chromeRegistry = chromeRegistry;
            _chromeManager = chromeManager;
        }

        public void Initialize()
        {
            _chromeRegistry.RegisterWindowFactory(ViewId, CreateView);
            _chromeRegistry.RegisterWidgetFactory(WidgetButtonId, _chromeManager.ShowWindowButtonFactory());

            var initialWindowParameters = new InitialWindowParameters
            {
                InitialLocation = InitialLocation.DockRight
            };

            var initialShowWindowButtonParameters = new InitialShowWindowButtonParameters()
            {
                Text = "Isolated Publisher",
                ToolTip = "Isolated Publisher",
                WindowFactoryID = ViewId,
                InitialParameters = initialWindowParameters,
            };

            _chromeManager.PlaceWidget(WidgetButtonId, "How/Where", initialShowWindowButtonParameters);
        }

        private bool CreateView(IWindowViewContainer viewcontainer, XDocument state)
        {
            var addInHost =
                viewcontainer.SetUp32BitProcessIsolatedWrapperFor<PublisherView>(state, typeof(SendingMessage));
            _eventRegistrator.RegisterMarshalingPublisher<SendingMessage>(viewcontainer, addInHost);

            viewcontainer.Title = "Isolated Publisher";
            return true;
        }
    }
}
