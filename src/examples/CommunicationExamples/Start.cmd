﻿@echo off
cls
setlocal

TITLE Morgan Stanley Internal Limits Management
COLOR 2F

rem **************************************************************************************
rem Parse command-line parameters
rem **************************************************************************************

SET FORCEPROFILE=
SET MSDE_RUNTIME_DEFAULT_SUFFIX=
SET HOMEDIR=%~dp0
 

start "" /D%HOMEDIR%  CommunicationExamples.exe %*

color
endlocal
