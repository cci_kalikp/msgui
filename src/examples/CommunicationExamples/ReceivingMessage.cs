﻿using System;
using System.Collections.Generic;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace CommunicationExamples
{

    //cw CommunicationMessages : 20
    /**
     * ### Message type for subscribing    
     */

    [Message("ReceivingMessage")]
    //{
    [Serializable]
    public class ReceivingMessage
    {

        public String Stock { get; set; }
        public String Message { get; set; }
        public decimal Price { get; set; }
        public OrderType OrderType { get; set; }
        public DateTime Date { get; set; }
        public List<Point> Points { get; set; }

    }

    //}
}
