﻿using System;
using System.Collections.Generic;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;


namespace CommunicationExamples
{
    //cw CommunicationMessages : 10  
    /**
     * ## Description
     * The message types used in the examples
     * ## Code
     * ### Message for publishing
     */

    // cw ViewToViewConnection : 31 cw Launcher_Cross_Launcher_Message: 20
    //{
    [Message("SendingMessage"), Serializable]
    //}

    //cw ViewToViewConnection: 31 cw CommunicationMessages : 11 cw Launcher_Cross_Launcher_Message: 21
    //{
    public class SendingMessage
    //}
    // cw CommunicationMessages : 11 cw Launcher_Cross_Launcher_Message: 22
    //{
    {
        public String StockName { get; set; }
        public String Description { get; set; }
        public decimal Price { get; set; }
        public OrderType OrderType { get; set; }
        public DateTime Date { get; set; }
        public List<Point> Points { get; set; } 
    }
    //}

    //cw Launcher_Cross_Launcher_Message: 23
    //{
    public enum OrderType
    {
        Sale = 0,
        Buy = 1
    }
    //}
}
