﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Mail;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Xml.Linq;

//cw ModuleToModulePublisher :15  
/**##namespace*/
/**IModule*/
//{
using MSDesktop.HTTPServer;
using Microsoft.Practices.Composite.Modularity;
//}
/**IChromexxx*/
//{
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
//}
/**ICommunicator*/
//cw ModuleToModulePublisher :15 cw ModuleToModule: 21
//{
using MorganStanley.MSDotNet.MSGui.Core.Messaging; 
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

//}

namespace CommunicationExamples.ModuleToModule
{

    public class PublishingModule : IModule
    {
        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;

        //cw ModuleToModulePublisher: 20
        /**##code*/
        //cw ModuleToModulePublisher: 25
        /**#### register instances*/
        //{
        private readonly ICommunicator _communicator;
        //}

        private IModulePublisher<SendingMessage> _publisher;
        private readonly string appId;
        //{
        public PublishingModule(IChromeManager chromeManager, IChromeRegistry chromeRegistry, ICommunicator communicator, IApplication application_)
        {
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
            _communicator = communicator;
            appId = application_.Name;
        }
        //}

        //cw ModuleToModulePublisher: 28
        public void Initialize()
        {
            /**#### get the instance */
            //{
            _publisher = _communicator.GetPublisher<SendingMessage>();
            //}

            _chromeRegistry.RegisterWidgetFactory(
                @"\\CommunicationExamples\ModuleToModule\PublishingWidget",
                CreateStrangeWidget);

            _chromeManager.PlaceWidget(
                @"\\CommunicationExamples\ModuleToModule\PublishingWidget",
                "ModuleToModule/Sender",
                null);

            foreach (var arg in Environment.GetCommandLineArgs())
            {
                if (arg.StartsWith("/ex=") || arg.StartsWith("-ex="))
                { 
                    _chromeManager.StatusBar.AddTextStatusItem("test_statusbar_item", StatusBarItemAlignment.Right);
                    _chromeManager.StatusBar["test_statusbar_item"].UpdateStatus(StatusLevel.Information, "Extra Arguments:" + arg.Substring(4)); 
                    break;
                  
                }
            }
           
        }
         

        private bool CreateStrangeWidget(IWidgetViewContainer widget, XDocument state)
        {
            var label = new Label {Content = "MS"};
            var textBox = new TextBox {Width = 200, AcceptsReturn = true};
            GroupBox grpEmail = new GroupBox() {Header = "Publish message through email"}; 
            WrapPanel pnlEmail = new WrapPanel(); 
            
            pnlEmail.Orientation = Orientation.Vertical;
            var cbAllowSaveRoute = new CheckBox {Content = "Allow Save Route"};
            cbAllowSaveRoute.IsChecked = true;
            var cbAllowStartNew = new CheckBox {Content = "Allow Start New Application"};
            cbAllowStartNew.IsChecked = true;
            var cbSerializeInJson = new CheckBox {Content = "Serialize message content in Json"};
            cbSerializeInJson.IsChecked = true;
            StackPanel st = new StackPanel();
            st.Orientation = Orientation.Horizontal;
            var label2 = new Label {Content = "Extra Argument"};
            st.Children.Add(label2);
            var txtExtraArg = new TextBox() {Width = 100, AcceptsReturn = false, Height = 20};
            st.Children.Add(txtExtraArg);
            st.Margin = new Thickness(0);

            var cbAlwaysStartNew = new CheckBox { Content = "Always Start New Application" };
            cbAlwaysStartNew.Checked += (sender_, args_) =>
            {
                cbAllowSaveRoute.IsEnabled = cbAllowStartNew.IsEnabled = false;
                cbAllowSaveRoute.IsChecked = false;
                cbAllowStartNew.IsChecked = true;
            };
            cbAlwaysStartNew.Unchecked += (sender_, args_) =>
                {
                    cbAllowSaveRoute.IsEnabled = cbAllowStartNew.IsEnabled = true; 
                };
           
            pnlEmail.Children.Add(cbAllowSaveRoute);
            pnlEmail.Children.Add(cbAllowStartNew);
            pnlEmail.Children.Add(cbAlwaysStartNew);
            pnlEmail.Children.Add(cbSerializeInJson);


            pnlEmail.Children.Add(st);
            grpEmail.Content = new ScrollViewer() { HorizontalScrollBarVisibility = ScrollBarVisibility.Auto, VerticalScrollBarVisibility = ScrollBarVisibility.Auto, Content = pnlEmail };

            var buttonSendEmail = new Button() { Content = "Send Email to Transfer Message to this App Later" };
            pnlEmail.Children.Add(buttonSendEmail);

            var buttonSendEmail2 = new Button() { Content = "Send Email to Start this App Later" };
            pnlEmail.Children.Add(buttonSendEmail2);
             
            buttonSendEmail.Click += (sender_, args_) =>
                {
                    var msg = new MailMessage();
                    msg.From = new MailAddress(Environment.UserName + "@ms.com");
                    msg.To.Add(new MailAddress(Environment.UserName + "@ms.com"));
                    msg.Subject = "Publish message to CommunicationExamples";
                    msg.IsBodyHtml = true;
                    bool allowSaveRoute = cbAllowSaveRoute.IsChecked.GetValueOrDefault();
                    bool allowStartNewApplication = cbAllowStartNew.IsChecked.GetValueOrDefault();
                    bool alwaysStartNewApplication = cbAlwaysStartNew.IsChecked.GetValueOrDefault();
                    string extraArgument = string.IsNullOrEmpty(txtExtraArg.Text) ? null : "/ex=" + txtExtraArg.Text;
                    //cw Launcher_Cross_Launcher_Message: 51
                    //{
                    SerializationFormat format = cbSerializeInJson.IsChecked.GetValueOrDefault() ? SerializationFormat.Json : SerializationFormat.Binary;
                    string url = DispatchableLauncherMessage.GenerateLink(
                            new SendingMessage
                                {
                                    StockName = "MS",
                                    Description = textBox.Text,
                                    Price = 999.99m,
                                    OrderType = OrderType.Buy,
                                    Date = DateTime.Now,
                                    Points = new List<Point>() { new Point(1.1, 2.2), new Point(3.3, 4.4) },
                                },
                                appId.StartsWith("msdesktop.communicationexmples") ? appId :
                                "msdesktop.communicationexmples.prod", format, allowStartNewApplication, allowSaveRoute, extraArgument, alwaysStartNewApplication);
                    //}
                    if (1 == 2)
                    {

                        if (format == SerializationFormat.Json)
                        {
                            //cw Launcher_Cross_Launcher_Message: 52
                            //{
                            url = DispatchableLauncherMessage.GenerateLink(
                          "SendingMessage",
                          new Dictionary<string, object>() 
                      { 
                          { "StockName", "MS" }, 
                          { "Description", textBox.Text },
                          {"Price", 999.99m}, 
                          {"OrderType", OrderType.Buy},
                          {"Date", DateTime.Now}, 
                          {"Points", new List<Point>{ new Point(1.1, 2.2), new Point(3.3, 4.4)}},
                      }, "msdesktop.communicationexmples.prod", allowStartNewApplication, allowSaveRoute, extraArgument, alwaysStartNewApplication);
                            //}
                        }
                    } 


                    msg.Body = string.Format("Click <a href=\"{0}\">here</a> {1}</br></br>If it doesn't open in your web browser, try copy following url: {0} and open it manually in your web browser</br>", url,
                        "to publish SendingMessage to CommunicationExamples (" + (allowSaveRoute ? "allow saving route" : "disallow saving route") + "; " + 
                        (allowStartNewApplication ? "allow starting new application" : "disallow starting new application") + ")");
       
                    using (var client = new SmtpClient("mta-hub.ms.com"))
                    {
                        client.Send(msg);  
                    }

                };


            buttonSendEmail2.Click += (sender_, args_) =>
            {
                var msg = new MailMessage();
                msg.From = new MailAddress(Environment.UserName + "@ms.com");
                msg.To.Add(new MailAddress(Environment.UserName + "@ms.com"));
                msg.Subject = "Start CommunicationExamples";
                msg.IsBodyHtml = true;
                bool alwaysStartNewApplication = cbAlwaysStartNew.IsChecked.GetValueOrDefault();
                string extraArgument = string.IsNullOrEmpty(txtExtraArg.Text) ? null : "/ex=" + txtExtraArg.Text;

                //cw Launcher_Cross_Launcher_Message: 66
                //{
                string url = DispatchableLauncherMessage.GenerateApplicationStartLink(appId.StartsWith("msdesktop.communicationexmples") ? appId :
                            "msdesktop.communicationexmples.prod",  extraArgument, alwaysStartNewApplication); 
                //}
                msg.Body = string.Format("Click <a href=\"{0}\">here</a> {1}</br></br>If it doesn't open in your web browser, try copy following url: {0} and open it manually in your web browser</br>", url,
                    "to Start CommunicationExamples " + (alwaysStartNewApplication ? "always" : "if not started yet"));

                using (var client = new SmtpClient("mta-hub.ms.com"))
                {
                    client.Send(msg);  
                }

            };



            GroupBox grpPublish = new GroupBox() {Header = "Publish message directly through launcher"};
            WrapPanel pnlPublish = new WrapPanel(); 
            pnlPublish.Orientation = Orientation.Vertical;
            var cbHideProgress = new CheckBox { Content = "Hide Progress" };
            cbHideProgress.IsChecked = true;
            var cbAllowSaveRoute2 = new CheckBox { Content = "Allow Save Route" };
            cbAllowSaveRoute2.IsChecked = true;
            var cbAlwaysStartNew2 = new CheckBox { Content = "Always Start New Application" };
            cbAlwaysStartNew2.Checked += (sender_, args_) =>
            {
                cbAllowSaveRoute2.IsEnabled = false;
                cbAllowSaveRoute2.IsChecked = false; 
            };
            cbAlwaysStartNew2.Unchecked += (sender_, args_) =>
                {
                    cbAllowSaveRoute2.IsEnabled = true;
                };

            pnlPublish.Children.Add(cbHideProgress);
            pnlPublish.Children.Add(cbAllowSaveRoute2);
            pnlPublish.Children.Add(cbAlwaysStartNew2);

            var st3 = new StackPanel();
            st3.Orientation = Orientation.Horizontal;
            st3.Children.Add(new Label {Content = "App Id", Height = 18, Padding =  new Thickness(-1)});
            var txtTargetAppId = new TextBox(){Height = 20};
            txtTargetAppId.Text = GetSiblingId();
            st3.Children.Add(txtTargetAppId);
            st3.Margin = new Thickness(0);
            pnlPublish.Children.Add(st3);

            var st2 = new StackPanel();
            st2.Orientation = Orientation.Horizontal;
            var label3 = new Label { Content = "Extra Argument", Height = 18, Padding = new Thickness(-1) };
            var txtExtraArg2 = new TextBox() { Width = 150, AcceptsReturn = false, Height = 20};
            st2.Margin= new Thickness(0);
            st2.Children.Add(label3);
            st2.Children.Add(txtExtraArg2);

            pnlPublish.Children.Add(st2);


            var btnPublishMessage = new Button() { Content = "Publish to another instance" };
            pnlPublish.Children.Add(btnPublishMessage);

            var btnStartApp2 = new Button() { Content = "Start another instance" };
            pnlPublish.Children.Add(btnStartApp2);

            btnPublishMessage.Click += (sender_, args_) =>
                {

                    string targetApplicationId = txtTargetAppId.Text;
                    bool hideProgressBar = cbHideProgress.IsChecked.GetValueOrDefault();
                    bool allowSaveRoute = cbAllowSaveRoute2.IsChecked.GetValueOrDefault();
                    bool alwaysStartNewApplication = cbAlwaysStartNew2.IsChecked.GetValueOrDefault();
                    string extraArgument = string.IsNullOrEmpty(txtExtraArg2.Text) ? null : "/ex=" + txtExtraArg2.Text;
                    //cw Launcher_Cross_Launcher_Message: 61
                    /**You could directly publish message to the target application similar to m2m publishing(usually because the target application might needs to be started if is down */
                    //{
                    DispatchableLauncherMessage.Publish(
                        new SendingMessage
                            {
                                StockName = "MS",
                                Description = textBox.Text,
                                Price = 999.99m,
                                OrderType = OrderType.Buy,
                                Date = DateTime.Now,
                                Points = new List<Point>() {new Point(1.1, 2.2), new Point(3.3, 4.4)},
                            },
                        targetApplicationId, hideProgressBar, allowSaveRoute, extraArgument, alwaysStartNewApplication);
                    //}
                };

            btnStartApp2.Click += (sender_, args_) =>
            {

                string targetApplicationId = txtTargetAppId.Text; 
                bool alwaysStartNewApplication = cbAlwaysStartNew2.IsChecked.GetValueOrDefault();
                string extraArgument = string.IsNullOrEmpty(txtExtraArg2.Text) ? null : "/ex=" + txtExtraArg2.Text;
                //cw Launcher_Cross_Launcher_Message: 67
                //{
                DispatchableLauncherMessage.StartApplication(targetApplicationId,  extraArgument, alwaysStartNewApplication); 
                //}
            };

            grpPublish.Content = new ScrollViewer() { HorizontalScrollBarVisibility = ScrollBarVisibility.Auto, VerticalScrollBarVisibility = ScrollBarVisibility.Auto, Content = pnlPublish };
            
            AutomationProperties.SetAutomationId(textBox, "ModulePublishingBox");
             
            //cw ModuleToModulePublisher: 30
            /**
             * #### Publish message
             */
            //cw ModuleToModulePublisher: 31
            //{
            //will send out the contents of the textbox
            textBox.TextChanged += (s, e) => 
                _publisher.PublishRelativeToCurrent(new SendingMessage {StockName = "MS", Description = textBox.Text}, CommunicationTargetFilter.MachineInstance);
            //}

            var dockPanel = new DockPanel();

            dockPanel.Children.Add(label);
            dockPanel.Children.Add(textBox); 
            dockPanel.Children.Add(grpEmail);
            dockPanel.Children.Add(grpPublish);
            widget.Content = dockPanel;
            return true;
        }
         
        private string GetSiblingId()
        {
            if (appId.StartsWith("msdesktop.communicationexmples.prod"))
            {
                string suffix = appId.Replace("msdesktop.communicationexmples.prod", string.Empty);
                if (string.IsNullOrEmpty(suffix)) return "msdesktop.communicationexmples.prod2";
                return "msdesktop.communicationexmples.prod";
            }
            else if (appId.StartsWith("msdesktop.communicationexmples.dev"))
            {
                string suffix = appId.Replace("msdesktop.communicationexmples.dev", string.Empty);
                if (string.IsNullOrEmpty(suffix)) return "msdesktop.communicationexmples.dev2";
                return "msdesktop.communicationexmples.dev";

            }
            return "msdesktop.communicationexmples.prod2";
        }
    }
}
