﻿using System;
using MSDesktop.WebSupport.MessageRouting;
using Microsoft.Practices.Unity;
using MSDesktop.WebSupport.CEF;

namespace CommunicationExamples.ModuleToModule
{ 
    class WebModule : CEFBasedHTMLModule
    {
        public WebModule(IUnityContainer unityContainer)
            : base(unityContainer)
        { 
        }

        protected override string Name
        {
            get { return "WebModule"; }
        }

        protected override Uri Url
        {
            get 
            { 
                //return new Uri("http://google.pl");
                return new Uri(String.Format("{0}\\ModuleToModule\\WebModule.html", AppDomain.CurrentDomain.BaseDirectory),
                              UriKind.RelativeOrAbsolute); 
            }
        }
    }
}
