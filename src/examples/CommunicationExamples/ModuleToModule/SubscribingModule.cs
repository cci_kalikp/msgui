﻿using System;
using Microsoft.Practices.Composite.Modularity;
//cw ModuleToModuleSubscriber: 10
/**##namepsace*/
//{
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
//}
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.StatusBar;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;


namespace CommunicationExamples.ModuleToModule
{
    public class SubscribingModule : IModule
    {
        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;

        //cw ModuleToModuleSubscriber: 20
        /**
         * ##context
         *  put the code in the SubscribingModule.Initialize method
         *  
         * ##code
         * #### Regist message type for subscribing
         */

        //{
        private readonly ICommunicator _communicator;
        //}

        private IModuleSubscriber<ReceivingMessage> _subscriber;

        //{
        public SubscribingModule(IChromeManager chromeManager, IChromeRegistry chromeRegistry,
                                 ICommunicator communicator)
        {
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
            _communicator = communicator;
        }
        //}

        public void Initialize()
        {
            var initialLabelParameters = new InitialStatusLabelParameters
                {
                    Level = StatusLevel.Information
                };

            var statusLabel = _chromeManager.AddStatusLabel(Guid.NewGuid().ToString(), initialLabelParameters);

            //cw ModuleToModuleSubscriber: 21 
            /**#### Get instances*/
            //{
            _subscriber = _communicator.GetSubscriber<ReceivingMessage>();
            //}

            //cw ModuleToModuleSubscriber: 30
            /**
            * #### Subscribe  and handle messages
            */
            //cw ModuleToModuleSubscriber: 31
            //{[csharp]
            _subscriber.GetObservable().Subscribe(msg =>
                {
                    statusLabel.Text = msg.OrderType.ToString() + " " + msg.Stock + ": " + msg.Message.Replace("\n", " ").Replace("\r", " ") + " with Price: " + msg.Price + " on " + msg.Date;
                }); //statusLabel displays messages in the statusbar
            //}
        }
    }
}
