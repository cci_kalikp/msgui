﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommunicationExamples.ViewToView
{
    /***
     * 
     * Tree structure 
     * 
     * - Communication
     *    -Message Definitions  
     *    -Message Convertion
     *    -View to view
     *         -publisher
     *         -subscriber
     *         
     *         -connection
     *             * UI
     *             * Adapters
     *             
     *    -Module to Module
     *         -publisher
     *         -subscriber
     *         -adapters
     *         
     *    -Ipc 
     * 
     */

    #region Communication
   
    #endregion


    #region AdapterService
    //cw MessageAdapter: 0
    /**
     * 
     * ## Description
     *  * MSDesktop allows passing messages from senders to receivers even they publish and subscribe different type of messages.        
     *  * The only thing needed is to register the converting method before sending the messages.
     *  * The view to view communication and module to module communication share the same message adapter service
     * 
     */

    //cw MessageAdapter: 10
  /**##code*/
    /**
     * ### Get instances
     * ##### In your managing module, obtain the instance for the adapter service     
     */

    //cw MessageAdapter:20
    /**
     * 
     * ###  Convert messages
     * ##### Provide your converting method to the adapter service
     * 
     */

    #endregion

    #region View To View
    //cw ViewToView: 10
    /**
     * ## Description
     * 
     *   * View to view communication is a point to point communicaiton among views.
     *  
     *     Each view container (IViewContinaerBase) can register as a publisher or subscriber for any message types.     
     *   
     *   * We provide an UI to allow the users connecting/disconnecting the message senders and subscribers at the runtime.
     *   
     * 
     *   * Convert messages if needed.  see [Message Convertion][cw_ref MessageAdapter]
     */

    //cw ViewToView: 30
    /**
     * ## Namespaces
     */



    #region  PublishPage

      //cw ViewToViewPublisher : 5
      /**
       * ## Description
       * Obtain publisher and publish messages
       * 
       */
    //cw ViewToViewPublisher :19
    /**##code
       * #### Get Instances
       * 
       * In the module that create the view, obtain the instance of the register
       */

      //cw ViewToViewPublisher : 28
    /**
    * #### Register
     * Register the view container as a publisher for the message type
    * 
    */

    //cw ViewToViewPublisher : 30
      /**
      * #### Publish messages
      */

    #endregion
    

    #region SubscriberPage
    //cw ViewToViewSubscriber: 10
    /**
     * ## Description
     *   Obtain subscriber, subscribe and handle messages
     * ### Get Instances
     * In your module to create the view, obtain the instance of the register
     */

    //cw ViewToViewSubscriber: 20
    /**
     * ##code
     * #### Register
     * Register the view container as a subscriber for the message type
     */

    //cw ViewToViewSubscriber : 30
    /**
    * #### Subscribe and handle messages
    *
    */

    #endregion

    #region  Connection
    //cw ViewToViewConnection: 0 
    /**
     * ## Description
     * 
     * MSDesktop provide a config GUI to allow users connecting/disconnecting the message senders and subscribers at the runtime 
     * 
     * Also, these connections will be persisted with the layout
     */

    //cw ViewToViewConnection: 10
    /**## Context 
     * When initialize the application
     */
    //cw ViewToViewConnection: 13
    /**
     * ## Code
     * To add the GUI into the application, just add the module 
     */


    //cw ViewToViewConnection: 20
    /**
     * ## GUI
     * After add the module, one menu item will be added into the application
     * 
     * ![ViewToViewConfigMenuItem](images/ViewToView/v2vConfigMenu.png)
     * 
     * click to open the config GUI
     * 
     * ![ViewToViewConfigUI](images/ViewToView/v2vConfigUI.png)
     */

    //cw ViewToViewConnection: 30
    /**
     * ## Persistence
     * 
     * The connections will be persisted with the layout.
     * By default, it will use the full type name, but, you can provide unique type id instead.
     */

    #endregion

 
   

    #endregion


    #region Module to Module
    //cw ModuleToModule: 10
    /**
     * ## Description
     * 
     *   * MSDesktop provides APIs for communication among modules
     * 
     *   * The messages sended are broadcasted to all subscribers
     *   
     *   * Convert messages if needed.  see [Message Convertion][cw_ref MessageAdapter]
     * 
     */

    //cw ModuleToModule: 20
    /**
     * ## Namespace
     */

    #region Publish
    //cw ModuleToModulePublisher: 10
    /**
     * ## Description
     * Obtain publisher and publish messages
     */

    #endregion

    #region subscribe
    //cw ModuleToModuleSubscriber: 10
    /**
    * ## Description
     * Obtain subscriber, subscribe and handle messages
    */
    #endregion


    #endregion


    class CWPages
    {
    }


    
}
