﻿//cw Component_Persistence_Overview :10
/**
 * ##Description
 * Component Persistence is enabled through setting PersistenceConfigurator.Configuration attached property on components to be persisted and then persisted automatically by the profile system or by a bruch of codes provided.
 * 
 * ## Assembly
 * To use DependancyPropertyPersistor, you need refer to <b>InfragisticsWPF4.Persistence.v14.1.dll</b> in your project
 * 
 * ## Namespace
 */

//cw Component_Persistence_Overview :15
/** 
 * ## Persistence Storage
 * There are 2 ways of specifying the persistence storage
 *  <li><b>Save along with the window view state</b></li>
 *  <li><b>Manage the persistence life cycle and storage manually</b>, can be Global or along with Current Profile or an existing Profile</li>
 * 
 * ## Persistent of controls
 * Not only a window can be persisted, controls on the ribbon/launcher bar or within the window can be persisted
 *  <li><b>Standalone</b></li>
 *  <li><b>Along with the parent window and even with multiple states</b></li>
 * 
 * ## Customization Support
 * Customization is supported in different ways
 *  <li><b>Define custom component persistor</b>, if the default ones doesn't qualify development needs</li>
 *  <li><b>Manual State Loading/Saving</b></li>
 *  <li><b>Define custom component persistor enumerator</b>, if developer want to change the default component persistor enumeration order or skip some enumerator in a generic way</li>
 *  <li><b>Save/Load state of a window or component to/from XDocument directly</b>, so a custom storage can be used</li>
 *  
 * ## A Simple Example
 * <li>Create a user control with <b>PersistenceConfigurator.Configuration</b> specified for components to be persisted</li>
 */

//cw Component_Persistence_Configuration: 10
/**
 * ##Description
 * PersistenceConfigurator.Configuration attached property is introduced to enable persistence of components, and is of a type "Configuration" with following properties:
 * <li><b>Persistors</b>(of type List&lt;IComponentPersistor&gt;), it can contain 0 or more instance of IComponentPersistor, which used persist "THIS" component. It would inherit the "ExceptionPolicy", "AutoLoadStage", "AutoSaveStage" of the configurtion it belongs to, if they are set to "Default" on the persistor and are not set to "Default" on the configuration it belongs to</li>
 * <li><b>ExceptionPolicy</b>(of enum type PersistenceExceptionPolicy), default is "Default" which means it would inherit the setting of this value from parent component, and if is the root, default is "Ignore", and if set to "Throw". If the calculated value of its child persistor is "Ignore", persistence would still go on if error occurs when executing a specific persistor, and if set to "Throw", the persistence would stop instantly when exception occurs on that persistor</li>
 * <li><b>AutoLoadStage</b>(of enum type LoadStage), default is "Default", which means it would inherit the setting of this value from parent component. The real AutoLoadStage is calculated based on value specified on the Configurator, the AutoLoadStage specified on the persistor, and the coded default AutoLoadStage for the persistor. The calculated value would be used to decide in what stage the component can be restored. If the calculated value is "Later", the state must be loaded using code</li>
 * <li><b>AutoSave</b>(of enum type AutoSaveMode), default is "Default", which means it would inherit the setting of this value from parent component. The real AutoSave is calculated based on value specified on the Configurator, the AutoSave specified on the persistor, and the coded default AutoSave for the persistor. The calculated value would be used to decide whether the persistor would be executed automatically when the root container is persisted or not</li>
 * <li><b>SuppressChildrenPersistence</b> (bool), default is false, if set to true, component persistors defined for children components of this componet would be neglected. This is useful when the component is a complex user control with persistence configuration specified itself, and the hoster doesn't want to persist it. Also, it can be used to turn on/off persistence of the component easily</li>
 * Also, Configuration implements "IPersistable", so itself can be persisted and restored
 * 
 * ## Persistors
 * ###XamDataGridLayoutPersistor: 
 * Can be applied to XamDataGrid
 * ###DependancyPropertyPersistor: 
 * Can be used to persist most components. The IG PersistenceManager is used if the properties to be persisted by the homemade simple implemenation. So always remember to reference <b>"InfragisticsWPF4.Persistence.v14.1"</b> assembly. 
 * 4 modes are supported for this persistor
 * <ol>
 * <li><b>PropertyName</b>: one or more properties of the component to be included/excluded can be defined by providing exact property names seperated by ";" or ","</li>
 * <li><b>Contains</b>: similar to "PropertyName" mode, except that the a contains check is executed upon the values specified to decide if the component property can be persisted</li>
 * <li><b>PropertyPath</b>: similar to "PropertyName" mode, except that it supports specifying children property names using a "ChildrenObjectPropertyName[].ChildPropertyName" gramma(so only for children of explicit enumeration types)</li>
 * <li><b>PropertyType</b>: use the type full name to specifiy properties</li>
 * </ol>
 * 
 * ## Assembly
 * To use DependancyPropertyPersistor, you need refer to <b>InfragisticsWPF4.Persistence.v14.1.dll</b> in your project
 * 
 * ##Set PropertyConfigurator.Configuration
 * ###1. Using DefaultConfigurationExtension Markup Extension in Xaml  
 **/
//cw Component_Persistence_Configuration: 20
/**
 * ###2. Define the properties of the Configuration explicitly
 */
//cw Component_Persistence_Configuration: 30
/**
 * ###3. Set the Configuration dynamically using code
 */

//cw Component_Persistence_Persist_Window: 10
/**
 * ##Description
 * The most commonly usage of component persistence is to persist the state of controls on a window. After the initial [persistence configuration][cw_ref Component_Persistence_Configuration] done, there are 3 ways(listed from the mostly commonly used to less commonly used) to persist the state of the window
 * 
 * ## Assembly
 * To use DependancyPropertyPersistor, you need refer to <b>InfragisticsWPF4.Persistence.v14.1.dll</b> in your project
 * 
 * ##Namespace 
 */

//cw Component_Persistence_Persist_Window: 12
/**
 * ## Method 1: Persist along with the profile specific window state
 * ### Situation 1: all controls are created directly in the view content:
 * Set <b>InitialWindowParameters.Persistable</b> to true when invoking the window through IChromeManager
  */
//

//cw Component_Persistence_Persist_Window: 14
/**
 * ### Situation 2: some controls are loaded using DataTemplate:
 *  Set <b>InitialWindowParameters.Persistable</b> to true, <b>InitialWindowParameters.PersistorType</b> to "typeof(ViewContainerPersistor&lt;VisualTreeElementPersistorEnumerator&gt;)", <b>InitialWindowParameters.DynamicComponentPersistors</b> to true when invoking the window through IChromeManager
 */
//cw Component_Persistence_Persist_Window: 16
/**
 * <br/>
 * The LoadState method of the root view persistor would be called when the window is loaded, and the SaveState method would be called when the current profile is saved. And the data would be saved to a new element "ViewState" (instead of the existing "State" element to avoid accidental overriden/misread of the component persistence data in user code)
 */
//cw Component_Persistence_Persist_Window: 17
/**
 * The root view persistor(which implement directly IRootComponentPersistor&lt;TEnumerator&gt;, and IPersistable indirectly) can be accessed using IPersistable <b>GetPersistor</b>(this IWindowViewContainer container_) extenstion method defined in <b>ComponentPersistenceExtensions</b>, if developer want to access/refresh the state other than the 2 stage mentioned above, or direct the state to some other storage 
 **/
//cw Component_Persistence_Persist_Window: 20
/**
 * ## Method 2: Perist along with the profile specific window state for windows created implicitly using [ViewModeAPI][cw_ref ViewModelAPI_Intro]
 * The only difference from a normal ViewModelAPI call when add a view model api driven button, provide an instance of FrameworkViewParameters implementing <b>"IPersistableFrameworkViewParameters"</b>(which is an empty interface inherit directly from "IFrameworkViewParameters") instead of implementing "IFrameworkViewParameters".
 **/

//cw Component_Persistence_Persist_Window: 30
/**
 * ## Method 3: Persist to Global, CurrentProfile or OtherProfile storage
 * Different from method 1 and 2, the saving/loading of window state should be invoked by developer explicitly by specifying one of the 3 storage(Global, CurrentProfile and OtherProfile), and also usually by specifying a unique state id
 * <li>Call <b>LoadStandaloneState</b>(this IWindowViewContainer container_, PersistenceStorage storage_, string stateId_, bool loadManualItems_ = false) extension method defined in <b>ComponentPersistenceExtensions</b> to load the state, and <b>SaveStandaloneState</b>(this IWindowViewContainer container_, PersistenceStorage storage_, string stateId_) extension method defined in <b>ComponentPersistenceExtensions</b> to save the state</li>
 **/
//cw Component_Persistence_Persist_Window: 35
/**
 * <li>The storage can be one of the following 3 types:
 * <li><b>Global</b>: instantiated using PersistenceStorage.Global, and the state would be persisted instantly to the global storage when the save method is called</li>
 * <li><b>CurrentProfile</b>: instantiated using PersistenceStorage.CurrentProfile, and the state would be saved in memory when the save method is called, and flush to persistence when the current profile is saved</li>
 * <li><b>OtherProfile</b>: instantiated using PersistenceStorage.Profile(string profileName_), and the state would be persistent instantly to the specific existing profile storage when the save method is called</li>
 * </li>
 * <br/>
 * <li>The root view persistor can be accessed using IPersistable <b>GetStandalonePersistor</b>(this IWindowViewContainer container_, string stateId_) extenstion method defined in <b>ComponentPersistenceExtensions</b> if developer want to redirect the state to some other storage</li>
 * <li><b>LoadingState</b>, <b>LoadedState</b>, <b>SavingState</b> and <b>SavedState</b> global events would be triggerred by <b>ComponentPersistenceExtensions</b> with argument of type <b>PersistenceStateEventArgs</b> which can be checked for the state being loaded/saved now</li>
 * 
 **/

//cw Component_Persistence_Persist_Control: 10
/**
 * ##Description
 * Other than persisting the window, a control can also be persisted, After the initial [persistence configuration][cw_ref Component_Persistence_Configuration] done, there are also 4 ways(listed from the mostly commonly used to less commonly used) to persist the state of the control
 *
 * ## Assembly
 * To use DependancyPropertyPersistor, you need refer to <b>InfragisticsWPF4.Persistence.v14.1.dll</b> in your project
 * 
 * ##Namespace 
 */

//cw Component_Persistence_Persist_Control: 12
/**
 * ## Method 1: Persist sub state to profile specific window state
 * <li>Use <b>LoadSubState</b>(this IWindowViewContainer container_, DependencyObject component_, bool loadManualItems_ = true, string subStateId_ = null) extension method defined in <b>ComponentPersistenceExtensions</b> to load the sub state from state of root container. Usually control states loaded using this way are configured to "AutoLoadStage" equals <b>"LoadStage.Later"</b></li>
 **/
//cw Component_Persistence_Persist_Control: 20
/**
 * <li>Use <b>SaveSubState</b>(this IWindowViewContainer container_, DependencyObject component_, string subStateId_ = null) extension method defined in <b>ComponentPersistenceExtensions</b> to save the sub state of to the state of root container.Usually control states saved using this way are configured to "AutoSave" equals <b>"AutoSaveMode.False"</b></li>
 * <li>Use <b>RemoveSubState</b>(this IWindowViewContainer container_, DependencyObject component_, string subStateId_ = null) extension method defined in <b>ComponentPersistenceExtensions</b> to remove the sub state from state of root container.Usually control states remove using this way are configured to "AutoSave" equals <b>"AutoSaveMode.False"</b></li>
 **/
//cw Component_Persistence_Persist_Control: 30
/**
 * <li>If the sub state id is provided, multiple states of the same component can be managed along with turning off auto load and auto save as following</li>
 **/

//cw Component_Persistence_Persist_Control: 40
/**
 * ## Method 2: Persist sub state to explicitly persisted(Global/CurrentProfile/OtherProfile) window state
 * <li>Use <b>LoadStandaloneSubState</b>(this IWindowViewContainer container_, PersistenceStorage storage_, string stateId_, DependencyObject component_, bool loadManualItems_ = true, string subStateId_ = null) extension method defined in <b>ComponentPersistenceExtensions</b> to load the sub state from state of root container. Usually control states loaded using this way are configured to "AutoLoadStage" equals <b>"LoadStage.Later"</b></li>
 **/
/**
* Xaml similar to Method 1
**/
//cw Component_Persistence_Persist_Control: 50
/**
 *<li>Use <b>SaveStandaloneSubState</b>(this IWindowViewContainer container_, PersistenceStorage storage_, string stateId_, DependencyObject component_, string subStateId_ = null) extension method defined in <b>ComponentPersistenceExtensions</b> to save the sub state of to the state of root container.Usually control states saved using this way are configured to "AutoSave" equals <b>"AutoSaveMode.False"</b></li>
 *<li>Use <b>RemoveStandaloneSubState</b>(this IWindowViewContainer container_, PersistenceStorage storage_, string stateId_, DependencyObject component_, string subStateId_ = null) extension method defined in <b>ComponentPersistenceExtensions</b> to remove the sub state from state of root container.Usually control states remove using this way are configured to "AutoSave" equals <b>"AutoSaveMode.False"</b></li>
 **/
/**
* Xaml similar to Method 1
**/
//cw Component_Persistence_Persist_Control: 60
/**
 * <li>If the sub state id is provided, multiple states of the same component can be managed along with turning off auto load and auto save as following</li>
 **/
/**
* Xaml similar to Method 1
**/

//cw Component_Persistence_Persist_Control: 70
/**
 * ## Method 3: Persist to global/profile widget infos
 *  Use IPersistable <b>GetStandaloneComponentPersistor</b>(this DependencyObject component_, string stateId_ = null) extension method defined in <b>ComponentPersistenceExtensions</b> 
 *  <li>Call its <b>LoadState</b> in the widget init handler, call its <b>SaveState</b> method in the widget dehydrate handler</li>
 **/
//cw Component_Persistence_Persist_Control: 72
/** 
 *  <li>Call its <b>LoadState</b> to store the state somewhere and call its <b>SaveState</b> method to load it from there</li>
 **/

//cw Component_Persistence_Persist_Control: 80
/**
 * ## Method 4: Persist to standalone Global/CurrentProfile/OtherProfile storage 
 * <li>Call the <b>LoadStandaloneComponentState</b>(this DependencyObject component_, PersistenceStorage storage_, string stateId_) extension method defined in <b>ComponentPersistenceExtensions</b> to load the state of the component specified by state id from the storage specified</li>
 **/
//cw Component_Persistence_Persist_Control: 90
/** 
 * <li>Call the <b>SaveStandaloneComponentState</b>(this DependencyObject component_, PersistenceStorage storage_, string stateId_) extension method defined in <b>ComponentPersistenceExtensions</b> to save the state of the component specified by state id to the storage specified</li>
 **/

//cw Component_Persistence_Extend_Framework: 10
/**
 * ##Description
 * If the current framework provided implementation doesn't qualify the development requirement, developer could extend the framework in different ways.
 * 
 * ##Namespace
 **/

//cw Component_Persistence_A_Complex_Example: 10
/**
 * ##Description
 * Other than the simple usage of the component persistence, thanks to the flexibility of the component persistence support, developer could handle some very complex situation, take, this example, a small layout management system that dynamically create panes with seperated defined(and most probably persistable by setting the [persistence configuration][cw_ref Component_Persistence_Configuration] already) user control as its content.
 * 
 * ## Assembly
 * To use DependancyPropertyPersistor, you need refer to <b>InfragisticsWPF4.Persistence.v14.1.dll</b> in your project
 * 
 * ##Namespace
 **/
using System;
using System.Collections;
using System.Collections.Generic; 
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Infragistics.Windows.DockManager;
using Infragistics.Windows.Ribbon;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Presentation.Commands; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace PersistenceComponentExamples
{
    public class PersistableViewModule:IModule
    {

        private const string PersistableWindowId1 = "PersistableWindowId1";
        private const string PersistableWindowId2 = "PersistableWindowId2";
        private const string PersistableWindowId3 = "PersistableWindowId3";
        private const string PersistableComponentId1 = "PersistableComponentId1";
        private const string PersistableComponentId2 = "PersistableComponentId2";
        private const string CustomPersistorWindowId1 = "CustomPersistorWindowId1";
        private const string CustomPersistorWindowId2 = "CustomPersistorWindowId2";

        private readonly IChromeManager chromeManager;
        private readonly IChromeRegistry chromeRegistry;
        private readonly IPersistenceService persistenceService;
        private const string ButtonId1 = "ButtonId1";
        private const string ButtonId2 = "ButtonId2";
        private const string ButtonId3 = "ButtonId3";
        private const string ButtonId4 = "ButtonId4";
        private const string ButtonId5 = "ButtonId5";
        private const string ButtonId6 = "ButtonId6";
        private const string ButtonId7 = "ButtonId7";

        private const string SaveStateButtonId = "SaveStateId"; 
        private const string GlobalPersistableRibbonToolId = "GlobalPersistableRibbonToolId";
        private const string LocalPersistableRibbonToolId = "LocalPersistableRibbonToolId";

        public PersistableViewModule(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_, IPersistenceService persistenceService_)
        {
            this.chromeManager = chromeManager_;
            this.chromeRegistry = chromeRegistry_;
            this.persistenceService = persistenceService_;
        }
         
        private IWindowViewContainer container = null;

        //cw Component_Persistence_Persist_Window: 31
        //{
        private PersistenceStorage storageSelected = PersistenceStorage.Global; 
        public void Initialize()
        {
            
            chromeRegistry.RegisterWidgetFactory(SaveStateButtonId, new InitializeWidgetHandler((w_, s_) =>
                {
                    var button = new Button() {Content = "Save State"};
                    button.Click += (sender_, args_) => container.SaveStandaloneState(storageSelected, PersistableWindowId1);
                    w_.Content = button;
                    return true;
                }));

            chromeRegistry.RegisterWindowFactory(PersistableWindowId1, (w_, s_) =>
                {
                    w_.Title = "Explicit Persistable Window";
                    var view = new ExplicitPersistableView(w_, PersistableWindowId1)
                        {
                            DataContext = new PersistableViewModel()
                        };
                    w_.Content = view;
                    container = w_;
                    container.LoadStandaloneState(storageSelected, PersistableWindowId1);
                   //}
                    IWidgetViewContainer dropdown = null;
                    dropdown = w_.Header.AddWidget(Guid.NewGuid().ToString(), new InitialDropdownButtonParameters()
                                                       {
                                                           Text = "Load State From",
                                                           Opening = (sender_, args_) =>
                                                               {
                                                                   MenuTool menuTool = sender_ as MenuTool;
                                                                   if (menuTool != null)
                                                                   {
                                                                       menuTool.ItemsSource = GetProfiles(view);
                                                                   } 
                                                               }
                                                       });
                    dropdown.AddWidget(new InitialButtonParameters()
                        {
                            Text = "Global",
                            Click = (sender_, args_) =>
                                {
                                    view.Storage = storageSelected = PersistenceStorage.Global; 
                                    container.LoadStandaloneState(storageSelected, PersistableWindowId1);
                                }
                        });
                    //cw Component_Persistence_Persist_Window: 32
                    //{
                    w_.Header.AddWidget(SaveStateButtonId, new InitialWidgetParameters());  
                    return true;
                });
            //}
            chromeRegistry.RegisterWidgetFactory(ButtonId1);
            chromeManager.Ribbon["Basic"]["Persistable Window"].AddWidget(ButtonId1,
                                                                            new InitialShowWindowButtonParameters()
                                                                                {
                                                                                    WindowFactoryID = PersistableWindowId1,
                                                                                    Text = "Open Explicit Persistable Window",
                                                                                    InitialParameters = new InitialWindowParameters()
                                                                                        {
                                                                                            Singleton = true,
                                                                                            SizingMethod = SizingMethod.Custom
                                                                                        }
                                                                                });



            //cw Component_Persistence_Overview :30
            /**
             * <li>Register a window with this control as Content and Create it with <b>"Persistable" set to true on the InitialShowWindowButtonParameters</b></li>
             */
            //{
            chromeRegistry.RegisterWindowFactory(PersistableWindowId2 , (w_, s_) =>
                             {
                                w_.Title = "Implicit Profile Specific Persistable Window";  
                                w_.Content = new ImplicitPersistableView(w_) { DataContext = new PersistableViewModel() };
                                 return true;
                             });

            chromeRegistry.RegisterWidgetFactory(ButtonId2);
            chromeManager.Ribbon["Basic"]["Persistable Window"].AddWidget(ButtonId2,
                                                                            new InitialShowWindowButtonParameters()
                                                                            {
                                                                                WindowFactoryID = PersistableWindowId2,
                                                                                Text = "Open Implicit Profile Specific Persistable Window",
                                                                                InitialParameters = new InitialWindowParameters()
                                                                                {
                                                                                    Singleton = false,
                                                                                    Persistable = true
                                                                                }
                                                                            });
            //}

            chromeRegistry.RegisterWindowFactory(PersistableWindowId3, (w_, s_) =>
            {
                w_.Title = "Implicit Profile Specific Persistable Window with DataTemplate";
                w_.Content = new ImplicitPersistableViewWithDataTemplate() { DataContext = new PersistableViewModel() };
                return true;
            });
            chromeManager.Ribbon["Basic"]["Persistable Window"].AddWidget(ButtonId7,
                                                                            new InitialShowWindowButtonParameters()
                                                                            {
                                                                                WindowFactoryID = PersistableWindowId3,
                                                                                Text = "Open Implicit Profile Specific Persistable Window with DataTemplate",
                                                                                InitialParameters = new InitialWindowParameters()
                                                                                {
                                                                                    Singleton = false,
                                                                                    Persistable = true,
                                                                                    DynamicComponentPersistors = true,
                                                                                    PersistorType = typeof(ViewContainerPersistor<VisualTreeElementPersistorEnumerator>)
                                                                                }
                                                                            });
            if (1 == 2)
            {
                //cw Component_Persistence_Persist_Window: 13
                //{
                chromeManager.CreateWindow(PersistableWindowId2, new InitialWindowParameters() { Persistable = true });      
                //}

                //cw Component_Persistence_Persist_Window: 15
                //{
                chromeManager.CreateWindow(PersistableWindowId2, new InitialWindowParameters()
                    {
                        Singleton = false,
                        Persistable = true,
                        DynamicComponentPersistors = true,
                        PersistorType = typeof (ViewContainerPersistor<VisualTreeElementPersistorEnumerator>)
                    });
                //}
            }

            //cw Component_Persistence_Persist_Control: 71
            //{
            chromeRegistry.RegisterGlobalStoredWidgetFactory(GlobalPersistableRibbonToolId, (w_, s_) =>
                { 
                    var filterControl = w_.Content as PersistableFilterControl ?? new PersistableFilterControl(); 
                    filterControl.GetStandaloneComponentPersistor().LoadState(s_);
                    w_.Content = filterControl;
                    return true;
                }, w_ =>
                {
                    PersistableFilterControl filterControl = w_.Content as PersistableFilterControl;
                    return (filterControl != null) ? filterControl.GetStandaloneComponentPersistor().SaveState() : null;
                }); 
            chromeManager.Ribbon["Basic"]["Global Persistable Widget"].AddWidget(GlobalPersistableRibbonToolId, new InitialWidgetParameters());

            chromeRegistry.RegisterWidgetFactory(LocalPersistableRibbonToolId, (w_, s_) =>
                {
                    var filterControl = w_.Content as PersistableFilterControl ?? new PersistableFilterControl();
                    filterControl.GetStandaloneComponentPersistor().LoadState(s_);
                    w_.Content = filterControl;
                    return true;
                }, w_ =>
                {
                    PersistableFilterControl filterControl = w_.Content as PersistableFilterControl;
                    return (filterControl != null) ? filterControl.GetStandaloneComponentPersistor().SaveState() : null; 
                });
            chromeManager.Ribbon["Basic"]["Local Persistable Widget"].AddWidget(LocalPersistableRibbonToolId, new InitialWidgetParameters());
           //}

            chromeRegistry.RegisterWindowFactory(PersistableComponentId1, (w_, s_) =>
            {
                w_.Title = "Standalone Persistable Component";
                StackPanel panel = new StackPanel();
                panel.Orientation = Orientation.Vertical;
                var filterControl1 = new StandalonePersistableFilterControl(persistenceService) { Margin = new Thickness(6) };
                filterControl1.FilterSettingName = "FilterA";
                persistenceService.AddGlobalPersistor(filterControl1.PersistorId, filterControl1.LoadState, filterControl1.SaveState);
                panel.Children.Add(filterControl1);
                var filterControl2 = new StandalonePersistableFilterControl(persistenceService) { Margin = new Thickness(6) };
                filterControl2.FilterSettingName = "FilterB";
                persistenceService.AddGlobalPersistor(filterControl2.PersistorId, filterControl2.LoadState, filterControl2.SaveState);
                panel.Children.Add(filterControl2);
                persistenceService.LoadGlobalItems(new string[] { filterControl1.PersistorId, filterControl2.PersistorId });
                w_.Content = panel;
                EventHandler<WindowEventArgs> closed = null;
                closed = (sender_, args_) =>
                    {
                        persistenceService.RemoveGlobalPersistor(filterControl1.PersistorId);
                        persistenceService.RemoveGlobalPersistor(filterControl2.PersistorId);
                        w_.Closed -= closed;
                        w_.ClosedQuietly -= closed;
                    };
                w_.Closed += closed;
                w_.ClosedQuietly += closed;
                return true;
            });

            chromeRegistry.RegisterWidgetFactory(ButtonId3);
            chromeManager.Ribbon["Advanced"]["Persistable Component"].AddWidget(ButtonId3,
                                                                            new InitialShowWindowButtonParameters()
                                                                            { 
                                                                                WindowFactoryID = PersistableComponentId1,
                                                                                Text = "Standalone Persistable Component",
                                                                                InitialParameters = new InitialWindowParameters()
                                                                                {
                                                                                    Singleton = true, 
                                                                                }
                                                                            });


           chromeRegistry.RegisterWindowFactory(PersistableComponentId2, (w_, s_) =>
            {
                w_.Title = "Custom Persistable Component Storage";
                StackPanel panel = new StackPanel();
                panel.Orientation = Orientation.Vertical;
                var filterControl1 = new StandalonePersistableFilterControl(persistenceService) {Margin = new Thickness(6)};
                filterControl1.FilterSettingName = "FilterC";
                filterControl1.SaveStateWithFilterName = true;
                persistenceService.AddGlobalPersistor(filterControl1.PersistorId, filterControl1.LoadState, filterControl1.SaveState);
                panel.Children.Add(filterControl1);
                var filterControl2 = new StandalonePersistableFilterControl(persistenceService) { Margin = new Thickness(6) };
                filterControl2.FilterSettingName = "FilterD";
                filterControl2.SaveStateWithFilterName = true;
                persistenceService.AddGlobalPersistor(filterControl2.PersistorId, filterControl2.LoadState, filterControl2.SaveState);
                panel.Children.Add(filterControl2);
                persistenceService.LoadGlobalItems(new string[]{filterControl1.PersistorId, filterControl2.PersistorId}); 
                w_.Content = panel;
                EventHandler<WindowEventArgs> closed = null;
                closed = (sender_, args_) =>
                {
                    persistenceService.RemoveGlobalPersistor(filterControl1.PersistorId);
                    persistenceService.RemoveGlobalPersistor(filterControl2.PersistorId);
                    w_.Closed -= closed;
                    w_.ClosedQuietly -= closed;
                };
                w_.Closed += closed;
                w_.ClosedQuietly += closed;
                return true;
            });

            chromeRegistry.RegisterWidgetFactory(ButtonId4);
            chromeManager.Ribbon["Advanced"]["Persistable Component"].AddWidget(ButtonId4,
                                                                            new InitialShowWindowButtonParameters()
                                                                            { 
                                                                                WindowFactoryID = PersistableComponentId2,
                                                                                Text = "Custom Persistable Component Storage",
                                                                                InitialParameters = new InitialWindowParameters()
                                                                                {
                                                                                    Singleton = true, 
                                                                                }
                                                                            });


            chromeRegistry.RegisterWindowFactory(CustomPersistorWindowId1, (w_, s_) =>
            {
                w_.Title = "Custom Persistor Example 1";
                w_.Content = new DynamicChildrenExample();
                return true;
            });

            chromeRegistry.RegisterWidgetFactory(ButtonId5);
            chromeManager.Ribbon["Advanced"]["Custom"].AddWidget(ButtonId5,
                                                                            new InitialShowWindowButtonParameters()
                                                                            {
                                                                                WindowFactoryID = CustomPersistorWindowId1,
                                                                                Text = "Custom Persistor Example 1",
                                                                                //cw Component_Persistence_A_Complex_Example: 52
                                                                                /**
                                                                                 * Set <b>DynamicComponentPersistors</b> to be true on the <b>InitialWindowParameters</b> which invoke this window to force the component persistors to be recollected always
                                                                                 **/
                                                                                //{
                                                                                InitialParameters = new InitialWindowParameters()
                                                                                {
                                                                                    Singleton = true,
                                                                                //}
                                                                                    Persistable = true,
                                                                               //{
                                                                                    DynamicComponentPersistors = true
                                                                                }
                                                                                //}
                                                                            });


            if (1 == 2)
            {   
                //cw Component_Persistence_Extend_Framework: 21
                /** 
                 * This component persistor then can also be registered as a default component persistor for a specific component type using one of the static overloaded <b>RegisterDefaultPersistor</b> methods of <b>PersistenceConfigurator</b>, so that it can be added to the persistence configuration easier by the <b>DefaultConfigurationExtension</b> markup extension
                 **/
                //{
                PersistenceConfigurator.RegisterDefaultPersistor<UniformGrid, ShapeContainerPersistor>();
                //}
            }

            //cw Component_Persistence_A_Complex_Example: 51
            /**
             * Register this XamDockManagerPersistor as default persistor for XamDockManager
             **/
            //{
            PersistenceConfigurator.RegisterDefaultPersistor<XamDockManager, XamDockManagerPersistor>();
            //}
            chromeRegistry.RegisterWindowFactory(CustomPersistorWindowId2, (w_, s_) =>
            {
                w_.Title = "Custom Persistor Example 2"; 
                w_.Content = new CustomPersistorExample();
                return true;
            });

            chromeRegistry.RegisterWidgetFactory(ButtonId6);

            //cw Component_Persistence_Extend_Framework: 40 cw Component_Persistence_A_Complex_Example: 42
            /**
             * This component persistor enumerator can then be specified to override the default enumerator(<b>LogicalTreeElementPersistorEnumerator</b>) by specifying the <b>PersistorType</b> in the <b>InitialWindowParameters</b> as following if the window state is managed by the profile service
             **/
            //{
            chromeManager.Ribbon["Advanced"]["Custom"].AddWidget(ButtonId6,
                                                                            new InitialShowWindowButtonParameters()
                                                                            {
                                                                                WindowFactoryID = CustomPersistorWindowId2,
                                                                                Text = "Custom Persistor Example 2",
                                                                                InitialParameters = new InitialWindowParameters()
                                                                                {
                                                                                    Singleton = true,
                                                                                    Persistable = true,
                                                                                    SizingMethod = SizingMethod.Custom,
                                                                                     DynamicComponentPersistors = true,
                                                                                    Width = 1000,
                                                                                    Height = 800,
                                                                                    PersistorType = typeof(ViewContainerPersistor<XamDockManagerSupportedPersistorEnumerator>), 
                                                                                }
                                                                            });
            //}
            //cw Component_Persistence_Extend_Framework: 41
            /**
             * Or in other cases, use the overloaded extension methods defined in <b>ComponentPersistenceExtensions</b> which accept component enumerator as the generic parameter
             **/
             

       //cw Component_Persistence_Persist_Window: 33
       //{
        }
        //}
        private IEnumerable GetProfiles(ExplicitPersistableView view_)
        {
            List<ToolMenuItem> profiles = new List<ToolMenuItem>();
            profiles.Add(new ToolMenuItem
            {
                Header = "Global",
                IsCheckable = true,
                IsChecked = storageSelected == PersistenceStorage.Global,
                Command = new DelegateCommand<object>(o =>
                    {
                        view_.Storage = storageSelected = PersistenceStorage.Global; 
                        container.LoadStandaloneState(storageSelected, PersistableWindowId1);
                    })

            });
            foreach (var profile in persistenceService.GetProfileNames())
            {
                var profileCopied = profile;
                bool isCurrentProfileName = persistenceService.GetCurrentProfileName() == profileCopied;
                profiles.Add(new ToolMenuItem
                {
                    Header = (isCurrentProfileName ? "Current Profile: " : "Profile: ") + profileCopied,
                    IsCheckable = true,
                    IsChecked = (isCurrentProfileName && storageSelected == PersistenceStorage.CurrentProfile) || 
                    (!isCurrentProfileName && storageSelected.ProfileName == profileCopied),
                    Command = new DelegateCommand<object>(o =>
                        {

                            view_.Storage = storageSelected = isCurrentProfileName
                                                                  ? PersistenceStorage.CurrentProfile
                                                                  : PersistenceStorage.Profile(profileCopied);
                            container.LoadStandaloneState(storageSelected, PersistableWindowId1);

                        })

                });
            }
            return profiles;
        } 
    }
}
