﻿using System; 
using System.Windows;
using System.Windows.Controls;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace PersistenceComponentExamples
{
    /// <summary>
    /// Interaction logic for PersistableView.xaml
    /// </summary>
    public partial class ExplicitPersistableView : UserControl
    {
        private readonly IWindowViewContainer container;
        private readonly string persistorId; 
        private bool loadingState = false;
        public ExplicitPersistableView(IWindowViewContainer container_, string persistorId_)
        {
            InitializeComponent();
            this.Storage = PersistenceStorage.Global;
            gridPersistable.EnableShowHideFields();
            this.container = container_;
            this.persistorId = persistorId_;
            cboFilterNames.Items.Add("Default");
            cboFilterNames.SelectedIndex = -1;
            ComponentPersistenceExtensions.LoadingState += ComponentPersistenceExtensions_LoadingState; 
            ComponentPersistenceExtensions.LoadedState += ComponentPersistenceExtensions_LoadedState;
            ComponentPersistenceExtensions.SavingState += ComponentPersistenceExtensions_SavingState;
            ComponentPersistenceExtensions.SavedState += ComponentPersistenceExtensions_SavedState;
            EventHandler<WindowEventArgs> closed = null;
            closed = (sender_, args_) =>
                {
                    ComponentPersistenceExtensions.LoadingState -= ComponentPersistenceExtensions_LoadingState;
                    ComponentPersistenceExtensions.LoadedState -= ComponentPersistenceExtensions_LoadedState;
                    ComponentPersistenceExtensions.SavingState -= ComponentPersistenceExtensions_SavingState;
                    ComponentPersistenceExtensions.SavedState -= ComponentPersistenceExtensions_SavedState;
                    container_.Closed -= closed;
                    container_.ClosedQuietly -= closed;
                };
            container_.Closed += closed;
            container_.ClosedQuietly += closed;
        }

        void ComponentPersistenceExtensions_SavedState(object sender, ComponentPersistenceExtensions.PersistenceStateEventArgs e)
        {
            if (e.Storage == this.Storage && e.PersistorId == this.persistorId)
            {
                loadingState = false;
            }
        }

        void ComponentPersistenceExtensions_SavingState(object sender, ComponentPersistenceExtensions.PersistenceStateEventArgs e)
        {
            if (e.Storage == this.Storage && e.PersistorId == this.persistorId)
            {
                loadingState = true;
            }
        }

        void ComponentPersistenceExtensions_LoadedState(object sender, ComponentPersistenceExtensions.PersistenceStateEventArgs e)
        {
            if (e.Storage == this.Storage && e.PersistorId == this.persistorId)
            {
                loadingState = false;
            }
        }

        void ComponentPersistenceExtensions_LoadingState(object sender, ComponentPersistenceExtensions.PersistenceStateEventArgs e)
        {
            if (e.Storage == this.Storage && e.PersistorId == this.persistorId)
            {
                loadingState = true;
            }
        }
         

        public PersistenceStorage Storage { get; set; }

        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(ExplicitPersistableView), new UIPropertyMetadata(null));

        public double ContainerWidth
        {
            get { return container.Width; }
            set { container.Width = value; }
        }

        public double ContainerHeight
        {
            get { return container.Height; }
            set { container.Height = value; }
        }

        //cw Component_Persistence_Persist_Control: 41
        /**Code as following:*/
        //{
        private void btnLoadExplicitField_OnClick(object sender_, RoutedEventArgs e_)
        {
            if (loadingState) return;
            container.LoadStandaloneSubState(Storage, persistorId, txtExplicitLoadedPersistableField);

        }
        //}

        //cw Component_Persistence_Persist_Control: 51
        /**Code as following:*/
        //{
        private void btnSaveExplicitField_OnClick(object sender, RoutedEventArgs e)
        {
            container.SaveStandaloneSubState(Storage, persistorId, txtExplicitSavedPersistableField);
        }
        //}

        //cw Component_Persistence_Persist_Control: 61
        /**Code as following:*/
        //{
        private void CboFilterNames_OnSelectionChanged(object sender_, SelectionChangedEventArgs e_)
        {
            if (loadingState) return;
            if (e_.AddedItems.Count == 0) return;
            container.LoadStandaloneSubState(Storage, persistorId, ctlFilter, true, e_.AddedItems[0].ToString());
        } 
         
        private void BtnSaveFilter_OnClick(object sender_, RoutedEventArgs e_)
        {
            container.SaveStandaloneSubState(Storage, persistorId, ctlFilter, string.IsNullOrEmpty(cboFilterNames.Text) ? "Default" : cboFilterNames.Text);
        }

        private void BtnDeleteFilter_OnClick(object sender_, RoutedEventArgs e_)
        {
            container.RemoveStandaloneSubState(Storage, persistorId, ctlFilter, cboFilterNames.Text);
            cboFilterNames.Items.Remove(cboFilterNames.Text);
        }
        //}

        private void BtnSaveFilterAs_OnClick(object sender, RoutedEventArgs e)
        {
            var result = TaskDialog.Show(new TaskDialogOptions()
                {
                    AllowDialogCancellation = true,
                    CommonButtons = TaskDialogCommonButtons.OKCancel,
                    UserInputEnabled = true,
                    Content = "Filter Name:",
                    Title = "Save Filter As"
                });
            if (result.Result == TaskDialogSimpleResult.Ok && !string.IsNullOrEmpty(result.UserInput))
            {
                string filterName = result.UserInput;
                container.SaveStandaloneSubState(Storage, persistorId, ctlFilter, filterName);
                if (!cboFilterNames.Items.Contains(filterName))
                {
                    cboFilterNames.Items.Add(filterName);
                    cboFilterNames.SelectedIndex = cboFilterNames.Items.Count - 1;
                }
                
            }
        }


    }
}
