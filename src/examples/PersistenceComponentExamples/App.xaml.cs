﻿using System.Windows;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Themes;
 
namespace PersistenceComponentExamples
{
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var boot = new Framework();
            boot.AddBlueTheme();
             
            boot.AddModule<PersistableViewModule>();
            boot.AddModule<PersistableViewAPIModule>();
            boot.Start();
        }
    }
}
