﻿using System.Windows;
using System.Windows.Controls;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace PersistenceComponentExamples
{
    /// <summary>
    /// Interaction logic for ImplicitPersistableView.xaml
    /// </summary>
    public partial class ImplicitPersistableView : UserControl
    {
        private readonly IWindowViewContainer container; 

        public ImplicitPersistableView(IWindowViewContainer container_)
        {
            InitializeComponent();
            gridPersistable.EnableShowHideFields();
            container = container_;
            cboFilterNames.Items.Add("Default");
            cboFilterNames.SelectedIndex = -1;
        }

        

        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(ImplicitPersistableView), new UIPropertyMetadata(null));


        //cw Component_Persistence_Persist_Control: 14
        /**Code as following:*/
        //{
        private void btnLoadExplicitField_OnClick(object sender_, RoutedEventArgs e_)
        {
            container.LoadSubState(txtExplicitLoadedPersistableField); 
        }
        //}

        //cw Component_Persistence_Persist_Control: 22
        /**Code as following:*/
        //{
        private void btnSaveExplicitField_OnClick(object sender, RoutedEventArgs e)
        { 
            container.SaveSubState(txtExplicitSavedPersistableField); 
        }
        //}

        //cw Component_Persistence_Persist_Control: 32
        /**Code as following:*/
        //{
        private void CboFilterNames_OnSelectionChanged(object sender_, SelectionChangedEventArgs e_)
        {
            if (e_.AddedItems.Count == 0) return;
            
            container.LoadSubState(ctlFilter, true, e_.AddedItems[0].ToString());
        } 

        private void BtnSaveFilter_OnClick(object sender_, RoutedEventArgs e_)
        {
            container.SaveSubState(ctlFilter, cboFilterNames.Text);
        } 

        private void BtnDeleteFilter_OnClick(object sender_, RoutedEventArgs e_)
        {
            container.RemoveSubState(ctlFilter, cboFilterNames.Text);
            cboFilterNames.Items.Remove(cboFilterNames.Text);
        }
        //}

        private void BtnSaveFilterAs_OnClick(object sender, RoutedEventArgs e)
        {
            var result = TaskDialog.Show(new TaskDialogOptions()
            {
                AllowDialogCancellation = true,
                CommonButtons = TaskDialogCommonButtons.OKCancel,
                UserInputEnabled = true,
                Content = "Filter Name:",
                Title = "Save Filter As"
            });
            if (result.Result == TaskDialogSimpleResult.Ok && !string.IsNullOrEmpty(result.UserInput))
            {
                string filterName = result.UserInput;
                container.SaveSubState(ctlFilter, filterName);
                if (!cboFilterNames.Items.Contains(filterName))
                {
                    cboFilterNames.Items.Add(filterName);
                    cboFilterNames.SelectedIndex = cboFilterNames.Items.Count - 1;
                } 
            }
        }


    }
}
