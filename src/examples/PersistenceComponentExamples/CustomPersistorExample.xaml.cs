﻿using System.Linq; 
using System.Windows;
using System.Windows.Controls; 
using Infragistics.Windows.DockManager;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;  
//cw Component_Persistence_Overview :12 cw Component_Persistence_Persist_Window: 11 cw Component_Persistence_Persist_Control: 11
//{
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
//}

//cw Component_Persistence_A_Complex_Example: 20
/**
 * ##Define the layout window
 * Here we use XamDockManager to manage the layout
 */
namespace PersistenceComponentExamples
{
    /// <summary>
    /// Interaction logic for CustomPersistorExample.xaml
    /// </summary>
    public partial class CustomPersistorExample : UserControl
    {
        //cw Component_Persistence_A_Complex_Example: 60
        /**
         * ##Load the state of dynamic pane content using code 
         * For those dynamically created pane content, since we've turned off the auto load stage(actually we could also set it to be a later than then auto load stage of the XamDockManager in this case, but for a complex ui with multiple level of embedded controls, manual code is more flexible), we need to call the <b>LoadSubState</b> extension method to restore the state of the pane content after the layouter for them (XamDockManager) are restored
         **/
        //{
        public CustomPersistorExample()
        {
            InitializeComponent();
            PersistenceConfigurator.StateLoaded += PersistenceConfigurator_StateLoaded;
        }

        void PersistenceConfigurator_StateLoaded(object sender, PersistenceConfigurator.ComponentPersistenceStateEventArgs e)
        {
            var dockManager = sender as XamDockManager;
            IWindowViewContainer container = this.GetContainer();
            if (dockManager != null && container != null)
            {
                PersistenceConfigurator.StateLoaded -= (PersistenceConfigurator_StateLoaded);
                foreach (var pane in dockManager.GetPanes(PaneNavigationOrder.VisibleOrder))
                {
                    if (pane.Tag != null) //new pane instead of static ones defined in xaml
                    {
                        DependencyObject content = pane.Content as DependencyObject;
                        if (content != null)
                        {
                            container.LoadSubState(content, true);
                        }
                    }
                }
            } 
        }
        //}

        //cw Component_Persistence_A_Complex_Example: 30
        /**
         * ##Code to add new panes
         */
        //{
        private int documentCount = -1;
        private void NewDocumentViewButton_OnClick(object sender_, RoutedEventArgs e_)
        {
            if (documentCount == -1)
            {
                documentCount =
                    XamDockManager.GetDockManager(dockArea)
                                  .GetPanes(PaneNavigationOrder.VisibleOrder)
                                  .Count(p_ => p_.PaneLocation == PaneLocation.Document);
            }
            documentCount++;
            ContentPane p = new ContentPane();
            p.Tag = true; //new pane instead of static ones defined in xaml
            p.Header = "File " + documentCount; 
            p.Name = "DocumentContent" + documentCount;
            //create a simple text box and configure it be be persistable and turn off auto load using code
            TextBox content = new TextBox();
            p.Content = content;
            Configuration config = new Configuration() {PersistenceName = "Content", AutoLoadStage = LoadStage.Later};
            config.Persistors.Add(new DependencyObjectPersistor(){ PropertyNamesToPersist = TextBox.TextProperty.Name});
            PersistenceConfigurator.SetConfiguration(content, config); 
            tabDocuments.Items.Add(p);
        }

        private int toolWindowCount = -1;
        private void NewRightDockedViewButton_OnClick(object sender_, RoutedEventArgs e_)
        {
            AddDockWindow(rightDocker);

        }

        private void NewLeftDockedViewButton_OnClick(object sender_, RoutedEventArgs e_)
        {
            AddDockWindow(leftDocker);
        }

        private void AddDockWindow(SplitPane container_)
        {
            if (toolWindowCount == -1)
            {
                toolWindowCount =
                    XamDockManager.GetDockManager(dockArea)
                                  .GetPanes(PaneNavigationOrder.VisibleOrder)
                                  .Count(p_ => p_.PaneLocation != PaneLocation.Document);
            }
            toolWindowCount++;
            ContentPane p = new ContentPane();
            p.Tag = true; //new pane instead of static ones defined in xaml
            p.Header = "Dockable window " + toolWindowCount;
            p.Name = "DockableContent" + toolWindowCount;
            //create a complex(with persistence configuration) user control and configure it be be persistable and turn off auto load using code
            var control = new PersistableFilterControl();
            p.Content = control;
            PersistenceConfigurator.SetConfiguration(control, new Configuration() { PersistenceName = "FilterSetting", AutoLoadStage = LoadStage.Later });
            container_.Panes.Add(p);
        }
        //}

        //cw Component_Persistence_A_Complex_Example: 70
        /**
         * ##Disable/Enable persistence of persistable user control
         * If the content of the pane is a persistable user control whose persistence configuration is out of control of us, we could turn off it by providing an empty <b>Configuraion</b> and set its <b>SuppressChildrenPersistence</b> property to true (or use the shortcut <b>SupressChildrenPersistenceExtension</b> markup extension)
         **/
        //cw Component_Persistence_A_Complex_Example: 75
        //{
        private void FlagPersist_OnChecked(object sender, RoutedEventArgs e)
        {
            PersistenceConfigurator.GetConfiguration(filterControl).SuppressChildrenPersistence = flagPersist.IsChecked.Value;
        }
        //}
    }
}
