﻿using System; 
using System.Windows;
using System.Windows.Controls; 
using System.Windows.Media; 
using System.Windows.Shapes;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace PersistenceComponentExamples
{
    /// <summary>
    /// Interaction logic for DynamicChildrenExample.xaml
    /// </summary>
    public partial class DynamicChildrenExample : UserControl
    {
        public DynamicChildrenExample()
        {
            InitializeComponent();
        }

        private void BtnCreateShape_OnClick(object sender_, RoutedEventArgs e_)
        {
            if (grdContainer.Rows * grdContainer.Columns == grdContainer.Children.Count)
            {
                if (grdContainer.Columns == 0) grdContainer.Columns = 4;
                grdContainer.Rows += 1;
            }
            int row = grdContainer.Children.Count / grdContainer.Columns;
            int column = grdContainer.Children.Count % grdContainer.Rows;
            //cw Component_Persistence_Configuration: 35
            //{
            Shape shape = GenerateRandomShape();
            Grid.SetColumn(shape, column);
            Grid.SetRow(shape, row);
            grdContainer.Children.Add(shape);
            shape.Name = "Shape" + grdContainer.Children.Count;
            var configuration = new Configuration()
                {
                    AutoLoadStage = LoadStage.AfterLoaded
                };
            configuration.Persistors.Add(new DependencyObjectPersistor());
            PersistenceConfigurator.SetConfiguration(shape,configuration);
            //} 
        }

        private void BtnRemoveShape_OnClick(object sender_, RoutedEventArgs e_)
        {
            if (grdContainer.Children.Count > 0)
            {
                grdContainer.Children.RemoveAt(grdContainer.Children.Count - 1); 
                if (Grid.GetColumn(grdContainer.Children[grdContainer.Children.Count - 1]) == grdContainer.Columns - 1)
                {
                    grdContainer.Rows -= 1;
                }
            }
        }

        private Shape GenerateRandomShape()
        {
            int random = new Random().Next(0, 8);
            int shapeRandom = random % 3;
            int colorRandom = random / 3;
            Shape shape = null;
            if (shapeRandom == 0)
            {
                shape = new Ellipse() { Width = 20, Height = 20 };
            }
            else if (shapeRandom == 1)
            {
                shape = new Rectangle() { Width = 20, Height = 20 };
            }
            else
            {
                shape = new Polygon()
                {
                    Points = new PointCollection(new Point[] { new Point(0, 20), new Point(10, 0), new Point(20, 20) }),
                    
                };

            }
            shape.StrokeThickness = 1;
            shape.Stroke = new SolidColorBrush(Colors.Black);
            if (colorRandom == 0)
            {
                shape.Fill = new SolidColorBrush(Colors.Pink);
            }
            else if (colorRandom == 1)
            {
                shape.Fill = new SolidColorBrush(Colors.BlueViolet);
            }
            else
            {
                shape.Fill = new SolidColorBrush(Colors.Aquamarine);
            }
            return shape;
        }
    }
}
