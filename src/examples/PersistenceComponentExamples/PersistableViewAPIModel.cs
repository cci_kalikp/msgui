﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using MSDesktop.ViewModelAPI;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace PersistenceComponentExamples
{

    public class PersistableViewAPIModel :ViewModelBase, IFrameworkViewModel<PersistableViewAPIParameters> 
    {

        public PersistableViewAPIModel()
        {
            Data = new List<GridItem>();
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { message = value; OnPropertyChanged("Message"); }
        } 

        public List<GridItem> Data { get; set; }

        public void Dispose()
        {
        }

        public void SetState(PersistableViewAPIParameters state_)
        {
            message = state_.Message;
        }

        public PersistableViewAPIParameters GetState()
        {
            return new PersistableViewAPIParameters { Message = message };
        }

        public bool CanClose
        {
            get
            {
                return true;
            }
        }

        public void SetCloseAction(Action close_)
        {
        }

        public void Closed()
        {
        }
         
    }

     
}
