﻿using System.Xml.Linq;
using MSDesktop.ViewModelAPI;

namespace PersistenceComponentExamples
{
    //cw Component_Persistence_Persist_Window: 22
    //{
    public class PersistableViewAPIParameters : IPersistableFrameworkViewParameters
    {
        public string Message { get; set; }

        public int Version { get; set; }

        public IFrameworkViewParameters GetDefault()
        {
            return new PersistableViewAPIParameters();
        }

        public XDocument Upgrade(int version, XDocument xDocument)
        {
            return xDocument;
        }
    }
    //}
}
