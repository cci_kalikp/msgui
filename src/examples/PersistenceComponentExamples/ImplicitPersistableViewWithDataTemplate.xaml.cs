﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PersistenceComponentExamples
{
    /// <summary>
    /// Interaction logic for ImplicitPersistableViewWithDataTemplate.xaml
    /// </summary>
    public partial class ImplicitPersistableViewWithDataTemplate : UserControl
    {
        public ImplicitPersistableViewWithDataTemplate()
        {
            InitializeComponent();
        }
    }
}
