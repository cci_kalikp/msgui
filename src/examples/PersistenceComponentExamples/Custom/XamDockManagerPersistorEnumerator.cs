﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Infragistics.Windows.DockManager; 
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace PersistenceComponentExamples
{
    //cw Component_Persistence_Extend_Framework: 30
    /**
     * ##Create your own Component Persistor Enumerator
     * Developer could either implement the <b>IComponentPersistorEnumerator</b> interface or more easily inherit from the abstract <b>ComponentPersistorEnumeratorBase</b> class(usually to skip persistence of specific controls) or inherit from the default <b>LogicalTreeElementPersistorEnumerator</b> class(usually to add special handling for specific component type) to reorder the persistors to be enumerated or to skip some persistors
     **/
    //cw Component_Persistence_A_Complex_Example: 40
    /**
     * ##Create a XamDockManagerSupportedPersistorEnumerator 
     * To enforce the panes to be visited by when loading/saving persistence according to the visible order
     **/
    //cw Component_Persistence_Extend_Framework: 31 cw Component_Persistence_A_Complex_Example: 41
    //{
    public  class XamDockManagerSupportedPersistorEnumerator : LogicalTreeElementPersistorEnumerator
    {
        protected override void AddChildrenEnumerators(IList<GetChildren> enumerators_)
        {
            //add the GetChildrenPane method before calling the base method, so the special logic of XamDockManager can be executed before using the default logical tree enumeration logic
            enumerators_.Add(GetChildrenPane);
            base.AddChildrenEnumerators(enumerators_);
        }

        //special handling for enumerate children of XamDockManager
        private IEnumerable GetChildrenPane(DependencyObject source_)
        {
            XamDockManager dockManager = source_ as XamDockManager;
            if (dockManager == null) return null;
            return dockManager.GetPanes(PaneNavigationOrder.VisibleOrder);
        } 
    }
    //}
}
