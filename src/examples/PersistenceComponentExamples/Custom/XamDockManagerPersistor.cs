﻿using System; 
using System.Linq; 
using System.Windows;
using System.Xml.Linq;
using Infragistics.Windows.DockManager;
using MorganStanley.MSDotNet.MSGui.Core; 
//cw Component_Persistence_A_Complex_Example: 11
//{
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
//}



namespace PersistenceComponentExamples 
{
    //cw Component_Persistence_A_Complex_Example: 50
    /**
     * ##Create a XamDockManagerPersistor 
     * To save/load the layout of the XamDockManager and also persist the control type and the persistence configuration of the content of the panes created on the fly, which can then be reconstructed and restored when load the state of this XamDockManager
     **/
    //{
    [Serializable]
    public class XamDockManagerPersistor:ComponentPersistorBase
    {
        public override LoadStage AutoLoadStageResolved
        {
            get
            {
                return AutoLoadStage == LoadStage.Default ? LoadStage.AfterShown : AutoLoadStage;
            }
        }

        protected override IComponentPersistor CloneCore()
        {
            return new XamDockManagerPersistor();
        }

        protected override void LoadStateCore(XDocument state_)
        {
            XamDockManager dockManager = component as XamDockManager;
            XElement newPanesEl = state_.Root.Descendants("NewPanes").FirstOrDefault();
            if (newPanesEl != null)
            {
                foreach (var paneEl in newPanesEl.Descendants("Pane"))
                {
                    //create the panes created on the fly previously
                    Type paneType = Type.GetType(paneEl.Attribute("Type").Value);
                    ContentPane pane = paneType.GetConstructor(new Type[0]).Invoke(new object[0]) as ContentPane;
                    if (pane != null)
                    {
                       pane.Header = paneEl.Attribute("Header").Value;
                       pane.Name = paneEl.Attribute("Name").Value;
                        pane.Tag = true;
                    }
                    var componentTypeAttr = paneEl.Attribute("ContentType");
                    if (componentTypeAttr != null)
                    {
                        //construct the content of the pane
                        Type componentType = Type.GetType(componentTypeAttr.Value);
                        FrameworkElement el =
                            componentType.GetConstructor(new Type[0]).Invoke(new object[0]) as FrameworkElement;
                        if (el != null)
                        {
                            el.Name = paneEl.Attribute("ContentName").Value;
                            pane.Content = el;
                        }
                        //set the persistence configuration to the pane content
                        XElement persistenceConfig = paneEl.Descendants().FirstOrDefault();
                        if (persistenceConfig != null)
                        {
                            Configuration config = new Configuration();
                            config.LoadState(new XDocument(persistenceConfig));
                            PersistenceConfigurator.SetConfiguration(el, config);
                        }
                    }
                    var parentNameAttr = paneEl.Attribute("ParentName");
                    if (parentNameAttr == null)
                    {
                        //find whatever split pane and add to it
                        SplitPane s =
                            dockManager.FindLogicalChildren<SplitPane>()
                                                  .FirstOrDefault(s_=>s_.FindLogicalParent<DocumentContentHost>() == null); 
                        if (s != null)
                        {
                            s.Panes.Add(pane);
                        }
                    }
                    else
                    {
                        SplitPane s2 =
                            dockManager.FindLogicalChildren<SplitPane>()
                                                  .FirstOrDefault(s_ => s_.Name == parentNameAttr.Name);
                        if (s2 != null)
                        {
                            s2.Panes.Add(pane);
                            return;
                        }
                        TabGroupPane tabPane =
                            dockManager.FindLogicalChildren<TabGroupPane>()
                                                  .FirstOrDefault(t_ => t_.Name == parentNameAttr.Name);
                        if (tabPane != null)
                        {
                            tabPane.Items.Add(pane);
                        }
                    }

                }
            }
            string layout = state_.ToString();
            dockManager.LoadLayout(layout);

        }

        protected override XDocument SaveStateCore()
        { 
            XamDockManager dockManager = component as XamDockManager;
            string layoutXml = dockManager.SaveLayout();
            layoutXml = layoutXml.Replace("\"NaN\"", "\"100\""); 
            XDocument doc = XDocument.Parse(layoutXml);
            XElement newPanesEl = new XElement("NewPanes");
            foreach (var pane in dockManager.GetPanes(PaneNavigationOrder.VisibleOrder))
            {
                if (pane.Tag != null) //new pane instead of static ones defined in xaml
                {
                    //persist the header and name of the pane
                    XElement newPaneEl = new XElement("Pane");
                    newPaneEl.SetAttributeValue("Type", pane.GetType().AssemblyQualifiedName);
                    newPaneEl.SetAttributeValue("Header", pane.Header.ToString());
                    newPaneEl.SetAttributeValue("Name", pane.Name);
                    FrameworkElement content = pane.Content as FrameworkElement;
                    if (content != null)
                    {
                        //persist the content type and its persistence configuration
                        newPaneEl.SetAttributeValue("ContentType", content.GetType().AssemblyQualifiedName);
                        newPaneEl.SetAttributeValue("ContentName", content.Name); 
                        Configuration configuration = PersistenceConfigurator.GetConfiguration(content);
                        if (configuration != null)
                        {
                            XDocument state = configuration.SaveState();
                            newPaneEl.Add(state.Root);
                        }
                    } 
                    
                    if (pane.PaneLocation != PaneLocation.Floating &&
                        pane.PaneLocation != PaneLocation.FloatingOnly)
                    {
                        newPanesEl.SetAttributeValue("ParentName", (pane.Parent as FrameworkElement).Name);

                    }
                    newPanesEl.Add(newPaneEl);
                }
            }
            doc.Root.Add(newPanesEl);
            return doc;
        }

        protected override string PersistorSuffix
        {
            get { return typeof(XamDockManager).FullName + "." + "Layout"; }
        }

        //only valid for persisting XamDockManager
        protected override bool Validate(object component_)
        {
            return component_ is XamDockManager;
        }
    }
    //}
}
