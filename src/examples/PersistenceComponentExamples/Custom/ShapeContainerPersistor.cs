﻿using System; 
using System.Linq; 
using System.Windows; 
using System.Windows.Controls.Primitives;
using System.Xml.Linq;
//cw Component_Persistence_Extend_Framework: 11
//{
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
//}

namespace PersistenceComponentExamples 
{
    //cw Component_Persistence_Extend_Framework: 20
    /**
     * ##Create your own Component Persistor
     * Developer could either implement the <b>IComponentPersistor</b> interface or more easily inherit from the abstract <b>ComponentPersistorBase</b> class to provide persistence logic. Remember to make the component persistor to be serializable, so that the persistor settings can be serialized when serialize the Configuration which owns it
     **/
    //{
    [Serializable]
    public class ShapeContainerPersistor : ComponentPersistorBase
    {
        //can override this to provide a auto load stage if the auto load stage is specified on the persistor or any parent configuration
        public override LoadStage AutoLoadStageResolved
        {
            get
            {
                return AutoLoadStage == LoadStage.Default ? LoadStage.AfterLoaded : AutoLoadStage;
            }
        }

        //create a copy of this persistor, if there is any properties set, should also be copied
        protected override IComponentPersistor CloneCore()
        {
            return new ShapeContainerPersistor();
        }

        //logic to load the state
        protected override void LoadStateCore(XDocument state_)
        {
            UniformGrid grid = component as UniformGrid;
            var rowsAttr = state_.Root.Attribute("Rows");
            if (rowsAttr != null)
            {
                grid.Rows = Int32.Parse(rowsAttr.Value);
            }
            var columnsAttr = state_.Root.Attribute("Columns");
            if (columnsAttr != null)
            {
                grid.Columns = Int32.Parse(columnsAttr.Value);
            }
            foreach (var element in state_.Root.Descendants("Child"))
            {
                var componentTypeAttr = element.Attribute("ComponentType");
                if (componentTypeAttr != null)
                {
                    Type componentType = Type.GetType(componentTypeAttr.Value);
                    FrameworkElement el = componentType.GetConstructor(new Type[0]).Invoke(new object[0]) as FrameworkElement;
                    if (el != null)
                    {
                        el.Name = element.Attribute("Name").Value;
                        grid.Children.Add(el);
                    }
                    XElement persistenceConfig = element.Descendants().FirstOrDefault();
                    if (persistenceConfig != null)
                    {
                        Configuration config = new Configuration();
                        config.LoadState(new XDocument(persistenceConfig));
                        PersistenceConfigurator.SetConfiguration(el, config);
                    }
                }
            } 
        }

        //logic to save the state
        protected override XDocument SaveStateCore()
        {
            UniformGrid grid = component as UniformGrid;
            XElement root = new XElement("Layout");
            root.SetAttributeValue("Rows", grid.Rows); 
            root.SetAttributeValue("Columns", grid.Columns);
            foreach (FrameworkElement child in grid.Children)
            {
                XElement childEl = new XElement("Child");
                childEl.SetAttributeValue("ComponentType", child.GetType().AssemblyQualifiedName);
                childEl.SetAttributeValue("Name", child.Name);
                Configuration configuration = PersistenceConfigurator.GetConfiguration(child);
                if (configuration != null)
                {
                    XDocument state = configuration.SaveState();
                    childEl.Add(state.Root);
                }
                root.Add(childEl);
            }
            return new XDocument(root);
        }

        //provide a suffix for the state to be persisted by this persistor
        protected override string PersistorSuffix
        {
            get { return typeof(UniformGrid).FullName + "." + "Layout"; }
        }

        //check if the component can be handled by this persistor
        protected override bool Validate(object component_)
        {
            return component_ is UniformGrid;
        }
    }
    //}
}
