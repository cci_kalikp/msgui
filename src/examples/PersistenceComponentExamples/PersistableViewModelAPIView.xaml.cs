﻿using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Impl;

namespace PersistenceComponentExamples
{
    /// <summary>
    /// Interaction logic for PersistableViewModelAPIView.xaml
    /// </summary>
    public partial class PersistableViewModelAPIView : UserControl
    {
        public PersistableViewModelAPIView()
        {
            InitializeComponent();
            gridPersistable.EnableShowHideFields(); 
        }

    }
}
