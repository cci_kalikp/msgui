﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq; 
using System.Windows;
using System.Windows.Controls; 
using System.Xml.Linq;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace PersistenceComponentExamples
{
    /// <summary>
    /// Interaction logic for StandalonePersistableFilterControl.xaml
    /// </summary>
    public partial class StandalonePersistableFilterControl : UserControl, IPersistable
    {
        private readonly IPersistenceService persistenceService;
        public StandalonePersistableFilterControl(IPersistenceService persistenceService_)
        {
            InitializeComponent();
            FilterNames = new ObservableCollection<string>();
            persistenceService = persistenceService_;
            FilterNames = new ObservableCollection<string>() { "Default" };
            filterStates.Add("Default", null);
        }
         

        public bool SaveStateWithFilterName
        {
            get { return (bool)GetValue(SaveStateWithFilterNameProperty); }
            set { SetValue(SaveStateWithFilterNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SaveStateWithFilterName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SaveStateWithFilterNameProperty =
            DependencyProperty.Register("SaveStateWithFilterName", typeof(bool), typeof(StandalonePersistableFilterControl), new UIPropertyMetadata(false));


        public string FilterSettingName
        {
            get { return (string)GetValue(FilterSettingNameProperty); }
            set { SetValue(FilterSettingNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FilterSettingName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FilterSettingNameProperty =
            DependencyProperty.Register("FilterSettingName", typeof(string), typeof(StandalonePersistableFilterControl), new UIPropertyMetadata("Filter"));



        public ObservableCollection<string> FilterNames
        {
            get { return (ObservableCollection<string>)GetValue(FilterNamesProperty); }
            set { SetValue(FilterNamesProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FilterNames.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FilterNamesProperty =
            DependencyProperty.Register("FilterNames", typeof(ObservableCollection<string>), typeof(StandalonePersistableFilterControl), new UIPropertyMetadata(null));

        //cw Component_Persistence_Persist_Control: 73 cw Component_Persistence_Persist_Control: 81
        //{
        private readonly Dictionary<string, XDocument> filterStates = new Dictionary<string, XDocument>();
        private IPersistable persistor;
        private void lstFilterNames_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0) return;
            //}

            if (SaveStateWithFilterName)
            {
                //cw Component_Persistence_Persist_Control: 74
                //{
                if (persistor == null)
                {
                    persistor = ctlFilter.GetStandaloneComponentPersistor(FilterSettingName);
                }
                persistor.LoadState(filterStates[e.AddedItems[0].ToString()]);
                //}
            }
            else
            {
                //cw Component_Persistence_Persist_Control: 82
                //{
                ctlFilter.LoadStandaloneComponentState(PersistenceStorage.Global, FilterSettingName + "." + e.AddedItems[0]);
                //}
            }
            //cw Component_Persistence_Persist_Control: 75 cw Component_Persistence_Persist_Control: 83
            //{
        }
        //}


        //cw Component_Persistence_Persist_Control: 76 cw Component_Persistence_Persist_Control: 91
        //{
        private void btnSaveFilter_Click(object sender, RoutedEventArgs e)
        {
            string filterName = lstFilterNames.Text;
            if (string.IsNullOrEmpty(filterName)) filterName = "Default";
         //}
            if (SaveStateWithFilterName)
            {
                //cw Component_Persistence_Persist_Control: 77
                //{
                if (persistor == null)
                {
                    persistor = ctlFilter.GetStandaloneComponentPersistor(FilterSettingName);
                }
                filterStates[filterName] = persistor.SaveState();
                //}
            }
            else
            {
                //cw Component_Persistence_Persist_Control: 92
                //{
                ctlFilter.SaveStandaloneComponentState(PersistenceStorage.Global, FilterSettingName + "." + filterName);
                //}
            }
            persistenceService.PersistGlobalItems(new string[] { PersistorId });
            //cw Component_Persistence_Persist_Control: 78 cw Component_Persistence_Persist_Control: 93
            //{
        }
        //}

        private void btnSaveFilterAs_Click(object sender, RoutedEventArgs e)
        {
            var result = TaskDialog.Show(new TaskDialogOptions()
            {
                AllowDialogCancellation = true,
                CommonButtons = TaskDialogCommonButtons.OKCancel,
                UserInputEnabled = true,
                Content = "Filter Name:",
                Title = "Save " + FilterSettingName + " As"
            });
            if (result.Result == TaskDialogSimpleResult.Ok && !string.IsNullOrEmpty(result.UserInput))
            {
                string filterName = result.UserInput;
                if (SaveStateWithFilterName)
                {
                    if (persistor == null)
                    {
                        persistor = ctlFilter.GetStandaloneComponentPersistor(FilterSettingName);
                    }
                    filterStates[filterName] = persistor.SaveState(); 
                }
                else
                {
                    ctlFilter.SaveStandaloneComponentState(PersistenceStorage.Global, FilterSettingName + "." + filterName);
                } 

                if (!FilterNames.Contains(filterName))
                {
                    FilterNames.Add(filterName);
                    lstFilterNames.SelectedIndex = FilterNames.Count - 1;
                }
                persistenceService.PersistGlobalItems(new string[] { PersistorId });

            }
        }
 
        public void LoadState(XDocument state_)
        {
            FilterNames.Clear();
            filterStates.Clear();
            foreach (var filter in state_.Root.Descendants("Filter"))
            {
                string name = filter.Attribute("Name").Value;
                if (!string.IsNullOrEmpty(name) && !FilterNames.Contains(name))
                {
                    FilterNames.Add(name);
                    if (SaveStateWithFilterName)
                    {
                        XDocument state = null;
                        XElement filterValueEl = filter.Descendants("Value").FirstOrDefault();
                        if (filterValueEl != null)
                        {
                            state = new XDocument(filterValueEl);
                        }
                        filterStates[name] = state;
                    }
                }
            }
            if (FilterNames.Count == 0)
            { 
                FilterNames.Add("Default");
                filterStates.Add("Default", null);
            }
        }

        public XDocument SaveState()
        {
            var rootEl = new XElement(PersistorId);
            foreach (var filter in FilterNames)
            {
                var filterEl = new XElement("Filter");
                 filterEl.SetAttributeValue("Name", filter);
                if (SaveStateWithFilterName)
                {
                    XDocument state;
                    if (filterStates.TryGetValue(filter, out state))
                    { 
                        if (state != null)
                        {
                            XElement filterValueEl = new XElement("Value");
                            filterValueEl.Add(state.Root);
                            filterEl.Add(filterValueEl);
                        }

                    }
                }
                rootEl.Add(filterEl);
            }
            return new XDocument(rootEl);
        }

        public string PersistorId
        {
            get { return FilterSettingName + ".FilterSettings"; }
        }
         
    }
}
