﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace PersistenceComponentExamples
{
    public class PersistableViewModel:ViewModelBase
    {
        public PersistableViewModel()
        {
            Data = new List<GridItem>();
        }
        private string persistableField;
        public string PersistableField
        {
            get { return persistableField; }
            set { persistableField = value; OnPropertyChanged("PersistableField"); }
        }

        private string explicitLoadedPersistableField;
        public string ExplicitLoadedPersistableField
        {
            get { return explicitLoadedPersistableField; }
            set { explicitLoadedPersistableField = value; OnPropertyChanged("ExplicitLoadedPersistableField"); }
        }

        private string explicitSavedPersistableField;
        public string ExplicitSavedPersistableField
        {
            get { return explicitSavedPersistableField; }
            set { explicitSavedPersistableField = value; OnPropertyChanged("ExplicitSavedPersistableField"); }
        }
        public List<GridItem> Data { get; set; }
    }

    public class GridItem
    {
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
    }
}
