﻿using System;
using System.Windows.Media;
using Microsoft.Practices.Unity;
using MSDesktop.ViewModelAPI; 

namespace PersistenceComponentExamples
{
    //cw Component_Persistence_Persist_Window: 21
    //{
    public class PersistableViewAPIModule : ViewModelBasedModule 
    {
        public PersistableViewAPIModule(IUnityContainer unityContainer_) : base(unityContainer_)
        { 
        }

        public override void Initialize()
        {

            AddRibbonButton<PersistableViewModelAPIView, PersistableViewAPIModel, PersistableViewAPIParameters>(
                "PersistableViewId3",
                "Persistable ViewModelAPI View", "Basic", "Persistable Window", null); 
        }
    }
    //}
}
