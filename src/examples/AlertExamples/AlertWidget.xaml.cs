﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MorganStanley.MSDotNet.MSGui.Core;

namespace AlertExamples
{
    /// <summary>
    /// Interaction logic for AlertWidget.xaml
    /// </summary>
    public partial class AlertWidget : UserControl
    {
        private readonly IAlert _alert;
        public AlertWidget(IAlert alert)
        {
            _alert = alert;
            InitializeComponent();
            AlertLevelsCombobox.ItemsSource = Enum.GetValues(typeof(AlertLevel));
        }

        //cw Alert: 52
        /**
         * Send alert
         */

        private void SendAlert(object sender, EventArgs args)
        {
            //{
            _alert.SubmitAlert(new Alert
                                   {
                                       AlertLevel =(AlertLevel) AlertLevelsCombobox.SelectedValue,  //set alert level
                                       Text = this.AlertMessageBox.Text  //set alert message
                                   }
                );
            //}
        }
    }
}
