﻿using System.Collections.Generic;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;


//cw Alert: 0
/** 
 * ## Description
 * 
 *   * First, define and setup Alert consumers, which will respond to the alerts.
 * 
 *   * Then, send alerts.
 * 
 * 
 * ## Namespaces
 */
//cw Alert: 2 
//{
using MorganStanley.MSDotNet.MSGui.Impl.Alert;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
//}
using MorganStanley.MSDotNet.MSGui.Themes;


namespace AlertExamples
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        //cw Alert: 10
        
        /** ## Context
         */
        //{ 
        protected override void OnStartup(StartupEventArgs e)
        //}
        {
       
            base.OnStartup(e);
            var boot = new Framework();
           
            boot.AddSimpleTheme();
            boot.EnableExceptionHandler("msguiadministrators@ms.com");



            //cw Alert: 11
            /**
              * ## Code
              * #### Add module
              */
            //{
            boot.AddModule<AlertModule>();
            //}

            //cw Alert: 41
            /**
            * #### Add consumers
            */
           
            if (false)
            {
               
                //{
                boot.SetAlertConsumer(new CustomizedAlertConsumer());
                //}
            }
            else
            {
                /**Multiple consumers are also supported*/
                //{
                boot.SetAlertConsumer(
                       new MultipleAlertConsumer(AlertChooser,   //see below
                                                 new AlertConsumerStatusBar(), // MSDesktop predefined consumer
                                                 new AlertConsumerDialogBox(), // MSDesktop predefined consumer
                                                 new CustomizedAlertConsumer() // customized consumer
                                                 ));
                //}

                /** In this case, you can decide how to choose among them.
                 
                 */
               
            }
            

            boot.Start();
        }

        //{
        private IEnumerable<IAlertConsumer> AlertChooser(Alert alert, IList<IAlertConsumer> consumers)
        {
            switch(alert.AlertLevel)
            {
                case AlertLevel.Trivial:
                case AlertLevel.Minor:
                    yield return consumers[0];
                    break;
                case AlertLevel.Medium:
                case AlertLevel.Major:
                    yield return consumers[1];
                    break;
                case AlertLevel.Critical:
                case AlertLevel.Blocker:
                    yield return consumers[0];   // use more than one consumer at once
                    yield return consumers[2];
                    break;

            }
        }
        //}
    }
}
