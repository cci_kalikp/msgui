﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
//cw Alert: 1
//{
using MorganStanley.MSDotNet.MSGui.Core;
//}
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace AlertExamples
{
    public class AlertModule: IModule
    {
        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;


        //cw Alert: 50
        /**
         * ### Send an alert
         * 
         * Get instance from framewrok
         * 
         */
        //{
        private readonly IAlert _alert;
        //}

        //{
        public AlertModule(IChromeManager chromeManager, 
            IChromeRegistry chromeRegistry, IAlert alert)
        //}
        {   
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
            _alert = alert;
        }


       
        public void Initialize()
        {
            _chromeRegistry.RegisterWidgetFactory(@"\\AlertExamples\SendAlert\AlertWidget", CreateAlertSenderWidget);
            _chromeManager.PlaceWidget(@"\\AlertExamples\SendAlert\AlertWidget", "Alert/Sender", null);

        }

        private bool CreateAlertSenderWidget(IWidgetViewContainer widget, XDocument state)
        {
            widget.Content = new AlertWidget(_alert);
            return true;
        }

        private bool CreateConsumerWidget(IWidgetViewContainer widget, XDocument state)
        {   
            widget.Content = new AlertWidget(_alert);
            return true;
        }
    }
}
