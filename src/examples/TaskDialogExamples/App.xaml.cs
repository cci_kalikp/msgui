﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;


using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Themes;
//cw TaskDialogs_Introduction :5 cw TaskDialogs_Advanced_Customization :5 cw TaskDialogs_Progress_Bar :5
/**##Namespaces*/
//{
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
//}

namespace TaskDialogExamples
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var boot = new Framework();
            boot.AddSimpleTheme();
            // ...
            boot.Start();

            //cw TaskDialogs_Introduction : 10
            /**##Description*/
            /**Task dialogs are a more advanced version of native Message Boxes. You are encouraged to use them instead of
             * `MessageBox.Show()` method.*/
            /**##Code*/
            /**Show a simple message*/
            //{
            TaskDialog.ShowMessage("Hello");
            //}
            /**Show a task dialog with buttons, icon and customized message and caption*/
            //{
            var result = TaskDialog.ShowMessage("Message text", "Caption", TaskDialogCommonButtons.OKCancel, VistaTaskDialogIcon.Warning);
            //}
            /**Process result*/
            //{
            if (result == TaskDialogSimpleResult.Cancel)
            {
                //user cancelled the dialog
            }
            //}
            /**It is important for TaskDialogs to always have an owner window. By default, the owner becomes the currently
             * active window (at the moment of displaying the dialog). You can specify the owner by passing the last parameter
             * of the `TaskDialog.ShowMessage` method family. If null is specified, the default behaviour is applied.*/
            //{
            Window someWindow = null;
            TaskDialog.ShowMessage("Hello", someWindow);
            //}

            //cw TaskDialogs_Advanced_Customization : 10
            /**##Description*/
            /**Task dialogs can be configured by passing a `TaskDialogOptions`*/
            /**##Code*/
            /**Prepare a dictionary mapping custom buttons names to actions. If you provide an action for
               a button, clicking it will not cause the dialog to close.*/
            //{
            var actions = new Dictionary<string, Action>();
            actions.Add("add", () => { Console.WriteLine("Add clicked"); });
            //}
            /**Show the dialog*/
            //{
            var showResult = TaskDialog.Show(new TaskDialogOptions
            {
                // setup custom buttons
                CustomButtons = new string[] { "add", "remove" },
                // provide code to be executed when custom buttons are clicked
                CustomButtonActions = actions,
                // enable text input
                UserInputEnabled = true,
            });
            //}
            /**Process user input*/
            //{
            Console.WriteLine(showResult.UserInput);
            //}
            
            //cw TaskDialogs_Progress_Bar : 10
            /**##Description*/
            /**Task dialogs can display and control a progress bar.*/
            /**##Code*/
            /**Create a handle object that will allow you to control the bar asynchronously*/
            //{
            var handle = new TaskDialogHandle();
            //}
            /**Prepare an action mapping dictionary. Now you can control the progress bar using the handle in your actions*/
            //{
            var tdActions = new Dictionary<string, Action>();
            tdActions.Add("add", () => { handle.ProgressBarCurrentValue++; });
            //}
            /**Display the dialog passing the handle*/
            //{
            TaskDialog.Show(new TaskDialogOptions
            {
                ProgressBarInitialValue = 0,
                ProgressBarMinimum = 0,
                ProgressBarMaximum = 100,
                CustomButtonActions = tdActions,
                CustomButtons = new string[] { "add", "close" }
            }, handle);
            //}
        }
    }
}
