﻿//cw Navigation_Bar : 10
/**##Description*/
/**
 * MSDesktop supports the new NavigationBar area docked to the left side of the window in ribbon shell mode. Widgets like outlook bar or tree view can then be added to it to give a navigation view.
 */
/**##Outlook Bar*/
/**
 * A native InitialOutlookBarGroupParameters is provided for adding outlookbar to this navigation area to make it easy to build a traditional office outlook view.
 */
/**##Assembly*/
/**
* If SmartApi is not enabled, you need to add a reference to InfragisticsWPF4.OutlookBar.v14.1 assembly if outlook bar is needed. 
*/

using System;
using System.Windows; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions; 
using MorganStanley.MSDotNet.MSGui.Themes;

namespace NavigationBarExample
{
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var boot = new Framework();
            //cw Navigation_Bar : 20
            /**##Code*/
            /**If the default infragistics outlook bar theme calculated based on theming doesn't qualify the requirement, either a brand new theme can be created to provide all styles of the controls or styles created for the outlook bar control can be merged to an innate theme (remember to change the theme name to suppress the default outlookbar theming)*/
            //{
            var theme = ThemeCreator.CreateBlackTheme();
            theme.Name = "MetroDark";
            theme.MergedDictionaries.Add(
     new ResourceDictionary
     {
         Source = new Uri("/NavigationBarExample;component/Theme/MetroDarkTheme.xaml",
             UriKind.RelativeOrAbsolute)
     }); 
            boot.AddTheme(theme);
            //}

            //cw Navigation_Bar : 21
            /**Hide the ribbon or other elements if needed*/
            //{
            boot.HideOrLockShellElements(HideLockUIGranularity.LockedLayoutMode);
            //}
            boot.SetLayoutLoadStrategy(LayoutLoadStrategy.NeverAsk);
            boot.EnableRemoveEmptyTabs(true, true);
            boot.EnableFlatMode();
            boot.AddModule<MainModule>();
            boot.AddModule<FunctionModule1>(); 
            boot.AddModule<FunctionModule2>(); 
            boot.Start();
        }
    }
}
