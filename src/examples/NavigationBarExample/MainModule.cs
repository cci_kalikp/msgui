﻿ 
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Infragistics.Windows.Controls;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using NavigationBarExample.View;

namespace NavigationBarExample
{
   
    public class MainModule:IModule
    {
        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;
        public const string OutlookBarArea = "OutlookBar";
        public MainModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_)
        {
            chromeRegistry = chromeRegistry_;
            chromeManager = chromeManager_;
        }


        public void Initialize()
        {  
             
            var mainWindow = Application.Current.Windows.Cast<object>().FirstOrDefault(
                x => x.GetType().Name == "ShellWindow") as Window;
            if (mainWindow != null)
            {
                //foreach (var panel in mainWindow.FindLogicalChildren<DockPanel>())
                //{
                //    if (panel.Name == "TopContentArea")
                //    { 
                //        panel.LastChildFill = false;
                //        panel.Children.Add(new Label() { Content = "Filter:", VerticalAlignment = VerticalAlignment.Center });
                //        panel.Children.Add(new TextBox() {Width = 300, VerticalAlignment = VerticalAlignment.Center});
                //        break;
                //    }
                //}
                //find the main tabwell and do some customization here
                foreach (var tab in mainWindow.FindLogicalChildren<CustomXamTabControl>())
                {
                    tab.TabItemCloseButtonVisibility = TabItemCloseButtonVisibility.Visible;
                    break;
                }
            }

            //cw Navigation_Bar : 22
            /**Adding a widget to the NavigationBar area enables the navigation bar*/
            /**If InitialOutlookBarParameters is provided as the initial widget parameter, outlook bar can be created automatically*/
            //{
           IOutlookBar  bar = chromeManager[ChromeArea.NavigationBar].AddWidget(OutlookBarArea, new InitialOutlookBarParameters()
                { 
                    Width = 200
                }) as IOutlookBar;
            //cast the IWidgetViewContainer to IOutlookBar for some outlook bar specific ui logic 
           bar.ActiveGroupChanged += bar_ActiveGroupChanged; 
            //}

           //cw Top_Area : 10
           /**##Description*/
           /**
            * MSDesktop supports the new TopContent area docked to the top of window below the ribbon in ribbon shell mode. Widgets can then be added to it to enable this area.
            */
            /**##Code*/
            //{
         var buttonContainer =  chromeManager[ChromeArea.TopContent].AddWidget(
   new InitialButtonParameters() { Text = "Toggle Navigation Bar", 
                                   IsCheckable = true,
                                   Click = (sender_, args_) => { bar.IsMinimized = !bar.IsMinimized; }
   });
            //}
         ContentControl button = buttonContainer.Content as ContentControl;
            if (button == null)
            {
                buttonContainer.ContentReady += (sender_, args_) =>
                    {
                        button = buttonContainer.Content as ContentControl; 
                        button.VerticalAlignment = VerticalAlignment.Center; 
                        button.Margin = new Thickness(5);
                        button.Padding = new Thickness(3);
                    };
            }
            else
            {
                button.HorizontalAlignment = HorizontalAlignment.Center;
                button.Margin = new Thickness(5);
                button.Padding = new Thickness(3);
            }
            //{
            //the state of the widget added to the top area can also be persisted
           chromeRegistry.RegisterGlobalStoredWidgetFactory("ctlFilter", (w_, s_) =>
           {
               var filterControl = w_.Content as PersistableFilterControl ?? new PersistableFilterControl();
               filterControl.GetStandaloneComponentPersistor().LoadState(s_);
               w_.Content = filterControl;
               return true;
           }, w_ =>
           {
               PersistableFilterControl filterControl = w_.Content as PersistableFilterControl;
               return (filterControl != null) ? filterControl.GetStandaloneComponentPersistor().SaveState() : null;
           });  
            chromeManager[ChromeArea.TopContent].AddWidget("ctlFilter", new InitialWidgetParameters());
            //}

        }
         

        void bar_ActiveGroupChanged(object sender, ActiveGroupChangedEventArgs e)
        { 
            chromeManager.SetMainWindowTitle("NavigationBarExample - " + e.New, false);
        }
    }
}
