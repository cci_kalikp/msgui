﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using NavigationBarExample.View;
using NavigationBarExample.ViewModel;

namespace NavigationBarExample
{
    class FunctionModule1:IModule
    {
        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;

        public FunctionModule1(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_)
        {
            chromeRegistry = chromeRegistry_;
            chromeManager = chromeManager_;
        }


        public void Initialize()
        {
            //cw Navigation_Bar : 23 
            /**Then outlook bar group can be added under the outlook bar created*/
            //{
           var group = chromeManager[ChromeArea.NavigationBar][MainModule.OutlookBarArea].AddWidget(new InitialOutlookBarGroupParameters()
                {
                    Text = "Group1",
                    //LargeImage =
                    //SmallImage = 
                });
            //}

           //cw Navigation_Bar : 24 
           /**User controls or other contents can then be added as the content of this outlook bar group*/
           //{
            chromeRegistry.RegisterWidgetFactory("Group1Content", (container_, state_) =>
                { 
                    var model = TreeViewModel.GetExample("Group1", chromeManager, chromeRegistry);
                    var control = new GroupContent(chromeManager);
                    control.DataContext = model;
                    container_.Content = control;
                    return true;
                });
            group.AddWidget("Group1Content", new InitialWidgetParameters());
            //}
        }
    }
     
}
