﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel; 

namespace NavigationBarExample.ViewModel
{
    public class TreeViewModel:ViewModelBase
    { 
        public TreeViewModel()
        {
            Children = new List<NodeViewModel>(); 
          
        }
        public static TreeViewModel GetExample(string groupName_, IChromeManager manager_, IChromeRegistry registry_)
        {
            TreeViewModel tree = new TreeViewModel();
            for (int i = 0; i < 4; i++)
            {
                NodeViewModel node = new NodeViewModel(manager_) {Text = "Root " + (i + 1)};
                tree.Children.Add(node);
                for (int j = 0; j < 4; j++)
                {
                    int viewIndex = j + 1;
                    NodeViewModel child = new NodeViewModel(manager_) { Text = "Node " + viewIndex, ViewId = "View" + groupName_ + (i + 1) + viewIndex, Parent = node };
                    registry_.RegisterWindowFactory(child.ViewId, (container_, state_) =>
                        {
                            Type controlType =
                                Type.GetType("NavigationBarExample.View.ViewContent" + viewIndex +
                                             ", NavigationBarExample");
                            container_.Content = Activator.CreateInstance(controlType);
                            container_.Title = child.Text;
                            return true;
                        });
                    node.Children.Add(child);
                }
            } 
            return tree;
        }
         
        public List<NodeViewModel> Children { get; set; }

        private SearchBoxViewModel searchBox;
        public SearchBoxViewModel SearchBox
        {
            get { return searchBox ?? (searchBox = new SearchBoxViewModel(this)); }
        }

        public ICommand OpenNodeCommand { get; private set; }
    }
}
