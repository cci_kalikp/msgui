﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace NavigationBarExample.ViewModel
{
    public class ViewHeaderViewModel:ViewModelBase
    {
        public ViewHeaderViewModel(NodeViewModel node_)
        {
            MainTitle = node_.Parent.Text;
            SubTitle = node_.Text;
        }

        public string MainTitle { get; set; }

        public string SubTitle { get; set; }
    }
}
