﻿using System;
using MorganStanley.MSDotNet.MSGui.Controls.SearchBox;

namespace NavigationBarExample.ViewModel
{
    public class SearchResultItem:SearchResultItemBase
    {
        public override string ItemPath
        {
            get { return itemPath; }
        }

        public NodeViewModel Node { get; private set; }
        private readonly string itemPath;
        public SearchResultItem(NodeViewModel node_)
        {
            Node = node_;
            itemPath = Node.Text;
            var parent = Node.Parent;
            while (parent != null)
            {
                itemPath = parent.Text + " → " + itemPath;
                parent = parent.Parent;
            }
        } 
    }
     
}
