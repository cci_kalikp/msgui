﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 
using MorganStanley.MSDotNet.MSGui.Core.ViewModel; 

namespace NavigationBarExample.ViewModel
{
    public class NodeViewModel:ViewModelBase
    {
        private readonly IChromeManager manager;
        public NodeViewModel(IChromeManager manager_)
        {
            Children = new List<NodeViewModel>();
            manager = manager_;
            OpenCommand = new DelegateCommand(_ =>
            {
                if (string.IsNullOrEmpty(ViewId)) return;
                var view = manager.Windows.FirstOrDefault(v_ => v_.FactoryID == ViewId);
                if (view != null)
                {
                    view.Activate();
                }
                else
                {
                    if (!string.IsNullOrEmpty(Text))
                    {
                        var tab = manager.Tabwell.Tabs.FirstOrDefault(t_ => t_.Title == Text);
                        if (tab != null)
                        {
                            tab.Activate();
                            manager_.CreateWindow(ViewId, new InitialWindowParameters()
                                {
                                    InitialLocation = InitialLocation.DockTabbed,
                                    InitialLocationTarget = tab.Windows[0],
                                    IsHeaderVisible = false
                                });
                        }
                        else
                        {
                            manager_.Tabwell.AddTab(Text, new ViewHeaderViewModel(this));
                            manager.CreateWindow(ViewId, new InitialWindowParameters()
                            {
                                InitialLocation = InitialLocation.DockInActiveTab | InitialLocation.DockTop,
                                IsHeaderVisible = false
                            });
                        }
                    } 

                }
            });
        }
        public string Text { get; set; }

        public string ViewId { get; set; }
         

        public List<NodeViewModel> Children { get; set; }
        public NodeViewModel Parent { get; set; }

        public ICommand OpenCommand { get; private set; }
    }
}
