﻿using System.Collections.Generic;
using System.Threading;
using MorganStanley.MSDotNet.MSGui.Controls.SearchBox;

namespace NavigationBarExample.ViewModel
{
    public class SearchBoxViewModel: SearchBoxViewModelBase
    {
        private List<NodeViewModel> nodes;
        public SearchBoxViewModel(TreeViewModel treeView_)
        {
            this.nodes = treeView_.Children;
            this.WatermarkText = "Search Views"; 
        }

        public override bool EnableTypeaheadDropDown
        {
            get { return true; } 
        }

        protected override void  UpdateSearchResultCore(CancellationToken ct_) 
        {
             string searchText = SearchText.Trim().ToLower();

            if (string.IsNullOrWhiteSpace(searchText))
            {
                SearchResultItems.Clear();
                SelectedItem = null;
                ShowDropDown = false;
                return;
            } 
            if (!AsyncSearch && !string.IsNullOrEmpty(lastResultSearchText) &&
                SearchResultItems.Count == 0 &&
                searchText.Contains(lastResultSearchText.ToLower()))
            {
                return;
            }
            List<ISearchResultItem> result = new List<ISearchResultItem>();
            foreach (var node in nodes)
            {
                SearchNode(node, result, searchText);
            } 

            ShowDropDown = result.Count > 0;
            this.SearchResultItems = result;
            base.OnPropertyChanged("SearchResultItems");
        }

        private void SearchNode(NodeViewModel node_, List<ISearchResultItem> result_, string searchText_)
        {
            if (!string.IsNullOrEmpty(node_.ViewId) && node_.Text.ToLower().Contains(searchText_))
            {
                result_.Add(new SearchResultItem(node_){SearchText = searchText_});
            }
            foreach (var child in node_.Children)
            {
                SearchNode(child, result_, searchText_);
            }
        }

        protected override void OnPropertyChanged(string propertyName_)
        {
            if (propertyName_ == "SelectedItem")
            {
                var selectedNode = SelectedItem as SearchResultItem;
                if (selectedNode != null)
                {
                    selectedNode.Node.OpenCommand.Execute(null);
                }
            }
            base.OnPropertyChanged(propertyName_);
        }
    }
}
