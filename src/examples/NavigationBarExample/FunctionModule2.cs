﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using NavigationBarExample.View;
using NavigationBarExample.ViewModel;

namespace NavigationBarExample
{
    class FunctionModule2:IModule
    {
        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;

        public FunctionModule2(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_)
        {
            chromeRegistry = chromeRegistry_;
            chromeManager = chromeManager_;
        }


        public void Initialize()
        {
           var group = chromeManager[ChromeArea.NavigationBar][MainModule.OutlookBarArea].AddWidget(new InitialOutlookBarGroupParameters()
                {
                    Text = "Group2",
                    //LargeImage =
                    //SmallImage = 
                });

            chromeRegistry.RegisterWidgetFactory("Group2Content", (container_, state_) =>
                {

                    var model = TreeViewModel.GetExample("Group2", chromeManager, chromeRegistry);
                    var control = new GroupContent(chromeManager);
                    control.DataContext = model;
                    container_.Content = control;
                    return true;
                });
            group.AddWidget("Group2Content", new InitialWidgetParameters());
        }
    } 
}
