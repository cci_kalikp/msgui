﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using NavigationBarExample.ViewModel;

namespace NavigationBarExample.View
{
    /// <summary>
    /// Interaction logic for GroupContent.xaml
    /// </summary>
    public partial class GroupContent : UserControl
    {
        private readonly IChromeManager manager;
        public GroupContent(IChromeManager manager_)
        {
            InitializeComponent();
            manager = manager_;

        }
         
        private void XamDataTree_OnActiveNodeChanged(object sender, Infragistics.Controls.Menus.ActiveNodeChangedEventArgs e)
        {
            var current = e.NewActiveTreeNode;
            if (current != null)
            {
                NodeViewModel node = current.Data as NodeViewModel;
                if (node != null)
                {
                   node.OpenCommand.Execute(null);
                }
            }
        }
    }
}
