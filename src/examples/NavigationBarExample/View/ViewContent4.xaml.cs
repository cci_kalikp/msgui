﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NavigationBarExample.View
{
    /// <summary>
    /// Interaction logic for ViewContent4.xaml
    /// </summary>
    public partial class ViewContent4 : UserControl
    {
        public ViewContent4()
        {
            InitializeComponent();
        }
    }
}
