﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls; 

namespace NavigationBarExample.View
{
    /// <summary>
    /// Interaction logic for PersistableFilterControl.xaml
    /// </summary>
    public partial class PersistableFilterControl : UserControl
    {
        public PersistableFilterControl()
        {
            InitializeComponent();
        }

        public bool StandaloneState { get; set; }


        public string FilterName
        {
            get { return (string)GetValue(FilterNameProperty); }
            set { SetValue(FilterNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FilterName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FilterNameProperty =
            DependencyProperty.Register("FilterName", typeof(string), typeof(PersistableFilterControl), new UIPropertyMetadata("Filter"));

    }
}
