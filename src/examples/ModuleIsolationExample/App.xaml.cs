﻿//cw Module_Isolation_Introduction :10
/**##Description*/
/**
 * This example demostrates how to configure isolated module. 
 * Modules themselves don't need to be designed specifically for isolation scenarios.
 */

using System;
using System.Windows;
using Microsoft.Practices.Composite.Modularity;
using ModuleIsolationExample.Options;
using ModuleIsolationExample.View2ViewCommunication;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl;

/**##Namespaces*/
/** Use the following namespace to enable Kraken communications in isolated modules scenarios: */
//{
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
//}
/** Use the following namespace to enable setting up an isolated wrapper for a module:*/
//{
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
//}
/**Use the following namespace to enable view-to-view communication configuration:*/
//{
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
//}

using MorganStanley.MSDotNet.MSGui.Themes;

namespace ModuleIsolationExample
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            /**##Instantiate the framework*/
            /**Create the Framework instance and apply a theme.*/
            //{
            var boot = new Framework();
            boot.AddSimpleTheme();
            //}

            /**##Initial Setup*/
            /** Before starting the framework, enable and configure Kraken communication. */
            //{
            boot.EnableMessageRoutingCommunication();
            boot.EnableMessageRoutingDefaultGui();
            boot.EnableMessageRoutingHistory();
            boot.EnableMessageRoutingHistoryGui();
            //}

            boot.EnableProcessIsolation();

            /**This example doesn't need CPS communication, but does need inter-process communication channel/
                We specify it as Optional - e.g. if it can be initialized, it will be used, otherwise we are still OK to prceed.*/
            //{
            boot.SetImcCommunicationStatus(CommunicationChannelStatus.Optional);
            //}

            /**##Non Isolated modules*/
            /** You can add your non-isolated modules in usual manner.*/
            //{
            // boot.AddModule<FlashPlayerBasedFlexModule>();
            //}

            // boot.AddModule<ManagingModule>();
            boot.AddModule<V2VConfigModule>();
            boot.AddModule<CrossProcessWindowModule>();
            // boot.AddModule<V2VPublisherModule>();
            // boot.AddModule<V2VSubscriberModule>();

            var isolationOptions = new ProcessIsolationOptions
                {
                    RespawnEnabled = true, 
                    // AdditionalCommandLine = "/debug",
                };

            /**##Isolated Modules Setup*/
            //{
            boot.SetUpProcessIsolatedWrapper<V2VSubscriberModule>(isolationOptions);
            boot.SetUpProcessIsolatedWrapper<IsolatedOptionsModule>(/*additionalCommandLine: "/debug"*/);
            boot.SetUpProcessIsolatedWrapper<IsolatedModule>(/*additionalCommandLine: "/debug"*/);
            //}

            /**##Start the Framework*/
            //{
            boot.Start();
            //}

            // boot.SetUp32BitProcessIsolatedWrapperFor<IsolatedModule>();
            // boot.SetUpProcessIsolatedWrapper<FlashPlayerBasedFlexModule>(IsolatedProcessPlatform.X86);

            /**##Isolated Modules Setup - after Framework.Start()*/
            /**You can as well set up your isolated modules after Start.*/
            //{
            var options = new ProcessIsolationOptions {RespawnEnabled = true};
            options.RespawnHandler.ProcessCrashed += (sender, args) =>
                {
                    var result = TaskDialog.ShowMessage("Subsystem process crashed. Do you want to restart it?", 
                        "Subsystem crashed", TaskDialogCommonButtons.YesNo,
                                           VistaTaskDialogIcon.Error);
                    if (result == TaskDialogSimpleResult.Yes)
                    {
                        args.Respawn = true;
                    }
                };
            boot.SetUpProcessIsolatedWrapper<V2VPublisherModule>(options);
            //}
        }
    }
}
