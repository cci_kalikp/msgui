﻿using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace ModuleIsolationExample
{
    internal sealed class CrossProcessWindowModule : IModule
    {
        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;

        public CrossProcessWindowModule(IChromeManager chromeManager, IChromeRegistry chromeRegistry)
        {
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
        }

        public void Initialize()
        {
            const string buttonFactory = "CrossProcessButtonFactory";

            _chromeRegistry.RegisterWidgetFactory(buttonFactory);

            var initialParameters = new InitialShowWindowButtonParameters
                {
                    Text = "Cross-Process Button",
                    Size = ButtonSize.Large,
                    WindowFactoryID = IsolatedModule.CrossProcessWindowFactoryId,
                    InitialParameters = new InitialWindowParameters
                        {
                            InitialLocation = InitialLocation.DockRight,
                            Title = "Cross-process view title",
                        }
                };

            _chromeManager.PlaceWidget(buttonFactory, "How/Where", initialParameters);
        }
    }
}
