﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Options;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace ModuleIsolationExample.Options
{
    /// <summary>
    /// Interaction logic for ExampleSettingWithView.xaml
    /// </summary>
    
    public partial class ExampleSettingWithView : UserControl, IValidatableOptionView, ISearchablePage, ISubItemAwareSeachablePage
   
    {
       
        public ExampleSettingWithView(IPersistenceService service_)
        {
            InitializeComponent();
            service_.AddGlobalPersistor("OptionsExample.Settings", LoadState, SaveState);
           
            this.DataContext = Settings; 
         
        }
       

        private Settings oldSettings;
       
        private Settings settings;
        public Settings Settings
        {
            get
            {
                if (settings == null)
                {
                    settings = new Settings();
                    settings.Add(new Pair("Setting1", "Value1"));
                    oldSettings = settings.Clone();
                }
                return settings;
            }
            set { settings = value; }
        }
       

        public XDocument SaveState()
        {
            return PersistenceExtensions.Store(Settings);
        }

        public void LoadState(XDocument state_)
        {
            var loaded = PersistenceExtensions.Restore<Settings>(state_);
            this.DataContext = Settings = loaded;
            oldSettings = settings.Clone();
        }

     
        public bool OnValidate()
        {
            if (Settings.Count == 0)
            {
                TaskDialog.ShowMessage("Must specify some settings",
                       "Invalid settings", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
                return false;
            }
            else if (Settings.Count > 5)
            {
                TaskDialog.ShowMessage("Too many some settings",
       "Invalid settings", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
                return false;
            }
            return true;
        }
       


       
        public void OnInvalid()
        {
            this.grid.Highlight();
        }
       
        public void OnOK()
        {
            PersistenceExtensions.Store(settings);
        }
        
        public void OnCancel()
        {
            if (oldSettings != null)
            {
                this.DataContext = Settings = oldSettings.Clone();         
            }
            else
            {
                settings = null;
                this.DataContext = Settings;
            }
        }
       
        public void OnDisplay()
        { 
        }
        
        public void OnHelp()
        { 
        }
         
        public new UIElement Content
        {
            get { return this; }
        }
       
        public int GetMatchCount(string textToSearch_)
        {
            return Settings.Count(setting_ => (setting_.Key ?? string.Empty).ToLower().Contains(textToSearch_.ToLower()) || 
                (setting_.Value ?? string.Empty).ToLower().Contains(textToSearch_.ToLower()));
        }
      
        public IList<KeyValuePair<string, object>> GetMatchedItems(string textToSearch_)
        {
            var matchedItems = new List<KeyValuePair<string, object>>();
            foreach (var pair2 in Settings)
            {
                if ((pair2.Key ?? string.Empty).ToLower().Contains(textToSearch_.ToLower()) ||
                (pair2.Value ?? string.Empty).ToLower().Contains(textToSearch_.ToLower()))
                {
                    matchedItems.Add(new KeyValuePair<string, object>(pair2.Key, pair2.Key));
                }
            }
            return matchedItems;

        }

        public void LocateItem(object item_)
        {
            string key = item_ as string;
            if (key != null)
            {
                foreach (var pair2 in Settings)
                {
                    if (pair2.Key == key)
                    {
                        grid.ActiveDataItem = pair2;
                        break;
                    }
                }
            }
        }
        


    }

    [Serializable]
    public class Settings : ObservableCollection<Pair>, ICloneable
    {

        public Settings Clone()
        {
            Settings s = new Settings();
            foreach (var setting in this)
            {
                s.Add(new Pair(setting.Key, setting.Value));
            }
            return s;
        }



        object ICloneable.Clone()
        {
            return Clone();
        }
    }

    [Serializable]
    public class Pair : ViewModelBase
    {
        public Pair (){}
        public Pair(string key_, string value_)
        {
            key = key_;
            _value = value_;
        }

        private string key;
        public string Key
        {
            get { return key; }
            set { key = value; OnPropertyChanged("Key"); }
        }

        private string _value;
        public string Value
        {
            get { return _value; }
            set { _value = value; OnPropertyChanged("Value"); }
        }
    }
}
