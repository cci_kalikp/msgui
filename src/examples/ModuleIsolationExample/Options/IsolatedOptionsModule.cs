﻿using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;

namespace ModuleIsolationExample.Options
{
    internal sealed class IsolatedOptionsModule : IModule
    {
        private readonly IApplicationOptions _options;
        private readonly IPersistenceService _persistenceService;

        public IsolatedOptionsModule(IApplicationOptions options, IPersistenceService persistenceService)
        {
            _persistenceService = persistenceService;
            _options = options;
        }

        public void Initialize()
        {
            _options.AddOptionPage(null, "General Settings", new ExampleSettingWithView(_persistenceService));
        }
    }
}
