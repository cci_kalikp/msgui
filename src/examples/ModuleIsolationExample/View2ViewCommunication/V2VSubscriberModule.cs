﻿using System;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MSDesktop.MessageRouting;


namespace ModuleIsolationExample.View2ViewCommunication
{
    public class V2VSubscriberModule: IModule
    {
        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;
        private readonly IRoutingCommunicator _communicator;

        private const string ViewId = "Subscriber";
        private const string WidgetButtonId = "CreateSubscriber";

        public V2VSubscriberModule(IChromeManager chromeManager,
            IChromeRegistry chromeRegistry,
            IRoutingCommunicator communicator)
        {
            _communicator = communicator;
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
        }

        public void Initialize()
        {
            _chromeRegistry.RegisterWindowFactory(ViewId, CreateView);
            _chromeRegistry.RegisterWidgetFactory(WidgetButtonId, _chromeManager.ShowWindowButtonFactory());

            var bitmapUri = new Uri(@"pack://application:,,,/ModuleIsolationExample;component/Resources/document_2_down_64.png", UriKind.RelativeOrAbsolute);

            var initialParameters = new InitialShowWindowButtonParameters
                {
                    Text = "Subscriber",
                    ToolTip = "Demo Button",
                    WindowFactoryID = ViewId,
                    Image = BitmapFrame.Create(bitmapUri),
                    InitialParameters = new InitialWindowParameters
                        {
                            InitialLocation = InitialLocation.DockRight,
                        },
                    Size = ButtonSize.Large,
                };

            _chromeManager.PlaceWidget(WidgetButtonId, "How/Where", initialParameters);
        }

      
        private bool CreateView(IWindowViewContainer viewcontainer, XDocument state)
        {
            var sub = _communicator.GetSubscriber<SendingMessage>();
            viewcontainer.Content = new SubscriberView(sub);
            viewcontainer.Title = "Subscriber";
            return true;
        }
       
    }
}
