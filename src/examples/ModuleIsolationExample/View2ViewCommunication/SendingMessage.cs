﻿using System;
using MSDesktop.MessageRouting.Message;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace ModuleIsolationExample.View2ViewCommunication
{
  
    [Message("SendingMessage"), Serializable]
   
    public class SendingMessage : IProvideMessageInfo
   
    {
        public String StockName { get; set; }
        public String Description { get; set; }
        public string GetInfo()
        {
            return "SendingMessage - Isolated over Kraken";
        }
    }
   
}
