﻿using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;



namespace ModuleIsolationExample.View2ViewCommunication
{

    public class ManagingModule: IModule
    {
        private readonly IAdapterService _adapterService;
        public ManagingModule(IAdapterService adapterService)
        {
            _adapterService = adapterService;
        }

        public void Initialize() // Module initializer
        {
            _adapterService.AddAdapter<SendingMessage, ReceivingMessage>(
               (m) => new ReceivingMessage() { Stock = m.StockName, Message = m.Description });
        }
    }
}
