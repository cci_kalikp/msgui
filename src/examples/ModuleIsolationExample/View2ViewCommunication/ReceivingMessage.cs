﻿using System;
using MSDesktop.MessageRouting.Message;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace ModuleIsolationExample.View2ViewCommunication
{
    [Message("ReceivingMessage")]
   
    [Serializable]
    public class ReceivingMessage : IProvideMessageInfo
    {
        public String Stock { get; set; }
        public String Message { get; set; }

        public string GetInfo()
        {
            return "ReceivingMessage - Isolated over Kraken";
        }
    }
}
