﻿using System;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using MSDesktop.MessageRouting;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;


namespace ModuleIsolationExample.View2ViewCommunication
{
    public class V2VPublisherModule: IModule
    {

        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;
        // private readonly V2VConfigModule _v2vConfigModule;

        private readonly IRoutingCommunicator _communicator;
      

        private const string ViewId = "Publisher";
        private const string WidgetButtonId = "CreatePublisher";

        
      
        public V2VPublisherModule(IChromeManager chromeManager, 
            IChromeRegistry chromeRegistry, 
            IRoutingCommunicator communicator)
        {
            _communicator = communicator;
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
        }



        public void Initialize()
        {
            _chromeRegistry.RegisterWindowFactory(ViewId, CreateView);
            _chromeRegistry.RegisterWidgetFactory(WidgetButtonId, _chromeManager.ShowWindowButtonFactory());

            var bitmapUri = new Uri(@"pack://application:,,,/ModuleIsolationExample;component/Resources/document_2_up_64.png", UriKind.RelativeOrAbsolute);
            var initialParameters = new InitialShowWindowButtonParameters
                {
                    Text = "Publisher",
                    ToolTip = "Demo Button",
                    WindowFactoryID = ViewId,
                    Image = BitmapFrame.Create(bitmapUri),
                    InitialParameters = new InitialWindowParameters
                        {
                            InitialLocation = InitialLocation.DockLeft
                        },
                    Size = ButtonSize.Large,
                };

            _chromeManager.PlaceWidget(WidgetButtonId, "How/Where", initialParameters);

            // _v2vConfigModule.AutoHookMode = AutoHookMode.IslandMode;
        }

        private bool CreateView(IWindowViewContainer viewcontainer, XDocument state)
        {
            var publisher = _communicator.GetPublisher<SendingMessage>();
            viewcontainer.Content = new PublisherView(publisher);
            viewcontainer.Title = "Publisher";
            return true;
        }
    }
}
