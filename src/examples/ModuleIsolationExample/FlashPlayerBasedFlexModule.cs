﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/ModuleIsolationExample/FlashPlayerBasedFlexModule.cs#3 $
// $Change: 868644 $
// $DateTime: 2014/02/27 02:07:52 $
// $Author: caijin $

using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using ModuleIsolationExample.Properties;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.FlexModule;

namespace ModuleIsolationExample
{
    /// <summary>
    /// Flash player ActiveX control hosting Flex module
    /// </summary>
    internal sealed class FlashPlayerBasedFlexModule : FlexModuleFlashPlayerBaseImpl, IModule
    {
        #region Private Fields


        private TextBox m_headerText;
        private const string BrowserViewId = "FlashPlayerBasedFlexView";
        private const string BrowserViewButtonId = "FlashPlayerBasedFlexViewWidget";

        private const string asd = "asd";
        private const string qwe = "qwe";

        private readonly Dispatcher _dispatcher;

        #endregion

        #region Constructor

        public FlashPlayerBasedFlexModule(IChromeRegistry chromeRegistry, IChromeManager chromeManager) :
            base(chromeRegistry, chromeManager,
                 new Uri(
                     Path.Combine(
                         Path.Combine(
                             Path.GetDirectoryName(
                                 Assembly.GetExecutingAssembly().Location), "Lib"), "msguitest35.swf")))
        {
            _dispatcher = Dispatcher.CurrentDispatcher;
        }

        #endregion

        #region IModule Members

        public override void Initialize()
        {
            m_chromeRegistry.RegisterWindowFactory(BrowserViewId, CreateView);
            m_chromeRegistry.RegisterWidgetFactory(BrowserViewButtonId, m_chromeManager.ShowWindowButtonFactory());
            m_chromeRegistry.RegisterWindowFactory(asd, ASDView);
            m_chromeRegistry.RegisterWidgetFactory(qwe, QWEInit);

            var initialParameters = new InitialShowWindowButtonParameters()
                {
                    Image = new Icon(Resources.Chart, 2, 32).ToImageSource(),
                    Text = "Isolated Flex Player",
                    ToolTip = "Isolated Flash Player",
                    Enabled = true,

                    //window settings
                    WindowFactoryID = BrowserViewId,
                    InitialParameters = new InitialWindowParameters()
                };
             
            m_chromeManager.PlaceWidget(BrowserViewButtonId, "Views/Flex", initialParameters);
            m_chromeManager.PlaceWidget(BrowserViewButtonId, ChromeArea.QAT, string.Empty, new InitialWidgetParameters());
            m_chromeManager.PlaceWidget(qwe, "Hello/world", new InitialWidgetParameters());
        }

        public bool ASDView(IWindowViewContainer wvc, XDocument state)
        {
            return true;
        }

        public bool QWEInit(IWidgetViewContainer wvc, XDocument state)
        {
            wvc.Content = new Infragistics.Windows.Ribbon.ButtonTool() {Content = "LOL", Id = qwe};

            return true;
        }

        #endregion

        #region View Creation

        protected override bool CreateView(IWindowViewContainer viewcontainer_, XDocument state)
        {
            if (base.CreateView(viewcontainer_, state))
            {
                viewcontainer_.Title = "Flash Player Based Flex View";

                m_headerText = new TextBox {Text = "Text to be sent to SWF"};
                viewcontainer_.HeaderItems.Add(m_headerText);
                var sendButton = new Button {Content = "Send"};
                sendButton.Click += sendButton_Click;
                viewcontainer_.HeaderItems.Add(sendButton);

                viewcontainer_.Closed += viewcontainer__Closed;
                viewcontainer_.ClosedQuietly += viewcontainer__Closed;

                return true;
            }
            return false;
        }

        private void viewcontainer__Closed(object sender, WindowEventArgs e)
        {
            e.ViewContainer.Closed -= viewcontainer__Closed;
            e.ViewContainer.ClosedQuietly -= viewcontainer__Closed;
            ((Button) e.ViewContainer.HeaderItems[1]).Click -= sendButton_Click;
        }

        private void sendButton_Click(object sender, RoutedEventArgs e)
        {
            CallToFlex("newMessage", m_headerText.Text);
        }

        #endregion

        #region Communication With Flex

        protected override object CallFromFlex(string functionName_, object[] arguments_)
        {
            if (functionName_.Equals("newMessage") && arguments_ != null && arguments_.Length > 0)
            {
                NewMessage(arguments_[0] as string);
            }
            return null;
        }

        /// <summary>
        /// Called to notify C# of a new message from the SWF IM client
        /// </summary>
        /// <param name="message_"></param>
        private void NewMessage(string message_)
        {
            m_headerText.Text = message_;
        }

        #endregion

    }
}
