﻿using System.Windows.Controls;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace ModuleIsolationExample
{
    internal class IsolatedModule : IModule
    {
        internal const string CrossProcessWindowFactoryId = "window";

        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;

        public IsolatedModule(IChromeManager chromeManager, IChromeRegistry chromeRegistry)
        {
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
        }

        public void Initialize()
        {
            _chromeRegistry.RegisterWindowFactory(CrossProcessWindowFactoryId, (container, state) =>
            {
                container.Content = new TextBox();
                return true;
            });

            /*
            var fakeQatArea = new WidgetViewContainer(ChromeArea.QAT);
            var initialParameters = new InitialShowWindowButtonParameters
            {
                WindowFactoryID = windowFactoryId,
                Text = "I'm from isolated subsystem"
            };

            _chromeManager.AddWidget("hello", initialParameters, fakeQatArea);
            */
        }
    }
}
