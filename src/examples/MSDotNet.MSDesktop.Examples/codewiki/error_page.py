try:
    import tornado.template
    import filter
except Exception as e:
    print 'cannot get tornado'
    print e
    raise e




doc = '''
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>      
        <style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important;
		-khtml-user-select: text!important;
	}
      </style>
        <meta name="fragment" content="!"> 
        <link rel="stylesheet" type="text/css" href="/msdotnet/codewiki/css/pygments.css"/> 
        <title>CodeWiki</title>
        <script type="text/javascript" src="http://toolkits.ms.com/webaurora/core.js"></script>
        <script type="text/javascript">        
            Module = new WebAuroraEnvironment('prod',false);        
            Module.load('ria','ext','4.1.0');   
            Module.getLoader('ria', 'ms-branding', 'ext-4.1.0').loadAll(['header','mslight']);            
        </script>

        <script type="text/javascript" src="/msdotnet/codewiki/js/error_page.js"></script>
        <link rel="stylesheet" type="text/css" href="/msdotnet/codewiki/css/codewiki.css"/>
    </head>  
<body >
    <div id="errorList"></div>
    <div id="errorDetails"></div>
    
</body>
'''

def page(req): 
    filter.preprocess(req)   
    return doc
    

get = page