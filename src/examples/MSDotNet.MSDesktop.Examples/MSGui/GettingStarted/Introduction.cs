﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSDesktop.MSGui.GettingStarted
{
	public class Introduction
	{
		public Introduction()
		{
			/**
			 * MSGui is the UI framework of MSDesktop featuring APIs for:
			 * 
			 * * view management
			 * * persistence
			 * * module-to-module communication
			 * * view-to-view communication
			 * * ...
			 */

		}

		public void View_Management()
		{
			/**
			 * The view managment API of MSGui is called the Chrome Manager, or CM. (The previous 
			   API, ControlParadigm is deprecated and scheduled to be decommissioned. More on this
			   in the [xref][upgrading to CM] section.)
			 */

			/**    
			 * ChromeManager offers a unified way to handle most aspects of the UI:
			 * 
			 * * Window management
			 *     * Window creation
			 *     * Docking
			 *     * Layout persistence
			 * * Application chrome - widgets
			 *     * Ribbon shell:
			 *         * Ribbon
			 *         * Quick Access Toolbar
			 *         * Statusbar
			 *         * System tray
			 *     * Launcher shell
			 *         * Launcher bar (with expanded and collapsed states)
			 *         * Statusbar
			 *         * System tray
			 * 
			 * Besides offering a simple, straightforward API ChromeManager has a great number of
			   shorthands for common scenarios and uses sensible defaults for the most common 
			   use-cases.
			*/

		}

		public void Persistence()
		{
			/**
			 * [placeholder]
			 */
		}

		public void Module_to_module_communication()
		{
			/**
			 * [placeholder]
			 */
		}

		public void View_to_view_communication()
		{
			/**
			 * [placeholder]
			 */
		}
	}
}
