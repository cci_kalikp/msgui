﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSDesktop.MSGui.Examples.Upgrading_to_ChromeManager
{
	// This is butt ugly, but I see no other way to present a constructor code snippet in the 
	// documentation other than using partial classes.

  //cw Chrome_Upgrade_From_ControlParadigm :20

	public partial class From_ControlParadigm
	{
		private IChromeRegistry m_chromeRegistry;
		private IChromeManager m_chromeManager;

		public From_ControlParadigm()
		{
			/**
			 * <h1>Upgrading from ControlParadigm </h1>
			 * <p class="h1_subtitle">For those that are already familiar with MSDesktop, this example highlights the important changes
			   between ControlParadigm and ChromeManager via an example module. for detailed information on ChromeManager, please 
			   refer to the [View Management] section of the API Documentation.</p>
			 */
		}

		public void Basics()
		{
			/**
			 * In ChromeManager the launcher bar, ribbon and statusbar interfaces are managed via a common API. Controls placed 
			   on these areas are called widgets.
			 * To create UI elements (widgets and windows alike) CM uses factory methods. These have to be registered
			   using IChromeRegistry, and will be called by the IChromeManager when needed.
			 */
		}

		public void Module_constructor()
		{
			/**
			 * In the module constructor, request the following interfaces from the DI container:
			 * 
			 * * IChromeRegistry <br />
			 * * IChromeManager <br />
			 */
		}
	}

	public partial class SampleModule
	{
		//{
		private IChromeRegistry m_chromeRegistry;
		private IChromeManager m_chromeManager;

		public SampleModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_)
		{
			this.m_chromeRegistry = chromeRegistry_;
			this.m_chromeManager = chromeManager_;
		}
		//}
	}

	public partial class From_ControlParadigm
	{
		public void Module_initialization()
		{
			/**
			 * Using the two interfaces, register a custom window factory, and a common button factory:
			 * (To use the built-in factories of CM, the MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers
			   is needed.)
			 * 
			 */
		}
	}

	public partial class SampleModule
	{
		//{ 
		public void Initialize()
		{
			this.m_chromeRegistry.RegisterWindowFactory("MyWindow", MyWindowFactory);
			this.m_chromeRegistry.RegisterWidgetFactory("MyButton", this.m_chromeManager.ButtonFactory());

			this.m_chromeManager.PlaceWidget("MyButton", "Ribbon", "Main/Common", new InitialButtonParameters()
			{
				Text = "Click me!",
				ToolTip = "Show a new MyWindow",
				Image = System.Drawing.SystemIcons.Information.ToImageSource(),
				Click = this.MyButton_Click
			});
		}
		//}
	}


	public partial class From_ControlParadigm
	{
		public void Window_factory()
		{
			/**
			 * Factories now need to return a boolean to indicate sucess/failure, otherwise they are largely 
			   similar to those used with ControlParadigm.
			 * 
			 */
		}
	}

	public partial class SampleModule
	{
		//{
		public bool MyWindowFactory(IWindowViewContainer emptyViewContainer_, XDocument savedState_)
		{
			emptyViewContainer_.Icon = System.Drawing.SystemIcons.Information;
			emptyViewContainer_.Title = "My window";
			emptyViewContainer_.Content = new System.Windows.Controls.TextBlock() { Text = "O HAI CM" };

			return true;
		}
		//}

		public void MyButton_Click(object sender, EventArgs e)
		{
			this.m_chromeManager.CreateWindow("MyWindow", new InitialWindowParameters());
		}
		//}
	}

	public partial class From_ControlParadigm
	{
		public void Shorthands()
		{
			/**
			 * ChromeManager aims to simplify common UI tasks as much as possible: 
			 */
			
			/** 
			 * For example, if the <em>root</em> parameter
			   is omitted from the above PlaceWidget call, CM will put the widget on the default control interface - on the 
			   launcher bar or the ribbon depending on the shell mode.
			 */

			//{
			this.m_chromeManager.PlaceWidget("MyButton", "Main/Common", new InitialButtonParameters() /* ... */);
			//}
			/**
			 * It is highly recommended to use this shorter form whenever possible, as this ensures that the application
			 * is shell mode independent.
			 */

			/**
			 * One more area where CM does the repetitive tasks instead of the developer is the implicit factory that is
			 * available for widget creation: if the RegisterWidget call does not specify a factory, CM will use its own
			 * logic to determine the factory to use based on the InitialParameters instance that is passed to the
			 * PlaceWidget call.
			 */
			//{
			this.m_chromeRegistry.RegisterWidgetFactory("MyButton");
			this.m_chromeManager.PlaceWidget("MyButton", "Main/Common", new InitialButtonParameters() /* ... */);
			//}

			/**
			 * Another shorthand is the ShowWindowButton factory: One of if not the most frequent use-case of buttons on the
			 * MSDesktop chrome is to pop up a new window. For this, no button click event handler is necessary:
			 */

			//{
			this.m_chromeRegistry.RegisterWindowFactory("MyWindow", MyWindowFactory);
			this.m_chromeRegistry.RegisterWidgetFactory("MyButton");
			this.m_chromeManager.PlaceWidget("MyButton", "Main/Common", new InitialShowWindowButtonParameters()
			{
				Text = "Click me!",
				ToolTip = "Show a new MyWindow",
				Image = System.Drawing.SystemIcons.Information.ToImageSource(),

				WindowFactoryID = "MyWindow"
			});
			//}
		}

		public bool MyWindowFactory(IWindowViewContainer emptyViewContainer_, XDocument savedState_)
		{
			emptyViewContainer_.Icon = System.Drawing.SystemIcons.Information;
			emptyViewContainer_.Title = "My window";
			emptyViewContainer_.Content = new System.Windows.Controls.TextBlock() { Text = "O HAI CM" };

			return true;
		}

	}
}
