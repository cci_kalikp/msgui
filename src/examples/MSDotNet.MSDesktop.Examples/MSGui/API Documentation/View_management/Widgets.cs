﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Xml.Linq;
using System.Windows.Controls;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MorganStanley.MSDotNet.MSDesktop.MSGui.API_Documentation.View_management
{
	class Widgets
	{
		public Widgets()
		{
      //cw Chrome_Management_Widgets :20
			/**
			 * Widgets are created and placed on the chrome using factories. Conventionaly, widgets need to be registered
			   before using them. This registration tells the Chrome Manager which factory is responsible for creating the
			   widget - a single factory can be used to create similar controls.
			 * 
			 * To simplify common UI creation, Chrome Manager contains several common factories for the most common UI 
			   elements such as buttons, combo boxes, text boxes and so on. 
			 * To use these factories, use the MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers namespace, the
			   factories will be available via extension methods on the IChromeManager interface.

			 */
		}

		public void Common_case()
		{
			IChromeManager m_chromeManager = null;
			/**
			 * When using common controls (button, combo box, dropdown button, textbox), the only thing needed to put 
			   something on the ribbon/launcher is a placement call using the IChromeManager interface.
			 * 
			 * Below sample will create a button and place it on the default control interface. In the ribbon shell 
			   mode, this will place this on the Tools group inside the Main tab, in launcher bode it will simply be
			   placed on the launcher.
			 */
			//{
            m_chromeManager.Ribbon["Main"]["Tools"].AddWidget("myWidget", new InitialButtonParameters()
			{
				Text = "Hello MSDesktop!",
				Size = ButtonSize.Small,
				Click = (sender, e) => 
				{
          TaskDialog.ShowMessage("!");
				}
			});
			//}
			/**
			 * In this simplified case, Chrome Manager implicitly registers the widget using a factory defined by the
			   Initial*Parameters instance passed to the AddWidget call.
			 * 
			 * For more complex scenarios involving custom controls, read on.
			 */
		}

		public void Widget_registration()
		{

			/**
			 * First, you will need a widget factory that is an InitializeWidgetHandler instance and returns true
			   if the creation succeeded. If you plan to make this factory reusable, you may want to create a new
			   InitialWidgetParameters descendant class to hold your parameters (this is optional).
			 */
		}
		
		//{
		class MyInitialParameters : InitialWidgetParameters
		{
			public string MyProperty { get; set; }
		}
		//}

		private void Widget_registration_2()
		{
			IChromeRegistry m_chromeRegistry = null;
			IChromeManager m_chromeManager = null;
			//{
			InitializeWidgetHandler MyFactory = (IWidgetViewContainer emptyWidget_, XDocument state) =>
			{
				var parameters = emptyWidget_.Parameters as MyInitialParameters;
				if (parameters != null)
				{
					// create your widget and assign it to Content here 
					emptyWidget_.Content = new Button()
					{
						Content = parameters.MyProperty
					};
					return true;
				}
				else
				{
					return false;
				}
			};
			//}

			/**
			 * When the factory is ready, widgets can be registered using the IChromeRegistry interface:
			 */

			//{
			m_chromeRegistry.RegisterWidgetFactory("myWidget", MyFactory);
			//}

			/**
			 * After the factory has been registered, you can place the control using the IChromeManager interface:
			 */
			//{
		    m_chromeManager.Ribbon["Main"]["Tools"].AddWidget("myWidget", new InitialWidgetParameters());
		    //}
		}
		
	}
}
