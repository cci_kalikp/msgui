﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSDesktop.MSGui.API_Documentation.View_management
{
	public class InitialWindowParameters
	{
		public InitialWindowParameters()
		{
      //cw InitialWindowParameters :40
			/**
			 * This class holds default values for a newly created window.
			 */
            /**
             * <style type="text/css">
               p { margin-bottom: 6pt; }
               .niceTable { font-size: 10pt; }
               .niceTable th { background: #555; color: white; border: 1px solid #555; font-weight: bold; padding: 3px }
               .niceTable table th { background: #ddd; color: black; border: 1px solid #ddd; font-weight: bold; padding: 3px; }
               .niceTable td { padding-bottom: 4px; border: 1px solid white; vertical-align: top }
               </style>
             * <table class="niceTable">
             * <thead>
             * 	   <tr>
             * 	       <th>Property</th>
             * 	       <th>Type</th>
             * 	       <th>Description</th>
             * 	   </tr>
             * </thead>
             * <tbody>
             * 	   <tr>
             * 	       <td>Singleton</td>
             * 	       <td>bool</td>
             * 	       <td>Determines whether the window should be treated as singleton.</td>
             * 	   </tr>
             * 	   <tr>
             * 	       <td>InitialLocation</td>
             * 	       <td>enum flags: InitialLocation</td>
             * 	       <td>Determines where the window will be positioned. This is a bit field so for example,
             * 	       for a custom location you need to use: InitialLocation.Custom | InitialLocation.Floating.
             * 			   <table class="niceTable">
             * 				<thead>
             * 					<tr>
             * 						<th>Value</th>
            * 						<th>Description</th>
             * 					</tr>
             * 				</thead>
             * 				<tbody>
             * 					<tr>
             * 						<td>Floating</td>
            * 						<td>The window will be floating and dockable.</td>
             * 					</tr>
             * 					<tr>
             * 						<td>FloatingOnly</td>
            * 						<td>The window will be floating, non-dockable.</td>
             * 					</tr>
             * 					<tr>
             * 						<td>DockInNewTab</td>
            * 						<td>The window will be docked to a new tab.</td>
             * 					</tr>
             * 					<tr>
             * 						<td>PlaceAtCursor</td>
            * 						<td>The window will be placed centered under the mouse pointer (taking screen constraints into account).</td>
             * 					</tr>
             * 					<tr>
             * 					    <td>Custom</td>
             * 					    <td>A user-provided callback will be called in order to determine window
             * 					    position. You can provide the callback using InitialLocationCallback.</td>
             * 					</tr>
             * 					<tr>
             * 					    <td>Dock(Top|Bottom|Left|Right)</td>
             * 					    <td>Dock inside a container provided with InitialLocationTarger parameter.</td>
             * 					</tr>
             * 				</tbody>
             * 		       </table>
             * 	       </td>
             * 	   </tr>
             * 	   <tr>
             * 	       <td>SizingMethod</td>
             * 	       <td>enum: Sizingmethod</td>
             * 	       <td>
             * 			   <table class="niceTable">
             * 				<thead style="font-weight:bold">
             * 					<tr>
             * 						<th>Value</th>
            * 						<th>Description</th>
             * 					</tr>
             * 				</thead>
             * 				<tbody>
             * 					<tr>
             * 						<td>SizeToContent</td>
            * 						<td>The window will be sized to accomodate its content. This setting will be canceled 
                                    when the user resizes the window.</td>
             * 					</tr>
             * 					<tr>
             * 						<td>Custom</td>
            * 						<td>The window can be sized freely, no sizing logic will be applied.</td>
             * 					</tr>
             * 				</tbody>
             * 		       </table>
             * 	       </td>
             * 	   </tr>
             * 	   <tr>
             * 	       <td>Width</td>
             * 	       <td>double</td>
             * 	       <td>Sets the initial width of the window.
             * 	       </td>
             * 	   </tr>
             * 	   <tr>
             * 	       <td>Height</td>
             * 	       <td>double</td>
             * 	       <td>Sets the initial height of the window.
             * 	       </td>
             * 	   </tr>
             * 	   <tr>
             * 	       <td>EnforceSizeRestrictions</td>
             * 	       <td>bool</td>
             * 	       <td>If true, the window can't be larger than a single screen.</td>
             * 	   </tr>
             * 	   <tr>
             * 	    <td>Transient</td>
             * 	    <td>bool</td>
             * 	    <td>If true, the window will not be persisted in user's profile. Use this 
             * 	    option as a replacement of dialog windows.</td>
             * 	   </tr>
             * 	   <tr>
             * 	    <td>Singleton</td>
             * 	    <td>bool</td>
             * 	    <td>
             * 	    If true, limits the number of instances of the window to one per one SingletonKey.
             * 	    If you don't set the SingletonKey, as a result only one instance of the window will be
             * 	    allowed. Any attempt to create more instances will result in activation of the only instance.
             * 	    </td>
             * 	   </tr>
             * 	   <tr>
             * 	    <td>SingletonKey</td>
             * 	    <td>bool</td>
             * 	    <td>
             * 	    Only makes sense when Singleton set to true. This option allows you to create more than one
             * 	    instance of a Singleton window. However, each instance has to have a different SingletonKey.
             * 	    For example, you could have two ShowWindowButtons, each one referring to the same window
             * 	    factory ID, both creating a Singleton window but with different SingletonKeys. This would result
             * 	    in allowing the user to create max two instances of that window.
             * 	    </td>
             * 	   </tr>
             * 	   </tbody>
             * </table>
             * 
             */
        }

	}
}
