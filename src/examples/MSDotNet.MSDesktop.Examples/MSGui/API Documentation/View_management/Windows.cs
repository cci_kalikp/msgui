﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSDesktop.MSGui.API_Documentation.View_management
{
	class Windows
	{
		IChromeRegistry m_chromeRegistry = null;
		IChromeManager m_chromeManager = null;

		public void Window_creation()
		{
      //cw Chrome_Management_Windows :20
			/**
			 * To create a window, a factory needs to be registered using an IChromeRegistry 
			   instance (acquirable from the DI container for example via a module constructor 
			   parameter):
			 */

			//{
			m_chromeRegistry.RegisterWindowFactory("MyWindow", this.MyWindowFactory);
			//}

			/**
			 * "MyWindow" is the ID of the factory - it's generally a good practice to store these 
			   in constants.
			 * 
			 * MyWindowFactory is a callback that performs the window creation:
			 */
		}

		//{
		bool MyWindowFactory(IWindowViewContainer emptyViewContainer, XDocument state)
		{
			emptyViewContainer.Content = null; /* replace this with the actual window content */

			return true;
		}
		//}

		public void Showing_a_window()
		{
			/**
			 * To show a window, an IChromeManager instance is used:
			 */
			//{
			m_chromeManager.CreateWindow("MyWindow", new MorganStanley.MSDotNet.MSGui.Core.ChromeManager.InitialWindowParameters());
			//}
			/**
			 * Parameters:
			 *   * "MyWindow" is the factory id registered as above,
			 *   * the InitialWindowParameters instance configures how the window will be shown
			 *     when its created. See [...]
			 */
		}
	}
}
