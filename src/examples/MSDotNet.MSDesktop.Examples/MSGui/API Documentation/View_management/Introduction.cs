﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSDesktop.MSGui.API_Documentation.View_management
{
	public class Introduction
	{
    //cw Chrome_Management_Introduction :20
		public void Introduction_()
		{
			/**
             * ##Introduction
			 * In MSGui, views are created and managed via the 2 interfaces of the ChromeManager API: 
			   IChromeRegistry is responsible for registering the factories used for creating the view 
			   during application startup, while IChromeManager provides facilities for the after-startup 
			   management tasks.
			 * 
			 */
		}

		public void Windows()
		{
			/**
             * ##Windows
			 * ChromeManager features a VisualStudio-like docking window manager that is able to
			   float windows, create tabbed areas and persist the created layouts. 
             * It also supports to create windows layouted as tile items in a tile container.
			 */
		}

		public void Widgets()
		{
			/**
			 * ##Widgets
             * 
             * Widgets are the controls on the different control interfaces. (Throughout the 
			   documentation the term <em>control interface</em> will be used to refer to 
			   logically separated visual areas of the application chrome. For example the 
			   ribbon, its accompanying quick access toolbar, and so on.)
			 */

			/**
			 * Depending on the shell mode (currently eiher ribbon or launcher), the supported 
			 * control interfaces are:
			 * 
			 *  * Ribbon shell:
			 *      * Ribbon - a Microsoft Office 2007 styled ribbon
			 *      * Quick Access Toolbar - area for frequently used buttons above or below 
			 *        (determined by end-user configuration) the ribbon
			 *      * Statusbar - the information bar at the very bottom of the window.
			 *      * System tray - the notification area on the taskbar
			 *  * Launcher shell
			 *      * Launcher bar (with expanded and collapsed states)
			 *      * Statusbar
			 *      * System tray
			 */
		}

        /**
         * ##Naming factories
         * 
         * MSDesktop uses strings called factory IDs to identify factories for windows and widgets. 
         * Factories are registered by modules and a application may load arbitrary modules. Therefore, 
         * in order to ensure uniqueness of IDs, the following naming convention should be followed for
         * factories' IDs:
         * 
         * {GUID}.HumanReadableName
         * 
         * This convention is not followed in the documentation so that readability is not decreased.
         */
	}

}
