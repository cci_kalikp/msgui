﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Windows.Media;
using System.Windows;
using System.Windows.Media.Imaging;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;

namespace MorganStanley.MSDotNet.MSDesktop.Examples.Dialog
{
	public class DialogWindow
	{
		public void Dialogs_in_MSDesktop()
		{
			IViewManager viewManager = null;

			/**
			 * To create a dialog, you will need to use the same IViewManager instance you would use to create a window:
			 */
			//{
			IDialogWindow dialog = viewManager.CreateDialog();
			//}

			/**
			 * You can set basic properties such as title, icon, content:
			 */
			//{
			dialog.Title = "Kenny";
		    var bitmap = System.Drawing.SystemIcons.Shield.ToBitmap();
		    dialog.Icon = bitmap.ToImageSource(true);  
			dialog.Content = new System.Windows.Controls.Button()
			{
				Content = "Hello, Dialog!"
			};
			//}

			/**
			 * You can also set sizing and location values including minimum and maximum dimensions and sizing behaviour:
			 */
			//{
			dialog.Width = 400;
			dialog.MinWidth = 400;
			dialog.MaxWidth = 800;

			dialog.Height = 300;
			dialog.MinHeight = 300;
			dialog.MaxHeight = 600;

			dialog.Left = 100;
			dialog.Top = 100;

			dialog.ResizeMode = ResizeMode.NoResize;
			dialog.SizeToContent = SizeToContent.WidthAndHeight;
			dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
			//}

			/**
			 * you can also register event handlers for the closing and closed events. In the former you can cancel the action by setting e.Cancel to true.
			 */
			//{
			dialog.Closing += (sender, e) => { e.Cancel = true; };
			dialog.Closed += (sender, e) => { Console.WriteLine("OMG, they killed Kenny!"); };
			//}
			/**
			 * Then, you can call Show() to show the dialog as non-modal, creating a transient window:
			 */
			//{
			dialog.Show();
			//}

			/**
			 * Or you can call ShowDialog() to show a modal dialog that returns a tri-state bool:
			 */
			//{
			bool? result = dialog.ShowDialog();
			//}

			/**
			 * This value can be set via the DialogResult property:
			 */
			//{
			dialog.DialogResult = false;
			//}

			/**
			 * To hide a dialog, call Hide() on it:
			 */
			//{
			dialog.Hide();
			//}

		}

	}
}
