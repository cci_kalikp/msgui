﻿using System.Windows.Controls;
using GetStarted_CopyLocal_IG_Patch;
using Microsoft.Practices.Unity;

namespace MorganStanley.MSDotNet.MSDesktop.Examples.ProcessIsolation
{
    /// <summary>
    /// Interaction logic for IsolatedControl.xaml
    /// </summary>
    public partial class IsolatedControl : UserControl
    {
        public IsolatedControl(IUnityContainer container)
        {
            DataContext = container.Resolve<IsolatedControlViewModel>();
            InitializeComponent();
        }
    }
}
