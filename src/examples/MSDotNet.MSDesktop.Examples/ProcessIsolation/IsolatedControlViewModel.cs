﻿using System.Windows;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace GetStarted_CopyLocal_IG_Patch
{
    public class IsolatedControlViewModel : DependencyObject
    {
        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;

        public IsolatedControlViewModel(IChromeManager chromeManager, IChromeRegistry chromeRegistry)
        {
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
            _chromeRegistry.RegisterWindowFactory("emptyWindow", (container, state) =>
                {
                    container.Title = "Empty window";
                    return true;
                });

            CreateEmptyWindowCommand = new RelayCommand(ExecuteCreateEmptyWindowCommand);
        }

        private void ExecuteCreateEmptyWindowCommand(object o)
        {
            _chromeManager.CreateWindow("emptyWindow", new InitialWindowParameters());
        }

        public ICommand CreateEmptyWindowCommand { get; internal set; }

        public string UserInput
        {
            get { return (string) GetValue(UserInputProperty); }
            set { SetValue(UserInputProperty, value); }
        }

        public static readonly DependencyProperty UserInputProperty =
            DependencyProperty.Register("UserInput", typeof (string), typeof (IsolatedControlViewModel),
                                        new UIPropertyMetadata(string.Empty));
    }
}
