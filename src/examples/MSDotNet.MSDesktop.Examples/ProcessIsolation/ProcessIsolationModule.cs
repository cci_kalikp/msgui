﻿using System.Windows.Input;
using GetStarted_CopyLocal_IG_Patch;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;

namespace MorganStanley.MSDotNet.MSDesktop.Examples.ProcessIsolation
{
    public class ProcessIsolationModule : IModule
    {
        private readonly IChromeRegistry _chromeRegistry;
        private readonly IChromeManager _chromeManager;
        private readonly IUnityContainer _container;

        public ProcessIsolationModule(IChromeRegistry chromeRegistry, 
            IChromeManager chromeManager,
            IUnityContainer container)
        {
            _chromeRegistry = chromeRegistry;
            _chromeManager = chromeManager;
            _container = container;
        }

        public void Initialize()
        {
            _chromeRegistry.RegisterWindowFactory("IsolatedControlWindow", 
                (container, state) =>
                    {
                        container.SetUpProcessIsolatedWrapperFor<IsolatedControl>(state);
                        return true;
                    });
            _chromeManager.Ribbon["Process isolation"]["New Process Isolation"].AddWidget(
                "View with VM",
                new InitialShowWindowButtonParameters
                    {
                        WindowFactoryID = "IsolatedControlWindow",
                        Text = "View with VM",
                        Gesture = new KeyGesture(Key.T, ModifierKeys.Alt)
                    });

            _chromeRegistry.RegisterWindowFactory("IsolatedControlWindow32",
                (container, state) =>
                {
                    container.SetUp32BitProcessIsolatedWrapperFor<IsolatedControl>(state);
                    return true;
                });
            _chromeManager.Ribbon["Process isolation"]["New Process Isolation"].AddWidget(
                "View with VM x86",
                new InitialShowWindowButtonParameters
                {
                    WindowFactoryID = "IsolatedControlWindow32",
                    Text = "View with VM x86"
                });

            _container.RegisterType<IsolatedControlViewModel>(new ContainerControlledLifetimeManager());
            _chromeRegistry.RegisterWindowFactory("NonIsolatedControl",
                (container, state) =>
                    {
                        container.Content = new IsolatedControl(_container);
                        return true;
                    });
            _chromeManager.Ribbon["Process isolation"]["New Process Isolation"].AddWidget(
                "View with VM (not isolated)",
                new InitialShowWindowButtonParameters
                {
                    WindowFactoryID = "NonIsolatedControl",
                    Text = "View with VM (not isolated)"
                });
        }
    }
}
