﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using Infragistics.Windows.Ribbon;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ShellMenu;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.StatusBar;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.SystemTray;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon.Widgets;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

namespace MorganStanley.MSDotNet.MSDesktop.Examples.View2ViewCommunication
{
	class TextView : IModule
	{
		readonly IChromeRegistry m_chromeRegistry;
		readonly IChromeManager m_chromeManager;
		readonly IAlert m_alert;

		private const string m_simpleViewId = @"\\LauncherModeExample\SimpleModule\SimpleView";
		private const string m_simpleViewId_NewTab = @"\\LauncherModeExample\SimpleModule\SimpleView-NewTab";
		private const string m_simpleViewId_SizeToContent = @"\\LauncherModeExample\SimpleModule\SimpleView-SizeToContent";
		private const string m_simpleViewId_PlaceAtCursor = @"\\LauncherModeExample\SimpleModule\SimpleView-PlaceAtCursor";
		private const string m_winformsAutosize = @"\\LauncherModeExample\SimpleModule\WinFormsView_AutoSize";

		//private readonly IShellWindowMenuFacade m_menuFacade;




		private readonly IAdapterService m_adapterService;
		private readonly V2VConfigModule m_v2vConfigModule;
		private readonly IEventRegistrator m_registrator;

		private readonly IEventAggregator _eventAggregator;
		private readonly IShellWindowMenuFacade m_swmf;

		public TextView(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, IAlert alert_,
			IEventAggregator eventAggregator,
            V2VConfigModule configModule_, IEventRegistrator ipc, IShellWindowMenuFacade swmf)
		{
			m_chromeRegistry = chromeRegistry_;
			m_chromeManager = chromeManager_;
			m_alert = alert_;

			m_swmf = swmf;

			m_registrator = ipc;
			_eventAggregator = eventAggregator;

			m_adapterService = ipc.AdapterService;
			m_v2vConfigModule = configModule_;
		}


		public void Initialize()
		{

			m_adapterService.AddAdapter<StockPrice, StockFeed>(x => new StockFeed{Name = x.StockName,Price = x.Price});


			//Register view builder
			this.m_chromeRegistry.RegisterWindowFactory(m_simpleViewId, AddView);
			this.m_chromeRegistry.RegisterWindowFactory(m_simpleViewId_NewTab, AddView_NewTab);
			this.m_chromeRegistry.RegisterWindowFactory(m_simpleViewId_SizeToContent, AddView_SizeToContent);
			this.m_chromeRegistry.RegisterWindowFactory(m_simpleViewId_PlaceAtCursor, AddView_PlaceAtCursor);
			this.m_chromeRegistry.RegisterWindowFactory(m_winformsAutosize, AddView_WinFormsAutoSize);

			this.m_chromeManager.Windows.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(ViewsObservable_CollectionChanged);

			//Register button to create view
			this.m_chromeRegistry.RegisterWidgetFactory("Create new window");
			this.m_chromeRegistry.RegisterWidgetFactory("New window in new tab");

			var widget = new InitialShowWindowButtonParameters
			{
				ToolTip = "Create new window",
				Image = BitmapFrame.Create(
					 new Uri(@"pack://application:,,,/View2ViewCommunication/Images/kiwi_logo.png",
						 UriKind.RelativeOrAbsolute)),
				WindowFactoryID = m_simpleViewId
			};

			this.m_chromeManager.PlaceWidget("Create new window",
				RibbonChromeManager.QAT_COMMANDAREA_NAME,
				"",
				widget);

			this.m_chromeManager.PlaceWidget("Create new window", RibbonChromeManager.RIBBON_COMMANDAREA_NAME, "Example/Tools", widget);

		    var widget2 = m_chromeManager.AddWidget("Test first button",
		                                            new InitialButtonParameters() {Text = "test first button"},
                                                    m_chromeManager.Ribbon["Example"]["Tools"]);
            m_chromeManager.Ribbon.QAT.AddWidget(widget2, 0);
			/*
			var menuItem = m_menuFacade.SaveLayoutAsMenu;
			if (menuItem != null)
			{
				var factoryId = "MenueQAT_SaveAs";
				m_chromeRegistry.RegisterWidgetFactory(factoryId, (vc, s) =>
				{
					if (vc == null) return false;
					vc.Content = menuItem; return true;
				});
				m_chromeManager.PlaceWidget(factoryId, RibbonChromeManager.QAT_COMMANDAREA_NAME, "",
											new InitialWidgetParameters());    
			}
			*/
			this.m_chromeManager.PlaceWidget(this.m_chromeManager.SaveLayoutAsMenuFactory(), ChromeArea.QAT, "", null);

           
			this.m_chromeManager.PlaceWidget("New window in new tab", RibbonChromeManager.QAT_COMMANDAREA_NAME, "", new InitialShowWindowButtonParameters()
						 {
							 ToolTip = "New window in new tab",
							 Image =
								 BitmapFrame.Create(
									 new Uri(@"pack://application:,,,/View2ViewCommunication/Images/kiwi_logo.png",
											 UriKind.RelativeOrAbsolute)),

							 WindowFactoryID = m_simpleViewId_SizeToContent
						 });


			this.m_chromeRegistry.RegisterWidgetFactory(@"\\LauncherModeExample\SimpleModule\StrangeWidget", CreateStrangeWidget);
			this.m_chromeManager.PlaceWidget(@"\\LauncherModeExample\SimpleModule\StrangeWidget", "Example/Tools", null);

			this.m_chromeRegistry.RegisterWidgetFactory("remove");
			var w = m_chromeManager.PlaceWidget("remove", "Example/Tools", new InitialButtonParameters()
			{
				Text = "Remove me",
				Click = (s, e) => { (m_chromeManager as IControlParadigmImplementator)[""].RemoveItem("remove"); }
			});

			w.ContentReady += (s, e) => { Trace.WriteLine("yep."); };

			var trayIcon = m_chromeManager.AddTrayIcon("trayIcon", new InitialTrayIconParameters()
			{
				Icon = System.Drawing.SystemIcons.Information,
				Text = "O HAI",
				Visibility = System.Windows.Visibility.Collapsed
			});




			//m_chromeManager.PlaceWidget("greetings.lolspeak", RibbonChromeManager.MENU_COMMANDAREA_NAME, "", new InitialTrayMenuItemParameters()
			//{
			//    Text = "OHAI"
			//});

			m_chromeManager.PlaceWidget("greetings.lolspeak", ChromeArea.SystemTray, "trayIcon/greetings", new InitialTrayMenuItemParameters()
			{
				Text = "OHAI"
			});
			m_chromeManager.PlaceWidget("greetings.gangsta", ChromeArea.SystemTray, "trayIcon/greetings", new InitialTrayMenuItemParameters()
			{
				Text = "Yo!"
			});

			this.m_chromeManager.PlaceWidget("trayShowButton", "Example/Tray icon", new InitialButtonParameters()
			{
				Text = "Show",
				Click = (s, e) => { trayIcon.Visibility = System.Windows.Visibility.Visible; }
			});

			this.m_chromeManager.PlaceWidget("trayHideButton", "Example/Tray icon", new InitialButtonParameters()
			{
				Text = "Hide",
				Click = (s, e) => { trayIcon.Visibility = System.Windows.Visibility.Collapsed; }
			});



			this.m_chromeManager.PlaceWidget("trayControlDropdown", "Example/Tray icon", new InitialDropdownButtonParameters()
			{
				Text = "Change icon"
			});




			this.m_chromeManager.PlaceWidget("trayControlDropdown.Exclamation", "Example/Tray icon/trayControlDropdown", new InitialButtonParameters()
			{
				Text = "Exclamation",
				Click = (s, e) =>
				{
					trayIcon.Icon = System.Drawing.SystemIcons.Exclamation;
				}
			});

			this.m_chromeManager.PlaceWidget("trayControlDropdown.Information", "Example/Tray icon/trayControlDropdown", new InitialButtonParameters()
			{
				Text = "Information",
				Click = (s, e) =>
				{
					trayIcon.Icon = System.Drawing.SystemIcons.Information;
				}
			});

			EventHandler grow = null;
			int i = 0;
			grow = (s, e) =>
			{
				this.m_chromeManager.PlaceWidget("growBtn" + (i++).ToString(), "Example/Grow", new InitialButtonParameters()
				{
					Text = "++",
					Click = grow
				});
			};
			grow(null, null);


			/*
			 * Statusbar sample code
			 */
			IStatusLabelViewContainer statusLabel = null;
			var progressbar = this.m_chromeManager.AddStatusProgressbar(Guid.NewGuid().ToString(), new InitialProgressBarParameters()
			{
				MinValue = 0,
				MaxValue = 10
			});

			this.m_chromeManager.AddButton("StatusBar++", "Example/Statusbar", new InitialButtonParameters()
			{
				Text = "++",
				Click = (s, e) =>
				{
					if (progressbar.Position < progressbar.MaxValue)
					{
						progressbar.Position++;
						statusLabel.Text = "";
						statusLabel.Level = StatusLevel.Information;
					}
					else
					{
						statusLabel.Text = "Max!";
						statusLabel.Level = StatusLevel.Critical;
					}
				}
			});

			this.m_chromeManager.AddButton("StatusBar--", "Example/Statusbar", new InitialButtonParameters()
			{
				Text = "--",
				Click = (s, e) =>
				{
					if (progressbar.Position > progressbar.MinValue)
					{
						progressbar.Position--;
						statusLabel.Text = "";
						statusLabel.Level = StatusLevel.Information;
					}
					else
					{
						statusLabel.Text = "Min!";
						statusLabel.Level = StatusLevel.Critical;
					}
				}
			});

			statusLabel = this.m_chromeManager.AddStatusLabel(Guid.NewGuid().ToString(), new InitialStatusLabelParameters()
			{
				Text = "Hello statusbar!",
				Level = StatusLevel.Information
			});


			this.m_chromeRegistry.RegisterWindowFactory("OoP MSIL", (IWindowViewContainer vc, XDocument state) =>
			{
				vc.SetUpProcessIsolatedWrapperFor<ProcessIsolationSample>(state);

				return true;
			});

			this.m_chromeRegistry.RegisterWindowFactory("OoP x86", (IWindowViewContainer vc, XDocument state) =>
			{
				vc.SetUp32BitProcessIsolatedWrapperFor<ProcessIsolationSample>(state);

				return true;
			});

			this.m_chromeManager.PlaceWidget("OoP MSIL", "Process isolation/Test", new InitialShowWindowButtonParameters()
			{
				Text = "MSIL",
				WindowFactoryID = "OoP MSIL"
			});

			this.m_chromeManager.PlaceWidget("OoP x86", "Process isolation/Test", new InitialShowWindowButtonParameters()
			{
				Text = "32 bit",
				WindowFactoryID = "OoP x86"
			});

			this.m_chromeRegistry.RegisterWidgetFactory("swmf", (s, e) =>
			{
				s.Content = m_swmf.HeaderVisibilityToggle;

				return true;
			});
			this.m_chromeManager.PlaceWidget("swmf", "chromeless/test", new InitialWidgetParameters());


			/*********************************************************************************************************/

			this.m_chromeManager.PlaceWidget("reordering 1", ChromeArea.Locker, "", new InitialButtonParameters()
			{
				Text = "Foo"
			});

			this.m_chromeManager.PlaceWidget("reordering 2", ChromeArea.Locker, "", new InitialButtonParameters()
			{
				Text = "Bar"
			});

			this.m_chromeManager.PlaceWidget("reordering 2", "Chrome reordering/Main", null);
			this.m_chromeManager.PlaceWidget("reordering 1", "Chrome reordering/Main", null);

			/*********************************************************************************************************/

			InitializeWindowHandler windowFactory = (IWindowViewContainer vc, XDocument state) =>
			{
				var rd = new ResourceDictionary()
				{
					Source = new Uri("pack://application:,,,/MSDotNet.MSDesktop.Examples;component/View2ViewCommunication/CustomWindowStyle.xaml", UriKind.RelativeOrAbsolute)
				};

				var b = new Button()
				{
					Margin = new Thickness(20),
					Width = 100,
					Height = 100,
					Content = "Hello world!"
				};


				b.Click += (s_, e) =>
				{
					var window = Window.GetWindow(b);
					var screen = System.Windows.Forms.Screen.FromRectangle(new System.Drawing.Rectangle((int)window.Left, (int)window.Top, (int)window.Width, (int)window.Height));

					if (window.Top + window.ActualHeight + 100 < screen.WorkingArea.Bottom)
					{
						b.Height += 100;
					}
					else if (window.ActualHeight + 100 < screen.WorkingArea.Bottom)
					{
						b.Height += 100;

						window.Top = screen.WorkingArea.Bottom - window.ActualHeight - 100;
					}
				};

				vc.Content = b;

				vc.Parameters.SizingMethod = MSDotNet.MSGui.Core.SizingMethod.SizeToContent;
				vc.Parameters.InitialLocation = InitialLocation.Custom | InitialLocation.Floating;
				vc.Parameters.InitialLocationCallback = InitialLocationHelper.MainDisplayBottomRight;
				vc.Parameters.AllowPartiallyOffscreen = false;
				vc.Parameters.ShowInTaskbar = false;
				vc.Parameters.Topmost = true;
				vc.Parameters.Resources = rd;

				return true;
			};

			this.m_chromeRegistry.RegisterWindowFactory("SampleWindow", windowFactory);



			this.m_chromeManager.PlaceWidget("SampleWindow_Button", "ChromeManager sample/Windows", new InitialButtonParameters()
			{
				Text = "Say hello!",
				Click = (s, e) =>
				{
					this.m_chromeManager.CreateWindow("SampleWindow", new InitialWindowParameters());
				}
			});

			this.m_chromeManager.PlaceWidget("status_combo", ChromeArea.StatusBar, "left", new InitialComboBoxParameters()
			{
				ItemsSource = new List<string>() { "lorem", "ipsum", "dolor", "sit", "amet" }
			});

			var mainStatus = this.m_chromeManager.GetWidget("MainStatus");
			mainStatus.ContentReady += (s,e) =>
			{
				((mainStatus.Content as IStatusBarItem).Control as UIElement).Visibility = Visibility.Collapsed;
			};

			//this.m_chromeManager.PlaceWidget("SampleWindow_Button2", "ChromeManager sample/Windows", new InitialShowWindowButtonParameters()
			//{
			//    Text = "Say hello! #2",
			//    WindowFactoryID = "SampleWindow",
			//    InitialParameters = new InitialWindowParameters()
			//    {
			//        SizingMethod = MSDotNet.MSGui.Core.SizingMethod.Custom,
			//        Height = 400
			//    }
			//});

			var qatWidget = this.m_chromeManager[ChromeArea.QAT].AddWidget("OHAI QAT", new InitialButtonParameters() {Text = "OHAI QAT"});

			var hekkBtn = this.m_chromeManager.AddButton("hekkBtn", "QAT tests/foo", new InitialButtonParameters()
			{
				Text = "remove OHAI QAT",
				Click = (sender, args) => this.m_chromeManager[ChromeArea.QAT].RemoveWidget(qatWidget)
			});


			this.m_chromeManager.Ribbon()["QAT tests"]["custom widgets"].AddWidget("hekkWidgetAddButton", new InitialButtonParameters()
			{
				Text = "Hekk Goes to QAT",
				Click = (sender, args) => hekkBtn.AddToQAT()
			});

            var hekkBtn2 = this.m_chromeManager.AddButton("hekkBtn2", "QAT tests/foo", new InitialButtonParameters()
            {
                Text = "remove OHAI QAT",
                Click = (sender, args) => this.m_chromeManager[ChromeArea.QAT].RemoveWidget(qatWidget)
            });

            this.m_chromeManager.Ribbon()["QAT tests"]["custom widgets"].AddWidget("hekkWidgetAddButton2", new InitialButtonParameters()
            {
                Text = "Hekk Goes to the beginning of QAT",
                Click = (sender, args) => m_chromeManager.Ribbon.QAT.AddWidget(hekkBtn2, 0)
            });

            this.m_chromeRegistry.RegisterWidgetFactoryMapping<InitialHekkWidgetParameters>(HekkWidgetFactory);

            this.m_chromeManager.Ribbon()["QAT tests"]["custom widgets"].AddWidget("hekkWidget", new InitialHekkWidgetParameters()
            {
                Text = "foo"
            });

            var gallery = this.m_chromeManager.Ribbon()["Gallery test"]["Samples"].AddWidget("OHAI GALLERY",
                new InitialRibbonGalleryParameters()
            {
                Text = "OHAI GALLERY",
                MinDropDownColumns = 3,
                ItemClicked = (sender, args) =>
                {
                    MessageBox.Show(args.Item.Text);
                }
            });
            
            var group1 = gallery.AddWidget(new InitialRibbonGalleryGroupParameters()
            {
                Text = "Lorem"
            });

            group1.AddWidget(new InitialRibbonGalleryItemParameters()
            {
                Text = "ipsum",
                Image = SystemIcons.Application.ToImageSource()
            });

            group1.AddWidget(new InitialRibbonGalleryItemParameters()
            {
                Text = "dolor",
                Image = SystemIcons.Shield.ToImageSource()
            });

            var group2 = gallery.AddWidget(new InitialRibbonGalleryGroupParameters()
            {
                Text = "sit"
            });

            group2.AddWidget(new InitialRibbonGalleryItemParameters()
            {
                Text = "amet",
                Image = SystemIcons.Information.ToImageSource(),
                Click = (sender, args) => MessageBox.Show("foo")
            });

		}

		void ViewsObservable_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			if (e.NewItems != null)
			{
				foreach (IWindowViewContainer vc in e.NewItems)
				{
					System.Diagnostics.Trace.WriteLine(String.Format("You added: {0}", vc.Title));
				}
			}

			if (e.OldItems != null)
			{
				foreach (IWindowViewContainer vc in e.OldItems)
				{
					System.Diagnostics.Trace.WriteLine(String.Format("You removed: {0}", vc.Title));
				}
			}
		}

		private bool CreateStrangeWidget(IWidgetViewContainer widget_, XDocument state)
		{
			TextBox textBox = new TextBox() { Text = "MyText", Width = 200, AcceptsReturn = true };

			Label letterCountlabel = new Label();
			Binding lettersCountBinding =
			  new Binding("Text.Count")
			  {
				  Source = textBox,
				  Mode = BindingMode.OneWay
			  };
			letterCountlabel.SetBinding(ContentControl.ContentProperty, lettersCountBinding);

			DockPanel dockPanel = new DockPanel();

			dockPanel.Children.Add(letterCountlabel);
			dockPanel.Children.Add(new Label { Content = "characters" });
			dockPanel.Children.Add(textBox);

			widget_.Content = dockPanel;
			//widget_.MinimizedView = textBox;

			return true;
		}

		private bool AddView(IWindowViewContainer viewContainer_, XDocument state_)
		{
		    viewContainer_.Parameters.ShowFlashBorder = true;
			IPublisher<StockPrice> publisher = m_registrator.RegisterPublisher<StockPrice>(viewContainer_);
			m_registrator.RegisterSubscriber<StockPrice>(viewContainer_);
			m_registrator.Connect<StockPrice, StockPrice>(viewContainer_, viewContainer_);
			var publisherView = new StockPublisher(publisher, _eventAggregator.GetEvent<ViewToViewCommunicationEvent<StockPrice>>(), viewContainer_.ID, viewContainer_);
			//viewContainer_.Closed += (x, y) => registrator.UnregisterPublisher<StockPrice>(publisher);
			viewContainer_.Content = publisherView;
			viewContainer_.Title = "Publisher";
			viewContainer_.Parameters.Width = 400;
			viewContainer_.Parameters.Height = 300;
			viewContainer_.Parameters.SizingMethod = SizingMethod.Custom; 
			


			m_v2vConfigModule.SignUpAutoHook(viewContainer_, null, null);

			return true;
		}

		private bool AddView_NewTab(IWindowViewContainer viewContainer_, XDocument state_)
		{
			AddView(viewContainer_, state_);
			viewContainer_.Parameters.InitialLocation = InitialLocation.DockInNewTab;

			return true;
		}

		private bool AddView_SizeToContent(IWindowViewContainer viewContainer_, XDocument state_)
		{

			ISubscriber<StockFeed> subscriber = m_registrator.RegisterSubscriber<StockFeed>(viewContainer_);



			/* var dialog = new FilterDialog();
			 dialog.ShowDialog();
			 if(!dialog.ShouldCreate)
				 return true;
 */
			var stockName = "MS";
			var filter = from msg in subscriber.GetObservable()
						 where msg.Name == stockName
						 select msg;
			var stockView = new StockPriceViewer(filter, _eventAggregator.GetEvent<ViewToViewCommunicationEvent<StockFeed>>(), viewContainer_.ID);

			

			//viewContainer_.Closed += (x, y) => registrator.UnregisterSubscriber<StockFeed>(subscriber);

			viewContainer_.Content = stockView;
			viewContainer_.Title = "Subscriber: " + stockName;
			viewContainer_.Parameters.Width = 400;
			viewContainer_.Parameters.Height = 300;
			viewContainer_.Parameters.SizingMethod = SizingMethod.SizeToContent;
			viewContainer_.Parameters.EnforceSizeRestrictions = false;
			


			m_v2vConfigModule.SignUpAutoHook(viewContainer_, null, null);

			return true;
		}

		private bool AddView_PlaceAtCursor(IWindowViewContainer viewContainer_, XDocument state_)
		{
			AddView(viewContainer_, state_);
			viewContainer_.Parameters.InitialLocation = InitialLocation.Floating & InitialLocation.PlaceAtCursor;

			return true;
		}

		private bool AddView_WinFormsAutoSize(IWindowViewContainer viewContainer_, XDocument state_)
		{
			return true;
		}

        [FactoryOptions(typeof(HekkWidgetViewContainer))]
        public bool HekkWidgetFactory(IWidgetViewContainer vc, XDocument state)
        {
            var hwvc = vc as HekkWidgetViewContainer;
            if (hwvc == null)
                return false;

            var tb = new TextBox
            {
                Text = vc.Parameters.Text
            };

            hwvc.TextChanged += (sender, args) =>
            {
                var control = sender as TextBox;
                if (control == null)
                    return;

                if (control.Text == "foo ")
                    vc.AddToQAT();
                else if (control.Text == "foo  ")
                    control.Foreground = new SolidColorBrush(Colors.Red);
            };

            vc.Content = tb;

            return true;
        }
    }

    public class HekkWidgetViewContainer : CustomRibbonWidgetViewContainer
    {
        private TextBox tb;

        public HekkWidgetViewContainer(string ID, InitialWidgetParameters parameters)
            : base(ID, parameters) 
        {
        }

        protected override void WireUpEventHandlers(object content)
        {
            this.tb = content as TextBox;
            this.tb.TextChanged += (sender, args) =>
            {
                var handler = this.TextChanged;
                if (handler != null)
                    handler(sender, args);
            };
        }

        public event TextChangedEventHandler TextChanged;
    }

    public class InitialHekkWidgetParameters : InitialWidgetParameters{}
}
