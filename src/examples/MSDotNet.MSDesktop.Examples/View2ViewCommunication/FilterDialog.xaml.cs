﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MorganStanley.MSDotNet.MSDesktop.Examples.View2ViewCommunication
{
    /// <summary>
    /// Interaction logic for FilterDialog.xaml
    /// </summary>
    public partial class FilterDialog : Window
    {

        private bool _shouldCreate;
        private string _stockName;
        
        public FilterDialog()
        {
            InitializeComponent();
        }

        public string StockName { get { return _stockName; } }
        public bool ShouldCreate { get { return _shouldCreate; } }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            _stockName = this.stockbox.Text;
            _shouldCreate = true;
            Close();
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            _shouldCreate = false;
            Close();
        }
    }
}
