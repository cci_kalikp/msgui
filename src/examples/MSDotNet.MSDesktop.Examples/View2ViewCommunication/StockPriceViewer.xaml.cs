﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Composite.Presentation.Events;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using System.Collections.ObjectModel;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

namespace MorganStanley.MSDotNet.MSDesktop.Examples.View2ViewCommunication
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class StockPriceViewer : UserControl
    {
       
        public ObservableCollection<StockFeed> Items { get; private set; }
        private readonly ViewToViewCommunicationEvent<StockFeed> _communicationEvent;
        private readonly string _viewId;

        public StockPriceViewer(IObservable<StockFeed> subscriber, ViewToViewCommunicationEvent<StockFeed> communicationEvent, string id)
        {
            InitializeComponent();
            Items = new ObservableCollection<StockFeed>();
            _communicationEvent = communicationEvent;

            this.prices.DataContext = this;

            communicationEvent.Subscribe(
                msg => { Console.WriteLine("EventReceived: " + id + ": " + msg.ViewContainerId + " " + msg.Message); },
                ThreadOption.BackgroundThread, true);
           
            
            subscriber.Subscribe((x) =>
                {
                    //System.Threading.Thread.Sleep(ran.Next(7000,10000));
                    System.Windows.Application.Current.Dispatcher.Invoke(
                        System.Windows.Threading.DispatcherPriority.Normal, (Action)delegate()
                    {
                        AddData(x);
                    });
                });
            
        }

       




        private void AddData(StockFeed value)
        {
            Items.Add(value);
           
        }

       
    }
   

  
}
