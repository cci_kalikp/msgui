﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;


namespace MorganStanley.MSDotNet.MSDesktop.Examples.View2ViewCommunication
{
    [Serializable]
    [Message("StockPrice", Description = "Stock Price", Grn = "Test grn", Version = "1.0")]
    public class StockPrice
    {
        public String StockName { get; set; }
        public String Price { get; set; }
    }
}
