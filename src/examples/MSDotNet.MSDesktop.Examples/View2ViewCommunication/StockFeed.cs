﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSDesktop.Examples.View2ViewCommunication
{
    
    [Serializable]
    public class StockFeed    
    {
        public String Name { get; set; }
        public String Price { get; set; }
    }
}
