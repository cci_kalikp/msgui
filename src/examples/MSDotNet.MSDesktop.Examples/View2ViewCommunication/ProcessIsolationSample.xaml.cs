﻿using System;
using System.Diagnostics;
using System.Windows.Controls;
using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSDesktop.Examples.View2ViewCommunication
{
	/// <summary>
	/// Interaction logic for ProcessIsolationSample.xaml
	/// </summary>
	public partial class ProcessIsolationSample : UserControl
	{
		public ProcessIsolationSample()
		{
			InitializeComponent();

			textBlock.Text = System.Environment.GetEnvironmentVariable("FOO");
		}

        public void Load(XDocument doc)
        {
            Dispatcher.BeginInvoke((Action)
                                   delegate
                                       {
                                           textBlock.Text += " " + doc.ToString(SaveOptions.None);
                                       }
                );
        }

        public XDocument Save()
        {
            return new XDocument(new XElement("Date", DateTime.Now.ToString()));
        }
	}
}
