﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using MorganStanley.MSDotNet.MSGui.Core;
using System.ComponentModel;
using System.Linq.Expressions;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

namespace MorganStanley.MSDotNet.MSDesktop.Examples.View2ViewCommunication
{
    public class TreeModel : INotifyPropertyChanged
    {
        public ObservableCollection<TreeModel> Views { get; internal set; }
        public  IWindowViewContainer Content {get; private set;}
        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsRegitered { 
            get
            {
                return registrator.GetPublishedTypes(Content).Count() > 0;
            
            }
        }

        private static EventRegistrator registrator;

        public TreeModel(IWindowViewContainer content_)
        {
            Content = content_;           
            
            Views = new ObservableCollection<TreeModel>();
        }





        protected virtual void RaisePropertyChanged<T>
            (Expression<Func<T>> propertyExpression)
        {
            RaisePropertyChanged(GetPropertyName(propertyExpression));
        }

        protected void RaisePropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler temp = PropertyChanged;
            if (temp != null)
            {
                temp(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public static string GetPropertyName<T>(Expression<Func<T>> e)
        {
            var member = (MemberExpression)e.Body;
            return member.Member.Name;
        }

    }
}
