﻿using System;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

namespace MorganStanley.MSDotNet.MSDesktop.Examples.View2ViewCommunication
{
    /// <summary>
    /// Interaction logic for StockPublisher.xaml
    /// </summary>
    public partial class StockPublisher : UserControl
    {
        private readonly IPublisher<StockPrice> _publisher;
        private readonly ViewToViewCommunicationEvent<StockPrice> _aggregatEvent;
        private readonly string _viewId;

        private readonly IWindowViewContainer _container;
        

        public StockPublisher(IPublisher<StockPrice> publisher_, ViewToViewCommunicationEvent<StockPrice> aggregateEvent, string id,  IWindowViewContainer container)
        {
            InitializeComponent();
            _publisher = publisher_;
            _aggregatEvent = aggregateEvent;
            _viewId = id;
            _container = container;

            //_aggregatEvent.Subscribe(msg => Console.WriteLine("EventAggregator: " + msg.ViewContainerId + " " + msg.Message), ThreadOption.BackgroundThread, false);
        }


        private bool _pubToAll;

        private void SubmitNewPrice(object sender, RoutedEventArgs e)
        {
            StockPrice sp = new StockPrice();
            sp.StockName = this.cb_stockNames.SelectionBoxItem as String;
            sp.Price = this.tb_price.Text;




            if (_pubToAll)
                _publisher.Publish(sp);
            else
               _publisher.Publish(sp, CommunicationTargetFilter.Tab, "Main");

            _pubToAll = !_pubToAll;

         /*    _aggregatEvent.Publish(new ViewToViewAggregateMessage<StockPrice>
                                       {
                                           ViewContainerId = _viewId,
                                           Message = sp
                                       });*/

            
            _container.Flash();
        }

       
    }

    
}
