﻿using System;

//cw EnvironmentSupportEnabling :1
/**##Description*/
/**MSDesktop framework can be made aware of the current environment, region, username and the subenvironment.
 * Enabling environment support allows you to query for the environment information later in the application.
 * Additionally, the feature enables labels representing environment on windows headers which makes the
 * application compliant to the global firm requirement for environmental labelling.*/

//cw EnvironmentSupportLabelling :1
/**##Description*/
/**By default enabling environment support turns on labelling on every window. You can either disable
 * the labelling completely or turn it off for docked windows.*/

//cw Chrome_Management_Config :1
/**##Description*/
/**MSDesktop provides a UI to allow users to reconfigure the widget chrome. Currently it allows to: 
 * <ul>
 * <li>hide/show widgets</li>
 * <li>create new ribbon groups/tabs</li>
 * <li>move and reorder widgets</li>
 * <li>assign keyboard gestures</li>
 * </ul>
 * The feature works both with ribbon shell and launcher bar shell. Once enabled, a new item in the
 * application menu is available which opens the chrome configuration screen.
 */

using System.Windows;
//cw EnvironmentSupportEnabling :2 cw EnvironmentSupportLabelling :2
/**##Namespaces*/
//{
using MSDesktop.Environment;
//}
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSDesktop.Examples.ChromeManager;
using MorganStanley.MSDotNet.MSDesktop.Examples.ProcessIsolation;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.MSGui.Themes;

namespace MorganStanley.MSDotNet.MSDesktop.Examples.View2ViewCommunication
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        static App()
        {
        }

        protected override void OnStartup(StartupEventArgs e_)
        {
            
            base.OnStartup(e_);
            //cw Chrome_Management_Config :2 EnvironmentSupportEnabling :2 cw EnvironmentSupportLabelling :2 
            /**##Context*/
            //cw EnvironmentSupportEnabling :2 cw EnvironmentSupportLabelling :2             
            /**Reference MSDesktop.Environment.*/
            //cw Chrome_Management_Config :2 EnvironmentSupportEnabling :2 cw EnvironmentSupportLabelling :2 
            /**Create an instance of the Framework class.*/
            //{
            Framework msguiFramework = new Framework();
            //}
            msguiFramework.AddModule<TextView>();
            msguiFramework.AddModule<CM20SampleModule>();
            msguiFramework.AddModule<ProcessIsolationModule>();
            msguiFramework.SetShellMode(ShellMode.RibbonWindow);
            //msguiFramework.UseLegacySplash();

            //msguiFramework.HideRibbon();
            //msguiFramework.SetLockedLayoutMode();

			//msguiFramework.AddModule<ViewToViewConfigModule>();

            msguiFramework.AddModule<V2VConfigModule>();

			//msguiFramework.AddModule<ConfigMockupModule>();
            //msguiFramework.AddModule<ViewToViewConfigModule>();
            //msguiFramework.SetShellMode(ShellMode.LauncherBarAndFloatingWindows);
            //msguiFramework.SetShellMode(ShellMode.LauncherBarAndFloatingWindows);
            //msguiFramework.DisableEnforcedSizeRestrictions();
            //msguiFramework.EnableLegacyControlPlacement();
            msguiFramework.EnableExceptionHandler("msguiadministrators@ms.com");
			msguiFramework.AddSimpleTheme();
			//msguiFramework.EnableTopmostFloatingPanes();
            //msguiFramework.EnableReflectionModuleLoad();    
			msguiFramework.EnableChromeReordering();        


            msguiFramework.SetApplicationName("Sample Application");
            //msguiFramework.AddBlueTheme();
            //msguiFramework.SetTheme("Blue");
            msguiFramework.EnableNonQuietCloseOfViews();
            msguiFramework.EnableTabsOwnFloatingWindows();

            //cw Chrome_Management_Config :3
            /**##Code*/
            /**Enable widget configuration UI. You can specify a boolean which decides whether the configuration should be global.
             * Global configuration means that ribbon/launcher bar configuration is the same for all user profiles on a given machine.
             */
            //{
            msguiFramework.EnableWidgetUserConfig();
            //}
            msguiFramework.AcquireFocusAfterStartup();

            if (false)
            {
                //cw EnvironmentSupportEnabling :3
                /**##Code*/
                /**In order to enable environment support you need to provide ways to resolve environment, region and username.
                 * Currently one of the predefined methods of resolving environment is from the command line (others like from concord profile string do exists). The command line
                 * argument should look like this: "-env:dev", "-env:qa", "-env:uat", "-env:prod".*/
                //{
                msguiFramework.EnableEnvironment(EnvironmentExtensions.EnvironProviderId.CommandLine,
                                                 EnvironmentExtensions.RegionProviderId.None,
                                                 EnvironmentExtensions.UsernameProviderId.None);
                //}
                /**Additionally, it's possible to provide custom methods of resolving environment, region and username:*/
                //{
                msguiFramework.EnableEnvironment(() =>
                    {
                        Console.WriteLine(@"Retrieving environment");
                        return Environ.Dev;
                    },
                                                 null, null, null);
                //}

                //cw EnvironmentSupportLabelling :3
                /**##Code*/
                /**In order to disable labelling completly:*/
                //{
                msguiFramework.HideOrLockShellElements(HideLockUIGranularity.HideEnvironmentLabelling);
                //}
                /**In order to only show labels for the main window (if available) and for floating windows:*/
                //{
                msguiFramework.HideOrLockShellElements(HideLockUIGranularity.HideEnvironmentLabellingForDockedWindows);
                //}
                /**In order to show two labels, one with the environment and the other with custom subenviroment string
                 * use the fourth parameter of EnableEnvironment method and provide a way to resolve subenvironment string:
                 */
                //{
                msguiFramework.EnableEnvironment(EnvironmentExtensions.EnvironProviderId.CommandLine, 
                    EnvironmentExtensions.RegionProviderId.None, 
                    EnvironmentExtensions.UsernameProviderId.None,
                    EnvironmentExtensions.SubenvironmentProviderId.CommandLine);
                //}
                /**With the above predefined subenvironment resolving method the framework expects a command line argument
                 * like this: "-subenv:somestring".*/
            }

            msguiFramework.EnableContentHostMode();

            msguiFramework.Start();
        }
        

        public class EnvModule : IModule
        {
            private readonly IUnityContainer _container;

            public EnvModule(IUnityContainer container_)
            {
                _container = container_;
            }

            public void Initialize()
            {
                //cw EnvironmentSupportEnabling :4
                /**In order to retrieve environment later in the application you can get an instance of IMSDesktopEnvironment from
                 * the Unity container.*/
                //{
                var env = _container.Resolve<IMSDesktopEnvironment>();
                Console.WriteLine("Environment: " + env.Environment);
                //}
            }
        }

    }
}
