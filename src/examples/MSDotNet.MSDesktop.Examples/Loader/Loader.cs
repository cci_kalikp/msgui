﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSDesktop.Examples.Loader
{
	public class About
	{
		public void About_()
		{
     
			/**
			 * The loader loader serves as a configurable common bootstrapper for MSDesktop applications. It
			   loads its configuration from the app.config and supports almost all configuration options for 
			   the framework, and supports the addition of boot-time modules if custom code is needed.
			 */
		}

		public void Availability()
		{ 
			/**
			 * The loader is availble in all MSDesktop release starting with 2012.36 in the Examples folder.
			 */
		}

	}

	public class Configuration
	{
		public void Overview()
		{
			/**
			 * The configuration is loaded from the MSDesktop node inside the app.config file. During startup, 
			   it is validated against a schema file to avoid configuration errors (this also enables intellisense
			   in Visual Studio for editing the configuration).
			 * 
			 * To create a sample configuration file as a starting point, launch the loader with the -msdesktop:writeDefaultConfig
			 * command line switch. This will create a config file named MorganStanley.Desktop.Loader.default.config.
			 */
		}

		public void Application_Information()
		{
			/**
			 * The <ApplicationInformation> section defines the application's identity. Options are:
                 * AppBarTitle - sets the name shown in launcher bar shell mode on the left of the launcher
                 * Name - The name of the application.
                 * IconPath - Network or local path to the application's icon.
                 * Version - Version string (alphanumeric).
			 */
		}

		public void GUI()
		{

		}

		public void Layout_and_persistence()
		{

		}

		public void Splash()
		{

		}

		public void Boot_modules()
		{
			/**
			 * Boot modules can be used to insert custom logic into the launcher. These should be classes implementing 
			 * the IModule interface, with a constructor taking one argument, a Framework instance. After startup, the 
			 * Initialize method will be called by the loader.
			 */

			/**
			 * Example
			 * 
			 * 
				<BootModules>
				  <Module>My.SampleModule, My.Assembly</Module>
				</BootModules>
			 */
		}

	}
}
