﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Diagnostics;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using NUnit.Framework;

namespace MorganStanley.MSDotNet.MSDesktop.Examples.Diagnostics
{
  [TestFixture]
  public class RenderCapabilities
  {
    public void What_is_RenderCapabilities()
    {
      /**
       * RenderCapability enables WPF applications to query for the current rendering tier for their associated Dispatcher object and to register for notification of changes.
       * The most important property of RenderCapability is 'Tier', that gives a result from the following possible values:
       * 
       * <table border="2">
       *  <tr>
       *    <td>
       *      Rendering tier
       *    </td>
       *    <td>
       *      Notes
       *    </td>
       *  </tr>
       *  <tr>
       *    <td>
       *      0
       *    </td>
       *    <td>
       *      No graphics hardware acceleration is available for the application on the device. All graphics features use software acceleration. The DirectX version level is less than version 9.0.
       *    </td>
       *  </tr>
       *  <tr>
       *    <td>
       *      1
       *    </td>
       *    <td>
       *      Most of the graphics features of WPF will use hardware acceleration if the necessary system resources are available and have not been exhausted. This corresponds to a DirectX version that is greater than or equal to 9.0.
       *    </td>
       *  </tr>
       *  <tr>
       *    <td>
       *      2
       *    </td>
       *    <td>
       *      Most of the graphics features of WPF will use hardware acceleration provided the necessary system resources have not been exhausted. This corresponds to a DirectX version that is greater than or equal to 9.0.
       *      <br/>
       * 
       *      4 or more multitexture units and at least 2.0 version of vertex shading needed for this next to being at least 120 MB of video RAM.
       *    </td>
       *  </tr>
       * </table>
       * 
       * As MSDesktop do use advanced GFX capabilities, it's strongly suggested to have an environment having Tier 2 capabilites. To be able to track the current setting, a diagnostics module was created.
       */

    }

    [TestCase()]
    public void How_to_set_up_diagnostics_for_RenderCapabilities_changes()
    {
      /**
       * To set up the logger for RenderCapabilites' Tier changes, you need to set up this module in your MSDesktop setup piece:
       */
      Framework boot = new Framework();

      //{
      boot.AddModule<MorganStanley.MSDotNet.MSGui.Impl.Diagnostics.RenderCapabilityLogger>();
      //}

      /**
       * This will produce logging lines similar to this:
       */

      /**[code]
       *     HandleTierChanged: WPF rendering tier:2
       */
    }
  }
}
