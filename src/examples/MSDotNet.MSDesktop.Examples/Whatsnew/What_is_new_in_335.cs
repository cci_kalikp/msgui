﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace MorganStanley.MSDotNet.MSDesktop.Examples.What_is_new
{
  [TestFixture]
  public class What_is_new_in_2011_335_0
  {
    public void What_is_new_in_version_2011_335_0()
    {
      /** 
       * * An optional logger has been added for RenderCapabilities' Tier changes, [see more](#MorganStanley.MSDotNet.MSDesktop.Examples.Diagnostics.RenderCapabilities)
       * * Option has been added to add items to the QAT area on the ribbon from the 'Orb' menu, [see more](#MorganStanley.MSDotNet.MSDesktop.Examples.Ribbon.Adding_items_to_QAT_from_Orb_menu)
       */
    }
  }
}