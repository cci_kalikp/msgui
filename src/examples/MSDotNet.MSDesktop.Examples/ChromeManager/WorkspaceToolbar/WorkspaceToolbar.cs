﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Xml.Linq;
using System.Collections.ObjectModel;

namespace MorganStanley.MSDotNet.MSDesktop.Examples.ChromeManager.WorkspaceToolbar
{
    public class WorkspaceToolbar : WidgetViewContainer
    {
        [FactoryOptions(typeof(WorkspaceToolbar))]
        public static bool Factory(IWidgetViewContainer vc, XDocument state)
        {
            vc.Content = new WorkspaceToolbarView()
            {
                DataContext = vc
            };
            
            return true;
        }

        public WorkspaceToolbar(string factoryID, InitialWidgetParameters parameters)
            : base(factoryID, parameters)
        {
            this.LeftItems = this.AddWidget(Guid.NewGuid().ToString(), new InitialWidgetParameters());
            this.RightItems = this.AddWidget(Guid.NewGuid().ToString(), new InitialWidgetParameters());
        }

        public readonly IWidgetViewContainer LeftItems;
        public ReadOnlyObservableCollection<object> WestSideItems
        {
            get
            {
                return (LeftItems as WidgetViewContainer).VisualChildren;
            }
        }

        public readonly IWidgetViewContainer RightItems;
        public ReadOnlyObservableCollection<object> EastSideItems
        {
            get
            {
                return (RightItems as WidgetViewContainer).VisualChildren;
            }
        }

        #region WidgetViewContainer overrides

        #endregion
    }

    public class WorkspaceToolbarParameters : InitialWidgetParameters
    {

    }
}
