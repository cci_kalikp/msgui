﻿using System;
using System.Collections.Specialized;
using System.Windows.Media;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSDesktop.Examples.ChromeManager.WorkspaceToolbar;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;

namespace MorganStanley.MSDotNet.MSDesktop.Examples.ChromeManager
{
    public class CM20SampleModule : IModule
    {
        private IChromeRegistry chromeRegistry;
        private IChromeManager chromeManager;

        public CM20SampleModule(IChromeRegistry chromeRegistry, IChromeManager chromeManager)
        {
            this.chromeRegistry = chromeRegistry;
            this.chromeManager = chromeManager;

            this.chromeRegistry.RegisterWidgetFactoryMapping<WorkspaceToolbarParameters>(WorkspaceToolbar.WorkspaceToolbar.Factory);
        }

        #region IModule Members

        public void Initialize()
        {
            // get a reference to the main tabwell of the shell
            var tabs = chromeManager.Tabs();
            tabs.AddTab("MyNewTab");

            // add a hook to the CollectionChanged event that adds the window dropdown button to new tabs
            (tabs.Tabs as INotifyCollectionChanged).CollectionChanged += (s, e) =>
            {
                if (e.NewItems != null)
                {
                    foreach (IDockTab tab in e.NewItems)
                    {
                        AddWindowCounter(tab);
                    }
                }
            };

            // add a button to the right edge of the tabwell
            tabs.RightEdge.AddWidget("AddTab_A", new InitialButtonParameters()
            {
                Text = "A",
                Click = (s, e) =>
                {
                    // when the button is clicked, create a tab and set up the toolbar inside it
                    var tab = tabs.AddTab("Type A");
                    tab.Toolbar = this.chromeManager.AddWidget(Guid.NewGuid().ToString(), new WorkspaceToolbarParameters()) as WorkspaceToolbar.WorkspaceToolbar;
                    var toolbar = tab.Toolbar as WorkspaceToolbar.WorkspaceToolbar;
                    toolbar.LeftItems.AddWidget(Guid.NewGuid().ToString(), new InitialShowWindowButtonParameters()
                    {
                        Text = "Publisher",
                        WindowFactoryID = @"\\LauncherModeExample\SimpleModule\SimpleView"
                    });

                    toolbar.RightItems.AddWidget(Guid.NewGuid().ToString(), new InitialButtonParameters()
                    {
                        Image = System.Drawing.SystemIcons.Question.ToImageSource()
                    });
                }
            });

            // add a button to the right edge of the tabwell
            tabs.RightEdge.AddWidget("AddTab_B", new InitialButtonParameters()
            {
                Text = "B",
                Click = (s, e) =>
                {
                    // when the button is clicked, create a tab and set up the toolbar inside it
                    var tab = tabs.AddTab("Type B");
                    tab.Toolbar = this.chromeManager.AddWidget(Guid.NewGuid().ToString(), new WorkspaceToolbarParameters()) as WorkspaceToolbar.WorkspaceToolbar;
                    var toolbar = tab.Toolbar as WorkspaceToolbar.WorkspaceToolbar;

                    toolbar.LeftItems.AddWidget(Guid.NewGuid().ToString(), new InitialShowWindowButtonParameters()
                    {
                        Text = "Subscriber",
                        WindowFactoryID = @"\\LauncherModeExample\SimpleModule\SimpleView-SizeToContent"
                    });

                    toolbar.RightItems.AddWidget(Guid.NewGuid().ToString(), new InitialButtonParameters()
                    {
                        Image = System.Drawing.SystemIcons.Shield.ToImageSource()
                    });
                }
            });


            // close the default tab
            tabs["Main"].Close();

            var ribbon = this.chromeManager.Ribbon();
            var sampleContext = ribbon.AddContext("sampleRibbonContext", "Foo", Colors.Red);
            sampleContext.AddTab("Foo").AddGroup("Bar").AddWidget("aaaOHAI Context", new InitialButtonParameters()
            {
                Text = "OHAI Context"
            });

            var sampleContext2 = ribbon.AddContext("sampleRibbonContext2", "Bar", Colors.Green);
            sampleContext2.AddTab("Bar").AddGroup("Bar2").AddWidget("aaaOHAI Context 2", new InitialButtonParameters()
            {
                Text = "OHAI Context 2"
            });

            var fooGroup = chromeManager["Ribbon"]["Context sample"]["Context:foo"];
            fooGroup.AddWidget("vvvShow", new InitialButtonParameters()
            {
                Text = "Show",
                Size = ButtonSize.Small,
                Click = (s, e) => { sampleContext.Show(); }
            });
            fooGroup.AddWidget("vvvShow +", new InitialButtonParameters()
            {
                Text = "Show +",
                Size = ButtonSize.Small,
                Click = (s, e) => { sampleContext.Show(false); }
            });
            fooGroup.AddWidget("vvvhide", new InitialButtonParameters()
            {
                Text = "hide",
                Size = ButtonSize.Small,
                Click = (s, e) => { sampleContext.Hide(); }
            });

            var barGroup = chromeManager["Ribbon"]["Context sample"]["Context:bar"];
            barGroup.AddWidget("zzzShow", new InitialButtonParameters()
            {
                Text = "Show",
                Size = ButtonSize.Small,
                Click = (s, e) => { sampleContext2.Show(); }
            });
            barGroup.AddWidget("zzzShow +", new InitialButtonParameters()
            {
                Text = "Show +",
                Size = ButtonSize.Small,
                Click = (s, e) => { sampleContext2.Show(false); }
            });
            barGroup.AddWidget("zzzHide", new InitialButtonParameters()
            {
                Text = "Hide",
                Size = ButtonSize.Small,
                Click = (s, e) => { sampleContext2.Hide(); }
            });

        }

        private void AddWindowCounter(IDockTab tab)
        {
            IDropdownButtonViewContainer tabButton = null;
            tabButton = tab.HeaderPostItems.AddWidget(Guid.NewGuid().ToString(), new InitialDropdownButtonParameters()
            {
                Text = tab.Windows.Count.ToString(),
                Opening = (s, e) =>
                {
                    // here too, as the window headers might have changed.
                    UpdateTabDropdownItems(tabButton, tab);
                }

            }) as IDropdownButtonViewContainer;

            (tab.Windows as INotifyCollectionChanged).CollectionChanged += (s, e) =>
            {
                tabButton.Text = tab.Windows.Count.ToString();
                UpdateTabDropdownItems(tabButton, tab);
            };
        }

        private void UpdateTabDropdownItems(IDropdownButtonViewContainer tabButton, IDockTab tab)
        {
            tabButton.Clear();
            foreach (var window in tab.Windows)
            {
                var w = window;
                tabButton.AddWidget(Guid.NewGuid().ToString(), new InitialButtonParameters()
                {
                    Text = w.Title,
                    Click = (s_, e_) => { w.Activate(); }
                });
            }
        }

        #endregion
    }
}
