﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using NUnit.Framework;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.Composite.Events;
using MorganStanley.MSDotNet.My;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Composite.Modularity;



namespace MorganStanley.MSDotNet.MSDesktop.Examples.Communicator
{
    [TestFixture]
    public class Module_2_module_comunication_examples
    {
      
       
        public void Define_the_data_that_will_be_passed_between_modules()
        {
            /**  We need to define a message object, this is very similar to DTO see .....
             */

        }
        //{
        public class StockPriceChange
        {
            public double Price { get; set; }
            public string StockName { get; set; }
            public override string ToString()
            {
                return StockName + ": " + Price;
            }
        }
        //}




        public void Define_module_that_will_send_data()
        {
            /**
             * We need a module that can publish data
             **/
        }
        //{
        public class SendingModule : IModule
        {
            private IModuleCommunicator<StockPriceChange> _communicator;            
           
            public SendingModule(IModuleCommunicator<StockPriceChange> communicator_)
            {
                _communicator = communicator_;
            }


            //... More functions

            //}

            /**             
             * When the sender do some real stuff, it may generate new data and publish to its receivers
             *              
             */

            //{            
            public void PublishMessage()
            {  
            
                StockPriceChange data = new StockPriceChange();
                data.StockName = "MS";
                data.Price = 20;
                _communicator.Publish(data);
            }
            //}

           
            public void Initialize()
            {

            }
            

        }
        


        public void A_module_that_subscribe_data()
        {
            /**
             * The following object definition will subscribe and handle data
             */
        }
        //{
        public class ReceivingModule: IModule
        {
            private IModuleCommunicator<StockPriceChange> _communicator;
            
            public ReceivingModule(IModuleCommunicator<StockPriceChange> communicator_)
            {
                _communicator = communicator_;
            }
          
            private void HandleMessage(StockPriceChange value)
            {
                Console.WriteLine(value);
            }
          
            public void BookMessage()
            {
                var filter = from message in _communicator.GetObservable()
                             where message.StockName.StartsWith("MS")
                             select message;

                filter.Subscribe(HandleMessage);               
            }
            //}

            public void Initialize()
            {
             
            }
            
        }
       

     

        [Test]
        public void Communication_between_modules()        
        {
            /** We usually use dependency injection to get hold of the "ModuleCommunicator" object, we will
             * only need to reference it by interface.
             */

            IEventAggregator eventAggregator = new ModuleToModuleMessageEventAggregator();            

            //{
            IModuleCommunicator<StockPriceChange> asyncCommunicator;
            //}

            asyncCommunicator = new ModuleCommunicator<StockPriceChange>(eventAggregator);            

           /**
            *  Initialize two module with the same asyncronized communicator
            */
            //{
            SendingModule sendingModule = new SendingModule(asyncCommunicator);
            ReceivingModule receivingModule = new ReceivingModule(asyncCommunicator);
            //}

            /**
             * Receiver book message from the communicator
             */
            //{
            receivingModule.BookMessage();
            //}
            sendingModule.Initialize();
            receivingModule.Initialize();

            /**
             * When sender publish message, receivingModule will receive it 
             */
            //{
            sendingModule.PublishMessage();
            //}

            Console.ReadLine();
        }
    }
}
