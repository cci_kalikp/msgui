﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/examples/MSDotNet.MSDesktop.Examples/Ribbon/QAT_Orb.cs#7 $
// $Change: 857738 $
// $DateTime: 2013/12/05 22:38:43 $
// $Author: caijin $

using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ShellMenu;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;


namespace MorganStanley.MSDotNet.MSDesktop.Examples.Ribbon
{
  //cw Ribbon_QAT :20
	// ReSharper disable InconsistentNaming
	// ReSharper disable JoinDeclarationAndInitializer
	// ReSharper disable PossibleNullReferenceException
	public class Adding_items_to_QAT_from_Orb_menu
	{
		public void Overview()
		{
			/**
			 * MSDesktop exposes copies of the menu items used in the ribbon based shell window for client code
			 * use. The predominant use case would be to add these menu tools to the Quick Access Toolbar on the ribbon.
			 */
		}
		public void Adding_Shell_Menu_Tools_to_the_Quick_Access_Toolbar()
		{
			/**
			 * You will need to use the MorganStanley.MSDotNet.MSGui.Core.ChromeManager and
			 * MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ShellMenu namespaces, and acquire an IChromeManager
			 * instance (through the Unity Container, for example by specifying them as your modules' constructor arguments).
			 */
			
			//{
			IChromeManager m_chromeManager;
			//}
			m_chromeManager = null;
		
			/**
			 * The first step is acquiring the reference to the desired menu tool and registering it with an ID
			 * using the IChromeRegistry.  
			 */
			// ReSharper disable ConvertToConstant.Local
			//{
			m_chromeManager.PlaceWidget(m_chromeManager.SaveLayoutAsMenuFactory(), ChromeArea.QAT, "", null);

 
			//}
			// ReSharper restore ConvertToConstant.Local
			//}
		}
		// ReSharper restore PossibleNullReferenceException
		// ReSharper restore JoinDeclarationAndInitializer
		// ReSharper restore InconsistentNaming
	}
}