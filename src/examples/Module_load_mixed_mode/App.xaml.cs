﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
//cw Add_Module_Via_AddModule_Call :25 cw Reflection_Module_Load :25
/**##namespace*/
/**use the following namespace*/
//{
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
//}
using System.Windows.Media.Imaging;
using System.IO;
using MorganStanley.MSDotNet.MSLog;
using System.Diagnostics;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Themes;
using Module_load_mixed_mode.Modules;


namespace Module_load_mixed_mode
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : Application
  {
    //cw Add_Module_Via_AddModule_Call :45 cw Reflection_Module_Load :45
    /**
     * * The `Framework.AddModule` method should be put in the `App.OnStartup` method
     */
    //{
    protected override void OnStartup(StartupEventArgs e)
    {
      base.OnStartup(e);
      var boot = new Framework();
      boot.AddSimpleTheme();
      // add your code here:
    //}
      //cw Add_Module_Via_AddModule_Call :60
      /** you can use the Framework.AddModule extension method, as follow*/

      //{
      boot.AddModule<ModuleViaMSDesktopFramework>();
      //}

      //{
      //uncomment this code to enable reflection based module load
      //}

      //cw Add_Module_Via_AddModule_Call :45 cw Reflection_Module_Load :45
    //{
      boot.Start();
    }
    //}

    //cw Add_Module_Via_AddModule_Call :60
    private void AlternativeAddModule()
    {
      var boot = new Framework();
      /**an alternative would be*/
      //{
      boot.AddModule(typeof(ModuleViaMSDesktopFramework));
      //}
    }


    private void EnableReflectionBasedModuleLoad(Framework boot)
    {
      //cw Reflection_Module_Load :10
      /**##description*/
      /**Enable automatical loading of modules which implements `IModule` interface*/
      //cw Reflection_Module_Load :40
      /**##context*/

      //cw Reflection_Module_Load :50
      /**##code*/
      /**move the code to the `App.OnStartup` to enable reflection based module load*/
      //{
      boot.EnableReflectionModuleLoad();
      //}
      /**##references*/
      /**
       * 
        ** [Add module via AddModule method][cw_ref Add_Module_Via_AddModule_Call]
        ** [Add module via App Config][cw_ref Add_Module_Via_App_Config]
       */

    }
  }

 
}
