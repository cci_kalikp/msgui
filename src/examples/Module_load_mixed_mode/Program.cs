﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MorganStanley.MSDotNet.Runtime;


namespace Module_load_mixed_mode
{
 
  public class Program
  {
    
    [STAThread]
    public static void Main(string[] args)
    {
      AssemblyResolver.RunMain(args);
    }
  
    [MSDEMain]
    public static void RealMain(string[] args)
    {
      var app = new App();
      app.Run();
    }
  
  }

}
