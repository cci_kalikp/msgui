﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//cw Add_Module_Via_App_Config :10
/**##description*/
/**This page shows how to configure modules to load in MSDesktop via the app.config file*/
/**##namespace*/
/**IModule*/
//{
using Microsoft.Practices.Composite.Modularity;
//}

namespace Module_load_mixed_mode.Modules
{
  /**##context*/
  //cw Add_Module_Via_App_Config :40  
  /**You may start by creating the MSDesktop project by following 
   * 
 ** [GetStarted with Assembly Resolver][cw_ref GetStarted_Non_AR]
 ** [GetStarted with Copy Local][cw_ref GetStarted_With_AR]
   *
   */

  //cw Add_Module_Via_App_Config :50
  /**##code*/
  /**first, create a module, by implementing hte IModule interface*/
  //{
  public class ModuleViaAppConfig : IModule
  {
    public void Initialize()
    {
      // put your initialization code here...
    }
  }
  //}
  //cw Add_Module_Via_App_Config :80
  /**##references*/
  /**
   ** [Add module via AddModule method][cw_ref Add_Module_Via_AddModule_Call]
   ** [Add module via reflection][cw_ref Reflection_Module_Load]
   */
}
