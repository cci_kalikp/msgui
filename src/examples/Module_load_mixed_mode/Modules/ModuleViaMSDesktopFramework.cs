﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//cw Add_Module_Via_AddModule_Call :10
/**##description*/
/**This page shows how to configure modules to load in MSDesktop via the app.config file*/
//cw Add_Module_Via_AddModule_Call :25
//{
using Microsoft.Practices.Composite.Modularity;
//}
/**##context*/
/**You may start by creating the MSDesktop project by following 
 * 
 ** [GetStarted with Assembly Resolver][cw_ref GetStarted_Non_AR]
 ** [GetStarted with Copy Local][cw_ref GetStarted_With_AR]
 */

//cw Add_Module_Via_AddModule_Call :55
/**##code*/

namespace Module_load_mixed_mode.Modules
{
  /**first, create a module, by implementing hte IModule interface*/
  //{
  public class ModuleViaMSDesktopFramework : IModule
  {
    public void Initialize()
    {
      // put your initialization code here...
    }
  }
  //}
  //cw Add_Module_Via_AddModule_Call :80
  /**##references*/
  /**
   ** [Add module via AddModule method][cw_ref Add_Module_Via_App_Config]
   ** [Add module via reflection][cw_ref Reflection_Module_Load]
   */
}
