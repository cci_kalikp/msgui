﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;


using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

using System.Windows.Media.Imaging;
using System.IO;
using MorganStanley.MSDotNet.MSLog;
using System.Diagnostics;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Themes;


namespace GetStarted_AR
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : Application
  {
   

    protected override void OnStartup(StartupEventArgs e)
    {
      base.OnStartup(e);
      var boot = new Framework();
      boot.AddSimpleTheme();
      // ...
      boot.Start();

    }
  }
}
