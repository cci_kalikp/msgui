﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//cw Define_AR_Startup_Object
/**##description*/
/** Start an empty MSDeskto project with AR (Assembly resolver) You can download the example to start playing with*/
/**##namespace*/
//{
using MorganStanley.MSDotNet.Runtime;
//}

namespace GetStarted_AR
{
  /**##context*/
  /**you will need to do the following in advance
   * 
   ** Create an empty WPF project
   ** remove the Startup Uri
   ** create/add appropriate .msde.config files
   ** Binding redirect and the Dotnet4 issues
   ** Set the correct startup object to `Program.Main`
   ** override the App OnStartup method
   *
   */
  /**##code*/
  /**Create entry function Program.Main*/
  public class Program
  {
    /**####Setup AssemblyResolver*/
    /**remember to decorate with `[STAThread]` attribute*/
    //{
    [STAThread]
    public static void Main(string[] args)
    {
      AssemblyResolver.RunMain(args);
    }
    //}

    /**####starts App*/
    /**in the RealMain of Assembly Resolver, create and start the App*/
    //{
    [MSDEMain]
    public static void RealMain(string[] args)
    {
      var app = new App();
      app.Run();
    }
    //}
  }
  /**##references*/
  /**
   *+ [remove the Startup Uri][cw_ref GetStarted_Remove_Start_Uri]
   *+ [create/add appropriate .msde.config files](http://wiki-eu.ms.com/twiki/cgi-bin/view/User/UserBoqwang/MSDesktopDocs_GetStarted_AR_Config_Files?skin=plain)
   *+ [Dotnet4 issues][cw_ref GetStarted_Dotnet4_Issues]
   *+ [Binding redirect][cw_ref GetStarted_Binding_Redirect]
   *+ [Override App.OnStartup][cw_ref GetStarted_App_OnStartup]
   */
}
