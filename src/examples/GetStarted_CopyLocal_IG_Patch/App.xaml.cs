﻿using System.Windows;
//cw GetStarted_App_OnStartup :30
/**##namespaces*/
/** use the following namespace*/
//{
using MorganStanley.MSDotNet.MSGui.Impl;
//}
using MorganStanley.MSDotNet.MSGui.Themes;


namespace GetStarted_CopyLocal_IG_Patch
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        //cw GetStarted_App_OnStartup :50
        /**##description*/
        /** Override OnStartup method to bootstrapp MSDesktop.
     */

        /**##context*/
        /**override the Application.OnStartup(StartupEventArgs e) method in App.xaml.cs file. */

        /**
    What you need beforehand    
    
     * * 1. Create a new WPF application
     * * 2. Remove the StartupUri attribute from App.xaml see [here][cw_ref GetStarted_Remove_Start_Uri]
    */
        /**##code*/
        //{

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var boot = new Framework();
            boot.AddSimpleTheme();
            boot.Start();
        }

        //}
        /**##references*/
        /**
     * + [Howto: Get Started with AR (Assembly Resolver)][cw_ref GetStarted_With_AR]
     */
    }

    #region Ignore

    //cw GetStarted_Remove_Start_Uri :50
    /**##description*/
    /** You may remove the `StartUri` attribute created by the Visual Studio Project Wizard.
   */
    /**##context*/
    /** 
   You may
   
   * * 1.Create a WPF application and then Open the App.xaml (not the code-behind file App.xaml.cs file) 
   * * 2.remove `StartupUri="MainWindow.xaml"`
   * * 3.remove MainWindow.xaml  
   */

    /**##code*/
    /**[raw]
    * <div>  
   &lt;Application x:Class="GetStarted_CopyLocal_IG_Patch.App"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             <strike>StartupUri="MainWindow.xaml"</strike>
             &gt;</div>
   */


    //cw GetStarted_Dotnet4_Issues :30
    /**##description*/
    /** Work around the DotNet 4 security issues
   */
    /**##context*/
    /** Put the configuration below to the **app.config** file.  */

    /**##code*/

    //cw GetStarted_Dotnet4_Issues :60
    /**##references*/
    /**
   * [DotNet4 Issues](http://wiki.ms.com/twiki/cgi-bin/view/MSDotnet/DotNet4Issues)
   */

    //cw GetStarted_Binding_Redirect :50
    /**##description*/
    /** Sometimes you may use a different version of one assemblies from the its time version. This example Demonstrate how to create binding Redirect by an example on InfragisticsWPF4.v14.1
   */
    /**##context*/
    /** Put the Binding redirect configuration below to the **app.config** file.  */

    /**##code*/

    //cw GetStarted_Binding_Redirect :60
    /**##references*/
    /**
   * [Assmebly Binding Redirection](http://msdn.microsoft.com/en-us/library/2fc472t2(v=vs.80).aspx)
   */

    #endregion Ignore
}

//cw GetStarted_Project_setup_and_references :1
/**
 * ##Project setup
 * In order to ensure the correct project setup, please follow these steps:
 * 
 * <ul>
 *  <li> Go to project preferences
 *  <li> Change Target Framework setting to ".NET Framework 4" (without "Client Profile")
 * </ul>
 * 
 * Next, follow steps in this document and in the remaining documents in this node.
 * 
 * ##Project references
 * The easiest way to get the references right is to copy them from the example: 
 * 
 * <ul>
 *  <li> Please use the download button at the top of this page
 *  <li> Open the downloaded csproj file in text mode
 *  <li> Find the ItemGroup containing References (not ProjectReferences)
 *  <li> Copy this ItemGroup and replace the relevant ItemGroup in your project file
 *  <li> Now you have added the transitive references. Additionally, you need to add references to MSDesktop itself.
 *      Please use the latest version from AFS and reference the following assemblies. Remember to set them 
 *      to Copy Local.
 *      <ul>
 *          <li> MSDotNet.MSGui.Core
 *          <li> MSDotNet.MSGui.Impl
 *          <li> MSDotNet.MSGui.Controls
 *          <li> MSDotNet.MSGui.Themes
 *          <li> Concord.Configuration
 *      </ul>
 *  <li> Reload your project
 * </ul>
 * 
 * ##Assembly bindings
 * Again, the easiest thing to do is to copy from the example:
 * 
 * <ul>
 *  <li> Please use the download button at the top of this page
 *  <li> Open the app.config file
 *  <li> Copy all assemblyBinding nodes to your app.config file
 *  <li> At this point you can also copy <b>loadFromRemoteSources</b> and <b>NetFx40_LegacySecurityPolicy</b> nodes.
 *      Depending on your caspol settings, you may need either both or one of them. Please see references
 *      below for details.
 * </ul>
 */

/** ##References
 * + [Resolving .NET 4.0 issues][cw_ref GetStarted_Dotnet4_Issues]
 */