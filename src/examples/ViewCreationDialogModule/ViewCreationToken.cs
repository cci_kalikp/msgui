﻿using System;
using System.Drawing;

namespace MorganStanley.MSDotNet.MSGui
{
  public class ViewCreationToken
  {    
    public string Group { get; set; }    
    public string Name { get; set; }
    public Bitmap Image { get; set; }
    public string Description { get; set; }
    internal string CreatorId { get; set; }
  }
}
