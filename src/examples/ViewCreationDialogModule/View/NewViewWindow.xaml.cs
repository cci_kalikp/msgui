﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Collections.ObjectModel;
using System.Windows.Media.Effects;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MorganStanley.MSDotNet.MSGui.View
{
  /// <summary>
  /// Interaction logic for NewViewWindow.xaml
  /// </summary>
  public partial class NewViewWindow : UserControl
  {
    ExtendedViewManager m_viewManager = null;
    public ObservableCollection<ViewCreationToken> ViewCreators { get; set; }

    public NewViewWindow(ExtendedViewManager viewManager_)
    {
      m_viewManager = viewManager_;
      ViewCreators = new ObservableCollection<ViewCreationToken>(viewManager_.Tokens);
      //m_creator.CreateViewToken += OnCreateViewToken;

      this.DataContext = this;
      InitializeComponent();

      lstGroups.SelectionChanged += new SelectionChangedEventHandler(lstGroups_SelectionChanged);

      lstGroups.SelectedIndex = 0;
    }

    void lstGroups_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      lstSelectedLevel.SelectedIndex = 0;
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      ViewCreationToken tok = lstSelectedLevel.SelectedValue as ViewCreationToken;

      if (tok != null)
      {
        m_viewManager.ViewManager.CreateView(tok.CreatorId);        
      }

      var win = Window.GetWindow(this);
      win.Close();
    }

    private void btnCancelClick(object sender, RoutedEventArgs e)
    {
      var win = Window.GetWindow(this);
      win.Close();
    }

    private void btnZoom_Click(object sender, RoutedEventArgs e)
    {
      if (zoomGrid.Visibility == Visibility.Visible)
      {
        zoomGrid2.Visibility = Visibility.Collapsed;
        zoomGrid.Visibility = Visibility.Collapsed;
        mainGrid.Effect = null;
        mainGrid.Background = Brushes.Transparent;
        return;
      }
      zoomGrid2.Visibility = Visibility.Visible;
      zoomGrid.Visibility = Visibility.Visible;
      //if (mainGrid.Effect != null)
      //{
      //    zoomGrid.Visibility = Visibility.Collapsed;
      //    mainGrid.Effect = null;
      //    mainGrid.Background = Brushes.Transparent;
      //    return;
      //}

      //zoomGrid.Visibility = Visibility.Visible;
      mainGrid.Background = Brushes.DarkGray;

      var blur = new BlurEffect();
      blur.Radius = 6;
      blur.KernelType = KernelType.Box;

      mainGrid.Effect = blur;      
    }
    protected void HandleDoubleClick(object sender, MouseButtonEventArgs e)
    {
      TaskDialog.ShowMessage("aaaaa");
      //var track = ((ListViewItem)sender).Content as Track; //Casting back to the binded Track
    }
  }
}