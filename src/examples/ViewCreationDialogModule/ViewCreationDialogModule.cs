﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/examples/ViewCreationDialogModule/ViewCreationDialogModule.cs#7 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Drawing;
using System.Windows;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;
using MorganStanley.MSDotNet.MSGui.View;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MorganStanley.MSDotNet.MSGui
{
  public class ViewCreationDialogModule : IModule
  {
    private readonly IViewManager m_viewManager;
    private readonly IControlParadigmRegistrator m_ribbon;
    private readonly IPersistenceService m_persistenceService;
    private readonly IUnityContainer m_container;

    public ViewCreationDialogModule(
      IControlParadigmRegistrator ribbon_,
      IViewManager viewManager_,
      IPersistenceService persistenceService_,
      IUnityContainer container_)
    {
      m_ribbon = ribbon_;
      m_viewManager = viewManager_;
      m_persistenceService = persistenceService_;
      m_container = container_;
    }

    public void Initialize()
    {
      ExtendedViewManager evm = new ExtendedViewManager(m_viewManager);
      m_container.RegisterInstance(evm);
      m_ribbon.AddMenuItem(
        new RibbonButton(
          SystemIcons.WinLogo,
          "Create View",
          RibbonButtonSize.Large,
          "Create View",
        delegate
        {

          //var viewReg = m_container.Resolve<IRegisterViewCreator>();

          if (evm.Tokens.Count > 0)
          {
            var view = new NewViewWindow(evm);
            var win = new Window
                        {
                          Content = view,
                          WindowStartupLocation = WindowStartupLocation.CenterScreen
                        };

            win.ShowDialog();
          }
          else
          {
            TaskDialog.ShowMessage("No Views Avaliable", "Error", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
          }
        }));
    }
  }
}
