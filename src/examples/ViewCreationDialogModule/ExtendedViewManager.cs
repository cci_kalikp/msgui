﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/examples/ViewCreationDialogModule/ExtendedViewManager.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Collections.Generic;
using System.Collections.ObjectModel;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui
{
  public class ExtendedViewManager
  {
    private readonly IViewManager m_viewManager;
    private readonly IList<ViewCreationToken> m_tokens = new ObservableCollection<ViewCreationToken>();

    public IList<ViewCreationToken> Tokens
    {
      get
      {
        return m_tokens;
      }
    }

    public IViewManager ViewManager
    {
      get
      {
        return m_viewManager;
      }
    }

    public ExtendedViewManager(IViewManager viewManager_)
    {
      m_viewManager = viewManager_;
    }

    public void AddViewCreator(string creatorId_, InitialiseViewHandler initialiseViewHandler_, ViewCreationToken token_)
    {
      m_viewManager.AddViewCreator(creatorId_, initialiseViewHandler_);
      token_.CreatorId = creatorId_;
      m_tokens.Add(token_);
    }

    public void AddViewCreator(string creatorId_, RestoreViewHandler restoreViewHandler_, SaveViewHandler saveViewHandler_, ViewCreationToken token_)
    {
      m_viewManager.AddViewCreator(creatorId_, restoreViewHandler_, saveViewHandler_);
      token_.CreatorId = creatorId_;
      m_tokens.Add(token_);
    }
  }
}

