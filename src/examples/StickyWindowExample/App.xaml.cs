﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using MSDesktop.CrossMachineApplication.Extensions;
using MorganStanley.MSDesktop.IPC;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.MSGui.Themes;

namespace StickyWindowExample
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var boot = new Framework();
            boot.AddSimpleTheme();
            boot.AddModule<V2VConfigModule>();
            boot.AddModule<StickyWindowsModule>();
            if (false)
            {
                boot.EnableStickinessForAllWindows(StickyWindowOptions.StickToAll);
            }
            
            boot.Start();

        }
    }
}
