﻿using System;
using System.Reactive.Linq;
using System.Windows.Controls;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MSDesktop.CrossMachineApplication;
using MSDesktop.CrossMachineApplication.Messages;

namespace StickyWindowExample
{
    internal class StickyWindowsModule : IModule
    {
        private const string SimpleWindowID = "SimpleWindowID";
        private const string SimpleWindowButtonID = "SimpleWindowButtonID";
        private const string TransferablePublisherWindowID = "TransferablePublisherWindowID";
        private const string TransferablePublisherWindowButtonID = "TransferablePublisherWindowButtonID";
        private const string TransferableSubscriberWindowID = "TransferableSubscriberWindowID";
        private const string TransferableSubscriberWindowButtonID = "TransferableSubscriberWindowButtonID";

        public StickyWindowsModule(IChromeRegistry chromeRegistry, IChromeManager chromeManager, IEventRegistrator eventRegistrator/*, IWindowTransferManager transferManager*/)
        {
            m_chromeManager = chromeManager;
            m_chromeRegistry = chromeRegistry;
            m_eventRegistrator = eventRegistrator;
            // m_transferManager = transferManager;
        }

        public void Initialize()
        {
            m_chromeRegistry.RegisterWindowFactory(SimpleWindowID, CreateSimpleWindow);
            m_chromeManager.PlaceWidget(SimpleWindowButtonID, "DD/Views", new InitialShowWindowButtonParameters
            {
                WindowFactoryID = SimpleWindowID,
                Text = "Transferable",
            });
            m_chromeRegistry.RegisterWindowFactory(TransferablePublisherWindowID, CreateTransferablePublisherWindow);
            m_chromeManager.PlaceWidget(TransferablePublisherWindowButtonID, "DD/Views", new InitialShowWindowButtonParameters
            {
                WindowFactoryID = TransferablePublisherWindowID,
                Text = "Transferable publisher",
            });
            m_chromeRegistry.RegisterWindowFactory(TransferableSubscriberWindowID, CreateTransferableSubscriberWindow);
            m_chromeManager.PlaceWidget(TransferableSubscriberWindowButtonID, "DD/Views", new InitialShowWindowButtonParameters
            {
                WindowFactoryID = TransferableSubscriberWindowID,
                Text = "Transferable subscriber",
            });
        }

        private static void PostCreateWindow(object sender, WindowEventArgs e)
        {
            e.ViewContainer.RegisterAsSticky();
            e.ViewContainer.Created -= PostCreateWindow;
        }

        private static bool CreateSimpleWindow(IWindowViewContainer emptyViewContainer, XDocument state)
        {
            TextBox textBox = emptyViewContainer.Content as TextBox;
            if (textBox == null)
            {
                textBox = new TextBox();
                textBox.Width = 300;
                emptyViewContainer.Content = textBox;
            }
            if (state != null)
            {
                textBox.Text = state.Root.Value;
            }
            emptyViewContainer.Title = "Transferable window";
            emptyViewContainer.Created += PostCreateWindow;

            AddHostInformation(emptyViewContainer);
            return true;
        }

        private bool CreateTransferablePublisherWindow(IWindowViewContainer emptyViewContainer, XDocument state)
        {
            var userInputTextBox = new TextBox();

            var sendButton = new Button() { Content = "Send" };

            var publisher = m_eventRegistrator.GetRegisteredPublisher<SampleMessage>(emptyViewContainer);
            if (publisher == null)
            {
                publisher = m_eventRegistrator.RegisterPublisher<SampleMessage>(emptyViewContainer);
            }
            // sendButton.Click += (s, e) => m_transferManager.RemotePublish(new SampleMessage { UserInput = userInputTextBox.Text }, emptyViewContainer);

            StackPanel panel = new StackPanel();
            panel.Orientation = Orientation.Vertical;
            panel.Children.Add(userInputTextBox);
            panel.Children.Add(sendButton);

            emptyViewContainer.Title = "Transferable publisher";
            emptyViewContainer.Content = panel;
            emptyViewContainer.Created += PostCreateWindow;

            AddHostInformation(emptyViewContainer);

            return true;
        }

        private bool CreateTransferableSubscriberWindow(IWindowViewContainer viewContainer, XDocument state)
        {
            var listBox = viewContainer.Content as ListBox;
            if (listBox == null)
            {
                listBox = new ListBox();
                var subscriber = m_eventRegistrator.RegisterSubscriber<SampleMessage>(viewContainer);
                subscriber.GetObservable().ObserveOnDispatcher().Subscribe(m => listBox.Items.Insert(0, m.UserInput));
            }
            if (state != null)
            {
                listBox.Items.Clear();
                foreach (var itemEl in state.Root.Elements("Item"))
                {
                    listBox.Items.Add(itemEl.Value);
                }
            }

            viewContainer.Title = "Transferable subscriber";
            viewContainer.Content = listBox;
            viewContainer.Created += PostCreateWindow;

            AddHostInformation(viewContainer);

            return true;
        }

        static void AddHostInformation(IWindowViewContainer viewContainer)
        {
            if (viewContainer.Parameters is InitialTransferableWindowParameters)
            {
                var hostInfo = new TextBlock
                {
                    Text = String.Format("[{0}]", (viewContainer.Parameters as InitialTransferableWindowParameters).OriginHost),
                    FontSize = 12,
                };
                viewContainer.HeaderItems.Clear();
                viewContainer.HeaderItems.Add(hostInfo);
            }
        }

        IChromeRegistry m_chromeRegistry;
        IChromeManager m_chromeManager;
        IEventRegistrator m_eventRegistrator;
        // private readonly IWindowTransferManager m_transferManager;
    }

    public class SampleMessage : ITransferableV2VMessage
    {
        public string UserInput { get; set; }

        XDocument ITransferableV2VMessage.SaveState()
        {
            XDocument doc = new XDocument();
            XElement el = new XElement("UserInput", UserInput);
            doc.Add(el);
            return doc;
        }

        void ITransferableV2VMessage.LoadState(XDocument state)
        {
            UserInput = state.Root.Value;
        }
    }
}
