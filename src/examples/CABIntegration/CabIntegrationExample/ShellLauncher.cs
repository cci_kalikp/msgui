using System;
using System.Windows.Forms;
using Infragistics.Practices.CompositeUI.WinForms;
using Microsoft.Practices.CompositeUI;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace CabIntegrationExample
{
    public class ShellLauncher : IGFormShellApplication<WorkItem, MainFrm2>
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            new ShellLauncher().Run();
        }

        protected override void Start()
        {
            msDesktop.SetApplicationName((this.Shell as Form).Text);
            msDesktop.AddModule<NavigationControlModule>();
            msDesktop.Start();
        }

        protected override void BeforeShellCreated()
        {
        }

        protected override void AfterShellCreated()
        {
            base.AfterShellCreated();

            //register the site (aka: location with CAB) that contains the UltraToolbarsManager. 
            this.RootWorkItem.UIExtensionSites.RegisterSite("Main", Shell.ultraToolbarsManager1.Toolbars["Main"]);
            this.RootWorkItem.UIExtensionSites.RegisterSite("Menu", Shell.MainMenuStrip);

            NavigationControlModule.RootWorkItem = RootWorkItem;
        }
    }
}