namespace CabIntegrationExample
{
    partial class MainFrm2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar1 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("Main");
            this.DockWorkspace = new Infragistics.Practices.CompositeUI.WinForms.UltraDockWorkspace(this.components);
            this._MainFrm2UnpinnedTabAreaLeft = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
            this._MainFrm2UnpinnedTabAreaRight = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
            this._MainFrm2UnpinnedTabAreaTop = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
            this._MainFrm2UnpinnedTabAreaBottom = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
            this._MainFrm2AutoHideControl = new Infragistics.Win.UltraWinDock.AutoHideControl();
            this.ultraToolbarsManager1 = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
            this._MainFrm2_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._MainFrm2_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._MainFrm2_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._MainFrm2_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.MDIWorkspace = new Infragistics.Practices.CompositeUI.WinForms.UltraMdiTabWorkspace(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            ((System.ComponentModel.ISupportInitialize)(this.DockWorkspace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MDIWorkspace)).BeginInit();
            this.SuspendLayout();
            // 
            // DockWorkspace
            // 
            this.DockWorkspace.DragWindowColor = System.Drawing.Color.GreenYellow;
            this.DockWorkspace.DragWindowStyle = Infragistics.Win.UltraWinDock.DragWindowStyle.LayeredWindowWithIndicators;
            this.DockWorkspace.HostControl = this;
            this.DockWorkspace.SettingsKey = "";
            this.DockWorkspace.WindowStyle = Infragistics.Win.UltraWinDock.WindowStyle.Office2003;
            this.DockWorkspace.WorkspaceName = "DockWorkspace";
            // 
            // _MainFrm2UnpinnedTabAreaLeft
            // 
            this._MainFrm2UnpinnedTabAreaLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this._MainFrm2UnpinnedTabAreaLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MainFrm2UnpinnedTabAreaLeft.Location = new System.Drawing.Point(0, 50);
            this._MainFrm2UnpinnedTabAreaLeft.Name = "_MainFrm2UnpinnedTabAreaLeft";
            this._MainFrm2UnpinnedTabAreaLeft.Owner = this.DockWorkspace;
            this._MainFrm2UnpinnedTabAreaLeft.Size = new System.Drawing.Size(0, 396);
            this._MainFrm2UnpinnedTabAreaLeft.TabIndex = 3;
            // 
            // _MainFrm2UnpinnedTabAreaRight
            // 
            this._MainFrm2UnpinnedTabAreaRight.Dock = System.Windows.Forms.DockStyle.Right;
            this._MainFrm2UnpinnedTabAreaRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MainFrm2UnpinnedTabAreaRight.Location = new System.Drawing.Point(632, 50);
            this._MainFrm2UnpinnedTabAreaRight.Name = "_MainFrm2UnpinnedTabAreaRight";
            this._MainFrm2UnpinnedTabAreaRight.Owner = this.DockWorkspace;
            this._MainFrm2UnpinnedTabAreaRight.Size = new System.Drawing.Size(0, 396);
            this._MainFrm2UnpinnedTabAreaRight.TabIndex = 4;
            // 
            // _MainFrm2UnpinnedTabAreaTop
            // 
            this._MainFrm2UnpinnedTabAreaTop.Dock = System.Windows.Forms.DockStyle.Top;
            this._MainFrm2UnpinnedTabAreaTop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MainFrm2UnpinnedTabAreaTop.Location = new System.Drawing.Point(0, 50);
            this._MainFrm2UnpinnedTabAreaTop.Name = "_MainFrm2UnpinnedTabAreaTop";
            this._MainFrm2UnpinnedTabAreaTop.Owner = this.DockWorkspace;
            this._MainFrm2UnpinnedTabAreaTop.Size = new System.Drawing.Size(632, 0);
            this._MainFrm2UnpinnedTabAreaTop.TabIndex = 5;
            // 
            // _MainFrm2UnpinnedTabAreaBottom
            // 
            this._MainFrm2UnpinnedTabAreaBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._MainFrm2UnpinnedTabAreaBottom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MainFrm2UnpinnedTabAreaBottom.Location = new System.Drawing.Point(0, 446);
            this._MainFrm2UnpinnedTabAreaBottom.Name = "_MainFrm2UnpinnedTabAreaBottom";
            this._MainFrm2UnpinnedTabAreaBottom.Owner = this.DockWorkspace;
            this._MainFrm2UnpinnedTabAreaBottom.Size = new System.Drawing.Size(632, 0);
            this._MainFrm2UnpinnedTabAreaBottom.TabIndex = 6;
            // 
            // _MainFrm2AutoHideControl
            // 
            this._MainFrm2AutoHideControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MainFrm2AutoHideControl.Location = new System.Drawing.Point(0, 301);
            this._MainFrm2AutoHideControl.Name = "_MainFrm2AutoHideControl";
            this._MainFrm2AutoHideControl.Owner = this.DockWorkspace;
            this._MainFrm2AutoHideControl.Size = new System.Drawing.Size(699, 157);
            this._MainFrm2AutoHideControl.TabIndex = 7;
            // 
            // ultraToolbarsManager1
            // 
            this.ultraToolbarsManager1.DesignerFlags = 1;
            this.ultraToolbarsManager1.DockWithinContainer = this;
            this.ultraToolbarsManager1.DockWithinContainerBaseType = typeof(System.Windows.Forms.Form);
            this.ultraToolbarsManager1.SettingsKey = "";
            this.ultraToolbarsManager1.ShowFullMenusDelay = 500;
            this.ultraToolbarsManager1.Style = Infragistics.Win.UltraWinToolbars.ToolbarStyle.Office2003;
            ultraToolbar1.DockedColumn = 0;
            ultraToolbar1.DockedRow = 0;
            ultraToolbar1.Settings.ToolDisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.TextOnlyAlways;
            ultraToolbar1.Text = "Main";
            this.ultraToolbarsManager1.Toolbars.AddRange(new Infragistics.Win.UltraWinToolbars.UltraToolbar[] {
            ultraToolbar1});
            // 
            // _MainFrm2_Toolbars_Dock_Area_Left
            // 
            this._MainFrm2_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._MainFrm2_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(190)))), ((int)(((byte)(245)))));
            this._MainFrm2_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
            this._MainFrm2_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
            this._MainFrm2_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 50);
            this._MainFrm2_Toolbars_Dock_Area_Left.Name = "_MainFrm2_Toolbars_Dock_Area_Left";
            this._MainFrm2_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(0, 396);
            this._MainFrm2_Toolbars_Dock_Area_Left.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // _MainFrm2_Toolbars_Dock_Area_Right
            // 
            this._MainFrm2_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._MainFrm2_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(190)))), ((int)(((byte)(245)))));
            this._MainFrm2_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
            this._MainFrm2_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
            this._MainFrm2_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(632, 50);
            this._MainFrm2_Toolbars_Dock_Area_Right.Name = "_MainFrm2_Toolbars_Dock_Area_Right";
            this._MainFrm2_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(0, 396);
            this._MainFrm2_Toolbars_Dock_Area_Right.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // _MainFrm2_Toolbars_Dock_Area_Top
            // 
            this._MainFrm2_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._MainFrm2_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(190)))), ((int)(((byte)(245)))));
            this._MainFrm2_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
            this._MainFrm2_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
            this._MainFrm2_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._MainFrm2_Toolbars_Dock_Area_Top.Name = "_MainFrm2_Toolbars_Dock_Area_Top";
            this._MainFrm2_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(632, 26);
            this._MainFrm2_Toolbars_Dock_Area_Top.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // _MainFrm2_Toolbars_Dock_Area_Bottom
            // 
            this._MainFrm2_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._MainFrm2_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(190)))), ((int)(((byte)(245)))));
            this._MainFrm2_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
            this._MainFrm2_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
            this._MainFrm2_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 446);
            this._MainFrm2_Toolbars_Dock_Area_Bottom.Name = "_MainFrm2_Toolbars_Dock_Area_Bottom";
            this._MainFrm2_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(632, 0);
            this._MainFrm2_Toolbars_Dock_Area_Bottom.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // MDIWorkspace
            // 
            this.MDIWorkspace.AllowNestedTabGroups = Infragistics.Win.DefaultableBoolean.True;
            this.MDIWorkspace.MdiParent = this;
            this.MDIWorkspace.SettingsKey = "";
            this.MDIWorkspace.TabSettings.AllowClose = Infragistics.Win.DefaultableBoolean.False;
            this.MDIWorkspace.ViewStyle = Infragistics.Win.UltraWinTabbedMdi.ViewStyle.VisualStudio2005;
            this.MDIWorkspace.WorkspaceName = "MDIWorkspace";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Location = new System.Drawing.Point(0, 26);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(632, 24);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // MainFrm2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 446);
            this.Controls.Add(this._MainFrm2AutoHideControl);
            this.Controls.Add(this._MainFrm2UnpinnedTabAreaBottom);
            this.Controls.Add(this._MainFrm2UnpinnedTabAreaTop);
            this.Controls.Add(this._MainFrm2UnpinnedTabAreaLeft);
            this.Controls.Add(this._MainFrm2UnpinnedTabAreaRight);
            this.Controls.Add(this._MainFrm2_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._MainFrm2_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._MainFrm2_Toolbars_Dock_Area_Bottom);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this._MainFrm2_Toolbars_Dock_Area_Top);
            this.IsMdiContainer = true;
            this.MinimumSize = new System.Drawing.Size(640, 480);
            this.Name = "MainFrm2";
            this.Text = "CAB Integration Example";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.DockWorkspace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MDIWorkspace)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Practices.CompositeUI.WinForms.UltraDockWorkspace DockWorkspace;
        private Infragistics.Win.UltraWinDock.AutoHideControl _MainFrm2AutoHideControl;
        private Infragistics.Win.UltraWinDock.UnpinnedTabArea _MainFrm2UnpinnedTabAreaTop;
        private Infragistics.Win.UltraWinDock.UnpinnedTabArea _MainFrm2UnpinnedTabAreaRight;
        private Infragistics.Win.UltraWinDock.UnpinnedTabArea _MainFrm2UnpinnedTabAreaLeft;
        private Infragistics.Win.UltraWinDock.UnpinnedTabArea _MainFrm2UnpinnedTabAreaBottom;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _MainFrm2_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _MainFrm2_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _MainFrm2_Toolbars_Dock_Area_Top;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _MainFrm2_Toolbars_Dock_Area_Bottom;
        private Infragistics.Practices.CompositeUI.WinForms.UltraMdiTabWorkspace MDIWorkspace;
        private System.Windows.Forms.MenuStrip menuStrip1;
        internal Infragistics.Win.UltraWinToolbars.UltraToolbarsManager ultraToolbarsManager1;


    }
}