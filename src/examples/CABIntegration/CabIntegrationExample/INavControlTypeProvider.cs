﻿using System;

namespace CabIntegrationExample
{
    public interface INavControlTypeProvider
    {
        Type NavControlType { get; }
    }
}
