﻿using Microsoft.Practices.CompositeUI;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using IModule = Microsoft.Practices.Composite.Modularity.IModule;

namespace CabIntegrationExample
{
    /// <summary>
    /// This module is going to be loaded by MSDesktop after all addins have
    /// been intialized. Its sole purpose is to tell MSDesktop which navigation control
    /// to use.
    /// </summary>
    public class NavigationControlModule : IModule
    {
        public static WorkItem RootWorkItem { get; set; }

        private readonly IChromeManager _manager;

        public NavigationControlModule(IChromeManager manager_)
        {
            _manager = manager_;
        }

        public void Initialize()
        {
            var navControlType = RootWorkItem.Services.Get<INavControlTypeProvider>();
            if (navControlType != null)
            {
                _manager.SetCABNavigationControl(navControlType.NavControlType);
            }
        }
    }
}
