namespace BlotterAddIn
{
    partial class BlotterView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlotterView));
            this.infoProvider = new Microsoft.Practices.CompositeUI.SmartParts.SmartPartInfoProvider();
            this.smartPartInfo1 = new Microsoft.Practices.CompositeUI.SmartParts.SmartPartInfo();
            this.ultraDockSmartPartInfo1 = new Infragistics.Practices.CompositeUI.WinForms.UltraDockSmartPartInfo();
            this.ultraMdiTabSmartPartInfo1 = new Infragistics.Practices.CompositeUI.WinForms.UltraMdiTabSmartPartInfo();
            this.blotter = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.blotter)).BeginInit();
            this.SuspendLayout();
            // 
            // smartPartInfo1
            // 
            this.smartPartInfo1.Description = "";
            this.smartPartInfo1.Title = "Blotter";
            this.infoProvider.Items.Add(this.smartPartInfo1);
            // 
            // ultraDockSmartPartInfo1
            // 
            this.ultraDockSmartPartInfo1.Description = "";
            this.ultraDockSmartPartInfo1.Image = ((System.Drawing.Image)(resources.GetObject("ultraDockSmartPartInfo1.Image")));
            this.ultraDockSmartPartInfo1.PreferredSize = new System.Drawing.Size(0, 0);
            this.ultraDockSmartPartInfo1.Title = "Blotter";
            this.infoProvider.Items.Add(this.ultraDockSmartPartInfo1);
            // 
            // ultraMdiTabSmartPartInfo1
            // 
            this.ultraMdiTabSmartPartInfo1.Description = "";
            this.ultraMdiTabSmartPartInfo1.Image = ((System.Drawing.Image)(resources.GetObject("ultraMdiTabSmartPartInfo1.Image")));
            this.ultraMdiTabSmartPartInfo1.Title = "Blotter";
            this.infoProvider.Items.Add(this.ultraMdiTabSmartPartInfo1);
            // 
            // blotter
            // 
            this.blotter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.blotter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.blotter.Location = new System.Drawing.Point(0, 0);
            this.blotter.Name = "blotter";
            this.blotter.Size = new System.Drawing.Size(545, 374);
            this.blotter.TabIndex = 0;
            // 
            // BlotterView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.blotter);
            this.Name = "BlotterView";
            this.Size = new System.Drawing.Size(545, 374);
            this.Load += new System.EventHandler(this.BlotterView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.blotter)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Practices.CompositeUI.SmartParts.SmartPartInfo smartPartInfo1;
        private Infragistics.Practices.CompositeUI.WinForms.UltraDockSmartPartInfo ultraDockSmartPartInfo1;
        private Infragistics.Practices.CompositeUI.WinForms.UltraMdiTabSmartPartInfo ultraMdiTabSmartPartInfo1;
        private Microsoft.Practices.CompositeUI.SmartParts.SmartPartInfoProvider infoProvider;
        private System.Windows.Forms.DataGridView blotter;
    }
}
