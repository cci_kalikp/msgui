using Microsoft.Practices.CompositeUI;

namespace BlotterAddIn
{
    /// <summary>
    /// This class will be automatically loaded by CAB since it
    /// derives from ModuleInit. This class is responsible for initializing
    /// this entire "Module" or "Plugin"
    /// </summary>
    public class BlotterAddInInit: ModuleInit
    {
        /// <summary>
        /// This property will be automatically assigned by CAB. 
        /// This is our reference to the Work Item.
        /// </summary>
        private WorkItem _workItem = null;
        [ServiceDependency]
        public WorkItem ParentWorkItem
        {
            set { _workItem = value; }
        }

        /// <summary>
        /// We need to handle the ModuleInit.Load method. We can put initilization
        /// code here. 
        /// </summary>
        public override void Load()
        {
            base.Load();

            BlotterWorkItem w = _workItem.WorkItems.AddNew<BlotterWorkItem>();

            w.Run();

            w.Activate();   
        }                
    }
}
