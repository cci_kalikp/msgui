using System;
using System.Windows.Forms;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI;

namespace BlotterAddIn
{
    [SmartPart]
    public partial class BlotterView : UserControl,ISmartPartInfoProvider
    {
        public BlotterView()
        {
            InitializeComponent();
        }

        private WorkItem _workItem;

        [ServiceDependency]
        public WorkItem WorkItem
        {
            set { _workItem = value; }
        }
        
        public ISmartPartInfo GetSmartPartInfo(Type smartPartInfoType)
        {
            // Implement ISmartPartInfoProvider in the containing smart part. Required in order for contained smart part infos to work.
            Microsoft.Practices.CompositeUI.SmartParts.ISmartPartInfoProvider ensureProvider = this;
            return this.infoProvider.GetSmartPartInfo(smartPartInfoType);

        }
        
        [CommandHandler("ButtonExampleClick")]
        public void OnButtonExampleClickCommand(object sender_, EventArgs e_)
        {
            MessageBox.Show("Button clicked!");
        }
        
        private void BlotterView_Load(object sender, EventArgs e)
        {
            var sampleData = new BindingSource();

            sampleData.Add(new Stock { Name = "MS", Price = 33.3 });

            blotter.DataSource = sampleData;
        }
        
    }

    public struct Stock
    {
        public string Name { get; set; }
        public double Price { get; set; }
    }

}
