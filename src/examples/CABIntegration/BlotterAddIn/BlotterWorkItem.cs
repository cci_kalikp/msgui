using System;
using System.Windows.Forms;
using CabIntegrationExample;
using Infragistics.Win.UltraWinToolbars;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.Unity;

namespace BlotterAddIn
{
    public class BlotterWorkItem: WorkItem
    {
        private ButtonTool _cmdPlay;
        private ToolStripMenuItem _menuItem;
        private ToolStripItem _toolStripItem;

        protected override void OnRunStarted()
        {
            base.OnRunStarted();

            var ws = BlotterSettings.Default.BlotterViewWorkspace;

            var v = SmartParts.AddNew<BlotterView>();
            var v2 = SmartParts.AddNew<NavigationView>();

            RootWorkItem.Services.AddNew(typeof (NavControlTypeProvider), typeof (INavControlTypeProvider));
            
            IniitUIElements();

            this.Workspaces[ws].Show(v);
            this.Workspaces[ws].Show(v2);
        }

        private void IniitUIElements()
        {
            _cmdPlay = new ButtonTool("ButtonExample");
            _cmdPlay.SharedProps.Caption = @"Button example";
            _cmdPlay = this.UIExtensionSites["Main"].Add(_cmdPlay);
            Commands["ButtonExampleClick"].AddInvoker(_cmdPlay, "ToolClick");

            _toolStripItem = new ToolStripMenuItem("Test menu item");

            _menuItem = new ToolStripMenuItem("Blotter menu");
            _menuItem.DropDownItems.Add(_toolStripItem);
            _menuItem = UIExtensionSites["Menu"].Add(_menuItem);
        }
    }
}
