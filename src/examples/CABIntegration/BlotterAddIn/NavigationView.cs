﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CabIntegrationExample;
using Microsoft.Practices.CompositeUI.SmartParts;

namespace BlotterAddIn
{
    public partial class NavigationView : UserControl, ISmartPartInfoProvider
    {
        public NavigationView()
        {
            InitializeComponent();
        }

        public Microsoft.Practices.CompositeUI.SmartParts.ISmartPartInfo GetSmartPartInfo(Type smartPartInfoType)
        {
            // Implement ISmartPartInfoProvider in the containing smart part. Required in order for contained smart part infos to work.
            Microsoft.Practices.CompositeUI.SmartParts.ISmartPartInfoProvider ensureProvider = this;
            return this.infoProvider.GetSmartPartInfo(smartPartInfoType);

        }
    }

    class NavControlTypeProvider : INavControlTypeProvider
    {
        public Type NavControlType
        {
            get
            {
                return typeof(NavigationView);
            }
        }
    }
}
