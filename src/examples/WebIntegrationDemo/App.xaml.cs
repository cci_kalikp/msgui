﻿using System.Windows;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.MSGui.Themes;
using MSDesktop.WebSupport;
using WebIntegrationDemo.MultiviewTest;
using WebIntegrationDemo.Stocks;

namespace WebIntegrationDemo
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);
			var boot = new Framework();
			boot.UsePrism();
            boot.AddSimpleTheme();
            boot.SetShellMode(ShellMode.RibbonWindow);
		    boot.SetupPersistenceStorage(new FilePersistenceStorage("WebIntegrationDemo", "Profiles"));

            boot.AddModule<StocksModule>();
            boot.AddModule<MultiviewWebModule>();
            boot.AddModule<V2VConfigModule>();

            boot.AddModule<FlexTradeHistoryModule>();
            boot.AddModule<LiveDataModule.LiveDataModule>();

            boot.AcquireFocusAfterStartup();
            
            boot.EnableWebSupport();

            boot.EnableMessageRoutingCommunication();

            boot.EnableLogging();

			boot.Start();
		}
	}
}
