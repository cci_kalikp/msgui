﻿using System;
using System.IO;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MSDesktop.WebSupport;
using MSDesktop.WebSupport.CEF;


namespace WebIntegrationDemo.MultiviewTest
{
    public class MultiviewWebModule : CEFBasedHTMLModule
    {
        public MultiviewWebModule(IUnityContainer unityContainer) : base(unityContainer)
        {
        }

        protected override string Name
        {
            get { return "MultiviewWebModule"; }
        }

        protected override Uri Url
        {
            get
            {
                //return new Uri("http://google.pl");
                return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "MultiviewTest\\MultiviewWebModule.html"),
                              UriKind.RelativeOrAbsolute);

                
            }
        }
    }
}
