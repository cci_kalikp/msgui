﻿using System;
using System.IO;
using MSDesktop.WebSupport;
using MSDesktop.WebSupport.CEF;
using MSDesktop.WebSupport.MessageRouting;
using Microsoft.Practices.Unity;


namespace WebIntegrationDemo.LiveDataModule
{
    //class LiveDataModule : AwesomiumBasedHTMLModule
    class LiveDataModule : CEFBasedHTMLModule
    {
        public LiveDataModule(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            this.EnableMessageRouting();
        }

        protected override string Name
        {
            get { return "LiveDataModule"; }
        }

        protected override Uri Url
        {
            get 
            { 
                //return new Uri("http://google.pl");
                return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "LiveDataModule\\WebModule.html"),
                              UriKind.RelativeOrAbsolute); 
            }
        }
    }
}
