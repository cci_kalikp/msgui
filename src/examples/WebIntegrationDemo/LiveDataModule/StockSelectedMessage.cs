﻿using System;

namespace WebIntegrationDemo.LiveDataModule
{
    [Serializable]
    public class StockSelectedMessage
    {
        public string Symbol { get; set; }
    }
}
