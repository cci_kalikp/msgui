﻿using System;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.Message;
using MSDesktop.MessageRouting.Routing;

namespace WebIntegrationDemo.LiveDataModule
{
    [Serializable]
    public class SimpleRoutedMessage : IProvideMessageInfo, IBypassRouting
    {
        public string Content { get; set; }

        public string GetInfo()
        {
            return "Simple routed message";
        }

        public Endpoint Target { get { return new Endpoint(); }}
    }
}
