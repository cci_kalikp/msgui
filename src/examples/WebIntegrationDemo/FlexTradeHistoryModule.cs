﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MSDesktop.WebSupport.Flex;

namespace WebIntegrationDemo
{
    public class FlexTradeHistoryModule : FlexModule
    {
        public FlexTradeHistoryModule(
            IUnityContainer container,
            IChromeManager chromeManager,
            IChromeRegistry chromeRegistry,
            IApplication application,
            IEventRegistrator eventRegistrator)
            : base(container, chromeManager, chromeRegistry, application, eventRegistrator)
        {
        }

        protected override Uri Url
        {
            get
            {
                return
                    new Uri(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Lib",
                                         "tradehistory", "bin-debug", "tradehistory.swf"));
            }
        }
    }
}
