﻿using System;

namespace WebIntegrationDemo.Stocks
{
    [Serializable]
    public class SimpleTextMessage
    {
        public string Text { get; set; }
    }
}
