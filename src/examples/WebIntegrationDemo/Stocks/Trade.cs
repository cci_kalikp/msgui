﻿using System;

namespace WebIntegrationDemo.Stocks
{
    [Serializable]
    public class Trade
    {
        public string Symbol { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
    }
}
