﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using WebIntegrationDemo.Stocks;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace WebIntegrationDemo
{
    /// <summary>
    /// Interaction logic for TradesGrid.xaml
    /// </summary>
    public partial class TradesGrid : UserControl
    {
        private IPublisher<Trade> m_publisher;

        internal IPublisher<Trade> Publisher { get { return m_publisher; } set { m_publisher = value; } }

        public TradesGrid()
        {
            InitializeComponent();
        }

        public TradesGrid(IPublisher<Trade> publisher) : this()
        {
            m_publisher = publisher;
        }

        
        private bool ProcessUserInput(out Stock stock, out int quantity)
        {
            stock = Grid.SelectedItem as Stock;
            if (!int.TryParse(QuantityTextBox.Text, out quantity))
            {
                TaskDialog.ShowMessage("Quantity must be a number");
                return false;
            }
            if (stock == null)
            {
                TaskDialog.ShowMessage("Please select a stock");
                return false;
            }
            return true;
        }

        public void BuyClick(object sender, RoutedEventArgs e)
        {
            int quantity;
            Stock stock;
            if (ProcessUserInput(out stock, out quantity))
            {
                m_publisher.Publish(new Trade
                {
                    Symbol = stock.Symbol,
                    Price = stock.Price,
                    Quantity = quantity
                });
            }
        }

        public void SellClick(object sender, RoutedEventArgs e)
        {
            int quantity;
            Stock stock;
            if (ProcessUserInput(out stock, out quantity))
            {
                var trade = new Trade
                {
                    Symbol = stock.Symbol,
                    Price = stock.Price,
                    Quantity = -quantity
                };
                m_publisher.Publish(trade);
            }
        }

        public void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            QuantityTextBox.Clear();
            QuantityTextBox.Focus();
        }
    }
}
