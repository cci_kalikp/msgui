﻿using System;
using MSDesktop.MessageRouting.Message;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace SubsystemIsolationExample.View2ViewCommunication
{
    //cw CommunicationMessages : 10
    /**
     * ## Description
     * The message types used in the examples
     * ## Code
     * ### Message for publishing
     */

    // cw ViewToViewConnection : 31
    //{
    [Message("SendingMessage"), Serializable]
    //}

    //cw ViewToViewConnection: 31 cw CommunicationMessages : 11
    //{
    public class SendingMessage : IProvideMessageInfo
    //}
    // cw CommunicationMessages : 11
    //{
    {
        public String StockName { get; set; }
        public String Description { get; set; }
        public string GetInfo()
        {
            return "SendingMessage - Isolated over Kraken";
        }
    }
    //}
}
