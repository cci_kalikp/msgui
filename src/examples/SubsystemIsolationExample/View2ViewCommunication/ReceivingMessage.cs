﻿using System;
using MSDesktop.MessageRouting.Message;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace SubsystemIsolationExample.View2ViewCommunication
{

    //cw CommunicationMessages : 20
    /**
     * ### Message type for subscribing    
     */

    [Message("ReceivingMessage")]
    //{
    [Serializable]
    public class ReceivingMessage : IProvideMessageInfo
    {
        public String Stock { get; set; }
        public String Message { get; set; }

        public string GetInfo()
        {
            return "ReceivingMessage - Isolated over Kraken";
        }
    }

    //}
}
