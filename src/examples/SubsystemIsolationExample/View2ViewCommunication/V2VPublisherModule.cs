﻿using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MSDesktop.MessageRouting;

//cw ViewToViewPublisher: 11
/**##namepace*/
//{

//}

namespace SubsystemIsolationExample.View2ViewCommunication
{
    public class V2VPublisherModule: IModule
    {

        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;
        // private readonly V2VConfigModule _v2vConfigModule;

        //cw ViewToViewPublisher : 20
        //{
        // private readonly IEventRegistrator _eventRegistrator;
        private readonly IRoutingCommunicator _communicator;
        //}

        private const string ViewId = "Publisher";
        private const string WidgetButtonId = "CreatePublisher";

        
        //{
        public V2VPublisherModule(IChromeManager chromeManager, 
            IChromeRegistry chromeRegistry, 
            IRoutingCommunicator communicator)
        {
            _communicator = communicator;
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
        }
        //}


        public void Initialize()
        {
            _chromeRegistry.RegisterWindowFactory(ViewId, CreateView);
            _chromeRegistry.RegisterWidgetFactory(WidgetButtonId, _chromeManager.ShowWindowButtonFactory());

            _chromeManager.PlaceWidget(WidgetButtonId, "How/Where",
            new InitialShowWindowButtonParameters()
            {
                Text = "Publisher",
                ToolTip = "Demo Button",
                WindowFactoryID = ViewId,
                InitialParameters = new InitialWindowParameters
                                        {
                                            InitialLocation = InitialLocation.DockLeft
                                        },
            });

            // _v2vConfigModule.AutoHookMode = AutoHookMode.IslandMode;
        }


        //cw ViewToViewPublisher : 28
        //{
        private bool CreateView(IWindowViewContainer viewcontainer, XDocument state)
        {
            var publisher = _communicator.GetPublisher<SendingMessage>();
            

            // var publisher = _eventRegistrator.RegisterPublisher<SendingMessage>(viewcontainer);
            viewcontainer.Content = new PublisherView(publisher);
            viewcontainer.Title = "Publisher";
            return true;
        }
        //}
    }
}
