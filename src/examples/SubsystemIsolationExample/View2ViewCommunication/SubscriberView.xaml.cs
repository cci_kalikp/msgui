﻿using System;
using System.Reactive.Linq;
using System.Windows.Controls;
using MSDesktop.MessageRouting;

namespace SubsystemIsolationExample.View2ViewCommunication
{
    /// <summary>
    /// Interaction logic for SubscriberView.xaml
    /// </summary>
    public partial class SubscriberView
    {
        public string BookingName
        {
            get
            {
                return (string)FilterBox.Dispatcher.Invoke((Func<string>)(() => (string)((ComboBoxItem)FilterBox.SelectedItem).Content));
            }
        }

        //cw ViewToViewSubscriber: 31
        //{[csharp]
        public SubscriberView(IRoutedSubscriber<SendingMessage> subscriber)
        {
            var filter = subscriber
                .GetObservable()
                .Where(msg => msg.StockName == BookingName);

            //BookingName value comes from the combobox of the view
            filter.Subscribe(
                m =>
                    {
                        MessageHistory.Dispatcher.Invoke(
                            (Action) (() => MessageHistory.Items.Add(m) //Display the message in the MessageHistory
                                     ));
                    });
           
            InitializeComponent();
        }
        //}
    }
}
