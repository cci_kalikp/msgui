﻿using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MSDesktop.MessageRouting;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;

//cw ViewToViewSubscriber :15
/**namespace*/
//{
//}

//cw ViewToView: 31
//{

//}

namespace SubsystemIsolationExample.View2ViewCommunication
{
    public class V2VSubscriberModule: IModule
    {
        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;
        private readonly IRoutingCommunicator _communicator;

        private const string ViewId = "Subscriber";
        private const string WidgetButtonId = "CreateSubscriber";

        public V2VSubscriberModule(IChromeManager chromeManager,
            IChromeRegistry chromeRegistry,
            IRoutingCommunicator communicator)
        {
            _communicator = communicator;
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
        }

        public void Initialize()
        {
            _chromeRegistry.RegisterWindowFactory(ViewId, CreateView);
            _chromeRegistry.RegisterWidgetFactory(WidgetButtonId, _chromeManager.ShowWindowButtonFactory());

            var initialParameters = new InitialShowWindowButtonParameters
                {
                    Text = "Sbuscriber", ToolTip = "Demo Button", WindowFactoryID = ViewId, InitialParameters = new InitialWindowParameters
                        {
                            InitialLocation = InitialLocation.DockRight
                        },
                };

            _chromeManager.PlaceWidget(WidgetButtonId, "How/Where", initialParameters);
        }

        //cw ViewToViewSubscriber: 21
        //{
        private bool CreateView(IWindowViewContainer viewcontainer, XDocument state)
        {
            var sub = _communicator.GetSubscriber<SendingMessage>();
            viewcontainer.Content = new SubscriberView(sub);
            viewcontainer.Title = "Subscriber";
            return true;
        }
        //}
    }
}
