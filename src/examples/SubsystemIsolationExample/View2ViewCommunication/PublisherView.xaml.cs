﻿using System;
using System.Windows;
using MSDesktop.MessageRouting;

namespace SubsystemIsolationExample.View2ViewCommunication
{
  //cw ViewToViewPublisher: 31
  //{[csharp]
    public partial class PublisherView
    {
        private readonly IRoutedPublisher<SendingMessage> _publisher;
        
        public PublisherView(IRoutedPublisher<SendingMessage> publisher)
        {   
            _publisher = publisher;
            InitializeComponent();
        }
        
        //Invoked when a button is clicked
        private void SubmitNewPrice(object sender, RoutedEventArgs e)
        {
            var sp = new SendingMessage
                         {
                             StockName = cb_stockNames.SelectionBoxItem as String,
                             Description = tb_price.Text
                         };

            _publisher.Publish(sp);
        }
    }
    //}
}
