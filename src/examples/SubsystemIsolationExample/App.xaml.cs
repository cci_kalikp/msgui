﻿using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.MSGui.Themes;
using SubsystemIsolationExample.View2ViewCommunication;

namespace SubsystemIsolationExample
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var boot = new Framework();
            boot.AddSimpleTheme();
            boot.EnableMessageRoutingCommunication();
            boot.EnableMessageRoutingDefaultGui();
            boot.EnableMessageRoutingHistory();
            boot.EnableMessageRoutingHistoryGui();

            boot.AddModule<ManagingModule>();
            boot.AddModule<V2VConfigModule>();

            boot.SetImcCommunicationStatus(CommunicationChannelStatus.Disabled);

            boot.Start();
            boot.SetUpProcessIsolatedWrapper<IsolatedModulesCollection>(IsolatedProcessPlatform.X86,
                                                                        additionalCommandLine: @"/debug");
        }
    }
}
