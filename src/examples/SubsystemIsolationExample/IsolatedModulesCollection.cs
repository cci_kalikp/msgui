﻿using System;
using System.Collections.Generic;
using MSDesktop.Isolation;
using MSDesktop.Isolation.Interfaces;
using SubsystemIsolationExample.View2ViewCommunication;

namespace SubsystemIsolationExample
{
    internal sealed class IsolatedModulesCollection : ISubsystemModuleCollection
    {
        public IEnumerable<Type> GetModules()
        {
            yield return typeof(V2VPublisherModule);
            yield return typeof(V2VSubscriberModule);
            yield return typeof(FlashPlayerBasedFlexModule);
        }
    }
}
