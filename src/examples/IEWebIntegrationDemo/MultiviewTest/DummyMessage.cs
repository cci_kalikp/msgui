﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IEWebIntegrationDemo.MultiviewTest
{
    public class DummyMessage
    {
        public string SomeString { get; set; }
        public int SomeInt { get; set; }
    }
}
