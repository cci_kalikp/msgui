﻿using System;
using System.IO;
using Microsoft.Practices.Unity;
using MSDesktop.WebSupport;

namespace IEWebIntegrationDemo.MultiviewTest
{
    public class MultiviewWebModule : HTMLModule
    {
        public MultiviewWebModule(IUnityContainer unityContainer) : base(unityContainer)
        {
        }

        protected override string Name
        {
            get { return "MultiviewWebModule"; }
        }

        protected override Uri Url
        {
            get
            {
                //return new Uri("http://google.pl");
                return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "MultiviewTest\\MultiviewWebModule.html"),
                              UriKind.RelativeOrAbsolute);

                
            }
        }
    }
}
