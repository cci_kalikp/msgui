﻿using System;
using System.IO;
using Microsoft.Practices.Unity;
using MSDesktop.WebSupport;
using MSDesktop.WebSupport.MessageRouting;

namespace IEWebIntegrationDemo.LiveDataModule
{
    //class LiveDataModule : AwesomiumBasedHTMLModule
    class LiveDataModule : HTMLModule
    {
        public LiveDataModule(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            this.EnableMessageRouting();
        }

        protected override string Name
        {
            get { return "LiveDataModule"; }
        }

        protected override Uri Url
        {
            get 
            { 
                //return new Uri("http://google.pl");
                return new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "LiveDataModule\\WebModule.html"),
                              UriKind.RelativeOrAbsolute); 
            }
        }
    }
}
