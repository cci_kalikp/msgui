﻿using System;

namespace IEWebIntegrationDemo.LiveDataModule
{
    [Serializable]
    public class StockSelectedMessage
    {
        public string Symbol { get; set; }
    }
}
