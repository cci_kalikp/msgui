﻿using System;

namespace IEWebIntegrationDemo.Stocks
{
    [Serializable]
    public class SimpleTextMessage
    {
        public string Text { get; set; }
    }
}
