﻿using System;

namespace IEWebIntegrationDemo.Stocks
{
    [Serializable]
    public class Trade
    {
        public string Symbol { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
    }
}
