﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using IEWebIntegrationDemo.MultiviewTest;
using IEWebIntegrationDemo.Stocks;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.MSGui.Themes;
using MSDesktop.WebSupport;

namespace IEWebIntegrationDemo
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var boot = new Framework();
            boot.UsePrism();
            boot.HideOrLockShellElements(HideLockUIGranularity.HideRibbon);
            boot.AddModernTheme(Colors.SeaGreen, ModernThemeBase.Dark);
            boot.SetShellMode(ShellMode.RibbonWindow);
            boot.SetupPersistenceStorage(new FilePersistenceStorage("IEWebIntegrationDemo", "Profiles"));

            boot.AddModule<StocksModule>();
            boot.AddModule<MultiviewWebModule>();
            boot.AddModule<V2VConfigModule>();

            boot.AddModule<LiveDataModule.LiveDataModule>();

            boot.AcquireFocusAfterStartup();

            boot.EnableWebSupport();

            boot.EnableMessageRoutingCommunication();

            boot.EnableLogging();

            boot.Start();
        }
    }
}
