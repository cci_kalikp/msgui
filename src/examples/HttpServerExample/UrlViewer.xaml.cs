﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes; 

namespace HttpServerExample
{
    /// <summary>
    /// Interaction logic for UrlViewer.xaml
    /// </summary>
    public partial class UrlViewer : UserControl
    {
        public UrlViewer()
        {
            InitializeComponent();
        }

        private object buttonClicked = null;
        private void Button_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Button button = sender as Button;
            if (button.IsEnabled)
            {
                buttonClicked = sender;
                button.Command.Execute(null); 
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            object copy = buttonClicked;
            buttonClicked = null;
            if (sender == copy)
            {
                e.Handled = true;
            } 
        }
         
    }
}
