﻿#region cw page

//cw HTTPServer: 10

/**
 * ## Description
 * Use HTTP Request to create views, perform module to module and view to view communication
 */
//cw HTTPServer: 15
/** 
 * ## Assembly
 * 
 * You need to add a reference to the MSDesktop.HTTPServer assembly.
 * If you want to use Json to serialize data, also need to reference Newtonsoft.Json assembly (\\san01b\DevAppsGML\dist\dotnet3rd\PROJ\jsondotnet\5.0.6\assemblies\Net40\Newtonsoft.Json.dll)
 */
//cw HTTPServer: 20
/**
 * ## Namespace
 */

//cw HTTPServer: 30
/**
 * ## Context 
 */

#endregion

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Impl; 
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.MSGui.Themes;
//cw HTTPServer: 21
//{
using MorganStanley.MSDotNet.MSGui.Impl.HTTPServer;
using MSDesktop.HTTPServer;
//}
using HttpServerExample.Initiation;
using HttpServerExample.ModuleToModule;
using HttpServerExample.ViewToView;

namespace HttpServerExample
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {            
        //cw HTTPServer: 31
        //{ 
        protected override void OnStartup(StartupEventArgs e)
        //}
        {
            base.OnStartup(e);
            var boot = new Framework();
            boot.AddSimpleTheme();
            //cw HTTPServer: 35
            /**
             * ## Code
             * 
             * #### Enable the HTTPServer
             * User could choose to specify the server listening port number and whether server is protected against request tampering.
             * By default, the port number is 8080, the server protection is on, and the hash seed transferred is used for protecting request.
            **/
            //{
            boot.EnableHTTPServer(UrlViewModel.Port, true, UrlViewModel.HashSeed);
            //}
            boot.AddModule<V2VPublisherModule>();
            boot.AddModule<V2VSubscriberModule>();
            boot.AddModule<PublishingModule>();
            boot.AddModule<SubscribingModule>();
            boot.AddModule<InitiatorModule>();
            boot.AddModule<V2VConfigModule>();

            Resources.MergedDictionaries.Add(
     new ResourceDictionary
     {
         Source = new Uri("/HttpServerExample;component/Resources/Styles.xaml",
             UriKind.RelativeOrAbsolute)
     });

            boot.Start();
        } 
    }
}
