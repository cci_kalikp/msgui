﻿using System;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using System.Diagnostics;

namespace HttpServerExample.Initiation
{
    public partial class InitiatorView : UserControl
    {
         
        public InitiatorView(UrlListViewModel model)
        {    
            InitializeComponent();
            this.DataContext = model;
        }
  
         
    } 
}
