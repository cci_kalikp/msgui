﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;

namespace HttpServerExample.Initiation
{
    public class InitiatorModule: IModule
    {        
        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry; 
        private const string ViewId = "Initiator"; 
        
        public InitiatorModule(IChromeManager chromeManager, 
            IChromeRegistry chromeRegistry)
        {
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry; 
        } 


        public void Initialize()
        {
            _chromeRegistry.RegisterWindowFactory(ViewId, CreateView);
            _chromeManager.PlaceWidget(ViewId, "Initiator",
            new InitialShowWindowButtonParameters()
            {
                Text = "Initiator",
                ToolTip = "Intitiator Page",
                WindowFactoryID = ViewId,
                InitialParameters = new InitialWindowParameters
                {
                    IsHeaderVisible = true,
                    InitialLocation = InitialLocation.DockInNewTab,
                    Singleton = true,
                    SingletonKey = ViewId
                },
            });
            //_chromeManager.LayoutLoaded += new EventHandler(_chromeManager_LayoutLoaded);

             
        }

        void _chromeManager_LayoutLoaded(object sender, EventArgs e)
        {
            _chromeManager.CreateWindow(ViewId,
            new InitialWindowParameters()
            {
                IsHeaderVisible = true,
                InitialLocation = InitialLocation.DockInNewTab,
                Singleton = true,
                SingletonKey = ViewId
            });
        }

 
        private bool CreateView(IWindowViewContainer viewcontainer, XDocument state)
        {
            viewcontainer.Content = new InitiatorView(new UrlListViewModel(UrlListUsage.Initiation));
            viewcontainer.Title = "Initiator";
            return true;
        }
    }
}
