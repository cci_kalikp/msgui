@echo off
set CLASSPATH=
call \\ms\dist\winaurora\PROJ\compat\incr\bin\useafs.cmd


call module load msjava/sunjdk/1.6.0_27




set RUNTIME_CLASSPATH=%~dp0\..\lib\launcherMessageAPI.jar;%~dp0\..\lib\launcherMessageExample.jar;
set RUNTIME_CLASSPATH=%RUNTIME_CLASSPATH%;
set RUNTIME_CLASSPATH=%RUNTIME_CLASSPATH%;\\ms\dist\ossjava\PROJ\jackson2\2.1.3\common\lib\jackson-databind-2.1.3.jar
set RUNTIME_CLASSPATH=%RUNTIME_CLASSPATH%;\\ms\dist\ossjava\PROJ\jackson2\2.1.3\common\lib\jackson-core-2.1.3.jar
set RUNTIME_CLASSPATH=%RUNTIME_CLASSPATH%;\\ms\dist\ossjava\PROJ\jackson2\2.1.3\common\lib\jackson-annotations-2.1.2.jar
set RUNTIME_CLASSPATH=%RUNTIME_CLASSPATH%;\\ms\dist\ossjava\PROJ\stax2\3.1.1\common\lib\stax2-api-3.1.1.jar
set RUNTIME_CLASSPATH=%RUNTIME_CLASSPATH%;%CLASSPATH%

set RUNTIME_CLASSPATH="%RUNTIME_CLASSPATH%"

set JVM_ARGS= -Djava.util.logging.config.class=msjava.base.slf4j.jul.LogConfig

set MAIN_CLASS=com.ms.msdotnet.msdesktop.examples.LauncherMessageExample

set MAIN_CLASS_ARGS=

set MSDE_IVY_ENDORSED_DIRS=

set MSDE_JNI_PATH=//;

set MSDE_LAUNCH_WRAPPER=

:: Run the program
echo %MSDE_LAUNCH_WRAPPER% java %JVM_ARGS% -Djava.endorsed.dirs=%MSDE_IVY_ENDORSED_DIRS% -Djava.library.path=%MSDE_JNI_PATH% -cp %RUNTIME_CLASSPATH% %MAIN_CLASS% %MAIN_CLASS_ARGS%
%MSDE_LAUNCH_WRAPPER% java %JVM_ARGS% -Djava.endorsed.dirs=%MSDE_IVY_ENDORSED_DIRS% -Djava.library.path=%MSDE_JNI_PATH% -cp %RUNTIME_CLASSPATH% %MAIN_CLASS% %MAIN_CLASS_ARGS%
