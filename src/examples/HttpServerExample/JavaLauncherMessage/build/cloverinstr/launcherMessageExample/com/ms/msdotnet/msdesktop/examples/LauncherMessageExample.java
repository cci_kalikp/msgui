/* $$ This file has been instrumented by Clover 3.1.5#20120425134455926 $$ */package com.ms.msdotnet.msdesktop.examples;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import com.ms.msdotnet.msdesktop.launchermessageapi.DispatchableLauncherMessage;

public class LauncherMessageExample {public static class __CLR3_1_57878i1wwpg2m{public static com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_1_5();if(20120425134455926L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.1.5#20120425134455926,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{67,58,92,77,83,68,69,92,112,106,101,110,107,98,108,100,92,119,111,114,107,115,112,97,99,101,92,109,115,100,111,116,110,101,116,45,109,115,103,117,105,45,114,101,108,101,97,115,101,45,116,114,117,110,107,92,109,115,100,111,116,110,101,116,92,109,115,103,117,105,92,116,114,117,110,107,92,115,114,99,92,101,120,97,109,112,108,101,115,92,72,116,116,112,83,101,114,118,101,114,69,120,97,109,112,108,101,92,74,97,118,97,76,97,117,110,99,104,101,114,77,101,115,115,97,103,101,92,98,117,105,108,100,92,46,99,108,111,118,101,114},null,1414721588623L,0L,288);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

    /**
     * @param args
     */
    public static void main(String[] args) {__CLR3_1_57878i1wwpg2m.R.inc(260);
        __CLR3_1_57878i1wwpg2m.R.inc(261);HashMap<String, Object> message = new HashMap<String, Object>();
        __CLR3_1_57878i1wwpg2m.R.inc(262);message.put("StockName", "MS"); 
        __CLR3_1_57878i1wwpg2m.R.inc(263);message.put("Description", "Ada");
        __CLR3_1_57878i1wwpg2m.R.inc(264);message.put("Price", 999.99); 
        __CLR3_1_57878i1wwpg2m.R.inc(265);message.put("OrderType", OrderType.Buy);
        __CLR3_1_57878i1wwpg2m.R.inc(266);message.put("Date", new Date(114,0,1,10,9,8));
        __CLR3_1_57878i1wwpg2m.R.inc(267);List<Point2D> points = new LinkedList<Point2D>();
        __CLR3_1_57878i1wwpg2m.R.inc(268);points.add(new Point2D.Double(1.1, 2.2));
        __CLR3_1_57878i1wwpg2m.R.inc(269);points.add(new Point2D.Double(3.3, 4.4));
        __CLR3_1_57878i1wwpg2m.R.inc(270);message.put("Points", points);

		__CLR3_1_57878i1wwpg2m.R.inc(271);DispatchableLauncherMessage.addSerializer(Point2D.class, new Point2DSerializer());
        
		__CLR3_1_57878i1wwpg2m.R.inc(272);try {
            __CLR3_1_57878i1wwpg2m.R.inc(273);String url = DispatchableLauncherMessage.generateLink("SendingMessage", message, "msdesktop.communicationexmples.prod");
			__CLR3_1_57878i1wwpg2m.R.inc(274);System.out.println("url generated:");
            __CLR3_1_57878i1wwpg2m.R.inc(275);System.out.println(url);
			
			__CLR3_1_57878i1wwpg2m.R.inc(276);StringBuilder longMessage = new StringBuilder("This is a super l"); 
			__CLR3_1_57878i1wwpg2m.R.inc(277);for(int i = 0; (((i < 3000)&&(__CLR3_1_57878i1wwpg2m.R.iget(278)!=0|true))||(__CLR3_1_57878i1wwpg2m.R.iget(279)==0&false)); i++)
			{{  
			   __CLR3_1_57878i1wwpg2m.R.inc(280);longMessage.append("o");
			}
			}__CLR3_1_57878i1wwpg2m.R.inc(281);longMessage.append(" message");
			__CLR3_1_57878i1wwpg2m.R.inc(282);message.put("Description",  longMessage.toString());
			__CLR3_1_57878i1wwpg2m.R.inc(283);url = DispatchableLauncherMessage.generateLink("SendingMessage", message, "msdesktop.communicationexmples.prod");
			__CLR3_1_57878i1wwpg2m.R.inc(284);System.out.println("another url generated:");
            __CLR3_1_57878i1wwpg2m.R.inc(285);System.out.println(url);
        } 
		catch (JsonProcessingException e) {
            __CLR3_1_57878i1wwpg2m.R.inc(286);throw new RuntimeException( e );
        }
		catch (IOException e) {
            __CLR3_1_57878i1wwpg2m.R.inc(287);throw new RuntimeException( e );
        }
    }

}
