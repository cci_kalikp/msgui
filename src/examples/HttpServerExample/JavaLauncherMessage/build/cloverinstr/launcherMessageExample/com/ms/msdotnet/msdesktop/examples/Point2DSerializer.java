/* $$ This file has been instrumented by Clover 3.1.5#20120425134455926 $$ */package com.ms.msdotnet.msdesktop.examples;

import java.awt.geom.Point2D;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class Point2DSerializer extends JsonSerializer<Point2D> {public static class __CLR3_1_58080i1wwpg2r{public static com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_1_5();if(20120425134455926L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.1.5#20120425134455926,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{67,58,92,77,83,68,69,92,112,106,101,110,107,98,108,100,92,119,111,114,107,115,112,97,99,101,92,109,115,100,111,116,110,101,116,45,109,115,103,117,105,45,114,101,108,101,97,115,101,45,116,114,117,110,107,92,109,115,100,111,116,110,101,116,92,109,115,103,117,105,92,116,114,117,110,107,92,115,114,99,92,101,120,97,109,112,108,101,115,92,72,116,116,112,83,101,114,118,101,114,69,120,97,109,112,108,101,92,74,97,118,97,76,97,117,110,99,104,101,114,77,101,115,115,97,103,101,92,98,117,105,108,100,92,46,99,108,111,118,101,114},null,1414721588623L,0L,290);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

    @Override
    public void serialize(Point2D value, JsonGenerator jgen, SerializerProvider provider) throws IOException,
            JsonProcessingException {__CLR3_1_58080i1wwpg2r.R.inc(288);
        __CLR3_1_58080i1wwpg2r.R.inc(289);jgen.writeString(value.getX() + "," + value.getY());
    }

}
