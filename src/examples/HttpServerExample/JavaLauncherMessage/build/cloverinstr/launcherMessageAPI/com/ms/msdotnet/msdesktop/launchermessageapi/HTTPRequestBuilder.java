/* $$ This file has been instrumented by Clover 3.1.5#20120425134455926 $$ */package com.ms.msdotnet.msdesktop.launchermessageapi;

import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPOutputStream; 

public class HTTPRequestBuilder {public static class __CLR3_1_5rri1wwpctu{public static com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_1_5();if(20120425134455926L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.1.5#20120425134455926,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{67,58,92,77,83,68,69,92,112,106,101,110,107,98,108,100,92,119,111,114,107,115,112,97,99,101,92,109,115,100,111,116,110,101,116,45,109,115,103,117,105,45,114,101,108,101,97,115,101,45,116,114,117,110,107,92,109,115,100,111,116,110,101,116,92,109,115,103,117,105,92,116,114,117,110,107,92,115,114,99,92,101,120,97,109,112,108,101,115,92,72,116,116,112,83,101,114,118,101,114,69,120,97,109,112,108,101,92,74,97,118,97,76,97,117,110,99,104,101,114,77,101,115,115,97,103,101,92,98,117,105,108,100,92,46,99,108,111,118,101,114},null,1414721584319L,0L,83);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}
 
    String SessionIdParameter = "id"; 
    String MessageParameter = "data"; 
    String HashParameter = "hash";
	String ZippedParameter = "zip";
    int CompressThreshold = 1500;
	 
    private String host;
    private int port;
    private boolean enableHash;
    private String hashSeed;
    
    public HTTPRequestBuilder(String host, int port, boolean enableHash, String hashSeed) {
        super();__CLR3_1_5rri1wwpctu.R.inc(28);__CLR3_1_5rri1wwpctu.R.inc(27);
        __CLR3_1_5rri1wwpctu.R.inc(29);this.host = host;
        __CLR3_1_5rri1wwpctu.R.inc(30);this.port = port;
        __CLR3_1_5rri1wwpctu.R.inc(31);this.enableHash = enableHash;
        __CLR3_1_5rri1wwpctu.R.inc(32);this.hashSeed = hashSeed;
    }
    
    public String getBaseUrl()
    {__CLR3_1_5rri1wwpctu.R.inc(33);
        __CLR3_1_5rri1wwpctu.R.inc(34);return "http://" + host + ":" + port + "/";
    }

    public String buildPublishUrl(String message_, String publisherId_) throws IOException
    {__CLR3_1_5rri1wwpctu.R.inc(35);
        __CLR3_1_5rri1wwpctu.R.inc(36);StringBuilder url = new StringBuilder(getBaseUrl());
        __CLR3_1_5rri1wwpctu.R.inc(37);url.append("pub.json");
        __CLR3_1_5rri1wwpctu.R.inc(38);url.append("?");  
	    __CLR3_1_5rri1wwpctu.R.inc(39);boolean zipped = false;
        __CLR3_1_5rri1wwpctu.R.inc(40);if ((((message_.length() > CompressThreshold)&&(__CLR3_1_5rri1wwpctu.R.iget(41)!=0|true))||(__CLR3_1_5rri1wwpctu.R.iget(42)==0&false))) //needs to be zipped
        {{
            __CLR3_1_5rri1wwpctu.R.inc(43);message_ = compress(message_);
            __CLR3_1_5rri1wwpctu.R.inc(44);zipped = true;
        }
        }__CLR3_1_5rri1wwpctu.R.inc(45);url.append(MessageParameter).append("=").append(URLEncoder.encode(message_));
        __CLR3_1_5rri1wwpctu.R.inc(46);if ((((zipped)&&(__CLR3_1_5rri1wwpctu.R.iget(47)!=0|true))||(__CLR3_1_5rri1wwpctu.R.iget(48)==0&false)))
        {{ 
			__CLR3_1_5rri1wwpctu.R.inc(49);url.append("&").append(ZippedParameter).append("=1");
        } 
        }__CLR3_1_5rri1wwpctu.R.inc(50);url.append("&").append(SessionIdParameter).append("=").append(publisherId_);
        __CLR3_1_5rri1wwpctu.R.inc(51);return protectUrl(url.toString());
    }
    
    public String protectUrl(String url_)
    {__CLR3_1_5rri1wwpctu.R.inc(52);
        __CLR3_1_5rri1wwpctu.R.inc(53);String hashResult = SHA256Hash(url_);
        __CLR3_1_5rri1wwpctu.R.inc(54);if ((((url_.contains("?"))&&(__CLR3_1_5rri1wwpctu.R.iget(55)!=0|true))||(__CLR3_1_5rri1wwpctu.R.iget(56)==0&false)))
        {{
            __CLR3_1_5rri1wwpctu.R.inc(57);url_ += "&" + HashParameter + "=" + hashResult;
        }
        }else
        {{
            __CLR3_1_5rri1wwpctu.R.inc(58);url_ += "?" + HashParameter + "=" + hashResult;
        }
        }__CLR3_1_5rri1wwpctu.R.inc(59);return url_;
    }
    
    private String SHA256Hash(String plainText) {__CLR3_1_5rri1wwpctu.R.inc(60);
        __CLR3_1_5rri1wwpctu.R.inc(61);MessageDigest md;
        __CLR3_1_5rri1wwpctu.R.inc(62);try {
            __CLR3_1_5rri1wwpctu.R.inc(63);md = MessageDigest.getInstance("SHA-256");

            __CLR3_1_5rri1wwpctu.R.inc(64);md.update((hashSeed + plainText).getBytes());
            __CLR3_1_5rri1wwpctu.R.inc(65);byte[] hashedBytes = md.digest();

            __CLR3_1_5rri1wwpctu.R.inc(66);StringBuilder hashString = new StringBuilder();
            __CLR3_1_5rri1wwpctu.R.inc(67);for (byte hashedByte : hashedBytes) {{
                __CLR3_1_5rri1wwpctu.R.inc(68);hashString.append(Integer.toString((hashedByte & 0xff) + 0x100, 16).substring(1));
            }
            }__CLR3_1_5rri1wwpctu.R.inc(69);return hashString.toString();
        } catch (NoSuchAlgorithmException e) { /* ignore */
            __CLR3_1_5rri1wwpctu.R.inc(70);return null;
        }
    } 
	
	
	private static String compress(String str) throws IOException {__CLR3_1_5rri1wwpctu.R.inc(71);
        __CLR3_1_5rri1wwpctu.R.inc(72);if ((((str == null || str.length() == 0)&&(__CLR3_1_5rri1wwpctu.R.iget(73)!=0|true))||(__CLR3_1_5rri1wwpctu.R.iget(74)==0&false))) {{
            __CLR3_1_5rri1wwpctu.R.inc(75);return str;
        } 
        }__CLR3_1_5rri1wwpctu.R.inc(76);ByteArrayOutputStream out = new ByteArrayOutputStream();
        __CLR3_1_5rri1wwpctu.R.inc(77);GZIPOutputStream gzip = new GZIPOutputStream(out);
        __CLR3_1_5rri1wwpctu.R.inc(78);gzip.write(str.getBytes());
        __CLR3_1_5rri1wwpctu.R.inc(79);gzip.close();  
		
		__CLR3_1_5rri1wwpctu.R.inc(80);byte[] data = out.toByteArray();  
		__CLR3_1_5rri1wwpctu.R.inc(81);char[] outChars = Base64Coder.encode(data);
 
        __CLR3_1_5rri1wwpctu.R.inc(82);return String.valueOf(outChars);
     }

}
