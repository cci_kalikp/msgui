/* $$ This file has been instrumented by Clover 3.1.5#20120425134455926 $$ */// Copyright 2003-2010 Christian d'Heureuse, Inventec Informatik AG, Zurich, Switzerland
// www.source-code.biz, www.inventec.ch/chdh
//
// This module is multi-licensed and may be used under the terms
// of any of the following licenses:
//
//  EPL, Eclipse Public License, V1.0 or later, http://www.eclipse.org/legal
//  LGPL, GNU Lesser General Public License, V2.1 or later, http://www.gnu.org/licenses/lgpl.html
//  GPL, GNU General Public License, V2 or later, http://www.gnu.org/licenses/gpl.html
//  AL, Apache License, V2.0 or later, http://www.apache.org/licenses
//  BSD, BSD License, http://www.opensource.org/licenses/bsd-license.php
//  MIT, MIT License, http://www.opensource.org/licenses/MIT
//
// Please contact the author if you need another license.
// This module is provided "as is", without warranties of any kind.

package com.ms.msdotnet.msdesktop.launchermessageapi;

/**
* A Base64 encoder/decoder.
*
* <p>
* This class is used to encode and decode data in Base64 format as described in RFC 1521.
*
* <p>
* Project home page: <a href="http://www.source-code.biz/base64coder/java/">www.source-code.biz/base64coder/java</a><br>
* Author: Christian d'Heureuse, Inventec Informatik AG, Zurich, Switzerland<br>
* Multi-licensed: EPL / LGPL / GPL / AL / BSD / MIT.
*/
public class Base64Coder {public static class __CLR3_1_52u2ui1wwpcv4{public static com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_1_5();if(20120425134455926L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.1.5#20120425134455926,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{67,58,92,77,83,68,69,92,112,106,101,110,107,98,108,100,92,119,111,114,107,115,112,97,99,101,92,109,115,100,111,116,110,101,116,45,109,115,103,117,105,45,114,101,108,101,97,115,101,45,116,114,117,110,107,92,109,115,100,111,116,110,101,116,92,109,115,103,117,105,92,116,114,117,110,107,92,115,114,99,92,101,120,97,109,112,108,101,115,92,72,116,116,112,83,101,114,118,101,114,69,120,97,109,112,108,101,92,74,97,118,97,76,97,117,110,99,104,101,114,77,101,115,115,97,103,101,92,98,117,105,108,100,92,46,99,108,111,118,101,114},null,1414721584319L,0L,254);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

// The line separator string of the operating system.
private static final String systemLineSeparator = System.getProperty("line.separator");

// Mapping table from 6-bit nibbles to Base64 characters.
private static final char[] map1 = new char[64];
   static {__CLR3_1_52u2ui1wwpcv4.R.inc(102);
      __CLR3_1_52u2ui1wwpcv4.R.inc(103);int i=0;
      __CLR3_1_52u2ui1wwpcv4.R.inc(104);for (char c='A'; (((c<='Z')&&(__CLR3_1_52u2ui1wwpcv4.R.iget(105)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(106)==0&false)); c++) {__CLR3_1_52u2ui1wwpcv4.R.inc(107);map1[i++] = c;
      }__CLR3_1_52u2ui1wwpcv4.R.inc(108);for (char c='a'; (((c<='z')&&(__CLR3_1_52u2ui1wwpcv4.R.iget(109)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(110)==0&false)); c++) {__CLR3_1_52u2ui1wwpcv4.R.inc(111);map1[i++] = c;
      }__CLR3_1_52u2ui1wwpcv4.R.inc(112);for (char c='0'; (((c<='9')&&(__CLR3_1_52u2ui1wwpcv4.R.iget(113)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(114)==0&false)); c++) {__CLR3_1_52u2ui1wwpcv4.R.inc(115);map1[i++] = c;
      }__CLR3_1_52u2ui1wwpcv4.R.inc(116);map1[i++] = '+'; __CLR3_1_52u2ui1wwpcv4.R.inc(117);map1[i++] = '/'; }

// Mapping table from Base64 characters to 6-bit nibbles.
private static final byte[] map2 = new byte[128];
   static {__CLR3_1_52u2ui1wwpcv4.R.inc(118);
      __CLR3_1_52u2ui1wwpcv4.R.inc(119);for (int i=0; (((i<map2.length)&&(__CLR3_1_52u2ui1wwpcv4.R.iget(120)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(121)==0&false)); i++) {__CLR3_1_52u2ui1wwpcv4.R.inc(122);map2[i] = -1;
      }__CLR3_1_52u2ui1wwpcv4.R.inc(123);for (int i=0; (((i<64)&&(__CLR3_1_52u2ui1wwpcv4.R.iget(124)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(125)==0&false)); i++) {__CLR3_1_52u2ui1wwpcv4.R.inc(126);map2[map1[i]] = (byte)i; }}

/**
* Encodes a string into Base64 format.
* No blanks or line breaks are inserted.
* @param s  A String to be encoded.
* @return   A String containing the Base64 encoded data.
*/
public static String encodeString (String s) {__CLR3_1_52u2ui1wwpcv4.R.inc(127);
   __CLR3_1_52u2ui1wwpcv4.R.inc(128);return new String(encode(s.getBytes())); }

/**
* Encodes a byte array into Base 64 format and breaks the output into lines of 76 characters.
* This method is compatible with <code>sun.misc.BASE64Encoder.encodeBuffer(byte[])</code>.
* @param in  An array containing the data bytes to be encoded.
* @return    A String containing the Base64 encoded data, broken into lines.
*/
public static String encodeLines (byte[] in) {__CLR3_1_52u2ui1wwpcv4.R.inc(129);
   __CLR3_1_52u2ui1wwpcv4.R.inc(130);return encodeLines(in, 0, in.length, 76, systemLineSeparator); }

/**
* Encodes a byte array into Base 64 format and breaks the output into lines.
* @param in            An array containing the data bytes to be encoded.
* @param iOff          Offset of the first byte in <code>in</code> to be processed.
* @param iLen          Number of bytes to be processed in <code>in</code>, starting at <code>iOff</code>.
* @param lineLen       Line length for the output data. Should be a multiple of 4.
* @param lineSeparator The line separator to be used to separate the output lines.
* @return              A String containing the Base64 encoded data, broken into lines.
*/
public static String encodeLines (byte[] in, int iOff, int iLen, int lineLen, String lineSeparator) {__CLR3_1_52u2ui1wwpcv4.R.inc(131);
   __CLR3_1_52u2ui1wwpcv4.R.inc(132);int blockLen = (lineLen*3) / 4;
   __CLR3_1_52u2ui1wwpcv4.R.inc(133);if ((((blockLen <= 0)&&(__CLR3_1_52u2ui1wwpcv4.R.iget(134)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(135)==0&false))) {__CLR3_1_52u2ui1wwpcv4.R.inc(136);throw new IllegalArgumentException();
   }__CLR3_1_52u2ui1wwpcv4.R.inc(137);int lines = (iLen+blockLen-1) / blockLen;
   __CLR3_1_52u2ui1wwpcv4.R.inc(138);int bufLen = ((iLen+2)/3)*4 + lines*lineSeparator.length();
   __CLR3_1_52u2ui1wwpcv4.R.inc(139);StringBuilder buf = new StringBuilder(bufLen);
   __CLR3_1_52u2ui1wwpcv4.R.inc(140);int ip = 0;
   __CLR3_1_52u2ui1wwpcv4.R.inc(141);while ((((ip < iLen)&&(__CLR3_1_52u2ui1wwpcv4.R.iget(142)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(143)==0&false))) {{
      __CLR3_1_52u2ui1wwpcv4.R.inc(144);int l = Math.min(iLen-ip, blockLen);
      __CLR3_1_52u2ui1wwpcv4.R.inc(145);buf.append (encode(in, iOff+ip, l));
      __CLR3_1_52u2ui1wwpcv4.R.inc(146);buf.append (lineSeparator);
      __CLR3_1_52u2ui1wwpcv4.R.inc(147);ip += l; }
   }__CLR3_1_52u2ui1wwpcv4.R.inc(148);return buf.toString(); }

/**
* Encodes a byte array into Base64 format.
* No blanks or line breaks are inserted in the output.
* @param in  An array containing the data bytes to be encoded.
* @return    A character array containing the Base64 encoded data.
*/
public static char[] encode (byte[] in) {__CLR3_1_52u2ui1wwpcv4.R.inc(149);
   __CLR3_1_52u2ui1wwpcv4.R.inc(150);return encode(in, 0, in.length); }

/**
* Encodes a byte array into Base64 format.
* No blanks or line breaks are inserted in the output.
* @param in    An array containing the data bytes to be encoded.
* @param iLen  Number of bytes to process in <code>in</code>.
* @return      A character array containing the Base64 encoded data.
*/
public static char[] encode (byte[] in, int iLen) {__CLR3_1_52u2ui1wwpcv4.R.inc(151);
   __CLR3_1_52u2ui1wwpcv4.R.inc(152);return encode(in, 0, iLen); }

/**
* Encodes a byte array into Base64 format.
* No blanks or line breaks are inserted in the output.
* @param in    An array containing the data bytes to be encoded.
* @param iOff  Offset of the first byte in <code>in</code> to be processed.
* @param iLen  Number of bytes to process in <code>in</code>, starting at <code>iOff</code>.
* @return      A character array containing the Base64 encoded data.
*/
public static char[] encode (byte[] in, int iOff, int iLen) {__CLR3_1_52u2ui1wwpcv4.R.inc(153);
   __CLR3_1_52u2ui1wwpcv4.R.inc(154);int oDataLen = (iLen*4+2)/3;       // output length without padding
   __CLR3_1_52u2ui1wwpcv4.R.inc(155);int oLen = ((iLen+2)/3)*4;         // output length including padding
   __CLR3_1_52u2ui1wwpcv4.R.inc(156);char[] out = new char[oLen];
   __CLR3_1_52u2ui1wwpcv4.R.inc(157);int ip = iOff;
   __CLR3_1_52u2ui1wwpcv4.R.inc(158);int iEnd = iOff + iLen;
   __CLR3_1_52u2ui1wwpcv4.R.inc(159);int op = 0;
   __CLR3_1_52u2ui1wwpcv4.R.inc(160);while ((((ip < iEnd)&&(__CLR3_1_52u2ui1wwpcv4.R.iget(161)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(162)==0&false))) {{
      __CLR3_1_52u2ui1wwpcv4.R.inc(163);int i0 = in[ip++] & 0xff;
      __CLR3_1_52u2ui1wwpcv4.R.inc(164);int i1 = (((ip < iEnd )&&(__CLR3_1_52u2ui1wwpcv4.R.iget(165)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(166)==0&false))? in[ip++] & 0xff : 0;
      __CLR3_1_52u2ui1wwpcv4.R.inc(167);int i2 = (((ip < iEnd )&&(__CLR3_1_52u2ui1wwpcv4.R.iget(168)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(169)==0&false))? in[ip++] & 0xff : 0;
      __CLR3_1_52u2ui1wwpcv4.R.inc(170);int o0 = i0 >>> 2;
      __CLR3_1_52u2ui1wwpcv4.R.inc(171);int o1 = ((i0 &   3) << 4) | (i1 >>> 4);
      __CLR3_1_52u2ui1wwpcv4.R.inc(172);int o2 = ((i1 & 0xf) << 2) | (i2 >>> 6);
      __CLR3_1_52u2ui1wwpcv4.R.inc(173);int o3 = i2 & 0x3F;
      __CLR3_1_52u2ui1wwpcv4.R.inc(174);out[op++] = map1[o0];
      __CLR3_1_52u2ui1wwpcv4.R.inc(175);out[op++] = map1[o1];
      __CLR3_1_52u2ui1wwpcv4.R.inc(176);out[op] = (((op < oDataLen )&&(__CLR3_1_52u2ui1wwpcv4.R.iget(177)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(178)==0&false))? map1[o2] : '='; __CLR3_1_52u2ui1wwpcv4.R.inc(179);op++;
      __CLR3_1_52u2ui1wwpcv4.R.inc(180);out[op] = (((op < oDataLen )&&(__CLR3_1_52u2ui1wwpcv4.R.iget(181)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(182)==0&false))? map1[o3] : '='; __CLR3_1_52u2ui1wwpcv4.R.inc(183);op++; }
   }__CLR3_1_52u2ui1wwpcv4.R.inc(184);return out; }

/**
* Decodes a string from Base64 format.
* No blanks or line breaks are allowed within the Base64 encoded input data.
* @param s  A Base64 String to be decoded.
* @return   A String containing the decoded data.
* @throws   IllegalArgumentException If the input is not valid Base64 encoded data.
*/
public static String decodeString (String s) {__CLR3_1_52u2ui1wwpcv4.R.inc(185);
   __CLR3_1_52u2ui1wwpcv4.R.inc(186);return new String(decode(s)); }

/**
* Decodes a byte array from Base64 format and ignores line separators, tabs and blanks.
* CR, LF, Tab and Space characters are ignored in the input data.
* This method is compatible with <code>sun.misc.BASE64Decoder.decodeBuffer(String)</code>.
* @param s  A Base64 String to be decoded.
* @return   An array containing the decoded data bytes.
* @throws   IllegalArgumentException If the input is not valid Base64 encoded data.
*/
public static byte[] decodeLines (String s) {__CLR3_1_52u2ui1wwpcv4.R.inc(187);
   __CLR3_1_52u2ui1wwpcv4.R.inc(188);char[] buf = new char[s.length()];
   __CLR3_1_52u2ui1wwpcv4.R.inc(189);int p = 0;
   __CLR3_1_52u2ui1wwpcv4.R.inc(190);for (int ip = 0; (((ip < s.length())&&(__CLR3_1_52u2ui1wwpcv4.R.iget(191)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(192)==0&false)); ip++) {{
      __CLR3_1_52u2ui1wwpcv4.R.inc(193);char c = s.charAt(ip);
      __CLR3_1_52u2ui1wwpcv4.R.inc(194);if ((((c != ' ' && c != '\r' && c != '\n' && c != '\t')&&(__CLR3_1_52u2ui1wwpcv4.R.iget(195)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(196)==0&false)))
         {__CLR3_1_52u2ui1wwpcv4.R.inc(197);buf[p++] = c; }}
   }__CLR3_1_52u2ui1wwpcv4.R.inc(198);return decode(buf, 0, p); }

/**
* Decodes a byte array from Base64 format.
* No blanks or line breaks are allowed within the Base64 encoded input data.
* @param s  A Base64 String to be decoded.
* @return   An array containing the decoded data bytes.
* @throws   IllegalArgumentException If the input is not valid Base64 encoded data.
*/
public static byte[] decode (String s) {__CLR3_1_52u2ui1wwpcv4.R.inc(199);
   __CLR3_1_52u2ui1wwpcv4.R.inc(200);return decode(s.toCharArray()); }

/**
* Decodes a byte array from Base64 format.
* No blanks or line breaks are allowed within the Base64 encoded input data.
* @param in  A character array containing the Base64 encoded data.
* @return    An array containing the decoded data bytes.
* @throws    IllegalArgumentException If the input is not valid Base64 encoded data.
*/
public static byte[] decode (char[] in) {__CLR3_1_52u2ui1wwpcv4.R.inc(201);
   __CLR3_1_52u2ui1wwpcv4.R.inc(202);return decode(in, 0, in.length); }

/**
* Decodes a byte array from Base64 format.
* No blanks or line breaks are allowed within the Base64 encoded input data.
* @param in    A character array containing the Base64 encoded data.
* @param iOff  Offset of the first character in <code>in</code> to be processed.
* @param iLen  Number of characters to process in <code>in</code>, starting at <code>iOff</code>.
* @return      An array containing the decoded data bytes.
* @throws      IllegalArgumentException If the input is not valid Base64 encoded data.
*/
public static byte[] decode (char[] in, int iOff, int iLen) {__CLR3_1_52u2ui1wwpcv4.R.inc(203);
   __CLR3_1_52u2ui1wwpcv4.R.inc(204);if ((((iLen%4 != 0)&&(__CLR3_1_52u2ui1wwpcv4.R.iget(205)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(206)==0&false))) {__CLR3_1_52u2ui1wwpcv4.R.inc(207);throw new IllegalArgumentException ("Length of Base64 encoded input string is not a multiple of 4.");
   }__CLR3_1_52u2ui1wwpcv4.R.inc(208);while ((((iLen > 0 && in[iOff+iLen-1] == '=')&&(__CLR3_1_52u2ui1wwpcv4.R.iget(209)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(210)==0&false))) {__CLR3_1_52u2ui1wwpcv4.R.inc(211);iLen--;
   }__CLR3_1_52u2ui1wwpcv4.R.inc(212);int oLen = (iLen*3) / 4;
   __CLR3_1_52u2ui1wwpcv4.R.inc(213);byte[] out = new byte[oLen];
   __CLR3_1_52u2ui1wwpcv4.R.inc(214);int ip = iOff;
   __CLR3_1_52u2ui1wwpcv4.R.inc(215);int iEnd = iOff + iLen;
   __CLR3_1_52u2ui1wwpcv4.R.inc(216);int op = 0;
   __CLR3_1_52u2ui1wwpcv4.R.inc(217);while ((((ip < iEnd)&&(__CLR3_1_52u2ui1wwpcv4.R.iget(218)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(219)==0&false))) {{
      __CLR3_1_52u2ui1wwpcv4.R.inc(220);int i0 = in[ip++];
      __CLR3_1_52u2ui1wwpcv4.R.inc(221);int i1 = in[ip++];
      __CLR3_1_52u2ui1wwpcv4.R.inc(222);int i2 = (((ip < iEnd )&&(__CLR3_1_52u2ui1wwpcv4.R.iget(223)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(224)==0&false))? in[ip++] : 'A';
      __CLR3_1_52u2ui1wwpcv4.R.inc(225);int i3 = (((ip < iEnd )&&(__CLR3_1_52u2ui1wwpcv4.R.iget(226)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(227)==0&false))? in[ip++] : 'A';
      __CLR3_1_52u2ui1wwpcv4.R.inc(228);if ((((i0 > 127 || i1 > 127 || i2 > 127 || i3 > 127)&&(__CLR3_1_52u2ui1wwpcv4.R.iget(229)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(230)==0&false)))
         {__CLR3_1_52u2ui1wwpcv4.R.inc(231);throw new IllegalArgumentException ("Illegal character in Base64 encoded data.");
      }__CLR3_1_52u2ui1wwpcv4.R.inc(232);int b0 = map2[i0];
      __CLR3_1_52u2ui1wwpcv4.R.inc(233);int b1 = map2[i1];
      __CLR3_1_52u2ui1wwpcv4.R.inc(234);int b2 = map2[i2];
      __CLR3_1_52u2ui1wwpcv4.R.inc(235);int b3 = map2[i3];
      __CLR3_1_52u2ui1wwpcv4.R.inc(236);if ((((b0 < 0 || b1 < 0 || b2 < 0 || b3 < 0)&&(__CLR3_1_52u2ui1wwpcv4.R.iget(237)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(238)==0&false)))
         {__CLR3_1_52u2ui1wwpcv4.R.inc(239);throw new IllegalArgumentException ("Illegal character in Base64 encoded data.");
      }__CLR3_1_52u2ui1wwpcv4.R.inc(240);int o0 = ( b0       <<2) | (b1>>>4);
      __CLR3_1_52u2ui1wwpcv4.R.inc(241);int o1 = ((b1 & 0xf)<<4) | (b2>>>2);
      __CLR3_1_52u2ui1wwpcv4.R.inc(242);int o2 = ((b2 &   3)<<6) |  b3;
      __CLR3_1_52u2ui1wwpcv4.R.inc(243);out[op++] = (byte)o0;
      __CLR3_1_52u2ui1wwpcv4.R.inc(244);if ((((op<oLen)&&(__CLR3_1_52u2ui1wwpcv4.R.iget(245)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(246)==0&false))) {__CLR3_1_52u2ui1wwpcv4.R.inc(247);out[op++] = (byte)o1;
      }__CLR3_1_52u2ui1wwpcv4.R.inc(248);if ((((op<oLen)&&(__CLR3_1_52u2ui1wwpcv4.R.iget(249)!=0|true))||(__CLR3_1_52u2ui1wwpcv4.R.iget(250)==0&false))) {__CLR3_1_52u2ui1wwpcv4.R.inc(251);out[op++] = (byte)o2; }}
   }__CLR3_1_52u2ui1wwpcv4.R.inc(252);return out; }

// Dummy constructor.
private Base64Coder() {__CLR3_1_52u2ui1wwpcv4.R.inc(253);}

} // end class Base64Coder