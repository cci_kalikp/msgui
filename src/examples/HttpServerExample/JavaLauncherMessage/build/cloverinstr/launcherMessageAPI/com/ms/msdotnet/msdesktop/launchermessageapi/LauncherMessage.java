/* $$ This file has been instrumented by Clover 3.1.5#20120425134455926 $$ */package com.ms.msdotnet.msdesktop.launchermessageapi;


import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.ms.msdotnet.msdesktop.launchermessageapi.serialization.IntValueEnum;
import com.ms.msdotnet.msdesktop.launchermessageapi.serialization.IntValueEnumSerializer; 

public class LauncherMessage {public static class __CLR3_1_52b2bi1wwpcuc{public static com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_1_5();if(20120425134455926L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.1.5#20120425134455926,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{67,58,92,77,83,68,69,92,112,106,101,110,107,98,108,100,92,119,111,114,107,115,112,97,99,101,92,109,115,100,111,116,110,101,116,45,109,115,103,117,105,45,114,101,108,101,97,115,101,45,116,114,117,110,107,92,109,115,100,111,116,110,101,116,92,109,115,103,117,105,92,116,114,117,110,107,92,115,114,99,92,101,120,97,109,112,108,101,115,92,72,116,116,112,83,101,114,118,101,114,69,120,97,109,112,108,101,92,74,97,118,97,76,97,117,110,99,104,101,114,77,101,115,115,97,103,101,92,98,117,105,108,100,92,46,99,108,111,118,101,114},null,1414721584319L,0L,102);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

    private String type;
    private String content;
    private int format;
    
    public LauncherMessage(String type, Map<String, Object> dictionary, SimpleModule module) throws JsonProcessingException {__CLR3_1_52b2bi1wwpcuc.R.inc(83);
        __CLR3_1_52b2bi1wwpcuc.R.inc(84);ObjectMapper mapper = new ObjectMapper(); 
        __CLR3_1_52b2bi1wwpcuc.R.inc(85);mapper.registerModule(module);
        __CLR3_1_52b2bi1wwpcuc.R.inc(86);mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ"));
        
        __CLR3_1_52b2bi1wwpcuc.R.inc(87);this.format = 1; //json
        __CLR3_1_52b2bi1wwpcuc.R.inc(88);this.type = type;
        __CLR3_1_52b2bi1wwpcuc.R.inc(89);this.content = mapper.writeValueAsString(dictionary);
    } 
	
    @JsonProperty("Type")
    public String getType() {__CLR3_1_52b2bi1wwpcuc.R.inc(90);
        __CLR3_1_52b2bi1wwpcuc.R.inc(91);return type;
    }

    public void setType(String type) {__CLR3_1_52b2bi1wwpcuc.R.inc(92);
        __CLR3_1_52b2bi1wwpcuc.R.inc(93);this.type = type;
    }

    @JsonProperty("Content")
    public String getContent() {__CLR3_1_52b2bi1wwpcuc.R.inc(94);
        __CLR3_1_52b2bi1wwpcuc.R.inc(95);return content;
    }

    public void setContent(String content) {__CLR3_1_52b2bi1wwpcuc.R.inc(96);
        __CLR3_1_52b2bi1wwpcuc.R.inc(97);this.content = content;
    }

    @JsonProperty("Format")
    public int getFormat() {__CLR3_1_52b2bi1wwpcuc.R.inc(98);
        __CLR3_1_52b2bi1wwpcuc.R.inc(99);return format;
    }

    public void setFormat(int format) {__CLR3_1_52b2bi1wwpcuc.R.inc(100);
        __CLR3_1_52b2bi1wwpcuc.R.inc(101);this.format = format;
    }
}
