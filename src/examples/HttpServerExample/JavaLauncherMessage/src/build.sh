#!/usr/bin/ksh
export ANT_OPTS="-XX:MaxPermSize=512M -Xmx1024m"
module load msjava/sunjdk/1.6.0_27
module load ossjava/ant/1.8.4

ant $*
