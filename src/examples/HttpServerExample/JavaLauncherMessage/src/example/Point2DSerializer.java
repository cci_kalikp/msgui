package com.ms.msdotnet.msdesktop.examples;

import java.awt.geom.Point2D;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class Point2DSerializer extends JsonSerializer<Point2D> {

    @Override
    public void serialize(Point2D value, JsonGenerator jgen, SerializerProvider provider) throws IOException,
            JsonProcessingException {
        jgen.writeString(value.getX() + "," + value.getY());
    }

}
