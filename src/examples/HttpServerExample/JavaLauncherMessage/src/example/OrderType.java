package com.ms.msdotnet.msdesktop.examples;

import com.ms.msdotnet.msdesktop.launchermessageapi.serialization.IntValueEnum;

public enum OrderType implements IntValueEnum {
    Buy(1), Sell(0);

    private int value;
    
    OrderType(int i) {
        this.value = i;
    }
    
    @Override
    public int getValue() {
        return value;
    }

}
