package com.ms.msdotnet.msdesktop.launchermessageapi.serialization;

public interface IntValueEnum {
    int getValue();
}
