package com.ms.msdotnet.msdesktop.launchermessageapi;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer; 
import com.ms.msdotnet.msdesktop.launchermessageapi.serialization.IntValueEnum; 
import com.ms.msdotnet.msdesktop.launchermessageapi.serialization.IntValueEnumSerializer;

public class DispatchableLauncherMessage {

    private static HTTPRequestBuilder requestBuilder = new HTTPRequestBuilder("localhost", LauncherConsts.HttpServerPort, LauncherConsts.EnableHash, LauncherConsts.HashSeed);
    
    public static String generateLink(String messageTypeName_, Map<String, Object> messageProperties_, String applicationKey_) throws JsonProcessingException, IOException  {
        return generateLink(messageTypeName_, messageProperties_, applicationKey_, true, true);
    }
        
    public static String generateLink(String messageTypeName_, Map<String, Object> messageProperties_, String applicationKey_, boolean allowNewApplication_) throws JsonProcessingException, IOException  {
        return generateLink(messageTypeName_, messageProperties_, applicationKey_, allowNewApplication_, true);
    }      
    
    public static String generateLink(String messageTypeName_, Map<String, Object> messageProperties_, String applicationKey_, boolean allowNewApplication_, boolean allowSaveRoute_) throws JsonProcessingException, IOException {
        Map<String, Object> dictionary = new HashMap<String, Object>();
        dictionary.put("AppKey",applicationKey_);
		if (!allowNewApplication_)
		{
           dictionary.put("CanNewApp", allowNewApplication_);
		}
		if (!allowSaveRoute_)
		{ 
           dictionary.put("CanSaveRoute", allowSaveRoute_);
		}
        dictionary.put("Payload", new LauncherMessage(messageTypeName_, messageProperties_,GetModule())); 
        String json = new ObjectMapper().writeValueAsString(dictionary);
        
        return requestBuilder.buildPublishUrl(json, LauncherConsts.DispatchApplicationMessagePublisherId);
    }
	
	private static SimpleModule module;
	public static <T> void addSerializer(Class<? extends T> type, JsonSerializer<T> ser)
	{ 
	     GetModule().addSerializer(type, ser);
	}
	
	private static SimpleModule GetModule()
	{
		 if (module == null)
		 {
		      module = new SimpleModule();
			  module.addSerializer(IntValueEnum.class, new IntValueEnumSerializer());
		 }
		 return module;
	}
}
