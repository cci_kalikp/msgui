﻿using System;
using System.Net;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Web;
using System.Windows.Input;
using Microsoft.Practices.Composite.Presentation.Commands;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.MSGui.Impl.HTTPServer;

namespace HttpServerExample
{
    public class UrlViewModel : ViewModelBase
    {
        //cw HTTPServer: 39
        /**
         * #### Create HttpRequestBuilder using the same setting as the HTTPServer to generate the http request urls
         * Here we create a raw request builder without hashing enabled and a protected request builder later on with hashing enabled
         */
        //{
        public const int Port = 8081;
        public static readonly string HashSeed = HttpRequestBuilder.GenerateHashSeed();
        private static readonly HttpRequestBuilder rawRequestBuilder = new HttpRequestBuilder(HttpRequestBuilder.Localhost, Port, false, null); 
        //}
        //cw HTTPServer: 83
        //{
        private static readonly HttpRequestBuilder protectedRequestBuilder = new HttpRequestBuilder(HttpRequestBuilder.Localhost, Port, true, HashSeed);
        //}

        public UrlViewModel(string rawUrl_, string title_)
        {
            this.title = title_;
            this.RawUrl = rawUrl_; 
            this.RequestUrlCommand = new DelegateCommand<object>(RequestUrl);
        }


        public static UrlViewModel ForCreateViewRequest(string factory_, XDocument state_, string title_ = null)
        {
            if (string.IsNullOrWhiteSpace(title_))
            {
                title_ = string.Format("Click to create a viewer from factory {0}", factory_);
            }
            //cw HTTPServer: 40
            /**
             * #### Get the URL string for creating window
             * Specify a factory id and transferring the state if there are some persistence information to be restored by the window to the HttpRequestBuilder created previously.
             */
            //{
            string rawUrl = rawRequestBuilder.BuildCreateViewUrl(factory_, state_); 
            //}
            return new UrlViewModel(rawUrl, title_);
        }

        public static UrlViewModel ForRegisterPulisherRequest(Type messageType_, string title_ = null)
        {
            if (string.IsNullOrWhiteSpace(title_))
            {
                title_ = string.Format("Click to register Module Publisher for event {0}", messageType_.FullName);
            }
            //cw HTTPServer: 45
            /**
             * #### Get the URL string for registering a module publisher for the message type specified
             * The session id for this registration can be specified or generated automatically, and is returned for later use if.
             */
            //{
            string publisherId = null;
            string rawUrl = rawRequestBuilder.BuildRegisterPulisherUrl(messageType_, ref publisherId); 
            //} 
            UrlViewModel model = new UrlViewModel(rawUrl, title_); 
            //model.allowRevisit = false;
            return model;
        }

        public static UrlViewModel ForRegisterSubscriberRequest(string moduleName_, string callbackMethod_, Type messageType_, string title_=null)
        {
            if (string.IsNullOrWhiteSpace(title_))
            {
                title_ = string.Format("Click to register Module Subscriber for event {0} callback into method {1}", messageType_.FullName,
                    string.IsNullOrWhiteSpace(callbackMethod_) ? "ConsumeMessage" : callbackMethod_);
            }
            //cw HTTPServer: 50
            /**
             * #### Get the URL string for registering a module subscriber for the message type specified
             * The name of the callbackMethod should be a public method on the subscriber module;
             * The subscriber module should also inherit from HTTPServerSubscriberModule, so that the HTTPServer can find it and invoke the callback method.
             * The session id for this registration can be specified or generated automatically, and is returned for later use if.
             */
            //{
            string subscriberId = null;
            string rawUrl = rawRequestBuilder.BuildRegisterSubscriberUrl(moduleName_, callbackMethod_, messageType_, ref subscriberId); 
            //} 
            UrlViewModel model = new UrlViewModel(rawUrl, title_); 
            model.allowRevisit = false;
            return model;
        }

        public static UrlViewModel ForPulishRequest(object message_, DataFormat format_, string publisherId_, string title_=null)
        {
            if (string.IsNullOrWhiteSpace(title_))
            {
                title_ = string.Format("Click to publish an event {0} using format {1} from module", message_.GetType().FullName, format_);
            }

            //cw HTTPServer: 55
            /**
             * #### Get the URL string for publishing the message from the module, using either XML format or Json format
             * The publisher id provided is returned/provided when the module publisher is registered.
             **/
            //{
            string rawUrl = rawRequestBuilder.BuildPulishUrl(message_, format_, publisherId_); 
            //}
            return new UrlViewModel(rawUrl, title_);
        }


        public static UrlViewModel ForCreatePulisherViewRequest(string factory_, XDocument state_, Type messageType_, string title_ = null)
        {
            if (string.IsNullOrWhiteSpace(title_))
            {
                title_ = string.Format("Click to create a view from factory {0} and register as a v2v publisher for event {1}", factory_, messageType_.FullName);
            }

            //cw HTTPServer: 70
            /**
             * #### Get the URL string for creating a window and at the same time register it as a view publisher for the message type specified
             * The session id for this registration is returned for later use. 
             **/
            //{
            string publisherId;
            string rawUrl = rawRequestBuilder.BuildCreatePulisherViewUrl(factory_, state_, messageType_, out publisherId); 
            //}
            return new UrlViewModel(rawUrl, title_);  
        }

        public static UrlViewModel ForCreateSubscriberViewRequest(string factory_, XDocument state_, string callbackMethod_, Type messageType_, string title_=null)
        {
            if (string.IsNullOrWhiteSpace(title_))
            {
                title_ = string.Format("Click to create a view from factory {0} and register as v2v subscriber for event {1} callback into method {2}", factory_,
                    messageType_.FullName, callbackMethod_);
            }
            //cw HTTPServer: 75
            /**
             * #### Get the URL string for creating a window and at the same time register it as a view subscriber for the message type specified
             * The callbackMethod must be a public method available on the subscriber view.
             * The session id for this registration is returned for later use. 
             * */
            //{
            string subscriberId;
            string rawUrl = rawRequestBuilder.BuildCreateSubscriberViewUrl(factory_, state_, callbackMethod_, messageType_, out subscriberId); 
            //}
            return new UrlViewModel(rawUrl, title_); 
        }


        public static UrlViewModel ForRegisterV2VPulisherRequest(string viewId_, Type messageType_, string title_=null)
        {
            if (string.IsNullOrWhiteSpace(title_))
            {
                title_ = string.Format("Click to register a v2v publisher for event {0}", messageType_.FullName);
            }

            //cw HTTPServer: 60
            /**
             * #### Get the URL string for registering a view as publisher for the message type specified
             * The session id for this registration is returned for later use. 
             **/
            //{
            string publisherId;
            string rawUrl = rawRequestBuilder.BuildRegisterV2VPulisherUrl(viewId_, messageType_, out publisherId); 
            //}
            UrlViewModel model = new UrlViewModel(rawUrl, title_); 
            model.allowRevisit = false;
            return model;
        }

        public static UrlViewModel ForRegisterV2VSubscriberRequest(string viewId_, string callbackMethod_, Type messageType_, string title_=null)
        {
            if (string.IsNullOrWhiteSpace(title_))
            {
                title_ = string.Format("Click to register a v2v subscriber for event {0} callback into method {1}", messageType_.FullName, callbackMethod_);
            }
            //cw HTTPServer: 65
            /**
             * #### Get the URL string for registering a view as subscriber for the message type specified
             * The callbackMethod must be a public method available on the subscriber view.
             * The session id for this registration is returned for later use. 
             **/
            //{
            string subscriberId;
            string rawUrl = rawRequestBuilder.BuildRegisterV2VSubscriberUrl(viewId_, callbackMethod_, messageType_, out subscriberId); 
            //}
            UrlViewModel model = new UrlViewModel(rawUrl, title_); 
            model.allowRevisit = false;
            return model;
        }

        public static UrlViewModel ForV2VPulishRequest(object message_, DataFormat format_, string publisherId_, string title_=null)
        {
            if (string.IsNullOrWhiteSpace(title_))
            {
                title_ = string.Format("Click to publish an event {0} using format {1} from view", message_.GetType().FullName, format_);
            }
            //cw HTTPServer: 80
            /**
             * #### Get the URL string for publishing the message from view, using either XML format or Json format
             * The publisher id provided is returned/provided when the module publisher is registered.
             **/
            //{
            string rawUrl = rawRequestBuilder.BuildV2VPulishUrl(message_, format_, publisherId_); 
            //}
            return new UrlViewModel(rawUrl, title_);
        }

        private string title;
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
                OnPropertyChanged("Title");
            }
        }

        private ActionType action;
        public ActionType Action
        {
            get
            {
                return action;
            }
            set
            {
                action = value;
                OnPropertyChanged("Action");
            }
        }

        private ObservableCollection<NameValuePair> parameters = new ObservableCollection<NameValuePair>();
        public ObservableCollection<NameValuePair> Parameters
        {
            get
            {
                return parameters;
            }
            set
            {
                parameters = value;
                OnPropertyChanged("Parameters");
            }
        }

        private string rawUrl;
        public string RawUrl
        {
            get
            {
                return rawUrl;
            }
            set
            {
                rawUrl = value;
                OnPropertyChanged("RawUrl");
                ParseUrl();
            }
        }

        private string protectedUrl;

        public string ProtectedUrl
        {
            get
            {
                return protectedUrl;
            }
            set
            {
                protectedUrl = value;
                OnPropertyChanged("ProtectedUrl");
            }
        }
        private string sessionId;
        public string SessionId
        {
            get
            {
                return sessionId;
            }
            set
            {
                sessionId = value;
                OnPropertyChanged("SessionId");
            }
        }

        private bool visited;
        public bool Visited
        {
            get
            {
                return visited;
            }
            set
            {
                visited = value;
                OnPropertyChanged("Visited");
            }
        }

        private bool canBeVisisted = true;
        public bool CanBeVisited
        {
            get
            {
                return canBeVisisted;
            }
            set
            {
                canBeVisisted = value;
                OnPropertyChanged("CanBeVisited");
            }
        }

        public ICommand RequestUrlCommand { get; set; }

        private string result;
        public string Result
        {
            get { return result; }
            set
            {
                result = value;
                OnPropertyChanged("Result");
            }
        }

        private bool allowRevisit = true;
        public virtual void RequestUrl(object parameters)
        {
            //cw HTTPServer: 85
            /**
             * #### Send the url as request to the server
             * */
            //{
            WebClient webClient = new WebClient();
            Result = webClient.DownloadString(ProtectedUrl);
            //}
            Visited = true;
            if (!allowRevisit)
            {
                CanBeVisited = false;
            }
        }

        private void ParseUrl()
        {
            Uri uri = new Uri(rawUrl); 
            Action = HttpRequestBuilder.ParseActionAPI(uri);
            //cw HTTPServer: 82
            /**
             * #### Protect a raw url (or could use the protected HttpRequestBuilder to create the url directly)
             **/
            //cw HTTPServer: 84
            //{
            ProtectedUrl = protectedRequestBuilder.ProtectUrl(rawUrl);
            //}
            parameters.Clear();
            NameValueCollection values = HttpUtility.ParseQueryString(uri.Query);
            foreach (string key in values.AllKeys)
            {
                if (key == "id")
                {
                    SessionId = values[key];
                }
                else
                {
                    parameters.Add(new NameValuePair(key, values[key]));
                }
            }
            
        }
         
    }

    public class NameValuePair
    {
        public NameValuePair(string name_, string value_)
        {
            this.Name = name_;
            this.Value = value_;
        }

        public string Name { get; set; }
        public string Value { get; set; }
    }
    
}
