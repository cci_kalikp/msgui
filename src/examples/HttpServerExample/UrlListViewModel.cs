﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.MSGui.Impl.HTTPServer; 

namespace HttpServerExample
{
    public enum UrlListUsage
    {
        Initiation,
        PublisherView,
        SubscriberView,
    }
    
    public class UrlListViewModel : ViewModelBase
    {
        public UrlListViewModel(UrlListUsage usage_, string sessionId_=null, string viewId_=null)
        {
            urls = new ObservableCollection<UrlViewModel>();
            switch (usage_)
            {
                case UrlListUsage.Initiation: 
                    AddModel(UrlViewModel.ForRegisterPulisherRequest(typeof(Trade)));
                    AddModel(UrlViewModel.ForRegisterSubscriberRequest("SubscribingModule",null, typeof(Trade)));
                    AddModel(UrlViewModel.ForCreateViewRequest("Publisher", CreateState(new InitialWindowParameters() { Singleton = true, SingletonKey = "Publisher", InitialLocation = InitialLocation.DockInNewTab, IsHeaderVisible = true })));
                    AddModel(UrlViewModel.ForCreateViewRequest("Subscriber", CreateState(new InitialWindowParameters() { Singleton = true, SingletonKey = "Subscriber", InitialLocation = InitialLocation.DockInNewTab, IsHeaderVisible = true })));
                    AddModel(UrlViewModel.ForCreatePulisherViewRequest("Publisher", CreateState(new InitialWindowParameters() { Singleton = true, SingletonKey = "Publisher2", InitialLocation = InitialLocation.DockInNewTab, IsHeaderVisible = true }), 
                        typeof(Trade)));
                    AddModel(UrlViewModel.ForCreateSubscriberViewRequest("Subscriber", CreateState(new InitialWindowParameters() { Singleton = true, SingletonKey = "Subscriber2", InitialLocation = InitialLocation.DockInNewTab, IsHeaderVisible = true }),
                        "UpdateUI", typeof(Trade)));
                    break;
                case UrlListUsage.PublisherView:
                    if (string.IsNullOrWhiteSpace(sessionId_))
                    {
                        AddModel(UrlViewModel.ForRegisterV2VPulisherRequest(viewId_, typeof(Trade)));
                    }
                    else
                    {
                        AddModel(UrlViewModel.ForV2VPulishRequest(new Trade() { Symbol = "MS", Quantity = 90, Price = 100, TradeType = TradeType.Buy }, DataFormat.Xml, sessionId_));
                        AddModel(UrlViewModel.ForV2VPulishRequest(new Trade() { Symbol = "MS", Quantity = 80, Price = 101, TradeType = TradeType.Sell }, DataFormat.Json, sessionId_));
                    }
                    break;
                case UrlListUsage.SubscriberView:
                    if (string.IsNullOrWhiteSpace(sessionId_))
                    {
                        AddModel(UrlViewModel.ForRegisterV2VSubscriberRequest(viewId_, "UpdateUI", typeof(Trade)));
                    }
                    break;
                default:
                    break;
            }
        }

        

        private void AddModel(UrlViewModel model_)
        {
            urls.Add(model_);
            model_.PropertyChanged += new PropertyChangedEventHandler(model_PropertyChanged);
        }

        void model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Visited")
            {
                UrlViewModel model = sender as UrlViewModel;
                if (model != null)
                {
                    var handler = UrlVisited;
                    if (handler != null)
                    {
                        handler(this, new UrlVisitedEvent(model));
                    }
                }
                
            }
        }

        public class UrlVisitedEvent:EventArgs
        {
            public UrlVisitedEvent(UrlViewModel model_)
            {
                Model = model_;
            }

            public UrlViewModel Model { get; private set; }
        }
        public static event EventHandler<UrlVisitedEvent> UrlVisited;

        //we could move this to the HttpRequestBuilder by specifying an InitialWindowParameters and size, 
        //but we would lose the agility of using a state xml to transfer more complex information
        private static XDocument CreateState(InitialWindowParameters parameters_=null)
        {
            XDocument state = new XDocument();
            XElement root = new XElement("state");
            if (parameters_ != null)
            {
                XElement parametersElement = new XElement("parameters", HttpRequestBuilder.SerializeToString(parameters_, DataFormat.Json));
                root.Add(parametersElement);
            }
            state.Add(root);
            return state;
        }

        //we could move this to the HttpServerModule by parsing the state into an InitialWindowParameters, 
        //but we would lose the agility of using a state xml to transfer more complex information
        internal static bool ParseState(XDocument state_, InitialWindowParameters parameters_, out string sessionId_)
        { 
            sessionId_ = null;
            if (state_ == null) return false;
            var sessionIdNode = state_.Root.Elements().FirstOrDefault(xe => xe.Name == "id");
            if (sessionIdNode != null)
            { 
                sessionId_ = sessionIdNode.Value;
            }  
            var parameterNode = state_.Root.Elements().FirstOrDefault(xe => xe.Name == "parameters");
            if (parameterNode != null)
            {
                var parameters = HttpRequestBuilder.DeserializeFromString(parameterNode.Value, typeof(InitialWindowParameters), DataFormat.Json) as InitialWindowParameters;
                if (parameters != null)
                {
                    parameters_.AllowPartiallyOffscreen = parameters.AllowPartiallyOffscreen;
                    parameters_.EnforceSizeRestrictions = parameters.EnforceSizeRestrictions;
                    parameters_.Height = parameters.Height;
                    parameters_.InitialLocation = parameters.InitialLocation;
                    parameters_.InitialLocationCallback = parameters.InitialLocationCallback;
                    parameters_.InitialLocationTarget = parameters.InitialLocationTarget;
                    parameters_.IsHeaderVisible = parameters.IsHeaderVisible;
                    parameters_.IsModal = parameters.IsModal;
                    parameters_.ResizeMode = parameters.ResizeMode;
                    parameters_.Resources = parameters.Resources;
                    parameters_.ShowFlashBorder = parameters.ShowFlashBorder;
                    parameters_.ShowInTaskbar = parameters.ShowInTaskbar;
                    parameters_.Singleton = parameters.Singleton;
                    parameters_.SingletonKey = parameters.SingletonKey;
                    parameters_.SizingMethod = parameters.SizingMethod;
                    parameters_.Topmost = parameters.Topmost;
                    parameters_.UseDockManager = parameters.UseDockManager;
                    parameters_.Width = parameters.Width;
                }
            }
            return true;
        }
        private ObservableCollection<UrlViewModel> urls;
        public ObservableCollection<UrlViewModel> Urls
        {
            get
            {
                return urls;
            }
            set
            {
                urls = value;
                OnPropertyChanged("Urls");
            }
        }
          
    }
}
