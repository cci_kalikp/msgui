﻿using System;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MSDesktop.HTTPServer;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.StatusBar;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Impl.HTTPServer;

namespace HttpServerExample.ModuleToModule
{
    //cw HTTPServer: 51
    //{
    public class SubscribingModule : HTTPServerSubscriberModule
    //}
    { 
     
        public SubscribingModule(IUnityContainer unityContainer_):base(unityContainer_)
        {
            
        }

        protected override string ModuleName
        {
            get { return "SubscribingModule"; }
        }

        IStatusLabelViewContainer statusLabel;
        protected override void InitializeCore()
        {
            statusLabel = chromeManager.AddStatusLabel(Guid.NewGuid().ToString(),
                                                   new InitialStatusLabelParameters
                                                   {
                                                       Level = StatusLevel.Information
                                                   });
            UrlListViewModel.UrlVisited += new EventHandler<UrlListViewModel.UrlVisitedEvent>(UrlsViewModel_UrlVisited);
        }

        void UrlsViewModel_UrlVisited(object sender, UrlListViewModel.UrlVisitedEvent e)
        {
            if (e.Model.Action == ActionType.RegisterSubscriber)
            {
                statusLabel.Text = "Subscriber Registered";
            }
        }

        public override void ConsumeMessage<T>(T message_)
        {
            Trade trade = message_ as Trade;
            if (trade != null)
            {
                UpdatetUI(trade);
            }
        }

        //cw HTTPServer: 52
        //{
        public void UpdatetUI(Trade trade_)
        {
            Dispatcher.CurrentDispatcher.Invoke(
                new Action(() => {
                    statusLabel.Level = StatusLevel.Critical;
                    statusLabel.Text = string.Format("{0} {2} Stocks of {1}  with Price {3:C}", trade_.TradeType, trade_.Symbol, trade_.Quantity, trade_.Price); 
                }));

        }
        //}
         
    }
}
