﻿using System;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 
using System.Xml.Linq;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.HTTPServer;

namespace HttpServerExample.ModuleToModule
{
    public class PublishingModule : IModule
    {

        private readonly IChromeManager chromeManager;
        private readonly IChromeRegistry chromeRegistry; 
        private const string DataPublisherViewId = "DataPublisherView";
        public PublishingModule(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_)
        {
            chromeManager = chromeManager_;
            chromeRegistry = chromeRegistry_; 
        }

        public void Initialize()
        {
             
            chromeRegistry.RegisterWindowFactory(
                DataPublisherViewId,
            CreateWindow);

            UrlListViewModel.UrlVisited += new EventHandler<UrlListViewModel.UrlVisitedEvent>(UrlsViewModel_UrlVisited);

        }

        private UrlViewModel registerPublisherUrl;
        void UrlsViewModel_UrlVisited(object sender, UrlListViewModel.UrlVisitedEvent e)
        {
            if (e.Model.Action == ActionType.RegisterPublisher)
            {
                registerPublisherUrl = e.Model;
                chromeManager.CreateWindow(DataPublisherViewId, 
                                    new InitialWindowParameters()
                                    {
                                       InitialLocation = MorganStanley.MSDotNet.MSGui.Core.InitialLocation.DockInNewTab,
                                       IsHeaderVisible =true,
                                       Singleton = true, 
                                    });
            }
       }
         

        private bool CreateWindow(IWindowViewContainer window_, XDocument state_)
        {
            StackPanel panel = new StackPanel();
            panel.Background = System.Windows.Media.Brushes.White; 
            UrlViewer viewer = new UrlViewer(); 
            viewer.DataContext =UrlViewModel.ForPulishRequest(new Trade() { Symbol = "MS", Price = 100, Quantity = 20, TradeType= TradeType.Sell }, DataFormat.Xml, registerPublisherUrl.SessionId);
            panel.Children.Add(viewer);
             
            viewer = new UrlViewer();
            viewer.DataContext = UrlViewModel.ForPulishRequest(new Trade() { Symbol = "MS", Price = 101, Quantity = 30, TradeType = TradeType.Buy }, DataFormat.Json, registerPublisherUrl.SessionId); 
            panel.Children.Add(viewer);

            window_.Content = panel;
            window_.Title = "Module Publisher";
            return true;
        }
    }
}
