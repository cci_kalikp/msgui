﻿using System.Windows.Controls;
using System;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.HTTPServer;

namespace HttpServerExample.ViewToView
{
    /// <summary>
    /// Interaction logic for SubscriberView.xaml
    /// </summary>
    //cw HTTPServer: 66
    //{
    public partial class SubscriberView : UserControl
    //}
    {

        private string subscriberId;
        private string viewId;
        private IChromeManager chromeManager;
        public SubscriberView(IChromeManager chromeManager_, string subscriberId_, string viewId_)
        {
             
            InitializeComponent();
            chromeManager = chromeManager_;
            viewId = viewId_;
            subscriberId = subscriberId_;
            this.DataContext = new UrlListViewModel(UrlListUsage.SubscriberView, subscriberId, viewId_);
            if (string.IsNullOrWhiteSpace(subscriberId))
            {
                UrlListViewModel.UrlVisited += new EventHandler<UrlListViewModel.UrlVisitedEvent>(UrlsViewModel_UrlVisited);
            }
            else
            {
                this.resultGrid.Visibility = System.Windows.Visibility.Visible;
                this.Main.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        void UrlsViewModel_UrlVisited(object sender, UrlListViewModel.UrlVisitedEvent e)
        {
            if (e.Model.Action == ActionType.RegisterV2VSubscriber)
            {
                this.subscriberId = e.Model.SessionId;
                this.DataContext = new UrlListViewModel(UrlListUsage.SubscriberView, subscriberId, viewId);
                this.resultGrid.Visibility = System.Windows.Visibility.Visible;
                this.Main.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        //cw HTTPServer: 67
        //{
        public void UpdateUI(Trade trade)
        {
            resultGrid.Items.Add(trade);
            var theWindow = chromeManager.Windows.FirstOrDefault(window_ => window_.Content == this); 
            if (theWindow != null)
            {
                theWindow.Activate();        
            }
        }
         //}
    }
}
