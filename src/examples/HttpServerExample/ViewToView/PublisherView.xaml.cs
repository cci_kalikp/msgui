﻿using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.HTTPServer;
using System;

namespace HttpServerExample.ViewToView
{    
    /// <summary>
    /// Interaction logic for PublisherView.xaml
    /// </summary>
    public partial class PublisherView : UserControl
    {
        private string publisherId;
        public PublisherView(string publisherId_, string viewid_)
        {   
            InitializeComponent(); 
            publisherId = publisherId_;
            this.DataContext = new UrlListViewModel(UrlListUsage.PublisherView, publisherId, viewid_);
            if (string.IsNullOrWhiteSpace(publisherId))
            {
                UrlListViewModel.UrlVisited += new EventHandler<UrlListViewModel.UrlVisitedEvent>(UrlsViewModel_UrlVisited);
            }
        }

        void UrlsViewModel_UrlVisited(object sender, UrlListViewModel.UrlVisitedEvent e)
        {
            if (e.Model.Action == ActionType.RegisterV2VPublisher && 
                !string.IsNullOrWhiteSpace(e.Model.SessionId))
            {
                this.publisherId = e.Model.SessionId;
                this.DataContext = new UrlListViewModel(UrlListUsage.PublisherView, publisherId);
            }
                
        } 
    } 
}
