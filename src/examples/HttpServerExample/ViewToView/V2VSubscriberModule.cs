﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
 using Microsoft.Practices.Composite.Modularity; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 
 

namespace HttpServerExample.ViewToView
{
    public class V2VSubscriberModule: IModule
    { 
        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;
        private const string ViewId = "Subscriber"; 
 
        public V2VSubscriberModule(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_)
        {
            chromeManager = chromeManager_;
            chromeRegistry = chromeRegistry_;
        } 

        public void Initialize()
        {
            chromeRegistry.RegisterWindowFactory(ViewId, CreateView);  

        }
         
        private bool CreateView(IWindowViewContainer viewContainer_, XDocument state_)
        {
            string subscriberId;
            UrlListViewModel.ParseState(state_, viewContainer_.Parameters, out subscriberId);
            SubscriberView viewer = new SubscriberView(chromeManager, subscriberId, viewContainer_.ID);
            viewContainer_.Content = viewer;
            viewContainer_.Title = "View Subscriber";
            return true; 
        } 
    }
}
