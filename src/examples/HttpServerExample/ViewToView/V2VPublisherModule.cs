﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;  

namespace HttpServerExample.ViewToView
{
    public class V2VPublisherModule: IModule
    {

        private readonly IChromeRegistry chromeRegistry; 
        private readonly IChromeManager chromeManager;
        private const string ViewId = "Publisher";  

        public V2VPublisherModule(IChromeRegistry chromeRegistry_,  IChromeManager chromeManager_)
        { 
            chromeRegistry = chromeRegistry_;
            chromeManager = chromeManager_; 
        }

        public void Initialize()
        {

            //cw HTTPServer: 41
            //{
            chromeRegistry.RegisterWindowFactory(ViewId, CreateView);  
            //}
        }


        //cw HTTPServer: 42
        //{
        private bool CreateView(IWindowViewContainer viewContainer_, XDocument state_)
        {
            string publisherId; 
            UrlListViewModel.ParseState(state_, viewContainer_.Parameters, out publisherId);
            PublisherView viewer = new PublisherView(publisherId, viewContainer_.ID);
            viewContainer_.Content = viewer;
            viewContainer_.Title = "View Publisher";
            return true;
        } 
        //}
    }
}
