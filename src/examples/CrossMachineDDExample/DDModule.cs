﻿using System;
using System.Reactive.Linq;
using System.Windows.Controls;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MSDesktop.CrossMachineApplication;
using MSDesktop.CrossMachineApplication.Messages;

namespace MorganStanley.MSDotNet.MSGui.Examples.CrossMachineDDExample
{
    public class DDModule : IModule
    {
        private const string SimpleWindowID = "SimpleWindowID";
        private const string SimpleWindowButtonID = "SimpleWindowButtonID";
        private const string TransferablePublisherWindowID = "TransferablePublisherWindowID";
        private const string TransferablePublisherWindowButtonID = "TransferablePublisherWindowButtonID";
        private const string TransferableSubscriberWindowID = "TransferableSubscriberWindowID";
        private const string TransferableSubscriberWindowButtonID = "TransferableSubscriberWindowButtonID";

        public DDModule(IChromeRegistry chromeRegistry, IChromeManager chromeManager, IEventRegistrator eventRegistrator, IWindowTransferManager transferManager)
        {
            m_chromeManager = chromeManager;
            m_chromeRegistry = chromeRegistry;
            m_eventRegistrator = eventRegistrator;
            m_transferManager = transferManager;
        }

        public void Initialize()
        {
            m_chromeRegistry.RegisterWindowFactory(SimpleWindowID, CreateSimpleWindow, DehydrateSimpleWindow);
            m_chromeManager.PlaceWidget(SimpleWindowButtonID, "DD/Views", new InitialShowWindowButtonParameters
            {
                WindowFactoryID = SimpleWindowID,
                Text = "Transferable",
                InitialParameters = new InitialTransferableWindowParameters()
            });
            m_chromeRegistry.RegisterWindowFactory(TransferablePublisherWindowID, CreateTransferablePublisherWindow, DehydrateTransferablePublisherWindow);
            m_chromeManager.PlaceWidget(TransferablePublisherWindowButtonID, "DD/Views", new InitialShowWindowButtonParameters
            {
                WindowFactoryID = TransferablePublisherWindowID,
                Text = "Transferable publisher",
                InitialParameters = new InitialTransferableWindowParameters()
            });
            m_chromeRegistry.RegisterWindowFactory(TransferableSubscriberWindowID, CreateTransferableSubscriberWindow, DehydrateTransferableSubscriberWindow);
            m_chromeManager.PlaceWidget(TransferableSubscriberWindowButtonID, "DD/Views", new InitialShowWindowButtonParameters
            {
                WindowFactoryID = TransferableSubscriberWindowID,
                Text = "Transferable subscriber",
                InitialParameters = new InitialTransferableWindowParameters()
            });
        }

        private static bool CreateSimpleWindow(IWindowViewContainer emptyViewContainer, XDocument state)
        {
            TextBox textBox = emptyViewContainer.Content as TextBox;
            if (textBox == null)
            {
                textBox = new TextBox();
                textBox.Width = 300;
                emptyViewContainer.Content = textBox;
            }
            if (state != null)
            {
                textBox.Text = state.Root.Value;
            }
            emptyViewContainer.Title = "Transferable window";
            AddHostInformation(emptyViewContainer);
            return true;
        }

        public XDocument DehydrateSimpleWindow(IWindowViewContainer windowViewContainer)
        {
            TextBox textBox = windowViewContainer.Content as TextBox;
            if (textBox != null)
            {
                var document = new XDocument();
                document.Add(new XElement("TextInput", textBox.Text));
                return document;
            }
            return null;
        }

        public bool CreateTransferablePublisherWindow(IWindowViewContainer emptyViewContainter, XDocument state)
        {
            TextBox userInputTextBox = new TextBox();

            Button sendButton = new Button() { Content = "Send" };

            var publisher = m_eventRegistrator.RegisterPublisher<SampleMessage>(emptyViewContainter);
            sendButton.Click += (s, e) => publisher.Publish(new SampleMessage { UserInput = userInputTextBox.Text });

            StackPanel panel = new StackPanel();
            panel.Orientation = Orientation.Vertical;
            panel.Children.Add(userInputTextBox);
            panel.Children.Add(sendButton);

            emptyViewContainter.Title = "Transferable publisher";
            emptyViewContainter.Content = panel;

            AddHostInformation(emptyViewContainter);

            return true;
        }

        public bool CreateTransferableSubscriberWindow(IWindowViewContainer viewContainer, XDocument state)
        {
            var listBox = viewContainer.Content as ListBox;
            if (listBox == null)
            {
                listBox = new ListBox();
                var subscriber = m_eventRegistrator.RegisterSubscriber<SampleMessage>(viewContainer);
                subscriber.GetObservable().ObserveOnDispatcher().Subscribe(m =>
                    {
                        listBox.Items.Insert(0, m.UserInput);
                    });
            }
            if (state != null)
            {
                listBox.Items.Clear();
                foreach (var itemEl in state.Root.Elements("Item"))
                {
                    listBox.Items.Add(itemEl.Value);
                }
            }

            viewContainer.Title = "Transferable subscriber";
            viewContainer.Content = listBox;

            AddHostInformation(viewContainer);

            return true;
        }

        public XDocument DehydrateTransferableSubscriberWindow(IWindowViewContainer windowViewContainer)
        {
            XDocument state = new XDocument();
            var listBox = windowViewContainer.Content as ListBox;
            if (listBox != null)
            {
                XElement listBoxEl = new XElement("ListBox");
                foreach (var item in listBox.Items)
                {
                    listBoxEl.Add(new XElement("Item", item.ToString()));
                }
                state.Add(listBoxEl);
            }
            return state;
        }

        public XDocument DehydrateTransferablePublisherWindow(IWindowViewContainer windowViewContainer)
        {
            XDocument state = new XDocument();
            return state;
        }

        static void AddHostInformation(IWindowViewContainer viewContainer)
        {
            if (viewContainer.Parameters is InitialTransferableWindowParameters)
            {
                var hostInfo = new TextBlock
                {
                    Text = String.Format("[{0}]", (viewContainer.Parameters as InitialTransferableWindowParameters).OriginHost),
                    FontSize = 12,
                };
                viewContainer.HeaderItems.Clear();
                viewContainer.HeaderItems.Add(hostInfo);
            }
        }

        IChromeRegistry m_chromeRegistry;
        IChromeManager m_chromeManager;
        IEventRegistrator m_eventRegistrator;
        private readonly IWindowTransferManager m_transferManager;
    }

    public class SampleMessage : ITransferableV2VMessage
    {
        public string UserInput { get; set; }

        XDocument ITransferableV2VMessage.SaveState()
        {
            XDocument doc = new XDocument();
            XElement el = new XElement("UserInput", UserInput);
            doc.Add(el);
            return doc;
        }

        void ITransferableV2VMessage.LoadState(XDocument state)
        {
            UserInput = state.Root.Value;
        }
    }
}
