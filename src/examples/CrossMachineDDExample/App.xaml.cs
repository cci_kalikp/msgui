﻿using System.Windows;
using MorganStanley.MSDesktop.IPC;
using MorganStanley.MSDotNet.MSGui.Examples.CrossMachineDDExample;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.MSGui.Themes;
using MSDesktop.CrossMachineApplication.Extensions;

namespace CrossMachineDDExample
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var boot = new Framework();
            boot.AddSimpleTheme();
            boot.AddModule<V2VConfigModule>();

            boot.EnableCrossMachineDragAndDrop(new IPCOptions
            {
                Host = "vmias6680",
                Port = "64291",
                Kerberos = true,
                GlobalTargeting = new IPCTargeting(new IPCTarget { Key = "application", Value = "CrossMachineDDExample" })
            });
            //boot.EnableCrossApplicationDragAndDrop();

            boot.AddModule<DDModule>();

            boot.Start();
        }
    }
}
