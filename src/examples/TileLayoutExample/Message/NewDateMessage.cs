﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TileLayoutExample.Message
{
    [Serializable]
    public class NewDateMessage
    {
        public DateTime Date;
    }
}
