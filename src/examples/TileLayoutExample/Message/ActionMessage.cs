﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TileLayoutExample.Message
{
    [Serializable]
    public class ActionMessage
    {
        public string Action;
        public DateTime Time;
        public string Application;
    }
}
