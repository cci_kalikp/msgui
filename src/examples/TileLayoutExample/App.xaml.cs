﻿//cw Workspace_Support_Overview: 10 
/**
 * ## Description
 * Top level floating windows can now be treated as standalone workspaces, and managed by the framework, similar to managing the layout profiles.*/
/** Workspace is supported both for the default dock layout and [tile layout][cw_ref Tile_Layout] */
/** ### Workspace in dock layout */
/**![Dock Layout](images/Workspace/Workspace_DockLayout.png)*/
/** ### Workspace in tile layout */
/**![Tile Layout](images/Workspace/Workspace_TileLayout.png)*/
/** ## Workspace Management */
/** ### Create/Save/Save As/Delete/Rename/List */
/** Create: Workspaces can be created using a seed view or as an empty window, and all windows registered for the same workspace category would be available in the "Edit" drop down on the workspace container */
/** Save/SaveAs/Delete: Workspaces can be managed through the "Edit" drop down button on the workspace container as well */
/** Rename: Workspaces can be renamed and deleted through the "Workspace Manager" option page */
/** List: Saved workspaces can be accessed through the ISubLayoutManager interface */
/** ## "Workspace Manager" Options Page */
/**![Tile Layout](images/Workspace/Workspace_Options.png)*/


//cw Workspace_Support_General: 10
/** 
 * ## Description
 * Workspace support is provided in MSDesktop.Workspaces.dll
 */

//cw Workspace_Support_Save_On_Exit: 10
/**
 * # Description
 * ### Workspace close strategy can be specified programmatically using one of the following enumerations
 * #### Default
 * If the workspace to be closed is changed, would be saved according to the options of Workspace Manager
 * #### AlwaysAsk
 * The workspace to be closed would always be saved according to the options of Workspace Manager
 * #### NeverAsk
 * Would always close without saving
 * #### UseCallback
 * Would use the callback specified to decide if the window to be closed need to save according to the options of Workspace Manager
 * #### NeverAskButPromptForExit
 * Would only be saved according to the options of Workspace Manager when the application exits and the workspace is changed
 * 
 * ### If workspace is  to be saved according above, "SaveOnExit" and "ShowExitDialog" of the workspace configuration specified by code or through ["Workspace Manager" options page][cw_ref Workspace_Support_Overview] would be used to decide if it really needs to be saved
 * #### SaveOnExit
 * If set to false, never save it; If set to true check the value of "ShowExitDialog"
 * #### ShowExitDialog
 * If set to true, the save workspace dialog would always popup; If set to false, the workspace would always be saved. 
 */
/** If user choose "Do not ask next time" at the same time. If user chooses "Save and Quit", the "ShowExitDialog" would be set to false automatically for later usage; If user chooses "Quit", the "SaveOnExit" would be set to false automatically. */

//cw Workspace_Support_Save_along_with_Profile: 10
/**
 * ## Description
 * The behavior of saving the open workspaces when saving the profile can be enabled programmatically or through ["Workspace Manager" options page][cw_ref Workspace_Support_Overview]
 */
 /** Also, if this option is enabled, when switching layouts or exit application, no more question about whether to save the open workspace would appear, since the decision about the whether to save profile would apply to the open workspaces at the same time*/

//cw Workspace_Support_General: 20 cw Workspace_Support_Save_On_Exit: 20 cw Workspace_Support_Save_along_with_Profile: 20
/** 
 * ## Assembly
 * 
 * You need to add a reference to the MSDesktop.Workspaces assembly.
 */
/** 
 * ## Namespace 
 */

using System;
using System.IO;
using System.Reflection;
using System.Windows;
//cw Workspace_Support_General: 21 cw Workspace_Support_Save_On_Exit: 21 cw Workspace_Support_Save_along_with_Profile: 21
//{   
using System.Windows.Media;
using MSDesktop.Workspaces;
//}

//cw Workspace_Support_Default_Layout: 10
/**
 * ## Description
 * A default layout with default workspaces created opened at startup can be specified using the startup persistence storage
 */
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
//cw Workspace_Support_Default_Layout: 20
/** 
 * ## Namespace 
 */
//{
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
//}
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.MSGui.Themes;
using MorganStanley.MSDotNet.MSGui.Themes.MSDesktop2014;
using TileLayoutExample.App1;
using TileLayoutExample.App2;
using TileLayoutExample.App3;
using TileLayoutExample.App4;

namespace TileLayoutExample
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{

        //cw Workspace_Support_General: 31
        /** 
         * ## Code
         * ### Enable workspace support
         * When enabling it, developer should provide the default title(or titles if you want to give different default title for different workspace category) of the workspace created and could provide default layout mode and default workspace category for workspaces created. And usually we put the shell into "LauncherBarAndFloatingWindows" mode in this case.
         */
        //cw Workspace_Support_Save_On_Exit: 31
        /** 
         * ## Code 
         */
        //cw Workspace_Support_General: 32  cw Workspace_Support_Default_Layout: 32 cw Workspace_Support_Save_On_Exit: 32
        //{
        protected override void OnStartup(StartupEventArgs e_)
		{
			base.OnStartup(e_);
			var boot = new Framework();
        //}  


            boot.SetViewHeaderHeight(21, 25);
            boot.SetConfigurationDependentShellMode(ShellMode.LauncherBarAndWindow, true);
            
            //only allow 2 shell modes and use harmonia
            //boot.SetConfigurationDependentShellMode(ShellMode.LauncherBarAndWindow, true, true,
            //    new ShellMode[] { ShellMode.LauncherBarAndFloatingWindows, ShellMode.RibbonAndFloatingWindows });
 
            var customTheme = new CustomBlackTheme();
            boot.AddConfigurationDependentTheme(customTheme.Name, true, new Theme[] { customTheme }, true, Colors.Orange,
                SimpleTheme.DefaultThemeName, BlackTheme.DefaultThemeName, BlueTheme.DefaultThemeName, WhiteTheme.DefaultThemeName,
                ModernTheme.DarkThemeName, ModernTheme.LightThemeName); 
             
            boot.EnableNonOwnedWindows();
       //cw Workspace_Support_General: 33
       //{  
            //comment out this line to run the example in normal mode
            boot.EnableWorkspaceSupport(LayoutEngine.Tile, WorkspacesManagementModule.GlobalWorkspaceCategory,"New Workspace");  
       //}
             
            boot.EnableTypeaheadDropdownOptionsSearch(); 

            //cw Workspace_Support_Save_On_Exit: 32
            /** ### Specify the WorkspaceCloseStrategy */
            //{
            boot.SetWorkspaceCloseStrategy(LayoutLoadStrategy.NeverAsk);
            //} 

            boot.SetLayoutLoadStrategy(LayoutLoadStrategy.NeverAskButPromptForExit);
            boot.HideOrLockShellElements(HideLockUIGranularity.HideNewTabButton | HideLockUIGranularity.DisableTabwellContextMenu | HideLockUIGranularity.HideStatusBar);

            if (1 == 2)
            {
                //cw Workspace_Support_Save_along_with_Profile: 30
                /** ## Code */
                //{
                boot.SaveWorkspaceWhenSavingProfile();
                //}
            } 

            //boot.EnableFlatMode();
            boot.AddModule<WorkspacesManagementModule>();
            boot.AddModule<App1Module>();
            boot.AddModule<App2Module>();
            boot.AddModule<App3Module>();
            boot.AddModule<App4Module>();
            boot.AddModule<App5Module>();
            boot.AddModule<V2VConfigModule>();


            //cw Workspace_Support_Default_Layout: 31 
            /** ## Steps */
            /**  A. Developer could run the application, create workspaces and save them; then save the profile with the workspaces adjusted appropriately as "Default" as well. */
            /**  B. Create a folder, for example named as "Configuration" under the project */
            /**  C. Then copy the workspaces files and the default layout from the %appdata%\Morgan Stanley\[ApplicationName] to this folder and make them "copy always"*/
            /**    C1. The saved workspaces are under Config folder. Copy the "Workspace.config" and the "MSDesktopWorkspace_*.config" */
            /**    C2. The saved default layout is directly under the folder and the named as "Default.state" (or if using multi file storage, the folder "Default_state" */
            /**  D. Call SetupPersistenceStorage extension method to point to the "Configuration" folder created in step 2 */
            /**  E. Call AcquireFocusAfterStartup extension method to make sure that the initially created workspaces are focused after application starts */
            /** ## Code */

            //cw Workspace_Support_Default_Layout: 33  
            //{
            boot.SetupPersistenceStorage(new FilePersistenceStorage("TileLayoutExample", PathUtilities.GetAbsolutePath("Configuration")));
            boot.AcquireFocusAfterStartup();
            //} 
              
            //boot.EnableWorkspaceMixingMode();
            //boot.SetLayoutLoadStrategy(LayoutLoadStrategy.NeverAsk);

            //cw Workspace_Support_General: 34 cw Workspace_Support_Default_Layout: 35 cw Workspace_Support_Save_On_Exit: 33
       //{
            boot.Start();
		}
        //}
	}
}
