﻿using System; 
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Xml.Linq;
using MSDesktop.Workspaces;
using MSDesktop.Workspaces.Interfaces;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Controls.Interop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using TileLayoutExample.App5;
using TileLayoutExample.Message;
using MessageBox = System.Windows.MessageBox;

namespace TileLayoutExample.App4
{
    public class App5Module : IModule
    {
        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;
        private readonly IUnityContainer container;
        private readonly ICommunicator communicator; 
        private const string App5 = "Application E"; 
        private readonly ImageSource App5Icon = new BitmapImage(
            new Uri(@"pack://application:,,,/TileLayoutExample;component/Resources/app5.png"));  
        public App5Module(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, 
            ICommunicator communicator_,  IUnityContainer container_)
        {
            this.chromeRegistry = chromeRegistry_;
            this.chromeManager = chromeManager_;
            this.communicator = communicator_; 
            this.container = container_;
        }

        public void Initialize()
        { 
            if (container.IsRegistered<IWorkspaceManager>())
            {
                chromeRegistry.RegisterWindowFactory(App5).SetInitHandler(CreateView).AddToWorkspace(image_: App5Icon);

            }
            else
            {
                chromeRegistry.RegisterWindowFactory(App5, CreateView);
                chromeManager.GetDefaultContainer()["Normal"].AddWidget(Guid.NewGuid().ToString(), new InitialShowWindowButtonParameters()
                {
                    WindowFactoryID = App5,
                    Text = "Application E",
                    Image = App5Icon,
                    WindowLayout = LayoutEngine.Tile
                }); 
            }

        }

        private bool CreateView(IWindowViewContainer emptyViewContainer_, XDocument state_)
        {
            emptyViewContainer_.Title = "Application E";
            var host = new SmoothSizingWindowsFormsHost
                {
                    Margin = new Thickness(0),
                    FontSize = 11.33,
                    FontFamily = new FontFamily("Microsoft Sans Serif"),
                    Child = new QueryControl(){Anchor = AnchorStyles.Bottom| AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top, Dock = DockStyle.Fill},
                    Background = Brushes.Transparent
                }; 
            host.Width = host.Child.Width;
            host.Height = host.Child.Height;
            emptyViewContainer_.Content = host;
            MyInitialTileItemParameters parameters = emptyViewContainer_.Parameters as MyInitialTileItemParameters;
            if (parameters != null && !string.IsNullOrEmpty(parameters.Message))
            {
                MessageBox.Show(parameters.Message, emptyViewContainer_.Title);
            }
            return true;
        }

 
         
    }
}
