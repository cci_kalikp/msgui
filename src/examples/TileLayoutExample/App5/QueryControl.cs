﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data; 
using MorganStanley.MSDotNet.MSGui.Controls.Interop; 

namespace TileLayoutExample.App5
{
    public partial class QueryControl : SmoothSizingHostedUserControl
    {
        public QueryControl()
        {
            InitializeComponent();
            BindGrid(); 
        }
 
        private void btnSearch_Click(object sender, EventArgs e)
        {
            dt.Rows.Clear();
            int count = new Random().Next(3, 20);
            var random = new Random();
            for (int i = 0; i < count; i++)
            {
                var row = dt.NewRow();
                row.ItemArray = new object[]
                    {"Data" + i, DateTime.Today.AddDays(random.Next(-1000, 1000)), random.Next(-100, 100)};
                dt.Rows.Add(row);
            }
            dt.AcceptChanges();

        }

        private DataTable dt;
        private void BindGrid()
        {
            dt = new DataTable("Data");
            dt.Columns.Add("Column A");
            dt.Columns.Add("Column B", typeof(DateTime));
            dt.Columns.Add("Column C", typeof(int));
            this.dataGridView1.DataSource = dt;
        }

 
    }
}
