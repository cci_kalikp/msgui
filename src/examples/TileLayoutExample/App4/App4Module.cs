﻿using System; 
using System.Windows;
using System.Windows.Controls; 
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using MSDesktop.Workspaces;
using MSDesktop.Workspaces.Interfaces;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging; 
using TileLayoutExample.Message;

namespace TileLayoutExample.App4
{
    public class App4Module : IModule
    {
        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;
        private readonly IUnityContainer container;
        private readonly ICommunicator communicator; 
        private const string App4 = "Application D";
        private IModuleSubscriber<ActionMessage> actionSubscriber;
        private readonly ImageSource App4Icon = new BitmapImage(
            new Uri(@"pack://application:,,,/TileLayoutExample;component/Resources/app4.png"));  
        public App4Module(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, 
            ICommunicator communicator_,  IUnityContainer container_)
        {
            this.chromeRegistry = chromeRegistry_;
            this.chromeManager = chromeManager_;
            this.communicator = communicator_; 
            this.container = container_;
        }

        public void Initialize()
        {
            actionSubscriber = communicator.GetSubscriber<ActionMessage>();
            if (container.IsRegistered<IWorkspaceManager>())
            {
                chromeRegistry.RegisterWindowFactory(App4).SetInitHandler(CreateView).AddToWorkspace(image_: App4Icon);

            }
            else
            {
                chromeRegistry.RegisterWindowFactory(App4, CreateView);
                chromeManager.GetDefaultContainer()["Normal"].AddWidget(Guid.NewGuid().ToString(), new InitialShowWindowButtonParameters()
                {
                    WindowFactoryID = App4,
                    Text = "Application D",
                    Image = App4Icon,
                    WindowLayout = LayoutEngine.Tile
                }); 
            }

        }

        private bool CreateView(IWindowViewContainer emptyViewContainer_, XDocument state_)
        {
            emptyViewContainer_.Title = "Application D";
            emptyViewContainer_.Parameters.IsHeaderVisible = true;
            TextBlock text = new TextBlock();
            emptyViewContainer_.Content = text;
            text.HorizontalAlignment = HorizontalAlignment.Stretch;
            text.VerticalAlignment = VerticalAlignment.Stretch;
            emptyViewContainer_.Parameters.SizingMethod = SizingMethod.Custom;
            emptyViewContainer_.Parameters.Width = 300;
            emptyViewContainer_.Parameters.Height = 600;

            actionSubscriber.GetObservable().Subscribe(m_ => text.Dispatcher.Invoke(new Action(() =>
                {
                    text.Text = text.Text + Environment.NewLine +
                                m_.Time.ToLongTimeString() + ":" + m_.Action + " upon " + m_.Application;
                })));
            return true;
        }
         
         
    }
}
