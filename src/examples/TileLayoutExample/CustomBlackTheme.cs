﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Themes;

namespace TileLayoutExample
{
    class CustomBlackTheme:BlackTheme
    {
        public CustomBlackTheme() 
        { 
            Name = "Default";  
        }

        protected override void InitializeCore(ShellMode shellMode_)
        {
 
            base.InitializeCore(shellMode_);

            var customResource = new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/TileLayoutExample;component/Themes/ApplicationStyleBlack.xaml")
                };
            if (shellMode_ == ShellMode.LauncherBarAndFloatingWindows)
            {
                customResource.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/TileLayoutExample;component/Themes/LauncherBarStyles.xaml")
                });
            }
            else if (shellMode_ == ShellMode.LauncherBarAndWindow)
            {
                customResource.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("pack://application:,,,/TileLayoutExample;component/Themes/WindowWithBarStyles.xaml")
                });
            } 
            this.MergedDictionaries.Add(customResource);
        }
    }
}
