﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace TileLayoutExample
{
    public static class ChromeManagerExtensions
    {
        public static IWidgetViewContainer GetDefaultContainer(this IChromeManager manager_)
        {
            if (manager_ is RibbonChromeManager)
            {
                return manager_[ChromeArea.Ribbon]["Tools"];
            }
            else
            {
                return manager_[ChromeArea.LauncherBar];
            }
        }
    }
}
