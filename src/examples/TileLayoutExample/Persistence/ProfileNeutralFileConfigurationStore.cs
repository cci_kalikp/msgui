﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using MorganStanley.IED.Concord.Configuration;

namespace TileLayoutExample.Persistence
{
    public class ProfileNeutralFileConfigurationStore : FileConfigurationStoreEx
    {
        private string userBaseDir;
 
        public override void Initialize(string settings_, System.Xml.XmlNode config_)
        {
            base.Initialize(settings_, config_);
            var node = config_.SelectSingleNode("UserBaseDir", null);
            if (node != null)
            {
                string sData = ParseDirectoryString(node.InnerXml);
                if (!string.IsNullOrEmpty(sData))
                {
                    userBaseDir = sData;
                }
            }
            if (string.IsNullOrEmpty(userBaseDir))
            {
                userBaseDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            }
        }
        protected override string GetUserDirectory(string username_, string app_, string reg_, string env_, string role_)
        {
            string dir = String.Format(@"{0}\Morgan Stanley\{1}\Configuration", userBaseDir, app_); 
            if (!System.IO.Directory.Exists(dir))
            {
                System.IO.Directory.CreateDirectory(dir);
            }

            return dir;
        }
    }
}
