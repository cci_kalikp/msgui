﻿//cw Tile_Layout: 10

/**
 * ## Description
 * Other than the default dockable window layout, MSDesktop also supports creating windows as controls layouted as tile item in tile container as following.
 */
/**![Tile Layout](images/Chrome/TileLayout.png)*/
/** ## Modes */
/** ### Edit mode: 
 * Child windows can be added to it and relocated, resized and removed(through the close button on the upper right of the child window). */
/** The zooming of the tile container can be also changed. */
/** ### None Edit mode:
 * Child windows if allow maximizing can be maximized or restored by clicking the maximize button on the upper right of the child window). */
/** Gripping the border between child windows can adjust the size of windows in both side. And resize the tile container would adjust the child windows close to container edge. */

//cw Tile_Layout: 20
/**
 * ## Namespace
 */


using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using MSDesktop.Workspaces.Interfaces;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
//cw Tile_Layout: 21
//{
using MorganStanley.MSDotNet.MSGui.Controls;
using MorganStanley.MSDotNet.MSGui.Controls.BarMenu;
using MorganStanley.MSDotNet.MSGui.Controls.Controls;
using MorganStanley.MSDotNet.MSGui.Controls.SplitButton;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
//}
using MSDesktop.Workspaces;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.LauncherBar;
using TileLayoutExample.App5;
using TileLayoutExample.WorkspacesManagement;
using Control = System.Windows.Controls.Control;
using Extensions = MSDesktop.Workspaces.Extensions;

namespace TileLayoutExample
{
    public class WorkspacesManagementModule:IModule
    { 
        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;
        private IWorkspaceManager workspaceManager;
        private ISubLayoutManager subLayoutManager;
        internal const string GlobalWorkspaceCategory = "Workspace";
        private readonly IUnityContainer container;
        private readonly ImageSource workspaceIcon = new BitmapImage(
            new Uri(@"pack://application:,,,/TileLayoutExample;component/Resources/tile.png")); 
        public WorkspacesManagementModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, IUnityContainer container_)
        {
            this.chromeRegistry = chromeRegistry_;
            this.chromeManager = chromeManager_;
            this.container = container_;
        }
         
        public void Initialize()
        {
            chromeRegistry.RegisterWindowFactoryMapping<InitialTileItemParameters, MyInitialTileItemParameters>();
            
            if (container.IsRegistered<IWorkspaceManager>())
            {
                //hide the "Workspace Manager" option
                //container.Resolve<IApplicationOptions>().RemoveOptionPage("MSDesktop", "Workspace Manager");

                //cw Workspace_Support_General: 63
                //{
                this.workspaceManager = container.Resolve<IWorkspaceManager>();
                //}

                //cw Workspace_Support_General: 40
                /** 
                 * ### List Workspaces
                 * A dropdown button can be created to create or access existing workspaces, utilizing the ISubLayoutManager.SubLayoutChanged event and ISubLayoutManager.LoadSubLayout method
                 */
                //{
                this.subLayoutManager = container.Resolve<ISubLayoutManager>();
                //}

                if (1 == 2)
                {
                    //cw Workspace_Support_Save_On_Exit: 40
                    /** ### Force the "SaveOnExit" and "ShowExitDialog" using code*/
                    //{
                    subLayoutManager.WriteSubLayoutConfig(Extensions.SaveOnExitOption, true.ToString());
                    subLayoutManager.WriteSubLayoutConfig(Extensions.ShowExitDialogOption, false.ToString());
                    //}
                }

                //cw Workspace_Support_General: 60
                /** 
                 * ### Default layout for tile layout workspaces
                 * Usually for tile layout workspaces, we need to override the default dropdown list for available tile items provided in "Edit" drop down on the container window. 
                 */
                /** This can be implemented by providing an instance InitialDropdownButtonParameters for AppStoreButtonParameters of the default InitialTileParameters */
                /** This parameter should provide an image for the dropdown button, and implement the Opening method for populating the drop down */
                //{
                InitialTileParameters.Defaults = new InitialTileParameters()
                {
                    Width = 1200,
                    Height = 900, 
                    Left = 5,
                    Top = 100,
                    WorkspaceCategory = GlobalWorkspaceCategory,
                    Icon = Properties.Resources.tile,
                    //AllowZoom = true,
                    AppStoreButtonParameters = new InitialDropdownButtonParameters()
                    {
                        Image = new BitmapImage(
        new Uri(@"pack://application:,,,/TileLayoutExample;component/Resources/app_store.png")),
                        Opening = Opening
                    }
                };
                //}

                //cw Workspace_Support_General: 41
                //{
                IWidgetViewContainer dropdown = null;
                //chromeManager[ChromeArea.Ribbon]["Tools"] or chromeManager[ChromeArea.LauncherBar] as the result of chromeManager.GetDefaultContainer() depending on the shell mode
                dropdown = chromeManager.GetDefaultContainer()["Workspaces"].AddWidget("WorkspaceDropdownId",
                                                                                       new InitialDropdownButtonParameters
                                                                                           ()
                                                                                           {
                                                                                               Text = "Workspaces",
                                                                                               Image = workspaceIcon,
                                                                                               Enabled = true
                                                                                           });  
                PopulateWorkspaces(dropdown); 
                //}

                var application = container.Resolve<IApplication>();
                string themeName = ReflHelper.PropertyGet(application, "CurrentThemeName") as string;
                if (themeName == "Default")
                {
                    if (dropdown.Content != null)
                    {
                        BarMenuTool menuTool = dropdown.Content as BarMenuTool;
                        if (menuTool != null)
                        {
                            menuTool.SetResourceReference(Control.ForegroundProperty, MSGuiColors.LaunchBarTitleBrushKey);
                        }
                    }
                    else
                    {
                        dropdown.ContentReady += (sender_, args_) =>
                        {
                            var dropdown2 = (IWidgetViewContainer)sender_;
                            BarMenuTool menuTool = dropdown2.Content as BarMenuTool;
                            if (menuTool != null)
                            {
                                menuTool.SetResourceReference(Control.ForegroundProperty, MSGuiColors.LaunchBarTitleBrushKey);
                            }
                        };
                    }
                }
               

                dropdown = chromeManager.GetDefaultContainer()["Workspaces"].AddWidget("WorkspaceSplitId", new InitialSplitButtonParameters()
                {
                    Text = "Create new workspace",
                    Image = workspaceIcon, 
                    Click = (sender_, args_) => chromeManager.CreateEmptyWorkspace(1200, 900)
                });
                if (dropdown.Content != null)
                {
                    SplitButton menuTool = dropdown.Content as SplitButton;
                    if (menuTool != null)
                    {
                        menuTool.Foreground = Brushes.White;
                    }
                }
                else
                {
                    dropdown.ContentReady += (sender_, args_) =>
                    {
                        var dropdown2 = (IWidgetViewContainer)sender_;
                        SplitButton menuTool = dropdown2.Content as SplitButton;
                        if (menuTool != null)
                        {
                            menuTool.Foreground = Brushes.White;
                        }
                    };
                }
                PopulateWorkspaces2(dropdown);

                chromeRegistry.ChainWindowFactoryMapping<InitialTileParameters>((container_, state_, chain_) =>
                    {

                        string buttonID = "myButton" + Guid.NewGuid();
                        System.Windows.Controls.Button flashAllButton = null; 
                        chromeRegistry.RegisterWidgetFactory(buttonID, (container2_, state2_) =>
                            {
                                flashAllButton = new System.Windows.Controls.Button() {Content = "Flash All", Tag = container_};
                                flashAllButton.Click += flashAllButton_Click;
                                container2_.Content = flashAllButton;
                                return true;
                            }); 
                        container_.Header.AddWidget(buttonID, new InitialWidgetParameters());
                        //unhook the global flashAllButton_Click handler to allow releasing of the tile view container when closed
                        EventHandler<WindowEventArgs> closedEventHandler = null;
                        container_.Closed += closedEventHandler = (sender_, args_) =>
                            {
                               if (flashAllButton != null)
                               {
                                   flashAllButton.Click -= flashAllButton_Click;
                               }
                                container_.Closed -= closedEventHandler; 
                                container_.ClosedQuietly -= closedEventHandler;
                            };
                        container_.ClosedQuietly += closedEventHandler;

                        //can move following code to the start of the method, so that the custom button is added after the system provided "edit" button
                        var initHandler = chain_.Pop();
                        var initResult = initHandler(container_, state_, chain_);
                        if (!initResult)
                            return false;

                        return true;
                    });
            } 
            else
            {
                //cw Tile_Layout: 40
                /**
                 * ## Code
                 */
                /** ### Default tile container initial parameters */
                /** Set the default size, location, icon for tile container automatically created when creating a child tile window */
                //{
                InitialTileParameters.Defaults = new InitialTileParameters()
                {
                    Width = 1200,
                    Height = 900,
                    Left = 5,
                    Top = 100,
                    Icon = Properties.Resources.tile, 
                    MarginSize = 4d, //margins between tile items
                    UnitSize = 12d, //resize unit
                };
                //}


                //cw Tile_Layout: 40 
                /** ### Default tile item initial parameters */
                /** Take the initial location, the custom close and maximize icon, etc. */
                //{
                InitialTileItemParameters.Defaults = new InitialTileItemParameters()
                    {
                        CloseIconSource = new Uri(@"pack://application:,,,/TileLayoutExample;component/Resources/close.png"),
                        MaximizeIconSource = new Uri(@"pack://application:,,,/TileLayoutExample;component/Resources/maximize.png"),
                        Left = 10,
                        Top = 10 
                    };
                //}
            }

            chromeRegistry.RegisterWindowFactory("PopupWindow", InitHandler);
            chromeManager.LayoutLoaded += new EventHandler(chromeManager_LayoutLoaded);

        }

        void flashAllButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.Button button = (System.Windows.Controls.Button) sender;
            ITileViewContainer tileViewContainer = (ITileViewContainer)button.Tag;
            foreach (var windowViewContainer in tileViewContainer.Children)
            {
                windowViewContainer.Flash();
            }
        }

        void chromeManager_LayoutLoaded(object sender, EventArgs e)
        {  
            chromeManager.CreateWindow("PopupWindow", new InitialWindowParameters(){ InitialLocation = InitialLocation.Custom | InitialLocation.FloatingOnly,
                Singleton = true,
                Transient = true,
                InitialLocationCallback = 
                (width_, height_) => 
                    new Rect(Screen.PrimaryScreen.WorkingArea.Right - width_ , Screen.PrimaryScreen.WorkingArea.Bottom - height_, width_, height_)
            });
        }

        private bool InitHandler(IWindowViewContainer emptyViewContainer_, XDocument state_)
        {
            emptyViewContainer_.Title = "Drag items to workspace";
            var host = new WindowsFormsHost
            {
                Margin = new Thickness(0),
                FontSize = 11.33,
                FontFamily = new FontFamily("Microsoft Sans Serif"),
                Child = new FloatingDragControl(),
                Background = Brushes.Transparent
            };
            host.Width = host.Child.Width;
            host.Height = host.Child.Height;
            emptyViewContainer_.Content = host;
            return true; 
        }


        //cw Workspace_Support_General: 42
        //{
        private void PopulateWorkspaces(IWidgetViewContainer dropdown_)
        {
            dropdown_.Clear();
            PopulateNewWorkspace(dropdown_);
            foreach (var sublayout in subLayoutManager.GetSubLayouts())
            {
                var subLayoutCopied = sublayout;
                dropdown_.AddWidget(
                    new InitialButtonParameters
                    {
                        Text = sublayout.LayoutName,
                        Image = workspaceIcon,
                        Click = (sender_, args_) => workspaceManager.LoadWorkspace(subLayoutCopied.LayoutName)
                    });
            }
            EventHandler<SubLayoutChangedEventArgs> handler = null;
            handler = (sender_, args_) =>
            {
                subLayoutManager.SubLayoutChanged -= handler;
                PopulateWorkspaces(dropdown_);
            };
            subLayoutManager.SubLayoutChanged += handler;
        }
        //}

        private void PopulateWorkspaces2(IWidgetViewContainer dropdown_)
        {
            dropdown_.Clear();
            bool hasWorkspace = false;
            foreach (var sublayout in subLayoutManager.GetSubLayouts())
            {
                var subLayoutCopied = sublayout;
                dropdown_.AddWidget(
                    new InitialButtonParameters
                    {
                        Text = sublayout.LayoutName,
                        Image = workspaceIcon,
                        Click = (sender_, args_) => workspaceManager.LoadWorkspace(subLayoutCopied.LayoutName)
                    });
                hasWorkspace = true;
            }
            EventHandler<SubLayoutChangedEventArgs> handler = null;
            handler = (sender_, args_) =>
            {
                subLayoutManager.SubLayoutChanged -= handler;
                PopulateWorkspaces2(dropdown_);
            };
            subLayoutManager.SubLayoutChanged += handler;
            IDropdownButtonViewContainer dropDown = dropdown_ as IDropdownButtonViewContainer;
            if (dropDown != null)
            {
                dropDown.IsDropDownEnabled = hasWorkspace;
            }

        }

        //cw Workspace_Support_General: 50
        /** 
         * ### Create New Workspace
         * A button can be added to the drop down button mentioned above to create an new empty workspace using the extension CreateEmptyWorkspace method by providing an instance of InitialWindowParameters(for dock layout) or InitialTileParameters(or tile layout) */
        /** WorkspaceCategory of the parameter if specified would override the default workspace category specified when enabling the workspace support */ 
        //{
        private void PopulateNewWorkspace(IWidgetViewContainer dropdown_)
        {
            dropdown_.AddWidget(new InitialButtonParameters()
            {
                Text = "Create new workspace",
                Image = workspaceIcon,
                Click = (sender_, args_) => chromeManager.CreateEmptyWorkspace(1200, 900)
            });
        }
        //}

        //cw Workspace_Support_General: 61
        //{
        private void Opening(object sender_, EventArgs eventArgs_)
        {
            Popup popup = sender_ as Popup;
            if (popup != null)
            {
                WindowEventArgs arg = eventArgs_ as WindowEventArgs;
                string workspaceCategory = arg == null ? string.Empty : arg.ViewContainer.Parameters.WorkspaceCategory;
                if (string.IsNullOrEmpty(workspaceCategory))
                {
                    workspaceCategory = GlobalWorkspaceCategory;
                }
                popup.Child = new AppStoreControl(workspaceManager, workspaceCategory);
            }
        } 
        //}

    }

    [Serializable]
    public class MyInitialTileItemParameters : InitialTileItemParameters
    {
        public string Message { get; set; }
    }
}
