﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace TileLayoutExample.WorkspacesManagement
{
    public partial class FloatingDragControl : UserControl
    {
        public FloatingDragControl()
        {
            InitializeComponent();
        }

        private void listView1_ItemDrag(object sender, ItemDragEventArgs e)
        {
            var listViewItem = e.Item as ListViewItem;
            if (listViewItem == null) return;
            var param = new TileItemDragInfo
            {
                FactoryId = listViewItem.Text,
                Parameters = new MyInitialTileItemParameters() { Message = "Hello " + listViewItem.Text}
            };
            listView1.DoDragDrop(param, DragDropEffects.All);
        }
    }
}
