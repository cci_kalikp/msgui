﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MSDesktop.Workspaces.Interfaces;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace TileLayoutExample.WorkspacesManagement
{
    /// <summary>
    /// Interaction logic for AppStoreControl.xaml
    /// </summary>
    public partial class AppStoreControl : UserControl
    {
        public AppStoreControl()
        {
            InitializeComponent();
        }

        //cw Workspace_Support_General: 62
        /** The content of the dropdown can be a control displaying all the available tile items(use IWorkspaceManager.GetChildWindowFactorys method), and allowing adding them to the tile container through Drag & Drop, the tile container would accept the drop if the tile item factory id is transferred*/
        //cw Workspace_Support_General: 64
        //{
        public AppStoreControl(IWorkspaceManager workspaceManager_, string workspaceCategory_):this()
        {
            widgetListBox.ItemsSource = workspaceManager_.GetChildWindowFactorys(workspaceCategory_); 
        }

        private void widgetListBox_PreviewMouseLeftButtonDown(object sender_, MouseButtonEventArgs e_)
        {
            var listBox = sender_ as ListBox;
            var data = GetDataFromListBox(listBox, e_.GetPosition(listBox));
            if (data != null)
            {
                listBox.SelectedItem = data;
                DragDrop.DoDragDrop(listBox, ((KeyValuePair<string, ImageSource>)data).Key, DragDropEffects.Move);
                //DragDrop.DoDragDrop(listBox, new TileItemDragInfo(){FactoryId = ((KeyValuePair<string, ImageSource>)data).Key, Parameters = new InitialTileItemParameters()}, DragDropEffects.Move);
                e_.Handled = true;
            } 
        }
        //}

        private object GetDataFromListBox(ListBox source_, Point point_)
        {
            var element = source_.InputHitTest(point_) as UIElement;
            if (element == null) return null;
            object data = DependencyProperty.UnsetValue;
            while (data == DependencyProperty.UnsetValue)
            {
                data = source_.ItemContainerGenerator.ItemFromContainer(element);
                if (data == DependencyProperty.UnsetValue)
                {
                    element = VisualTreeHelper.GetParent(element) as UIElement;
                } 

                if (element == source_) return null;
            }
             
            return (data != DependencyProperty.UnsetValue) ? data : null;
        }

    }
}
