﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using MSDesktop.Workspaces;
using MSDesktop.Workspaces.Interfaces;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.Printing;
using MorganStanley.MSDotNet.MSGui.Impl.Options;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using TileLayoutExample.Message;

namespace TileLayoutExample.App2
{
    public class App2Module : IModule
    {
        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;
        private readonly IUnityContainer container;
        private readonly ICommunicator communicator;
        private readonly IEventRegistrator eventRegistrator;
        private readonly V2VConfigModule v2VConfigModule;
        private const string App2 = "Application B";
        private IModulePublisher<ActionMessage> actionPublisher;
        private readonly ImageSource App2Icon = new BitmapImage(
            new Uri(@"pack://application:,,,/TileLayoutExample;component/Resources/app2.png")); 
        readonly DataTemplate template = new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/TileLayoutExample;component/App2/ApplicationDict.xaml") }
            ["ApplicationDataTemplate"] as DataTemplate;
        public App2Module(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, 
            ICommunicator communicator_, IEventRegistrator eventRegistrator_,
            V2VConfigModule v2VConfigModule_, IUnityContainer container_)
        {
            this.chromeRegistry = chromeRegistry_;
            this.chromeManager = chromeManager_;
            this.communicator = communicator_;
            this.eventRegistrator = eventRegistrator_;
            this.v2VConfigModule = v2VConfigModule_;
            this.container = container_;
        }

        public void Initialize()
        {
            actionPublisher = communicator.GetPublisher<ActionMessage>();
            if (container.IsRegistered<IWorkspaceManager>())
            { 
                chromeRegistry.RegisterWindowFactory(App2).SetInitHandler(CreateView).AddToWorkspace(null, App2Icon);  
            }
            else
            {
                //cw Tile_Layout: 50 
                /** ### Register tile item window factory */
                //{
                chromeRegistry.RegisterWindowFactory(App2, CreateView);
                //}
                chromeManager.GetDefaultContainer()["Normal"].AddWidget(Guid.NewGuid().ToString(), new InitialButtonParameters()
                {
                    Text = "Application B",
                    Image = App2Icon,
                    Click = (sender_, args_) =>

                        //cw Tile_Layout: 70 
                        /** ### Create window*/
                        /** The initial parameters transferred must be of type InitialTileItemParameters */
                        /** Same as dockable window, if no size related parameter is specified, it's sized to its content, if SizeMethod is set to Custom, Width and Height must be specified */
                        /** InitialLocationTarget can be specified as the target tile container for this child tile item, if not specified, if no tile container, a new one would be to exactly host this tile item; if there is any existing tile container, the first/active one would be used.*/
                        /** InitialLocationCallback can be specified to calculate the startup position dynamically based on the tile container size and the tile item size */
                        //{
                        chromeManager.CreateWindow(App2, new InitialTileItemParameters()
                        {
                            AllowMaximize = true,
                            SizingMethod = SizingMethod.Custom,
                            Height = 200,
                            Width = 300,
                            //Left = 100,
                            //Top = 100,
                            //dock to the bottom right corner of the tile container
                            InitialLocationCallback = (parentWidth_, parentHeight_, viewWidth_, viewHeight_) => new PointF((float)(parentWidth_ - viewWidth_ - 4), (float)(parentHeight_ - viewHeight_ - 4)),
                            ShowFlashBorder = false,
                            InitialLocationTarget = chromeManager.Windows.LastOrDefault(w_=> w_ is ITileViewContainer)
                        })
                        //}
                }); 
            }

        }

        private bool CreateView(IWindowViewContainer emptyViewContainer_, XDocument state_)
        {
            //cw Tile_Layout: 100
            /** 
             * ## Other features
             * Other features provided by normal dockable window views are also supported in the tile item view.Take following:
             * ### V2V communication and auto connection hookup 
             * Usually the V2VConfigModule.AutoHookMode should be set to */
            /** A. "AutoHookMode.IslandMode" regardless workspace is enabled or not */
            /** B. "AutoHookMode.TabAndFloatingMode" in a) none workspace mode; AND b) the shell mode is not "LauncherBarAndFloatingWindows"; AND c) developer want to enable tile items in different tile container to communicate when the tile container are put into the same tab or are all floating */
            /** Then "SignUpAutoHook" called for the tile item, to make all the tile items in the same tile container to be hooked */
            //cw Tile_Layout: 102
            //{
            v2VConfigModule.SignUpAutoHook(emptyViewContainer_, null, null);
            var pub = eventRegistrator.RegisterPublisher<NewDateMessage>(emptyViewContainer_);
            //}

            emptyViewContainer_.Title = "Application B";
            emptyViewContainer_.Parameters.IsHeaderVisible = false;
            Grid header = new Grid();
            header.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(25) });
            header.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            header.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            header.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            Label text = new Label() { Content = "Application List" };
            header.Children.Add(text);

            Button button1 = new Button() { Content = "Print All", HorizontalAlignment = HorizontalAlignment.Right };
            button1.Click += delegate
                {
                    IPrintManager printManager = chromeManager as IPrintManager;
                    printManager.PreviewPrintAll();
                };
            header.Children.Add(button1);
            Grid.SetColumn(button1, 1);

            Button button2 = new Button() {Content = "New Transaction", HorizontalAlignment = HorizontalAlignment.Right}; 
            button2.Click += delegate {  pub.Publish(new NewDateMessage(){Date = DateTime.Today}); };
            header.Children.Add(button2);
            Grid.SetColumn(button2, 2);
 
            GroupBox panel = new GroupBox() { Header = header };
            ListView list = new ListView();


            list.ItemTemplate = template;
            Grid.SetIsSharedSizeScope(list, true);
            panel.Content = list;
            //cw Tile_Layout: 110
            /** ### Get Parent Tile Container */
            //{
            ITileItemViewContainer tileItemViewContainer = emptyViewContainer_ as ITileItemViewContainer;
            //}
            if (tileItemViewContainer != null)
            {
                tileItemViewContainer.Parameters.AllowMaximize = false;
                //{
                EventHandler<WindowEventArgs> handler = null;
                handler = (sender_, args_) =>
                    {
                        IViewCollectionContainer tileView = tileItemViewContainer.Parent;
                        if (tileView != null)
                        {
                            list.ItemsSource = tileView.Children;
                        }
                        else
                        {
                            tileItemViewContainer.ParentChanged -= handler;
                        }
                    };
                tileItemViewContainer.ParentChanged += handler;
                //}
            }
             

            list.CommandBindings.Add(new CommandBinding(FlashCommand, (sender_, args_) =>
            {
                var container2 = args_.Parameter as IWindowViewContainer;
                if (container2 != null)
                {
                    //cw Tile_Layout: 130
                    /**  
                     * ### Flash
                     */
                    //{
                    container2.Flash();
                    //}
                }
                actionPublisher.Publish(new ActionMessage(){Action = FlashCommand.Text, Application = container2.Title, Time = DateTime.Now});
            }));
            list.CommandBindings.Add(new CommandBinding(RenameCommand, (sender_, args_) =>
            {
                var container2 = args_.Parameter as IWindowViewContainer;
                if (container2 != null)
                {
                    //cw Tile_Layout: 140
                    /**  
                     * ### Rename
                     * When the tile item gots focus, header of the tile item can be invoked using WindowRenameGesture (usually Alt+F2) or execute RenameChildItemTitleCommand, and changes to the title can be committed by pressing enter 
                     **/
                    //{
                    var parent = (container2.Content as DependencyObject).FindVisualParent<TileItem>();
                    if (parent != null)
                    {
                        Command.RenameTitleCommand.Execute(null, parent);
                    //}
                        actionPublisher.Publish(new ActionMessage() { Action = RenameCommand.Text, Application = container2.Title, Time = DateTime.Now });
                    //{
                    }
                    //}
                }
            }));
            list.CommandBindings.Add(new CommandBinding(ActivateCommand, (sender_, args_) =>
            {
                var container2 = args_.Parameter as IWindowViewContainer;
                if (container2 != null)
                {
                    container2.Activate();
                    actionPublisher.Publish(new ActionMessage() { Action = ActivateCommand.Text, Application = container2.Title, Time = DateTime.Now });
                }
            }));

            list.CommandBindings.Add(new CommandBinding(PrintCommand, (sender_, args_) =>
            {
                var container2 = args_.Parameter as IWindowViewContainer;
                if (container2 != null)
                {
                    //cw Tile_Layout: 120
                    /**  
                     * ### Print 
                     */
                    //{
                    IPrintManager printManager = chromeManager as IPrintManager;
                    printManager.PreviewPrint(container2);
                    //}
                    actionPublisher.Publish(new ActionMessage() { Action = PrintCommand.Text, Application = container2.Title, Time = DateTime.Now });
                }
            }));
            list.CommandBindings.Add(new CommandBinding(CloseCommand, (sender_, args_) =>
            {
                var container2 = args_.Parameter as IWindowViewContainer;
                if (container2 != null)
                {
                    //cw Tile_Layout: 170
                    /**  
                     * ### Close tile item programmatically 
                     */
                    //{
                    container2.Close();
                    //}
                    actionPublisher.Publish(new ActionMessage() { Action = CloseCommand.Text, Application = container2.Title, Time = DateTime.Now });
                }
            }));
             
            emptyViewContainer_.Content = panel;
            return true;
        }
         

        public static RoutedUICommand FlashCommand = new RoutedUICommand("Flash", "FlashCommand", typeof(Button));
        public static RoutedUICommand RenameCommand = new RoutedUICommand("Rename", "RenameCommand", typeof(Button));
        public static RoutedUICommand ActivateCommand = new RoutedUICommand("Activate", "ActivateCommand", typeof(Button));
        public static RoutedUICommand PrintCommand = new RoutedUICommand("Print", "PrintCommand", typeof(Button));
        public static RoutedUICommand CloseCommand = new RoutedUICommand("Close", "CloseCommand", typeof(Button));
    }
}
