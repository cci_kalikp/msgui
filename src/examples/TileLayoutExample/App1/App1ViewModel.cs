﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace TileLayoutExample.App1
{
 
    public class App1ViewModel : ViewModelBase
    {
        public App1ViewModel()
        {
            Details = new ObservableCollection<App1Detail>
                {
                    new App1Detail(this)
                        {
                            Amount = 100,
                            Comment = "comment 1",
                            Name = "Transaction1",
                            TransactionDate = DateTime.Now.AddDays(-10)
                        },
                    new App1Detail(this)
                        {
                            Amount = 150,
                            Comment = "comment 2",
                            Name = "Transaction1",
                            TransactionDate = DateTime.Now.AddDays(-2)
                        },
                    new App1Detail(this)
                        {
                            Amount = 600.77m,
                            Comment = "comment 3",
                            Name = "Transaction1",
                            TransactionDate = DateTime.Now.AddHours(-5)
                        }
                };
            Details.CollectionChanged += Details_CollectionChanged;
        }

        void Details_CollectionChanged(object sender_, NotifyCollectionChangedEventArgs e)
        {
           if (e.Action == NotifyCollectionChangedAction.Add || 
               e.Action == NotifyCollectionChangedAction.Remove)
           {
               TotalCount = Details.Count;
           }
        }

        private decimal totalAmount;
        public decimal TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; OnPropertyChanged("TotalAmount"); }
        }

        private int totalCount;
        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; OnPropertyChanged("TotalCount"); }
        }

        private DateTime? lastTransactionDate;
        public DateTime? LastTransactionDate
        {
            get { return lastTransactionDate; }
            set { lastTransactionDate = value; OnPropertyChanged("LastTransactionDate"); }
        }

        public ObservableCollection<App1Detail> Details { get; private set; }

        public void NewTransaction(DateTime date_)
        {
            this.Details.Add(new App1Detail(this)
            {
                TransactionDate = date_, 
                Amount = new Random().Next(1000), 
                Name = "Transaction" + (Details.Count + 1), 
                Comment = "comment " + (Details.Count + 1)
            });
        }
    }

    public class App1Detail:ViewModelBase
    {
        private App1ViewModel parent;
        public App1Detail(App1ViewModel parent_)
        {
            parent = parent_;
            
        }
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged("Name"); }
        }

        private DateTime transactionDate;
        public DateTime TransactionDate
        {
            get { return transactionDate; }
            set
            {
                if (transactionDate != value)
                {
                    transactionDate = value;
                    OnPropertyChanged("TransactionDate");
                    if (parent.LastTransactionDate == null ||
                        parent.LastTransactionDate.Value.CompareTo(transactionDate) < 0)
                    {
                        parent.LastTransactionDate = transactionDate;
                    }
                }
            }
        }

        private decimal amount;
        public Decimal Amount
        {
            get { return amount; }
            set 
            {
                if (amount != value)
                {
                    parent.TotalAmount += value - amount;
                    amount = value;
                    OnPropertyChanged("Amount");  
                }
            }
        }

        private string comment;
        public string Comment
        {
            get { return comment; }
            set { comment = value; OnPropertyChanged("Comment"); }
        }
    }
}
