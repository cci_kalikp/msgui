﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using MSDesktop.Workspaces.Interfaces;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MSDesktop.Workspaces;
using MorganStanley.MSDotNet.MSGui.Core.Messaging; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using TileLayoutExample.Message;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;

namespace TileLayoutExample.App1
{
    public class App1Module:IModule
    {
        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;
        private readonly IUnityContainer container; 
        private readonly IEventRegistrator eventRegistrator;
        private readonly V2VConfigModule v2VConfigModule;
        private const string App1 = "Application A";
        private readonly ImageSource App1Icon = new BitmapImage(
            new Uri(@"pack://application:,,,/TileLayoutExample;component/Resources/app1.png")); 
        private const string App1ButtonId1 = "App1ButtonId1";
        private const string App1ButtonId2 = "App1ButtonId2";

        //cw Tile_Layout: 40 
        /** ### Module Constructor */
        //{
        public App1Module(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, 
        //}
            IEventRegistrator eventRegistrator_,  
            V2VConfigModule v2VConfigModule_, IUnityContainer container_)
        {
            this.container = container_;
            //{
            this.chromeRegistry = chromeRegistry_;
            this.chromeManager = chromeManager_;
            //}
            this.eventRegistrator = eventRegistrator_;
            this.v2VConfigModule = v2VConfigModule_;

            //cw Tile_Layout: 101
            //{
            this.v2VConfigModule.AutoHookMode = AutoHookMode.IslandMode;
            //}
        }

        public void Initialize()
        {
             if (container.IsRegistered<IWorkspaceManager>())
            {
                //cw Workspace_Support_General: 70
                /** 
                 * ### Register window factory available for workspace
                 */
                /** Chain the call to AddToWorkspace extension method after registering the WindowFactor*/
                /** The image can be provided to populate the icon in the "Edit" dropdown; workspace category can be provided to override the default workspace category; layout engine can be provided to override the default layout mode(dock/tile)*/
                //{
                chromeRegistry.RegisterWindowFactory(App1).SetInitHandler(CreateView).AddToWorkspace(null, App1Icon, LayoutEngine.Default);
                 //}
            }
            else
            {
                chromeRegistry.RegisterWindowFactory(App1, CreateView);
                //cw Tile_Layout: 80 
                /** ### Button to invoke tile item window*/
                /** If no specified tile item parameters is needed, setting WindowLayout property of InitialShowWindowButtonParameters to "LayoutEngine.Tile" is enough */
                 //{
                //chromeManager[ChromeArea.Ribbon]["Tools"] or chromeManager[ChromeArea.LauncherBar] as the result of  chromeManager.GetDefaultContainer() depending on the shell mode
                chromeManager.GetDefaultContainer()["Normal"].AddWidget(Guid.NewGuid().ToString(), new InitialShowWindowButtonParameters()
                {
                    WindowFactoryID = App1,
                    Text = "Application A",
                    Image = App1Icon,
                    WindowLayout = LayoutEngine.Tile 
                });
                 //}
            }

         }

        //cw Tile_Layout: 60  
        //{
        private bool CreateView(IWindowViewContainer emptyViewContainer_, XDocument state_)
        {
            var model = new App1ViewModel();
            App1View view = new App1View();
            view.DataContext = model;
            emptyViewContainer_.Content = view;
            emptyViewContainer_.Title = "Application A";
            emptyViewContainer_.Icon =new Uri(@"pack://application:,,,/TileLayoutExample;component/Resources/app1.png").ToIcon(new PngBitmapEncoder());
             
            string button2Id = App1ButtonId2 + Guid.NewGuid(); 
            chromeRegistry.RegisterWidgetFactory(button2Id, (container_, state2_) =>
            {
                var button = new Button();
                button.Content = container_.Parameters.Text;
                button.Click += (sender_, args_) =>
                {

                    var parent = button.FindVisualParent<TileItem>();
                    if (parent != null)
                    {
                        Command.RenameTitleCommand.Execute(null, parent);

                    }
                };

                container_.Content = button;
                return true;
            });
            emptyViewContainer_.Header.AddWidget(button2Id, new InitialWidgetParameters()
            {
                Text = "Change Title",
            });

       //}
            //cw Tile_Layout: 150
            /**  
             * ### Add widget to view header
             */
            //{
            string button1Id = App1ButtonId1 + Guid.NewGuid();
            chromeRegistry.RegisterWidgetFactory(button1Id, (container_, state1_) =>
            {
                var button = new Button();
                button.Content = container_.Parameters.Text;
                container_.Content = button;
                return true;
            });
            emptyViewContainer_.Header.AddWidget(button1Id, new InitialWidgetParameters()
            {
                Text = "Hello World",
            });

            //}


            //(emptyViewContainer_.Parameters as InitialTileItemParameters).AllowMaximize = true;
            v2VConfigModule.SignUpAutoHook(emptyViewContainer_, null, null);
            var sub = eventRegistrator.RegisterSubscriber<NewDateMessage>(emptyViewContainer_);
            sub.GetObservable().Subscribe(m_ => view.Dispatcher.Invoke((Action)(() => model.NewTransaction(m_.Date))));

        //cw Tile_Layout: 61  
        //{
             return true;
        }
        //}
 
    }
}
