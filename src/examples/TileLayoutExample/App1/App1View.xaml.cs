﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TileLayoutExample.App1
{
    /// <summary>
    /// Interaction logic for App1View.xaml
    /// </summary>
    public partial class App1View : UserControl
    {
        public App1View()
        {
            InitializeComponent();
        }
    }
}
