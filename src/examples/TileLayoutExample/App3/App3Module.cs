﻿using System; 
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using MSDesktop.Workspaces.Interfaces;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MSDesktop.Workspaces;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using TileLayoutExample.Message;

namespace TileLayoutExample.App3
{
    public class App3Module:IModule
    {
        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;
        private readonly IUnityContainer container;
        private readonly IEventRegistrator eventRegistrator;
        private readonly V2VConfigModule v2VConfigModule;
        private const string App1 = "Application C";
        private readonly ImageSource App3Icon = new BitmapImage(
            new Uri(@"pack://application:,,,/TileLayoutExample;component/Resources/app3.png"));

        public App3Module(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_,
            IEventRegistrator eventRegistrator_,  V2VConfigModule v2VConfigModule_, IUnityContainer container_)
        {
            this.container = container_;
            this.chromeRegistry = chromeRegistry_;
            this.chromeManager = chromeManager_; 
            this.eventRegistrator = eventRegistrator_;
            this.v2VConfigModule = v2VConfigModule_;
        }

        public void Initialize()
        {
             
            if (container.IsRegistered<IWorkspaceManager>())
            {
                chromeRegistry.RegisterWindowFactory(App1).SetInitHandler(CreateView).AddToWorkspace(image_: App3Icon);
            }
            else
            {
                chromeRegistry.RegisterWindowFactory(App1, CreateView);
                //cw Tile_Layout: 90  
                /** The InitialParameters property of InitialShowWindowButtonParameters can be specified to provide more specific tile item configuration*/
                //{
                //chromeManager[ChromeArea.Ribbon]["Tools"] or chromeManager[ChromeArea.LauncherBar] as the result of chromeManager.GetDefaultContainer() depending on the shell mode
                chromeManager.GetDefaultContainer()["Normal"].AddWidget(Guid.NewGuid().ToString(), new InitialShowWindowButtonParameters()
                {
                    WindowFactoryID = App1,
                    Text = "Application C",
                    Image = App3Icon,
                    InitialParameters =  new InitialTileItemParameters()
                        {
                            
                        }
                });
                //}
            }
 
            
        }

        private bool CreateView(IWindowViewContainer emptyViewContainer_, XDocument state_)
        {
            emptyViewContainer_.Parameters.ShowFlashBorder = false;
            emptyViewContainer_.Title = "Application C";

            if (1 == 2)
            {   
                //cw Tile_Layout: 160
                /**  
                 * ### Hide tile item header
                 */
                //{
                emptyViewContainer_.Parameters.IsHeaderVisible = false;
                //}
            }

            emptyViewContainer_.Parameters.Resources = new ResourceDictionary()
                {
                    Source = new Uri(@"pack://application:,,,/TileLayoutExample;component/App3/App3ViewDict.xaml")
                };
            var model = new App3ViewModel();
            ListView list = new ListView(); 
            list.ItemsSource = model.Details;
            WinToBrushConverter converter = new WinToBrushConverter();
            foreach (var app3DetailViewModel in model.Details)
            {
                var copy = app3DetailViewModel;
                copy.PropertyChanged += (sender_, args_) =>
                    {
                        Color color = ((SolidColorBrush)converter.Convert(copy.Win, typeof(SolidColorBrush), null, null)).Color;
                        emptyViewContainer_.Flash(color);
                    };
            }

            v2VConfigModule.SignUpAutoHook(emptyViewContainer_, null, null);

            //cw Tile_Layout: 105
            //{
            var sub = eventRegistrator.RegisterSubscriber<NewDateMessage>(emptyViewContainer_);
            //}
            sub.GetObservable().Subscribe(m_ => list.Dispatcher.Invoke((Action)(() => model.TryLuck(m_.Date)))); 

            emptyViewContainer_.Content = list; 
            return true;
        }
    }
}
