﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace TileLayoutExample.App3
{
    public class WinToStateConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int win = (int) value;
            if (win > 60) return "Wow, I won lots of money.";
            if (win > 0) return "Yeah, I won some money.";
            if (win == 0) return "Well, I loose nothing.";
            if (win < -60) return "Argh, I lost lots of money.";
            return "Alas, I lost some money.";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
