﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel; 

namespace TileLayoutExample.App3
{
 
    public class App3ViewModel
    {
        public App3ViewModel()
        {
            Details = new ObservableCollection<App3DetailViewModel>
                {
                    new App3DetailViewModel()
                        { 
                            Date = DateTime.Today.AddDays(-5),
                            Win = new Random(1).Next(-100, 100)
                        },
                    new App3DetailViewModel()
                        {
                             Date = DateTime.Today.AddDays(-4),
                            Win = new Random(2).Next(-100, 100)
                        },
                    new App3DetailViewModel()
                        {
                            Date = DateTime.Today.AddDays(-3),
                            Win = new Random(3).Next(-100, 100)
                        },
                    new App3DetailViewModel()
                        { 
                            Date = DateTime.Today.AddDays(-2),
                            Win = new Random(4).Next(-100, 100)
                        },
                    new App3DetailViewModel()
                        { 
                            Date = DateTime.Today.AddDays(-1),
                            Win = new Random(5).Next(-100, 100)
                        }
                };
        }

        public ObservableCollection<App3DetailViewModel> Details { get; private set; }

        public void TryLuck(DateTime dateTime_)
        {
            var item = new App3DetailViewModel();
            item.CommandTryLuck.Execute(null);
            item.Date = dateTime_;
            this.Details.Add(item);
        }
    }

    public class App3DetailViewModel : ViewModelBase
    {
        public App3DetailViewModel()
        {
             CommandTryLuck = new DelegateCommand(o_ =>
                 {
                     this.Win = new Random().Next(-100, 100); 

                 });
        }

        public ICommand CommandTryLuck { get; private set; }
        private DateTime? date = null;

        public DateTime? Date
        {
            get { return date; }
            set { date = value; OnPropertyChanged("Date"); }
        }
 

        private int win;
        public int Win
        {
            get { return win; }
            set
            {
                win = value;
                OnPropertyChanged("Win");
            }
        }
    }
     
}
