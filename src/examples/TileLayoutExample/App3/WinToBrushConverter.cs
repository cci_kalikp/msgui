﻿using System;
using System.Collections.Generic; 
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media; 

namespace TileLayoutExample.App3
{
    public class WinToBrushConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int win = (int) value;
            return win >= 0 ? new SolidColorBrush(Colors.Green) : new SolidColorBrush(Colors.Red);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
