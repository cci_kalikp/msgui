﻿using System;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.Routing;

namespace MessageRoutingExampleOne.Modules
{
    [Serializable]
    public class DemoThreeMessage : IExplicitTarget
    {
        public string HostName { get; set; }
        public string CPSServer { get; set; } 
        public int I { get; set; }
        public Endpoint Target
        {
            get
            {
                return new Endpoint()
                    {
                        Application = "MSDesktop Third",
                        Hostname = string.IsNullOrEmpty(HostName) ? "KUIT000154W" : HostName,
                        Username = Environment.UserName,
                        CpsServer = string.IsNullOrEmpty(CPSServer) ? "ms.tcp://igrid11362.devin1.ms.com:9992" : CPSServer, 
                        Kerberosed = true
                    };
            }
        }

        public override string ToString()
        {
            return "DemoThreeMessage" + I;
        }
    }
}