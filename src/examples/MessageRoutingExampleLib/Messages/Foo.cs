﻿using System;
using MSDesktop.MessageRouting.Message;

namespace MessageRoutingExampleLib.Messages
{
    [Serializable]
    public class Foo : IProvideMessageInfo
    {
        public string Action { get; set; }
        public string Message { get; set; }

        public string GetInfo()
        {
            return "Foo message.";
        }

        public override string ToString()
        {
            return string.Format("[Foo]: {0}", this.Message);
        }
    }
}
