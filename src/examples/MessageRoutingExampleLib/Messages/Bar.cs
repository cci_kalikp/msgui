﻿using System;
using MSDesktop.MessageRouting.Message;

namespace MessageRoutingExampleLib.Messages
{
    [Serializable]
    public class Bar
    {
        public string Action { get; set; }
        public string Message { get; set; }
         
        public override string ToString()
        {
            return string.Format("[BAR]: {0}", this.Message);
        }
    }
}
