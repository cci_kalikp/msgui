﻿using System;
using MSDesktop.MessageRouting.Message;

namespace MessageRoutingExampleLib.Messages
{
    [Serializable]
    public class Test 
    {
        public string Action { get; set; }
        public string Message { get; set; }
 
        public override string ToString()
        {
            return string.Format("[Test]: {0}", this.Message);
        }
    }
}
